mppeuct.controller('Consultar', ['$scope', '$location', '$rootScope', 'redimencionar', '$http', '$timeout', '$sce', 'ngProgressLite', 'session', function ($scope, $location, $rootScope, redimencionar, $http, $timeout, $sce, ngProgressLite, session) {
        ngProgressLite.start();
        ngProgressLite.set(0.2);
        $rootScope.show2 = 0;
        $rootScope.iniciar = true;
        $rootScope.menuActive = $location.path();
        $scope.formData = {};
        $scope.datos_registro = {};
        $rootScope.title = "Consultar";
        $scope.pageClass2 = 'hidden';
        $http.post("index.php?lugar=doc&accion=Datos_consultar")
                .success(function (respuesta) {
                    if (String(respuesta.data).trim() != "false") {
                        $scope.consultageneral = respuesta.data;
                        $rootScope.datosconsulta = 1;
                    } else {
                        $rootScope.datosconsulta = 0;
                    }
                }).error(function () {
            console.log('error');
        });
        $http.post("index.php?lugar=doc&accion=Consultar_usuario")
                .success(function (respuesta) {
                    if (String(respuesta.data).trim() != "false") {
                        $scope.consultageneraladmin = respuesta.data;
                        $rootScope.datosconsultaadmin = 1;
                    } else {
                        $rootScope.datosconsultaadmin = 0;
                    }
                }).error(function () {
            console.log('error');
        });
        $scope.estatus = function (id, status, usuario) {
            if (status === 't') {
                $scope.statusact = 'Activo';
                $scope.statusina = 'Inactivo';
            } else {
                $scope.statusina = 'Activo';
                $scope.statusact = 'Inactivo';
            }
            $.jAlert({'type': 'confirm',
                'title': '¡Reiniciar Clave Usuario!',
                'size': 'md',
                'theme': 'black',
                'backgroundColor': 'white',
                'confirmQuestion': '<font style="font-size:18px;">¿Estás seguro que deseas cambiar el estatus al usuario:<br> <strong style=" color: red;">' + usuario + '</strong> de ' + $scope.statusact + ' a ' + $scope.statusina + '</font>',
                'confirmBtnText': 'Si', 'denyBtnText': 'No',
                'showAnimation': 'flipInX',
                'hideAnimation': 'flipOutX',
                'onConfirm': function () {
                    $scope.temp = {'id': id, 'status': status};
                    $http.post("index.php?lugar=doc&accion=Cambiar_estatus", $scope.temp)
                            .success(function (respuesta) {
                                if (respuesta.data != "false") {
                                    $.jAlert({
                                        'title': '¡Exito!',
                                        'content': 'Se ha actualizado el estatus correctamente.',
                                        'theme': 'green',
                                        'size': 'md',
                                        'showAnimation': 'bounceInDown',
                                        'hideAnimation': 'bounceOutDown',
                                        'btns': {'text': 'Aceptar', 'theme': 'green'}
                                    });
                                    window.location.href = "#/Consultar_usuario_act";
                                } else {
                                    $.jAlert({
                                        'title': '¡Error!.',
                                        'content': 'No se han guardado los datos, intenta de nuevo.',
                                        'theme': 'red',
                                        'size': 'md',
                                        'showAnimation': 'bounceInDown',
                                        'hideAnimation': 'bounceOutDown',
                                        'btns': {'text': 'Aceptar', 'theme': 'red'}
                                    });
                                }
                            }).error(function () {
                        $.jAlert({
                            'title': '¡Error!.',
                            'content': 'Error al actualizado el estatus del usuario.',
                            'theme': 'red',
                            'size': 'md',
                            'showAnimation': 'bounceInDown',
                            'hideAnimation': 'bounceOutDown',
                            'btns': {'text': 'Aceptar', 'theme': 'red'}
                        });
                        console.log('error');
                    });
                }, 'onDeny': function () {
                }
            });
        };
        $scope.reiniciar = function (id, cedula, usuario) {
            $.jAlert({'type': 'confirm',
                'title': '¡Reiniciar Clave Usuario!',
                'size': 'md',
                'theme': 'black',
                'backgroundColor': 'white',
                'confirmQuestion': '<font style="font-size:18px;">¿Estás seguro que deseas reiniciar la clave de acceso al usuario: <strong style=" color: red;">' + usuario + '</strong>?</font>',
                'confirmBtnText': 'Si', 'denyBtnText': 'No',
                'showAnimation': 'flipInX',
                'hideAnimation': 'flipOutX',
                'onConfirm': function () {
                    $scope.temp = {'id': id, 'cedula': cedula};
                    $http.post("index.php?lugar=doc&accion=Reiniciar_clave", $scope.temp)
                            .success(function (respuesta) {
                                if (respuesta.data != "false") {
                                    $.jAlert({
                                        'title': '¡Exito!',
                                        'content': 'Se ha reiniciado la clave correctamente.',
                                        'theme': 'green',
                                        'size': 'md',
                                        'showAnimation': 'bounceInDown',
                                        'hideAnimation': 'bounceOutDown',
                                        'btns': {'text': 'Aceptar', 'theme': 'green'}
                                    });
                                    window.location.href = "#/Consultar_usuario_act";
                                } else {
                                    $.jAlert({
                                        'title': '¡Error!.',
                                        'content': 'No se han guardado los datos, intenta de nuevo.',
                                        'theme': 'red',
                                        'size': 'md',
                                        'showAnimation': 'bounceInDown',
                                        'hideAnimation': 'bounceOutDown',
                                        'btns': {'text': 'Aceptar', 'theme': 'red'}
                                    });
                                }
                            }).error(function () {
                        $.jAlert({
                            'title': '¡Error!.',
                            'content': 'Error al reiniciar clave del usuario.',
                            'theme': 'red',
                            'size': 'md',
                            'showAnimation': 'bounceInDown',
                            'hideAnimation': 'bounceOutDown',
                            'btns': {'text': 'Aceptar', 'theme': 'red'}
                        });
                        console.log('error');
                    });
                }, 'onDeny': function () {
                }
            });
        };
        $scope.editar = function (id, perfil_id, usuario) {
            $.jAlert({'type': 'confirm',
                'title': '¡Cambiar Perfil Usuario!',
                'size': 'md',
                'theme': 'black',
                'backgroundColor': 'white',
                'confirmQuestion': '<font style="font-size:18px;">¿Estás seguro que deseas cambiar el perfil al usuario: <strong style=" color: red;">' + usuario + '</strong>?</font>',
                'confirmBtnText': 'Si', 'denyBtnText': 'No',
                'showAnimation': 'flipInX',
                'hideAnimation': 'flipOutX',
                'onConfirm': function () {
                    $scope.temp = {'id': id, 'perfil_id': perfil_id};
                    $http.post("index.php?lugar=doc&accion=Editar_Perfil_usuario", $scope.temp)
                            .success(function (respuesta) {
                                if (respuesta.data != "false") {
                                    $.jAlert({
                                        'title': '¡Exito!',
                                        'content': 'Se ha actualizado correctamente.',
                                        'theme': 'green',
                                        'size': 'md',
                                        'showAnimation': 'bounceInDown',
                                        'hideAnimation': 'bounceOutDown',
                                        'btns': {'text': 'Aceptar', 'theme': 'green'}
                                    });
                                    window.location.href = "#/Consultar_usuario_act";
                                } else {
                                    $.jAlert({
                                        'title': '¡Error!.',
                                        'content': 'No se han guardado los datos, intenta de nuevo.',
                                        'theme': 'red',
                                        'size': 'md',
                                        'showAnimation': 'bounceInDown',
                                        'hideAnimation': 'bounceOutDown',
                                        'btns': {'text': 'Aceptar', 'theme': 'red'}
                                    });
                                }
                            }).error(function () {
                        $.jAlert({
                            'title': '¡Error!.',
                            'content': 'Error al actualizar perfil del usuario.',
                            'theme': 'red',
                            'size': 'md',
                            'showAnimation': 'bounceInDown',
                            'hideAnimation': 'bounceOutDown',
                            'btns': {'text': 'Aceptar', 'theme': 'red'}
                        });
                        console.log('error');
                    });
                }, 'onDeny': function () {
                }
            });
        };
        $scope.doSomething = function () {
            $timeout(function () {
                $('.tabla_consulta').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'excel', 'csv', 'pdf'
                    ],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                });
                $scope.pageClass2 = 'animated slideInDown';
                $timeout(function () {
                    ngProgressLite.done();
                    $rootScope.show = 1;
                    $rootScope.show2 = 1;
                    $(".left-side").height($(".content").height());
                }, 100, true);
            }, 0);
            redimencionar.redimencionar();
        }
        $scope.ver = function (id) {
            ngProgressLite.start();
            ngProgressLite.set(0.2);
            $rootScope.show2 = 0;
            var body = '';
            $rootScope.modaltitle = true;
            $rootScope.modal = {
                template: ""
            };
            $rootScope.modal = {
                body: body,
                footer: "",
                id: id,
                tiempo: true,
                template: "index.php?lugar=home&accion=Ver"
            };
        };

    }]);