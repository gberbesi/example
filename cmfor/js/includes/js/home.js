// Creacion del modulo
var mppeuct = angular.module('mppeuct', ['ngRoute', 'ngSanitize', 'ngProgressLite']);
// Configuracion de las rutas
mppeuct.config(['$routeProvider', 'ngProgressLiteProvider', '$httpProvider', function ($routeProvider, ngProgressLiteProvide, $httpProvider) {
        $routeProvider
                .when('/Registrar_solicitud', {
                    templateUrl: 'index.php?lugar=home&accion=Registrar'
                })
                .when('/Consultar', {
                    templateUrl: 'index.php?lugar=home&accion=Consultar',
                    controller: 'Consultar'
                })
                .when('/Consultar_usuario', {
                    templateUrl: 'index.php?lugar=home&accion=Consultar_usuario',
                    controller: 'Consultar'
                })
                .otherwise({
                    redirectTo: '/Registrar_solicitud'
                });
        var requests = 0;
        function show() {
            requests++;
//            if (requests === 1) {
//                $.post("index.php?lugar=login&accion=SesionAjax", function (respuesta) {
//                    if (respuesta.data == 'false') {
//                        window.location.href = "./";
//                    }
//                }, 'json');
//            }
        }
        function hide() {
            requests--;
            if (requests === 0) {
            }
        }
        $httpProvider.interceptors.push(function ($q) {
            return {
                'request': function (config) {
                    show();
                    return $q.when(config);
                }, 'response': function (response) {
                    hide();
                    return $q.when(response);
                }, 'responseError': function (rejection) {
                    hide();
                    return $q.reject(rejection);
                }
            };
        });
        ngProgressLiteProvide.settings.speed = 300;
    }]);
angular.module('main', ['ui.router'])
        .config(['$locationProvider', '$stateProvider', function ($locationProvider, $stateProvider) {
                $stateProvider
                        .state('home', {
                            url: '/',
                            views: {
                                'header': {
                                    templateUrl: '/app/header.html'
                                },
                                'content': {
                                    templateUrl: '/app/content.html'
                                }
                            }
                        });
            }]);
mppeuct.directive('sizeelement', ['$window', '$rootScope', function ($window, $rootScope) {
        return function (scope, element) {
            var w = angular.element(element);
            var e = angular.element($window);
            scope.getWindowDimensions = function () {
                return {'h': w.height()
                    , 'w': w.width(), 'min': e.width()};
            };
            scope.$watch(scope.getWindowDimensions, function (newValue) {
                $rootScope.height = newValue.h;
                $rootScope.windowWidth = newValue.w;
                $rootScope.minWidth = newValue.min - newValue.h;

                scope.style = function () {
                    return {
                        'height': (newValue.h - 100) + 'px',
                        'width': (newValue.w - 100) + 'px'
                    };
                };

            }, true);
            e.bind('resize', function () {
                if (e.width() <= 561) {
                    $('.logo').hide();
                    $('body > .header .navbar').css("margin", "0");
                } else {
                    $('.logo').show();
                    $('body > .header .navbar').css("margin-left", "220px");
                }
                scope.$apply();
            });
        };
    }]);
mppeuct.factory('redimencionar', ['$timeout', function ($timeout) {
        var result = {
            redimencionar: function () {
                if ($(window).width() <= 992) {
                    $('.row-offcanvas').removeClass('active');
                    $('.left-side').removeClass("collapse-left");
                    $(".right-side").removeClass("strech");
                    $('.row-offcanvas').removeClass("relative");
                }
                if ($(window).width() <= 561) {
                    $('.logo').hide();
                    $('body > .header .navbar').css("margin", "0");
                } else {
                    $('.logo').show();
                    $('body > .header .navbar').css("margin-left", "220px");
                }
                $('.sidebar').affix({offset: {top: 105}});

                return true;
            }
        };
        return result;

    }]);
mppeuct.factory('session', ['$http', '$rootScope', '$window', function ($http, $rootScope, $window) {
        var result = {
            validar_redirec: function () {
                $http.post("index.php?lugar=login&accion=SesionAjax")
                        .success(function (respuesta) {
                            if (respuesta.data == 'false') {
                                $window.location.href = './'
                            }
                            ;
                        }).error(function () {
                    console.log('error');
                });
            },
            validar_return: function () {
                var data = false;
                $http.post("index.php?lugar=login&accion=SesionAjax")
                        .success(function (respuesta) {
                            if (respuesta.data == 'false') {
                                data = false;
                            }
                            return  data;
                        }).error(function () {
                    console.log('error');
                });
            }
        };
        return result;
    }]);
mppeuct.controller('Inicio', ['$scope', '$location', '$rootScope', 'redimencionar', '$timeout', 'ngProgressLite', function ($scope, $location, $rootScope, redimencionar, ngProgressLite) {
        $rootScope.menuActive = $location.path();
        $scope.pageClass = 'animated fadeInDown';
        $rootScope.title = "Inicio";
        redimencionar.redimencionar();
    }]);
mppeuct.controller('app', ["$scope", "$window", "$rootScope", '$timeout', 'ngProgressLite', function ($scope, $window, $rootScope, $timeout, ngProgressLite) {
        $rootScope.mostrarOcultar = function () {
            if ($(window).width() <= 992) {
                $('.row-offcanvas').toggleClass('active');
                $('.row-offcanvas').toggleClass("relative");
                $('.left-side').removeClass("collapse-left");
                $(".right-side").removeClass("strech");
            } else {
                $('.left-side').toggleClass("collapse-left");
                $(".right-side").toggleClass("strech");
            }
            ;
        }
    }]);