mppeuct.controller('Registrar_solicitud', ['$scope', '$location', '$rootScope', 'redimencionar', '$http', '$timeout', '$sce', 'ngProgressLite', "session", function ($scope, $location, $rootScope, redimencionar, $http, $timeout, $sce, ngProgressLite, session) {
        ngProgressLite.start();
        ngProgressLite.set(0.6);
        $rootScope.show2 = 0;
        $rootScope.iniciar = true;
        $rootScope.menuActive = $location.path();
        $scope.formData = {};
        $scope.informcacion = {};
        $scope.datos_registro = {};
        $scope.iniVista = 0;
        $scope.formData.n = 0;
        $rootScope.title = "Adjudicacion-vv";
        $scope.titlepagina = 'Adjudicacion-vv';
        $rootScope.evento = '';
        $scope.finishLoading = function () {
            $(document).scrollTop(0);
            if ($scope.iniVista > 0) {
                if ($scope.iniVista == 1) {
                    $scope.pageClass2 = 'animated fadeOutLeft';
                    $timeout(function () {
                        $scope.pageClass2 = 'animated fadeInRight';
                        ngProgressLite.done();
                    }, 250);
                } else {
                    $scope.pageClass2 = 'animated fadeOutRight';
                    $timeout(function () {
                        $scope.pageClass2 = 'animated fadeInLeft';
                        ngProgressLite.done();
                    }, 250);
                }
            }
        }
        $http.post("index.php?lugar=doc&accion=Datos_registro")
                .success(function (respuesta) {
                    if (respuesta.data) {
                        $scope.datos_registro = respuesta.data;
                        $scope.template = "index.php?lugar=home&accion=tabs&t=tab1";
                        $scope.pageClass2 = 'animated slideInDown';
                        $timeout(function () {
                            redimencionar.redimencionar();
                            ngProgressLite.done();
                            $rootScope.show = 1;
                            $rootScope.show2 = 1;
                        }, 200);
                    }
                }).error(function () {
            console.log('error');
        });
        $scope.ubicacion = {
            otrorevisor: function () {
                ngProgressLite.start();
                ngProgressLite.set(0.6);
                $scope.temp = $scope.formData;
                $http.post("index.php?lugar=doc&accion=ConsultarViviendaPatria", $scope.temp)
                        .success(function (respuesta) {
                            if (String(respuesta.data).trim() != "false") {
                                $scope.ano = respuesta.data.fe_nac.substr(0, 4);
                                $scope.mes = respuesta.data.fe_nac.substr(5, 2);
                                $scope.dia = respuesta.data.fe_nac.substr(8, 2);
                                $scope.fechanacfor = $scope.dia + '/' + $scope.mes + '/' + $scope.ano;

                                $scope.dteDate = new Date();
                                $scope.edadano = $scope.dteDate.getFullYear() - $scope.ano;
                                $scope.edad = ($scope.dteDate.getFullYear() + 1900) - $scope.ano;
                                if ($scope.dteDate.getMonth() < ($scope.mes - 1)) {
                                    $scope.edad--;
                                }
                                if ((($scope.mes - 1) == $scope.dteDate.getMonth()) && ($scope.dteDate.getDate() < $scope.dia)) {
                                    $scope.edad--;
                                }
                                if ($scope.edad > 1900) {
                                    $scope.edad -= 1900;
                                }
                                $scope.formData.fechanac = $scope.fechanacfor;
                                $scope.formData.edad = $scope.edad;
                                $scope.informcacion = respuesta.data;
                                ngProgressLite.start();
                                ngProgressLite.set(0.6);
                                $scope.template = "index.php?lugar=home&accion=tabs&t=tab2";
                                $scope.iniVista = 1;
                            } else {
                                $.jAlert({
                                    'title': '¡Adjudicación!.',
                                    'content': 'Usted no ha sido Pre-Adjudicado.',
                                    'theme': 'yellow',
                                    'size': 'md',
                                    'showAnimation': 'bounceInDown',
                                    'hideAnimation': 'bounceOutDown',
                                    'btns': {'text': 'Aceptar', 'theme': 'red'}
                                });
                            }

                        }).error(function () {
                    console.log('error');
                });
            }
            //fin consultar otro revisor
        };
        $scope.submitForm = function (formData, tab) {
            $scope.formData = {};
            $scope.informcacion = {};
            ngProgressLite.start();
            ngProgressLite.set(0.6);
            $scope.formData.n = 0;
            $scope.template = "index.php?lugar=home&accion=tabs&t=" + tab;
        };
        $scope.submitForm2 = function (formData, tab) {
            $scope.formData = {};
            $scope.informcacion = {};
            ngProgressLite.start();
            ngProgressLite.set(0.6);
            $scope.formData.n = 0;
            $scope.template = "index.php?lugar=home&accion=tabs&t=" + tab;
        };
        $scope.submitFormFin = function (familia_id) {
            $http.post("index.php?lugar=doc&accion=Cambiar_estatus", familia_id)
                    .success(function (respuesta) {
                        if (respuesta.data != "false") {
                            $scope.formData.n = 0;
                            $scope.template = "index.php?lugar=home&accion=tabs&t=tab3";
                        } else {
                            $.jAlert({
                                'title': '¡Error en la Adjudicación!.',
                                'content': 'No se ha podido adjudicar la vivienda.',
                                'theme': 'red',
                                'size': 'md',
                                'showAnimation': 'bounceInDown',
                                'hideAnimation': 'bounceOutDown',
                                'btns': {'text': 'Aceptar', 'theme': 'red'}
                            });
                        }
                    }).error(function () {
                console.log('error');
            });
        };
//funcion para bloquear campos solo letras o solo numeros
        (function (a) {
            a.fn.validCampoMCTI = function (b) {
                a(this).on({keypress: function (a) {
                        var c = a.which, d = a.keyCode, e = String.fromCharCode(c).toLowerCase(), f = b;
                        (-1 != f.indexOf(e) || 9 == d || 37 != c && 37 == d || 39 == d && 39 != c || 8 == d || 46 == d && 46 != c) && 161 != c || a.preventDefault()
                    }})
            }
        })(jQuery);
//fin funcion para bloquear campos solo letras o solo numeros
    }]);
