<?php

/**
 * This is the model class for table "mesas_servicio".
 *
 * The followings are the available columns in table 'mesas_servicio':
 * @property string $id_mesaserv
 * @property integer $id_mesa
 * @property string $id_comite
 * @property string $cedulavc
 * @property string $nbvc
 * @property string $nb2vc
 * @property string $apvc
 * @property string $ap2vc
 * @property string $corvc
 * @property string $tlfvc
 */
class MesaServ extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MesaServ the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.mesas_servicio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_mesaserv, id_mesa, id_comite', 'required'),
			array('id_mesa', 'numerical', 'integerOnly'=>true),
			array('nbvc, nb2vc, apvc, ap2vc, corvc, tlfvc', 'length', 'max'=>50),
			array('cedulavc', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_mesaserv, id_mesa, id_comite, cedulavc, nbvc, nb2vc, apvc, ap2vc, corvc, tlfvc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_mesaserv' => 'Id Mesaserv',
			'id_mesa' => 'Id Mesa',
			'id_comite' => 'Id Comite',
			'cedulavc' => 'Cédula',
			'nbvc' => 'Nombre ',
			'nb2vc' => 'Segundo Nombre ',
			'apvc' => 'Apellido ',
			'ap2vc' => 'Segundo Apellido ',
			'corvc' => 'Correo ',
			'tlfvc' => 'Telefono ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_mesaserv',$this->id_mesaserv,true);
		$criteria->compare('id_mesa',$this->id_mesa);
		$criteria->compare('id_comite',$this->id_comite,true);
		$criteria->compare('cedulavc',$this->cedulavc,true);
		$criteria->compare('nbvc',$this->nbvc,true);
		$criteria->compare('nb2vc',$this->nb2vc,true);
		$criteria->compare('apvc',$this->apvc,true);
		$criteria->compare('ap2vc',$this->ap2vc,true);
		$criteria->compare('corvc',$this->corvc,true);
		$criteria->compare('tlfvc',$this->tlfvc,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}