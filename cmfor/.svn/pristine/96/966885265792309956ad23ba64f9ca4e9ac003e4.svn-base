<?php

/**
 * This is the model class for table "tasas".
 *
 * The followings are the available columns in table 'tasas':
 * @property integer $id
 * @property integer $ano
 * @property integer $mes
 * @property string $ipc
 * @property string $ta
 * @property string $tp
 */
class Tasas extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tasas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tasas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ipc, ta, tp', 'required'),
			array('ano, mes', 'numerical', 'integerOnly'=>true),
			array('ipc, ta, tp', 'length', 'max'=>10),
			array('ipc,ta,tp', 'match', 
					'pattern' => '/^[0-9]\d{0,2}(\.[0-9]\d{2,2})*(\,\d{1,2})?$/', 
					'message' => 'El valor númerico debe tener un formato 100.000,00'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ano, mes, ipc, ta, tp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ano' => 'Año',
			'mes' => 'Mes',
			'ipc' => 'INPC',
			'ta' => 'Tasa Activa %',
			'tp' => 'Tasa pasiva %',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		if($this->ano!=NULL)$this->ano=(int)$this->ano;
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ano',$this->ano);
		$criteria->compare('mes',$this->mes);
		$criteria->compare('ipc',$this->ipc,true);
		$criteria->compare('ta',$this->ta,true);
		$criteria->compare('tp',$this->tp,true);
		$criteria->order='ano DESC, mes DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	
	public function mesLiteral($id)
	{
	
		if($id==1)return 'ENERO';
		else if($id==2)return 'FEBRERO';
		else if($id==3)return 'MARZO';
		else if($id==4)return 'ABRIL';
		else if($id==5)return 'MAYO';
		else if($id==6)return 'JUNIO';
		else if($id==7)return 'JULIO';
		else if($id==8)return 'AGOSTO';
		else if($id==9)return 'SEPTIEMBRE';
		else if($id==10)return 'OCTUBRE';
		else if($id==11)return 'NOVIEMBRE';
		else if($id==12)return 'DICIEMBRE';
		else return 'ERROR';
		
	
	
	}
	
		protected function beforeSave() {
			
			$this->ipc=str_replace(".", "", $this->ipc);
			$this->ipc=str_replace(",", ".", $this->ipc);
			
			$this->ta=str_replace(".", "", $this->ta);
			$this->ta=str_replace(",", ".", $this->ta);			
	
			$this->tp=str_replace(".", "", $this->tp);
			$this->tp=str_replace(",", ".", $this->tp);
			
			$this->ta=$this->ta/100;
			$this->tp=$this->tp/100;
		
			return parent::beforeSave();
		} 
}