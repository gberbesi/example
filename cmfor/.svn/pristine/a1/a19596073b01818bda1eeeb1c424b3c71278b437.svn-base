<?php
 
/**
 * This is the model class for table "encuesta.enc_promotor".
 *
 * The followings are the available columns in table 'encuesta.enc_promotor':
 * @property string $enc_promotor_id
 * @property string $nacionalidad
 * @property integer $cedula
 * @property string $codigo_cdlp
 * @property string $nombre1
 * @property string $nombre2
 * @property string $apellido1
 * @property string $apellido2
 * @property string $fecha_nac
 * @property integer $telefono1
 * @property integer $telefono2
 * @property string $correo
 * @property integer $estado
 * @property integer $municipio
 * @property integer $parroquia
 * @property integer $sector
 * @property string $direccion
 * @property integer $torre1
 * @property integer $torre2
 * @property integer $torre3
 * @property integer $torre4
 * @property integer $torre5
 * @property integer $torre6
 * @property integer $id_urbanismo
 * @property string $etapa
 */
class EncPromotor extends CActiveRecord
{
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EncPromotor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.enc_promotor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nacionalidad, cedula, codigo_cdlp, nombre1, apellido1, fecha_nac, telefono1, correo', 'required'),
			array('cedula, telefono1, telefono2, estado, municipio, parroquia, sector, torre1, torre2, torre3, torre4, torre5, torre6, id_urbanismo', 'numerical', 'integerOnly'=>true),

				// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('enc_promotor_id, nacionalidad, cedula, codigo_cdlp, nombre1, nombre2, apellido1, apellido2, fecha_nac, telefono1, telefono2, correo, estado, municipio, parroquia, sector, direccion, torre1, torre2, torre3, torre4, torre5, torre6, id_urbanismo, etapa,nom_comite', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		
        return array(
            'urbanismos' => array(self::BELONGS_TO, 'EncUrbanismo', 'id_urbanismo'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'enc_promotor_id' => 'Enc Promotor',
			'nacionalidad' => 'Nacionalidad',
			'cedula' => 'Cedula',
			'codigo_cdlp' => 'Codigo Cdlp',
			'nombre1' => 'Nombre1',
			'nombre2' => 'Nombre2',
			'apellido1' => 'Apellido1',
			'apellido2' => 'Apellido2',
			'fecha_nac' => 'Fecha Nac',
			'telefono1' => 'Telefono1',
			'telefono2' => 'Telefono2',
			'correo' => 'Correo',
			'estado' => 'Estado',
			'municipio' => 'Municipio',
			'parroquia' => 'Parroquia',
			'sector' => 'Sector',
			'direccion' => 'Direccion',
			'torre1' => 'Torre1',
			'torre2' => 'Torre2',
			'torre3' => 'Torre3',
			'torre4' => 'Torre4',
			'torre5' => 'Torre5',
			'torre6' => 'Torre6',
			'id_urbanismo' => 'Nombre del urbanismo',
			'etapa' => 'Etapa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('enc_promotor_id',$this->enc_promotor_id,true);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('cedula',$this->cedula);
		$criteria->compare('codigo_cdlp',$this->codigo_cdlp,true);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('fecha_nac',$this->fecha_nac,true);
		$criteria->compare('telefono1',$this->telefono1);
		$criteria->compare('telefono2',$this->telefono2);
		$criteria->compare('correo',$this->correo,true);
		$criteria->compare('estado',$this->estado);
		$criteria->compare('municipio',$this->municipio);
		$criteria->compare('parroquia',$this->parroquia);
		$criteria->compare('sector',$this->sector);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('torre1',$this->torre1);
		$criteria->compare('torre2',$this->torre2);
		$criteria->compare('torre3',$this->torre3);
		$criteria->compare('torre4',$this->torre4);
		$criteria->compare('torre5',$this->torre5);
		$criteria->compare('torre6',$this->torre6);
		$criteria->compare('id_urbanismo',$this->id_urbanismo);
		$criteria->compare('etapa',$this->etapa,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}