<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_comite')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_comite), array('view', 'id'=>$data->id_comite)); ?>
	

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('monto_inicial')); ?>:</b>
	<?php echo CHtml::encode($data->monto_inicial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ajuste_ipc')); ?>:</b>
	<?php echo CHtml::encode($data->ajuste_ipc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ajuste_ta')); ?>:</b>
	<?php echo CHtml::encode($data->ajuste_ta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ajuste_tp')); ?>:</b>
	<?php echo CHtml::encode($data->ajuste_tp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor_actualizado')); ?>:</b>
	<?php echo CHtml::encode($data->valor_actualizado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('registro')); ?>:</b>
	<?php echo CHtml::encode($data->registro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nu_registro')); ?>:</b>
	<?php echo CHtml::encode($data->nu_registro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nu_tomo')); ?>:</b>
	<?php echo CHtml::encode($data->nu_tomo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('protocolo')); ?>:</b>
	<?php echo CHtml::encode($data->protocolo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fe_registro')); ?>:</b>
	<?php echo CHtml::encode($data->fe_registro); ?>
	<br />

	*/ ?>

</div>