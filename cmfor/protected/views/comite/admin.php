<?php
$this->breadcrumbs=array(
	'Terrenos'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List Terreno', 'url'=>array('index')),
	array('label'=>'Calcular Justo Valor del Terreno', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('terreno-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

function cambiar ($monto) {
    $monto_final=number_format($monto,2,",", ".");
    return $monto_final;
}
?>

<h1>Lista de Comites Multifamiliares</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'terreno-grid',
	'cssFile'=> Yii::app()->request->baseUrl.'/css/cgridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_comite',
		
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
