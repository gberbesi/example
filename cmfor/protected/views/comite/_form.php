<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!--<?php var_dump($datopiso) ;?>-->
<style>
	.row_campos_detalles div,.row_campos_detalles input,.row_campos_detalles select{padding:0px;margin:0px;border-radius:0px;height:20px;}.boton_menos,.boton_mas{height:20px;padding:2px 3px 2px 3px;}#th_button_nuevo{text-align:center;width:80px;}.td_botones button{float:left;}.div_botones_listar{}.div_botones_listar button{margin-left:3px;}.ui-datepicker-month, .ui-datepicker-year{color:#333;font-weight: 900;}.row{margin-top: 20px;margin-bottom: 20px;}
	label.btn span {font-size: 1em}
	label input[type="radio"] ~ i.fa.fa-circle-o{color: #c8c8c8; display: inline}
	label input[type="radio"] ~ i.fa.fa-dot-circle-o{display: none}
	label input[type="radio"]:checked ~ i.fa.fa-circle-o{display: none}
	label input[type="radio"]:checked ~ i.fa.fa-dot-circle-o{color: #7AA3CC;display: inline;}label:hover input[type="radio"] ~ i.fa {color: #7AA3CC}
	label input[type="checkbox"] ~ i.fa.fa-square-o{color: #c8c8c8;display: inline}
	label input[type="checkbox"] ~ i.fa.fa-check-square-o{display: none}
	label input[type="checkbox"]:checked ~ i.fa.fa-square-o{display: none}
	label input[type="checkbox"]:checked ~ i.fa.fa-check-square-o{color: #7AA3CC;display: inline}
	label:hover input[type="checkbox"] ~ i.fa {color: #7AA3CC}
	.has-error div[data-toggle="buttons"] label.active{color: #7AA3CC}
	div[data-toggle="buttons"] label.active{color: #7AA3CC}
	div[data-toggle="buttons"] label:hover {color: #7AA3CC}
	
	.has-error div[data-toggle="buttons"] label {
		color: #a94442;
	}
	.fa-2x{
		font-size: 1em;
	}
	div[data-toggle="buttons"] label { 
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 20px;
	font-weight: normal;
	line-height: 2em;
	text-align: left;
	white-space: nowrap;
	vertical-align: top;
	cursor: pointer;
	background-color: none;
	border: 0px solid #c8c8c8;
	border-radius: 3px;
	color: #c8c8c8;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	-o-user-select: none;
	user-select: none;
	}
	div[data-toggle="buttons"] label:active, div[data-toggle="buttons"] label.active {
	-webkit-box-shadow: none;
	box-shadow: none;
	}
	span.required {
		color:red;
	}
	.help-block {
		font-size: 20px;
	}
	p.stop:hover{
		color:black;
	}
</style>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'comite-form',
	'enableAjaxValidation'=>false,
)); ?>
<br>


<div class="container-fluid">
<fieldset>
	<legend ><span style="color:red">*</span>Datos del Urbanismo</legend>
	<div class=" col-md-4">
			
		<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'urbau',
				'value'=>$datourbanismo->urbanismo_nombre,
				),true);
		?>
	
			
		</div>


<div class=" col-md-4">
			
		<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'etapa',
				'value'=>$datosvoc->etapa,
				),true);
		?>
	
			
		</div>
		<div class=" col-md-4">
			
		<?php echo $this->renderPartial('_fieldh',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'torreu',
				'value'=>$datosvoc->torre,
				),true);
		?>
	
			
		</div>
		<div class=" col-md-4">
			
		<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'torreu',
				'value'=>$datotorre->nom_torre,
				),true);
		?>
	
			
		</div>

</fieldset>
<fieldset>
		<legend><span style="color:red">*</span>Datos De la Infraestructura de Torre</legend>

		<div class="col-md-4">
			
	<?php $elev = CHtml::listData(Respuesta2::model()->findAll(), 'id_respuesta', 'nombre_mesa')?>
	<?php echo $this->renderPartial('_dropDownrq',array(
		'model'=>$model,
		'form'=>$form,											
		'atributo'=>'st_elevador',
		'list'=>$elev,),true);?>			
		</div>

		<div class="col-md-4">
			
	<?php $luz = CHtml::listData(Respuesta2::model()->findAll(), 'id_respuesta', 'nombre_mesa')?>
	<?php echo $this->renderPartial('_dropDownrq',array(
		'model'=>$model,
		'form'=>$form,											
		'atributo'=>'st_ilumi',
		'list'=>$luz,),true);?>			
		</div>
		<div class="col-md-4">
			
	<?php $arcom = CHtml::listData(Respuesta2::model()->findAll(), 'id_respuesta', 'nombre_mesa')?>
	<?php echo $this->renderPartial('_dropDownrq',array(
		'model'=>$model,
		'form'=>$form,											
		'atributo'=>'st_arecom',
		'list'=>$arcom,),true);?>			
		</div>
		
	</fieldset>
<fieldset>
	<legend ><span style="color:red">*</span>Datos de Vocero</legend>
	
		


<div class="col-md-4 " style="margin-right: 100%">
		<div class="form-group ">
			<?php echo $form->labelEx($model,"id_vocero");?>
			<div class="input-group">
				<div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="button_nacionalidad"><?php echo ($datosvoc->nacionalidad?$datosvoc->nacionalidad:'E'); ?></span><span class="caret"></span></button>
					<ul class="dropdown-menu" role="menu">
						<li><a >E</a></li>
						<li><a >V</a></li>
					</ul>
				</div>
				
					<?php echo $form->textField($model,"id_vocero",array('class'=>'form-control','id'=>'ci','required'=>true,'value'=>$datosvoc->cedula,'readonly'=>true));?>
				
			</div>
			
		</div>

		
	</div>
	
	<div class=" col-md-3">
			
		<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'nb_vocero',
				'value'=>$datosvoc->nombre1,
				'id'=>'nombre',
				),true);
		?>
	
			
		</div>
			<div >
			
		<?php echo $this->renderPartial('_fieldh',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'nb2vcr',
				'id'=>'nb2vcr',
				),true);
		?>
	
			
		</div>
		<div class="col-md-3">
			
		<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'ape_vocero',
				'value'=>$datosvoc->apellido1,
				'id'=>'ape_vocero',

				),true);
		?>
	
			
		</div>
	
		<div >
			
		<?php echo $this->renderPartial('_fieldh',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'ap2vcr',
				'id'=>'ap2vcr',
				),true);
		?>
	
			
		</div>
		<div class="col-md-3">
			
		<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'tlf_vocero',
				'value'=>$datosvoc->telefono1,
				),true);
		?>
	
			
		</div>
		<div class="col-md-3">
			
		<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'cor_vocero',
				'value'=>$datosvoc->correo,
				),true);
		?>			
		</div>
	</fieldset><br>
	<fieldset>
		<legend><span style="color:red">*</span>Registro De Apartamentos</legend>	
			<div class="col-md-4">
			
	<?php 
$datopiso=CHtml::listData(EncPiso::model()->findAll('torre_id=:piso',array(':piso'=>$datotorre->enc_torre_id)),'enc_piso_id','nu_piso'); ?>
	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$urba,
		'form'=>$form,											
		'atributo'=>'piso_id',
		'change'=>'buscarapto()',
		'list'=>$datopiso,),true);?>			
</div>
		<div class="col-md-4">
	<label>Apartamento</label>
	<select  class="form-control" style="width:100%" id="apto" name="EncApartamento[piso_id]" >	
		<option value='0'>SELECCIONE</option>
	</select>
	</div>
	<div >
		
			
		<?php echo $this->renderPartial('_fieldh',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'idapto',
				'id'=>'idapto',
				),true);
		?>			

	</div>
	


	</fieldset>
	
	

<div id="familia" style="display:none">
	<fieldset>
		<legend><span style="color:red">*</span>Datos De familia</legend>
		<div class="col-md-2" >
			
				<?php $sino = array('T'=>'Si','F'=>'No')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'protocolizado',
				'id'=>'protocolizado',
				'list'=>$sino,),true);?>			
		</div>
		<div class="col-md-2" style="margin-right: 60%">
			
				<?php $parente = CHtml::listData(FamParentesco::model()->findAll(), 'nombre', 'nombre')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$persona,
				'form'=>$form,											
				'atributo'=>'parentesco_id',
				'id'=>'parentesco',
				'list'=>$parente,),true);?>			
		</div>
		<div >
			
				
				<?php echo $this->renderPartial('_fieldh',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'idapto',
				'id'=>'idaptfam',
				),true);?>			
		</div>
	<div class="col-md-3">
			
		<?php echo $this->renderPartial('_field',array(
				'model'=>$persona,
				'form'=>$form,
				'atributo'=>'codigo_carnetdlp',
				'id'=>'cdlp'
				),true);
		?>
	
			
		</div>
		
	<div class="form-group col-md-5">
			<?php echo $form->labelEx($persona,"cedula");?>
			<div class="input-group">
				<div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="button_nacionalidad2"><?php echo ($persona->nacionalidad?$persona->nacionalidad:'V'); ?></span><span class="caret"></span></button>
					<ul class="dropdown-menu" role="menu">
						<li><a onclick="$('#PerPersona_nacionalidad').val('V');$('#button_nacionalidad2').text('V');">V</a></li>
						<li><a onclick="$('#PerPersona_nacionalidad').val('E');$('#button_nacionalidad2').text('E');">E</a></li>
					</ul>
				</div>
					<?php echo $form->textField($persona,"cedula",array('class'=>'form-control','id'=>'cedulafam'));?>
				<span class="input-group-btn">
					<a id="buscarpersona"><span class="btn btn-primary" >Buscar</span></a>
				</span>
			</div>
			
		</div>
		<div class="col-md-3">
			
				<?php $sexo = array('M'=>'Masculino','F'=>'Femenino')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$persona,
				'form'=>$form,											
				'atributo'=>'sexo',
				'id'=>'sexo',
				'list'=>$sexo,),true);?>			
		</div>
				<div class="col-md-3">
			
		<?php echo $this->renderPartial('_field',array(
				'model'=>$persona,
				'form'=>$form,
				'atributo'=>'nombre1',
				'id'=>'nombrecarga'
				),true);
		?>
	
			
		</div>
		<div class="col-md-3">
			
		<?php echo $this->renderPartial('_field',array(
				'model'=>$persona,
				'form'=>$form,
				'atributo'=>'apellido1',
				'id'=>'apellidocarga'
				),true);
		?>
	
			
		</div>
		<div class="col-md-3">
			
		<?php echo $this->renderPartial('_field',array(
				'model'=>$persona,
				'form'=>$form,
				'atributo'=>'fe_nac',
				'id'=>'fechacarga'
				),true);
		?>
	
			
		</div>
		<div class="col-md-3">
			
		<?php echo $this->renderPartial('_field',array(
				'model'=>$persona,
				'form'=>$form,
				'atributo'=>'e_mail',
				'id'=>'emailcarga'
				),true);
		?>
	
			
		</div>
		<div class="col-md-3">
			
		<?php echo $this->renderPartial('_field',array(
				'model'=>$persona,
				'form'=>$form,
				'atributo'=>'per_telefono',
				'id'=>'telefonocarga'
				),true);
		?>
	
			
		</div>
		<div class="col-md-3">
			
				<?php $sectores = CHtml::listData(FamSectorSocial::model()->findAll(), 'id_fam_sector_social', 'descripcion')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$persona,
				'form'=>$form,											
				'atributo'=>'sector',
				'id'=>'sector',
				'list'=>$sectores,),true);?>			
		</div>
		<div class="col-md-3">
			
				<?php $edocv = CHtml::listData(FamEdoCivil::model()->findAll(), 'edo_civil_id', 'nombre')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$persona,
				'form'=>$form,											
				'atributo'=>'edocivil',
				'id'=>'edocivil',
				'list'=>$edocv,),true);?>			
		</div>
		<div class="col-md-3">
			
				<?php $paises = CHtml::listData(Pais::model()->findAll(), 'id_pais', 'despai')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$persona,
				'form'=>$form,											
				'atributo'=>'id_pais',
				'id'=>'pais',
				'list'=>$paises,),true);?>			
		</div>
			<div class="col-md-3">
			
				<?php $anos = array('1'=>'menor a 1950','2'=>'mayor a 2000','3'=>' mayor a 2010')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$persona,
				'form'=>$form,											
				'atributo'=>'ano_ingreso',
				'id'=>'aingreso',
				'list'=>$anos,),true);?>			
		</div>
		
		
		
		</fieldset>
		<fieldset>
			<legend><span style="color:red">*</span>Datos Socio-económicos</legend>
			<div class="col-md-3">
			
				<?php $si = array('t'=>'Si','f'=>'No')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'estudia',
				'change'=>'estudiau()',
				'id'=>'estudia',
				'list'=>$si,),true);?>			
		</div>

			<div class="col-md-3">
			
				<?php $gradoins = CHtml::listData(FamGradoInstruccion::model()->findAll(), 'grado_instruccion_id', 'nombre')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'gradoins',
				'id'=>'gradoins',
				'list'=>$gradoins,),true);?>			
		</div>
		<div class="col-md-3">
			
				<?php $ocupacion = CHtml::listData(FamOcupacion::model()->findAll(), 'ocupacion_id', 'nombre')?>
				
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'ocupacion',
				'id'=>'ocupacion',
				'list'=>$ocupacion,),true);?>			
		</div>
		<div class="col-md-3">
			
				<?php $si = array('t'=>'Si','f'=>'No')?>
				<?php echo $this->renderPartial('_nfield',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'ingreso',
				'id'=>'ingreso',
				'list'=>$si,),true);?>			
		</div>
		<div id="estudiaubica"  class="col-lg-12" style="display:none">
			<fieldset>			
				<legend>Dirección de Estudio</legend>
				
				<?php echo $this->renderPartial('_estadoMunicipioParroquia',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributoEstado'=>'geo_estado_estudia',
				'idest'=>'estudia_estado',
				'atributoMunicipio'=>'geo_municipio_estudia',
				'idmun'=>'estudia_municipio',
				'atributoParroquia'=>'geo_parroquia_estudia',
				'idpar'=>'estudia_parroquia',
				'size'=>4,
				),true);?>	
				</fieldset>		
		</div>
		
		<div class="col-md-3">
			
				<?php $si = array('t'=>'Si','f'=>'No')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'trabaja',
				'change'=>'trabajau()',
				'id'=>'trabajo',
				'list'=>$si,),true);?>			
		</div>
				<div class="col-md-3">
			
				<?php $condemp = CHtml::listData(FamCondEmpleado::model()->findAll(), 'trabajo_id', 'nb_trabajo')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'condicionemp',
				'id'=>'condicionemp',
				'list'=>$condemp,),true);?>			
		</div>
		<div class="col-md-3">
			
				<?php $tiempp = CHtml::listData(FamTiempoAdmpublica::model()->findAll(), 'id_fam_tiempo_admpublica', 'nombre')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'tiempoadmin',
				'id'=>'tiempoadmin',
				'list'=>$tiempp,),true);?>			
		</div>
		<div class="col-md-3">
			
				<?php $ingretipo = CHtml::listData(ClasificacionIngreso::model()->findAll(), 'id_clasificacion', 'nb_clasificacion')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'ingretipo',
				'id'=>'ingretipo',
				'list'=>$ingretipo,),true);?>			
		</div>
		<div id="trabajoubica" class="col-lg-12" style="display:none"  >
			<fieldset>			
				<legend>Dirección de Trabajo</legend>
				
				<?php echo $this->renderPartial('_estadoMunicipioParroquia',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributoEstado'=>'geo_estado_trabaja',
				'idest'=>'estado_trabaja',
				'idmun'=>'municipio_trabaja',				
				'idpar'=>'parroquia_trabaja',
				'atributoMunicipio'=>'geo_municipio_trabaja',
				'atributoParroquia'=>'geo_parroquia_trabaja',
				'size'=>4,
				),true);?>	
				</fieldset>
		
		</div>
		<div class="col-md-3">
			
				<?php $si = array('t'=>'Si','f'=>'No')?>
				<?php echo $this->renderPartial('_nfield',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'honorarios',
				'id'=>'honorarios',
				'list'=>$si,),true);?>			
		</div>
		<div class="col-md-3">
			
				<?php $si = array('t'=>'Si','f'=>'No')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'discapacidad',
				'change'=>'discanum()',
				'id'=>'discapacidad',
				'list'=>$si,),true);?>			
		</div>
		<div class="col-md-3" id="conapdis2" style="display:none">
			
				
				<?php echo $this->renderPartial('_nfield',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'conapdis',
				'id'=>'conapdis',
				),true);?>			
		</div>
			<div class="col-md-3">
			
				<?php $enfermedad = CHtml::listData(FamEnfermedad::model()->findAll(), 'enfermedad_id', 'nombre')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'enfermedad',
				'id'=>'enfermedad',
				'list'=>$enfermedad,),true);?>			
		</div>
		
		<div class="col-md-3">
			
				<?php $si = array('t'=>'Si','f'=>'No')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'faov',
				'id'=>'faov',
				'list'=>$si,),true);?>			
		</div>
		<div class="col-md-3">
			
				<?php $si = array('t'=>'Si','f'=>'No')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'pertenecemision',
				'id'=>'pertenecemision',
				'change'=>'misionp()',
				'list'=>$si,),true);?>			
		</div>
		<div class="col-md-3" id="misionpe" style="display:none" >
			
				<?php $mision = CHtml::listData(FamMision::model()->findAll(), 'mision_id', 'nombre')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'id_pertemision',
				'id'=>'id_pertemis',
				'list'=>$mision,),true);?>			
		</div>
		<div class="col-md-3">
			
				<?php $si = array('t'=>'Si','f'=>'No')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'beneficiomision',
				'change'=>'misionb()',
				'id'=>'beneficiomision',
				'list'=>$si,),true);?>			
		</div>
		<div class="col-md-3" id="misionbe" style="display:none">
			
				<?php $mision = CHtml::listData(FamMision::model()->findAll(), 'mision_id', 'nombre')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'id_benefmision',
				'id'=>'id_benefmision',
				'list'=>$mision,),true);?>			
		</div>
		<div class="col-md-3">
			
				<?php $si = array('t'=>'Si','f'=>'No')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'deporte',
				'change'=>'deportep()',
				'id'=>'deporte',
				'list'=>$si,),true);?>			
		</div>
		<div class="col-md-3" id="deportep" style="display:none">
			
				<?php $deporte = CHtml::listData(FamDeporte::model()->findAll(), 'deporte_id', 'nombre')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$economico,
				'form'=>$form,											
				'atributo'=>'id_deporte',
				'id'=>'id_deporte',
				'list'=>$deporte,),true);?>			
		</div>
		<div >
		<a onclick="integrante()"><span class="btn btn-primary" >Añadir Integrante</span></a>
		<div>
	
		
		
		<table class="table table-striped table-condensed table-bordered" id="familiar">
			<tr style="font-weight: bold; color:black">
				<td style="color:black">ID</td>
				<td style="color:black">Cédula</td>
				<td style="color:black">Nombre</td>
				<td style="color:black">Apellido</td>
				<td style="color:black">Parentesco</td>
				<td style="color:black">Acción</td>
			</tr>
			
		</table>

		

		
		
		
		</fieldset>
	
		

</div>
<div class="col-md-2 col-md-offset-5"   >
<button type="submit" class="btn btn-primary btn-lg btn-block" >Registrar</button>
</div>
<div>
<div class="col-md-4" id="deportep" style="display:none">
			
				
				<?php echo $this->renderPartial('_field',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'arid',
				'id'=>'arid',
				),true);?>			
		</div>	
</div>
<div class="col-md-4" id="deportep" style="display:none">
			
				
				<?php echo $this->renderPartial('_field',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'arci',
				'id'=>'arci',
				),true);?>			
		</div>	
</div>
<div class="col-md-4" id="deportep" style="display:none">
			
				
				<?php echo $this->renderPartial('_field',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'arnb',
				'id'=>'arnb',
				),true);?>			
		</div>	
</div>
<div class="col-md-4" id="deportep" style="display:none">
			
				
				<?php echo $this->renderPartial('_field',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'arapl',
				'id'=>'arapl',
				),true);?>			
		</div>	
</div>
<div class="col-md-4" id="deportep" style="display:none">
			
				
				<?php echo $this->renderPartial('_field',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'arprnt',
				'id'=>'arprnt',
				),true);?>			
		</div>	
</div>
<div class="col-md-4" id="deportep" style="display:none">
			
				
				<?php echo $this->renderPartial('_field',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'arocultos',
				'id'=>'arocultos',
				),true);?>			
		</div>	
</div>

	<br><br><br>
	<br><br><br>
	<br><br><br>
	

	</div>





			<?php $this->endWidget(); ?>


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		
		$("#buscarpersona").click(function() {
			var ci = $("#cedulafam").val();
			var nac= $("#button_nacionalidad2").text();
	
			$.ajax({
				url:"<?php echo $this->createUrl('Cedula'); ?>",
				cache: false,
				type: "POST",
				dataType: "json",
				data: ({cedula:ci,nacionalidad:nac}),
			
				success: function(data){

					console.log(data);
					if(data==2){
					$('#nombrecarga').val('');
					$('#apellidocarga').val('');
					$('#fechacarga').val('');
					$("#cedulafam").val('');
					swal("Atención","Esta cedula no se encuentra registrada","warning");
					}else{
					if(data.nacionalidad!='') {
					$('#button_nacionalidad2').text(data.nac);
					}

					$('#nombrecarga').val(data.nombre1);
					$('#apellidocarga').val(data.apellido1);
					$('#fechacarga').val(data.fecha_nac);
					$("#cedulafam").val(data.cedula);

					$('#nombrecarga').attr('readonly','readonly');
					$('#apellidocarga').attr('readonly','readonly');
					$('#fechacarga').attr('readonly','readonly');
					
						}


				}
			});
		
		});

	});

</script>
<script>
	function añadir($tmn) {


		var mesa = $('#Comite_mesas_id').val();	
		$.ajax({
				url:"<?php echo $this->createUrl('Serv'); ?>",
				cache: false,
				type: "POST",
				dataType: "json",
				data: ({mesa:mesa}),
			
				success: function(data){
					
					
					var nombre = (data.nombre);
					$('#sevcio'+mesa).text(nombre);
				}
			});

		for(var i=1 ; i<=$tmn ; i++){

			if(i==mesa){
				$('#'+i+'').show('fast');
				
			}else{
				$('#'+i+'').hide();
			}

		}
		
			

		};


	   function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }
 

    function soloNumeros(e){

	var key = window.Event ? e.which : e.keyCode

	return (key >= 48 && key <= 57)

}
	
$("#comite-form").submit(function( event ) {
 var tmn= $('#familiar tr').length;
 var arraymesas = "";
 var arrayci = "";
 var arraynb = "";
 var arrayap = "";
 var arraycr = "";
 var arraytlf = "";
var jefe=0;
 for (i=1;i<=tmn;i++){
var prtp= $('#familiar tr').eq(''+i+'').find('td').eq(4).text();
if(prtp=='JEFE(A) FAMILIA'){
	jefe++;
}
 }
 if(jefe==0){
 		swal("Atención","Debe cargar un jefe de familia","warning");
 		event.preventDefault();
 		return false;
 }

 for (i=1;i<=tmn;i++){

 	var id= $('#familiar tr').eq(''+i+'').find('td').eq(0).text();
 	var ci= $('#familiar tr').eq(''+i+'').find('td').eq(1).text();
 	var nb= $('#familiar tr').eq(''+i+'').find('td').eq(2).text();
 	var ap= $('#familiar tr').eq(''+i+'').find('td').eq(3).text();
 	var prt= $('#familiar tr').eq(''+i+'').find('td').eq(4).text();
 	var ocultos= $('#familiar tr').eq(''+i+'').find('td').eq(6).text();
 	if(i==1){

		arrayid=id;
		arrayci=ci;
		arraynb=nb;
		arrayap=ap;
		arrayprt=prt;
		arrayocultos=ocultos;
		

		}else{

		arrayid=arrayid+","+id;
		arrayci=arrayci+","+ci;
		arraynb=arraynb+","+nb;
		arrayap=arrayap+","+ap;
		arrayprt=arrayprt+","+prt;
		arrayocultos=arrayocultos+","+ocultos;

}
 	

 	

 }
 $('#arid').val(arrayid);
 $('#arci').val(arrayci);
 $('#arnb').val(arraynb);
 $('#arapl').val(arrayap);
 $('#arprnt').val(arrayprt);
 $('#arocultos').val(arrayocultos);



 
});
function estudiau(){
	var est = $("#estudia").val();
	if (est=='t'){
		$("#estudiaubica").show('fast');
	}else{
		$("#estudiaubica").hide();
	}
}
function trabajau(){
	var trab = $("#trabajo").val();
	if (trab=='t'){
		$("#trabajoubica").show('fast');
	}else{
		$("#trabajoubica").hide();
	}
}
function discanum(){
	var disca = $("#discapacidad").val();
	if (disca=='t'){
		$("#conapdis2").show('fast');
	}else{
		$("#conapdis2").hide();
	}

}
function misionp(){
	var mision = $("#pertenecemision").val();
	if (mision=='t'){
		$("#misionpe").show('fast');
	}else{
		$("#misionpe").hide();
	}
}
function misionb(){
	var benef = $("#beneficiomision").val();
	if (benef=='t'){
		$("#misionbe").show('fast');
	}else{
		$("#misionbe").hide();
	}
}
function deportep(){
	var benef = $("#deporte").val();
	if (benef=='t'){
		$("#deportep").show('fast');
	}else{
		$("#deportep").hide();
	}
}

var idtabular=1;
function integrante(){
	var cedula= $("#cedulafam").val();
	var nac= $("#button_nacionalidad2").text();
	var parentesco = $('#parentesco').val();
	var cdlp = $('#cdlp').val();
	var sexo = $('#sexo').val();
	var nombrecarga = $('#nombrecarga').val();
	var apellidocarga = $('#apellidocarga').val();
	var fechacarga = $('#fechacarga').val();
	var emailcarga = $('#emailcarga').val();
	var telefonocarga = $('#telefonocarga').val();
	var sector = $('#sector').val();
	var edocivil = $('#edocivil').val();
	var pais = $('#pais').val();
	var ingreso = $('#ingreso').val();
	var estudia = $('#estudia').val();
		var estudia_estado = $('#estudia_estado').val();
		var estudia_municipio = $('#estudia_municipio').val();
		var estudia_parroquia = $('#estudia_parroquia').val();
	var gradoins = $('#gradoins').val();
	var ocupacion = $('#ocupacion').val();
	var aingreso = $('#aingreso').val();
	var trabajo = $('#trabajo').val();
		var estado_trabaja = $('#estado_trabaja').val();
		var municipio_trabaja = $('#municipio_trabaja').val();
		var parroquia_trabaja = $('#parroquia_trabaja').val();
	var condicionemp = $('#condicionemp').val();
	var tiempoadmin = $('#tiempoadmin').val();
	var ingretipo = $('#ingretipo').val();
	var discapacidad = $('#discapacidad').val();
	var conapdis = $('#conapdis').val();
	var enfermedad = $('#enfermedad').val();
	var faov = $('#faov').val();
	var pertenecemision = $('#pertenecemision').val();
		var id_pertemis = $('#id_pertemis').val();
	var beneficiomision = $('#beneficiomision').val();
		var id_benefmision = $('#id_benefmision').val();
	var deporte = $('#deporte').val();
		var id_deporte = $('#id_deporte').val();
	
	 var tmn= $('#familiar tr').length;

var jefe=0;
var conyuge=0;
 for (i=1;i<=tmn;i++){
var prtp= $('#familiar tr').eq(''+i+'').find('td').eq(4).text();
if(prtp=='JEFE(A) FAMILIA'){
	jefe++;
}
if(prtp=='CONYUGE'){
	conyuge++;
}
 }
 if(jefe==1 && parentesco=='JEFE(A) FAMILIA' ){
 		swal("Atención","ya existe un jefe de familia","warning");

 		return false;
 }
 if(conyuge==1 && parentesco=='CONYUGE' ){
 		swal("Atención","Ya Posee un Conyuge","warning");

 		return false;
 }


 for (i=1;i<=tmn;i++){

 	
 	var ci= $('#familiar tr').eq(''+i+'').find('td').eq(1).text();
 	if(ci == cedula){
 		swal("Atención","Esta Cédula Ya Se encuentra registrada","warning");
 		
 		cedula= $("#cedulafam").val('');
		nombrecarga = $('#nombrecarga').val('');
		apellidocarga = $('#apellidocarga').val('');

		return false;
 	}

 }
 	


	if(parentesco==''){
		swal("Atención","Debe Seleccionar un Parentesco","warning");
		return false;
	} 
	if(parentesco==1 || parentesco==3 || parentesco==4 || parentesco==7 || parentesco==9 || parentesco==12 || parentesco==22 || parentesco==23 ||  parentesco==24 ){

		if(cdlp==''||cedula==''||sexo==''||nombrecarga==''||apellidocarga==''||fechacarga==''||emailcarga==''||telefonocarga==''||sector==''||edocivil==''||pais==''||estudia==''||gradoins==''||ocupacion==''||ingreso==''||trabajo==''||condicionemp==''||tiempoadmin==''||ingretipo==''||discapacidad==''||enfermedad==''||faov==''||pertenecemision==''||beneficiomision==''||deporte==''){

		if(cdlp==''){
			swal('Atencion','El campo Codigo Carnet no puede estar vacio ',"warning");
			return false;
		}
		if(cedula==''){
			swal('Atencion','El campo Cedula no puede estar vacio ',"warning");
			return false;
		}
		if(sexo==''){
			swal('Atencion','El campo Género no puede estar vacio ',"warning");
			return false;
		}
		if(nombrecarga==''){
			swal('Atencion','El campo Nombre no puede estar vacio ',"warning");
			return false;
		}
		if(apellidocarga==''){
			swal('Atencion','El campo  Apellido no puede estar vacio ',"warning");
			return false;
		}
		if(fechacarga==''){
			swal('Atencion','El campo Fecha de Nacimiento no puede estar vacio ',"warning");
			return false;
		}        
		if(emailcarga==''){
			swal('Atencion','El campo Correo no puede estar vacio ',"warning");
			return false;
		}
		if(telefonocarga==''){
			swal('Atencion','El campo Teléfono no puede estar vacio ',"warning");
			return false;
		}
		if(sector==''){
			swal('Atencion','El campo Sector no puede estar vacio ',"warning");
			return false;
		}
		if(edocivil==''){
			swal('Atencion','El campo Estado Civil no puede estar vacio ',"warning");
			return false;
		}
		if(pais==''){
			swal('Atencion','El campo Pais Origen no puede estar vacio ',"warning");
			return false;
		}    
		if(estudia==''){
			swal('Atencion','Debe Indicar si Estudia ',"warning");
			return false;
		}else if(estudia=='t'){
			if(estudia_estado==''){
				swal('Atencion','Debe Indicar el Estado donde Estudia',"warning");
				return false;
			}
			if(estudia_municipio==''){
				swal('Atencion','Debe Indicar el Municipio donde Estudia',"warning");
				return false;
			}
			if(estudia_parroquia==''){
				swal('Atencion','Debe Indicar la Parroquia donde Estudia',"warning");
				return false;
			}
		}
		if(gradoins==''){
			swal('Atencion','El campo Grado de Instrucción no puede estar vacio ',"warning");
			return false;
		}
		if(ocupacion==''){
			swal('Atencion','El campo Ocupación no puede estar vacio ',"warning");
			return false;
		}
		if(ingreso==''){
			swal('Atencion','El campo Ingreso no puede estar vacio ',"warning");
			return false;
		}
		if(trabajo==''){
			swal('Atencion','Debe Indicar si Trabaja ',"warning");
			return false;
		}else if (trabajo=='t'){
			if(estado_trabaja==''){
				swal('Atencion','Debe Indicar el Estado donde Trabaja',"warning");
				return false;
			}
			if(municipio_trabaja==''){
				swal('Atencion','Debe Indicar el Municipio donde Trabaja',"warning");
				return false;
			}
			if(parroquia_trabaja==''){
				swal('Atencion','Debe Indicar la Parroquia donde Trabaja',"warning");
				return false;
			}

		}
		if(condicionemp==''){
			swal('Atencion','El campo Condicion Empleado no puede estar vacio ',"warning");
				return false;
		}
		if(tiempoadmin==''){
			swal('Atencion','El campo Tiempo en Admin.Pública no puede estar vacio ',"warning");
				return false;
		}
		if(ingretipo==''){
			swal('Atencion','El campo Clasificacion Ingreso no puede estar vacio ',"warning");
				return false;
		}

		if(discapacidad==''){
			swal('Atencion','Debe Indicar si posee Discapacidad',"warning");
				return false;
		}else if(discapacidad=='t'){
			if(conapdis==''){
				swal('Atencion','El campo Número Conapdisno puede estar vacio ',"warning");
				return false;
			}
		}
		if(enfermedad==''){
				swal('Atencion','Debe Indicar si posee Enfermedad',"warning");
				return false;
		}
		if(faov==''){
				swal('Atencion','Debe Indicar si cotiza FAOV ',"warning");
				return false;
		}
		if(pertenecemision==''){
				swal('Atencion','Debe Indicar si Pertenece a alguna Misión  ',"warning");
				return false;
		}else if(pertenecemision=='t'){
			if(id_pertemis==''){
			swal('Atencion','Debe Especificar la Misión ',"warning");
				return false;
				}
		}
		if(beneficiomision==''){
				swal('Atencion','Debe Indicar si es Beneficiado por alguna Misión ',"warning");
				return false;
		}else if(beneficiomision=='t'){
			if(id_benefmision==''){
			swal('Atencion','Debe Especificar la Misión ',"warning");
				return false;
				}
		}
		if(deporte==''){
				swal('Atencion','Debe Indicar si practica algun Deporte ',"warning");
				return false;
		}else if(deporte=='t'){
			if(id_deporte==''){
			swal('Atencion','Debe Especificar el Deporte ',"warning");
				return false;
				}
		}
}else{

	$("#familiar").append('<tr id='+idtabular+'><td>'+idtabular+'</td><td>'+cedula+'</td><td>'+nombrecarga+'</td><td>'+apellidocarga+'</td><td>'+parentesco+'</td><td><a href=# onclick="eliminar('+idtabular+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Eliminar Carga" ></span></a></td><td style="display:none">'+nac+'#'+cdlp+'#'+sexo+'#'+fechacarga+'#'+emailcarga+'#'+telefonocarga+'#'+sector+'#'+edocivil+'#'+pais+'#'+ingreso+'#'+estudia+'#'+estudia_estado+'#'+estudia_municipio+'#'+estudia_parroquia+'#'+gradoins+'#'+ocupacion+'#'+aingreso+'#'+trabajo+'#'+estado_trabaja+'#'+municipio_trabaja+'#'+parroquia_trabaja+'#'+condicionemp+'#'+tiempoadmin+'#'+ingretipo+'#'+discapacidad+'#'+conapdis+'#'+enfermedad+'#'+faov+'#'+pertenecemision+'#'+id_pertemis+'#'+beneficiomision+'#'+id_benefmision+'#'+deporte+'#'+id_deporte+'</td></tr>');
	idtabular++;
	cedula= $("#cedulafam").val('');
	nombrecarga = $('#nombrecarga').val('');
	apellidocarga = $('#apellidocarga').val('');
	cdlp = $("#cdlp").val('');



}
	}else{
		if(sexo==''||nombrecarga==''||apellidocarga==''||fechacarga==''||estudia==''||discapacidad==''||deporte==''){
		if(sexo==''){
			swal('Atencion','El campo Género no puede estar vacio ',"warning");
			return false;
		}
		if(nombrecarga==''){
			swal('Atencion','El campo Nombre no puede estar vacio ',"warning");
			return false;
		}
		if(apellidocarga==''){
			swal('Atencion','El campo  Apellido no puede estar vacio ',"warning");
			return false;
		}
		if(fechacarga==''){
			swal('Atencion','El campo Fecha de Nacimiento no puede estar vacio ',"warning");
			return false;
		}        
		if(estudia==''){
			swal('Atencion','Debe Indicar si Estudia ',"warning");
			return false;
		}else if(estudia=='t'){
			if(estudia_estado==''){
				swal('Atencion','Debe Indicar el Estado donde Estudia',"warning");
				return false;
			}
			if(estudia_municipio==''){
				swal('Atencion','Debe Indicar el Municipio donde Estudia',"warning");
				return false;
			}
			if(estudia_parroquia==''){
				swal('Atencion','Debe Indicar la Parroquia donde Estudia',"warning");
				return false;
			}
		}
		if(discapacidad==''){
			swal('Atencion','Debe Indicar si posee Discapacidad',"warning");
				return false;
		}else if(discapacidad=='t'){
			if(conapdis==''){
				swal('Atencion','El campo Número Conapdisno puede estar vacio ',"warning");
				return false;
			}
		}
			if(deporte==''){
				swal('Atencion','Debe Indicar si practica algun Deporte ',"warning");
				return false;
		}else if(deporte=='t'){
			if(id_deporte==''){
			swal('Atencion','Debe Especificar el Deporte ',"warning");
				return false;
				}
		}
	}else{$("#familiar").append('<tr id='+idtabular+'><td>'+idtabular+'</td><td>'+cedula+'</td><td>'+nombrecarga+'</td><td>'+apellidocarga+'</td><td>'+parentesco+'</td><td><a style="color:black" onclick="eliminar('+idtabular+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Eliminar Carga" ></span></a></td><td style="display:none">'+nac+'#'+cdlp+'#'+sexo+'#'+fechacarga+'#'+emailcarga+'#'+telefonocarga+'#'+sector+'#'+edocivil+'#'+pais+'#'+ingreso+'#'+estudia+'#'+estudia_estado+'#'+estudia_municipio+'#'+estudia_parroquia+'#'+gradoins+'#'+ocupacion+'#'+aingreso+'#'+trabajo+'#'+estado_trabaja+'#'+municipio_trabaja+'#'+parroquia_trabaja+'#'+condicionemp+'#'+tiempoadmin+'#'+ingretipo+'#'+discapacidad+'#'+conapdis+'#'+enfermedad+'#'+faov+'#'+pertenecemision+'#'+id_pertemis+'#'+beneficiomision+'#'+id_benefmision+'#'+deporte+'#'+id_deporte+'</td></tr>');
	idtabular++;
$("#cedulafam").val('');
	$('#parentesco').val('');
	$('#cdlp').val();
	$('#nombrecarga').val('');
	$('#apellidocarga').val('');}
	
}


	}


function buscarapto(){
	
	piso = $("#EncApartamento_piso_id").val();
	$.ajax({
				url:"<?php echo $this->createUrl('Apto'); ?>",
				cache: false,
				type: "POST",
				dataType: "html",
				data: ({piso:piso}),
			
				success: function(data){
					$("#apto").html(data);
				}
			});
}
function buscartodo($id){
	

	alert($id);
	}
$("#EncApartamento_piso_id").on('change',function(){
	var apar = $("#apto").val();
	var piso = $("#EncApartamento_piso_id").val();
	$("#idapto").val('');
	$("#familia").hide();
	if(piso==''){
		swal("Atención","Debe seleccionar Primero un Piso","warning");
		$("#familia").hide();
		$("#idapto").val('');
		return false;
	}
		if(apar==''){
		swal("Atención","Debe seleccionar Primero un Apartamento","warning");
		$("#familia").hide();
		$("#idapto").val('');
		return false;	
	}
	
});

$('#apto').on('change',function(){

$("#idapto").val($('#apto').val());

	var aptopi = $("#idapto").val();
	var piso = $("#EncApartamento_piso_id").val();
	var apar = $("#apto").val();
	
	
	if(aptopi==''){
		if(piso==''){
		swal("Atención","Debe seleccionar Primero un Piso","warning");
		$("#familia").hide();
		return false;
		}
		if(apar==''){
		swal("Atención","Debe seleccionar Primero un Apartamento","warning");
		$("#familia").hide();
		return false;	
		}
	}else{
	$("#familia").show('slow');
	$("#idaptfam").val(aptopi);
	}

	});
function familia(){
	var aptopi = $("#idapto").val();
	var piso = $("#EncApartamento_piso_id").val();
	var apar = $("#apto").val();
	
	
	if(aptopi==''){
		if(piso==''){
		swal("Atención","Debe seleccionar Primero un Piso","warning");
		return false;
		}
		if(apar==''){
		swal("Atención","Debe seleccionar Primero un Apartamento","warning");
		return false;	
		}
	}else{
	$("#familia").show('slow');
	$("#idaptfam").val(aptopi);
	}
}

function eliminar($id){
$("#familiar tr#"+$id).remove();

}

</script>
 
<?php if($_GET['carga']==1){ ?>  
  <script>
$( document ).ready(function() {

swal("Atencion!", "Datos guardados con exito","success");
});
</script>
  <?php } ?>