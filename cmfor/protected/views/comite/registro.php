<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style>
	.row_campos_detalles div,.row_campos_detalles input,.row_campos_detalles select{padding:0px;margin:0px;border-radius:0px;height:20px;}.boton_menos,.boton_mas{height:20px;padding:2px 3px 2px 3px;}#th_button_nuevo{text-align:center;width:80px;}.td_botones button{float:left;}.div_botones_listar{}.div_botones_listar button{margin-left:3px;}.ui-datepicker-month, .ui-datepicker-year{color:#333;font-weight: 900;}.row{margin-top: 20px;margin-bottom: 20px;}
	label.btn span {font-size: 1em}
	label input[type="radio"] ~ i.fa.fa-circle-o{color: #c8c8c8; display: inline}
	label input[type="radio"] ~ i.fa.fa-dot-circle-o{display: none}
	label input[type="radio"]:checked ~ i.fa.fa-circle-o{display: none}
	label input[type="radio"]:checked ~ i.fa.fa-dot-circle-o{color: #7AA3CC;display: inline;}label:hover input[type="radio"] ~ i.fa {color: #7AA3CC}
	label input[type="checkbox"] ~ i.fa.fa-square-o{color: #c8c8c8;display: inline}
	label input[type="checkbox"] ~ i.fa.fa-check-square-o{display: none}
	label input[type="checkbox"]:checked ~ i.fa.fa-square-o{display: none}
	label input[type="checkbox"]:checked ~ i.fa.fa-check-square-o{color: #7AA3CC;display: inline}
	label:hover input[type="checkbox"] ~ i.fa {color: #7AA3CC}
	.has-error div[data-toggle="buttons"] label.active{color: #7AA3CC}
	div[data-toggle="buttons"] label.active{color: #7AA3CC}
	div[data-toggle="buttons"] label:hover {color: #7AA3CC}
	
	.has-error div[data-toggle="buttons"] label {
		color: #a94442;
	}
	.fa-2x{
		font-size: 1em;
	}
	div[data-toggle="buttons"] label {
	display: inline-block; 
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 20px;
	font-weight: normal;
	line-height: 2em;
	text-align: left;
	white-space: nowrap;
	vertical-align: top;
	cursor: pointer;
	background-color: none;
	border: 0px solid #c8c8c8;
	border-radius: 3px;
	color: #c8c8c8;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	-o-user-select: none;
	user-select: none;
	}
	div[data-toggle="buttons"] label:active, div[data-toggle="buttons"] label.active {
	-webkit-box-shadow: none;
	box-shadow: none;
	}
	span.required {
		color:red;
	}
	.help-block {
		font-size: 20px;
	}
	p.stop:hover{
		color:black;
	}
</style>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'comite-form',
	'enableAjaxValidation'=>false,
)); ?>

<br>
<div class="container-fluid">
	<h3>REGISTRO DE COMITÉ MULTIFAMILIAR DE GESTION</h3>
	<br><br>
<fieldset> 

	<legend align="center">DATOS GENERALES DEL COMITÉ MULTIFAMILIAR DE GESTION</legend>
		<legend><span style="color:red">*</span>Datos Del Urbanismo</legend>	
	
	<div class="row">
								<?php echo $this->renderPartial('_estadoMunicipioParroquiarq',array(
										'model'=>$model,
										'form'=>$form,
										'atributoEstado'=>'id_estado',
										'atributoMunicipio'=>'id_municipio',
										'atributoParroquia'=>'id_parroquia',
										'size'=>'4',
										),true); 
								?>
								
							</div>

									<div class="col-md-3">
			
	<?php $urban = CHtml::listData(Urbanismo::model()->findAll(), 'id_urbanismo', 'nb_urbanismo')?>
	<?php echo $this->renderPartial('_dropDownrq',array(
		'model'=>$model,
		'form'=>$form,											
		'atributo'=>'id_urbanismo',
		'onchange'=>'asociar()',
		'list'=>$urban,),true);?>			
		</div>

							<div class="col-md-3">
			
		<?php echo $this->renderPartial('_fieldr',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'torre',
				),true);
		?>
	
			
		</div>
		<div class="col-md-3">
			
		<?php echo $this->renderPartial('_fieldr',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'vivienda',
				),true);
		?>
	
			
		</div>

					<div class="col-md-12" >
<fieldset>

	<legend id="sevcio<?php echo $i ?>"><span style="color:red">*</span>Datos Del Vocero</legend>
				<div class="form-group col-md-3">

			<?php echo "<span style='color:red'>*</span>";echo $form->labelEx($serv,"cedulavc");?>
			<div class="input-group">
				<div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="button_nacionalidad3"><?php echo ($model->jfc_nac?$model->jfc_nac:'V'); ?></span><span class="caret"></span></button>
					<ul class="dropdown-menu" role="menu">
						<li><a onclick="$('#resnac').val('V');$('#button_nacionalidad3').text('V');">V</a></li>
						<li><a onclick="$('#resnac').val('E');$('#button_nacionalidad3').text('E');">E</a></li>
					</ul>
				</div>
					<?php echo $form->textField($serv,"cedulavc",array('class'=>'form-control','id'=>'resci'));?>
				<span class="input-group-btn">
					<button onclick="buscarpersona3()" class="btn btn-primary" type="button">Buscar</button>
				</span>
			</div>
			
		</div>
		<div class="col-md-2">
		<?php echo $this->renderPartial('_fieldReadOnly',array(
											'model'=>$serv,
											'form'=>$form,
											'atributo'=>'nbvc',
											'id'=>'nbvcs',
											'size'=>'3',
											),true); 
									?>
								</div>
			
												<div class="col-md-2">
		<?php echo $this->renderPartial('_fieldReadOnly',array(
											'model'=>$serv,
											'form'=>$form,
											'atributo'=>'apvc',
											'id'=>'apvcs',
											'size'=>'3',
											),true); 
									?>
								</div>


									<div class="col-md-3">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$serv,
											'form'=>$form,
											'atributo'=>'corvc',
											'id'=>'corvcs',
											'size'=>'3',
											),true); 
									?>
								</div>
								
		<div class="col-md-2">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$serv,
											'form'=>$form,
											'atributo'=>'tlfvc',
											'id'=>'tlfvcs',
											'size'=>'3',
											),true); 
									?>
								</div>
</div>
<div class="input-group col-md-4" >
<label for="MesaServ_apvc">Tipo</label>
<div>

<select placeholder="--SELECCIONE--" class="form-control" style="width:60%" required="1" name="Comite[p_o_s]" id="p_o_s">
<option value="">--SELECCIONE--</option>
<option value="PRINCIPAL">PRINCIPAL</option>
<option value="SUPLENTE">SUPLENTE</option>
</select> 

<span class="input-group-btn">
<button onclick="Agregar()" class="btn btn-primary" type="button">Agregar</button>
</span>
</div>
</div >

</fieldset>
			


<div class="col-md-12" id="productotal">
	<br><br>
<fieldset>

<legend align="center">VOCEROS PRINCIPALES Y SUPLENTES</legend>

<table class=" table table-responsive table-striped table-bordered" id="voceros_carga">
	<tr >
	<td style="color:black">ESTATUS DE VOCERO</td>
	<td style="color:black">NOMBRE </td>
	<td style="color:black">APELLIDO </td>
	<td style="color:black">Nº CÉDULA</td>
	<td style="color:black">Nº TELEFONO</td>
	<td style="color:black">CORREO ELECTTÓNICO</td>	
	<td style="color:black">ACCION</td>	

	</tr>

	

</table>

</fieldset>
</div>

<div class="col-md-12" >
	<br><br><br>
<fieldset>
		<legend align="center" >Observación de la carga</legend>
<div class="col-md-12">
			
		<?php echo $this->renderPartial('_fieldr',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'observacion',
				),true);
		?>
	
			
		</div>

</fieldset>
</div>
		
	<?php $this->endWidget(); ?>
	</div>





<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>

var id_tabular=1;
				function buscarpersona3($id){
			var ci = $("#resci").val();
		
			var nac= $("#button_nacionalidad3").val();
	
			$.ajax({
				url:"<?php echo $this->createUrl('Cedula'); ?>",
				cache: false,
				type: "POST",
				dataType: "json",
				data: ({cedula:ci,nacionalidad:nac}),
			
				success: function(data){

					$('#resci').val(data.cedula);
					$('#nbvcs').val(data.nombre1);
					$('#apvcs').val(data.apellido1);
					
				}
			});
		
		};

function Agregar(){

	var tipo=$('#p_o_s').val();	
	var nom=$('#nbvcs').val();
	var ape=$('#apvcs').val();	
	var cedula=$('#resci').val();	
	var tele=$('#tlfvcs').val();
	var correo=$('#corvcs').val();
     var duplicada=0;
var vueltas= $('#productotal table tr').length;

for(var i=1;i<vueltas;i++){
var cedulatabla= $('#productotal table tr').eq(''+i+'').find('td').eq(3).text();

if(cedulatabla==cedula){
	duplicada=1;
	break;
}
}

	if(duplicada !=1){
	if(tipo!='' && correo!='' && tele!=''){	 					
	

	var agrega='<tr id="'+id_tabular+'"><td style="color:black">'+tipo+'</td><td style="color:black">'+nom+'</td><td style="color:black">'+ape+'</td><td style="color:black">'+cedula+'</td><td style="color:black">'+tele+'</td><td style="color:black">'+correo+'</td> <td ><a style="color:black" href=# onclick="eliminar('+id_tabular+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Eliminar" ></span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:black" href=# onclick="modificar('+id_tabular+')" ><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Eliminar" ></span></a></td></tr>';

		$('#voceros_carga').append(agrega);
		id_tabular++;
	 $('#p_o_s').val('');	
	 $('#nbvcs').val('');
	 $('#apvcs').val('');	
	 $('#resci').val('');	
	 $('#tlfvcs').val('');
	 $('#corvcs').val('');
	}else
	{
		if(tipo==''){swal("Atencion!", "Debe especificar si el vocero es (PRINCIPAL) o (SUPLENTE)","error");}
		else if(correo==''){swal("Atencion!", "Debe especificar el correo","error");}
		else if(tele==''){swal("Atencion!", "Debe especificar el telefono","error");}

		
	}
}else{swal("Atencion!", "La cedula ya esta cargada","error");
 $('#p_o_s').val('');	
	 $('#nbvcs').val('');
	 $('#apvcs').val('');	
	 $('#resci').val('');	
	 $('#tlfvcs').val('');
	 $('#corvcs').val('');
	 }
		};

		function eliminar($id){
			
		$('#'+$id).remove();

		};

		function modificar(id){
			
		var tipo= $('#productotal table tr#'+id+'').find('td').eq(0).text();
		var nombre= $('#productotal table tr#'+id+'').find('td').eq(1).text();
		var apellido= $('#productotal table tr#'+id+'').find('td').eq(2).text();
		var cedula= $('#productotal table tr#'+id+'').find('td').eq(3).text();
		var telefono= $('#productotal table tr#'+id+'').find('td').eq(4).text();
		var correo= $('#productotal table tr#'+id+'').find('td').eq(5).text();

	$('#p_o_s').val(tipo);	
	$('#nbvcs').val(nombre);
	$('#apvcs').val(apellido);	
	$('#resci').val(cedula);	
	$('#tlfvcs').val(telefono);
	$('#corvcs').val(correo);
		
		$('#'+id).remove();

		};


	

	</script>