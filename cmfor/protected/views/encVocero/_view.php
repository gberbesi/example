<?php
/* @var $this EncVoceroController */
/* @var $data EncVocero */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('enc_vocero_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->enc_vocero_id), array('view', 'id'=>$data->enc_vocero_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nacionalidad')); ?>:</b>
	<?php echo CHtml::encode($data->nacionalidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula')); ?>:</b>
	<?php echo CHtml::encode($data->cedula); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo_cdlp')); ?>:</b>
	<?php echo CHtml::encode($data->codigo_cdlp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre1')); ?>:</b>
	<?php echo CHtml::encode($data->nombre1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre2')); ?>:</b>
	<?php echo CHtml::encode($data->nombre2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido1')); ?>:</b>
	<?php echo CHtml::encode($data->apellido1); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido2')); ?>:</b>
	<?php echo CHtml::encode($data->apellido2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono1')); ?>:</b>
	<?php echo CHtml::encode($data->telefono1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('correo')); ?>:</b>
	<?php echo CHtml::encode($data->correo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('torre')); ?>:</b>
	<?php echo CHtml::encode($data->torre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_urbanismo')); ?>:</b>
	<?php echo CHtml::encode($data->id_urbanismo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('etapa')); ?>:</b>
	<?php echo CHtml::encode($data->etapa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_id')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo')); ?>:</b>
	<?php echo CHtml::encode($data->tipo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('promotor_id')); ?>:</b>
	<?php echo CHtml::encode($data->promotor_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apartamento')); ?>:</b>
	<?php echo CHtml::encode($data->apartamento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('votos')); ?>:</b>
	<?php echo CHtml::encode($data->votos); ?>
	<br />

	*/ ?>

</div>