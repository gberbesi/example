<?php
/* @var $this EncVoceroController */
/* @var $model EncVocero */

$this->breadcrumbs=array(
	'Enc Voceros'=>array('index'),
	$model->enc_vocero_id,
);

$this->menu=array(
	array('label'=>'List EncVocero', 'url'=>array('index')),
	array('label'=>'Create EncVocero', 'url'=>array('create')),
	array('label'=>'Update EncVocero', 'url'=>array('update', 'id'=>$model->enc_vocero_id)),
	array('label'=>'Delete EncVocero', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->enc_vocero_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EncVocero', 'url'=>array('admin')),
);
?>

<h1>View EncVocero #<?php echo $model->enc_vocero_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'enc_vocero_id',
		'nacionalidad',
		'cedula',
		'codigo_cdlp',
		'nombre1',
		'nombre2',
		'apellido1',
		'apellido2',
		'telefono1',
		'correo',
		'torre',
		'id_urbanismo',
		'etapa',
		'usuario_id',
		'tipo',
		'promotor_id',
		'apartamento',
		'votos',
	),
)); ?>
