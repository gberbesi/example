<?php
/* @var $this EncVoceroController */
/* @var $model EncVocero */

$this->breadcrumbs=array(
	'Enc Voceros'=>array('index'),
	$model->enc_vocero_id=>array('view','id'=>$model->enc_vocero_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EncVocero', 'url'=>array('index')),
	array('label'=>'Create EncVocero', 'url'=>array('create')),
	array('label'=>'View EncVocero', 'url'=>array('view', 'id'=>$model->enc_vocero_id)),
	array('label'=>'Manage EncVocero', 'url'=>array('admin')),
);
?>

<h1>Update EncVocero <?php echo $model->enc_vocero_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>