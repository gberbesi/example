<?php
/* @var $this EncVoceroController */
/* @var $model EncVocero */

$this->breadcrumbs=array(
	'Enc Voceros'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EncVocero', 'url'=>array('index')),
	array('label'=>'Manage EncVocero', 'url'=>array('admin')),
);
?>

<h1>Create EncVocero</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>