<?php
/* @var $this EncVoceroController */
/* @var $model EncVocero */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'enc-vocero-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nacionalidad'); ?>
		<?php echo $form->textField($model,'nacionalidad'); ?>
		<?php echo $form->error($model,'nacionalidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cedula'); ?>
		<?php echo $form->textField($model,'cedula'); ?>
		<?php echo $form->error($model,'cedula'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'codigo_cdlp'); ?>
		<?php echo $form->textField($model,'codigo_cdlp',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'codigo_cdlp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre1'); ?>
		<?php echo $form->textField($model,'nombre1',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'nombre1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre2'); ?>
		<?php echo $form->textField($model,'nombre2',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'nombre2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apellido1'); ?>
		<?php echo $form->textField($model,'apellido1',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'apellido1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apellido2'); ?>
		<?php echo $form->textField($model,'apellido2',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'apellido2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono1'); ?>
		<?php echo $form->textField($model,'telefono1'); ?>
		<?php echo $form->error($model,'telefono1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'correo'); ?>
		<?php echo $form->textField($model,'correo',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'correo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'torre'); ?>
		<?php echo $form->textField($model,'torre'); ?>
		<?php echo $form->error($model,'torre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_urbanismo'); ?>
		<?php echo $form->textField($model,'id_urbanismo'); ?>
		<?php echo $form->error($model,'id_urbanismo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'etapa'); ?>
		<?php echo $form->textField($model,'etapa',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'etapa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usuario_id'); ?>
		<?php echo $form->textField($model,'usuario_id'); ?>
		<?php echo $form->error($model,'usuario_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo'); ?>
		<?php echo $form->textField($model,'tipo',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'tipo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'promotor_id'); ?>
		<?php echo $form->textField($model,'promotor_id'); ?>
		<?php echo $form->error($model,'promotor_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apartamento'); ?>
		<?php echo $form->textField($model,'apartamento',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'apartamento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'votos'); ?>
		<?php echo $form->textField($model,'votos'); ?>
		<?php echo $form->error($model,'votos'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->