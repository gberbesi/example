<?php
/* @var $this EncVoceroController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Enc Voceros',
);

$this->menu=array(
	array('label'=>'Create EncVocero', 'url'=>array('create')),
	array('label'=>'Manage EncVocero', 'url'=>array('admin')),
);
?>

<h1>Enc Voceros</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
