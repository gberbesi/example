<?php
/* @var $this EncVoceroController */
/* @var $model EncVocero */

$this->breadcrumbs=array(
	'Enc Voceros'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List EncVocero', 'url'=>array('index')),
	array('label'=>'Create EncVocero', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#enc-vocero-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Enc Voceros</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'enc-vocero-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'enc_vocero_id',
		'nacionalidad',
		'cedula',
		'codigo_cdlp',
		'nombre1',
		'nombre2',
		/*
		'apellido1',
		'apellido2',
		'telefono1',
		'correo',
		'torre',
		'id_urbanismo',
		'etapa',
		'usuario_id',
		'tipo',
		'promotor_id',
		'apartamento',
		'votos',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
