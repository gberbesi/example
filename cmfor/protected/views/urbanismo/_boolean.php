<div class="text-center form-group <?php if(isset($_POST[get_class($model)])) echo $model->hasErrors($atributo)?'has-error':'has-success'; ?>">
	<?php echo $form->labelEx($model,$atributo); ?>
	<br>
	<?php echo $form->hiddenField($model,$atributo,array('value'=>'')); ?>
	<div class="btn-group" data-toggle="buttons">
		<?php foreach($valores as $key=>$valor): ?>
		<label class="btn <?php echo ($model->$atributo==$key ? "active" : "")?>">
			<input <?php echo ($model->$atributo==$key ? "checked" : "") ?> type="radio" value="<?php echo $key; ?>" name="<?php echo get_class($model).'['.$atributo.']'; ?>">
			<i class="fa fa-check-circle-o fa-2x"></i><span> <?php echo $valor; ?></span>
		</label>
		<?php endforeach; ?>
	</div>
	<?php echo $form->error($model,$atributo,array('class'=>'help-block text-center')); ?>
</div>