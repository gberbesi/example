<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_urbanismo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_urbanismo), array('view', 'id'=>$data->id_urbanismo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_estado')); ?>:</b>
	<?php echo CHtml::encode($data->id_estado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_municipio')); ?>:</b>
	<?php echo CHtml::encode($data->id_municipio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_parroquia')); ?>:</b>
	<?php echo CHtml::encode($data->id_parroquia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_ciudad')); ?>:</b>
	<?php echo CHtml::encode($data->id_ciudad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nb_vocero')); ?>:</b>
	<?php echo CHtml::encode($data->nb_vocero); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ape_vocero')); ?>:</b>
	<?php echo CHtml::encode($data->ape_vocero); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('tlf_vocero')); ?>:</b>
	<?php echo CHtml::encode($data->tlf_vocero); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cor_vocero')); ?>:</b>
	<?php echo CHtml::encode($data->cor_vocero); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nb_urbanismo')); ?>:</b>
	<?php echo CHtml::encode($data->nb_urbanismo); ?>
	<br />

	*/ ?>

</div>