<?php
$this->breadcrumbs=array(
	'Urbanismos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Urbanismo', 'url'=>array('index')),
	array('label'=>'Manage Urbanismo', 'url'=>array('admin')),
);
?>

<h1>Create Urbanismo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>