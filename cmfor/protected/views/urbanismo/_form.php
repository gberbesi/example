<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'urbanismo-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_urbanismo'); ?>
		<?php echo $form->textField($model,'id_urbanismo'); ?>
		<?php echo $form->error($model,'id_urbanismo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_estado'); ?>
		<?php echo $form->textField($model,'id_estado'); ?>
		<?php echo $form->error($model,'id_estado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_municipio'); ?>
		<?php echo $form->textField($model,'id_municipio'); ?>
		<?php echo $form->error($model,'id_municipio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_parroquia'); ?>
		<?php echo $form->textField($model,'id_parroquia'); ?>
		<?php echo $form->error($model,'id_parroquia'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_ciudad'); ?>
		<?php echo $form->textField($model,'id_ciudad'); ?>
		<?php echo $form->error($model,'id_ciudad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nb_vocero'); ?>
		<?php echo $form->textField($model,'nb_vocero',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'nb_vocero'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ape_vocero'); ?>
		<?php echo $form->textField($model,'ape_vocero',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'ape_vocero'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tlf_vocero'); ?>
		<?php echo $form->textField($model,'tlf_vocero',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'tlf_vocero'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cor_vocero'); ?>
		<?php echo $form->textField($model,'cor_vocero',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'cor_vocero'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nb_urbanismo'); ?>
		<?php echo $form->textField($model,'nb_urbanismo',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'nb_urbanismo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->