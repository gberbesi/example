<?php
$this->breadcrumbs=array(
	'Urbanismos',
);

$this->menu=array(
	array('label'=>'Create Urbanismo', 'url'=>array('create')),
	array('label'=>'Manage Urbanismo', 'url'=>array('admin')),
);
?>

<h1>Urbanismos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
