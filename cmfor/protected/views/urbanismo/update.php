<?php
$this->breadcrumbs=array(
	'Urbanismos'=>array('index'),
	$model->id_urbanismo=>array('view','id'=>$model->id_urbanismo),
	'Update',
);

$this->menu=array(
	array('label'=>'List Urbanismo', 'url'=>array('index')),
	array('label'=>'Create Urbanismo', 'url'=>array('create')),
	array('label'=>'View Urbanismo', 'url'=>array('view', 'id'=>$model->id_urbanismo)),
	array('label'=>'Manage Urbanismo', 'url'=>array('admin')),
);
?>

<h1>Update Urbanismo <?php echo $model->id_urbanismo; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>