<?php
$this->breadcrumbs=array(
	'Urbanismos'=>array('index'),
	$model->id_urbanismo,
);

$this->menu=array(
	array('label'=>'List Urbanismo', 'url'=>array('index')),
	array('label'=>'Create Urbanismo', 'url'=>array('create')),
	array('label'=>'Update Urbanismo', 'url'=>array('update', 'id'=>$model->id_urbanismo)),
	array('label'=>'Delete Urbanismo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_urbanismo),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Urbanismo', 'url'=>array('admin')),
);
?>

<h1>View Urbanismo #<?php echo $model->id_urbanismo; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_urbanismo',
		'id_estado',
		'id_municipio',
		'id_parroquia',
		'id_ciudad',
		'nb_vocero',
		'ape_vocero',
		'tlf_vocero',
		'cor_vocero',
		'nb_urbanismo',
	),
)); ?>
