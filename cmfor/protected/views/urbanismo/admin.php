<?php
$this->breadcrumbs=array(
	'Urbanismos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Urbanismo', 'url'=>array('index')),
	array('label'=>'Create Urbanismo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('urbanismo-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Seguimiento de Urbanismos</h1>



<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>

<?php $this->renderPartial('application.views.urbanismo._search',array(
	'model'=>$model,
)); ?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'urbanismo-grid',
	'cssFile'=> Yii::app()->request->baseUrl.'/css/cgridview.css',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'urbanismo_nombre',
		'estado',
		'municipio',
		'parroquia',
		'direccion',
	
		/*
		'municipio',
		'municipio',
		'municipio',
	
		*/
		 array(
                                          'header'=>"",
                                                    'htmlOptions'=>array('class'=>'col-md-1 col-lg-1 text-center','style'=>'color:red;'),
                                         'value'=>'"<a data-toggle=\"tooltip\" onclick=\"selectedAlumno3(".$data->municipio.")\" title=\"Imprimir Facturas\" href=\"#\"><i class=\" glyphicon glyphicon-download-alt\" style=\"color:black;font-size:20px; \"></i></a>"',
                                                    'type'=>'raw',
                                         ),
		
	),
)); ?>


<script>
function selectedAlumno3($id){

$('#familia').show('fast');
}
</script>