<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'enc_promotor_id'); ?>
		<?php echo $form->textField($model,'enc_promotor_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nacionalidad'); ?>
		<?php echo $form->textField($model,'nacionalidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cedula'); ?>
		<?php echo $form->textField($model,'cedula'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'codigo_cdlp'); ?>
		<?php echo $form->textField($model,'codigo_cdlp',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre1'); ?>
		<?php echo $form->textField($model,'nombre1',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre2'); ?>
		<?php echo $form->textField($model,'nombre2',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'apellido1'); ?>
		<?php echo $form->textField($model,'apellido1',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'apellido2'); ?>
		<?php echo $form->textField($model,'apellido2',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_nac'); ?>
		<?php echo $form->textField($model,'fecha_nac'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telefono1'); ?>
		<?php echo $form->textField($model,'telefono1'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'telefono2'); ?>
		<?php echo $form->textField($model,'telefono2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'correo'); ?>
		<?php echo $form->textField($model,'correo',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado'); ?>
		<?php echo $form->textField($model,'estado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'municipio'); ?>
		<?php echo $form->textField($model,'municipio'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parroquia'); ?>
		<?php echo $form->textField($model,'parroquia'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sector'); ?>
		<?php echo $form->textField($model,'sector'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'direccion'); ?>
		<?php echo $form->textField($model,'direccion',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'torre1'); ?>
		<?php echo $form->textField($model,'torre1'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'torre2'); ?>
		<?php echo $form->textField($model,'torre2'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'torre3'); ?>
		<?php echo $form->textField($model,'torre3'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'torre4'); ?>
		<?php echo $form->textField($model,'torre4'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'torre5'); ?>
		<?php echo $form->textField($model,'torre5'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'torre6'); ?>
		<?php echo $form->textField($model,'torre6'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_urbanismo'); ?>
		<?php echo $form->textField($model,'id_urbanismo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'etapa'); ?>
		<?php echo $form->textField($model,'etapa',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->