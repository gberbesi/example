<fieldset>
	<legend>Familia Del apartamento #<?php echo $personas[0]->apartamento_id; ?></legend>
	<table class="table table-striped table-bordered">
		<tr >
			<td style="color:black">Cédula</td>
			<td style="color:black">Nombre</td>
			<td style="color:black">Apellido</td>
			<td style="color:black">Teléfono</td>
			<td style="color:black">Parentesco</td>
		</tr>
		<?php foreach ($personas as $key => $value) { ?>
			<?php $parentesco= FamParentesco::model()->find('parentesco_id=:parent',array('parent'=>$value->parentesco_id)) ;?>
			<tr>
				<td><?php echo $value->cedula;?></td>
				<td><?php echo $value->nombre1 .' '. $value->nombre2;?></td>
				<td><?php echo $value->apellido1 .' '. $value->apellido2;?></td>
				<td><?php echo $value->per_telefono;?></td>
				<td><?php echo $parentesco->nombre;?></td>
			</tr>
		<?php } ?>	
	</table>
	<br><br><br>
<div class="col-md-2 col-md-offset-10">
	<a href="javascript:window.history.back();"><button class="btn btn-primary"><span class="glyphicon glyphicon-arrow-left"></span> Volver atrás</button></a>
</div>
</fieldset>