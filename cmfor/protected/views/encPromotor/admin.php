<?php
$this->breadcrumbs=array(
	'Enc Promotors'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List EncPromotor', 'url'=>array('index')),
	array('label'=>'Create EncPromotor', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('enc-promotor-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Información Promotores - Torres - Urbanismo</h1>



<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'enc-promotor-grid',
	'dataProvider'=>$model->search(),
	
	//'filter'=>$model,
	'columns'=>array(
		array(
      			'header'=>'<p style="color:white;font-weight:bold" align="center">Cédula Promotor</p>',
 				'name'=>'urbanismo_nombre',
 		 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  				'value'=>'$data->cedula',
    ),
		array(
      			'header'=>'<p style="color:white;font-weight:bold" align="center">Nombre Promotor</p>',
 				'name'=>'urbanismo_nombre',
 		 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  				'value'=>'$data->nombre1',
    ),
		array(
      			'header'=>'<p style="color:white;font-weight:bold" align="center">Apellido Promotor</p>',
 				'name'=>'urbanismo_nombre',
 		 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  				'value'=>'$data->apellido1',
    ),
	array(
      			'header'=>'<p style="color:white;font-weight:bold" align="center">Nombre Urbanismo</p>',
 				'name'=>'urbanismo_nombre',
 		 		'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  				//'value'=>' $data->urbanismos->urbanismo_nombre ',
				'value'=>'"<a style=\"color:black\" data-toggle=\"tooltip\" onclick=\"selectedAlumno4(".$data->id_urbanismo.")\" title=\"Ver Registro\" href=\"#\">$data->id_urbanismo</a>"',
                                                    'type'=>'raw',
  				

    ),	
    array(
      			'header'=>'<p style="color:white;font-weight:bold" align="center">Correo</p>',
 				'name'=>'urbanismo_nombre',
 		 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  				'value'=> '$data->correo',
    ),	
     array(
                                          'header'=>"",
                                                    'htmlOptions'=>array('class'=>'col-md-1 col-lg-1 text-center','style'=>'color:red;'),
                                         'value'=>'"<a data-toggle=\"tooltip\" onclick=\"selectedAlumno5(".$data->enc_promotor_id.")\" title=\"Ver Registro\" ><i class=\" glyphicon glyphicon-search\" style=\"color:black;font-size:20px; \"></i></a>"',
                                                    'type'=>'raw',
                                         ),
     array(
                                          'header'=>"",
                                                    'htmlOptions'=>array('class'=>'col-md-1 col-lg-1 text-center','style'=>'color:red;'),
                                         'value'=>'"<a data-toggle=\"tooltip\" onclick=\"selectedAlumno6(".$data->enc_promotor_id.")\" title=\"Ver Registro\" ><i class=\" glyphicon glyphicon-home\" style=\"color:black;font-size:20px; \"></i></a>"',
                                                    'type'=>'raw',
                                         ),
		
		
	),
)); ?>

<div class="modal fade col-md-12" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">

           <h3 align="center" class="modal-title" id="exampleModal3Label" style='font-weight:bold'>Imformacion del urbanimo</h3>
      </div>
      
      <div class="modal-body" >
<div class="table table-responsive table-striped col-md-12" id="pegar">
 
</div>

<div class="modal-footer">
        <button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button>
      </div>
                  </div>
            </div>

</div>
</div>



		<script>
		function selectedAlumno5($id){

  location.href='index.php?r=encPromotor/view&id='+$id;
}

function selectedAlumno6($id){

  location.href='index.php?r=encPromotor/torre&id='+$id;
}

function selectedAlumno4($id){

  

   $.ajax({ 
      method: "POST", 
    
      dataType: "html",
      url: "<?php echo CController::createUrl('Updatetabular') ?>",
      data: {id_urbanismo:$id }
    })
    .done(function(msg) {
$('#pegar').html(msg);
});
 
    $('#exampleModal').modal();




}
</script>		
