<?php
$this->breadcrumbs=array(
	'Enc Promotors'=>array('index'),
	$model->enc_promotor_id=>array('view','id'=>$model->enc_promotor_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EncPromotor', 'url'=>array('index')),
	array('label'=>'Create EncPromotor', 'url'=>array('create')),
	array('label'=>'View EncPromotor', 'url'=>array('view', 'id'=>$model->enc_promotor_id)),
	array('label'=>'Manage EncPromotor', 'url'=>array('admin')),
);
?>

<h1>Update EncPromotor <?php echo $model->enc_promotor_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>