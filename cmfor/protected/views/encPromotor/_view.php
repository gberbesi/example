<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('enc_promotor_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->enc_promotor_id), array('view', 'id'=>$data->enc_promotor_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nacionalidad')); ?>:</b>
	<?php echo CHtml::encode($data->nacionalidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula')); ?>:</b>
	<?php echo CHtml::encode($data->cedula); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo_cdlp')); ?>:</b>
	<?php echo CHtml::encode($data->codigo_cdlp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre1')); ?>:</b>
	<?php echo CHtml::encode($data->nombre1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre2')); ?>:</b>
	<?php echo CHtml::encode($data->nombre2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido1')); ?>:</b>
	<?php echo CHtml::encode($data->apellido1); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido2')); ?>:</b>
	<?php echo CHtml::encode($data->apellido2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_nac')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_nac); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono1')); ?>:</b>
	<?php echo CHtml::encode($data->telefono1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono2')); ?>:</b>
	<?php echo CHtml::encode($data->telefono2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('correo')); ?>:</b>
	<?php echo CHtml::encode($data->correo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('municipio')); ?>:</b>
	<?php echo CHtml::encode($data->municipio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parroquia')); ?>:</b>
	<?php echo CHtml::encode($data->parroquia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sector')); ?>:</b>
	<?php echo CHtml::encode($data->sector); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('torre1')); ?>:</b>
	<?php echo CHtml::encode($data->torre1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('torre2')); ?>:</b>
	<?php echo CHtml::encode($data->torre2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('torre3')); ?>:</b>
	<?php echo CHtml::encode($data->torre3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('torre4')); ?>:</b>
	<?php echo CHtml::encode($data->torre4); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('torre5')); ?>:</b>
	<?php echo CHtml::encode($data->torre5); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('torre6')); ?>:</b>
	<?php echo CHtml::encode($data->torre6); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_urbanismo')); ?>:</b>
	<?php echo CHtml::encode($data->id_urbanismo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('etapa')); ?>:</b>
	<?php echo CHtml::encode($data->etapa); ?>
	<br />

	*/ ?>

</div>