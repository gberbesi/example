<?php 
//var_dump(count($cars));exit();
$fact=Yii::app()->request->requestUri;
$server=$_SERVER['HTTP_HOST'];
$url=$server.$fact;





$html='<p align="center" ><b> ACTA DE ASAMBLEA CONSTITUTIVA Y ESTATUTOS DEL  <br>';



$html.='
COMITÉ MULTIFAMILIAR DE GESTIÓN “'.strtoupper($datotorre->nom_comite).'” </b><p> <br>
<p ALIGN="justify">En el día de hoy '.date('d').' de '.date('m').' del año '.date('Y').', siendo las '.date('h:m').', los ciudadanos abajo firmantes, mayores de edad, debidamente identificados, adjudicatarios del urbanismo construido en el marco de la Gran Misión Vivienda Venezuela “'.strtoupper($datourbanismo->urbanismo_nombre).'”, ubicado en'.$datourbanismo->sector.', de la Parroquia '.$datourbanismo->parroquia.', Municipio '.$datourbanismo->municipio.' del estado'.$datourbanismo->estado.', en ejercicio del derecho a la participación previsto en los Artículos 5, 62 y 70 de la Constitución de la República Bolivariana de Venezuela, nos hemos reunidos en Asamblea General de Adjudicatarios como máxima instancia de deliberación y toma de decisiones en el ejercicio del Poder Popular para constituir, como en efecto lo hacemos según lo contemplado en los artículos 14, 15, 16 y 17 de la Ley del Régimen de Propiedad de las Viviendas de la Gran Misión Vivienda Venezuela, el Comité Multifamiliar de Gestión, el cual se regirá por las cláusulas que se identifican a continuación, redactadas con suficiente amplitud, apoyadas en las leyes del Poder Popular, para que sirvan a su vez como Acta Constitutiva y Estatutos Sociales.<p>

<p align="center"><b>CAPITULO I </b><p>
<p align="center"><b>DISPOSICIONES GENERALES</b><p>
<p ALIGN="justify"><b>CLÁUSULA PRIMERA:</b> Los presentes Estatutos tienen como fundamento legal la Ley del Régimen de Propiedad de las Viviendas de la Gran Misión Vivienda Venezuela, Gaceta Oficial N° 6.021, del 6 de abril de 2011, la Ley Orgánica de los Consejos Comunales, Gaceta Oficial N° 39.335, del 28 de diciembre de 2009  y como fundamento marco y legal la Ley Orgánica del Poder Popular, publicada en la Gaceta Oficial de la República Bolivariana de Venezuela Nº 6.011, de fecha 21 de diciembre de 2010, inspirándose en la doctrina del Libertador Simón Bolívar y en los principios y valores socialistas de democracia participativa y protagónica, interés colectivo, equidad, justicia, igualdad social y de género, complementariedad, diversidad cultural, defensa de los derechos humanos, corresponsabilidad, cogestión, autogestión, cooperación, solidaridad, honestidad, transparencia, eficacia, eficiencia, efectividad, universalidad, responsabilidad, deber social, rendición de cuentas, control social,  libre debate de ideas, sustentabilidad, defensa y protección ambiental, garantía de los derechos de la mujer, de niños, niñas y adolescentes y de toda persona en situación de vulnerabilidad, defensa de la integridad territorial y de la soberanía nacional.<br><br> <b>CLÁUSULA SEGUNDA:</b> Las normas establecidas en la presente Acta, el Reglamento de Convivencia Ciudadana que a tales efecto dicte la Asamblea General de Adjudicatarios del urbanismo, las disposiciones contempladas en la Ley del Régimen de Propiedad de las Viviendas de la Gran Misión Vivienda Venezuela, la Ley Orgánica de los Consejos Comunales y las de la Ley Orgánica del Poder Popular, serán de obligatorio cumplimiento por parte de todos los voceros y las voceras que integran el Comité Multifamiliar de Gestión y por los ciudadanos de la comunidad en general.</p>

<p align="center"><b>CAPITULO II</b><p>
<p align="center"><b>DENOMINACIÓN, OBJETO Y ESTRUCTURA</b><p>
<p ALIGN="justify"><b>CLÁUSULA TERCERA:</b> DENOMINACIÓN. El Comité Multifamiliar de Gestión se denominará “'.strtoupper($datotorre->nom_comite).'”, y se encuentra ubicado en la siguiente dirección:'.$datourbanismo->direccion.'.<br>

<br><br><b>CLÁUSULA CUARTA:</b> OBJETO. El Comité Multifamiliar de Gestión conformado mediante el presente documento, tiene por objeto ser el órgano de decisión y de análisis de los problemas comunes que surjan en la comunidad habitacional, y ser los promotores, defensores y  cuidadores de la propiedad multifamiliar que reposa en la comunidad de adjudicatarios, mediante el manejo responsable, transparente y eficiente de los recursos aportados por estos para su conservación y mantenimiento. <br><br><b>CLÁUSULA QUINTA:</b> FUNCIONES. El Comité Multifamiliar de Gestión tendrá las siguientes funciones: a) Convocar a los adjudicatarios a las reuniones para discutir asuntos concernientes a la comunidad vecinal. b) Ejercer las funciones de administración de los recursos necesarios para el mantenimiento y buen funcionamiento de las instalaciones comunes a las edificaciones a las que se refiere esta ley. c) Establecer los mecanismos necesarios para lograr la participación protagónica de cada uno de los integrantes de los grupos vecinales. d) Fijar los aportes indispensables para el buen funcionamiento de las edificaciones a las que se refiere la ley, y los mecanismos para lograr la efectividad de tales pagos, aprobados por la Asamblea de Adjudicatarios. e) Velar por el buen uso que se haga de las cosas comunes y adoptar las normas de convivencia que fueren necesarias para la comunidad. f) Trabajar coordinadamente y articular con los demás voceros y voceras que conforman los demás comités, mesas técnicas y en general con las demás vocerías que conforman el Colectivo de Coordinación Comunitaria del Consejo Comunal, de conformidad con la Ley Orgánica de los Consejos Comunales, artículos 24 y 25. g) Diseñar y someter a la aprobación de la Asamblea de Adjudicatarios el Plan de Conservación y Mantenimiento de las Áreas Comunes y de Servicio. h) Conducir el proceso participativo de elaboración del Reglamento de Convivencia Ciudadana aludido en estos estatutos y en la Ley y someterlo a su aprobación por la Asamblea General de Adjudicatarios. <br><br><b>CLÁUSULA SEXTA:</b> AMBITO DE ACTUACIÓN ESPACIAL. El Comité Multifamiliar de Gestión tendrá como ámbito de actuación la (las) Torre(s) '.$cars[$a].', sector, cuadra, terraza, manzana '.$datourbanismo->sector.', calle '.$datourbanismo->calle.', del urbanismo '.$datourbanismo->urbanismo_nombre.', el cual ha sido ratificado en esta Asamblea General, no aceptándose la constitución de otro Comité Multifamiliar de Gestión dentro del ámbito de actuación espacial aquí identificado. Debiendo funcionar articuladamente con los demás comités multifamiliares de gestión que existan y con los comités articulados en el Órgano Ejecutivo y en el Colectivo de Coordinación Comunitaria del Consejo Comunal del urbanismo.<br>
<br><br><b>CLÁUSULA SEPTIMA:</b> ESTRUCTURA. A los fines de su funcionamiento, el Comité Multifamiliar de Gestión estará conformado al menos por tres (tres) voceros principales y sus respectivos suplentes con las siguientes responsabilidades: un (1) vocero principal con su suplente para ejercer funciones de organización y articulación; un (1) vocero principal con su suplente para ejercer funciones de administración, conservación e infraestructura y; un (1) vocero principal con su suplente para ejercer funciones de formación y de convivencia ciudadana. Estos deberán ser adjudicatarios de las unidades familiares y serán designados con el 51% de los votos, en donde por cada unidad familiar corresponderá un voto.<br><br><b>CLÁUSULA OCTAVA:</b> RESPONSABILIDADES DE LOS VOCEROS DE ORGANIZACIÓN Y ARTICULACIÓN: Promover la organización, participación y movilización de los ciudadanos de la comunidad en asuntos que conciernen a los intereses de la comunidad y de la patria; Convocar a las Asamblea de Adjudicatarios; Preparar la agenda y llevar la dirección de las asambleas; Llevar las actas de asambleas y el manejo de los medios de convocatoria. <br><br><b>CLÁUSULA NOVENA:</b> RESPONSABILIDADES DE LOS VOCEROS DE ADMINISTRACIÓN, CONSERVACIÓN E INFRAESTRUCTURA. Llevar los registros contables con sus respectivos soportes que demuestren los ingresos y egresos requeridos para el mantenimiento y conservación de las áreas comunes y de servicio; consignar ante la Unidad de Contraloría Social del Consejo Comunal, el comprobante de declaración jurada de patrimonio de todos los voceros que conforman el Comité Multifamiliar de Gestión; Administrar en corresponsabilidad con los demás voceros principales del Comité Multifamiliar de Gestión los fondos creados para cumplir con los fines planteados previa aprobación de la Asamblea; preparar los informes y los mecanismos necesarios para realizar la rendición de cuentas ante la Asamblea; dirigir en corresponsabilidad con los demás voceros el Plan de Conservación y Mantenimiento de Áreas Comunes y de Servicio; preparar los proyectos de reparación, recuperación y de servicio que sean necesarios para el funcionamiento de los equipos, máquinas e infraestructuras a ser presentados ante la Asamblea.   <br><br><b>CLÁUSULA DÉCIMA:</b> RESPONSABILIDADES DE LOS VOCEROS DE FORMACIÓN Y CONVIVENCIA CIUDADANA. Promover el buen vivir mediante la convivencia ciudadana, la formación integral y las actividades culturales y deportivas del urbanismo, en articulación con los demás comités relacionados del Consejo Comunal;  organizar el voluntariado social como escuela generadora de conciencia y activadora del deber social; motivar y organizar a la comunidad para formarse y organizarse en torno a proyectos sociales, comunitarios y socioproductivos que beneficien a la colectividad. </p>
<p align="center"><b>CAPITULO III</b><p>
<p align="center"><b>DE LOS VOCEROS Y VOCERAS DEL COMITÉ MULTIFAMILIAR DE GESTIÓN</b><p>
<p ALIGN="justify"><b>CLÁUSULA DÉCIMO PRIMERO:</b> DEFINICION. Partiendo de la finalidad de garantizar el buen vivir y el bienestar social y colectivo de la comunidad de adjudicatarios; sin discriminaciones por motivos de origen étnico, religioso, condición social, sexo, orientación sexual, identidad y expresión de género, idioma, opinión política, nacionalidad u origen, edad, posición económica, condición de discapacidad o cualquier otra circunstancia personal, jurídica o social, que tenga por resultado anular o menoscabar el reconocimiento, goce o ejercicio de los derechos y garantías constitucionales, los voceros y voceras del Comité Multifamiliar de Gestión como instancia comprendida dentro del Poder Popular, se definen como los ciudadanos con la condición de adjudicatarios beneficiados por la Gran Misión Vivienda Venezuela, que resulten electos mediante proceso de elección popular en Asamblea General de Adjudicatarios del urbanismo, para ejercer y coordinar las funciones antes identificadas por mandato de esta Asamblea. <br><br><b>CLÁUSULA DÉCIMO SEGUNDO:</b> REQUISITOS. Para postularse como vocero o vocera del Comité Multifamiliar de Gestión se hará conforme a la Ley del Régimen de Propiedad de la Gran Misión Vivienda Venezuela en cuanto a la durabilidad de las vocerías y en la condición de adjudicatario que debe tener el postulante; y conforme a la Ley Orgánica de los Consejos Comunales de acuerdo a lo establecido en su Artículo 15, numerales 2, 5, 6, 7, 8, 9, 10 y 11. <br><br><b>CLÁUSULA DÉCIMO TERCERO:</b> DEBERES. Son deberes de los voceros y voceras del Comité Multifamiliar de Gestión; la solvencia moral ante la comunidad y la sociedad; la defensa de la soberanía nacional y de los intereses de la patria; el apego estricto a las leyes del Poder Popular; impuso de las formas e instancias de organización promovidas por la Ley Orgánica del Poder Popular, la disciplina, la participación ciudadana, la solidaridad, la integración, la ayuda mutua, la corresponsabilidad social; la rendición de cuentas permanente apegadas a la legalidad; el manejo transparente, oportuno y eficaz de los recursos aportados por la comunidad de adjudicatarios para el mantenimiento y conservación de la propiedad multifamiliar. <br><br><b>CLÁUSULA DÉCIMO CUARTO:</b> CARÁCTER VOLUNTARIO. El ejercicio de las funciones de los voceros y voceras del Comité Multifamiliar de Gestión tendrá carácter voluntario y se desarrollará con espíritu unitario y compromiso con los intereses de la comunidad y de la patria. <br><br><b>CLÁUSULA DÉCIMO QUINTO:</b> DURACIÓN Y REELECCIÓN. Los voceros y voceras del Comité Multifamiliar de Gestión tendrán un (1) año en sus funciones, contados a partir del momento de su elección y podrán ser reelectos o reelectas. <br><br><b>CLÁUSULA DÉCIMO SEXTO:</b> DEFINICIÓN DE LA REVOCATORIA. Se entiende por revocatoria, la separación definitiva del ejercicio de las funciones de los voceros o voceras del Comité Multifamiliar de Gestión por estar incurso en alguna de las causales de revocatoria establecidas en estos estatutos y en la Ley. Procedimiento que debe ser decidido en Asamblea General de Adjudicatarios una vez se reúnan los elementos de prueba suficiente para ello. <br><br><b>CLÁUSULA DÉCIMO SEPTIMO:</b> CAUSALES DE REVOCATORIA. Los voceros o voceras del Comité Multifamiliar de Gestión, podrán ser revocados o revocadas por la Asamblea de Adjudicatarios, siempre que se encuentren incursos en alguna de las causales siguientes: 1. Actuar de forma contraria a las decisiones tomadas por la Asamblea de Adjudicatarios o el Colectivo de Coordinación Comunitaria del Consejo Comunal. 2. Incumplimiento a las funciones identificadas en estos estatutos  y en la Ley del Régimen de Propiedad de las Viviendas de la Gran Misión Vivienda Venezuela. 3. Omisión o negativa por parte de los voceros o voceras del Comité Multifamiliar de Gestión a presentar la rendición de cuenta de los recursos dirigidos a la conservación y mantenimiento de la propiedad multifamiliar. 4. Representar y/o negociar individualmente asuntos propios del Comité Multifamiliar de Gestión o del consejo comunal que  corresponda decidir a la Asamblea. 5. La no rendición de cuentas permanentes o en el momento exigido por la Asamblea de Adjudicatarios. 6. Incurrir en malversación, apropiación, desviación de los recursos asignados, captados o aportados para el cumplimiento de las funciones conferidas por estos estatutos y la ley, así como cualquier otro delito previsto  en  la  Ley  Contra  la  corrupción  y  el   ordenamiento  jurídico   en materia penal. 7. Omisión en la presentación o falsedad comprobada en los datos de la declaración jurada de patrimonio de inicio y cese de funciones. 8. Actuar de forma contraria a moral y las buenas costumbres y a las normas establecidas en el Reglamento de Convivencia Ciudadana aprobado por la Asamblea. </p>
<br><br><br>
    <p align="center"><b>VOCEROS PRINCIPALES ELECTOS</b><p>

    <table style="border:1px solid black; border-collapse:collapse;width:100%">
    <tr >
    <td style="border:1px solid black;" >Nº</td>
    <td style="border:1px solid black;">APELLIDOS Y NOMBRES</td>
    <td style="border:1px solid black;">CÉDULA</td>
    <td style="border:1px solid black;">APTO.</td>
    <td style="border:1px solid black;">TELEFONO</td>
    <td style="border:1px solid black;">VOTOS</td>
    </tr>';
    $i=1;
foreach ($datovoceroo as $key => $value) {
if($datovoceroo[$key]->tipo=='ENCARGADO           ' || $datovoceroo[$key]->tipo=='PRINCIPAL           ' ){
  
$html.='<tr>
    <td style="border:1px solid black;" >'.$i.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->nombre1.' '.$datovoceroo[$key]->apellido1.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->cedula.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->apartamento.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->telefono1.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->votos.'</td>
    </tr>';
    $i++;
    }
}

$html.='</table>
<p align="center"><b>VOCEROS SUPLENTES ELECTOS</b><p>

    <table style="border:1px solid black; border-collapse:collapse;width:100%">
    <tr >
    <td style="border:1px solid black;" >Nº</td>
    <td style="border:1px solid black;">APELLIDOS Y NOMBRES</td>
    <td style="border:1px solid black;">CÉDULA</td>
    <td style="border:1px solid black;">APTO.</td>
    <td style="border:1px solid black;">TELEFONO</td>
    <td style="border:1px solid black;">VOTOS</td>
    </tr>';
  $i=1;
    foreach ($datovoceroo as $key => $value) {
if($datovoceroo[$key]->tipo=='SUPLENTE            '){

$html.='<tr>
    <td style="border:1px solid black;" >'.$i.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->nombre1.' '.$datovoceroo[$key]->apellido1.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->cedula.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->apartamento.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->telefono1.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->votos.'</td>
    </tr>';
    $i++;
    }
}
    
$html.='</table><br>
<p ALIGN="justify"><b>PRIMERA:</b> de acuerdo a la voluntad expresada por la Asamblea General de Adjudicatarios de conformidad con lo establecido en los Artículos 62 y 70 de la Constitución de la República Bolivariana de Venezuela, las personas seleccionadas como cuentadantes del Comité Multifamiliar de Gestión ante la Banca Pública y Privada, son los tres voceros que quedaron como voceros principales, los cuales son los siguientes:</p>

<p align="center"><b>VOCEROS PRINCIPALES ELECTOS</b><p>

    <table style="border:1px solid black; border-collapse:collapse;width:100%">
    <tr >
    <td style="border:1px solid black;" >Nº</td>
    <td style="border:1px solid black;">APELLIDOS Y NOMBRES</td>
    <td style="border:1px solid black;">CÉDULA</td>
    <td style="border:1px solid black;">APTO.</td>
    <td style="border:1px solid black;">TELEFONO</td>
    <td style="border:1px solid black;">VOTOS</td>
    </tr>
    ';
 $i=1;
foreach ($datovoceroo as $key => $value) {
if($datovoceroo[$key]->tipo=='ENCARGADO           ' || $datovoceroo[$key]->tipo=='PRINCIPAL           ' ){
 
$html.='<tr>
    <td style="border:1px solid black;" >'.$i.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->nombre1.' '.$datovoceroo[$key]->apellido1.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->cedula.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->apartamento.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->telefono1.'</td>
    <td style="border:1px solid black;">'.$datovoceroo[$key]->votos.'</td>
    </tr>';
    $i++;
    }
}



    $html.='</table> <br>
<p ALIGN="justify"><b>SEGUNDA:</b> Esta Asamblea General de Adjudicatarios autoriza, al ciudadano(a) '.strtoupper($datosurb->nombre1).'  '.strtoupper($datosurb->apellido1).', titular de la cedula de Identidad N°'.$datosurb->cedula.', en su carácter de ciudadano beneficiado  de la comunidad de adjudicatarios del urbanismo '.strtoupper($datourbanismo->urbanismo_nombre).', para que solicite el Registro del presente documento, por ante el Ministerio de de Redes Populares en Vivienda y Fundacomunal a través de la Taquilla de Registro. Se levanta el acta,  los presentes adjudicatarios firman en señal de conformidad y aceptación de su contenido:</p>
<br><br><br>

<table style="border:1px solid black; border-collapse:collapse;width:100%">
    <tr >
    <td style="border:1px solid black;" >Nº</td>
    <td style="border:1px solid black;">APELLIDOS Y NOMBRES</td>
    <td style="border:1px solid black;">CÉDULA</td>
    <td style="border:1px solid black;">APTO.</td>
    <td style="border:1px solid black;">TELEFONO</td>
    <td style="border:1px solid black;">VOTOS</td>
    </tr>
';

for($j=1;$j<65;$j++){

$html.='
    <tr >
    <td style="border:1px solid black;">'.$j.'</td>
    <td style="border:1px solid black;">  </td>
    <td style="border:1px solid black;">  </td>
    <td style="border:1px solid black;">  </td>
    <td style="border:1px solid black;">  </td>
    <td style="border:1px solid black;">  </td>
    </tr>
';
}
$html.='
</table> 
 <div style="margin-left:520px;"><br><br><barcode code="'.$url.'" type="QR" class="barcode" size="1.2" error="M" /></div>
';

 
 $footer='Autorizada mediante resolución Nº DGT-R-48-2016 del 7 de octubre de 2016';

?>
<?php 
  if($vista == 1)
  {
    echo '<div class="form">';
    ?>
<div class="printable">
        <?php echo $header.$html.$footer; ?>
      </div>
      <!-- Cierre de la clase para imprimir  -->
    
    <div style ="align:center; position:fixed;bottom:0;">
        <table>
              <tr>
                <td>
  
  
    <?php 
            $this->widget('zii.widgets.jui.CJuiButton', array(
                                'cssFile'=> Yii::app()->request->baseUrl.'/css/jquery.css',
                    'buttonType'=>'button',
                                'name'=>'Imprimir',
                    'caption'=>'Imprimir',
                    'onclick'=>'js:function(){$(".printable").print();}',
                  
    
          ));?>
          </td>
  
      </tr>
    </table>
        </div>
        <?php 
        echo '</div>';
  }
  else
  {
    
    $pdf = Yii::createComponent('application.extensions.mpdf53.mpdf');
    //$mpdf=new mPDF('c',array(100,200),10,10,10,22,15,9);
    $mpdf=new mPDF('c','A4','',10,15,10,10,15,9);
    $mpdf->SetHTMLHeader($header);
    $mpdf->SetHTMLFooter($footer);
    $mpdf->SetTitle('Factura N#'.$factura->factura);
    $mpdf->SetFooter(' |Pg{PAGENO}/{nbpg}|');
    $mpdf->WriteHTML($html);
   
    $mpdf->Output('Factura.pdf','I'); 
    exit;
  }
 // }  
?>