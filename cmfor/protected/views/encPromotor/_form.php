<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style>
	.row_campos_detalles div,.row_campos_detalles input,.row_campos_detalles select{padding:0px;margin:0px;border-radius:0px;height:20px;}.boton_menos,.boton_mas{height:20px;padding:2px 3px 2px 3px;}#th_button_nuevo{text-align:center;width:80px;}.td_botones button{float:left;}.div_botones_listar{}.div_botones_listar button{margin-left:3px;}.ui-datepicker-month, .ui-datepicker-year{color:#333;font-weight: 900;}.row{margin-top: 20px;margin-bottom: 20px;}
	label.btn span {font-size: 1em}
	label input[type="radio"] ~ i.fa.fa-circle-o{color: #c8c8c8; display: inline}
	label input[type="radio"] ~ i.fa.fa-dot-circle-o{display: none}
	label input[type="radio"]:checked ~ i.fa.fa-circle-o{display: none}
	label input[type="radio"]:checked ~ i.fa.fa-dot-circle-o{color: #7AA3CC;display: inline;}label:hover input[type="radio"] ~ i.fa {color: #7AA3CC}
	label input[type="checkbox"] ~ i.fa.fa-square-o{color: #c8c8c8;display: inline}
	label input[type="checkbox"] ~ i.fa.fa-check-square-o{display: none}
	label input[type="checkbox"]:checked ~ i.fa.fa-square-o{display: none}
	label input[type="checkbox"]:checked ~ i.fa.fa-check-square-o{color: #7AA3CC;display: inline}
	label:hover input[type="checkbox"] ~ i.fa {color: #7AA3CC}
	.has-error div[data-toggle="buttons"] label.active{color: #7AA3CC}
	div[data-toggle="buttons"] label.active{color: #7AA3CC}
	div[data-toggle="buttons"] label:hover {color: #7AA3CC}
	
	.has-error div[data-toggle="buttons"] label {
		color: #a94442;
	}
	.fa-2x{
		font-size: 1em;
	}
	div[data-toggle="buttons"] label { 
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 20px;
	font-weight: normal;
	line-height: 2em;
	text-align: left;
	white-space: nowrap;
	vertical-align: top;
	cursor: pointer;
	background-color: none;
	border: 0px solid #c8c8c8;
	border-radius: 3px;
	color: #c8c8c8;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	-o-user-select: none;
	user-select: none;
	}
	div[data-toggle="buttons"] label:active, div[data-toggle="buttons"] label.active {
	-webkit-box-shadow: none;
	box-shadow: none;
	}
	span.required {
		color:red;
	}
	.help-block {
		font-size: 20px;
	}
	p.stop:hover{
		color:black;
	}
</style>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'enc-promotor-form',
	'enableAjaxValidation'=>false,
)); ?>



<fieldset>
	<div class=" col-md-12"> 
	<legend align="center">DATOS DEL URBANISMO</legend>

	<div class=" col-md-3">
			
		<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'id_urbanismo',
				'id'=>'nombre',
				'value'=>$datourbanismo->urbanismo_nombre
				),true);
		?>

	
			
		</div>

			<div class=" col-md-3">
			
		<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'etapa',
				'id'=>'nombre',
				'value'=>$datosurb->etapa
				),true);
		?>
	
			
		</div>

			<div class=" col-md-3">
			
		<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,
				'atributo'=>'ubigeo',
				'id'=>'nombre', 
				'value'=>$datourbanismo->ubigeo
				),true);
		?>
	
			
		</div>



</div>
<?php $verurb=SerEqVia::model()->findAll('enc_urbanismo_id='.$datosurb->id_urbanismo);  
$verurb2=OrganizacionesUrb::model()->findAll('enc_urbanismo_id='.$datosurb->id_urbanismo);
$reporte=EncTorre::model()->findAll('id_promotor='.$datosurb->enc_promotor_id); 

?>



   <ul class="nav nav-tabs col-md-offset-1" >
   	<?php if(!$verurb2){ ?>
    <li  class="col col-md-3 col-xs-12 active"><a data-toggle="tab" href="#organizac1" id="organiz" style="color:#22C7FC; font-size: 17px;" ><strong>ORGANIZACIÓN</strong></a></li>
    <?php } ?>
    <?php if(!$verurb){ ?>
    <li class="col col-md-3 col-xs-12"><a data-toggle="tab" href="#urbanismo1" id="urbaniz" style="color: #22C7FC; font-size: 17px;"><strong> URBANISMO</strong> </a></li>
    <?php } ?>
    <li class="col col-md-3 col-xs-12"><a data-toggle="tab" href="#voceros1"  id="vocero" style="color: #22C7FC; font-size: 17px;"><strong>VOCEROS</strong></a></li>

     <?php if($reporte){ ?>
    <li class="col col-md-3 col-xs-12"><a data-toggle="tab" href="#reporte1" id="reporte" style="color: #22C7FC; font-size: 17px;"><strong> REPORTES</strong> </a></li>
    <?php } ?>
    </ul>
   


 <div class="tab-content">
<?php if($reporte){ ?>
<div id="reporte1" class="tab-pane fade  ">
	<div class=" col-md-12"> 

<?php if($datosurb->torre1){ ?> 
	<div class=" col-md-4">

 <?php $torreid1=EncTorre::model()->find('id_promotor=:user and nom_torre=:torr',array(':user'=>(int)$datosurb->enc_promotor_id,':torr'=>(string)$datosurb->torre1)); 
 ?>

	<a  id="g1" value="<?php echo $datosurb->enc_promotor_id ?>,<?php echo $torreid1->enc_torre_id ?>" title="Ver Registro" > <img align="center"  style="width:50%; margin-left:20% " src="<?php echo Yii::app()->request->baseUrl; ?>/images/torre.png" /></a>
	<p style="color:black " align="center">torre <?php echo $datosurb->torre1 ?></p>	
</div>
<?php } ?>

	<?php if($datosurb->torre2){ ?> 
<?php $torreid2=EncTorre::model()->find('id_promotor=:user and nom_torre=:torr',array(':user'=>(int)$datosurb->enc_promotor_id,':torr'=>(string)$datosurb->torre2)); 

 ?>

	<div class=" col-md-4">
	<a  id="g2" value="<?php echo $datosurb->enc_promotor_id ?>,<?php echo $torreid2->enc_torre_id ?>" title="Ver Registro" ><img align="center"  style="width:50%; margin-left:20%  " src="<?php echo Yii::app()->request->baseUrl; ?>/images/torre.png" /></a>
	<p style="color:black " align="center">torre <?php echo $datosurb->torre2 ?></p>	
</div>
<?php } ?>

	<?php if($datosurb->torre3){ ?> 
<?php $torreid3=EncTorre::model()->find('id_promotor=:user and nom_torre=:torr',array(':user'=>(int)$datosurb->enc_promotor_id,':torr'=>(string)$datosurb->torre3)); 
 ?>


	<div class=" col-md-4">
	<a  id="g3" value="<?php echo $datosurb->enc_promotor_id ?>,<?php echo $torreid3->enc_torre_id ?>" title="Ver Registro" ><img align="center"  style="width:50%; margin-left:20%  " src="<?php echo Yii::app()->request->baseUrl; ?>/images/torre.png" /></a>
	<p style="color:black " align="center">torre <?php echo $datosurb->torre3 ?></p>	
</div>
<?php } ?>

	<?php if($datosurb->torre4){ ?> 
		<?php $torreid4=EncTorre::model()->find('id_promotor=:user and nom_torre=:torr',array(':user'=>(int)$datosurb->enc_promotor_id,':torr'=>(string)$datosurb->torre4)); 
 ?>
	<div class=" col-md-4">
	<a  id="g4" value="<?php echo $datosurb->enc_promotor_id ?>,<?php echo $torreid4->enc_torre_id ?>" title="Ver Registro" ><img align="center"  style="width:50%; margin-left:20%  " src="<?php echo Yii::app()->request->baseUrl; ?>/images/torre.png" /></a>
	<p style="color:black " align="center">torre <?php echo $datosurb->torre4 ?></p>	
</div>
<?php } ?>

	<?php if($datosurb->torre5){ ?> 
		<?php $torreid5=EncTorre::model()->find('id_promotor=:user and nom_torre=:torr',array(':user'=>(int)$datosurb->enc_promotor_id,':torr'=>(string)$datosurb->torre5)); 
 ?>
	<div class=" col-md-4">
	<a  id="g5" value="<?php echo $datosurb->enc_promotor_id ?>,<?php echo $torreid5->enc_torre_id ?>" title="Ver Registro" ><img align="center"  style="width:50%; margin-left:20%  " src="<?php echo Yii::app()->request->baseUrl; ?>/images/torre.png" /></a>
	<p style="color:black " align="center">torre <?php echo $datosurb->torre5 ?></p>	
</div>
<?php } ?>

	<?php if($datosurb->torre6){ ?> 
		<?php $torreid6=EncTorre::model()->find('id_promotor=:user and nom_torre=:torr',array(':user'=>(int)$datosurb->enc_promotor_id,':torr'=>(string)$datosurb->torre6)); 
 ?>
	<div class=" col-md-4">
	<a  id="g6" value="<?php echo $datosurb->enc_promotor_id ?>,<?php echo $torreid6->enc_torre_id ?>" title="Ver Registro" ><img align="center"  style="width:50%; margin-left:20%  " src="<?php echo Yii::app()->request->baseUrl; ?>/images/torre.png" /></a>
	<p style="color:black " align="center">torre <?php echo $datosurb->torre6 ?></p>	
</div>
<?php } ?>



</div>
</div>
<?php } ?>

 




<?php if(!$verurb2){ ?>
<div id="organizac1" class="tab-pane fade in active ">

<div class=" col-md-12"> 
<br>
		<legend align="center">ORGANIZACIONES DEL PODER POPULAR PRESENTES EN EL URBANISMO</legend>

<div id="tabular" name="<?php echo $tamaño;?>">

			
			<div id="" class="col-md-12" >
<fieldset>
	<legend id="sevcio<?php echo $i ?>"></legend>


<div class="col-md-4">
<?php $arcom = CHtml::listData(Mesas::model()->findAll(), 'nombre_mesa', 'nombre_mesa');?>
		<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$organizacion,
		'form'=>$form,											
		'atributo'=>'organizacion_comunal',
		'id'=>'Comite_mesas_id',
		'list'=>$arcom,),true);?>
								</div>


				<div class="form-group col-md-4">

			<?php echo "<span style='color:red'>*</span>";echo $form->labelEx($organizacion,"cedula");?>
			<div class="input-group">
				<div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="button_nacionalidad3"><?php echo ($organizacion->nacionalidad?$organizacion->nacionalidad:'V'); ?></span><span class="caret"></span></button>
					<ul class="dropdown-menu" role="menu">
						<li><a onclick="$('#resnac').val('V');$('#button_nacionalidad3').text('V');">V</a></li>
						<li><a onclick="$('#resnac').val('E');$('#button_nacionalidad3').text('E');">E</a></li>
					</ul>
				</div>
					<?php echo $form->textField($organizacion,"cedula",array('class'=>'form-control','maxlength'=>9,'id'=>'resci'));?>
				<span class="input-group-btn">
					<button onclick="buscarpersona3('<?php echo $i; ?>')" class="btn btn-primary" type="button">Buscar</button>
				</span>
			</div>
			
		</div>

		<div class="col-md-4">
		<?php echo $this->renderPartial('_fieldReadOnly',array(
											'model'=>$organizacion,
											'form'=>$form,
											'atributo'=>'nombre1',
											'id'=>'nbvcs'.$i.'',
											'size'=>'3',
											),true); 
									?>
								</div>
			
		<div class="col-md-4">
		<?php echo $this->renderPartial('_fieldReadOnly',array(
											'model'=>$organizacion,
											'form'=>$form,
											'atributo'=>'apellido1',
											'id'=>'apvcs'.$i.'',
											'size'=>'3',
											),true); 
									?>
								</div>


									<div class="col-md-3">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$organizacion,
											'form'=>$form,
											'atributo'=>'correo',
											'id'=>'corvcs'.$i.'',
											'onBlur'=>'correo("vocero")',
											'size'=>'3',
											),true); 
									?>
								</div>

											<div class="col-md-4">
			<?php echo $form->labelEx($organizacion,"telefono1");?>
			<div class="input-group">
		
				<div class="input-group-btn" style="width:100%">	

		<?php echo $this->renderPartial('_nfield',array(
											'model'=>$organizacion,
											'form'=>$form,
											'atributo'=>'telefono1',
											'id'=>'tlfvcs'.$i.'',
											'max'=>11,
											//'max'=>'telefono('.$i.')',
											'size'=>'3',
											),true); 
									?>
								</div>
								<button onclick="Agregar2()" class="btn btn-primary" type="button">Agregar</button>
							</div>
							</div>




</fieldset>
			</div>
		
</div>
	</div>
<legend align="center">DATOS DEL REPRESENTANTE </legend>
<table class=" table table-responsive table-striped table-bordered" id="organizacion_carga">
	<tr >
	<td style="color:black">NOMBRE </td>
	<td style="color:black">APELLIDO </td>
	<td style="color:black">Nº CÉDULA</td>
	<td style="color:black">Nº TELEFONO</td>
	<td style="color:black">CORREO ELECTRÓNICO</td>	
	<td style="color:black">MESA DE TRABAJO</td>
	<td style="color:black">ACCION</td>

	</tr> 

	

</table>

<br><br>
<div class="col-md-2 col-md-offset-5" id="sig"   >
	<a  data-toggle="tab" onclick="siguiente('urba')" class="btn btn-primary btn-lg " align="center">Siguiente</a> <!--  href="#urbanismo"  lo que quite-->
</div>
  </div>




<div class="col-md-2" style="display:none">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$organizacion,
											'form'=>$form,
											'atributo'=>'arrayci',
											'id'=>'arrayci',
											'size'=>'3',
											),true); 
									?>
								</div>

<div class="col-md-2" style="display:none">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$organizacion,
											'form'=>$form,
											'atributo'=>'arraynb',
											'id'=>'arraynb',
											'size'=>'3',
											),true); 
									?>
								</div>
			
<div class="col-md-2" style="display:none">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$organizacion,
											'form'=>$form,
											'atributo'=>'arrayap',
											'id'=>'arrayap',
											'size'=>'3',
											),true); 
									?>
								</div>


<div class="col-md-3" style="display:none">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$organizacion,
											'form'=>$form,
											'atributo'=>'arraycr',
											'id'=>'arraycr',
											'size'=>'3',
											),true); 
									?>
								</div>
<div class="col-md-2" style="display:none">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$organizacion,
											'form'=>$form,
											'atributo'=>'arraytlf',
											'id'=>'arraytlf',
											'size'=>'3',
											),true); 
									?>
								</div>

								<div class="col-md-2" style="display:none">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$organizacion,
											'form'=>$form,
											'atributo'=>'arraymesa',
											'id'=>'arraymesa',
											'size'=>'3',
											),true); 
									?>
								</div>

 <?php } ?>



 
<div id="urbanismo1" class="tab-pane fade  "> 

<div class=" col-md-12"> 
	<br>
	<fieldset>
	<legend align="center"> DATOS DEL URBANISMO SERVICIOS, EQUIPAMIENTO Y VIALIDAD</legend>

<legend > SERVICIO DE AGUA POTABLE</legend>


			<div class="col-md-3"> 

	<?php $arcom = CHtml::listData(Respuesta::model()->findAll('id_respuesta is not null order by id_respuesta DESC'), 'id_respuesta', 'nombre_mesa');
	?>
		

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'acueducto',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'pozo_profundo',
		'list'=>$arcom,),true);?>			
		</div>

<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'tanque',
		'list'=>$arcom,),true);?>			
		</div>

		<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'camión_tanque',
		'list'=>$arcom,),true);?>			
		</div>

	</fieldset>
</div>

<div class=" col-md-12"> 
	<fieldset>
<legend > AGUAS SERVIDAS</legend>

<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'red_cloacas',
		'list'=>$arcom,),true);?>			
		</div>

		<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'laguna_oxidación',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'planta_aguas_servidas',
		'list'=>$arcom,),true);?>			
		</div>

	<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'pozo_séptico',
		'list'=>$arcom,),true);?>			
		</div>

</fieldset>
</div>

<div class=" col-md-12"> 
	<fieldset>
<legend > ELECTRICIDAD</legend>

	<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'aerea',
		'list'=>$arcom,),true);?>			
		</div>

		<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'subterranea',
		'list'=>$arcom,),true);?>			
		</div>	

</fieldset>
</div>

<div class=" col-md-12"> 
	<fieldset>
<legend > SERVICIO DE GAS</legend>

		<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'por_tubería',
		'list'=>$arcom,),true);?>			
		</div>

		<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'subterranea',
		'list'=>$arcom,),true);?>			
		</div>

		<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'tanque_almacenamiento',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'bombona_individual',
		'list'=>$arcom,),true);?>			
		</div>

</fieldset>
</div>

<div class=" col-md-12"> 
	<fieldset>
<legend > SERVICIO DE TRANSPORTE</legend>

		<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'metro',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'metrobus',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'autobuses',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'camionetas',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'rustico',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'ferrocarril',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'ninguno',
		'list'=>$arcom,),true);?>			
		</div>


</fieldset>
</div>

<div class=" col-md-12"> 
	<fieldset>
<legend > VIALIDAD INTERNA</legend>

<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'avenidas_calles_internas',
		'onchange'=>'avenidas_calles_internas()',
		'id'=>'avenidas_calles_internas1',
		'list'=>$arcom,),true);?>			
		</div>

		<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'aceras_brocales',
		'onchange'=>'aceras_brocales()',
		'id'=>'aceras_brocales1',
		'list'=>$arcom,),true);?>			
		</div>

		<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'vialidad_acceso_urbanismo',
		'onchange'=>'vialidad_acceso_urbanismo()',
		'id'=>'vialidad_acceso_urbanismo1',
		'list'=>$arcom,),true);?>			
		</div>

</fieldset>
</div>

<div class=" col-md-12"> 
	<fieldset>
<legend > EQUIPAMIENTO URBANO </legend>

		<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'parques_plazas',
		'onchange'=>'Parques_Plazas()',
		'id'=>'parques_plazas1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'canchas_deportivas',
		'onchange'=>'canchas_deportivas()',
		'id'=>'canchas_deportivas1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'educativa_prescolar',
		'onchange'=>'educativa_prescolar()',
		'id'=>'educativa_prescolar1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'centros_religiosos',
		'onchange'=>'centros_religiosos()',
		'id'=>'centros_religiosos1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'educativa_básica_media',
		'onchange'=>'educativa_básica_media()',
		'id'=>'educativa_básica_media1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'escuela_técnica_robinsoniana',
		'onchange'=>'escuela_técnica_robinsoniana()',
		'id'=>'escuela_técnica_robinsoniana1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'infocentro',
		'onchange'=>'infocentro()',
		'id'=>'infocentro1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'locales_industria',
		'onchange'=>'locales_industria()',
		'id'=>'locales_industria1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'locales_comercios',
		'onchange'=>'locales_comercios()',
		'id'=>'locales_comercios1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'centros_comunicación',
		'onchange'=>'centros_comunicación()',
		'id'=>'centros_comunicación1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'centros_salud',
		'onchange'=>'centros_salud()',
		'id'=>'centros_salud1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'centro_diagnóstico',
		'onchange'=>'centro_diagnóstico()',
		'id'=>'centro_diagnóstico1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'módulos_policiales',
		'onchange'=>'módulos_policiales()',
		'id'=>'módulos_policiales1',
		'list'=>$arcom,),true);?>			
		</div>

				<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'casa_comunitaria',
		'onchange'=>'casa_comunitaria()',
		'id'=>'casa_comunitaria1',
		'list'=>$arcom,),true);?>			
		</div>



						<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'centros_culturales',
		'onchange'=>'centros_culturales()',
		'id'=>'centros_culturales1',
		'list'=>$arcom,),true);?>			
		</div>

								<div class="col-md-3"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$servicios,
		'form'=>$form,											
		'atributo'=>'centros_electorales',
		'onchange'=>'centros_electorales()',
		'id'=>'centros_electorales1',
		'list'=>$arcom,),true);?>			
		</div>

</fieldset>
</div>

<div class=" col-md-12"> 
<?php $arcom = CHtml::listData(Respuesta2::model()->findAll(), 'id_respuesta', 'nombre_mesa');
	?>

	<fieldset>
	<legend align="center">  MANTENIMIENTO DEL EQUIPAMIENTO URBANO Y VIALIDAD</legend>
<legend > CONSERVACIÓN DEL EQUIPAMIENTO URBANO</legend>

<div class="col-md-3" id="parques_plazas2" style="display:none "> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'parques_plazas',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="canchas_deportivas"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'canchas_deportivas',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="educativa_prescolar"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'educativa_prescolar',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="educativa_básica_media"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'educativa_básica_media',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="escuela_técnica_robinsoniana"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,							 				
		'atributo'=>'escuela_tecnica_robinsoniana',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="infocentro"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'infocentro',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="locales_industria"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'locales_industria',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="locales_comercios"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'locales_comercios',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="centros_comunicación"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'centros_comunicación',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="centros_salud"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'centros_salud',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="centro_diagnóstico"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'centro_diagnóstico',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="módulos_policiales"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'módulos_policiales',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="centros_religiosos"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'centros_religiosos',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="centros_culturales"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'centros_culturales',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="centros_electorales"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'centros_electorales',
		'list'=>$arcom,),true);?>			
		</div>

					<div class="col-md-3" style="display:none " id="casa_comunitaria"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'casa_omunitaria',
		'list'=>$arcom,),true);?>			
		</div>

	</fieldset>
</div>


<div class=" col-md-12"> 
	<fieldset>
<legend > ESTADO DE CONSERVACIÓN DE LA VIALIDAD </legend>

					<div class="col-md-3" style="display:none " id="avenidas_calles_internas"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'avenidas_calles_internas',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="aceras_brocales"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'aceras_brocales',
		'list'=>$arcom,),true);?>			
		</div>

			<div class="col-md-3" style="display:none " id="vialidad_acceso_urbanismo"> 

	<?php echo $this->renderPartial('_dropDown',array(
		'model'=>$mant_servicios,
		'form'=>$form,											
		'atributo'=>'vialidad_acceso_urbanismo',
		'list'=>$arcom,),true);?>			
		</div>

</fieldset>
<br><br>
<div class="col-md-2 col-md-offset-5"   >
	<a  data-toggle="tab" onclick="siguiente('voce')"  class="btn btn-primary btn-lg " align="center">Siguiente</a><!-- href="#mes" a onclick -->
</div>
</div>
</div>

<div id="voceros1" class="tab-pane fade  ">
<div class="form-group col-md-12">
	<?php $j=0; ?>
	<br>
<legend align="center"> DATOS DE LOS VOCEROS </legend>

<div class="form-group col-md-3">
	<label for="EncVocero_cedula" class="required">Torre</label>
<select placeholder="--SELECCIONE--" class="form-control" style="width:60%;" name="OrganizacionesUrb[cedula]" id="p_o_s" onchange="torre()">
<option value="">--SELECCIONE--</option>
<?php if($datosurb->torre1){  ?>
<?php $datotorress=EncTorre::model()->find('id_urbanismo=:user and nom_torre=:torre and nr_etapa=:etapa ',array(':user'=>(int)$datosurb->id_urbanismo,':torre'=>(string)$datosurb->torre1,':etapa'=>$datosurb->etapa)); if(!$datotorress->nom_torre){ ?>
<option value="<?php echo $datosurb->torre1 ?>"><?php echo $datosurb->torre1 ?></option>
<?php $cars[$j]=$datosurb->torre1; $j++; }} ?>
<?php if($datosurb->torre2){  ?>
	<?php $datotorress=EncTorre::model()->find('id_urbanismo=:user and nom_torre=:torre and nr_etapa=:etapa ',array(':user'=>(int)$datosurb->id_urbanismo,':torre'=>(string)$datosurb->torre2,':etapa'=>$datosurb->etapa)); if(!$datotorress->nom_torre){ ?>
<option value="<?php echo $datosurb->torre2 ?>"><?php echo $datosurb->torre2 ?></option>
<?php $cars[$j]=$datosurb->torre2; $j++; }} ?>
<?php if($datosurb->torre3){  ?>
	<?php $datotorress=EncTorre::model()->find('id_urbanismo=:user and nom_torre=:torre and nr_etapa=:etapa ',array(':user'=>(int)$datosurb->id_urbanismo,':torre'=>(string)$datosurb->torre3,':etapa'=>$datosurb->etapa)); if(!$datotorress->nom_torre){ ?>
<option value="<?php echo $datosurb->torre3 ?>"><?php echo $datosurb->torre3 ?></option>
<?php  $cars[$j]=$datosurb->torre3; $j++;}} ?>
<?php if($datosurb->torre4){  ?>
	<?php $datotorress=EncTorre::model()->find('id_urbanismo=:user and nom_torre=:torre and nr_etapa=:etapa ',array(':user'=>(int)$datosurb->id_urbanismo,':torre'=>(string)$datosurb->torre4,':etapa'=>$datosurb->etapa)); if(!$datotorress->nom_torre){ ?>
<option value="<?php echo $datosurb->torre4 ?>"><?php echo $datosurb->torre4 ?></option>
<?php $cars[$j]=$datosurb->torre4; $j++; }} ?>
<?php if($datosurb->torre5){  ?>
	<?php $datotorress=EncTorre::model()->find('id_urbanismo=:user and nom_torre=:torre and nr_etapa=:etapa ',array(':user'=>(int)$datosurb->id_urbanismo,':torre'=>(string)$datosurb->torre5,':etapa'=>$datosurb->etapa)); if(!$datotorress->nom_torre){ ?>
<option value="<?php echo $datosurb->torre5 ?>"><?php echo $datosurb->torre5 ?></option>
<?php $cars[$j]=$datosurb->torre5; $j++; }} ?>
<?php if($datosurb->torre6){  ?>
	<?php $datotorress=EncTorre::model()->find('id_urbanismo=:user and nom_torre=:torre and nr_etapa=:etapa ',array(':user'=>(int)$datosurb->id_urbanismo,':torre'=>(string)$datosurb->torre6,':etapa'=>$datosurb->etapa)); if(!$datotorress->nom_torre){ ?>
<option value="<?php echo $datosurb->torre6 ?>"><?php echo $datosurb->torre6 ?></option>
<?php $cars[$j]=$datosurb->torre6; $j++; }} ?>
</select>
</div>
</div>

<?php  for($i=0;$i<count($cars);$i++){?>

		

<div class=" col-md-12" style="display:none"  id="<?php echo"torre".$cars[$i]?>"> 
	<fieldset>
<div class="form-group col-md-4">

		<?php echo $this->renderPartial('_nfield',array(
											'model'=>$torre,
											'form'=>$form,
											'atributo'=>'cant_pisos',
											'id'=>'cant_pisos'.$cars[$i],
											'size'=>'3',
											'max'=>2,
											),true); 
									?>
</div>
<div class="form-group col-md-4">

<?php echo $this->renderPartial('_nfield',array(
											'model'=>$torre,
											'form'=>$form,
											'atributo'=>'apartamento_pisos',
											'id'=>'apartamento_pisos'.$cars[$i],
											'size'=>'3',
											'max'=>2,

											),true); 
									?>

</div>

<div class="form-group col-md-4">

<?php echo $this->renderPartial('_field',array(
											'model'=>$torre,
											'form'=>$form,
											'atributo'=>'nom_comite',
											'id'=>'nom_comite'.$cars[$i],
											'size'=>'3',
											'max'=>30,
											),true); 
									?>

</div>

				<div class="form-group col-md-3">

			<?php echo "<span style='color:red'>*</span>";echo $form->labelEx($model_vocero,"cedula");?>
			<div class="input-group">
				<div class="input-group-btn">
					<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="button_nacionalidad4<?php echo $cars[$i]?>"><?php echo ($model_vocero->nacionalidad?$model_vocero->nacionalidad:'V'); ?></span><span class="caret"></span></button>
					<ul class="dropdown-menu" role="menu">
						<li><a onclick="$('#resnac').val('V');$('#button_nacionalidad4'+<?php echo CJSON::encode($cars[$i]) ?>).text('V');">V</a></li>
						<li><a onclick="$('#resnac').val('E');$('#button_nacionalidad4'+ <?php echo CJSON::encode($cars[$i]) ?>).text('E');">E</a></li>
					</ul>
				</div>
					<?php echo $form->textField($model_vocero,"cedula",array('class'=>'form-control','id'=>'cedu'.$cars[$i],'onBlur'=>'cedula2('.$cars[$i].')','maxlength'=>9,));?>
				<span class="input-group-btn">
					<button onclick="buscarpersona4('<?php echo $cars[$i]; ?>')" class="btn btn-primary" type="button">Buscar</button>
				</span>
			</div>
			
		</div>
		<div class="col-md-3">
		<?php echo $this->renderPartial('_fieldReadOnly',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'nombre1',
											'id'=>'nombre1'.$cars[$i],
											'size'=>'3',
											),true); 
									?>
								</div>
			
												<div class="col-md-3">
		<?php echo $this->renderPartial('_fieldReadOnly',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'apellido1',
											'id'=>'apellido1'.$cars[$i],
											'size'=>'3',
											),true); 
									?>
								</div>


									<div class="col-md-3">
		<?php echo $this->renderPartial('_emailfield',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'correo',
											'onBlur'=>'correo('.$cars[$i].')',
											'id'=>'correo1'.$cars[$i],
											'size'=>'3',
											),true); 
									?>
								</div>
								
		<div class="col-md-3">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'telefono1',
											'id'=>'telefono1'.$cars[$i],
											'maxlength'=>11,
											'onBlur'=>'telefono2('.$cars[$i].')',
											'size'=>'3',
											'max'=>11,

											),true); 
									?>
								</div>

				<div class="col-md-3">
		<?php echo $this->renderPartial('_nfield',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'votos',
											'id'=>'votos'.$cars[$i],
											'size'=>'3',
											'max'=>3
											),true); 
									?>
								</div>

						<div class="col-md-3">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'apartamento',
											'id'=>'apartamento'.$cars[$i],
											'size'=>'3',
											'max'=>10,
											),true); 
									?>
								</div>										

								<div class="input-group col-md-3" >
<label for="MesaServ_apvc">Tipo de Vocero:</label>
<div>

<select placeholder="--SELECCIONE--" class="form-control" style="width:60%"  id="p_o_s<?php echo $cars[$i]?>">
<option value="">--SELECCIONE--</option>
<option value="PRINCIPAL">PRINCIPAL</option>
<option value="SUPLENTE">SUPLENTE</option>
<option value="ENCARGADO">ENCARGADO</option>
</select> 

<span class="input-group-btn">
<button onclick="Agregar('<?php echo $cars[$i]?>')" class="btn btn-primary" type="button">Agregar</button>
</span>
</div>
</div >
<div class="col-md-12" id="productotal">
	<br>
	<h5 style="color: red"> Nota: debe
agregar un (01) vocero encargado, dos (02)
principales y tres (03) suplentes, el vocero
Encargado es el delegado para el registro de la
torre.</h5>
<fieldset>

<legend align="center">VOCEROS PRINCIPALES Y SUPLENTES</legend>

<table class=" table table-responsive table-striped table-bordered" id="voceros_carga<?php echo $cars[$i]?>">
	<tr >
	<td style="color:black">ESTATUS DE VOCERO</td>
	<td style="color:black">NOMBRE </td>
	<td style="color:black">APELLIDO </td>
	<td style="color:black">Nº CÉDULA</td>
	<td style="color:black">Nº TELEFONO</td>
	<td style="color:black">CORREO ELECTTÓNICO</td>	
	<td style="color:black">VOTOS</td>	
	<td style="color:black">APARTAMENTO</td>	
	<td style="color:black">ACCION</td>	

	</tr>

	

</table>

</fieldset>
</div>
</fieldset>
</div>

<?php }?>
<div class="col-md-2 col-md-offset-5"   >
	<br><br><br><br>
<button type="submit" class="btn btn-primary btn-lg btn-block" >Registrar</button>
</div> 
	</div>
</div>
		<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$torre,
											'form'=>$form,
											'atributo'=>'arraycant_pisos',
											'id'=>'arraycant_pisos',
											'size'=>'3',
											),true); 
									?>
								</div>

								<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$torre,
											'form'=>$form,
											'atributo'=>'arrayapartamento_pisos',
											'id'=>'arrayapartamento_pisos',
											'size'=>'3',
											),true); 
									?>
								</div>

							<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$torre,
											'form'=>$form,
											'atributo'=>'arraynom_torre',
											'id'=>'arraynom_torre',
											'size'=>'3',
											),true); 
									?>
								</div>	

		<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'arrayvoce_tipi',
											'id'=>'arrayvoce_tipi',
											'size'=>'3',
											),true); 
									?>
								</div>

		<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'arrayvoce_nac',
											'id'=>'arrayvoce_nac',
											'size'=>'3',
											),true); 
									?>
								</div>






								
								<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'arrayvoce_nom',
											'id'=>'arrayvoce_nom',
											'size'=>'3',
											),true); 
									?>
								</div>	

								<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'arrayvoce_ape',
											'id'=>'arrayvoce_ape',
											'size'=>'3',
											),true); 
									?>
								</div>
								<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'arrayvoce_cedu',
											'id'=>'arrayvoce_cedu',
											'size'=>'3',
											),true); 
									?>
								</div>
								<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'arrayvoce_tel',
											'id'=>'arrayvoce_tel',
											'size'=>'3',
											),true); 
									?>
								</div>	

								<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'arrayvoce_corr',
											'id'=>'arrayvoce_corr',
											'size'=>'3',
											),true); 
									?>
								</div>

								<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'arrayvoce_torr',
											'id'=>'arrayvoce_torr',
											'size'=>'3',
											),true); 
									?>
								</div>

								<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$torre,
											'form'=>$form,
											'atributo'=>'arraynom_comite',
											'id'=>'arraynom_comite',
											'size'=>'3',
											),true); 
									?>
								</div>	

								<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'arrayvoce_apto',
											'id'=>'arrayvoce_apto',
											'size'=>'3',
											),true); 
									?>
								</div>

								<div class="col-md-2" style="display:none ">
		<?php echo $this->renderPartial('_field',array(
											'model'=>$model_vocero,
											'form'=>$form,
											'atributo'=>'arrayvoce_votos',
											'id'=>'arrayvoce_votos',
											'size'=>'3',
											),true); 
									?>
								</div>

																								



<?php $this->endWidget(); ?>

</fieldset>


<script >

$("#g1").click(function() {
	var campo= $('#g1').attr('value');
	var res = campo.split(",");
	var $tor=res[1];
	var $pro=res[0];

		if(!$tor){
		swal("Atencion!","no se encontraron datos","error");
		}else{

			location.href='index.php?r=encPromotor/Reportepromotor2&id='+$pro+'&tor='+$tor;
		}

	});

$("#g2").click(function() {
	var campo= $('#g2').attr('value');
	var res = campo.split(",");
	var $tor=res[1];
	var $pro=res[0];

		if(!$tor){
		swal("Atencion!","no se encontraron datos","error");
		}else{

			location.href='index.php?r=encPromotor/Reportepromotor2&id='+$pro+'&tor='+$tor;
		}

	});
$("#g3").click(function() {
	var campo= $('#g3').attr('value');
	var res = campo.split(",");
	var $tor=res[1];
	var $pro=res[0];

		if(!$tor){
		swal("Atencion!","no se encontraron datos","error");
		}else{

			location.href='index.php?r=encPromotor/Reportepromotor2&id='+$pro+'&tor='+$tor;
		}

	});
$("#g4").click(function() {
	var campo= $('#g4').attr('value');
	var res = campo.split(",");
	var $tor=res[1];
	var $pro=res[0];

		if(!$tor){
		swal("Atencion!","no se encontraron datos","error");
		}else{

			location.href='index.php?r=encPromotor/Reportepromotor2&id='+$pro+'&tor='+$tor;
		}

	});
$("#g5").click(function() {
	var campo= $('#g5').attr('value');
	var res = campo.split(",");
	var $tor=res[1];
	var $pro=res[0];

		if(!$tor){
		swal("Atencion!","no se encontraron datos","error");
		}else{

			location.href='index.php?r=encPromotor/Reportepromotor2&id='+$pro+'&tor='+$tor;
		}

	});
$("#g6").click(function() {
	var campo= $('#g6').attr('value');
	var res = campo.split(",");
	var $tor=res[1];
	var $pro=res[0];

		if(!$tor){
		swal("Atencion!","no se encontraron datos","error");
		}else{

			location.href='index.php?r=encPromotor/Reportepromotor2&id='+$pro+'&tor='+$tor;
		}

	});


	$(document).ready(function() {
		
		$("#buscarpersona").click(function() {
			var ci = $("#ci").val();
			var nac= $("#button_nacionalidad").val();
	
			$.ajax({
				url:"<?php echo $this->createUrl('Cedula'); ?>",
				cache: false,
				type: "POST",
				dataType: "json",
				data: ({cedula:ci,nacionalidad:nac}),
			
				success: function(data){

					console.log(data);
					if(data.nacionalidad!='') {
					$('#GrGmvvT_nacionalidad').val(data.nacionalidad);
					$('#button_nacionalidad').text(data.nacionalidad);
					}
					$('#ci').val(data.cedula);
					$('#nacion').val(data.nacionalidad);
					$('#nombre').val(data.nombre1);
					$('#nb2vcr').val(data.nombre2);
					$('#ape_vocero').val(data.apellido1);
					$('#ap2vcr').val(data.apellido2);

					$('#GrGmvvT_segundo_apellido').val(data.apellido2);
					$('#GrGmvvT_mensaje').val(data.mensaje);

				}
			});
		
		});
		$("#buscarpersona2").click(function() {
			var ci = $("#jfc_ci").val();
			var nac= $("#button_nacionalidad2").val();
	
			$.ajax({
				url:"<?php echo $this->createUrl('Cedula'); ?>",
				cache: false,
				type: "POST",
				dataType: "json",
				data: ({cedula:ci,nacionalidad:nac}),
			
				success: function(data){

					console.log(data);
					if(data.nacionalidad!='') {
						$('#button_nacionalidad2').val(data.nacionalidad);
						$('#button_nacionalidad2').text(data.nacionalidad);
					}
					$('#jfc_ci').val(data.cedula);
					$('#nacion2').val(data.nacionalidad);
					$('#jfc_nombre').val(data.nombre1);
					$('#jfc_apellido').val(data.apellido1);
					$('#nb2jfc').val(data.nombre2);
					$('#ap2jfc').val(data.apellido2);

				}
			});
		
		});

	});


</script>
<script>




			function buscarpersona3($id){
			var ci = $("#resci"+$id).val();
		
			var nac= $("#button_nacionalidad3"+$id).text();
			$.ajax({
				url:"<?php echo $this->createUrl('Cedula'); ?>",
				cache: false,
				type: "POST",
				dataType: "json",
				data: ({cedula:ci,nacionalidad:nac}),
			
				success: function(data){
					console.log(data);
					
					if(data != 1){
					$('#resci'+$id).val(data.cedula);
					$('#nbvcs'+$id).val(data.nombre1);
					$('#apvcs'+$id).val(data.apellido1);
					
				}else{
					swal("Atencion!", "No se encontaron los datos","error");
					$('#resci'+$id).val('');
					$('#nbvcs'+$id).val('');
					$('#apvcs'+$id).val('');
				}
					
				}
			});
		
		};

	   function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }
 

    function soloNumeros(e){

	var key = window.Event ? e.which : e.keyCode

	return (key >= 48 && key <= 57)

}
	function asociar() {


		var mesa = $('#Comite_id_urbanismo').val();	
		alert(mesa);
		event.preventDefault();
		/*$.ajax({
				url:"<?php echo $this->createUrl('Serv'); ?>",
				cache: false,
				type: "POST",
				dataType: "json",
				data: ({mesa:mesa}),
			
				success: function(data){
					
					
					var nombre = (data.nombre);
					$('#sevcio'+mesa).text(nombre);
				}
			});*/

		for(var i=1 ; i<=$tmn ; i++){

			if(i==mesa){
				$('#'+i+'').show('fast');
				
			}else{
				$('#'+i+'').hide();
			}
	}
		};


$("#enc-promotor-form").submit(function( event ) {

 var tmn = $('#tabular').attr('name');
 var arraymesas = "";
 var arrayci = "";
 var arraynb = "";
 var arrayap = "";
 var arraycr = "";
 var arraytlf = "";


var arlene2 = <?php echo CJSON::encode($cars)?>;
var tamaño=arlene2.length;
var no_lleno=0;



for(var i=0;i<tamaño;i++){
	var interno=$('#voceros_carga'+arlene2[i]+' tr').length;

	var encargado=0;
	var principal=0;
	var suplente=0;
	
	if(interno>1){

  for(var j=1;j<interno;j++){

var tipotabla= $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(0).text();

if(tipotabla=='PRINCIPAL'){
principal++;
}
else if(tipotabla=='SUPLENTE'){
suplente++;
}
else if(tipotabla=='ENCARGADO'){
encargado++;
}

  }

if(encargado < 1){
	swal("Atencion!", "falta un encargado en la torre "+arlene2[i]+"","error");
	event.preventDefault();
            return false;
}
if(principal < 2){
	swal("Atencion!", "falta vocero principal en la torre "+arlene2[i]+"","error");
	event.preventDefault();
            return false;
}
if(suplente < 3){
	swal("Atencion!", "falta vocero suplente en la torre "+arlene2[i]+"","error");
	event.preventDefault();
            return false;
}
}else{
	 no_lleno++;
}

}

if(no_lleno==tamaño){
	swal("Atencion!", "debe cargar almenos una torre","error");
	event.preventDefault();
            return false;
}



var tmorg=$('#organizacion_carga tr').length;
 for (i=1;i<tmorg;i++){

if(arraymesas==''){
	arraymesas=$('#organizacion_carga tr').eq(''+i+'').find('td').eq(0).text();
}else{
	arraymesas= arraymesas+","+ $('#organizacion_carga tr').eq(''+i+'').find('td').eq(0).text();
}

if(arrayci==''){
	arrayci=$('#organizacion_carga tr').eq(''+i+'').find('td').eq(1).text(); 
}else{
	arrayci= arrayci+","+ $('#organizacion_carga tr').eq(''+i+'').find('td').eq(1).text();
}

if(arraynb==''){
	arraynb=$('#organizacion_carga tr').eq(''+i+'').find('td').eq(2).text(); 
}else{
	arraynb= arraynb+","+ $('#organizacion_carga tr').eq(''+i+'').find('td').eq(2).text();
}

if(arrayap==''){
	arrayap=$('#organizacion_carga tr').eq(''+i+'').find('td').eq(3).text(); 
}else{
	arrayap= arrayap+","+ $('#organizacion_carga tr').eq(''+i+'').find('td').eq(3).text();
}

if(arraycr==''){
	arraycr=$('#organizacion_carga tr').eq(''+i+'').find('td').eq(4).text(); 
}else{
	arraycr= arraycr+","+ $('#organizacion_carga tr').eq(''+i+'').find('td').eq(4).text();
}

if(arraytlf==''){
	arraytlf=$('#organizacion_carga tr').eq(''+i+'').find('td').eq(5).text(); 
}else{
	arraytlf= arraytlf+","+ $('#organizacion_carga tr').eq(''+i+'').find('td').eq(5).text();
}

		
}
 	
 $('#arrayci').val(arrayci);
 $('#arraynb').val(arraynb);
 $('#arrayap').val(arrayap);
 $('#arraycr').val(arraycr);
 $('#arraytlf').val(arraytlf);
 $('#arraymesa').val(arraymesas);

var arlene2 = <?php echo CJSON::encode($cars)?>;
var tamaño=arlene2.length;

var arraycant_pisos='';
var arrayapartamento_pisos='';
var arraynom_torre='';
var arraynom_comite='';

for(var i=0;i<tamaño;i++){

if(arraycant_pisos==''){
	arraycant_pisos=$('#cant_pisos'+arlene2[i]).val(); 
}else{
	arraycant_pisos= arraycant_pisos+","+ $('#cant_pisos'+arlene2[i]).val();
}

if(arrayapartamento_pisos==''){
	arrayapartamento_pisos=$('#apartamento_pisos'+arlene2[i]).val(); 
}else{

	arrayapartamento_pisos=arrayapartamento_pisos+","+$('#apartamento_pisos'+arlene2[i]).val();
}

if(arraynom_torre==''){
	arraynom_torre=arlene2[i]; 
}else{

	arraynom_torre=arraynom_torre+","+arlene2[i];
}

if(arraynom_comite==''){
	arraynom_comite=$('#nom_comite'+arlene2[i]).val(); 
}else{

	arraynom_comite=arraynom_comite+","+$('#nom_comite'+arlene2[i]).val();
}

 
}

$('#arraycant_pisos').val(arraycant_pisos);

$('#arrayapartamento_pisos').val(arrayapartamento_pisos);

$('#arraynom_torre').val(arraynom_torre);

$('#arraynom_comite').val(arraynom_comite);



var interno=$('#voceros_carga1 tr').length;

var arrayvoce_tipi='';
var arrayvoce_nom='';
var arrayvoce_ape='';
var arrayvoce_cedu='';
var arrayvoce_tel='';
var arrayvoce_corr='';
var arrayvoce_torr='';
var arrayvoce_nac='';

var arrayvoce_apto='';
var arrayvoce_votos='';


for(var i=0;i<tamaño;i++){
	var interno=$('#voceros_carga'+arlene2[i]+' tr').length;
  for(var j=1;j<interno;j++){

if(arrayvoce_tipi==''){
arrayvoce_tipi=$('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(0).text();

}else{
if( $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(0).text() !=''){
arrayvoce_tipi=arrayvoce_tipi+","+ $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(0).text();
}
}

if(arrayvoce_nom==''){
arrayvoce_nom=$('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(1).text();
}else{

if( $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(1).text() !=''){	
arrayvoce_nom=arrayvoce_nom+","+ $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(1).text();

}
}

if(arrayvoce_ape==''){
arrayvoce_ape=$('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(2).text();
}else{
if($('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(2).text() != ''){

arrayvoce_ape=arrayvoce_ape+","+ $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(2).text();
}
}

if(arrayvoce_cedu==''){
arrayvoce_cedu=$('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(3).text();
}else{
	if($('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(0).text() != ''){
arrayvoce_cedu=arrayvoce_cedu+","+ $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(3).text();
}
}

if(arrayvoce_tel==''){
arrayvoce_tel=$('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(4).text();
}else{
	if($('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(4).text() != ''){}
arrayvoce_tel=arrayvoce_tel+","+ $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(4).text();
}

if(arrayvoce_corr==''){
arrayvoce_corr=$('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(5).text();
}else{
	if($('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(5).text() != ''){
arrayvoce_corr=arrayvoce_corr+","+ $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(5).text();
}

}

if(arrayvoce_torr==''){
arrayvoce_torr=$('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(6).text();
}else{
	if($('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(6).text() != ''){
arrayvoce_torr=arrayvoce_torr+","+ $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(6).text();
}

}


if(arrayvoce_votos==''){
arrayvoce_votos=$('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(7).text();
}else{
	if($('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(7).text() != ''){
arrayvoce_votos=arrayvoce_votos+","+ $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(7).text();
}

}

if(arrayvoce_apto==''){
arrayvoce_apto=$('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(8).text();
}else{
	if($('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(8).text() != ''){
arrayvoce_apto=arrayvoce_apto+","+ $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(8).text();
}

}

if(arrayvoce_nac==''){
arrayvoce_nac=$('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(9).text();
}else{
	if($('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(9).text() != ''){
arrayvoce_nac=arrayvoce_nac+","+ $('#voceros_carga'+arlene2[i]+' tr').eq(''+j+'').find('td').eq(9).text();
}

}


		  }

		}




		$('#arrayvoce_tipi').val(arrayvoce_tipi);
		$('#arrayvoce_nom').val(arrayvoce_nom);
		$('#arrayvoce_ape').val(arrayvoce_ape);
		$('#arrayvoce_cedu').val(arrayvoce_cedu);
		$('#arrayvoce_tel').val(arrayvoce_tel);
		$('#arrayvoce_corr').val(arrayvoce_corr);
		$('#arrayvoce_torr').val(arrayvoce_torr);
		$('#arrayvoce_nac').val(arrayvoce_nac);


		$('#arrayvoce_votos').val(arrayvoce_votos);
		$('#arrayvoce_apto').val(arrayvoce_apto);




});

function torre() {

var mesa=$('#p_o_s').val();

for(var i=1 ; i<=1000 ; i++){

			if(i==mesa){
				$('#torre'+i).show('fast');
			
			}else{
				$('#torre'+i).hide();
			}

		}
		}

</script>




<script>

	var id_tabular=1;
					function buscarpersona4($id){
				var ci = $("#cedu"+$id).val();

				var nac= $("#button_nacionalidad4"+$id).text();
		
		
				$.ajax({
					url:"<?php echo $this->createUrl('Cedula4'); ?>",
					cache: false,
					type: "POST",
					dataType: "json",
					data: ({cedula:ci,nacionalidad:nac}),
				
					success: function(data){
	if(data!=1){
	if(data.usuario==1||data.vocero==1||data.Promotor==1){

		swal("Atencion!", "Esta cedula ya se encuentra regitrada en sistema.","error");
						$('#cedu'+$id).val('');
						$('#nombre1'+$id).val('');
						$('#apellido1'+$id).val('');
	}else{

						$('#cedu'+$id).val(data.cedula);
						$('#nombre1'+$id).val(data.nombre1);
						$('#apellido1'+$id).val(data.apellido1);

	}					
		}else{
	swal("Atencion!", "No se encontaron los datos","error");
			$('#cedu'+$id).val('');
						$('#nombre1'+$id).val('');
						$('#apellido1'+$id).val('');
		}				
						
					}
				});
			
			};

	function Agregar($id){
	var duplicada=0;
		var tipo=$('#p_o_s'+$id).val();	
		var nom=$('#nombre1'+$id).val();
		var ape=$('#apellido1'+$id).val();	
		var cedula=$('#cedu'+$id).val();	
		var tele=$('#telefono1'+$id).val();
		var correo=$('#correo1'+$id).val();
		var votos=$('#votos'+$id).val();;
		var apartamento=$('#apartamento'+$id).val();
		var nac= $("#button_nacionalidad4"+$id).text();



	if($('#correo1'+$id).val().indexOf('@', 0) == -1 || $('#correo1'+$id).val().indexOf('.', 0) == -1) {
	            swal("Atencion!", "El correo electrónico introducido no es correcto.","error");
	            return false;
	        }

	        if(tipo=='') {
	            swal("Atencion!","Debe especificar si el vocero es (PRINCIPAL) o (SUPLENTE).","error");
	            return false;
	        }
	        if(nom=='') {
	            swal("Atencion!", "Debe seleccionar un vocero.","error");
	            return false;
	        }
	        if(cedula=='') {
	            swal("Atencion!", "Debe seleccionar un vocero.","error");
	            return false;
	        }
	        if(tele=='') {
	            swal("Atencion!", "Debe especificar un telefono.","error");
	            return false;
	        }
	        if(votos=='') {
	            swal("Atencion!", "Debe especificar una cantidad de votos.","error");
	            return false;
	        }
	        if(apartamento=='') {
	            swal("Atencion!", "Debe especificar un apartamento.","error");
	            return false;
	        }


	     
	var vueltas= $('#productotal table tr').length;

	for(var i=1;i<vueltas;i++){
	var cedulatabla= $('#productotal table tr').eq(''+i+'').find('td').eq(3).text();

	if(cedulatabla==cedula){
		duplicada=1;
		break;
	}
	}


	var vueltas= $('#voceros_carga'+$id+' tr').length;
	var encargado=0;
	var principal=0;
	var suplente=0;

	for(var i=1;i<vueltas;i++){
	var tipotabla= $('#voceros_carga'+$id+' tr').eq(''+i+'').find('td').eq(0).text();

	if(tipotabla=='PRINCIPAL'){
	principal++;
	}
	else if(tipotabla=='SUPLENTE'){
	suplente++;
	}
	else if(tipotabla=='ENCARGADO'){
	encargado++;
	}

	}

	if(encargado==1 && tipo == 'ENCARGADO'){

		swal("Atencion!", "llego al limite de encargados por torre.","error");
	            return false;

	}
	if(principal==2 && tipo == 'PRINCIPAL'){
		swal("Atencion!", "llego al limite de voceros principales por torre.","error");
	            return false;

	}
	if(suplente==3 && tipo == 'SUPLENTE'){
		swal("Atencion!", "llego al limite de voceros suplentes por torre.","error");
	            return false;

	}


	for(var i=1;i<vueltas;i++){
	var apartamentos= $('#voceros_carga'+$id+' tr').eq(''+i+'').find('td').eq(8).text();
	if(apartamento==apartamentos){
		swal("Atencion!", "ya existe un miembro del comite en este apartamento.","error");
	            return false;
	}

}


	if(duplicada !=1){
	if(tipo!='' && correo!='' && tele!='' && nom!='' && votos!='' && apartamento!=''||cedula!=''){	 					
	

	var agrega='<tr id="'+id_tabular+'"><td style="color:black">'+tipo+'</td><td style="color:black">'+nom+'</td><td style="color:black">'+ape+'</td><td style="color:black">'+cedula+'</td><td style="color:black">'+tele+'</td><td style="color:black">'+correo+'</td><td style="display:none">'+$id+'</td><td style="color:black">'+votos+'</td><td style="color:black">'+apartamento+'</td><td style="color:black;display:none">'+nac+'</td>  <td ><a style="color:black" href=# onclick="eliminar('+id_tabular+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Eliminar" ></span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style="color:black" href=# onclick="modificar('+id_tabular+')" ><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="modificar" ></span></a></td></tr>';

		$('#voceros_carga'+$id).append(agrega);
		id_tabular++;
		 $('#p_o_s'+$id).val('');	
		$('#nombre1'+$id).val('');
		$('#apellido1'+$id).val('');	
		$('#cedu'+$id).val('');	
		$('#telefono1'+$id).val('');
		$('#correo1'+$id).val('');

		$('#votos'+$id).val('');
		$('#apartamento'+$id).val('');
	}else
	{
		if(tipo==''){swal("Atencion!", "Debe especificar si el vocero es (PRINCIPAL) o (SUPLENTE)","error");}
		else if(correo==''){swal("Atencion!", "Debe especificar el correo","error");}
		else if(tele==''){swal("Atencion!", "Debe especificar el telefono","error");}
		else if(nom==''){swal("Atencion!", "Debe especificar un vocero","error");}
		else if(apartamento==''){swal("Atencion!", "Debe especificar el apartamento del vocero","error");}
		else if(votos==''){swal("Atencion!", "Debe especificar los votos","error");}


		
	}
	}else{swal("Atencion!", "La cedula ya esta cargada","error");
		 		 $('#p_o_s'+$id).val('');	
				$('#nombre1'+$id).val('');
				$('#apellido1'+$id).val('');	
				$('#cedu'+$id).val('');	
				$('#telefono1'+$id).val('');
				$('#correo1'+$id).val('');
	 }
		};

		function eliminar($id){
			
		$('#'+$id).remove();

		};

		function modificar(id){
			
		var tipo= $('tr#'+id+'').find('td').eq(0).text();
		var nombre= $('tr#'+id+'').find('td').eq(1).text();
		var apellido= $('tr#'+id+'').find('td').eq(2).text();
		var cedula= $('tr#'+id+'').find('td').eq(3).text();
		var telefono= $('tr#'+id+'').find('td').eq(4).text();
		var correo= $('tr#'+id+'').find('td').eq(5).text();
		var $id= $('tr#'+id+'').find('td').eq(6).text();
		
		$('#p_o_s'+$id).val(tipo);	
		$('#nombre1'+$id).val(nombre);
		$('#apellido1'+$id).val(apellido);	
		$('#cedu'+$id).val(cedula);	
		$('#telefono1'+$id).val(telefono);
		$('#correo1'+$id).val(correo);

		$('#'+id).remove();

		};


	function Parques_Plazas(){
		var parques_plazas=$('#parques_plazas1').val();

			if(parques_plazas==1){
				$('#parques_plazas2').show('fast');
				
			}else if(parques_plazas==2){
				$('#parques_plazas2').hide();
			}
			else if(parques_plazas==3){
				$('#parques_plazas2').hide();
			}


	}

	function canchas_deportivas(){
		var canchas_deportivas=$('#canchas_deportivas1').val();

			if(canchas_deportivas==1){
				$('#canchas_deportivas').show('fast');
				
			}else if(canchas_deportivas==2){
				$('#canchas_deportivas').hide();
			}
			else if(canchas_deportivas==3){
				$('#canchas_deportivas').hide();
			}


	}

	function educativa_prescolar(){
		var educativa_prescolar=$('#educativa_prescolar1').val();

			if(educativa_prescolar==1){
				$('#educativa_prescolar').show('fast');
				
			}else if(educativa_prescolar==2){
				$('#educativa_prescolar').hide();
			}
			else if(educativa_prescolar==3){
				$('#educativa_prescolar').hide();
			}


	}	

	function centros_religiosos(){
		var centros_religiosos=$('#centros_religiosos1').val();

			if(centros_religiosos==1){
				$('#centros_religiosos').show('fast');
				
			}else if(centros_religiosos==2){
				$('#centros_religiosos').hide();
			}
			else if(centros_religiosos==3){
				$('#centros_religiosos').hide();
			}


	}	

	function educativa_básica_media(){
		var educativa_básica_media=$('#educativa_básica_media1').val();

			if(educativa_básica_media==1){
				$('#educativa_básica_media').show('fast');
				
			}else if(educativa_básica_media==2){
				$('#educativa_básica_media').hide();
			}
			else if(educativa_básica_media==3){
				$('#educativa_básica_media').hide();
			}


	}

	function escuela_técnica_robinsoniana(){
		var escuela_técnica_robinsoniana=$('#escuela_técnica_robinsoniana1').val();

			if(escuela_técnica_robinsoniana==1){
				$('#escuela_técnica_robinsoniana').show('fast');
				
			}else if(escuela_técnica_robinsoniana==2){
				$('#escuela_técnica_robinsoniana').hide();
			}
			else if(escuela_técnica_robinsoniana==3){
				$('#escuela_técnica_robinsoniana').hide();
			}


	}

		function infocentro(){
		var infocentro=$('#infocentro1').val();

			if(infocentro==1){
				$('#infocentro').show('fast');
				
			}else if(infocentro==2){
				$('#infocentro').hide();
			}
			else if(infocentro==3){
				$('#infocentro').hide();
			}


	}

	function locales_industria(){
		var locales_industria=$('#locales_industria1').val();

			if(locales_industria==1){
				$('#locales_industria').show('fast');
				
			}else if(locales_industria==2){
				$('#locales_industria').hide();
			}
			else if(locales_industria==3){
				$('#locales_industria').hide();
			}


	}	

	function locales_comercios(){
		var locales_comercios=$('#locales_comercios1').val();

			if(locales_comercios==1){
				$('#locales_comercios').show('fast');
				
			}else if(locales_comercios==2){
				$('#locales_comercios').hide();
			}
			else if(locales_comercios==3){
				$('#locales_comercios').hide();
			}


	}


	function centros_comunicación(){
		var centros_comunicación=$('#centros_comunicación1').val();

			if(centros_comunicación==1){
				$('#centros_comunicación').show('fast');
				
			}else if(centros_comunicación==2){
				$('#centros_comunicación').hide();
			}
			else if(centros_comunicación==3){
				$('#centros_comunicación').hide();
			}


	}

	function centros_salud(){
		var centros_salud=$('#centros_salud1').val();

			if(centros_salud==1){
				$('#centros_salud').show('fast');
				
			}else if(centros_salud==2){
				$('#centros_salud').hide();
			}
			else if(centros_salud==3){
				$('#centros_salud').hide();
			}


	}

	function centro_diagnóstico(){
		var centro_diagnóstico=$('#centro_diagnóstico1').val();

			if(centro_diagnóstico==1){
				$('#centro_diagnóstico').show('fast');
				
			}else if(centro_diagnóstico==2){
				$('#centro_diagnóstico').hide();
			}
			else if(centro_diagnóstico==3){
				$('#centro_diagnóstico').hide();
			}


	}

	function módulos_policiales(){
		var módulos_policiales=$('#módulos_policiales1').val();

			if(módulos_policiales==1){
				$('#módulos_policiales').show('fast');
				
			}else if(módulos_policiales==2){
				$('#módulos_policiales').hide();
			}
			else if(módulos_policiales==3){
				$('#módulos_policiales').hide();
			}


	}	

	function casa_comunitaria(){
		var casa_comunitaria=$('#casa_comunitaria1').val();

			if(casa_comunitaria==1){
				$('#casa_comunitaria').show('fast');
				
			}else if(casa_comunitaria==2){
				$('#casa_comunitaria').hide();
			}
			else if(casa_comunitaria==3){
				$('#casa_comunitaria').hide();
			}


	}

	function centros_culturales(){
		var centros_culturales=$('#centros_culturales1').val();

			if(centros_culturales==1){
				$('#centros_culturales').show('fast');
				
			}else if(centros_culturales==2){
				$('#centros_culturales').hide();
			}
			else if(centros_culturales==3){
				$('#centros_culturales').hide();
			}


	}

	function centros_electorales(){
		var centros_electorales=$('#centros_electorales1').val();

			if(centros_electorales==1){
				$('#centros_electorales').show('fast');
				
			}else if(centros_electorales==2){
				$('#centros_electorales').hide();
			}
			else if(centros_electorales==3){
				$('#centros_electorales').hide();
			}


	}

	function avenidas_calles_internas(){
		var avenidas_calles_internas=$('#avenidas_calles_internas1').val();

			if(avenidas_calles_internas==1){
				$('#avenidas_calles_internas').show('fast');
				
			}else if(avenidas_calles_internas==2){
				$('#avenidas_calles_internas').hide();
			}
			else if(avenidas_calles_internas==3){
				$('#avenidas_calles_internas').hide();
			}


	}

	function aceras_brocales(){
		var aceras_brocales=$('#aceras_brocales1').val();

			if(aceras_brocales==1){
				$('#aceras_brocales').show('fast');
				
			}else if(aceras_brocales==2){
				$('#aceras_brocales').hide();
			}
			else if(aceras_brocales==3){
				$('#aceras_brocales').hide();
			}


	}

	function vialidad_acceso_urbanismo(){
		var vialidad_acceso_urbanismo=$('#vialidad_acceso_urbanismo1').val();

			if(vialidad_acceso_urbanismo==1){
				$('#vialidad_acceso_urbanismo').show('fast');
				
			}else if(vialidad_acceso_urbanismo==2){
				$('#vialidad_acceso_urbanismo').hide();
			}
			else if(vialidad_acceso_urbanismo==3){
				$('#vialidad_acceso_urbanismo').hide();
			}


	}

	function correo($id){
		
		if($id=="vocero"){
		if($('#corvcs').val().indexOf('@', 0) == -1 || $('#corvcs').val().indexOf('.', 0) == -1) {
            swal("Atencion!", "El correo electrónico introducido no es correcto.","error");
            $('#corvcs').val('');
            return false;
        }
        }else{

        if($('#correo1'+$id).val().indexOf('@', 0) == -1 || $('#correo1'+$id).val().indexOf('.', 0) == -1) {
            swal("Atencion!", "El correo electrónico introducido no es correcto.","error");
            $('#correo1'+$id).val('');
            return false;
        }
}
	}



		function cedula(){

		var cedula=$('#resci').val();
		if(isNaN(cedula)){
		swal("Atencion!", "Solo aceptas numeros","error");
		$('#resci').val("");

}

	}
	

	function telefono($i){

	var cedula=$('#tlfvcs'+$i).val();

		if(isNaN(cedula)){
		swal("Atencion!", "Solo aceptas numeros","error");
		$('#tlfvcs'+$i).val("");

	}
}

function telefono2($i){

	var cedula=$('#telefono1'+$i).val();

		if(isNaN(cedula)){
		swal("Atencion!", "Solo aceptas numeros","error");
		$('#telefono1'+$i).val("");

	}
}


function cedula2($i){

		var cedula=$('#cedu'+$i).val();
		if(isNaN(cedula)){
		swal("Atencion!", "Solo aceptas numeros","error");
		$('#cedu'+$i).val("");

}

	}


var tabula2=1;
		function eliminarorga($id){


swal({
  title: "¿Estás seguro?",
  text: "Una vez eliminado, no podrá recuperar esta mesa!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    $('#organizacion'+$id).remove();
  } 
});
		}

function Agregar2(){

var cedula=$('#resci').val();
var nombre=$('#nbvcs').val();
var apellido=$('#apvcs').val();
var correo=$('#corvcs').val();
var telefono=$('#tlfvcs').val();
var organizacion=$('#Comite_mesas_id').val();
var nac= $("#button_nacionalidad3").text();

if(organizacion==""||telefono==""||correo==""||apellido==""||nombre==""||cedula==""){
	swal("Atencion!", "Debe llenar todos los campos","error");
}else{

var agrega='<tr id="organizacion'+tabula2+'"><td style="color:black">'+nombre+'</td><td style="color:black">'+apellido+'</td><td style="color:black">'+cedula+'</td><td style="color:black">'+telefono+'</td><td style="color:black">'+correo+'</td><td style="color:black">'+organizacion+'</td><td style="color:black;display:none">'+nac+'</td>  <td ><a style="color:black" href=# onclick="eliminarorga('+tabula2+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Eliminar" ></tr>';

		$('#organizacion_carga').append(agrega);

$('#resci').val("");
$('#nbvcs').val("");
$('#apvcs').val("");
$('#corvcs').val("");
$('#tlfvcs').val("");
$('#Comite_mesas_id').val("");
$("#button_nacionalidad3").text();
tabula2++;
}
}


function siguiente($id) {
  var x = document.getElementsByClassName("col col-md-3 col-xs-12");
  var t = x.length;
  console.log(x);
  console.log(t);
  if( $id=='urba' && t==3){
  	
  	console.log(x[0]);
  	x[0].setAttribute("class","col col-md-3 col-xs-12");
  	x[1].setAttribute("class","col col-md-3 col-xs-12 active");
  	
  	document.getElementById("organizac1").setAttribute("class","tab-pane fade");
  	document.getElementById("urbanismo1").setAttribute("class","tab-pane fade in active");
  	//document.getElementById("col col-md-3 col-xs-12 active").setAttribute("class","col col-md-3 col-xs-12"); 
  } else if( $id=='voce' && t==3){
	  	
	  	console.log(x[1]);
	  	x[1].setAttribute("class","col col-md-3 col-xs-12");
	  	x[2].setAttribute("class","col col-md-3 col-xs-12 active");
	  	
	  	document.getElementById("urbanismo1").setAttribute("class","tab-pane fade");
	  	document.getElementById("voceros1").setAttribute("class","tab-pane fade in active");
	  	//document.getElementById("col col-md-3 col-xs-12 active").setAttribute("class","col col-md-3 col-xs-12"); 
  } else if( $id=='urba' && t==2){
  	
  	console.log(x[0]);
  	x[0].setAttribute("class","col col-md-3 col-xs-12");
  	x[1].setAttribute("class","col col-md-3 col-xs-12 active");
  	
  	document.getElementById("organizac1").setAttribute("class","tab-pane fade");
  	document.getElementById("voceros1").setAttribute("class","tab-pane fade in active");
  	//document.getElementById("col col-md-3 col-xs-12 active").setAttribute("class","col col-md-3 col-xs-12"); 
  }
  
}

	</script>


	<?php if($_GET['carga']==1){ ?>  
  <script>
$( document ).ready(function() {

swal("Atencion!", "Datos guardados con exito","success");
});
</script>
  <?php } ?>