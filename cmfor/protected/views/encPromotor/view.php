<?php 
$estado = GeoEstado::model()->find('geo_estado_id='.$model->estado);
$municipio = GeoMunicipio::model()->find('geo_municipio_id='.$model->municipio);
$parroquia = GeoParroquia::model()->find('geo_parroquia_id='.$model->parroquia);
?>
<fieldset>
<table class=" table table-striped table-bordered" style="border-bottom:2px solid black;border-top:2px solid black;border-right:2px solid black;border-left:2px solid black">
	<tr ><th colspan="4" width="100%" ><p align="center" style="font-size: 20px"><b>Datos del Promotor </b></p></th></tr>
<tr>
	<td style="background:powderblue;color:black" align="center"><b>Cédula</b></td>
	<td style="background:powderblue;color:black" align="center"><b>Carnet De la Patria</b></td>
	<td style="background:powderblue;color:black" align="center"><b>Nombres</b></td>
	<td style="background:powderblue;color:black" align="center"><b>Apellidos</b></td>
</tr>
<tr>
	<td  align="center"> <?php echo $model->cedula; ?> </td>
	<td  align="center"> <?php echo $model->codigo_cdlp; ?> </td>
	<td  align="center"> <?php echo $model->nombre1 . " ".$model->nombre2 ; ?> </td>
	<td  align="center"> <?php echo $model->apellido1 . " ".$model->apellido2 ; ?> </td>
</tr>
<tr>
	<td style="background:powderblue;color:black" align="center"><b>Fecha nacimiento</b></td>
	<td style="background:powderblue;color:black" align="center"><b>Teléfono</b></td>
	<td style="background:powderblue;color:black" align="center"><b>Correo</b></td>	
	<td style="background:powderblue;color:black" align="center"><b>Estado</b></td>	
</tr>
<tr>	
	<td  align="center"> <?php echo $model->fecha_nac; ?> </td>
	<td  align="center"> <?php echo $model->telefono1; ?> </td>
	<td  align="center"> <?php echo $model->correo; ?> </td>
	<td  align="center"> <?php echo $estado->nombre; ?> </td>
</tr>
<tr>
	<td style="background:powderblue;color:black" align="center"><b>Municipio</b></td>
	<td style="background:powderblue;color:black" align="center"><b>Parroquia</b></td>
	<td  colspan="2" style="background:powderblue;color:black" align="center"><b>Direción</b></td>	
</tr>
<tr>
	<td align="center"> <?php echo $municipio->nombre; ?> </td>
	<td align="center"> <?php echo $parroquia->nombre; ?> </td>
	<td colspan="2" align="center"> <?php echo $model->direccion; ?> </td> 
</tr>
</table>
<br><br><br>
<div class="col-md-2 col-md-offset-10">
	<a href="javascript:window.history.back();"><button class="btn btn-primary"><span class="glyphicon glyphicon-arrow-left"></span> Volver atrás</button></a>
</div>
</fieldset>


