<?php
$this->breadcrumbs=array(
	'Enc Promotors',
);

$this->menu=array(
	array('label'=>'Create EncPromotor', 'url'=>array('create')),
	array('label'=>'Manage EncPromotor', 'url'=>array('admin')),
);
?>

<h1>Enc Promotors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
