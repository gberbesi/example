<fieldset>
	<?php $torre = EncTorre::model()->find('enc_torre_id=:torre',array('torre'=>$tor)) ;?>
	<legend>Apartamentos de la torre #<?php echo $torre->nom_torre; ?></legend>

<table class="table table-striped table-bordered">
	
<tr>
	<td style="color:black">Piso</td>
	<td style="color:black">Apartamento</td>
	<td style="color:black">Acción</td>
</tr>
<?php foreach ($aptos as $key => $value) { 
	
	$pisos2 = EncPiso::model()->find('enc_piso_id='.$aptos[$key]->piso_id);
	$persona = PerPersona::model()->find('apartamento_id='.$aptos[$key]->enc_apartamento_id);
	$idapto= $persona->apartamento_id;
	?>
	<tr>
		<td><?php echo  $pisos2->nu_piso; ?></td>
		<td><?php echo $aptos[$key]->nomesclatura; ?></td>
		<td><a data-toggle="tooltip" onclick="verFam(<?php echo $aptos[$key]->enc_apartamento_id; ?>,<?php echo $idapto;?>)" title="Ver Registro" ><i class=" glyphicon glyphicon-search" style="color:black;font-size:20px; \"></i></a></td>
</tr>

<?php } ?>

</table><br>
<br><br><br>
<div class="col-md-2 col-md-offset-10">
	<a href="javascript:window.history.back();"><button class="btn btn-primary"><span class="glyphicon glyphicon-arrow-left"></span> Volver atrás</button></a>
</div>
</fieldset>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
	
function verFam($id,$persona){

if(!$persona){
		swal("Atencion!", "Este Apartamento No ha Sido Cargado","warning");
		}else{

			location.href='index.php?r=encPromotor/Fam&id='+$id;
		}


}

</script>