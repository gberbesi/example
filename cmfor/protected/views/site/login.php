<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

 <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form_1.css" />   

<h1>Ingresar</h1>


<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Los campos con<span class="required">*</span> son requeridos.</p>
       <div class="borderfomulario" class="form" style=" padding-top:17px; width:500px;  margin: 0px auto; margin-bottom: 1.5em; padding-bottom: 7px; background: #fff ;">
	<div style="padding:0 25px;">
       
        <fieldset style="background-color:#f4f3f3">
        <table width="100%">
            
   <tr>
            
    <td width="50%">
	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>

	</div>

		
	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>
                    
     
    </td> 

    </tr>
    
    </table></fieldset>
         
         <div class="row buttons" style="text-align: center">
	<?php echo CHtml::submitButton('Entrar'); ?>
	 </div>
        
        </div></div>
<?php $this->endWidget(); ?>
</div><!-- form -->
