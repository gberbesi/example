<?php
$this->breadcrumbs=array(
	'Geo Municipios'=>array('index'),
	$model->geo_municipio_id,
);

$this->menu=array(
	array('label'=>'List GeoMunicipio', 'url'=>array('index')),
	array('label'=>'Create GeoMunicipio', 'url'=>array('create')),
	array('label'=>'Update GeoMunicipio', 'url'=>array('update', 'id'=>$model->geo_municipio_id)),
	array('label'=>'Delete GeoMunicipio', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->geo_municipio_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GeoMunicipio', 'url'=>array('admin')),
);
?>

<h1>View GeoMunicipio #<?php echo $model->geo_municipio_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'geo_municipio_id',
		'geo_estado_id',
		'nombre',
	),
)); ?>
