<?php
$this->breadcrumbs=array(
	'Geo Municipios'=>array('index'),
	$model->geo_municipio_id=>array('view','id'=>$model->geo_municipio_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List GeoMunicipio', 'url'=>array('index')),
	array('label'=>'Create GeoMunicipio', 'url'=>array('create')),
	array('label'=>'View GeoMunicipio', 'url'=>array('view', 'id'=>$model->geo_municipio_id)),
	array('label'=>'Manage GeoMunicipio', 'url'=>array('admin')),
);
?>

<h1>Update GeoMunicipio <?php echo $model->geo_municipio_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>