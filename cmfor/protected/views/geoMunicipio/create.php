<?php
$this->breadcrumbs=array(
	'Geo Municipios'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GeoMunicipio', 'url'=>array('index')),
	array('label'=>'Manage GeoMunicipio', 'url'=>array('admin')),
);
?>

<h1>Create GeoMunicipio</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>