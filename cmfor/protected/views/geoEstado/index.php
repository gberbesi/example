<?php
$this->breadcrumbs=array(
	'Geo Estados',
);

$this->menu=array(
	array('label'=>'Create GeoEstado', 'url'=>array('create')),
	array('label'=>'Manage GeoEstado', 'url'=>array('admin')),
);
?>

<h1>Geo Estados</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
