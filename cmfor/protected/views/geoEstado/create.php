<?php
$this->breadcrumbs=array(
	'Geo Estados'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GeoEstado', 'url'=>array('index')),
	array('label'=>'Manage GeoEstado', 'url'=>array('admin')),
);
?>

<h1>Create GeoEstado</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>