<?php
$this->breadcrumbs=array(
	'Geo Estados'=>array('index'),
	$model->geo_estado_id=>array('view','id'=>$model->geo_estado_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List GeoEstado', 'url'=>array('index')),
	array('label'=>'Create GeoEstado', 'url'=>array('create')),
	array('label'=>'View GeoEstado', 'url'=>array('view', 'id'=>$model->geo_estado_id)),
	array('label'=>'Manage GeoEstado', 'url'=>array('admin')),
);
?>

<h1>Update GeoEstado <?php echo $model->geo_estado_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>