<?php
$this->breadcrumbs=array(
	'Geo Estados'=>array('index'),
	$model->geo_estado_id,
);

$this->menu=array(
	array('label'=>'List GeoEstado', 'url'=>array('index')),
	array('label'=>'Create GeoEstado', 'url'=>array('create')),
	array('label'=>'Update GeoEstado', 'url'=>array('update', 'id'=>$model->geo_estado_id)),
	array('label'=>'Delete GeoEstado', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->geo_estado_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GeoEstado', 'url'=>array('admin')),
);
?>

<h1>View GeoEstado #<?php echo $model->geo_estado_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'geo_estado_id',
		'nombre',
	),
)); ?>
