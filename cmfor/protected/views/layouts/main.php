<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/vistadata.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/menu.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/terreno.css" />
       <script src="<?php echo Yii::app()->request->baseUrl; ?>/js/zeta.js" type="text/javascript"></script>
     

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">


<div id="header">
	<center><img width="945" src="<?php echo Yii::app()->request->baseUrl; ?>/images/encabezado.jpg"></center>
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/icono.png" type="image/x-icon" /> 
</div><!-- header -->
    
	 <!--  Menu  -->
<div>
       <?php //if(!Yii::app()->user->isGuest): ?>
	<div class="menuxavier" >
		    <ul id="menuconestilo">
			    
				<?php /* ?>
			    <li><a href="<?php echo Yii::app()->createUrl('/'); ?>" class="drop">Inicio</a><!-- Begin 3 columns Item --></li><!-- End 3 columns Item Logs-->
			    <?php */ ?>
			 
			    <?php if(!Yii::app()->user->isGuest): ?>   
				    <li><a href="#" class="drop">Justo Valor del Terreno</a><!-- Begin 3 columns Item -->
				        <div class="dropdown_2columns"><!-- Begin 3 columns container -->
				            <div class="col_2">
				                <ul class="greybox2">
				                	<li><a href="<?php echo Yii::app()->createUrl('/terreno/create'); ?>">Calcular Justo Valor del Terreno</a></li>
			                        <li><a href="<?php echo Yii::app()->createUrl('/terreno/admin'); ?>">Listar Terrenos</a></li>
			                        <li><a href="<?php echo Yii::app()->createUrl('/tasas/admin'); ?>">Carga de INPC y Tasas de Interés</a></li>
				                </ul>   
				            </div>
				        </div><!-- End 3 columns container -->
				    </li><!-- End 3 columns Item Fechas-->
			    <?php endif; ?>
			    
				<?php if(!Yii::app()->user->isGuest): ?>
				    <li class="menu_right"><a href="#" class="drop">Opciones de usuario</a><!-- Begin 3 columns Item -->
				        <div class="dropdown_2columns align_right"><!-- Begin 3 columns container -->
				            <div class="col_2">
				                <ul class="greybox2">
				                    <?php if(!Yii::app()->user->isGuest): ?>
				                    	<li><a href="<?php echo Yii::app()->createUrl('/site/logout'); ?>"><?php echo 'Cerrar sesión ('.Yii::app()->user->name.')' ?></a></li>
				                    <?php endif; ?> 
				                </ul>   
				            </div>
				        </div><!-- End 3 columns container -->
				    </li><!-- End 3 columns Item OPCIONES DE USUARIOS-->
				<?php endif; ?>

			</ul>
	</div>

   
</div>
    
    <!-- Fin Menu -->

	<div id="mainmenu">
		<?php /* $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); */?>
	</div><!-- mainmenu -->
	<?php /*if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif*/ ?>

	<?php echo $content; ?>

	<div id="footer">
            <u><font color=#B40404>Cumpliendo con el Decreto 3.390</font></u><br/>
		<?php //echo Yii::powered(); ?>
		<i>Del Software Libre al Software Socialista</i><br/>
		Copyleft &copy; <?php echo date('Y'); ?> Ministerio del Poder Popular para la Vivienda y Hábitat.
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
