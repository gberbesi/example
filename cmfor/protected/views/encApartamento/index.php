<?php
/* @var $this EncApartamentoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Enc Apartamentos',
);

$this->menu=array(
	array('label'=>'Create EncApartamento', 'url'=>array('create')),
	array('label'=>'Manage EncApartamento', 'url'=>array('admin')),
);
?>

<h1>Enc Apartamentos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
