<?php
/* @var $this EncApartamentoController */
/* @var $model EncApartamento */

$this->breadcrumbs=array(
	'Enc Apartamentos'=>array('index'),
	$model->enc_apartamento_id,
);

$this->menu=array(
	array('label'=>'List EncApartamento', 'url'=>array('index')),
	array('label'=>'Create EncApartamento', 'url'=>array('create')),
	array('label'=>'Update EncApartamento', 'url'=>array('update', 'id'=>$model->enc_apartamento_id)),
	array('label'=>'Delete EncApartamento', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->enc_apartamento_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EncApartamento', 'url'=>array('admin')),
);
?>

<h1>View EncApartamento #<?php echo $model->enc_apartamento_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'enc_apartamento_id',
		'piso_id',
		'urbanismo',
		'etapa',
		'torre',
		'nomesclatura',
	),
)); ?>
