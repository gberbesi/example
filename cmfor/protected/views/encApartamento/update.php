<?php
/* @var $this EncApartamentoController */
/* @var $model EncApartamento */

$this->breadcrumbs=array(
	'Enc Apartamentos'=>array('index'),
	$model->enc_apartamento_id=>array('view','id'=>$model->enc_apartamento_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EncApartamento', 'url'=>array('index')),
	array('label'=>'Create EncApartamento', 'url'=>array('create')),
	array('label'=>'View EncApartamento', 'url'=>array('view', 'id'=>$model->enc_apartamento_id)),
	array('label'=>'Manage EncApartamento', 'url'=>array('admin')),
);
?>

<h1>Update EncApartamento <?php echo $model->enc_apartamento_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>