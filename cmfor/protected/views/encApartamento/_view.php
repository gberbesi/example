<?php
/* @var $this EncApartamentoController */
/* @var $data EncApartamento */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('enc_apartamento_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->enc_apartamento_id), array('view', 'id'=>$data->enc_apartamento_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('piso_id')); ?>:</b>
	<?php echo CHtml::encode($data->piso_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('urbanismo')); ?>:</b>
	<?php echo CHtml::encode($data->urbanismo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('etapa')); ?>:</b>
	<?php echo CHtml::encode($data->etapa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('torre')); ?>:</b>
	<?php echo CHtml::encode($data->torre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomesclatura')); ?>:</b>
	<?php echo CHtml::encode($data->nomesclatura); ?>
	<br />


</div>