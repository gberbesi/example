<?php
/* @var $this EncApartamentoController */
/* @var $model EncApartamento */

$this->breadcrumbs=array(
	'Enc Apartamentos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EncApartamento', 'url'=>array('index')),
	array('label'=>'Manage EncApartamento', 'url'=>array('admin')),
);
?>

<h1>Create EncApartamento</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>