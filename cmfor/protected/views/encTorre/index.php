<?php
/* @var $this EncTorreController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Enc Torres',
);

$this->menu=array(
	array('label'=>'Create EncTorre', 'url'=>array('create')),
	array('label'=>'Manage EncTorre', 'url'=>array('admin')),
);
?>

<h1>Enc Torres</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
