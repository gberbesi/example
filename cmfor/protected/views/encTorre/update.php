<?php
/* @var $this EncTorreController */
/* @var $model EncTorre */

$this->breadcrumbs=array(
	'Enc Torres'=>array('index'),
	$model->enc_torre_id=>array('view','id'=>$model->enc_torre_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EncTorre', 'url'=>array('index')),
	array('label'=>'Create EncTorre', 'url'=>array('create')),
	array('label'=>'View EncTorre', 'url'=>array('view', 'id'=>$model->enc_torre_id)),
	array('label'=>'Manage EncTorre', 'url'=>array('admin')),
);
?>

<h1>Update EncTorre <?php echo $model->enc_torre_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>