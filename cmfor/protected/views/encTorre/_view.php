<?php
/* @var $this EncTorreController */
/* @var $data EncTorre */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('enc_torre_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->enc_torre_id), array('view', 'id'=>$data->enc_torre_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_urbanismo')); ?>:</b>
	<?php echo CHtml::encode($data->id_urbanismo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nom_torre')); ?>:</b>
	<?php echo CHtml::encode($data->nom_torre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nr_etapa')); ?>:</b>
	<?php echo CHtml::encode($data->nr_etapa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_vivienda')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_vivienda); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cant_pisos')); ?>:</b>
	<?php echo CHtml::encode($data->cant_pisos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apartamento_pisos')); ?>:</b>
	<?php echo CHtml::encode($data->apartamento_pisos); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('nom_comite')); ?>:</b>
	<?php echo CHtml::encode($data->nom_comite); ?>
	<br />

	*/ ?>

</div>