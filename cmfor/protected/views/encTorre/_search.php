<?php
/* @var $this EncTorreController */
/* @var $model EncTorre */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'enc_torre_id'); ?>
		<?php echo $form->textField($model,'enc_torre_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_urbanismo'); ?>
		<?php echo $form->textField($model,'id_urbanismo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nom_torre'); ?>
		<?php echo $form->textField($model,'nom_torre',array('size'=>15,'maxlength'=>15)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nr_etapa'); ?>
		<?php echo $form->textField($model,'nr_etapa'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipo_vivienda'); ?>
		<?php echo $form->textField($model,'tipo_vivienda'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cant_pisos'); ?>
		<?php echo $form->textField($model,'cant_pisos'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'apartamento_pisos'); ?>
		<?php echo $form->textField($model,'apartamento_pisos'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nom_comite'); ?>
		<?php echo $form->textField($model,'nom_comite',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->