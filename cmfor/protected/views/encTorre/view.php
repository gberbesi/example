<?php
/* @var $this EncTorreController */
/* @var $model EncTorre */

$this->breadcrumbs=array(
	'Enc Torres'=>array('index'),
	$model->enc_torre_id,
);

$this->menu=array(
	array('label'=>'List EncTorre', 'url'=>array('index')),
	array('label'=>'Create EncTorre', 'url'=>array('create')),
	array('label'=>'Update EncTorre', 'url'=>array('update', 'id'=>$model->enc_torre_id)),
	array('label'=>'Delete EncTorre', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->enc_torre_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EncTorre', 'url'=>array('admin')),
);
?>

<h1>View EncTorre #<?php echo $model->enc_torre_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'enc_torre_id',
		'id_urbanismo',
		'nom_torre',
		'nr_etapa',
		'tipo_vivienda',
		'cant_pisos',
		'apartamento_pisos',
		'nom_comite',
	),
)); ?>
