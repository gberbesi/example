<?php
/* @var $this EncTorreController */
/* @var $model EncTorre */

$this->breadcrumbs=array(
	'Enc Torres'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List EncTorre', 'url'=>array('index')),
	array('label'=>'Manage EncTorre', 'url'=>array('admin')),
);
?>

<h1>Create EncTorre</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>