<?php
/* @var $this EncTorreController */
/* @var $model EncTorre */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'enc-torre-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_urbanismo'); ?>
		<?php echo $form->textField($model,'id_urbanismo'); ?>
		<?php echo $form->error($model,'id_urbanismo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nom_torre'); ?>
		<?php echo $form->textField($model,'nom_torre',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'nom_torre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nr_etapa'); ?>
		<?php echo $form->textField($model,'nr_etapa'); ?>
		<?php echo $form->error($model,'nr_etapa'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo_vivienda'); ?>
		<?php echo $form->textField($model,'tipo_vivienda'); ?>
		<?php echo $form->error($model,'tipo_vivienda'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cant_pisos'); ?>
		<?php echo $form->textField($model,'cant_pisos'); ?>
		<?php echo $form->error($model,'cant_pisos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'apartamento_pisos'); ?>
		<?php echo $form->textField($model,'apartamento_pisos'); ?>
		<?php echo $form->error($model,'apartamento_pisos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nom_comite'); ?>
		<?php echo $form->textField($model,'nom_comite',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'nom_comite'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->