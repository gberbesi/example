<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
		
		<!-- Font -->
		<link href='css/censov2/login/assets/css/css.css' rel='stylesheet' type='text/css'>
		<link href='css/censov2/login/assets/css/css2.css' rel='stylesheet' type='text/css'>
		<!-- Font -->
		
		
        <link rel="stylesheet" href="css/censov2/login/assets/css/normalize.css">
        <link rel="stylesheet" href="css/censov2/login/assets/css/main.css">
        <link rel="stylesheet" href="css/censov2/login/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/censov2/login/assets/css/animate.css">
        
        <link rel="stylesheet" href="css/censov2/login/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/censov2/login/assets/css/style.css">
        <link rel="stylesheet" href="css/censov2/login/assets/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="css/censov2/login/assets/css/sweet.css">
        <script src="css/censov2/login/assets/js/vendor/modernizr-2.8.3.min.js"></script>
		
		
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		
		<!-- Header Start -->
		<header id="home">
			
			<!-- Main Menu Start -->
			<div class="main-menu">
				<div class="navbar-wrapper">
					<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
						<div class="container">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="sr-only">Toggle Navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								
								<a href="#" class="navbar-brand"> </a>							
							</div>
							
							<div class="navbar-collapse collapse">
								<ul class="nav navbar-nav navbar-right">
									<li><a href="#home">Inicio</a></li>
									<li><a href="#about">Información</a></li>
									<li><a href="#contact-us">Actualiza tus datos</a></li>
									<li><a href="#" data-toggle="modal" data-target="#modal_recuperar">Recuperar Contraseña</a></li>
								</ul>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<!-- Main Menu End -->
			
			<!-- Sider Start -->
			<div class="slider">
				<div id="fawesome-carousel" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators indicatior2">
						<li data-target="#fawesome-carousel" data-slide-to="0" class="active"></li>
					</ol>
				 
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<img src="css/censov2/login/assets/img/slider/portada3.jpg" alt="Sider Big Image" width="100%" style="margin-top: 20px">
							<div class="carousel-caption">
								<div class="slider-btn wow fadeIn">
									<div style="background: rgba(0,0,0,.6);padding: 20px; width: 78%;margin-left: 17%; margin-top: 13%; border-radius: 10px;">
										<a class="btn btn-danger" data-toggle="modal" data-target="#modal_registro" style="width: 193px;height: 50px;padding-top: 15px; margin-right: 95px; font-size: 18px;">Registro de Usuario</a>
										<a href="#contact-us" class="btn btn-danger" style="width: 252px;height: 50px;padding-top: 15px; font-size: 18px;">Ir a actualización de datos</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Sider End -->
			
		</header>
		<!-- Header End -->
		
		

		<section id="info-img">
			<div class="item" style="margin-bottom: 45px;" >
								<img src="css/censov2/login/assets/img/slider/slider8.jpg" alt="Sider Big Image" width="100%" style="position: absolute; z-index: -1 ;height:600px;">

			<b><h3 style="text-align: center; text-shadow: 4px 4px white; margin-left: 80px; padding: 50px; line-height: 50px; ">
					<br><br>
					El Gobierno Bolivariano a través de la Gran Misión Vivienda Venezuela<br> reimpulsa el Plan de Registro y Actualización de datos a través de la plataforma Patria, en aras de dignificar al pueblo venezolano.<br><br></h3>
					<h3 style="text-align: center; text-shadow: 4px 1px white; margin-left: 80px; line-height: 50px; color: black; font-size: 27px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;El legado de nuestro Comandante Hugo Rafael Chávez Frías sigue vigente <br> Gracias al Presidente Obrero Nicolás Maduro<br></h3> 
					
				</div>
			</div>
		</section>
<!--
		<section id="about" class="site-padding">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<div class="title">
							<h3> Información </h3>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row animatedParent">
					<div class="col-sm-4 service-box animated">
						<img src="css/censov2/login/assets/img/slider/slider5.jpg" data-toggle="modal" data-target="#modal_gmvv">
						<div id="modal_gmvv" class="modal fade" role="dialog" style="margin-top:60px;">
							  <div class="modal-dialog">

							    <!-- Modal content-->
							    <!--
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title" style="text-align: center;"> La GMVV: </h4>
							      </div>
							      <div class="modal-body">
							        <p style="text-align: justify;">Tiene como objetivo subsanar el déficit habitacional del país, dignificando al pueblo que requiere más atención con respecto al tema de vivienda.</p>
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							      </div>
							    </div>

							  </div>
							</div>
					</div>
					<div class="col-sm-4 service-box">
						<img src="css/censov2/login/assets/img/slider/slider6.jpg" data-toggle="modal" data-target="#modal_plan">
						<div id="modal_plan" class="modal fade" role="dialog" style="margin-top:60px;">
							  <div class="modal-dialog">

							    <!-- Modal content-->
							    <!--
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal">&times;</button>
							        <h4 class="modal-title" style="text-align: center;"> Plan GMVV Constituyente Juvenil: </h4>
							      </div>
							      <div class="modal-body">
							        <p style="text-align: justify;">El objetivo de dicho plan es atender la población joven comprendidos entre los 15 y 34, con el fin de garantizar el acceso a la vivienda durante el 2018-2022.</p>
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
							      </div>
							    </div>

							  </div>
							</div>
					</div>
					<div class="col-sm-4 service-box animated">
						<img src="css/censov2/login/assets/img/slider/slider7.jpg" data-toggle="modal" data-target="#modal_mihogar">

						<div id="modal_mihogar" class="modal fade" role="dialog" style="margin-top:60px;">
						  <div class="modal-dialog">

						    <!-- Modal content-->
						    <!--
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal">&times;</button>
						        <h4 class="modal-title" style="text-align: center;"> 0800-MiHogar: </h4>
						      </div>
						      <div class="modal-body">
						        <p style="text-align: justify;">Tiene como propósito ayudar a las familias de clase media a poder adquirir una vivienda digna a precio justo, en este sentido lo que estén interesados en optar por el plan, deberán poseer ingresos entre 3 y 20 salarios mínimos.</p>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						      </div>
						    </div>

						  </div>
						</div>
					</div>
				</div>
			</div>
		</section>
		-->
		<!-- Contact -->
		<?php echo $content; ?>
		<!-- Contact -->
		
		
		<!-- footer -->
		
		<footer>
			<div class="container">
			</div>
			
		</footer>
		
		<!-- footer -->
		
		
        <script src="css/censov2/login/assets/js/plugins.js"></script>
        <script src="css/censov2/login/assets/js/bootstrap.min.js"></script>
        <script src="css/censov2/login/assets/js/jquery.mousewheel-3.0.6.pack.js"></script>
        <script src="css/censov2/login/assets/js/paralax.js"></script>
        <script src="css/censov2/login/assets/js/jquery.smooth-scroll.js"></script>
        <script src="css/censov2/login/assets/js/jquery.sticky.js"></script>
        <script src="css/censov2/login/assets/js/wow.min.js"></script>
        <script src="css/censov2/login/assets/js/main.js"></script>
        <script src="css/censov2/login/assets/js/sweet.min.js"></script>
        
		
		<script type="text/javascript">
			$(document).ready(function(){
				$('a[href^="#"]').on('click',function (e) {
					e.preventDefault();

					var target = this.hash;
					var $target = $(target);

					$('html, body').stop().animate({
						 'scrollTop': $target.offset().top
					}, 900, 'swing');
					});
			});
		</script>
		
		<script src="css/censov2/login/assets/js/custom.js"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            
            /*REGISTRO DE PERSONA VÍA AJAX*/
			function registrar_ajax() {
				nacionalidad = document.getElementById('nacionalidad').value;
				cedula = document.getElementById('cedula').value;
				nombre1 = document.getElementById('nombre1').value;
				apellido1 = document.getElementById('apellido1').value;
				fecha_nacimiento = document.getElementById('fecha_nacimiento').value;
				email = document.getElementById('email').value;
				celular = document.getElementById('celular').value;
				if ( nacionalidad == '' || cedula == '' || nombre1 == '' || apellido1 == '' || fecha_nacimiento == '' || email == '' || celular == '' ) 
				{
					swal({ title: '¡ERROR!', text: 'Debe llenar los campos obligatorios', type: 'error', showCancelButton: false, closeOnConfirm: true, confirmButtonText: 'OK' },function() 
					{
						$('#modal_registro').modal('show');
					});
				}
				else 
				{
					swal({ title: '¡Confirmar!', text: '¿Confirma ejecutar la operación?', type: 'info', showCancelButton: true, closeOnConfirm: false, showLoaderOnConfirm: true, confirmButtonText: 'SI', cancelButtonText: 'NO' },function() 
					{
						$.post("admin/control/control_persona.php",{nacionalidad:nacionalidad,cedula:cedula,nombre1:nombre1,apellido1:apellido1,fecha_nacimiento:fecha_nacimiento,email:email,celular:celular,evento:"registrar_ajax"},function(response) 
						{
							console.log(response);
							//console.log("ajax enviado");
						}).done(function(response) 
						{
							r = JSON.parse(response);
							if ( r.resultado == '1' )
							{
								swal('¡OPERACIÓN EXITOSA!','Registrado con éxito. Su usuario y contraseña será: '+cedula+', ya puede ingresar al sistema.','success');
								$("#form_registro")[0].reset();
							}
							else if ( r.resultado == '2' )
							{
								swal('¡ERROR!','Esa cédula ya está registrada en el sistema','error');
								$('#modal_registro').modal('show');
							}
						}).fail(function() 
						{
							swal('¡ERROR!','Ha ocurrido un error en el servidor','error');
							$('#modal_registro').modal('show');
						});
					});
				}
			}

			function ingresar() {
				usuario = document.getElementById('usuario').value;
				contrasena = document.getElementById('contrasena').value;
				if ( usuario == '' || contrasena == '' ) 
				{
					swal('¡ERROR!','Ingresa tu usuario y contraseña','error');
				}
				else 
				{
					swal({ title: '¡Confirmar!', text: '¿Confirma ingresar al sistema?', type: 'info', showCancelButton: true, closeOnConfirm: false, showLoaderOnConfirm: true, confirmButtonText: 'SI', cancelButtonText: 'NO' },function() 
					{
						$.post("admin/control/control_usuario.php",{usuario:usuario,contrasena:contrasena,evento:"ingresar"},function(response) 
						{
							console.log(response);
							//console.log("ajax enviado");
						}).done(function(response) 
						{
							r = JSON.parse(response);
							if ( r.resultado == '1' )
							{
								swal({ title: '¡Ingreso exitoso!', text: '', type: 'info', showCancelButton: false, closeOnConfirm: true,  confirmButtonText: 'Continuar' },function() 
								{
									location.href = "admin/";
								});
							}
							else if ( r.resultado == '2' )
							{
								swal('¡ERROR!','Usuario o contraseña incorrecta','error');
							}
						}).fail(function() 
						{
							swal('¡ERROR!','Ha ocurrido un error en el servidor','error');
						});
					});
				}
			}

        </script>
    </body>
</html>
