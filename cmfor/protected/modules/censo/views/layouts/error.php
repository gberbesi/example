<!DOCTYPE HTML>
<html>
	<head>
		<title>Actualizacion de censo de vivienda 2017</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
		
		
		<link type="text/css" href="js/censov2/libreria/bootstrap-3.1.1/css/bootstrap.min.css"  rel="stylesheet" />
		<link type="text/css" href="js/censov2/libreria/bootstrap-3.1.1/css/bootstrap-theme.min.css"  rel="stylesheet" />
		
		
		<!-- Custom CSS -->
		<link href="css/censov2/style.css" rel='stylesheet' type='text/css' />
		<!-- Graph CSS -->
		<link href="css/censov2/font-awesome.css" rel="stylesheet"> 
		<!-- jQuery -->
		<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'>
		<!-- lined-icons -->
		<link rel="stylesheet" href="css/censov2/icon-font.min.css" type='text/css' />
		<!-- //lined-icons -->
		<!-- <link href="css/censov2/bootstrap-select.min.css" rel="stylesheet" type="text/css" >  -->
		
		
		<link type="text/css" href="css/censov2/select2/css/select2.min.css"  rel="stylesheet" />
		<link type="text/css" href="css/censov2/select2/css/select2-bootstrap.min.css"  rel="stylesheet" />
	  

	  <!--//skycons-icons-->
	</head> 
	<body>
		<div class="page-container">
			<!--/content-inner-->
			<div class="content">
				<div class="inner-content">
					<!-- header-starts -->
					<div class="header-section">
						<!--menu-right-->

						<!--//menu-right-->
						<div class="clearfix"></div>
					</div>
					<!-- //header-ends -->
					<div class="container fondov">
						<div style="margin-top: 10px; padding: 15px; background: rgba(255,255,255,.9); border: 1px solid #ccc; border-radius: 10px; box-shadow: 0 0 10px black;">
							
							
							<!-- -->
							<?php echo $content; ?>
							<!-- -->
							
							
						</div>
					</div>
					<!--footer section start-->
					<!--f-ooter>
					   <p>&copy 2016 Augment . All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank">W3layouts.</a></p>
					</footer>
					<!--footer section end-->
				</div>
			</div>
			<!--//content-inner-->
			<!--/sidebar-menu-->

		</div>
		<script type="text/javascript">
			var toggle = true;

			$(".sidebar-icon").click(function() {                
			if (toggle)
			{
			  $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
			  $("#menu span").css({"position":"absolute"});
			}
			else
			{
			  $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
			  setTimeout(function() {
				$("#menu span").css({"position":"relative"});
			  }, 400);
			}

			toggle = !toggle;
			});
			
			$(document).ready(function() {
				$('select').select2({
					theme: "bootstrap",
					placeholder: "Seleccione",
					allowClear: true
				});
				


				

				
				
			
			});
		</script>
		<!--js 
		<link rel="stylesheet" href="css/vroom.css">
		<script type="text/javascript" src="js/vroom.js"></script>
		<script type="text/javascript" src="js/TweenLite.min.js"></script>
		<script type="text/javascript" src="js/CSSPlugin.min.js"></script>-->

		<!-- Bootstrap Core JavaScript -->
		<script src="js/censov2/js/bootstrap.min.js"></script>
		<script src="js/censov2/select2/select2.full.min.js"></script>
		<!-- <script src="js/censov2/js/scripts.js"></script> -->
	</body>
</html>