<?php
$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<h2 style="text-align:center;">Error <?php echo $code; ?></h2>

<div class="borderfomulario" style="padding:10px;border:#AD1818 2px solid; width:370px; margin: 0px auto; margin-bottom: 1.5em; background: #F5F5F5;">
               
               <table>
                       <tr>
                               <td>
                                       <img style="vertical-align:middle;" alt="Error" src="<?php echo Yii::app()->request->baseUrl; ?>/images/error404.png">
                               </td>
                               <td>
                                      <div style="text-align:center;"> <b>Acceso denegado</b><br></div>
									<div style="text-align:justify;">Ha intentado acceder a un sitio restringido, si considera que es un error, contacte con el administrador para solicitar el acceso.</div>	
									   
                               </td>
                       </tr>
               </table>
               

</div>
<div style="text-align:center;">
       <h2><?php echo CHtml::link('Volver al inicio',array('/'.$this->module->id.'/usuario/login')); ?></h2>
</div>
 
 
