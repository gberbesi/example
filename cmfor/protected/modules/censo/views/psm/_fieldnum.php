<div class="form-group <?php if(isset($_POST[get_class($model)])) echo $model->hasErrors($atributo)?'has-error':'has-success'; ?>">
	<?php echo $form->labelEx($model,$atributo); ?>
	<?php echo $form->textField($model,$atributo,array('class'=>'form-control','onkeypress'=>'return soloNumeros(event)'));?>
	<?php echo $form->error($model,$atributo,array('class'=>'help-block text-center')); ?>
</div>