
<style>
	.row_campos_detalles div,.row_campos_detalles input,.row_campos_detalles select{padding:0px;margin:0px;border-radius:0px;height:20px;}.boton_menos,.boton_mas{height:20px;padding:2px 3px 2px 3px;}#th_button_nuevo{text-align:center;width:80px;}.td_botones button{float:left;}.div_botones_listar{}.div_botones_listar button{margin-left:3px;}.ui-datepicker-month, .ui-datepicker-year{color:#333;font-weight: 900;}.row{margin-top: 20px;margin-bottom: 20px;}
	label.btn span {font-size: 1em}
	label input[type="radio"] ~ i.fa.fa-circle-o{color: #c8c8c8; display: inline}
	label input[type="radio"] ~ i.fa.fa-dot-circle-o{display: none}
	label input[type="radio"]:checked ~ i.fa.fa-circle-o{display: none}
	label input[type="radio"]:checked ~ i.fa.fa-dot-circle-o{color: #7AA3CC;display: inline;}label:hover input[type="radio"] ~ i.fa {color: #7AA3CC}
	label input[type="checkbox"] ~ i.fa.fa-square-o{color: #c8c8c8;display: inline}
	label input[type="checkbox"] ~ i.fa.fa-check-square-o{display: none}
	label input[type="checkbox"]:checked ~ i.fa.fa-square-o{display: none}
	label input[type="checkbox"]:checked ~ i.fa.fa-check-square-o{color: #7AA3CC;display: inline}
	label:hover input[type="checkbox"] ~ i.fa {color: #7AA3CC}
	.has-error div[data-toggle="buttons"] label.active{color: #7AA3CC}
	div[data-toggle="buttons"] label.active{color: #7AA3CC}
	div[data-toggle="buttons"] label:hover {color: #7AA3CC}
	
	.has-error div[data-toggle="buttons"] label {
		color: #a94442;
	}
	
	div[data-toggle="buttons"] label {
	display: inline-block;
	padding: 6px 12px;
	margin-bottom: 0;
	font-size: 20px;
	font-weight: normal;
	line-height: 2em;
	text-align: left;
	white-space: nowrap;
	vertical-align: top;
	cursor: pointer;
	background-color: none;
	border: 0px solid #c8c8c8;
	border-radius: 3px;
	color: #c8c8c8;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	-o-user-select: none;
	user-select: none;
	}
	
	
	div[data-toggle="buttons"] label:active, div[data-toggle="buttons"] label.active {
	-webkit-box-shadow: none;
	box-shadow: none;
	}
	
	span.required {
		color:red;
	}
	
	.help-block {
		font-size: 20px;
	}

	/*
	.has-error .btn-group  {
		color:#a94442;
	}
	*/
</style>

<div>
	<div class="container-fluid">
		<div class="container-fluid">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'registro-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
			<center>
				<h2>Actualización de Datos "0800" Mi Hogar </h2>
				<?php echo $form->textField($psm,"mensaje",array("style"=>"width:100%; background-color:white; border:0; color:red; font-size:19px; font-weight:bold;text-align:center;","disabled"=>true));?>
				
			</center>

			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<span style="float:right; color:red">(*) Campos obligatorios</span>
				</div>
			</div>	

			<?php
			if($psm->nombre1!=null){

			 	 $primerRegistro=true;

}else{

	$primerRegistro=false;
				
}
// echo $psm->parentesco_id;exit(); //Para verificar si es jefe de familia

			 ?>
		

			<div class="row" style="font-size: 22px;">
				<div class="col-md-4">
					<div class="form-group <?php if(isset($_POST['Censado'])) echo $psm->hasErrors('cedula')?'has-error':'has-success'; ?>">
						<?php echo $form->labelEx($psm,"cedula"); ?>
						<div class="input-group">
							<div class="input-group-btn">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="button_nacionalidad"><?php echo ($psm->nacionalidad?$psm->nacionalidad:'V'); ?></span><span class="caret"></span></button>
								<ul class="dropdown-menu" role="menu">
									<li><a onclick="$('#Censado_nacionalidad').val('V');$('#button_nacionalidad').text('V');">V</a></li>
									<li><a onclick="$('#Censado_nacionalidad').val('E');$('#button_nacionalidad').text('E');">E</a></li>
								</ul>
							</div>
<?php echo $form->textField($psm,"cedula",array('class'=>'form-control',"disabled"=>$primerRegistro,'onkeypress'=>'return soloNumeros(event)'));?>
 <span class="input-group-btn"<?php echo ($primerRegistro) ? "style='display:none'":"";?> >

			<button id="buscarpersona" class="btn btn-primary" type="button">Buscar</button> ?>

</span>

						</div>
						<?php echo $form->hiddenField($psm,"nacionalidad");?>
						<?php echo $form->error($psm,'cedula',array('class'=>'help-block text-center')); ?>
					</div>
				</div>
			
				<div class="col-md-2">
					
					<?php echo $this->renderPartial('_field',array(
							'model'=>$psm,
							'form'=>$form,
							'atributo'=>'nombre1',
							),true);
					?>
				</div>
			
				<div class="col-md-2">
					<?php echo $this->renderPartial('_field',array(
							'model'=>$psm,
							'form'=>$form,
							'atributo'=>'nombre2',
							),true);
					?>
				</div>
			
				<div class="col-md-2">
					<?php echo $this->renderPartial('_field',array(
							'model'=>$psm,
							'form'=>$form,
							'atributo'=>'apellido1',
							),true);
					?>
				</div>
			
				<div class="col-md-2">
					<?php echo $this->renderPartial('_field',array(
							'model'=>$psm,
							'form'=>$form,
							'atributo'=>'apellido2',
							),true);
					?>
				</div>
			</div>


			<div class="container-fluid" style="margin-top: 30px; font-size: 22px;">
				<div id="exTab3" class="container-fluid">	
					<ul  class="nav nav-pills nav-justified">
						<?php $activo=3; ?>
						<?php if($jefefamilia): ?> 
						<?php $activo=1; ?>
						<?php $opcion=1; ?>
						
						<li <?php echo ($activo==1?'class="active"':''); ?> >
							<a id="xa" href="#a" data-toggle="tab" style=" margin-right: 10px;border-radius: 20px; font-size:19px;"><span class="badge"><?php $opcion++; ?></span><p class="stop"> <b>Domicilio </b> </p></a>
						</li>
					
						<li <?php echo ($activo==2?'class="active"':''); ?>>
							<a id="xb" href="#b" data-toggle="tab" style=" margin-right: 10px;border-radius: 20px; font-size:19px;"><span class="badge"><?php $opcion++; ?></span><p class="stop"><b> Ubicación Geográfica de Solicitud  </b></p></a>
						</li>
						<?php endif; ?>
						<li <?php echo ($activo==3?'class="active"':''); ?>>
							<a id="xc" href="#c" data-toggle="tab" style=" margin-right: 10px;border-radius: 20px; font-size:19px;"><span class="badge"><?php $opcion++; ?></span><p class="stop"><b> Personales </b></p></a>
						</li>
						<li <?php echo ($activo==4?'class="active"':''); ?>>
							<a id="xd" href="#d" data-toggle="tab" style=" margin-right: 10px;border-radius: 20px; font-size:19px;"><span class="badge"><?php $opcion++; ?></span><p class="stop"> <b>Contacto  </b></p></a>
						</li>
						<li <?php echo ($activo==5?'class="active"':''); ?>>
							<a id="xe" href="#e" data-toggle="tab" style=" margin-right: 10px;border-radius: 20px; font-size:19px;"><span class="badge"><?php $opcion++; ?></span><p class="stop"><b > Socio-Económicos  </b></p></a>
						</li>
					</ul>
					

					<div class="tab-content clearfix">

						<?php if($jefefamilia): ?>
						<div class="tab-pane fade <?php echo ($activo==1?'in active':''); ?>" id="a">
							<hr/>
							<div class="row">
								
								<?php echo $this->renderPartial('_estadoMunicipioParroquia',array(
										'model'=>$psmDireccion,
										'form'=>$form,
										'atributoEstado'=>'[0]estado_id',
										'atributoMunicipio'=>'[0]municipio_id',
										'atributoParroquia'=>'[0]parroquia_id',
										'size'=>'4',
										),true); 
								?>

								
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-group <?php if(isset($_POST['Censado'])) echo $psmDireccion->hasErrors('direccion')?'has-error':'has-success'; ?>">
										<?php echo $form->labelEx($psmDireccion,"direccion"); ?>
										<?php echo $form->textArea($psmDireccion,'[0]direccion',array('class'=>'form-control')); ?>
										<?php echo $form->error($psmDireccion,'direccion',array('class'=>'help-block text-center')); ?>
									</div>
								</div>
							</div>
							
							<div class="row">
								
								<div class="col-md-4">
									<?php
										$condicionVivList = CHtml::listData(FamCondicionViviendaOrigen::model()->findAll('st_condicion_vivienda= true ORDER BY nombre'), 'condicion_vivienda_id', 'nombre')
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psmDireccion,
											'form'=>$form,
											'atributo'=>'[0]condicion_vivienda_id',
											'list'=>$condicionVivList,
											),true); 
									?>
								</div>
								
								<div class="col-md-4">
									<?php
										$calidadVivList = CHtml::listData(FamCalidadViviendaOrigen::model()->findAll('estatus= true ORDER BY nombre'), 'calidad_vivienda_id', 'nombre')
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psmDireccion,
											'form'=>$form,
											'atributo'=>'[0]calidad_vivienda_id',
											'list'=>$calidadVivList,
											),true); 
									?>
								</div>
								
								
								<div class="col-md-4">
									<?php
										$nivelRiesgoList = CHtml::listData(FamNivelRiesgo::model()->findAll(), 'id_fam_nivel_riesgo', 'descripcion');
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'fam_nivel_riesgo_id',
											'list'=>$nivelRiesgoList,
											),true); 
									?>
								</div>
								
							</div>
							
							<div class="row">
							
								<div class="col-md-3">
									<?php echo $this->renderPartial('_boolean',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'propietario_terreno',
											'valores'=>array(1=>'SI',0=>'NO'),
											),true); 
									?>
								</div>
								
								<div id="terrenoflag" <?php echo (($psm->propietario_terreno)?'':'style="display:none;"'); ?>>
									<?php echo $this->renderPartial('_estadoMunicipioParroquia',array(
											'model'=>$psm,
											'form'=>$form,
											'atributoEstado'=>'estado_terreno_id',
											'atributoMunicipio'=>'municipio_terreno_id',
											'atributoParroquia'=>'parroquia_terreno_id',
											'size'=>'3',
											),true); 
									?>
								</div>
								
							</div>
							
							<div class="row">
								<div class="col-md-12 text-center">
									
									<?php
										if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")) {
											echo CHtml::link(
												'<span class="glyphicon glyphicon-arrow-left"></span> Atras',
												 array('censo/index','cedulajf'=>$cedulajf),
												 array('class'=>'btn btn-primary btn-lg','confirm' => '¿Cancelar actualización para esta persona?')
											); 
										} elseif($primerRegistro==true) {
											echo CHtml::link(
												'<span class="glyphicon glyphicon-arrow-left"></span> Atras',
												 array('censo/chose'),
												 array('class'=>'btn btn-primary btn-lg','confirm' => '¿Cancelar actualización para esta persona?')
											); 
										}elseif($primerRegistro==false) {
											echo CHtml::link(
												'<span class="glyphicon glyphicon-arrow-left"></span> Atras',
												 array('censo/index'),
												 array('class'=>'btn btn-primary btn-lg','confirm' => '¿Cancelar actualización para esta persona?')
											); 
										}

									?>
										
									<a class="btn btn-success btn-lg" onclick="$('#xb').tab('show')">Siguiente <span class="glyphicon glyphicon-arrow-right" ></span></a>
								</div>
							</div>

						</div>
						
						<div class="tab-pane fade <?php echo ($activo==2?'in active':''); ?>" id="b">
							<hr/>
							
							<div class="row">
								<div class="col-md-12">
									<h4 align="center" style="background-color: rgba(51,102,255,0.2); border-radius: 10px; font-size: 22px;">En las casillas que se muestran a continuación seleccione los datos correspondientes a la dirección de solicitud</h4>
									<h3>Primera Opción</h3>
								</div>
							</div>
							
							<div class="row">
								<?php echo $this->renderPartial('_estadoMunicipioParroquiaCiudad',array(
										'model'=>$psmDireccionDestino,
										'form'=>$form,
										'atributoEstado'=>'[1]estado_id',
										'atributoMunicipio'=>'[1]municipio_id',
										'atributoParroquia'=>'[1]parroquia_id',
										'atributoCiudad'=>'[1]ciudad_destino_1',
										'size'=>'3',
										),true); 
								?>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<h3>Segunda Opción</h3>
								</div>
							</div>
							
							<div class="row">
								<?php echo $this->renderPartial('_estadoMunicipioParroquiaCiudad',array(
										'model'=>$psmDireccionDestino,
										'form'=>$form,
										'atributoEstado'=>'[1]estado_dos_id',
										'atributoMunicipio'=>'[1]municipio_dos_id',
										'atributoParroquia'=>'[1]parroquia_dos_id',
										'atributoCiudad'=>'[1]ciudad_destino_2',
										'size'=>'3',
										),true); 
								?>
							</div>


				
					

							<div class="row">
								<div class="col-md-12">
								<?php echo $form->labelEx($psmDireccion,"tipo_solicitud_id"); ?><br>
								<?php echo $form->textField($psmDireccion,"tipo_solicitud_id",array("value"=>$psmDireccionDestino->tipoSolicitud->nombre,"disabled"=>true,"style"=>"width:100%")); ?>
											
									
								</div>
							</div> 
						

							<div class="row">
								<div class="col-md-12">
									<?php
										$solicitudList = CHtml::listData(FamTipoSolicitud::model()->findAll(), 'tipo_solicitud_id', 'nombre')
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'tipo_solicitud_id2',
											'list'=>$solicitudList,
											),true); 
									?>
									
								</div>
							</div> 
						
							<div class="row">
								<div class="col-md-12 text-center">
									<a class="btn btn-primary btn-lg" onclick="$('#xa').tab('show')"><span class="glyphicon glyphicon-arrow-left"></span> Atras</a>
									<a class="btn btn-success btn-lg" onclick="$('#xc').tab('show')">Siguiente <span class="glyphicon glyphicon-arrow-right" ></span></a>
								</div>
							</div>
						</div>
						<?php endif; ?>
						
						<div class="tab-pane fade <?php echo ($activo==3?'in active':''); ?>" id="c">
							<hr/>
							
							<?php if(!$jefefamilia): ?>
							<div class="row">
							
								<div class="col-md-4">
									<?php
										$parentescoList = CHtml::listData(FamParentesco::model()->findAll('parentesco_id !=12  and estatus=true'), 'parentesco_id', 'nombre');
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'parentesco_id',
											'list'=>$parentescoList,
											),true); 
									?>
								</div>
							
							</div>
							<?php endif; ?>
							
							<div class="row">
								
								
								<div class="col-md-4">
									<?php
										$edoCivilList = CHtml::listData(FamEdoCivil::model()->findAll('st_edo_civil=true ORDER BY nombre'), 'edo_civil_id', 'nombre')
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'edo_civil_id',
											'list'=>$edoCivilList,
											),true); 
									?>
								</div>
								
								<div class="col-md-4">
									<div class="form-group <?php if(isset($_POST['Censado'])) echo $psm->hasErrors('fe_nac')?'has-error':'has-success'; ?>">
										<?php echo $form->labelEx($psm,"fe_nac"); ?>
										<?php echo $form->textField($psm,"fe_nac",array('class'=>'form-control date'));?>
										<?php echo $form->error($psm,'fe_nac',array('class'=>'help-block text-center')); ?>
									</div>
								</div>
								
								
								
								<div class="col-md-4">
									<?php echo $this->renderPartial('_boolean',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'sexo',
											'valores'=>array('M'=>'Masculino','F'=>'Femenino'),
											),true); 
									?>
								</div>
								
							</div>

						
							<div class="row">
								<div class="col-md-6">
									<?php
										$paisList = CHtml::listData(Pais::model()->findAll(array('order'=>'despai')), 'id_pais', 'despai')
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'id_pais',
											'list'=>$paisList,
											),true); 
									?>
								</div>
							
								<div class="col-md-6">
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'tiempo_pais',
											'list'=>$psm->fecha(),
											),true); 
									?>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12 text-center">
									
									<?php if(!$jefefamilia): ?>
									
										<?php
											if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")) {
												echo CHtml::link(
													'<span class="glyphicon glyphicon-arrow-left"></span> Atras',
													 array('censo/index','cedulajf'=>$cedulajf),
													 array('class'=>'btn btn-primary btn-lg','confirm' => '¿Cancelar actualización para esta persona?')
												); 
											} if($primerRegistro){
												echo CHtml::link(
													'<span class="glyphicon glyphicon-arrow-left"></span> Atras',
													 array('censo/chose'),
													 array('class'=>'btn btn-primary btn-lg','confirm' => '¿Cancelar actualización para esta persona?')
												); 
											}else {
												echo CHtml::link(
													'<span class="glyphicon glyphicon-arrow-left"></span> Atras',
													 array('censo/index'),
													 array('class'=>'btn btn-primary btn-lg','confirm' => '¿Cancelar actualización para esta persona?')
												); 
											}
										?>

									<?php else: ?>
										<a class="btn btn-primary btn-lg" onclick="$('#xb').tab('show')"><span class="glyphicon glyphicon-arrow-left"></span> Atras</a>
									<?php endif; ?>
									<a class="btn btn-success btn-lg" onclick="$('#xd').tab('show')">Siguiente <span class="glyphicon glyphicon-arrow-right" ></span></a>
								</div>
							</div>

						</div>
						
						<div class="tab-pane fade <?php echo ($activo==4?'in active':''); ?>" id="d">
							<hr/>
							<div class="row">
								<div class="col-md-4">
									<?php echo $this->renderPartial('_field',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'e_mail',
											),true); 
									?>
								</div>
							
								<div class="col-md-4">
									<div class="text-center form-group <?php if(isset($_POST['Censado'])) echo $psm->hasErrors('telefono_1')?'has-error':'has-success'; ?>">
										<?php echo $form->labelEx($psm,"telefono_1"); ?>
										<?php $this->widget('CMaskedTextField', array('model' => $psm,
																			  'attribute' => "telefono_1",
																			  'mask' => '(9999)-999-99-99',
																			  'htmlOptions' => array('placeholder'=>'(9999)-999-99-99','class'=>'form-control')));?>
										<?php echo $form->error($psm,'telefono_1',array('class'=>'help-block text-center')); ?>
									</div>
								</div>
							
								<div class="col-md-4">
									<div class="text-center form-group <?php if(isset($_POST['Censado'])) echo $psm->hasErrors('telefono_2')?'has-error':'has-success'; ?>">
										<?php echo $form->labelEx($psm,"telefono_2"); ?>
										<?php $this->widget('CMaskedTextField', array('model' => $psm,
																			  'attribute' => "telefono_2",
																			  'mask' => '(9999)-999-99-99',
																			  'htmlOptions' => array('placeholder'=>'(9999)-999-99-99','class'=>'form-control')));?>
										<?php echo $form->error($psm,'telefono_2',array('class'=>'help-block text-center')); ?>
									</div>
								</div>
							</div>  	

							<div class="row">
								<div class="col-md-12 text-center">
									<a class="btn btn-primary btn-lg" onclick="$('#xc').tab('show')"><span class="glyphicon glyphicon-arrow-left"></span> Atras</a>
									<a class="btn btn-success btn-lg" onclick="$('#xe').tab('show')">Siguiente <span class="glyphicon glyphicon-arrow-right" ></span></a>
								</div>
							</div>
							
						</div>
						
						<div class="tab-pane fade <?php echo ($activo==5?'in active':''); ?>" id="e">
							<hr/>
							
							<div class="row">
							
								<div class="col-md-3">
									<?php echo $this->renderPartial('_boolean',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'estudia',
											'valores'=>array(1=>'SI',0=>'NO'),
											),true); 
									?>
								</div>
								
								<div id="estudiaflag" <?php echo (($psm->estudia)?'':'style="display:none;"'); ?>>
									<?php echo $this->renderPartial('_estadoMunicipioParroquia',array(
											'model'=>$psm,
											'form'=>$form,
											'atributoEstado'=>'estado_estudia_id',
											'atributoMunicipio'=>'municipio_estudia_id',
											'atributoParroquia'=>'parroquia_estudia_id',
											'size'=>'3',
											),true); 
									?>
								</div>
								
							</div>
							
							<div class="row">
							
								
								<div class="col-md-4">
									<?php
										$gradoInstruList = CHtml::listData(FamGradoInstruccion::model()->findAll('estatus=true'), 'grado_instruccion_id', 'nombre');
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'grado_instruccion_id',
											'list'=>$gradoInstruList,
											),true); 
									?>
								</div>
								
								
								
								<div class="col-md-4">
									<?php
										$enfermedadList = CHtml::listData(FamEnfermedad::model()->findAll('estatus=TRUE order by nombre asc'), 'enfermedad_id', 'nombre');
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'enfermedad_id',
											'list'=>$enfermedadList,
											),true); 
									?>
								</div>
								
								<div class="col-md-3" id="sexoflag" <?php echo (($psm->sexo=='F')?'':'style="display:none;"'); ?>>
									<?php echo $this->renderPartial('_boolean',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'estado_embarazo',
											'valores'=>array(1=>'SI',0=>'NO'),
											),true);
									?>
								</div>
							
							</div>
							
							<div class="row">
							
								<div class="col-md-3">
									<?php echo $this->renderPartial('_boolean',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'discapacitado',
											'valores'=>array(1=>'SI',0=>'NO'),
											),true); 
									?>
								</div>
								
								<div class="col-md-3" id="discapacitadoflag" <?php echo (($psm->discapacitado)?'':'style="display:none;"'); ?>>
									<?php echo $this->renderPartial('_field',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'nro_conapdis',
											),true); 
									?>
								</div>
								
							</div>
							
							<div class="row">
								
								<div class="col-md-3">
									<?php echo $this->renderPartial('_boolean',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'trabaja',
											'valores'=>array(1=>'SI',0=>'NO'),
											),true); 
									?>
								</div>
								
								<div id="trabajaflag" <?php echo (($psm->trabaja)?'':'style="display:none;"'); ?>>
									<?php echo $this->renderPartial('_estadoMunicipioParroquia',array(
											'model'=>$psm,
											'form'=>$form,
											'atributoEstado'=>'estado_trabaja_id',
											'atributoMunicipio'=>'municipio_trabaja_id',
											'atributoParroquia'=>'parroquia_trabaja_id',
											'size'=>'3',
											),true); 
									?>
								</div>
							</div>
							
							<div class="row">
								
								<div class="col-md-3">
									<?php
										$trabajoList = CHtml::listData(PerTrabajo::model()->findAll('tp_sector=3 ORDER BY nb_trabajo'), 'trabajo_id', 'nb_trabajo')
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'trabajo_id',
											'list'=>$trabajoList,
											),true); 
									?>
								</div>
								
								<div class="col-md-4" id="publicaflag" <?php echo (($psm->trabajo_id=='21')?'':'style="display:none;"'); ?>>
									<?php
										$tiempoAdmList = CHtml::listData(FamTiempoAdmpublica::model()->findAll('estatus=TRUE ORDER BY id_fam_tiempo_admpublica'), 'id_fam_tiempo_admpublica', 'nombre')
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'tiempo_admpublica_id',
											'list'=>$tiempoAdmList,
											),true); 
									?>
								</div>
								

								
								<div class="col-md-3">
									<?php
										$clasificacionList = CHtml::listData(ClasificacionIngreso::model()->findAll('st_clasificacion=true order by nb_clasificacion asc'), 'id_clasificacion', 'nb_clasificacion');
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'id_clasificacion',
											'list'=>$clasificacionList,
											),true); 
									?>

								</div>
								
							</div>
							
							
							<div class="row">
							
								<div class="col-md-4">

										<?php
											$ocupacionList = CHtml::listData(FamOcupacion::model()->findAll('estatus=true ORDER BY nombre'), 'ocupacion_id', 'nombre');
										?>
										<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'ocupacion_id',
											'list'=>$ocupacionList,
											),true); 
										?>

								</div>
								
								<div class="col-md-4">
									<?php
										$sectorSocialList = CHtml::listData(FamSectorSocial::model()->findAll('st_sector_social=true ORDER BY descripcion'), 'id_fam_sector_social', 'descripcion');
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'fam_sector_social_id',
											'list'=>$sectorSocialList,
											),true); 
									?>
								</div>
								
							</div>

			

						
							<div class="row">
								<div class="col-md-3">
									<?php echo $this->renderPartial('_field',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'ingreso_mensual',
											),true); 
									?>
								</div>

								<div class="col-md-3">
									<?php echo $this->renderPartial('_field',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'honorarios_profesionales',
											),true); 
									?>
								</div>
							
								<div class="col-md-3">
									<?php echo $this->renderPartial('_field',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'bono_alimentacion',
											),true); 
									?>
								</div>
							
								<div class="col-md-3">
									<?php echo $this->renderPartial('_field',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'otros_ingresos',
											),true); 
									?>
								</div>
							</div>

						 
							<div class="row">
								<div class="col-md-6">
									<?php echo $this->renderPartial('_fieldnum',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'serial_cdlp',
											
											),true); 
									?>
								</div>
							<?php if(
								($psm->parentesco_id==12 && !Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")) 
								|| 
								($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")) ) { ?>
								
								<div class="col-md-6">
									<?php echo $this->renderPartial('_field2',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'codigo_cdlp',
											),true); 
									?>
								</div>
								<?php } elseif($psm->parentesco_id!=12||($cedulajf==NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")) ) { ?>

									<div class="col-md-6">
									<?php echo $this->renderPartial('_field',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'codigo_cdlp',
											),true); 
									?>
								</div>	 	

									<?php } ?>
							</div>

						
							<div class="row">
							
								<div class="col-md-3">
									<?php echo $this->renderPartial('_boolean',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'insc_faov',
											'valores'=>array(1=>'SI',0=>'NO'),
											),true); 
									?>
								</div>
								
								<div class="col-md-3">
									<?php echo $this->renderPartial('_boolean',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'mision',
											'valores'=>array(1=>'SI',0=>'NO'),
											),true); 
									?>
								</div>

								<div class="col-md-6" id="misionflag" <?php echo (($psm->mision)?'':'style="display:none;"'); ?>>
									<?php
										$misionList = CHtml::listData(FamMision::model()->findAll('estatus=true ORDER BY nombre'), 'mision_id', 'nombre');
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'mision_id',
											'list'=>$misionList,
											),true); 
									?>
								</div>
								
							</div> 

							<!-- NUevooo  -->

							<div class="row">
															
								<div class="col-md-3">
									<?php echo $this->renderPartial('_boolean',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'mision2',
											'valores'=>array(1=>'SI',0=>'NO'),
											),true); 
									?>
								</div>

								<div class="col-md-6" id="mision2flag" <?php echo (($psm->mision2)?'':'style="display:none;"'); ?>>
									<?php
										$misionList = CHtml::listData(FamMision::model()->findAll('estatus=true ORDER BY nombre'), 'mision_id', 'nombre');
									?>
									<?php echo $this->renderPartial('_dropDown',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'mision_id2',
											'list'=>$misionList,
											),true); 
									?>
								</div>
								
							</div> 	

							<div class="row">
							
								<div class="col-md-3">
									<?php echo $this->renderPartial('_boolean',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'pertenece_avv',
											'valores'=>array(1=>'SI',0=>'NO'),
											),true); 
									?>
								</div>
								
								<div class="col-md-3" id="pertenece_avvflag" <?php echo (($psm->pertenece_avv)?'':'style="display:none;"'); ?>>
									<?php echo $this->renderPartial('_field',array(
											'model'=>$psm,
											'form'=>$form,
											'atributo'=>'nombre_avv',
											),true); 
									?>
								</div>
								
							</div>


							<!-- NUevooo  --> 

							<div class="row">
								<div class="col-md-12 text-center">
									<a class="btn btn-primary btn-lg" onclick="$('#xd').tab('show')"><span class="glyphicon glyphicon-arrow-left"></span> Atras</a>
									<button id="registrar" class="btn btn-success btn-lg" type="submit" name="evento">
										<span class="glyphicon glyphicon-floppy-disk "></span>
										Guardar
									</button>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			

			<?php $this->endWidget(); ?>

		</div>
	</div>
	<div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
	  <div class="modal-dialog modal-sm" role="document">
		<div class="modal-content" style="width: 500px">
			<div class="modal-header text-center">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">DATOS INGRESADOS SON INVALIDOS</h4>
			</div>
			<div class="modal-body text-center" style="width: 500px; text-align: left; color: red">
			             
			             <?php echo $form->errorSummary($psm)?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
			</div>
		</div>
	  </div>
	</div>
</div>


<script type="text/javascript">

	$(document).ready(function() {
		
		$("#buscarpersona").click(function() {
		  
			$.ajax({
				url:"<?php echo $this->createUrl('Cedula'); ?>",
				cache: false,
				type: "POST",
				dataType: "json",
				data: ({cedula:$('#Censado_cedula').val(),familia:''}),
				beforeSend: function(xkr){

				},
				success: function(data){
					//console.log(data);
					if(data.nacionalidad!='') {
						$('#Censado_nacionalidad').val(data.nacionalidad);
						$('#button_nacionalidad').text(data.nacionalidad)
					}
					$('#Censado_nombre1').val(data.nombre1);
					$('#Censado_nombre2').val(data.nombre2);
					$('#Censado_apellido1').val(data.apellido1);
					$('#Censado_apellido2').val(data.apellido2);
					$('#Censado_mensaje').val(data.mensaje);
				}
			});
		
		});
		
		$('input[type=radio][name=Censado\\[trabaja\\]]').on('change', function() {
			if($(this).val()=='1') {
				$('#trabajaflag').show('fast');
			} else {
				$('#trabajaflag').hide();
			}
		});
		
		$('input[type=radio][name=Censado\\[estudia\\]]').on('change', function() {
			if($(this).val()=='1') {
				$('#estudiaflag').show('fast');
			} else {
				$('#estudiaflag').hide();
			}
		});
		
		$('input[type=radio][name=Censado\\[discapacitado\\]]').on('change', function() {
			if($(this).val()=='1') {
				$('#discapacitadoflag').show('fast');
			} else {
				$('#discapacitadoflag').hide();
			}
		});
		
		$('input[type=radio][name=Censado\\[propietario_terreno\\]]').on('change', function() {
			if($(this).val()=='1') {
				$('#terrenoflag').show('fast');
			} else {
				$('#terrenoflag').hide();
			}
		});
		
		$('input[type=radio][name=Censado\\[mision\\]]').on('change', function() {
			if($(this).val()=='1') {
				$('#misionflag').show('fast');
			} else {
				$('#misionflag').hide();
			}
		});

		$('input[type=radio][name=Censado\\[mision2\\]]').on('change', function() {
			if($(this).val()=='1') {
				$('#mision2flag').show('fast');
			} else {
				$('#mision2flag').hide();
			}
		});
		
		$('input[type=radio][name=Censado\\[sexo\\]]').on('change', function() {
			if($(this).val()=='F') {
				$('#sexoflag').show('fast');
			} else {
				$('#sexoflag').hide();
			}
		});
		
		$('#Censado_trabajo_id').on('change', function() {
			if($(this).val()=='21') {
				$('#publicaflag').show('fast');
			} else {
				$('#publicaflag').hide();
			}
		});

		$('input[type=radio][name=Censado\\[pertenece_avv\\]]').on('change', function() {
			if($(this).val()=='1') {
				$('#pertenece_avvflag').show('fast');
			} else {
				$('#pertenece_avvflag').hide();
			}
		});
		
		
	});

</script>
<script>

		   function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }
 

    function soloNumeros(e){

	var key = window.Event ? e.which : e.keyCode

	return (key >= 48 && key <= 57)

}
</script>