<?php
	$estadoAtt = preg_replace('/\[\w+\]/', '', $atributoEstado);
	$municipioAtt = preg_replace('/\[\w+\]/', '', $atributoMunicipio);
	$parroquiaAtt = preg_replace('/\[\w+\]/', '', $atributoParroquia);
?>
<div class="col-md-<?php echo $size;?>">
	<div class="form-group <?php if(isset($_POST[get_class($model)])) echo $model->hasErrors($estadoAtt)?'has-error':'has-success'; ?>">
		<?php echo $form->labelEx($model,$atributoEstado); ?>
		<?php
		$estadoList = CHtml::listData(GeoEstado::model()->findAll(array('condition'=>'nombre not like :ubicacion AND nombre NOT LIKE \'OTRO\' AND nombre NOT LIKE \'OTRA\'',
														'params'=>array(':ubicacion'=>"SIN UBICACION"),
														'order'=>'nombre asc'
													)), 
													'geo_estado_id', 'nombre');
		?>
		<?php echo $form->dropDownList($model,$atributoEstado,$estadoList,array(
			'empty'=>'--SELECCIONE--',
			'placeholder'=>'--SELECCIONE--',
			'class'=>'form-control',
			'style'=>'width:100%',
			'ajax' => array(
				'type'=>'POST', //request type
				'data'=>array('estado_id'=>'js:$(this).val()'),
				'url'=>CController::createUrl('BuscarMunicipio'), //url to call.
				'update'=>'#'.CHtml::activeId($model,$atributoMunicipio), //selector to update
				'complete'=>'function(data,status){
					//$.unblockUI();
				}',
				'beforeSend'=>'function(data,status){

					
					jQuery("#'.Chtml::activeId($model,$atributoMunicipio).'").html(\''.CHtml::tag('option',array('value' => ''),'--SELECCIONE--',true).'\');
					jQuery("#'.Chtml::activeId($model,$atributoMunicipio).'").select2("val", "");
					
					
					jQuery("#'.Chtml::activeId($model,$atributoParroquia).'").html(\''.CHtml::tag('option',array('value' => ''),'--SELECCIONE--',true).'\');
					jQuery("#'.Chtml::activeId($model,$atributoParroquia).'").select2("val", "");

					

					
					if($("#'.CHtml::activeId($model,$atributoEstado).'").val()=="") {
						return false;
					}
				}',
				),
			)); ?>
		<?php echo $form->error($model,$atributoEstado,array('class'=>'help-block text-center')); ?>
	</div>
</div>

<div class="col-md-<?php echo $size;?>">
	<div class="form-group <?php if(isset($_POST[get_class($model)])) echo $model->hasErrors($municipioAtt)?'has-error':'has-success'; ?>">
		<?php echo $form->labelEx($model,$atributoMunicipio); ?>
		<?php
		if($model->$estadoAtt=='') $model->$estadoAtt=NULL;
		$municipioList = CHtml::listData(GeoMunicipio::model()->findAll(array('condition'=>'nombre not like :ubicacion AND geo_estado_id=:geo_estado_id AND nombre NOT LIKE \'OTRO\' AND nombre NOT LIKE \'OTRA\'',
														'params'=>array(':ubicacion'=>"SIN UBICACION",':geo_estado_id'=>$model->$estadoAtt),
														'order'=>'nombre asc'
													)), 
													'geo_municipio_id', 'nombre');
		?>
		<?php echo $form->dropDownList($model,$atributoMunicipio,$municipioList,array(
			'empty'=>'--SELECCIONE--',
			'placeholder'=>'--SELECCIONE--',
			'class'=>'form-control',
			'style'=>'width:100%',
			'ajax' => array(
			'type'=>'POST', //request type
			'data'=>array('municipio_id'=>'js:$(this).val()'),
			'url'=>CController::createUrl('BuscarParroquia'), //url to call.
			'beforeSend'=>'function(data,status){
				
				jQuery("#'.Chtml::activeId($model,$atributoParroquia).'").html(\''.CHtml::tag('option',array('value' => ''),'--SELECCIONE--',true).'\');
				jQuery("#'.Chtml::activeId($model,$atributoParroquia).'").select2("val", "");

				if($("#'.CHtml::activeId($model,$atributoMunicipio).'").val()=="") {
					return false;
				}

			}',
			'update'=>'#'.CHtml::activeId($model,$atributoParroquia), //selector to update
			),
			)); ?>
		<?php echo $form->error($model,$atributoMunicipio,array('class'=>'help-block text-center')); ?>
	</div>
</div>
<div class="col-md-<?php echo $size;?>">
	<?php
		if($model->$municipioAtt=='') $model->$municipioAtt=NULL;
		$parroquiaList = CHtml::listData(GeoParroquia::model()->findAll(array('condition'=>'nombre not like :ubicacion AND geo_municipio_id=:geo_municipio_id AND nombre NOT LIKE \'OTRO\' AND nombre NOT LIKE \'OTRA\'',
														'params'=>array(':ubicacion'=>"SIN UBICACION",':geo_municipio_id'=>$model->$municipioAtt),
														'order'=>'nombre asc'
													)), 
													'geo_parroquia_id', 'nombre');
	?>
	<?php echo $this->renderPartial('_dropDown',array(
			'model'=>$model,
			'form'=>$form,
			'atributo'=>$atributoParroquia,
			'list'=>$parroquiaList,
			),true); 
	?>
</div>