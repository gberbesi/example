<style>
  .centrar{
	  position: absolute;
	  top: 50%; 
	  left: 50%;
	  transform: translate(-50%, -50%);
	}
</style>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'censado-codigo',
	'enableAjaxValidation'=>false,
)); ?>

<?php foreach(Yii::app()->user->getFlashes() as $key => $message) {
	        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	    }?>

	<div class="centrar" style="padding:10px; 
						overflow:hidden;
						border:2px solid #AD1818;
						-webkit-border-radius: 4px;
						-moz-border-radius: 4px;
						border-radius: 4px;
						-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(128, 129, 129, 0.6);
						-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(128, 129, 129, 0.6);
						box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(128, 129, 129, 0.6); 
						width:600px; 
						margin: 0px auto; 
						background: #F5F5F5;
						align:center">

		<h1 align="center">Verificación de Datos</h1>
		<p class="note" style="text-align: center">Los Campos con <span class="required">*</span> son requeridos.</p>
		<hr>
		<br>
	<center><b>  Sr(a). Introduzca el código del certificado </b> </center>
		<br>

	<div class="row"  style="text-align:center;">
		<?php echo $form->labelEx($model,'codigo'); ?>
		<?php echo $form->textField($model,'codigo', array('placeholder'=>'Código del certificado','style'=>'text-align:center')); ?>
		<?php echo $form->error($model,'codigo'); ?>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
			<div class="row" style="text-align:center;">
				<?php echo $form->labelEx($model,'verifyCode'); ?>
				<div>
				
				<?php echo $form->textField($model,'verifyCode',array('style'=>'text-align:center','placeholder'=>'Introduzca la imagen de abajo','autocomplete'=>'off','title'=>'Introduzca el código que aparece en la imagen. Solo letras. No distingue de mayúsculas y minúsculas. Para cambiar el código haga click en la imagen')); ?>
				</div>
				<?php echo $form->error($model,'verifyCode'); ?>
				<div style="width:160px;display:inline-block;" toolt="toolt" title="Haga Click para obtener otra imagen de verificacion">
					<?php $this->widget('CCaptcha',array(
							'clickableImage'=>true,
							'showRefreshButton'=>false,
							)); ?>
				</div>
			</div>
	<?php endif; ?>

<hr>

		
	<div style="text-align:center;" class="row buttons">
		<?php echo CHtml::submitButton('Consultar', array('class'=>'btn btn-primary')); ?>
	</div>


	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->

