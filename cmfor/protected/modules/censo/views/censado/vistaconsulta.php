	<style>

	.centrar{
  position: absolute;
  top: 50%; 
  left: 50%;
  transform: translate(-50%, -50%);
	}

	</style>


	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/vistatabla.css" />
					     
					 		<div class="centrar" style="padding:10px; 
										overflow:hidden;
										border:2px solid #940000;
										-webkit-border-radius: 5px;
										-moz-border-radius: 4px;
										border-radius: 4px;
										-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(128, 129, 129, 0.6);
										-moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(128, 129, 129, 0.6);
										box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(128, 129, 129, 0.6); 
										width:900px; 
										margin: 0px auto;
										padding-top:20px;
										padding-left: 40px; 
										padding-right:-20px;
										padding-bottom: 30px;
										background: #F5F5F5;
										align:center;">	

<div align="right" style="width:890px">
<tr><?php echo CHtml::link(CHtml::image('images/regre.png','ATRAS'),array('createcodig'),array('title'=>'Volver')); ?></tr>
</div>

<center><h3>Información del Beneficiario: <?php echo $consultaBeneficio->nombre1;  ?> <?php echo $consultaBeneficio->apellido1;  ?></h3></center>


<br>
	
		<table class="detail-view"style="width:850px;align:center;">
			<tr class="even" >
				<td style="text-align:center;width: 100%;font: sans-serif;font-size: 13px;font-weight: bold;" ><?php echo "Datos Personales del Beneficiario: \n\n"?></td>
			</tr>
		</table>

		<table class="detail-view"style="width:850px;align:center;">
		<tr class="even" >
		<td style="text-align:center;width: 50%;font: sans-serif;font-size: 13px;font-weight: bold;" ><?php echo "Nombres: \n\n"?><?php echo '<font color="#9b2929">'.strtoupper($consultaBeneficio->nombre1)." ".strtoupper($consultaBeneficio->nombre2).'</font>'; ?> </td> 
		<td style="text-align:center;width: 50%;font: sans-serif;font-size: 13px;font-weight: bold;" ><?php echo "Apellidos: \n\n"?><?php echo '<font color="#9b2929">'.strtoupper($consultaBeneficio->apellido1)." ".strtoupper($consultaBeneficio->apellido2).'</font>'; ?> </td> 
		</tr>
		</table>

		<table class="detail-view"style="width:850px;align:center;">
		<tr class="even" >
			<td style="text-align:center;width: 50%;font: sans-serif;font-size: 13px;font-weight: bold;" ><?php echo "Cédula de Identidad: \n\n"?><?php echo '<font color="#9b2929">'.strtoupper($consultaBeneficio->cedula).'</font>'; ?> </td>
		</tr>
		</table>


		<table class="detail-view"style="width:850px;align:center;">
			<tr class="even" >
				<td style="text-align:center;width: 100%;font: sans-serif;font-size: 13px;font-weight: bold;" ><?php echo "Datos de Pre-Asignación: \n\n"?></td>
			</tr>
		</table>

		<table class="detail-view"style="width:850px;align:center;">
		<tr class="even" >
		<td style="text-align:center;width: 33%;font: sans-serif;font-size: 13px;font-weight: bold;" ><?php echo "Estado: \n\n"?><?php echo '<font color="#9b2929">'.strtoupper($consultaBeneficio->estado).'</font>'; ?> </td>
		<td style="text-align:center;width: 33%;font: sans-serif;font-size: 13px;font-weight: bold;" ><?php echo "Municipio: \n\n"?><?php echo '<font color="#9b2929">'.strtoupper($consultaBeneficio->municipio).'</font>'; ?> </td>  
		<td style="text-align:center;width: 33%;font: sans-serif;font-size: 13px;font-weight: bold;" ><?php echo "Parroquia: \n\n"?><?php echo '<font color="#9b2929">'.strtoupper($consultaBeneficio->parroquia).'</font>'; ?> </td>
		</tr>
		</table>

		<table class="detail-view"style="width:850px;align:center;">
		<tr class="even" >
		<td style="text-align:center;width: 50%;font: sans-serif;font-size: 13px;font-weight: bold;" ><?php echo "Urbanización: \n\n"?><?php echo '<font color="#9b2929">'.strtoupper($consultaBeneficio->desarrollo).'</font>'; ?> </td>
		<td style="text-align:center;width: 50%;font: sans-serif;font-size: 13px;font-weight: bold;" ><?php echo "Torre: \n\n"?><?php echo '<font color="#9b2929">'.strtoupper($consultaBeneficio->geo_nombre_edificacion_destino).'</font>'; ?> </td>
		</tr>
		</table>

		
</div>