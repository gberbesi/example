<?php
/* @var $this CensadoController */
/* @var $model Censado */
/* @var $form CActiveForm */
?>
<style>
	.si {
		background-color: #5cb85c;
		padding: .6em;
		color: #fff;
		text-align: center;
		border-radius: .25em;
		font-weight: bold;
	}
	
	.no {
		background-color: #d9534f;
		padding: .6em;
		color: #fff;
		text-align: center;
		border-radius: .25em;
		font-weight: bold;
	}
</style>        
<div class="form">
  
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'censado-form',
	
	'enableAjaxValidation'=>false,
)); ?>
    
      <?php if(Yii::app()-> user-> hasFlash('error') || Yii::app()-> user-> hasFlash('success')):
	    foreach(Yii::app()->user->getFlashes() as $key => $message) {
	        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	    }
	endif; ?>
    
    	<?php foreach(Yii::app()->user->getFlashes() as $key => $message) {
	        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	    }?>
    <div class="flash-notice"><center>Introduzca los datos solicitados, para realizar la verificación de Correo del <b>USUARIO</b> </center></div>
	
    <div id="informacion"></div>
    
	<fieldset>
	<table>
		<tr>
				<td>	
					<?php echo $form->labelEx($modelPersona,'nacionalidad'); ?>
					<?php $this->widget('ext.select2.ESelect2', array(
															    'model'=>$modelPersona,
															    'attribute'=>'nacionalidad',
															    'data'=>array ('V'=> 'Venezolano', 'E'=> 'Extranjero'),
																//'options'=>array('allowClear'=>true,),	    
															    'htmlOptions'=>array('placeholder'=>'Seleccione','style'=>'width:150px;'),));	?>
						<?php echo $form->error($modelPersona,'nacionalidad'); ?>
                                </td>
                <td>
					<?php echo $form->labelEx($modelPersona,'cedula'); ?>
					<?php echo $form->textField($modelPersona,'cedula',array('placeholder'=>'Cédula')); ?>
					<?php echo $form->error($modelPersona,'cedula'); ?>
					</td>
				 <td>	
				 
					<button onClick="buscarCedula();return false" class="add-on"><i class="icon-search"></i></button>
				</td>
                </tr>
                <tr>
			<td>      
				<div class="row">
					<?php echo $form->labelEx($modelPersona,"nombre1"); ?>
					<?php echo $form->textField($modelPersona,"nombre1", array('readOnly'=>true,'placeholder'=>'Primer Nombre'	)); ?> 
					<?php echo $form->error($modelPersona,'nombre1'); ?>
                        </td>
                        <td>      
				<div class="row">
					<?php echo $form->labelEx($modelPersona,"nombre2"); ?>
					<?php echo $form->textField($modelPersona,"nombre2", array('readOnly'=>true,'placeholder'=>'Primer Nombre'	)); ?> 
					<?php echo $form->error($modelPersona,'nombre2'); ?>
                        </td>
                        <td>      
				<div class="row">
					<?php echo $form->labelEx($modelPersona,"apellido1"); ?>
					<?php echo $form->textField($modelPersona,"apellido1", array('readOnly'=>true,'placeholder'=>'Primer Nombre',)); ?> 
					<?php echo $form->error($modelPersona,'apellido1'); ?>
                        </td>
                        <td>      
				<div class="row">
					<?php echo $form->labelEx($modelPersona,"apellido2"); ?>
					<?php echo $form->textField($modelPersona,"apellido2", array('readOnly'=>true,'placeholder'=>'Primer Nombre',)); ?> 
					<?php echo $form->error($modelPersona,'apellido2'); ?>
                        </td>
                        
                        
                      
	       </tr>
               
                <tr>
			<td>      
				<div class="row">
                                            <?php echo $form->labelEx($modelPersona,'censo0800'); ?>
                                            <div id="censo0800"> &nbsp;  </div>
                                </div>
                        </td>
                        <td>      
				<div class="row">
                                            <?php echo $form->labelEx($modelPersona,'censoGmvv'); ?>
                                            <div id="censoGmvv"> &nbsp; </div>
                                </div>
                        </td>
                        <td>      

                        </td>
	       </tr>               
               
           
		
			<tr>
                           
				<td>
					<div class="row">
						<?php echo $form->labelEx($modelPersona,'e_mail'); ?>
						<?php //echo $form->textField($modelPersona,'e_mail',array('style'=>'width:100%')); ?>
                                                <?php echo $form->textField($modelPersona,'e_mail'); ?>
						<?php echo $form->error($modelPersona,'e_mail'); ?>
					</div>
				</td>                              
				<td> 
					<div class="row">

						<?php echo $form->labelEx($modelPersona,'contrasena'); ?>
						<?php echo $form->checkBox($modelPersona,'contrasena'); ?>
						<?php echo $form->error($modelPersona,'contrasena'); ?>

					</div>					
				</td>
                              
				
			</tr>
		</table>
		
            
            	<div class="row buttons" style="text-align:center">
                    <?php echo CHtml::submitButton($modelPersona->isNewRecord ? 'Modificar' : 'Modificar'); ?>
                </div>
	</fieldset>
	

<?php $this->endWidget(); ?>

</div><!-- form -->
<script>
$("#<?php echo CHtml::activeId($modelPersona,"nacionalidad");?>").change(function(){
		buscarCedula();
	});
$("#<?php echo CHtml::activeId($modelPersona,"cedula");?>").change(function(){
		//buscarCedula();

});



function buscarCedula() {
	
	var cedula = $("#<?php echo CHtml::activeId($modelPersona,"cedula");?>").val();
	var nac = $("#<?php echo CHtml::activeId($modelPersona,"nacionalidad");?>").val();
	var fe_nac = $("#<?php echo CHtml::activeId($modelPersona,"fe_nac");?>").val();
var lugar = $("#<?php echo CHtml::activeId($modelPersona,"lugar");?>").val();
	if(cedula!='' && (nac=='V' || nac=='E' ) && !isNaN(cedula) && fe_nac !='' && lugar!=''){
	<?php echo Chtml::ajax(array(
			                    'type'=>'POST',
								'dataType'=>'json',
								'data'=>array('cedula'=>  'js:$("#'.CHtml::activeId($modelPersona, "cedula").'").val()',
												'nac'=>   'js:$("#'.CHtml::activeId($modelPersona, "nacionalidad").'").val()',
												'fe_nac'=>'js:$("#'.CHtml::activeId($modelPersona,"fe_nac").'").val()',
												'lugar'=>   'registro',), //url to call.
												


			                    'url'=>CController::createUrl('CedulaCensado'),
								'beforeSend'=>'function(xhr) {
													$("#'.CHtml::activeId($modelPersona, "nombre1").'").attr("value","");
                                                                                                        $("#'.CHtml::activeId($modelPersona, "nombre2").'").attr("value","");
													$("#'.CHtml::activeId($modelPersona, "apellido1").'").attr("value","");
                                                                                                        $("#'.CHtml::activeId($modelPersona, "apellido2").'").attr("value","");
                                                                                                        $("#'.CHtml::activeId($modelPersona, "e_mail").'").attr("value","");
                                                                                                        $("#censo0800").html("&nbsp;");
                                                                                                        $("#censoGmvv").html("&nbsp;");                                                                                                            
                                                                                                           
            $("#informacion").html("");$.blockUI({ message: $("#domMessage")});}',
								'complete'=>'function(xhr) {$("#imasg").hide();$.unblockUI();}',
			                    'success'=>'function(data,status){
											//$.unblockUI();
											$("#imasg_").hide();
											if(data.mensaje!="") {
												$("#informacion").html("");
												$("#persona").html("");
												if(data.tipo =="error"){
                                                                                                    $("#informacion").html(data.mensaje);
                                                                                                }
											}
										    else {
										      	if(data.datos!=""){
													$("#persona").html(data.datos);
													$("#'.CHtml::activeId($modelPersona, "nombre1").'").attr("value",data.nombre1);
                                                                                                        $("#'.CHtml::activeId($modelPersona, "nombre2").'").attr("value",data.nombre2);
													$("#'.CHtml::activeId($modelPersona, "apellido1").'").attr("value",data.apellido1);
                                                                                                        $("#'.CHtml::activeId($modelPersona, "apellido2").'").attr("value",data.apellido2);
													$("#'.CHtml::activeId($modelPersona, "e_mail").'").attr("value",data.correo);
													$("#'.CHtml::activeId($modelPersona, "telefono_1").'").attr("value",data.telefono1);
                                                                                                        $("#censo0800").html(data.censo0800);
                                                                                                        $("#censoGmvv").html(data.censoGmvv);
													$("#'.CHtml::activeId($modelPersona, "telefono_1").'").click();
													if(data.tipo =="error"){
									                 	$("#informacion").html(data.mensaje);
									                 }
												}	
											}
						}'));
	?>
	}
	else{

	}
}

</script>




