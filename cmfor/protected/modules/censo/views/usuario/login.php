     <style>
	.ocultar {
		display: none;
	}
</style>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'registro-form1',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'action'=>array($this->action->id.'#contact-us')
)); 

?>

<style>
	div#img{background: url('images/registro.jpg');

width: 90%;
height: 90%;

margin-left: 2%;
background-size: cover;
position: absolute;
z-index: -1;
border-radius: 20px;



}

</style>
	<section id="contact-us">
				
		<div class="container">
			<div class="row">

				<div class="col-sm-12">
					<div class="title">

						<h3> Actualiza tus Datos </h3>
					</div>
				</div>
			</div>
		</div>

	<div style="border-style: solid; width: 95%;" align="center"  id="img"  >
	</div>
		<div class="contact">
			
			<div class="container">
				<div class="row">
					<div class="col-sm-5 col-sm-offset-4">
						<br><br><br><br><br><br><br><br><br>
						<h4 style="text-align: center; font-size:23px;"> Ingrese aquí su código del carnet de la patria y contraseña </strong></h4>
						<div class="input-group <?php if(isset($_POST['LoginForm'])) echo $model->hasErrors('username')?'has-error':'has-success'; ?>">
							<label class="sr-only">Usuario</label>
							<span class="input-group-addon" id="basic-addon1"><h6 class="glyphicon glyphicon-user"></h6></span>
							<?php echo $form->textField($model,"username",array('class'=>'form-control','placeholder'=>'Usuario (codigo del carnet de la patria)'));?>
						</div>
						<div class="form-group <?php if(isset($_POST['LoginForm'])) echo $model->hasErrors('username')?'has-error':'has-success'; ?>">
							<?php echo $form->error($model,'username',array('class'=>'help-block text-center')); ?>
						</div>

						<div style="height: 10px;"></div>

						<div class="input-group <?php if(isset($_POST['LoginForm'])) echo $model->hasErrors('password')?'has-error':'has-success'; ?>">
							<label class="sr-only">Contraseña</label>
							<span class="input-group-addon" id="basic-addon1"><h6 class="glyphicon glyphicon-lock"></h6></span>
							<?php echo $form->passwordField($model,"password",array('class'=>'form-control','placeholder'=>'Contraseña'));?>
						</div>
						<div class="form-group <?php if(isset($_POST['LoginForm'])) echo $model->hasErrors('password')?'has-error':'has-success'; ?>">
							<?php echo $form->error($model,'password',array('class'=>'help-block text-center')); ?>
						</div>

						<div style="height: 10px;"></div>

						<div class="text-center">
							<?php $this->widget('CCaptcha',array(
								'clickableImage'=>true,
								'showRefreshButton'=>false,

							)); ?>
						</div>

						<div class="input-group <?php if(isset($_POST['LoginForm'])) echo $Captcha->hasErrors('verifyCode')?'has-error':'has-success'; ?>">
							<span class="input-group-addon" id="basic-addon1"><h6 class="glyphicon glyphicon-barcode"></h6></span>
							<?php echo $form->textField($Captcha,'verifyCode',array('autocomplete'=>'off','class'=>'form-control','placeholder'=>'Introduzca el código captcha')); ?>
						</div>
						<div class="form-group <?php if(isset($_POST['LoginForm'])) echo $Captcha->hasErrors('verifyCode')?'has-error':'has-success'; ?>">
							<?php echo $form->error($Captcha,'verifyCode',array('class'=>'help-block text-center')); ?>
						</div>

						<div style="height: 10px;"></div>

						<div class="col-sm-offset-4">
							<?php echo CHtml::submitButton('Ingresar al Sistema',array('class'=>'btn btn-danger btn-lg')); ?>
						</div>
						<div style="text-align:center;margin-top:10px;">
							<a href="#" data-toggle="modal" data-target="#modal_recuperar"><button type="button" class="btn btn-link" style="font-size: 20px;">¿Olvidó su contraseña?</button></a>
						</div>

					</div>
						
						<br><br><br><br><br><br><br>
					</div>
				</div>
			</div>
		</div>
	</section>		

<?php $this->endWidget(); ?>



<!--MODAL DE REGISTRO-->

<div id="modal_registro" class="modal fade" role="dialog" data-backdrop="true" style="margin-top:60px;">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'registro-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
		'htmlOptions'=>array('class'=>'form-horizontal'),
		//'action'=>array($this->action->id.'#contact-us')
		)); 

	?>
	<div class="modal-dialog">

	<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title" style="text-align: center;"> Formulario de Registro </h3>

				
			</div>
			<div class="modal-body">

				<div class="row">
					<div class="col-lg-12 table-body">

					
							<?php
								//PARA AJUSTAR EL LABEL Y LOS DIV DEL FORMULARIO
								$LabelFormClass='col-lg-6 col-md-6 col-sm-6 col-lg-offset-0 col-md-offset-0 col-sm-offset-0 control-label';
								$DivFormClass='col-lg-5 col-md-5 col-sm-5 col-lg-offset-0 col-md-offset-0 col-sm-offset-0';
								$MarginDivForm = 'margin-top:-10px;';
							?>

							<h3 style="color:#6d6d6d;margin-left:10px;">Datos Personales</h3>
							<div style="border-bottom: 1px solid #FF625B"></div>
							<div style="height: 15px;"></div>
							
							<div class="form-group <?php if(isset($_POST['PerPersona'])) echo $modelPersona->hasErrors('codigo_carnetdlp')?'has-error':'has-success'; ?>" style="<?php echo $MarginDivForm; ?>">
								<label class="<?php echo $LabelFormClass; ?>" style="font-size: 14px;" ><b>Código Del Carnet De La Patria</b><span class="text-obligatorio"> *</span></label>
								<div class="<?php echo $DivFormClass; ?>" >
									<?php echo $form->textField($modelPersona,"codigo_carnetdlp",array('class'=>'form-control input-sm','onkeypress'=>'return soloNumeros(event)'));?>
									<?php echo $form->error($modelPersona,'codigo_carnetdlp',array('class'=>'help-block text-center')); ?>
								</div>
							</div>

							<div class="form-group <?php if(isset($_POST['PerPersona'])) echo $modelPersona->hasErrors('cedula')?'has-error':'has-success'; ?>" style="<?php echo $MarginDivForm; ?>">
								<label class="<?php echo $LabelFormClass; ?>" style="font-size: 15px;" >Cédula <span class="text-obligatorio"> *</span></label>
								<div class="<?php echo $DivFormClass; ?>" >
									<div class="input-group">
										<span class="input-group-btn">
											<?php echo $form->dropDownList($modelPersona, 'nacionalidad', array('V' => 'V', 'E' => 'E'),array('class'=>'form-control input-sm','style'=>'width:50px;')); ?>
										</span>
										<?php echo $form->textField($modelPersona,"cedula",array('class'=>'form-control input-sm','onkeypress'=>'return soloNumeros(event)'));?>
									</div>
									<?php echo $form->error($modelPersona,'cedula',array('class'=>'help-block text-center')); ?>
								</div>
							</div>
							
							<?php if($mensajeGeneral!=NULL): ?>
								<div class="form-group">
									<h4 class="" style="text-align: center; color:red;"><?php echo $mensajeGeneral; ?></h4>
								</div>
							<?php endif; ?>
							
							<div class="<?php echo ($modelPersona->flag!='43')?'ocultar ':'' ?>">
							
								<div class="form-group <?php if(isset($_POST['PerPersona'])) echo $modelPersona->hasErrors('fe_nac')?'has-error':'has-success'; ?>" style="<?php echo $MarginDivForm; ?>">
									<label class="<?php echo $LabelFormClass; ?>" style="font-size: 14px;" > Fecha de Nacimiento <span class="text-obligatorio"> *</span></label>
									<div class="<?php echo $DivFormClass; ?>" >
										<?php echo $form->textField($modelPersona,"fe_nac",array('class'=>'form-control input-sm date'));?>
										<?php echo $form->error($modelPersona,'fe_nac',array('class'=>'help-block text-center')); ?>
									</div>
								</div>


								<div class="form-group <?php if(isset($_POST['PerPersona'])) echo $modelPersona->hasErrors('nombre1')?'has-error':'has-success'; ?>" style="<?php echo $MarginDivForm; ?>">
									<label class="<?php echo $LabelFormClass; ?>" style="font-size: 15px;">Primer Nombre <span class="text-obligatorio"> *</span></label>
									<div class="<?php echo $DivFormClass; ?>" >
										<?php echo $form->textField($modelPersona,"nombre1",array('class'=>'form-control input-sm'));?>
										<?php echo $form->error($modelPersona,'nombre1',array('class'=>'help-block text-center')); ?>
									</div>
								</div>

								<div class="form-group <?php if(isset($_POST['PerPersona'])) echo $modelPersona->hasErrors('apellido1')?'has-error':'has-success'; ?>" style="padding-bottom:0px;<?php echo $MarginDivForm; ?>">
									<label class="<?php echo $LabelFormClass; ?>" style="font-size: 15px;">Primer Apellido <span class="text-obligatorio"> *</span></label>
									<div class="<?php echo $DivFormClass; ?>" >
										<?php echo $form->textField($modelPersona,"apellido1",array('class'=>'form-control input-sm'));?>
										<?php echo $form->error($modelPersona,'apellido1',array('class'=>'help-block text-center')); ?>
									</div>
								</div>

					
								<h3 style="color:#6d6d6d;margin-left:10px;"> Datos de Contacto </h3>
								<div style="border-bottom: 1px solid #FF625B"></div>
								<div style="height: 15px;"></div>

								<div class="form-group <?php if(isset($_POST['PerPersona'])) echo $modelPersona->hasErrors('e_mail')?'has-error':'has-success'; ?>">
									<label class="<?php echo $LabelFormClass; ?>" style="font-size: 15px;" > Correo Electrónico <span class="text-obligatorio"> *</span></label>
									<div class="<?php echo $DivFormClass; ?>" >
										<?php echo $form->textField($modelPersona,"e_mail",array('class'=>'form-control input-sm'));?>
										<?php echo $form->error($modelPersona,'e_mail',array('class'=>'help-block text-center')); ?>
									</div>
								</div>

								<div class="form-group <?php if(isset($_POST['PerPersona'])) echo $modelTelefono->hasErrors('nu_telefono')?'has-error':'has-success'; ?>" style="<?php echo $MarginDivForm; ?>">
									<label class="<?php echo $LabelFormClass; ?>" style="font-size: 15px;" > N° de Celular <span class="text-obligatorio"> *</span></label>
									<div class="<?php echo $DivFormClass; ?>" >
										<?php $this->widget('CMaskedTextField', array('model' => $modelTelefono,
																				  'attribute' => "nu_telefono",
																				  'mask' => '(9999)-999-99-99',
																				  'htmlOptions' => array('placeholder'=>'(9999)-999-99-99','class'=>'form-control input-sm')));?>
										<?php echo $form->error($modelTelefono,'nu_telefono',array('class'=>'help-block text-center')); ?>
									</div>
								</div>
								
								<?php echo $form->hiddenField($modelPersona,"flag");?>
							
							</div>
							
							<?php if($mostrarCatcha): ?>
							
								<div class="form-group" style="<?php echo $MarginDivForm; ?>">
									<label class="<?php echo $LabelFormClass; ?>" > </label>
									<div class="<?php echo $DivFormClass; ?>" >
										<div class="text-center">
											<?php $this->widget('CCaptcha',array(
												'clickableImage'=>true,
												'showRefreshButton'=>false,
											)); ?>
										</div>
									</div>
								</div>

								<div class="form-group <?php if(isset($_POST['PerPersona'])) echo $Captcha->hasErrors('verifyCode')?'has-error':'has-success'; ?>" style="<?php echo $MarginDivForm; ?>">
									<label class="<?php echo $LabelFormClass; ?>" style="font-size: 15px;" > Código Verificación <span class="text-obligatorio"> *</span></label>
									<div class="<?php echo $DivFormClass; ?>" >
										<?php echo $form->textField($Captcha,'verifyCode',array('id'=>'Captcha_verifyCode2','autocomplete'=>'off','class'=>'form-control','placeholder'=>'Introduzca el código de arriba')); ?>
										<?php echo $form->error($Captcha,'verifyCode',array('class'=>'help-block text-center')); ?>
									</div>
								</div>
							
							<?php else: ?>
							
								<?php echo $form->hiddenField($Captcha,"verifyCode",array('id'=>'Captcha_verifyCode2'));?>
								
							<?php endif; ?>


					</div>
				</div>
			</div>
			<div class="modal-footer">
			
				<div class="<?php echo ($modelPersona->flag=='43')?'ocultar ':'' ?>text-center">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<?php echo CHtml::submitButton('Siguiente',array('class'=>'btn btn-success')); ?>
				</div>
				<div class="<?php echo ($modelPersona->flag!='43')?'ocultar ':'' ?>">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<?php echo CHtml::submitButton('Registrar',array('class'=>'btn btn-danger')); ?>
				</div>
			</div>
		</div>

	</div>
	<?php $this->endWidget(); ?>
</div>

<!--FIN MODAL DE REGISTRO-->




<!--FIN MODAL DE RECUPERAR CONTRASEÑA-->

<div id="modal_recuperar" class="modal fade" role="dialog" data-backdrop="true" style="margin-top:60px;">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'recuperar-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
		'htmlOptions'=>array('class'=>'form-horizontal'),
		//'action'=>array($this->action->id.'#contact-us')
		)); 

	?>
	<div class="modal-dialog">

	<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title" style="text-align: center;"> Recuperar Contraseña </h3>

				
			</div>
			<div class="modal-body">

				<div class="row">
					<div class="col-lg-12 table-body">

					
							<?php
								//PARA AJUSTAR EL LABEL Y LOS DIV DEL FORMULARIO
								$LabelFormClass='col-lg-6 col-md-6 col-sm-6 col-lg-offset-0 col-md-offset-0 col-sm-offset-0 control-label';
								$DivFormClass='col-lg-5 col-md-5 col-sm-5 col-lg-offset-0 col-md-offset-0 col-sm-offset-0';
								$MarginDivForm = 'margin-top:-10px;';
							?>

							<h3 style="color:#6d6d6d;margin-left:10px;">Datos Personales</h3>
							<div style="border-bottom: 1px solid #FF625B"></div>
							<div style="height: 15px;"></div>
							


							<div class="form-group <?php if(isset($_POST['RecuperarForm'])) echo $modelRecuperar->hasErrors('cedula')?'has-error':'has-success'; ?>" style="<?php echo $MarginDivForm; ?>">
								<label class="<?php echo $LabelFormClass; ?>" style="font-size: 15px;" >Cédula <span class="text-obligatorio"> *</span></label>
								<div class="<?php echo $DivFormClass; ?>" >
									<div class="input-group">
										<span class="input-group-btn">
											<?php echo $form->dropDownList($modelRecuperar, 'nacionalidad', array('V' => 'V', 'E' => 'E'),array('class'=>'form-control input-sm','style'=>'width:50px;')); ?>
										</span>
										<?php echo $form->textField($modelRecuperar,"cedula",array('class'=>'form-control input-sm','onkeypress'=>'return soloNumeros(event)'));?>
									</div>
									<?php echo $form->error($modelRecuperar,'cedula',array('class'=>'help-block text-center')); ?>
								</div>
							</div>

							
							<div class="form-group" style="<?php echo $MarginDivForm; ?>">
								<label class="<?php echo $LabelFormClass; ?>" > </label>
								<div class="<?php echo $DivFormClass; ?>" >
									<div class="text-center">
										<?php $this->widget('CCaptcha',array(
											'clickableImage'=>true,
											'showRefreshButton'=>false,
										)); ?>
									</div>
								</div>
							</div>

							<div class="form-group <?php if(isset($_POST['RecuperarForm'])) echo $Captcha->hasErrors('verifyCode')?'has-error':'has-success'; ?>" style="<?php echo $MarginDivForm; ?>">
								<label class="<?php echo $LabelFormClass; ?>" style="font-size: 15px;" > Código Verificación <span class="text-obligatorio"> *</span></label>
								<div class="<?php echo $DivFormClass; ?>" >
									<?php echo $form->textField($Captcha,'verifyCode',array('id'=>'Captcha_verifyCode3','autocomplete'=>'off','class'=>'form-control','placeholder'=>'Introduzca el código de arriba')); ?>
									<?php echo $form->error($Captcha,'verifyCode',array('class'=>'help-block text-center')); ?>
								</div>
							</div>
							



					</div>
				</div>
			</div>
			<div class="modal-footer" style="margin-top: 100px;">
			
				<div class="text-center">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<?php echo CHtml::submitButton('Recuperar contraseña',array('class'=>'btn btn-success')); ?>
				</div>

			</div>
		</div>

	</div>
	<?php $this->endWidget(); ?>
</div>

<!--FIN MODAL DE RECUPERAR CONTRASEÑA-->


<?php
	$mensaje = '';
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
    	if($key=='success') {
    		$text='¡OPERACIÓN EXITOSA!';
    	} elseif($key=='error') {
			$text='¡OPERACIÓN NO PROCESADA!';
    	}
        $mensaje .= "swal('".$text."','".$message."','".$key."');";
    }
	
	Yii::app()->clientScript->registerScript('mensaje', $mensaje, CClientScript::POS_READY);
?>

<script>
 

    function soloNumeros(e){

	var key = window.Event ? e.which : e.keyCode

	return (key >= 48 && key <= 57)

}

</script>

