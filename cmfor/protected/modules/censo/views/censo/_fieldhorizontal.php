<div class="form-group <?php if(isset($_POST[get_class($model)])) echo $model->hasErrors($atributo)?'has-error':'has-success'; ?>">
	<?php echo $form->labelEx($model,$atributo,array('class'=>'col-md-offset-3 col-sm-2 control-label')); ?>
	<div class="col-sm-3">
		<?php echo $form->passwordField($model,$atributo,array('class'=>'form-control'));?>
		<?php echo $form->error($model,$atributo,array('class'=>'help-block text-center')); ?>
	</div>
</div>