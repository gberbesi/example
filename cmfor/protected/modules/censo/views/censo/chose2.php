<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/styles.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/terreno.css" />


<div class="panel panel-primary" style="border: none;">

	<div class="panel-heading"><p style="text-align: center;">Panel Elección del Censo a Actualizar</p></div>
	<div class="panel-body">
		
	<h3 style="text-align: center">Bienvenidos al Sistema de Actualización de Solicitudes de la Gran Misión Vivienda Venezuela.<br> A continuación, escoja la acción que desea realizar: </h3><br><br>
		
			<?php  echo CHtml::link('Actualizar sus datos',array('censo/index'),array('class'=>'btn btn-primary btn-lg','style'=>'margin-right:230px; margin-left:20%')); ?>
 
			<?php
			 echo CHtml::link('Consulta de estatus', '', array('id'=>'buscarComplementario','class'=>'btn btn-primary btn-lg')); ?>
			

	</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Resumen</h4>
      </div>
      <div class="modal-body" id="datos">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>


<?php 
Yii::app()->clientScript->registerScript('complemento','

	$( "#buscarComplementario" ).click(function(e) {
		e.preventDefault();
	  	buscarComplementario(e);
	});
    
	function buscarComplementario(e){

		
		cedula="17428863";
		if(cedula!=""){

				var c="'.CController::createUrl('/reportes/Cruce/CedulaAjax2').'";	
				
				$.ajax({
					url:c,
					cache: false,                
					type: "POST",
					data: ({cedula:cedula}),
					//beforeSend: cargando(id), //antes de envia
					success: function(data) 
					{	
						//$(".load_"+id).css("display","none");
						try
                           {
                           		$("#myModal").modal();
								$("#datos").html(data);
                                
					
                            } catch(err)  {
                                                       //alert("error");
                            } 

					},
			      error: function (xhr, ajaxOptions, thrownError) {
			      	//$(".load_"+id).css("display","none");
			        //alert(xhr.status);
			        //alert(thrownError);
			      }						
				});	
			}
			else{
				//alert("")
			}		
		}
		
	
	
	
		',CClientScript::POS_END);
?>
