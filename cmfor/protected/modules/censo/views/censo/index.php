<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
		echo "<div class='alert alert-".$key." text-center fade in' align='center'><a href='#' class='close' data-dismiss='alert' aria-label='close' style='background-color: white'>&times;</a><b>".$message."</b></div>";
    }
?>
			
		
			

<div class="panel panel-primary" style="border: none;">

	<div class="panel-heading">Panel Elección del Censo a Actualizar</div>
	<div class="panel-body">
		
<!----------------------------------------------------------------------------------- 0800 ---------------------------------------------------------------------->
		<?php if(count($censoAll)>0): ?>
		
			<?php foreach($censoAll as $censo): ?>
			
				<?php if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")): ?>
					<?php echo CHtml::link('Registrar familiar',array('psm/registro','cedulajf'=>$cedulajf,'familiaid'=>$censo->familia_id),array('class'=>'btn btn-primary btn-lg')); ?>
				<?php else: ?>
					<?php echo CHtml::link('Registrar familiar',array('psm/registro','familiaid'=>$censo->familia_id),array('class'=>'btn btn-primary btn-lg')); ?>
				<?php endif; ?>
				<table class="table table-bordered table-hover">
					<tr style="background: #EEE; font-size: 20PX;">
						<td colspan="7"  style="color: black; font-weight: bold;"> <center>CENSO 0800 MI HOGAR</center></td>
					</tr>
					<tr style="background: #EEE; ">
						<td style="color: black; font-weight: bold;">Cédula</td>
						<td style="color: black; font-weight: bold;">Nombres</td>
						<td style="color: black; font-weight: bold;">Apellidos</td>
						<td style="color: black; font-weight: bold;">Rol de Parentesco</td>
						<td style="color: black; font-weight: bold; width: 10%;">Imprimir Ficha</td>
						<td style="color: black; font-weight: bold; width: 10%;">Actualizar</td>
						<td style="color: black; font-weight: bold; width: 10%;">Borrar Familiar</td>
						
					</tr>
					
					<?php 
					$censoFamilia = Censado::model()->findAll(array('condition'=>'familia_id=:familia_id','params'=>array(':familia_id'=>$censo->familia_id),'order'=>'parentesco_id=12 DESC'));
					?>
					<?php foreach($censoFamilia as $key=>$censo): ?>
						<tr style="color:black">
							<td style="color:black"><?php echo $censo->cedula; ?></td>
							<td style="color:black"><?php echo $censo->nombre1.' '.$censo->nombre2; ?></td>
							<td style="color:black"><?php echo $censo->apellido1.' '.$censo->apellido2; ?></td>
							<td style="color:black"><?php echo $censo->parentesco->nombre; ?></td>

								<td align="center">
								<?php if($censo->parentesco_id=='12'): ?>
									<a href="<?php echo $this->createUrl('psm/certificado', array('familiaId'=>$censo->familia_id)); ?>" class="btn btn-primary btn-lg" data-toggle="tooltip" title="Haga clic para Imprimir Ficha" data-placement="left"><span class="glyphicon glyphicon-download-alt"></span></span></a>
								<?php endif; ?>
							</td>
							<td align="center" >
								<?php if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")): ?>
									<a href="<?php echo $this->createUrl('psm/registro', array('cedulajf'=>$cedulajf,'persona'=>$censo->censado_id,'familiaid'=>$censo->familia_id))?>" class="btn btn-success btn-lg" data-toggle="tooltip" title="Haga clic para actualizar sus datos" data-placement="left"><span class="glyphicon glyphicon-retweet" ></span></span></a>
								<?php else: ?>
									<a href="<?php echo $this->createUrl('psm/registro', array('persona'=>$censo->censado_id,'familiaid'=>$censo->familia_id))?>" class="btn btn-success btn-lg" data-toggle="tooltip" title="Haga clic para actualizar sus datos" data-placement="left"><span class="glyphicon glyphicon-retweet" ></span></span></a>
								<?php endif; ?>
							</td>

						
							<td align="center">
								<?php if($censo->parentesco_id!='12'): ?>
									<?php if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")): ?>
										<a href="<?php echo $this->createUrl('psm/delete', array('persona'=>$censo->censado_id,'cedulajf'=>$cedulajf)); ?>" class="btn btn-danger btn-lg" data-toggle="tooltip" title="Haga clic para borrar" data-placement="left"><span class="glyphicon glyphicon-remove"></span></span></a>
									<?php else: ?>
										<a href="<?php echo $this->createUrl('psm/delete', array('persona'=>$censo->censado_id)); ?>" class="btn btn-danger btn-lg" data-toggle="tooltip" title="Haga clic para borrar" data-placement="left"><span class="glyphicon glyphicon-remove"></span></span></a>
									<?php endif; ?>
								<?php endif; ?>
							</td>

						</tr>
					<?php endforeach; ?>
				</table>
			<?php endforeach; ?>



			
		
		
			
			
		<?php endif; ?>
		
<!----------------------------------------------------------------------------------- GMVV ---------------------------------------------------------------------->
		
		<?php if(count($gmvvAll)>0): ?><br>
		
			<?php foreach($gmvvAll as $gmvv): ?>
			
				<?php if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")): ?>
					<?php echo CHtml::link('Registrar familiar',array('gmvv/registro','cedulajf'=>$cedulajf,'familiaid'=>$gmvv->familia_id),array('class'=>'btn btn-primary btn-lg')); ?>
				<?php else: ?>
					<?php echo CHtml::link('Registrar familiar',array('gmvv/registro','familiaid'=>$gmvv->familia_id),array('class'=>'btn btn-primary btn-lg')); ?>
				<?php endif; ?>
				<table class="table table-bordered table-hover" >
					<tr style="background: #EEE; font-size: 20PX;" >
						<td colspan="7" style="color: black; font-weight: bold;"> <center>CENSO GRAN MISIÓN VIVIENDA VENEZUELA</center></td>
					</tr>

					<tr style="background: #EEE; ">
						<td style="color: black; font-weight: bold;">Cédula</td>
						<td style="color: black; font-weight: bold;">Nombres</td>
						<td style="color: black; font-weight: bold;">Apellidos</td>
						<td style="color: black; font-weight: bold;">Rol de Parentesco</td>

						<td style="color: black; font-weight: bold; width: 10%;">Imprimir Ficha</td>
						<td style="color: black; font-weight: bold; width: 10%;">Actualizar</td>
						
						<td style="color: black; font-weight: bold; width: 10%;">Borrar Familiar</td>

					</tr>
					
					<?php
						$gmvvFamilia = GrGmvvT::model()->findAll(array('condition'=>'familia_id=:familia_id','params'=>array(':familia_id'=>$gmvv->familia_id),'order'=>'parentesco_id=12 DESC'));
					?>
					<?php foreach($gmvvFamilia as $key=>$gmvv): ?>
						<tr style="color:black">
							<td style="color:black"><?php echo $gmvv->ci; ?></td>
							<td style="color:black"><?php echo $gmvv->primer_nombre.' '.$gmvv->segundo_nombre; ?></td>
							<td style="color:black"><?php echo $gmvv->primer_apellido.' '.$gmvv->segundo_apellido; ?></td>
							<td style="color:black"><?php echo $gmvv->parentesco->nombre; ?></td>
							<td align="center">
								<?php if($gmvv->parentesco_id=='12'): ?>
									<a href="<?php echo $this->createUrl('gmvv/certificado', array('familiaId'=>$gmvv->familia_id)); ?>" class="btn btn-primary btn-lg" data-toggle="tooltip" title="Haga clic para Imprimir Ficha" data-placement="left"><span class="glyphicon glyphicon-download-alt"></span></span></a>
								<?php endif; ?>
							</td>

							
							<td align="center">
								<?php if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")): ?>
									<a href="<?php echo $this->createUrl('gmvv/registro', array('cedulajf'=>$cedulajf,'persona'=>$gmvv->id_gr_gmvv_t,'familiaid'=>$gmvv->familia_id))?>" class="btn btn-success btn-lg" data-toggle="tooltip" title="Haga clic para actualizar sus datos" data-placement="left"><span class="glyphicon glyphicon-retweet" ></span></span></a>
								<?php else: ?>
									<a href="<?php echo $this->createUrl('gmvv/registro', array('persona'=>$gmvv->id_gr_gmvv_t,'familiaid'=>$gmvv->familia_id))?>" class="btn btn-success btn-lg" data-toggle="tooltip" title="Haga clic para actualizar sus datos" data-placement="left"><span class="glyphicon glyphicon-retweet" ></span></span></a>
								<?php endif; ?>
							</td>

						

							<td align="center">
							<?php if($gmvv->parentesco_id!='12'): ?>
								<?php if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")): ?>
									<a href="<?php echo $this->createUrl('gmvv/delete', array('persona'=>$gmvv->id_gr_gmvv_t,'cedulajf'=>$cedulajf)); ?>" class="btn btn-danger btn-lg" data-toggle="tooltip" title="Haga clic para borrar" data-placement="left"><span class="glyphicon glyphicon-remove"></span></span></a>
								<?php else: ?>
									<a href="<?php echo $this->createUrl('gmvv/delete', array('persona'=>$gmvv->id_gr_gmvv_t)); ?>" class="btn btn-danger btn-lg" data-toggle="tooltip" title="Haga clic para borrar" data-placement="left"><span class="glyphicon glyphicon-remove"></span></span></a>
								<?php endif; ?>
							<?php endif; ?>
							</td>
							

						</tr>
					<?php endforeach; ?>

				</table>
			
			<?php endforeach; ?>
			
		<?php endif; ?>


		<script type="text/javascript">
			
			function redireccionar() {
				// body...
				 location.href='?vista=ver_censo';
			}
		</script>


	</div>
</div>