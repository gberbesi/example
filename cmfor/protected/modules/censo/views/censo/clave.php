
<h1 align="center">Cambiar Contraseña del Usuario <?php echo $model->nb_usuario; ?></h1>

<div align="center"  class="form">

<?php

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-usuarios-form',
	//'enableAjaxValidation'=>true,
	'clientOptions'=>array(
		//'validateOnSubmit'=>true,
	),
	'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message) {
        echo '<div class="alert alert-' . $key . '"><b>' . $message . "</b></div>\n";
    }
	
	
	?>

	<?php echo $this->renderPartial('_fieldhorizontal',array(
			'model'=>$model,
			'form'=>$form,
			'atributo'=>'contrasena',
			),true); 
	?>
	
	<?php echo $this->renderPartial('_fieldhorizontal',array(
			'model'=>$model,
			'form'=>$form,
			'atributo'=>'nueva',
			),true); 
	?>
	
	<?php echo $this->renderPartial('_fieldhorizontal',array(
			'model'=>$model,
			'form'=>$form,
			'atributo'=>'repetirnueva',
			),true); 
	?>
    
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Cambiar' : 'Modificar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->