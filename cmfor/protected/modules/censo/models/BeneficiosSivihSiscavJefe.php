<?php

/**
 * This is the model class for table "beneficios_sivih_siscav_jefe".
 *
 * The followings are the available columns in table 'beneficios_sivih_siscav_jefe':
 * @property string $familia_id
 * @property string $beneficio_id
 * @property integer $persona_id
 * @property string $nacionalidad
 * @property integer $cedula
 * @property string $nombre1
 * @property string $nombre2
 * @property string $apellido1
 * @property string $apellido2
 * @property string $fecha_nac
 * @property string $sexo
 * @property boolean $jefe_familia
 * @property integer $geo_estado_destino_id
 * @property string $estado
 * @property integer $geo_municipio_destino_id
 * @property string $municipio
 * @property integer $geo_parroquia_destino_id
 * @property string $parroquia
 * @property string $geo_ciudad_destino
 * @property string $geo_sector_destino
 * @property string $geo_calle_destino
 * @property string $desarrollo
 * @property string $geo_nombre_edificacion_destino
 * @property string $geo_etapa_destino
 * @property string $tipo_vivienda
 * @property string $geo_piso_destino
 * @property string $geo_num_casa_apto_destino
 * @property string $monto_beneficio
 * @property string $fecha_adjudicacion
 * @property string $organizacion
 * @property string $estatus_desactivacion
 * @property string $status_beneficio
 * @property string $fecha
 * @property string $observaciones
 * @property string $org_beneficio
 * @property boolean $psm
 * @property string $codigo_beneficio
 * @property integer $parentesco_id
 * @property boolean $st_adjudicacion
 * @property boolean $preasignacion
 */
class BeneficiosSivihSiscavJefe extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BeneficiosSivihSiscavJefe the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'beneficios_sivih_siscav_jefe';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('persona_id, cedula, geo_estado_destino_id, geo_municipio_destino_id, geo_parroquia_destino_id, parentesco_id', 'numerical', 'integerOnly'=>true),
			array('nacionalidad, sexo', 'length', 'max'=>1),
			array('familia_id, beneficio_id, nombre1, nombre2, apellido1, apellido2, fecha_nac, jefe_familia, estado, municipio, parroquia, geo_ciudad_destino, geo_sector_destino, geo_calle_destino, desarrollo, geo_nombre_edificacion_destino, geo_etapa_destino, tipo_vivienda, geo_piso_destino, geo_num_casa_apto_destino, monto_beneficio, fecha_adjudicacion, organizacion, estatus_desactivacion, status_beneficio, fecha, observaciones, org_beneficio, psm, codigo_beneficio, st_adjudicacion, preasignacion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('familia_id, beneficio_id, persona_id, nacionalidad, cedula, nombre1, nombre2, apellido1, apellido2, fecha_nac, sexo, jefe_familia, geo_estado_destino_id, estado, geo_municipio_destino_id, municipio, geo_parroquia_destino_id, parroquia, geo_ciudad_destino, geo_sector_destino, geo_calle_destino, desarrollo, geo_nombre_edificacion_destino, geo_etapa_destino, tipo_vivienda, geo_piso_destino, geo_num_casa_apto_destino, monto_beneficio, fecha_adjudicacion, organizacion, estatus_desactivacion, status_beneficio, fecha, observaciones, org_beneficio, psm, codigo_beneficio, parentesco_id, st_adjudicacion, preasignacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'familia_id' => 'Familia',
			'beneficio_id' => 'Beneficio',
			'persona_id' => 'Persona',
			'nacionalidad' => 'Nacionalidad',
			'cedula' => 'Cedula',
			'nombre1' => 'Nombre1',
			'nombre2' => 'Nombre2',
			'apellido1' => 'Apellido1',
			'apellido2' => 'Apellido2',
			'fecha_nac' => 'Fecha Nac',
			'sexo' => 'Sexo',
			'jefe_familia' => 'Jefe Familia',
			'geo_estado_destino_id' => 'Geo Estado Destino',
			'estado' => 'Estado',
			'geo_municipio_destino_id' => 'Geo Municipio Destino',
			'municipio' => 'Municipio',
			'geo_parroquia_destino_id' => 'Geo Parroquia Destino',
			'parroquia' => 'Parroquia',
			'geo_ciudad_destino' => 'Geo Ciudad Destino',
			'geo_sector_destino' => 'Geo Sector Destino',
			'geo_calle_destino' => 'Geo Calle Destino',
			'desarrollo' => 'Desarrollo',
			'geo_nombre_edificacion_destino' => 'Geo Nombre Edificacion Destino',
			'geo_etapa_destino' => 'Geo Etapa Destino',
			'tipo_vivienda' => 'Tipo Vivienda',
			'geo_piso_destino' => 'Geo Piso Destino',
			'geo_num_casa_apto_destino' => 'Geo Num Casa Apto Destino',
			'monto_beneficio' => 'Monto Beneficio',
			'fecha_adjudicacion' => 'Fecha Adjudicacion',
			'organizacion' => 'Organizacion',
			'estatus_desactivacion' => 'Estatus Desactivacion',
			'status_beneficio' => 'Status Beneficio',
			'fecha' => 'Fecha',
			'observaciones' => 'Observaciones',
			'org_beneficio' => 'Org Beneficio',
			'psm' => 'Psm',
			'codigo_beneficio' => 'Codigo Beneficio',
			'parentesco_id' => 'Parentesco',
			'st_adjudicacion' => 'St Adjudicacion',
			'preasignacion' => 'Preasignacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('familia_id',$this->familia_id,true);
		$criteria->compare('beneficio_id',$this->beneficio_id,true);
		$criteria->compare('persona_id',$this->persona_id);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('cedula',$this->cedula);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('fecha_nac',$this->fecha_nac,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('jefe_familia',$this->jefe_familia);
		$criteria->compare('geo_estado_destino_id',$this->geo_estado_destino_id);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('geo_municipio_destino_id',$this->geo_municipio_destino_id);
		$criteria->compare('municipio',$this->municipio,true);
		$criteria->compare('geo_parroquia_destino_id',$this->geo_parroquia_destino_id);
		$criteria->compare('parroquia',$this->parroquia,true);
		$criteria->compare('geo_ciudad_destino',$this->geo_ciudad_destino,true);
		$criteria->compare('geo_sector_destino',$this->geo_sector_destino,true);
		$criteria->compare('geo_calle_destino',$this->geo_calle_destino,true);
		$criteria->compare('desarrollo',$this->desarrollo,true);
		$criteria->compare('geo_nombre_edificacion_destino',$this->geo_nombre_edificacion_destino,true);
		$criteria->compare('geo_etapa_destino',$this->geo_etapa_destino,true);
		$criteria->compare('tipo_vivienda',$this->tipo_vivienda,true);
		$criteria->compare('geo_piso_destino',$this->geo_piso_destino,true);
		$criteria->compare('geo_num_casa_apto_destino',$this->geo_num_casa_apto_destino,true);
		$criteria->compare('monto_beneficio',$this->monto_beneficio,true);
		$criteria->compare('fecha_adjudicacion',$this->fecha_adjudicacion,true);
		$criteria->compare('organizacion',$this->organizacion,true);
		$criteria->compare('estatus_desactivacion',$this->estatus_desactivacion,true);
		$criteria->compare('status_beneficio',$this->status_beneficio,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('observaciones',$this->observaciones,true);
		$criteria->compare('org_beneficio',$this->org_beneficio,true);
		$criteria->compare('psm',$this->psm);
		$criteria->compare('codigo_beneficio',$this->codigo_beneficio,true);
		$criteria->compare('parentesco_id',$this->parentesco_id);
		$criteria->compare('st_adjudicacion',$this->st_adjudicacion);
		$criteria->compare('preasignacion',$this->preasignacion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}