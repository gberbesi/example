<?php

/**
 * This is the model class for table "per_telefono".
 *
 * The followings are the available columns in table 'per_telefono':
 * @property integer $telefono_id
 * @property string $persona_id
 * @property string $nu_telefono
 * @property string $usu_aud_id
 *
 * The followings are the available model relations:
 * @property PerPersona $persona
 */
class PerTelefono extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PerTelefono the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'per_telefono';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nu_telefono', 'required'),
			array('nu_telefono', 'filter', 'filter'=>'trim'),
			array('nu_telefono', 'validarTelefonoMovil'),
			array('nu_telefono, usu_aud_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('telefono_id, persona_id, nu_telefono, usu_aud_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'persona' => array(self::BELONGS_TO, 'PerPersona', 'persona_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'telefono_id' => 'Telefono',
			'persona_id' => 'Persona',
			'nu_telefono' => 'Nu Telefono',
			'usu_aud_id' => 'Usu Aud',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('telefono_id',$this->telefono_id);
		$criteria->compare('persona_id',$this->persona_id,true);
		$criteria->compare('nu_telefono',$this->nu_telefono,true);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function validarTelefonoMovil(){
		if(!$this->hasErrors('nu_telefono')) {
			$buscar   = array("(", ")", "-");
			$telf_movil  =  substr(str_replace($buscar, '', $this->nu_telefono), 0, 4);
			
			if($telf_movil != '0424' && $telf_movil != '0414' && $telf_movil != '0426' && $telf_movil != '0416' && $telf_movil != '0412'){
				$this->addError('nu_telefono', 'código invalido para teléfono movil');
			}
		}
	}
	protected function beforeSave() {  //PARA CAMBIAR AL FORMATO QUE ACEPTA POSTGRES ANTES DE GUARDAR

		$buscar   = array("(", ")", "-");
		$this->nu_telefono = str_replace($buscar, '', $this->nu_telefono);
		return parent::beforeSave();
	}
	
}