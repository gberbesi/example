<?php

/**
 * This is the model class for table "fam_familia_beneficiado".
 *
 * The followings are the available columns in table 'fam_familia_beneficiado':
 * @property string $familia_id
 * @property integer $beneficiado_id
 * @property integer $parentesco_id
 * @property boolean $jefe_familia
 * @property boolean $st_familia_beneficiado
 * @property boolean $reg_gmvv
 * @property string $usu_aud_id
 * @property boolean $reg_plan
 * @property boolean $reg_gmvo
 * @property string $usu_cambio_jefe_id
 */
class FamFamiliaBeneficiado extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FamFamiliaBeneficiado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fam_familia_beneficiado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('familia_id, beneficiado_id, parentesco_id', 'required'),
			array('beneficiado_id, parentesco_id', 'numerical', 'integerOnly'=>true),
			array('jefe_familia, st_familia_beneficiado, reg_gmvv, usu_aud_id, reg_plan, reg_gmvo, usu_cambio_jefe_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('familia_id, beneficiado_id, parentesco_id, jefe_familia, st_familia_beneficiado, reg_gmvv, usu_aud_id, reg_plan, reg_gmvo, usu_cambio_jefe_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'familias'=>array(self::BELONGS_TO,'FamFamilia','familia_id'),
			'famBeneficiado'=>array(self::BELONGS_TO,'FamBeneficiado','beneficiado_id'),
			'famParentesco'=>array(self::BELONGS_TO,'FamParentesco','parentesco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'familia_id' => 'Familia',
			'beneficiado_id' => 'Beneficiado',
			'parentesco_id' => 'Parentesco',
			'jefe_familia' => 'Jefe Familia',
			'st_familia_beneficiado' => 'St Familia Beneficiado',
			'reg_gmvv' => 'Reg Gmvv',
			'usu_aud_id' => 'Usu Aud',
			'reg_plan' => 'Reg Plan',
			'reg_gmvo' => 'Reg Gmvo',
			'usu_cambio_jefe_id' => 'Usu Cambio Jefe',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('familia_id',$this->familia_id,true);
		$criteria->compare('beneficiado_id',$this->beneficiado_id);
		$criteria->compare('parentesco_id',$this->parentesco_id);
		$criteria->compare('jefe_familia',$this->jefe_familia);
		$criteria->compare('st_familia_beneficiado',$this->st_familia_beneficiado);
		$criteria->compare('reg_gmvv',$this->reg_gmvv);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('reg_plan',$this->reg_plan);
		$criteria->compare('reg_gmvo',$this->reg_gmvo);
		$criteria->compare('usu_cambio_jefe_id',$this->usu_cambio_jefe_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}