<?php

/**
 * This is the model class for table "auditoria.gmvv_gr_gmvv_t_audit".
 *
 * The followings are the available columns in table 'auditoria.gmvv_gr_gmvv_t_audit':
 * @property string $audit_timestamp
 * @property string $audit_dbuser
 * @property string $audit_action
 * @property string $audit_old
 * @property string $audit_new
 * @property string $id_gr_gmvv_t
 * @property string $id_gr_gmvv_t_audit
 * @property string $ci
 * @property string $familia_id
 * @property string $usu_aud_id
 * @property string $transaction_id
 */
class GmvvGrGmvvTAudit extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GmvvGrGmvvTAudit the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'auditoria.gmvv_gr_gmvv_t_audit';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('audit_timestamp, audit_dbuser, audit_action', 'required'),
			array('audit_action', 'length', 'max'=>1),
			array('audit_old, audit_new, id_gr_gmvv_t, ci, familia_id, usu_aud_id, transaction_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('audit_timestamp, audit_dbuser, audit_action, audit_old, audit_new, id_gr_gmvv_t, id_gr_gmvv_t_audit, ci, familia_id, usu_aud_id, transaction_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'audit_timestamp' => 'Audit Timestamp',
			'audit_dbuser' => 'Audit Dbuser',
			'audit_action' => 'Audit Action',
			'audit_old' => 'Audit Old',
			'audit_new' => 'Audit New',
			'id_gr_gmvv_t' => 'Id Gr Gmvv T',
			'id_gr_gmvv_t_audit' => 'Id Gr Gmvv T Audit',
			'ci' => 'Ci',
			'familia_id' => 'Familia',
			'usu_aud_id' => 'Usu Aud',
			'transaction_id' => 'Transaction',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('audit_timestamp',$this->audit_timestamp,true);
		$criteria->compare('audit_dbuser',$this->audit_dbuser,true);
		$criteria->compare('audit_action',$this->audit_action,true);
		$criteria->compare('audit_old',$this->audit_old,true);
		$criteria->compare('audit_new',$this->audit_new,true);
		$criteria->compare('id_gr_gmvv_t',$this->id_gr_gmvv_t,true);
		$criteria->compare('id_gr_gmvv_t_audit',$this->id_gr_gmvv_t_audit,true);
		$criteria->compare('ci',$this->ci,true);
		$criteria->compare('familia_id',$this->familia_id,true);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('transaction_id',$this->transaction_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}