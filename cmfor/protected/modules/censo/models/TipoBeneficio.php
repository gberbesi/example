<?php

/**
 * This is the model class for table "beneficios.tipo_beneficio".
 *
 * The followings are the available columns in table 'beneficios.tipo_beneficio':
 * @property integer $tipo_beneficio_id
 * @property string $nombre
 */
class TipoBeneficio extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TipoBeneficio the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'beneficios.tipo_beneficio';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('tipo_beneficio_id, nombre', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'tipo_beneficio_id' => 'Tipo Beneficio',
            'nombre' => 'Nombre',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('tipo_beneficio_id',$this->tipo_beneficio_id);
        $criteria->compare('nombre',$this->nombre,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}