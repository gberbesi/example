<?php

/**
 * This is the model class for table "fam_calidad_vivienda_origen".
 *
 * The followings are the available columns in table 'fam_calidad_vivienda_origen':
 * @property integer $calidad_vivienda_id
 * @property string $nombre
 * @property boolean $estatus
 *
 * The followings are the available model relations:
 * @property FamFamiliaDireccionOrigen[] $famFamiliaDireccionOrigens
 */
class FamCalidadViviendaOrigen extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FamCalidadViviendaOrigen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fam_calidad_vivienda_origen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, estatus', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('calidad_vivienda_id, nombre, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'famFamiliaDireccionOrigens' => array(self::HAS_MANY, 'FamFamiliaDireccionOrigen', 'calidad_vivienda_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'calidad_vivienda_id' => 'Calidad Vivienda',
			'nombre' => 'Nombre',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('calidad_vivienda_id',$this->calidad_vivienda_id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('estatus',$this->estatus);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}