<?php

/**
 * This is the model class for table "censo.direccion".
 *
 * The followings are the available columns in table 'censo.direccion':
 * @property integer $direccion_id
 * @property string $direccion
 * @property integer $estado_id
 * @property integer $municipio_id
 * @property integer $parroquia_id
 * @property string $ciudad_destino_1
 * @property string $ciudad_destino_2
 * @property string $usu_aud_id
 * @property string $condicion_vivienda_id
 * @property string $calidad_vivienda_id
 * @property string $tipo_solicitud_id
 * @property string $fecha_migracion
 * @property string $tipo_programa
 * @property string $planteamiento
 * @property string $nro_habitaciones_de_la_residencia
 */
class Direccion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Direccion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'censo.direccion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('estado_id, municipio_id, parroquia_id', 'required'),
			array('estado_dos_id, municipio_dos_id, parroquia_dos_id, estado_id, municipio_id, parroquia_id', 'numerical', 'integerOnly'=>true),
			array('direccion, ciudad_destino_1, ciudad_destino_2, usu_aud_id, condicion_vivienda_id, calidad_vivienda_id, tipo_solicitud_id, fecha_migracion, tipo_programa, planteamiento, nro_habitaciones_de_la_residencia,tipo_solicitud_id2', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('direccion_id, direccion, estado_id, municipio_id, parroquia_id, ciudad_destino_1, ciudad_destino_2, usu_aud_id, condicion_vivienda_id, calidad_vivienda_id, tipo_solicitud_id, fecha_migracion, tipo_programa, planteamiento, nro_habitaciones_de_la_residencia,tipo_solicitud_id2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'familias' => array(self::HAS_MANY, 'Familia', 'direccion_destino'),
			'familias1' => array(self::HAS_MANY, 'Familia', 'direccion_id'),
			'tipoSolicitud' => array(self::BELONGS_TO, 'FamTipoSolicitud', 'tipo_solicitud_id'),
			'calidadVivienda' => array(self::BELONGS_TO, 'FamCalidadViviendaOrigen', 'calidad_vivienda_id'),
			'ciudadDestino1' => array(self::BELONGS_TO, 'GeoCiudad', 'ciudad_destino_1'),
			'ciudadDestino2' => array(self::BELONGS_TO, 'GeoCiudad', 'ciudad_destino_2'),
			'condicionVivienda' => array(self::BELONGS_TO, 'FamCondicionViviendaOrigen', 'condicion_vivienda_id'),
			'estado' => array(self::BELONGS_TO, 'GeoEstado', 'estado_id'),
			'municipio' => array(self::BELONGS_TO, 'GeoMunicipio', 'municipio_id'),
			'parroquia' => array(self::BELONGS_TO, 'GeoParroquia', 'parroquia_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'direccion_id' => 'Dirección',
			'direccion' => 'Dirección',
			'estado_id' => 'Estado',
			'municipio_id' => 'Municipio',
			'parroquia_id' => 'Parroquia',
			'ciudad_destino_1' => 'Ciudad', 
			'ciudad_destino_2' => 'Ciudad',
			'usu_aud_id' => 'Usu Aud',
			'condicion_vivienda_id' => 'Condición Vivienda',
			'calidad_vivienda_id' => 'Calidad Vivienda',
			'tipo_solicitud_id' => 'Tipo Solicitud (Anterior)',
			'fecha_migracion' => 'Fecha Migración',
			'tipo_programa' => 'Tipo Programa',
			'planteamiento' => 'Planteamiento',
			'nro_habitaciones_de_la_residencia' => 'Nro Habitaciones De La Residencia',
			
			'estado_dos_id' => 'Estado',
			'municipio_dos_id' => 'Municipio',
			'parroquia_dos_id' => 'Parroquia',
			'tipo_solicitud_id2' => 'Tipo de Solicitud (Actual)',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('direccion_id',$this->direccion_id);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('estado_id',$this->estado_id);
		$criteria->compare('municipio_id',$this->municipio_id);
		$criteria->compare('parroquia_id',$this->parroquia_id);
		$criteria->compare('ciudad_destino_1',$this->ciudad_destino_1,true);
		$criteria->compare('ciudad_destino_2',$this->ciudad_destino_2,true);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('condicion_vivienda_id',$this->condicion_vivienda_id,true);
		$criteria->compare('calidad_vivienda_id',$this->calidad_vivienda_id,true);
		$criteria->compare('tipo_solicitud_id',$this->tipo_solicitud_id,true);
		$criteria->compare('fecha_migracion',$this->fecha_migracion,true);
		$criteria->compare('tipo_programa',$this->tipo_programa,true);
		$criteria->compare('planteamiento',$this->planteamiento,true);
		$criteria->compare('nro_habitaciones_de_la_residencia',$this->nro_habitaciones_de_la_residencia,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function beforeSave() {
		foreach ($this->attributes as $key => $value)
			if ($value=='')
				$this->$key = NULL;
			
		return parent::beforeSave();
	}
}