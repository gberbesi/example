<?php

/**
 * This is the model class for table "comunidad_avv".
 *
 * The followings are the available columns in table 'comunidad_avv':
 * @property integer $idcomunidad_avv
 * @property string $nombre_avv
 * @property integer $direccion_id
 * @property integer $urbanismo_id
 * @property boolean $st_comunidad_avv
 * @property string $usu_aud_id
 * @property integer $ente_id
 * @property string $url_imagen
 * @property string $terreno_id
 *
 * The followings are the available model relations:
 * @property UrbUrbanismo $urbanismo
 * @property Ente $ente
 * @property GeoDireccion $direccion
 * @property GeoTerreno $terreno
 * @property RepresentanteAvv[] $representanteAvvs
 * @property FamFamiliaAvv[] $famFamiliaAvvs
 */
class ComunidadAvv extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ComunidadAvv the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comunidad_avv';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('direccion_id, urbanismo_id, ente_id', 'numerical', 'integerOnly'=>true),
			array('nombre_avv', 'length', 'max'=>250),
			array('st_comunidad_avv, usu_aud_id, url_imagen, terreno_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idcomunidad_avv, nombre_avv, direccion_id, urbanismo_id, st_comunidad_avv, usu_aud_id, ente_id, url_imagen, terreno_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'urbanismo' => array(self::BELONGS_TO, 'UrbUrbanismo', 'urbanismo_id'),
			'ente' => array(self::BELONGS_TO, 'Ente', 'ente_id'),
			'direccion' => array(self::BELONGS_TO, 'GeoDireccion', 'direccion_id'),
			'terreno' => array(self::BELONGS_TO, 'GeoTerreno', 'terreno_id'),
			'representanteAvvs' => array(self::HAS_MANY, 'RepresentanteAvv', 'idcomunidad_avv'),
			'famFamiliaAvvs' => array(self::HAS_MANY, 'FamFamiliaAvv', 'idcomunidad_avv'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idcomunidad_avv' => 'Idcomunidad Avv',
			'nombre_avv' => 'Nombre Avv',
			'direccion_id' => 'Direccion',
			'urbanismo_id' => 'Urbanismo',
			'st_comunidad_avv' => 'St Comunidad Avv',
			'usu_aud_id' => 'Usu Aud',
			'ente_id' => 'Ente',
			'url_imagen' => 'Url Imagen',
			'terreno_id' => 'Terreno',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idcomunidad_avv',$this->idcomunidad_avv);
		$criteria->compare('nombre_avv',$this->nombre_avv,true);
		$criteria->compare('direccion_id',$this->direccion_id);
		$criteria->compare('urbanismo_id',$this->urbanismo_id);
		$criteria->compare('st_comunidad_avv',$this->st_comunidad_avv);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('ente_id',$this->ente_id);
		$criteria->compare('url_imagen',$this->url_imagen,true);
		$criteria->compare('terreno_id',$this->terreno_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}