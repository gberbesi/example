<?php

/**
 * This is the model class for table "censo.busqueda_persona_censos".
 *
 * The followings are the available columns in table 'censo.busqueda_persona_censos':
 * @property string $serial
 * @property string $familia_id
 * @property string $censado_id
 * @property string $beneficiado_sigevih_id
 * @property string $nacionalidad
 * @property string $cedula
 * @property string $nombre1
 * @property string $nombre2
 * @property string $apellido1
 * @property string $apellido2
 * @property string $fe_nac
 * @property string $sexo
 * @property string $e_mail
 * @property integer $id_pais
 * @property string $pais
 * @property integer $tiempo_pais
 * @property integer $parentesco_id
 * @property string $rol
 * @property integer $grado_instruccion_id
 * @property string $grado_instruccion
 * @property integer $enfermedad_id
 * @property string $enfermedad
 * @property integer $ocupacion_id
 * @property string $ocupacion
 * @property string $trabajo_id
 * @property string $institucion
 * @property string $ocupacion_otro
 * @property boolean $estado_embarazo
 * @property boolean $estudia
 * @property string $ingreso_mensual
 * @property integer $edo_civil_id
 * @property string $ed_civil
 * @property integer $id_clasificacion
 * @property string $clasificacioningreso
 * @property boolean $insc_faov
 * @property boolean $trabaja
 * @property string $telefono_1
 * @property string $telefono_2
 * @property boolean $st_censado
 * @property boolean $migrado
 * @property string $comunidad
 * @property string $fam_clasificacion_id
 * @property string $nb_clasificacion
 * @property boolean $censo
 */
class BusquedaPersonaCensos extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BusquedaPersonaCensos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'censo.busqueda_persona_censos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_pais, tiempo_pais, parentesco_id, grado_instruccion_id, enfermedad_id, ocupacion_id, edo_civil_id, id_clasificacion', 'numerical', 'integerOnly'=>true),
			array('serial, familia_id, censado_id, beneficiado_sigevih_id, nacionalidad, cedula, nombre1, nombre2, apellido1, apellido2, fe_nac, sexo, e_mail, pais, rol, grado_instruccion, enfermedad, ocupacion, trabajo_id, institucion, ocupacion_otro, estado_embarazo, estudia, ingreso_mensual, ed_civil, clasificacioningreso, insc_faov, trabaja, telefono_1, telefono_2, st_censado, migrado, comunidad, fam_clasificacion_id, nb_clasificacion, censo', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('serial, familia_id, censado_id, beneficiado_sigevih_id, nacionalidad, cedula, nombre1, nombre2, apellido1, apellido2, fe_nac, sexo, e_mail, id_pais, pais, tiempo_pais, parentesco_id, rol, grado_instruccion_id, grado_instruccion, enfermedad_id, enfermedad, ocupacion_id, ocupacion, trabajo_id, institucion, ocupacion_otro, estado_embarazo, estudia, ingreso_mensual, edo_civil_id, ed_civil, id_clasificacion, clasificacioningreso, insc_faov, trabaja, telefono_1, telefono_2, st_censado, migrado, comunidad, fam_clasificacion_id, nb_clasificacion, censo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	public function visibleEliminarJefe0800($nacionalidad, $cedula){
		Yii::import('application.modules.familia.models.AdjAdjudicacion',true);
		$model = AdjAdjudicacion::model()->with('familia.famBeneficiados.persona')->find('nacionalidad=:nacionalidad AND cedula=:cedula AND st_adjudicacion=TRUE',array(':nacionalidad'=>strtoupper($nacionalidad),':cedula'=>$cedula));
		// 		Retorna FALSE si esta adjudicado
		if($model){
			return False;
		}
		else
			return TRUE;
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'serial' => 'Serial',
		
			'familia_id' => 'Familia',
			'cedula' => 'Cédula',
			'nombre1' => 'Primer Nombre',
			'nombre2' => 'Segundo Nombre',
			'apellido1' => 'Primer Apellido',
			'apellido2' => 'Segundo Apellido',
			'fe_nac' => 'Fe Nac',
			'sexo' => 'Sexo',
			'parentesco_id' => 'Parentesco',
			'rol' => 'Rol',
			'migrado' => 'Estatus',
			'comunidad' => 'Comunidad',
			'fam_clasificacion_id' => 'Clasificación',
			'nb_clasificacion' => 'Clasificación',
		
		
			'censado_id' => 'Censado',
			'beneficiado_sigevih_id' => 'Beneficiado Sigevih',
			'nacionalidad' => 'Nac',
			'fe_nac' => 'Fe Nac',
			'sexo' => 'Sexo',
			'e_mail' => 'E Mail',
			'id_pais' => 'Id Pais',
			'pais' => 'Pais',
			'tiempo_pais' => 'Tiempo Pais',
			'parentesco_id' => 'Parentesco',
			'rol' => 'Rol',
			'grado_instruccion_id' => 'Grado Instruccion',
			'grado_instruccion' => 'Grado Instruccion',
			'enfermedad_id' => 'Enfermedad',
			'enfermedad' => 'Enfermedad',
			'ocupacion_id' => 'Ocupacion',
			'ocupacion' => 'Ocupacion',
			'trabajo_id' => 'Trabajo',
			'institucion' => 'Institucion',
			'ocupacion_otro' => 'Ocupacion Otro',
			'estado_embarazo' => 'Estado Embarazo',
			'estudia' => 'Estudia',
			'ingreso_mensual' => 'Ingreso Mensual',
			'edo_civil_id' => 'Edo Civil',
			'ed_civil' => 'Ed Civil',
			'id_clasificacion' => 'Id Clasificacion',
			'clasificacioningreso' => 'Clasificacioningreso',
			'insc_faov' => 'Insc Faov',
			'trabaja' => 'Trabaja',
			'telefono_1' => 'Telefono 1',
			'telefono_2' => 'Telefono 2',
			'st_censado' => 'St Censado',
			'migrado' => 'Migrado',
			'comunidad' => 'Comunidad',
			'fam_clasificacion_id' => 'Clasificación',
			'nb_clasificacion' => 'Nb Clasificacion',
			'censo' => 'Censo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		//1070999
		
		
		//$model = PerPersona::model()->find('cedula=:persona_id',array('persona_id'=>16903321));//prueba
		$criteria=new CDbCriteria;
			//////////////////produccion
		$model = UsuUsuario::model()->with('persona')->find('usuario_id=:usuario_id',array(':usuario_id'=>Yii::app()->user->id));//realidad
		$criteria->compare('nacionalidad',$model->persona->nacionalidad);
		$criteria->compare('cedula',$model->persona->cedula);
		///////fin produccion 
		
		/////prueba
		//$criteria->compare('cedula','11304514');
			
//		
//		if(!$model )
//			$criteria->condition='cedula = null';
//		else {
////			$criteria->compare('nacionalidad',$model->nacionalidad);
////			$criteria->compare('cedula',$model->cedula);
//			$criteria->compare('nacionalidad',$this->nacionalidad);
//			$criteria->compare('cedula',$this->cedula);
//		}
//		
		$criteria->compare('familia_id',$this->familia_id,true);
		$criteria->compare('censado_id',$this->censado_id,true);
		$criteria->compare('beneficiado_sigevih_id',$this->beneficiado_sigevih_id,true);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('fe_nac',$this->fe_nac,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('e_mail',$this->e_mail,true);
		$criteria->compare('id_pais',$this->id_pais);
		$criteria->compare('pais',$this->pais,true);
		$criteria->compare('tiempo_pais',$this->tiempo_pais);
		$criteria->compare('parentesco_id',$this->parentesco_id);
		$criteria->compare('rol',$this->rol,true);
		$criteria->compare('grado_instruccion_id',$this->grado_instruccion_id);
		$criteria->compare('grado_instruccion',$this->grado_instruccion,true);
		$criteria->compare('enfermedad_id',$this->enfermedad_id);
		$criteria->compare('enfermedad',$this->enfermedad,true);
		$criteria->compare('ocupacion_id',$this->ocupacion_id);
		$criteria->compare('ocupacion',$this->ocupacion,true);
		$criteria->compare('trabajo_id',$this->trabajo_id,true);
		$criteria->compare('institucion',$this->institucion,true);
		$criteria->compare('ocupacion_otro',$this->ocupacion_otro,true);
		$criteria->compare('estado_embarazo',$this->estado_embarazo);
		$criteria->compare('estudia',$this->estudia);
		$criteria->compare('ingreso_mensual',$this->ingreso_mensual,true);
		$criteria->compare('edo_civil_id',$this->edo_civil_id);
		$criteria->compare('ed_civil',$this->ed_civil,true);
		$criteria->compare('id_clasificacion',$this->id_clasificacion);
		$criteria->compare('clasificacioningreso',$this->clasificacioningreso,true);
		$criteria->compare('insc_faov',$this->insc_faov);
		$criteria->compare('trabaja',$this->trabaja);
		$criteria->compare('telefono_1',$this->telefono_1,true);
		$criteria->compare('telefono_2',$this->telefono_2,true);
		$criteria->compare('st_censado',true);
		$criteria->compare('migrado',$this->migrado);
		$criteria->compare('comunidad',$this->comunidad,true);
		$criteria->compare('fam_clasificacion_id',$this->fam_clasificacion_id,true);
		$criteria->compare('nb_clasificacion',$this->nb_clasificacion,true);
		$criteria->compare('censo',$this->censo);
		$criteria->addCondition('parentesco_id = 12 AND (fam_clasificacion_id =6 OR fam_clasificacion_id = 7  )'); 
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function searchAdmin(){
		
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$criteria=new CDbCriteria;

		if(!isset($this->cedula) ||$this->cedula =="")
			$criteria->condition='cedula = null';
		else{
			$criteria->compare('cedula',trim($this->cedula));
		}
		$criteria->compare('nacionalidad',$this->nacionalidad);
		$criteria->compare('familia_id',$this->familia_id,true);
		$criteria->compare('censado_id',$this->censado_id,true);
		$criteria->compare('beneficiado_sigevih_id',$this->beneficiado_sigevih_id,true);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('fe_nac',$this->fe_nac,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('e_mail',$this->e_mail,true);
		$criteria->compare('id_pais',$this->id_pais);
		$criteria->compare('pais',$this->pais,true);
		$criteria->compare('tiempo_pais',$this->tiempo_pais);
		$criteria->compare('parentesco_id',$this->parentesco_id);
		$criteria->compare('rol',$this->rol,true);
		$criteria->compare('grado_instruccion_id',$this->grado_instruccion_id);
		$criteria->compare('grado_instruccion',$this->grado_instruccion,true);
		$criteria->compare('enfermedad_id',$this->enfermedad_id);
		$criteria->compare('enfermedad',$this->enfermedad,true);
		$criteria->compare('ocupacion_id',$this->ocupacion_id);
		$criteria->compare('ocupacion',$this->ocupacion,true);
		$criteria->compare('trabajo_id',$this->trabajo_id,true);
		$criteria->compare('institucion',$this->institucion,true);
		$criteria->compare('ocupacion_otro',$this->ocupacion_otro,true);
		$criteria->compare('estado_embarazo',$this->estado_embarazo);
		$criteria->compare('estudia',$this->estudia);
		$criteria->compare('ingreso_mensual',$this->ingreso_mensual,true);
		$criteria->compare('edo_civil_id',$this->edo_civil_id);
		$criteria->compare('ed_civil',$this->ed_civil,true);
		$criteria->compare('id_clasificacion',$this->id_clasificacion);
		$criteria->compare('clasificacioningreso',$this->clasificacioningreso,true);
		$criteria->compare('insc_faov',$this->insc_faov);
		$criteria->compare('trabaja',$this->trabaja);
		$criteria->compare('telefono_1',$this->telefono_1,true);
		$criteria->compare('telefono_2',$this->telefono_2,true);
		$criteria->compare('st_censado',$this->st_censado);
		$criteria->compare('migrado',$this->migrado);
		$criteria->compare('comunidad',$this->comunidad,true);
		$criteria->compare('fam_clasificacion_id',$this->fam_clasificacion_id,true);
		$criteria->compare('nb_clasificacion',$this->nb_clasificacion,true);
		$criteria->compare('censo',$this->censo);
		$criteria->addCondition('(fam_clasificacion_id =6 OR fam_clasificacion_id = 7  OR fam_clasificacion_id = 19) AND st_censado=TRUE'); 
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	
	}

		public function searchAdminJuventud(){
		
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$criteria=new CDbCriteria;

		if(!isset($this->cedula) ||$this->cedula =="")
			$criteria->condition='cedula = null';
		else{
			$criteria->compare('cedula',trim($this->cedula));
		}
		$criteria->compare('nacionalidad',$this->nacionalidad);
		$criteria->compare('familia_id',$this->familia_id,true);
		$criteria->compare('censado_id',$this->censado_id,true);
		$criteria->compare('beneficiado_sigevih_id',$this->beneficiado_sigevih_id,true);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('fe_nac',$this->fe_nac,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('e_mail',$this->e_mail,true);
		$criteria->compare('id_pais',$this->id_pais);
		$criteria->compare('pais',$this->pais,true);
		$criteria->compare('tiempo_pais',$this->tiempo_pais);
		$criteria->compare('parentesco_id',$this->parentesco_id);
		$criteria->compare('rol',$this->rol,true);
		$criteria->compare('grado_instruccion_id',$this->grado_instruccion_id);
		$criteria->compare('grado_instruccion',$this->grado_instruccion,true);
		$criteria->compare('enfermedad_id',$this->enfermedad_id);
		$criteria->compare('enfermedad',$this->enfermedad,true);
		$criteria->compare('ocupacion_id',$this->ocupacion_id);
		$criteria->compare('ocupacion',$this->ocupacion,true);
		$criteria->compare('trabajo_id',$this->trabajo_id,true);
		$criteria->compare('institucion',$this->institucion,true);
		$criteria->compare('ocupacion_otro',$this->ocupacion_otro,true);
		$criteria->compare('estado_embarazo',$this->estado_embarazo);
		$criteria->compare('estudia',$this->estudia);
		$criteria->compare('ingreso_mensual',$this->ingreso_mensual,true);
		$criteria->compare('edo_civil_id',$this->edo_civil_id);
		$criteria->compare('ed_civil',$this->ed_civil,true);
		$criteria->compare('id_clasificacion',$this->id_clasificacion);
		$criteria->compare('clasificacioningreso',$this->clasificacioningreso,true);
		$criteria->compare('insc_faov',$this->insc_faov);
		$criteria->compare('trabaja',$this->trabaja);
		$criteria->compare('telefono_1',$this->telefono_1,true);
		$criteria->compare('telefono_2',$this->telefono_2,true);
		$criteria->compare('st_censado',$this->st_censado);
		$criteria->compare('migrado',$this->migrado);
		$criteria->compare('comunidad',$this->comunidad,true);
		$criteria->compare('fam_clasificacion_id',$this->fam_clasificacion_id,true);
		$criteria->compare('nb_clasificacion',$this->nb_clasificacion,true);
		$criteria->compare('censo',$this->censo);
		$criteria->addCondition('( fam_clasificacion_id = 19) AND st_censado=TRUE'); 
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	
	}

}