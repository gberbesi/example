<?php

/**
 * This is the model class for table "fam_sector_social".
 *
 * The followings are the available columns in table 'fam_sector_social':
 * @property integer $id_fam_sector_social
 * @property string $descripcion
 * @property boolean $st_sector_social
 */
class FamSectorSocial extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FamSectorSocial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fam_sector_social';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, st_sector_social', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_fam_sector_social, descripcion, st_sector_social', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_fam_sector_social' => 'Id Fam Sector Social',
			'descripcion' => 'Descripcion',
			'st_sector_social' => 'St Sector Social',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_fam_sector_social',$this->id_fam_sector_social);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('st_sector_social',$this->st_sector_social);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}