<?php

/**
 * This is the model class for table "per_trabajo".
 *
 * The followings are the available columns in table 'per_trabajo':
 * @property integer $trabajo_id
 * @property string $nb_trabajo
 * @property integer $tp_sector
 * @property boolean $st_trabajo
 * @property string $usu_aud_id
 *
 * The followings are the available model relations:
 * @property FamBeneficiado[] $famBeneficiados
 */
class PerTrabajo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PerTrabajo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'per_trabajo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nb_trabajo, tp_sector', 'required'),
			array('tp_sector', 'numerical', 'integerOnly'=>true),
			array('st_trabajo, usu_aud_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('trabajo_id, nb_trabajo, tp_sector, st_trabajo, usu_aud_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'famBeneficiados' => array(self::HAS_MANY, 'FamBeneficiado', 'trabajo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'trabajo_id' => 'Trabajo',
			'nb_trabajo' => 'Nb Trabajo',
			'tp_sector' => 'Tp Sector',
			'st_trabajo' => 'St Trabajo',
			'usu_aud_id' => 'Usu Aud',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('trabajo_id',$this->trabajo_id);
		$criteria->compare('nb_trabajo',$this->nb_trabajo,true);
		$criteria->compare('tp_sector',$this->tp_sector);
		$criteria->compare('st_trabajo',$this->st_trabajo);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}