<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class RecuperarForm extends CFormModel
{
	public $nacionalidad;
	public $cedula;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// verifyCode needs to be entered correctly
			array('cedula, nacionalidad', 'required'),
			array('cedula','length', 'max'=>10),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'nacionalidad'=>'Nacionalidad',
			'cedula'=>'Cédula',
		);
	}
	public function claveAleatoria(){
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$cad = "";
		for($i=0;$i<8;$i++) {
		$cad .= substr($str,rand(0,62),1);
		}
		return $cad;
	}
	public function claveRecuperacion($aquien,$aquiennombre,$mensaje)
	{
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		$mail = new JPhpMailer;
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['mailHost'];/*'172.16.0.20';*/
		$mail->Port = Yii::app()->params['mailPortSsl'];
		$mail->CharSet = "utf-8";
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';
		$mail->Username = Yii::app()->params['mailUsername'];/*'xmolina';*/
		$mail->Password = Yii::app()->params['mailUserPassw'];
		$mail->SetFrom(Yii::app()->params['mailRemitente'], Yii::app()->params['nombreRemitenteActualizacion']);
		
		$mail->Subject = Yii::app()->params['mailAsuntoActualizacion'];
		//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($mensaje);
		$mail->AddAddress(/*'xmolina@minvih.gob.ve'*/$aquien, $aquiennombre);
		
		if(!$mail->Send()) {
		   throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente intentelo mas tarde.',500);
		}
		else {
		   //echo 'Mail enviado!';
		}

	}	
}
