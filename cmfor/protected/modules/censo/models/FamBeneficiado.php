<?php

/**
 * This is the model class for table "fam_beneficiado".
 *
 * The followings are the available columns in table 'fam_beneficiado':
 * @property integer $beneficiado_id
 * @property integer $persona_id
 * @property integer $enfermedad_id
 * @property integer $grado_instruccion_id
 * @property integer $ocupacion_id
 * @property string $ocupacion_otro
 * @property boolean $estado_embarazo
 * @property boolean $estudia
 * @property string $ingreso_mensual
 * @property string $url_imagen
 * @property integer $edo_civil_id
 * @property string $usu_aud_id
 * @property string $trabajo_id
 * @property string $id_clasificacion
 * @property boolean $insc_faov
 * @property boolean $trabaja
 * @property integer $grado_academico_id
 * @property integer $deporte_id
 * @property string $mision_id
 * @property string $cargo
 * @property string $direccion_trabajo
 * @property string $lugar_trabajo
 * @property string $institucion_de_trabajo
 * @property string $tlf_oficina
 * @property boolean $discapacitado
 * @property integer $fam_nivel_riesgo_id
 *
 * The followings are the available model relations:
 * @property ClasificacionIngreso $idClasificacion
 * @property FamEdoCivil $edoCivil
 * @property FamDeporte $deporte
 * @property FamGradoAcademico $gradoAcademico
 * @property FamMision $mision
 * @property FamEnfermedad $enfermedad
 * @property FamGradoInstruccion $gradoInstruccion
 * @property FamOcupacion $ocupacion
 * @property PerPersona $persona
 * @property PerTrabajo $trabajo
 * @property FamNivelRiesgo $famNivelRiesgo
 * @property FamFamilia[] $famFamilias
 */
class FamBeneficiado extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FamBeneficiado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fam_beneficiado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('persona_id', 'required'),
			array('persona_id, enfermedad_id, grado_instruccion_id, ocupacion_id, edo_civil_id, grado_academico_id, deporte_id, fam_nivel_riesgo_id', 'numerical', 'integerOnly'=>true),
			array('ingreso_mensual', 'length', 'max'=>8),
			array('ocupacion_otro, estado_embarazo, estudia, url_imagen, usu_aud_id, trabajo_id, id_clasificacion, insc_faov, trabaja, mision_id, cargo, direccion_trabajo, lugar_trabajo, institucion_de_trabajo, tlf_oficina, discapacitado', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('beneficiado_id, persona_id, enfermedad_id, grado_instruccion_id, ocupacion_id, ocupacion_otro, estado_embarazo, estudia, ingreso_mensual, url_imagen, edo_civil_id, usu_aud_id, trabajo_id, id_clasificacion, insc_faov, trabaja, grado_academico_id, deporte_id, mision_id, cargo, direccion_trabajo, lugar_trabajo, institucion_de_trabajo, tlf_oficina, discapacitado, fam_nivel_riesgo_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idClasificacion' => array(self::BELONGS_TO, 'ClasificacionIngreso', 'id_clasificacion'),
			'edoCivil' => array(self::BELONGS_TO, 'FamEdoCivil', 'edo_civil_id'),
			'deporte' => array(self::BELONGS_TO, 'FamDeporte', 'deporte_id'),
			'gradoAcademico' => array(self::BELONGS_TO, 'FamGradoAcademico', 'grado_academico_id'),
			'mision' => array(self::BELONGS_TO, 'FamMision', 'mision_id'),
			'enfermedad' => array(self::BELONGS_TO, 'FamEnfermedad', 'enfermedad_id'),
			'gradoInstruccion' => array(self::BELONGS_TO, 'FamGradoInstruccion', 'grado_instruccion_id'),
			'ocupacion' => array(self::BELONGS_TO, 'FamOcupacion', 'ocupacion_id'),
			'persona' => array(self::BELONGS_TO, 'PerPersona', 'persona_id'),
			'trabajo' => array(self::BELONGS_TO, 'PerTrabajo', 'trabajo_id'),
			'famNivelRiesgo' => array(self::BELONGS_TO, 'FamNivelRiesgo', 'fam_nivel_riesgo_id'),
			'famFamilias' => array(self::MANY_MANY, 'FamFamilia', 'fam_familia_beneficiado(beneficiado_id, familia_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'beneficiado_id' => 'Beneficiado',
			'persona_id' => 'Persona',
			'enfermedad_id' => 'Enfermedad',
			'grado_instruccion_id' => 'Grado Instruccion',
			'ocupacion_id' => 'Ocupacion',
			'ocupacion_otro' => 'Ocupacion Otro',
			'estado_embarazo' => 'Estado Embarazo',
			'estudia' => 'Estudia',
			'ingreso_mensual' => 'Ingreso Mensual',
			'url_imagen' => 'Url Imagen',
			'edo_civil_id' => 'Edo Civil',
			'usu_aud_id' => 'Usu Aud',
			'trabajo_id' => 'Trabajo',
			'id_clasificacion' => 'Id Clasificacion',
			'insc_faov' => 'Insc Faov',
			'trabaja' => 'Trabaja',
			'grado_academico_id' => 'Grado Academico',
			'deporte_id' => 'Deporte',
			'mision_id' => 'Mision',
			'cargo' => 'Cargo',
			'direccion_trabajo' => 'Direccion Trabajo',
			'lugar_trabajo' => 'Lugar Trabajo',
			'institucion_de_trabajo' => 'Institucion De Trabajo',
			'tlf_oficina' => 'Tlf Oficina',
			'discapacitado' => 'Discapacitado',
			'fam_nivel_riesgo_id' => 'Fam Nivel Riesgo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('beneficiado_id',$this->beneficiado_id);
		$criteria->compare('persona_id',$this->persona_id);
		$criteria->compare('enfermedad_id',$this->enfermedad_id);
		$criteria->compare('grado_instruccion_id',$this->grado_instruccion_id);
		$criteria->compare('ocupacion_id',$this->ocupacion_id);
		$criteria->compare('ocupacion_otro',$this->ocupacion_otro,true);
		$criteria->compare('estado_embarazo',$this->estado_embarazo);
		$criteria->compare('estudia',$this->estudia);
		$criteria->compare('ingreso_mensual',$this->ingreso_mensual,true);
		$criteria->compare('url_imagen',$this->url_imagen,true);
		$criteria->compare('edo_civil_id',$this->edo_civil_id);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('trabajo_id',$this->trabajo_id,true);
		$criteria->compare('id_clasificacion',$this->id_clasificacion,true);
		$criteria->compare('insc_faov',$this->insc_faov);
		$criteria->compare('trabaja',$this->trabaja);
		$criteria->compare('grado_academico_id',$this->grado_academico_id);
		$criteria->compare('deporte_id',$this->deporte_id);
		$criteria->compare('mision_id',$this->mision_id,true);
		$criteria->compare('cargo',$this->cargo,true);
		$criteria->compare('direccion_trabajo',$this->direccion_trabajo,true);
		$criteria->compare('lugar_trabajo',$this->lugar_trabajo,true);
		$criteria->compare('institucion_de_trabajo',$this->institucion_de_trabajo,true);
		$criteria->compare('tlf_oficina',$this->tlf_oficina,true);
		$criteria->compare('discapacitado',$this->discapacitado);
		$criteria->compare('fam_nivel_riesgo_id',$this->fam_nivel_riesgo_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}