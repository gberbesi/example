<?php

/**
 * This is the model class for table "censo.censado".
 *
 * The followings are the available columns in table 'censo.censado':
 * @property integer $censado_id
 * @property string $nacionalidad
 * @property integer $cedula
 * @property string $nombre1
 * @property string $nombre2
 * @property string $apellido1
 * @property string $apellido2
 * @property string $fe_nac
 * @property string $sexo
 * @property string $e_mail
 * @property string $id_pais
 * @property integer $tiempo_pais
 * @property integer $enfermedad_id
 * @property integer $grado_instruccion_id
 * @property integer $ocupacion_id
 * @property string $ocupacion_otro
 * @property boolean $estado_embarazo
 * @property boolean $estudia
 * @property string $ingreso_mensual
 * @property integer $edo_civil_id
 * @property string $usu_aud_id
 * @property string $trabajo_id
 * @property string $id_clasificacion
 * @property boolean $insc_faov
 * @property boolean $trabaja
 * @property integer $parentesco_id
 * @property boolean $st_censado
 * @property string $familia_id
 * @property string $telefono_1
 * @property string $telefono_2
 * @property string $beneficiado_sigevih_id
 * @property boolean $migrado
 * @property string $mision_id
 * @property string $fecha_migracion
 * @property string $twitter
 * @property string $institucion_de_trabajo
 * @property string $direccion_donde_labora
 * @property string $foto_jefe_familia
 * @property integer $nro_habitaciones_de_la_residencia
 * @property string $telefono_3
 * @property integer $forma_de_pago
 * @property string $honorarios_profesionales
 * @property string $bono_alimentacion
 * @property string $otros_ingresos
 * @property string $lugar_trabajo
 * @property string $serial_cdlp
 * @property string $codigo_cdlp
 */
	
class Censado extends CActiveRecord
{
	public $contrasena;
	public $censo0800;
	public $censoGmvv;
	public $mensaje="";
	public $codigo;
	public $verifyCode;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Censado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'censo.censado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		
			//rol jefe familia
			array('discapacitado, fam_nivel_riesgo_id, cedula, nacionalidad, nombre1, apellido1,  edo_civil_id, fe_nac, sexo, telefono_1, grado_instruccion_id, ocupacion_id, ingreso_mensual, serial_cdlp, codigo_cdlp,tipo_solicitud_id2, e_mail', 'required','on'=>'jefefamilia'),
			array('cedula','length','min'=>5,'max'=>8,'on'=>'jefefamilia'),
			array('nombre1,nombre2,apellido1,apellido2', 'match', 'pattern'=>'/^([a-zA-ZÁÉÍÓÚáéíóúñÑ\s])+$/', 'message' => 'Este campo puede contener solamente letras','on'=>'jefefamilia'),
			array('e_mail', 'email','on'=>'jefefamilia'),
			array('ingreso_mensual,honorarios_profesionales,bono_alimentacion,otros_ingresos', 'match',  //PARA VALIDAR CAMPOS CON FORMATO MONEDA
						'pattern' => '/^[0-9]\d{0,2}(\.[0-9]\d{2,2})*(\,\d{1,2})?$/',
						'message' => 'El valor del campo debe tener un formato 100.000,00','on'=>'jefefamilia'),
			array('ingreso_mensual', 'montoMaximo', 'max'=>18,'on'=>'jefefamilia'),
			array('honorarios_profesionales', 'montoMaximo', 'max'=>10,'on'=>'jefefamilia'),
			array('bono_alimentacion', 'montoMaximo', 'max'=>8,'on'=>'jefefamilia'),
			array('otros_ingresos', 'montoMaximo', 'max'=>10,'on'=>'jefefamilia'),
			array('cedula, tiempo_pais, enfermedad_id, grado_instruccion_id, ocupacion_id, edo_civil_id, parentesco_id, nro_habitaciones_de_la_residencia, forma_de_pago', 'numerical', 'integerOnly'=>true,'on'=>'jefefamilia'),
			array('fe_nac','date','format'=>'dd/MM/yyyy','message'=>'El formato de Fecha es inválido (dd/mm/aaaa)','on'=>'jefefamilia'),
			  
		
			//rol carga familiar
			array('discapacitado, nombre1, apellido1, parentesco_id, edo_civil_id, fe_nac, sexo, grado_instruccion_id, ocupacion_id', 'required','on'=>'cargafamiliar'),
			array('cedula','length','min'=>5,'max'=>8,'on'=>'cargafamiliar'),
			array('nombre1,nombre2,apellido1,apellido2', 'match', 'pattern'=>'/^([a-zA-ZÁÉÍÓÚáéíóúñÑ\s])+$/', 'message' => 'Este campo puede contener solamente letras','on'=>'cargafamiliar'),
			array('e_mail', 'email','on'=>'cargafamiliar'),
			array('ingreso_mensual,honorarios_profesionales,bono_alimentacion,otros_ingresos', 'match',  //PARA VALIDAR CAMPOS CON FORMATO MONEDA
						'pattern' => '/^[0-9]\d{0,2}(\.[0-9]\d{2,2})*(\,\d{1,2})?$/',
						'message' => 'El valor del campo debe tener un formato 100.000,00','on'=>'cargafamiliar'),
			array('ingreso_mensual', 'montoMaximo', 'max'=>18,'on'=>'cargafamiliar'),
			array('honorarios_profesionales', 'montoMaximo', 'max'=>10,'on'=>'cargafamiliar'),
			array('bono_alimentacion', 'montoMaximo', 'max'=>8,'on'=>'cargafamiliar'),
			array('otros_ingresos', 'montoMaximo', 'max'=>10,'on'=>'cargafamiliar'),
			array('cedula, tiempo_pais, enfermedad_id, grado_instruccion_id, ocupacion_id, edo_civil_id, parentesco_id, nro_habitaciones_de_la_residencia, forma_de_pago', 'numerical', 'integerOnly'=>true,'on'=>'cargafamiliar'),
			array('fe_nac','date','format'=>'dd/MM/yyyy','message'=>'El formato de Fecha es inválido (dd/mm/aaaa)','on'=>'cargafamiliar'),

			//array('cedula', 'valida_cedula','on'=>'cargafamiliar'),
			
			array('cedula, e_mail', 'required','on' => array('reiniciodedatos')),
			array('e_mail', 'email','on' => array('reiniciodedatos')),
			
			array('codigo','required','on'=>array('verificarCodigo')),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(),'on'=>'verificarCodigo'),
			
			//array('nacionalidad, sexo', 'length', 'max'=>1),
			//array('ingreso_mensual', 'length', 'max'=>8),
			//array('honorarios_profesionales, otros_ingresos', 'length', 'max'=>12),
			//array('bono_alimentacion', 'length', 'max'=>10),
			array('mision, estado_trabaja_id, municipio_trabaja_id, parroquia_trabaja_id, estado_estudia_id, municipio_estudia_id, parroquia_estudia_id, estado_terreno_id, municipio_terreno_id, parroquia_terreno_id, estado_uno_id, municipio_uno_id, parroquia_uno_id, estado_dos_id, municipio_dos_id, parroquia_dos_id, tiempo_admpublica_id, fam_sector_social_id, propietario_terreno, nro_conapdis, fam_nivel_riesgo_id, nacionalidad, nombre2, apellido2, fe_nac, e_mail, id_pais, ocupacion_otro, estado_embarazo, estudia, usu_aud_id, trabajo_id, id_clasificacion, insc_faov, trabaja, st_censado, familia_id, telefono_1, telefono_2, beneficiado_sigevih_id, migrado, mision_id, fecha_migracion, twitter, institucion_de_trabajo, direccion_donde_labora, foto_jefe_familia, telefono_3, lugar_trabajo, serial_cdlp, codigo_cdlp, mision,mision2,mision_id2,tipo_solicitud_id2,pertenece_avv,nombre_avv, codigo, contrasena', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('censado_id, nacionalidad, cedula, nombre1, nombre2, apellido1, apellido2, fe_nac, sexo, e_mail, id_pais, tiempo_pais, enfermedad_id, grado_instruccion_id, ocupacion_id, ocupacion_otro, estado_embarazo, estudia, ingreso_mensual, edo_civil_id, usu_aud_id, trabajo_id, id_clasificacion, insc_faov, trabaja, parentesco_id, st_censado, familia_id, telefono_1, telefono_2, beneficiado_sigevih_id, migrado, mision_id, fecha_migracion, twitter, institucion_de_trabajo, direccion_donde_labora, foto_jefe_familia, nro_habitaciones_de_la_residencia, telefono_3, forma_de_pago, honorarios_profesionales, bono_alimentacion, otros_ingresos, lugar_trabajo, serial_cdlp, codigo_cdlp ,mision2,mision_id2,tipo_solicitud_id2,pertenece_avv,nombre_avv', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parentesco' => array(self::BELONGS_TO, 'FamParentesco', 'parentesco_id'),
			'famParentesco' => array(self::BELONGS_TO, 'FamParentesco', 'parentesco_id'),
			
			'idClasificacion' => array(self::BELONGS_TO, 'ClasificacionIngreso', 'id_clasificacion'),
			'beneficiadoSigevih' => array(self::BELONGS_TO, 'FamBeneficiado', 'beneficiado_sigevih_id'),
			'idPais' => array(self::BELONGS_TO, 'Pais', 'id_pais'),
			'parentesco' => array(self::BELONGS_TO, 'FamParentesco', 'parentesco_id'),
			'ocupacion' => array(self::BELONGS_TO, 'FamOcupacion', 'ocupacion_id'),
			'gradoInstruccion' => array(self::BELONGS_TO, 'FamGradoInstruccion', 'grado_instruccion_id'),
			'enfermedad' => array(self::BELONGS_TO, 'FamEnfermedad', 'enfermedad_id'),
			'edoCivil' => array(self::BELONGS_TO, 'FamEdoCivil', 'edo_civil_id'),
			'trabajo' => array(self::BELONGS_TO, 'PerTrabajo', 'trabajo_id'),
			'familia' => array(self::BELONGS_TO, 'Familia', 'familia_id'),
			'mision' => array(self::BELONGS_TO, 'FamMision', 'mision_id'),
            'ci' => array(self::BELONGS_TO, 'GrGmvvT', 'cedula'),
            'id_gr_gmvv_t' => array(self::BELONGS_TO, 'GrGmvvT', 'censado_id'), 
            'cedula' => array(self::BELONGS_TO, 'Censado', 'ci'), 
            'censado_id' => array(self::BELONGS_TO, 'Censado', 'id_gr_gmvv_t'), 
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'censado_id' => 'Censado',
			'nacionalidad' => 'Nacionalidad',
			'cedula' => 'Cédula',
			'nombre1' => 'Primer Nombre',
			'nombre2' => 'Segundo Nombre',
			'apellido1' => 'Primer Apellido',
			'apellido2' => 'Segundo Apellido',
			'fe_nac' => 'Fecha de Nacimiento',
			'sexo' => 'Género',
			'e_mail' => 'Dirección de Correo Electrónico',
			'id_pais' => 'País de Origen (Solo para Extranjeros)',
			'tiempo_pais' => 'Año de Origen (Solo para Extranjeros)',
			'enfermedad_id' => 'Enfermedad',
			'grado_instruccion_id' => 'Grado de Instrucción',
			'ocupacion_id' => 'Ocupación',
			'ocupacion_otro' => 'Ocupacion Otro',
			'estado_embarazo' => '¿Estado de Embarazo?',
			'estudia' => '¿Estudia?',
			'ingreso_mensual' => 'Ingreso Mensual',
			'edo_civil_id' => 'Estado Civil',
			'usu_aud_id' => 'Usu Aud',
			'trabajo_id' => 'Condición de Empleo',
			'id_clasificacion' => 'Clasificación de Ingreso',
			'insc_faov' => '¿Cotiza Faov?',
			'trabaja' => '¿Actualmente Labora?',
			'parentesco_id' => 'Parentesco en relación jefe familiar',
			'st_censado' => 'St Censado',
			'familia_id' => 'Familia',
			'telefono_1' => 'N° de Celular',
			'telefono_2' => 'Otro N° de Teléfono',
			'beneficiado_sigevih_id' => 'Beneficiado Sigevih',
			'migrado' => 'Migrado',
			'mision_id' => 'Especifique',
			'fecha_migracion' => 'Fecha Migracion',
			'twitter' => 'Twitter',
			'institucion_de_trabajo' => 'Institucion De Trabajo',
			'direccion_donde_labora' => 'Direccion Donde Labora',
			'foto_jefe_familia' => 'Foto Jefe Familia',
			'nro_habitaciones_de_la_residencia' => 'Nro Habitaciones De La Residencia',
			'telefono_3' => 'Telefono 3',
			'forma_de_pago' => 'Forma De Pago',
			'honorarios_profesionales' => 'Honorarios profesionales',
			'bono_alimentacion' => 'Bono Alimentación',
			'otros_ingresos' => 'Otros Ingresos',
			'lugar_trabajo' => 'Lugar Trabajo',
			'serial_cdlp' => 'Serial Carnet de la Patria',
			'codigo_cdlp' => 'Código Carnet de la Patria',
			'mision' => '¿Presta servicios en alguna misión?',
			'fam_nivel_riesgo_id' => 'Nivel de Riesgo Vital',
			'discapacitado' => '¿Posee Discapacidad?',
			'nro_conapdis' => 'Número CONAPDIS',
			'propietario_terreno' => '¿Es usted propietario de algún terreno?',
			'fam_sector_social_id' => 'Sector al cual pertenece',
			'tiempo_admpublica_id' => 'Años en la Administración Pública',
			
			'estado_uno_id' => 'Estado',
			'municipio_uno_id' => 'Municipio',
			'parroquia_uno_id' => 'Parroquia',
			'estado_dos_id' => 'Estado',
			'municipio_dos_id' => 'Municipio',
			'parroquia_dos_id' => 'Parroquia',
			
			'estado_terreno_id' => 'Estado del terreno',
			'municipio_terreno_id' => 'Municipio del terreno',
			'parroquia_terreno_id' => 'Parroquia del terreno',
			
			'estado_estudia_id' => 'Estado donde estudia',
			'municipio_estudia_id' => 'Municipio donde estudia',
			'parroquia_estudia_id' => 'Parroquia donde estudia',
			
			'estado_trabaja_id' => 'Estado donde trabaja',
			'municipio_trabaja_id' => 'Municipio donde trabaja',
			'parroquia_trabaja_id' => 'Parroquia donde trabaja',
			'mision_id2'=>'Especifique',
			'mision2'=>'¿Es o ha sido beneficiado por alguna misión?',
			'tipo_solicitud_id2'=>'Tipo de solicitud (Actual)',
			'tipo_solicitud_id'=>'Tipo de solicitud (Anterior)',
			'pertenece_avv'=>'¿Pertenece a una Asamblea de Viviendo Venezolano?',
			'nombre_avv'=>'Nombre de la AVV',
			
			'contrasena' => 'Reiniciar Contraseña',
			'censo0800' => '¿0800?',
			'censoGmvv' => '¿GMVV?',
			'codigo'=>'Introduzca Código',
			'verifyCode'=>'Código de Verificación',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('censado_id',$this->censado_id);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('cedula',$this->cedula);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('fe_nac',$this->fe_nac,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('e_mail',$this->e_mail,true);
		$criteria->compare('id_pais',$this->id_pais,true);
		$criteria->compare('tiempo_pais',$this->tiempo_pais);
		$criteria->compare('enfermedad_id',$this->enfermedad_id);
		$criteria->compare('grado_instruccion_id',$this->grado_instruccion_id);
		$criteria->compare('ocupacion_id',$this->ocupacion_id);
		$criteria->compare('ocupacion_otro',$this->ocupacion_otro,true);
		$criteria->compare('estado_embarazo',$this->estado_embarazo);
		$criteria->compare('estudia',$this->estudia);
		$criteria->compare('ingreso_mensual',$this->ingreso_mensual,true);
		$criteria->compare('edo_civil_id',$this->edo_civil_id);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('trabajo_id',$this->trabajo_id,true);
		$criteria->compare('id_clasificacion',$this->id_clasificacion,true);
		$criteria->compare('insc_faov',$this->insc_faov);
		$criteria->compare('trabaja',$this->trabaja);
		$criteria->compare('parentesco_id',$this->parentesco_id);
		$criteria->compare('st_censado',$this->st_censado);
		$criteria->compare('familia_id',$this->familia_id,true);
		$criteria->compare('telefono_1',$this->telefono_1,true);
		$criteria->compare('telefono_2',$this->telefono_2,true);
		$criteria->compare('beneficiado_sigevih_id',$this->beneficiado_sigevih_id,true);
		$criteria->compare('migrado',$this->migrado);
		$criteria->compare('mision_id',$this->mision_id,true);
		$criteria->compare('fecha_migracion',$this->fecha_migracion,true);
		$criteria->compare('twitter',$this->twitter,true);
		$criteria->compare('institucion_de_trabajo',$this->institucion_de_trabajo,true);
		$criteria->compare('direccion_donde_labora',$this->direccion_donde_labora,true);
		$criteria->compare('foto_jefe_familia',$this->foto_jefe_familia,true);
		$criteria->compare('nro_habitaciones_de_la_residencia',$this->nro_habitaciones_de_la_residencia);
		$criteria->compare('telefono_3',$this->telefono_3,true);
		$criteria->compare('forma_de_pago',$this->forma_de_pago);
		$criteria->compare('honorarios_profesionales',$this->honorarios_profesionales,true);
		$criteria->compare('bono_alimentacion',$this->bono_alimentacion,true);
		$criteria->compare('otros_ingresos',$this->otros_ingresos,true);
		$criteria->compare('lugar_trabajo',$this->lugar_trabajo,true);
		$criteria->compare('serial_cdlp',$this->serial_cdlp,true);
		$criteria->compare('codigo_cdlp',$this->codigo_cdlp,true);
		$criteria->compare('pertenece_avv',$this->pertenece_avv);
		$criteria->compare('nombre_avv',$this->nombre_avv);
		



	
		$criteria->compare('mision_id2',$this->mision_id2,true);

		$criteria->compare('tipo_solicitud_id2',$this->tipo_solicitud_id2,true);



		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function fecha() {		
		$tope = date("Y");
		$edad_max = 120;
		$fechas = array();
		
		for($a= $tope - $edad_max; $a<=$tope; $a++)
			$fechas[$a]=$a;
		
        return $fechas;
	}
	
	public function montoMaximo($attribute,$params) {

		if(!$this->hasErrors($attribute)) {
			
			$numero=str_replace('.', '', $this->$attribute);
			$numero=str_replace(',', '.', $numero);
			
			$numero = explode(".",$numero);

			if(strlen($numero[0])>$params['max']) {
				$this->addError($attribute, 'Número excede el valor máximo permitido');
			}
		
		}

	}
	
	public function beforeSave() {
		foreach ($this->attributes as $key => $value)
			if ($value=='')
				$this->$key = NULL;
			
		return parent::beforeSave();
	}


	public function valida_cedula($attribute,$params) {

		$cedu= PerPersona::model()->findByAttributes(array('cedula'=>$this->$attribute));

			 if($cedu) {
			 	$this->addError($attribute, 'ESTA CEDULA YA ESTA REGISTRADA');
	}	
}

	public function correo($aquien,$aquiennombre,$mensaje)
	{
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		$mail = new JPhpMailer;
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['mailHost'];/*'172.16.0.20';*/
		$mail->Port = Yii::app()->params['mailPortSsl'];
		$mail->CharSet = "utf-8";
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';
		$mail->Username = Yii::app()->params['mailUsername'];/*'xmolina';*/
		$mail->Password = Yii::app()->params['mailUserPassw'];
		$mail->SetFrom(Yii::app()->params['mailRemitente'], Yii::app()->params['nombreRemitente']);
		
		$mail->Subject = Yii::app()->params['mailAsunto'];
		//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($mensaje);
		$mail->AddAddress($aquien, $aquiennombre);
		
		if(!$mail->Send()) {
		   throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente intentelo mas tarde.',500);
		}
		else {
		   //echo 'Mail enviado!';
		}

	}	

}



