<?php

/**
 * This is the model class for table "beneficios.beneficio".
 *
 * The followings are the available columns in table 'beneficios.beneficio':
 * @property integer $beneficio_id
 * @property string $fecha_creacion
 * @property integer $geo_estado_origen_id
 * @property integer $geo_municipio_origen_id
 * @property integer $geo_parroquia_origen_id
 * @property string $geo_ciudad_origen
 * @property string $geo_sector_origen
 * @property string $geo_calle_origen
 * @property string $geo_nombre_edificacion
 * @property string $geo_num_casa_apto_origen
 * @property integer $geo_estado_destino_id
 * @property integer $geo_municipio_destino_id
 * @property integer $geo_parroquia_destino_id
 * @property string $geo_ciudad_destino
 * @property string $geo_sector_destino
 * @property string $geo_calle_destino
 * @property string $geo_nombre_edificacion_destino
 * @property string $geo_num_casa_apto_destino
 * @property string $geo_etapa_destino
 * @property string $geo_piso_destino
 * @property integer $beneficio_tipo_vivienda_id
 * @property integer $desarrollo_id
 * @property integer $tipo_beneficio_id
 * @property string $fecha_adjudicacion
 * @property string $beneficio_fuente
 * @property string $organizacion
 * @property string $monto_beneficio
 * @property integer $id_usuario
 * @property string $fecha_modificacion
 * @property boolean $carga_masiva
 * @property string $usu_aud_id
 */
class Beneficio extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Beneficio the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'beneficios.beneficio';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('geo_estado_origen_id, geo_municipio_origen_id, geo_parroquia_origen_id, geo_estado_destino_id, geo_municipio_destino_id, geo_parroquia_destino_id, beneficio_tipo_vivienda_id, desarrollo_id, tipo_beneficio_id, id_usuario', 'numerical', 'integerOnly'=>true),
            array('monto_beneficio', 'length', 'max'=>15),
            array('fecha_creacion, geo_ciudad_origen, geo_sector_origen, geo_calle_origen, geo_nombre_edificacion, geo_num_casa_apto_origen, geo_ciudad_destino, geo_sector_destino, geo_calle_destino, geo_nombre_edificacion_destino, geo_num_casa_apto_destino, geo_etapa_destino, geo_piso_destino, fecha_adjudicacion, beneficio_fuente, organizacion, fecha_modificacion, carga_masiva, usu_aud_id', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('beneficio_id, fecha_creacion, geo_estado_origen_id, geo_municipio_origen_id, geo_parroquia_origen_id, geo_ciudad_origen, geo_sector_origen, geo_calle_origen, geo_nombre_edificacion, geo_num_casa_apto_origen, geo_estado_destino_id, geo_municipio_destino_id, geo_parroquia_destino_id, geo_ciudad_destino, geo_sector_destino, geo_calle_destino, geo_nombre_edificacion_destino, geo_num_casa_apto_destino, geo_etapa_destino, geo_piso_destino, beneficio_tipo_vivienda_id, desarrollo_id, tipo_beneficio_id, fecha_adjudicacion, beneficio_fuente, organizacion, monto_beneficio, id_usuario, fecha_modificacion, carga_masiva, usu_aud_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'beneficio_id' => 'Beneficio',
            'fecha_creacion' => 'Fecha Creacion',
            'geo_estado_origen_id' => 'Geo Estado Origen',
            'geo_municipio_origen_id' => 'Geo Municipio Origen',
            'geo_parroquia_origen_id' => 'Geo Parroquia Origen',
            'geo_ciudad_origen' => 'Geo Ciudad Origen',
            'geo_sector_origen' => 'Geo Sector Origen',
            'geo_calle_origen' => 'Geo Calle Origen',
            'geo_nombre_edificacion' => 'Geo Nombre Edificacion',
            'geo_num_casa_apto_origen' => 'Geo Num Casa Apto Origen',
            'geo_estado_destino_id' => 'Geo Estado Destino',
            'geo_municipio_destino_id' => 'Geo Municipio Destino',
            'geo_parroquia_destino_id' => 'Geo Parroquia Destino',
            'geo_ciudad_destino' => 'Geo Ciudad Destino',
            'geo_sector_destino' => 'Geo Sector Destino',
            'geo_calle_destino' => 'Geo Calle Destino',
            'geo_nombre_edificacion_destino' => 'Geo Nombre Edificacion Destino',
            'geo_num_casa_apto_destino' => 'Geo Num Casa Apto Destino',
            'geo_etapa_destino' => 'Geo Etapa Destino',
            'geo_piso_destino' => 'Geo Piso Destino',
            'beneficio_tipo_vivienda_id' => 'Beneficio Tipo Vivienda',
            'desarrollo_id' => 'Desarrollo',
            'tipo_beneficio_id' => 'Tipo Beneficio',
            'fecha_adjudicacion' => 'Fecha Adjudicacion',
            'beneficio_fuente' => 'Beneficio Fuente',
            'organizacion' => 'Organizacion',
            'monto_beneficio' => 'Monto Beneficio',
            'id_usuario' => 'Id Usuario',
            'fecha_modificacion' => 'Fecha Modificacion',
            'carga_masiva' => 'Carga Masiva',
            'usu_aud_id' => 'Usu Aud',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('beneficio_id',$this->beneficio_id);
        $criteria->compare('fecha_creacion',$this->fecha_creacion,true);
        $criteria->compare('geo_estado_origen_id',$this->geo_estado_origen_id);
        $criteria->compare('geo_municipio_origen_id',$this->geo_municipio_origen_id);
        $criteria->compare('geo_parroquia_origen_id',$this->geo_parroquia_origen_id);
        $criteria->compare('geo_ciudad_origen',$this->geo_ciudad_origen,true);
        $criteria->compare('geo_sector_origen',$this->geo_sector_origen,true);
        $criteria->compare('geo_calle_origen',$this->geo_calle_origen,true);
        $criteria->compare('geo_nombre_edificacion',$this->geo_nombre_edificacion,true);
        $criteria->compare('geo_num_casa_apto_origen',$this->geo_num_casa_apto_origen,true);
        $criteria->compare('geo_estado_destino_id',$this->geo_estado_destino_id);
        $criteria->compare('geo_municipio_destino_id',$this->geo_municipio_destino_id);
        $criteria->compare('geo_parroquia_destino_id',$this->geo_parroquia_destino_id);
        $criteria->compare('geo_ciudad_destino',$this->geo_ciudad_destino,true);
        $criteria->compare('geo_sector_destino',$this->geo_sector_destino,true);
        $criteria->compare('geo_calle_destino',$this->geo_calle_destino,true);
        $criteria->compare('geo_nombre_edificacion_destino',$this->geo_nombre_edificacion_destino,true);
        $criteria->compare('geo_num_casa_apto_destino',$this->geo_num_casa_apto_destino,true);
        $criteria->compare('geo_etapa_destino',$this->geo_etapa_destino,true);
        $criteria->compare('geo_piso_destino',$this->geo_piso_destino,true);
        $criteria->compare('beneficio_tipo_vivienda_id',$this->beneficio_tipo_vivienda_id);
        $criteria->compare('desarrollo_id',$this->desarrollo_id);
        $criteria->compare('tipo_beneficio_id',$this->tipo_beneficio_id);
        $criteria->compare('fecha_adjudicacion',$this->fecha_adjudicacion,true);
        $criteria->compare('beneficio_fuente',$this->beneficio_fuente,true);
        $criteria->compare('organizacion',$this->organizacion,true);
        $criteria->compare('monto_beneficio',$this->monto_beneficio,true);
        $criteria->compare('id_usuario',$this->id_usuario);
        $criteria->compare('fecha_modificacion',$this->fecha_modificacion,true);
        $criteria->compare('carga_masiva',$this->carga_masiva);
        $criteria->compare('usu_aud_id',$this->usu_aud_id,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}