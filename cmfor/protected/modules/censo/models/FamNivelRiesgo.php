<?php

/**
 * This is the model class for table "fam_nivel_riesgo".
 *
 * The followings are the available columns in table 'fam_nivel_riesgo':
 * @property integer $id_fam_nivel_riesgo
 * @property string $descripcion
 * @property string $fecha
 *
 * The followings are the available model relations:
 * @property FamBeneficiado[] $famBeneficiados
 */
class FamNivelRiesgo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FamNivelRiesgo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fam_nivel_riesgo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, fecha', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_fam_nivel_riesgo, descripcion, fecha', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'famBeneficiados' => array(self::HAS_MANY, 'FamBeneficiado', 'fam_nivel_riesgo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_fam_nivel_riesgo' => 'Id Fam Nivel Riesgo',
			'descripcion' => 'Descripcion',
			'fecha' => 'Fecha',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_fam_nivel_riesgo',$this->id_fam_nivel_riesgo);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('fecha',$this->fecha,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}