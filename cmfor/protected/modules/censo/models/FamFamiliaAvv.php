<?php

/**
 * This is the model class for table "fam_familia_avv".
 *
 * The followings are the available columns in table 'fam_familia_avv':
 * @property integer $idfam_familia_avv
 * @property integer $idcomunidad_avv
 * @property string $fe_ingreso
 * @property boolean $st_familia_avv
 * @property string $usu_aud_id
 * @property string $url_imagen
 * @property integer $familia_id
 *
 * The followings are the available model relations:
 * @property ComunidadAvv $idcomunidadAvv
 * @property FamFamilia $familia
 */
class FamFamiliaAvv extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FamFamiliaAvv the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fam_familia_avv';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idcomunidad_avv, familia_id', 'numerical', 'integerOnly'=>true),
			array('fe_ingreso, st_familia_avv, usu_aud_id, url_imagen', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idfam_familia_avv, idcomunidad_avv, fe_ingreso, st_familia_avv, usu_aud_id, url_imagen, familia_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idcomunidadAvv' => array(self::BELONGS_TO, 'ComunidadAvv', 'idcomunidad_avv'),
			'familia' => array(self::BELONGS_TO, 'FamFamilia', 'familia_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idfam_familia_avv' => 'Idfam Familia Avv',
			'idcomunidad_avv' => 'Idcomunidad Avv',
			'fe_ingreso' => 'Fe Ingreso',
			'st_familia_avv' => 'St Familia Avv',
			'usu_aud_id' => 'Usu Aud',
			'url_imagen' => 'Url Imagen',
			'familia_id' => 'Familia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idfam_familia_avv',$this->idfam_familia_avv);
		$criteria->compare('idcomunidad_avv',$this->idcomunidad_avv);
		$criteria->compare('fe_ingreso',$this->fe_ingreso,true);
		$criteria->compare('st_familia_avv',$this->st_familia_avv);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('url_imagen',$this->url_imagen,true);
		$criteria->compare('familia_id',$this->familia_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}