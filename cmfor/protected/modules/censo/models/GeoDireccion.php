<?php

/**
 * This is the model class for table "geo_direccion".
 *
 * The followings are the available columns in table 'geo_direccion':
 * @property integer $direccion_id
 * @property string $direccion
 * @property integer $municipio_id
 * @property integer $parroquia_id
 * @property string $usu_aud_id
 * @property integer $estado_id
 * @property integer $nro_habitaciones
 *
 * The followings are the available model relations:
 * @property GeoUbicacion.geoEstado $estado
 * @property GeoUbicacion.geoMunicipio $municipio
 * @property GeoUbicacion.geoParroquia $parroquia
 * @property ComunidadAvv[] $comunidadAvvs
 * @property GeoTerreno[] $geoTerrenos
 * @property FamCiudadDestino[] $famCiudadDestinos
 * @property FamCiudadDestino2[] $famCiudadDestino2s
 */
class GeoDireccion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GeoDireccion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'geo_direccion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('municipio_id, parroquia_id, estado_id, nro_habitaciones', 'numerical', 'integerOnly'=>true),
			array('direccion, usu_aud_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('direccion_id, direccion, municipio_id, parroquia_id, usu_aud_id, estado_id, nro_habitaciones', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'estado' => array(self::BELONGS_TO, 'GeoEstado', 'estado_id'),
			'municipio' => array(self::BELONGS_TO, 'GeoUbicacion.geoMunicipio', 'municipio_id'),
			'parroquia' => array(self::BELONGS_TO, 'GeoUbicacion.geoParroquia', 'parroquia_id'),
			'comunidadAvvs' => array(self::HAS_MANY, 'ComunidadAvv', 'direccion_id'),
			'geoTerrenos' => array(self::HAS_MANY, 'GeoTerreno', 'direccion_id'),
			'famCiudadDestinos' => array(self::HAS_MANY, 'FamCiudadDestino', 'direccion_id'),
			'famCiudadDestino2s' => array(self::HAS_MANY, 'FamCiudadDestino2', 'direccion_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'direccion_id' => 'Direccion',
			'direccion' => 'Direccion',
			'municipio_id' => 'Municipio',
			'parroquia_id' => 'Parroquia',
			'usu_aud_id' => 'Usu Aud',
			'estado_id' => 'Estado',
			'nro_habitaciones' => 'Nro Habitaciones',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('direccion_id',$this->direccion_id);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('municipio_id',$this->municipio_id);
		$criteria->compare('parroquia_id',$this->parroquia_id);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('estado_id',$this->estado_id);
		$criteria->compare('nro_habitaciones',$this->nro_habitaciones);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}