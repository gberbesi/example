<?php

/**
 * This is the model class for table "fam_edo_civil".
 *
 * The followings are the available columns in table 'fam_edo_civil':
 * @property integer $edo_civil_id
 * @property string $nombre
 * @property boolean $st_edo_civil
 * @property string $usu_aud_id
 *
 * The followings are the available model relations:
 * @property FamBeneficiado[] $famBeneficiados
 */
class FamEdoCivil extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FamEdoCivil the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fam_edo_civil';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre', 'required'),
			array('st_edo_civil, usu_aud_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('edo_civil_id, nombre, st_edo_civil, usu_aud_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'famBeneficiados' => array(self::HAS_MANY, 'FamBeneficiado', 'edo_civil_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'edo_civil_id' => 'Edo Civil',
			'nombre' => 'Nombre',
			'st_edo_civil' => 'St Edo Civil',
			'usu_aud_id' => 'Usu Aud',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('edo_civil_id',$this->edo_civil_id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('st_edo_civil',$this->st_edo_civil);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}