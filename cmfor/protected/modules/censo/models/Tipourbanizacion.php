 <?php

/**
 * This is the model class for table "tipourbanizacion".
 *
 * The followings are the available columns in table 'tipourbanizacion':
 * @property string $nombre_urba
 * @property integer $id_urba
 */
class Tipourbanizacion extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Tipourbanizacion the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tipourbanizacion';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nombre_urba', 'required'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('nombre_urba, id_urba', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'nombre_urba' => 'Nombre Urba',
            'id_urba' => 'Id Urba',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('nombre_urba',$this->nombre_urba,true);
        $criteria->compare('id_urba',$this->id_urba);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
} 