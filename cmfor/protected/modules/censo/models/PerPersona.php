<?php

/**
 * This is the model class for table "per_persona".
 *
 * The followings are the available columns in table 'per_persona':
 * @property integer $persona_id
 * @property string $nacionalidad
 * @property integer $cedula
 * @property string $nombre1
 * @property string $nombre2
 * @property string $apellido1
 * @property string $apellido2
 * @property string $fe_nac
 * @property string $sexo
 * @property string $e_mail
 * @property string $usu_aud_id
 * @property string $pasaporte
 * @property string $id_pais
 * @property integer $tiempo_pais
 * @property boolean $inhabilitado
 * @property boolean $cosolicitante
 *
 * The followings are the available model relations:
 * @property FamBeneficiado[] $famBeneficiados
 * @property RepNotificacion[] $repNotificacions
 * @property PersonaLegal[] $personaLegals
 * @property PerTelefono[] $perTelefonos
 * @property RepresentanteAvv[] $representanteAvvs
 * @property UsuUsuario[] $usuUsuarios
 * @property UrbUrbanismo[] $urbUrbanismos
 * @property Archivo[] $archivos
 * @property Pais $idPais
 * @property DocAbogado[] $docAbogados
 * @property BitacoraDesactivacionBeneficio[] $bitacoraDesactivacionBeneficios
 */
class PerPersona extends CActiveRecord
{
	public $mensaje="";
	public $flag;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PerPersona the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'per_persona';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nacionalidad, nombre1, nombre2, apellido1, apellido2, fe_nac, cedula, e_mail, codigo_carnetdlp', 'filter', 'filter'=>'trim'),
			array('codigo_carnetdlp', 'filter', 'filter'=>array($this,'formatoCDLP')),
			array('nacionalidad, nombre1, apellido1, fe_nac, cedula, e_mail,codigo_carnetdlp', 'required'),
			array('nombre1, apellido1', 'match', 'pattern'=>'/^([a-zA-ZÁÉÍÓÚáéíóúñÑ\s])+$/', 'message' => 'Este campo puede contener solamente letras'),
			array('nombre1, apellido1, nombre2, apellido2','filter','filter'=>array($this,'letraCapital')),
			array('cedula, tiempo_pais', 'numerical', 'integerOnly'=>true, 'message'=>'Ingrese Solo Numeros'),
			array('nacionalidad, sexo', 'length', 'max'=>1),
			array('e_mail', 'email'),
			array('e_mail', 'filter', 'filter'=>'strtolower'),
			//array('e_mail','email_de_usuario'),
			array('e_mail','val_correo_hotmail'),
			//array('cedula','es_usuario'),

			

			array('fe_nac', 'date', 'format'=>'dd/MM/yyyy','message' => 'El formato de Fecha es invalido. <br/>Ej. valido: (dd/mm/aaaa)'),
			
			array('codigo_carnetdlp','numerical', 'integerOnly'=>true,'message'=>'El Código del carnet debe ser numérico'),
			
			array('cedula','length', 'max'=>9, 'on'=>'regitroCenso'),
			array('cedula','val_cedula', 'on'=>'regitroCenso'),
			array('cedula','val_cedula_adm', 'on'=>'regitroCenso'),
			
			array('nombre2, apellido2, fe_nac, e_mail, usu_aud_id, pasaporte, id_pais, inhabilitado, cosolicitante, flag', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('persona_id, nacionalidad, cedula, nombre1, nombre2, apellido1, apellido2, fe_nac, sexo, e_mail, usu_aud_id, pasaporte, id_pais, tiempo_pais, inhabilitado, cosolicitante', 'safe', 'on'=>'search'),
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'famBeneficiados' => array(self::HAS_MANY, 'FamBeneficiado', 'persona_id'),
			'repNotificacions' => array(self::HAS_MANY, 'RepNotificacion', 'persona_id'),
			'personaLegals' => array(self::HAS_MANY, 'PersonaLegal', 'persona_id'),
			'perTelefonos' => array(self::HAS_MANY, 'PerTelefono', 'persona_id'),
			'representanteAvvs' => array(self::HAS_MANY, 'RepresentanteAvv', 'idpersona'),
			'usuUsuarios' => array(self::HAS_MANY, 'UsuUsuario', 'persona_id'),
			'urbUrbanismos' => array(self::HAS_MANY, 'UrbUrbanismo', 'persona_id'),
			'archivos' => array(self::MANY_MANY, 'Archivo', 'persona_archivo(persona_id, archivo_id)'),
			'idPais' => array(self::BELONGS_TO, 'Pais', 'id_pais'),
			'docAbogados' => array(self::HAS_MANY, 'DocAbogado', 'persona_id'),
			'bitacoraDesactivacionBeneficios' => array(self::HAS_MANY, 'BitacoraDesactivacionBeneficio', 'persona_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'persona_id' => 'Persona',
			'nacionalidad' => 'Nacionalidad',
			'cedula' => 'Cedula',
			'nombre1' => 'Nombre1',
			'nombre2' => 'Nombre2',
			'apellido1' => 'Apellido1',
			'apellido2' => 'Apellido2',
			'fe_nac' => 'Fe Nac',
			'sexo' => 'Sexo',
			'e_mail' => 'E Mail',
			'usu_aud_id' => 'Usu Aud',
			'pasaporte' => 'Pasaporte',
			'id_pais' => 'Id Pais',
			'tiempo_pais' => 'Tiempo Pais',
			'inhabilitado' => 'Inhabilitado',
			'cosolicitante' => 'Cosolicitante',
			'codigo_carnetdlp'=>'Código de carnet',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('persona_id',$this->persona_id);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('cedula',$this->cedula);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('fe_nac',$this->fe_nac,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('e_mail',$this->e_mail,true);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('pasaporte',$this->pasaporte,true);
		$criteria->compare('id_pais',$this->id_pais,true);
		$criteria->compare('tiempo_pais',$this->tiempo_pais);
		$criteria->compare('inhabilitado',$this->inhabilitado);
		$criteria->compare('cosolicitante',$this->cosolicitante);
		$criteria->compare('codigo_carnetdlp',$this->codigo_carnetdlp);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	} 
	
	public function letraCapital($value) {
		return mb_convert_case($value, MB_CASE_TITLE, "UTF-8");
	}
	
	public function es_usuario($attribute) {
		
		if(!$this->hasErrors($attribute)) {
			
			$cedula=(int)$this->$attribute;
			
			$usuario=false; 
			$persona=PerPersona::model()->find('cedula=:cedula',array(':cedula'=>$cedula));
			if($persona) {
				$usuario=UsuUsuario::model()->find('persona_id=:persona_id AND st_usuario=true',array(':persona_id'=>$persona->persona_id));
			}
			
			if($usuario){
				$this->addError('cedula','Cédula ya se encuentra registrada');
			}
		
		}
	
	}
	
	public function email_de_usuario($attribute,$params){
	
		if(!$this->hasErrors($attribute)) {
			
			$persona=PerPersona::model()->find('e_mail=:e_mail',array(':e_mail'=>$this->$attribute));
			if($persona) {
				if ($persona->cedula!=$this->cedula)
					$this->addError('e_mail','Correo Electronico ya se encuentra registrado');	
			}
		
		}
	
	}

	public function val_correo_hotmail($attributes) {
	   	
		if(!$this->hasErrors($attribute)) {

		   $pos = strpos(strtolower($this->e_mail), 'hotmail');
		   $pos2 = strpos(strtolower($this->e_mail), 'outlook');

		   if($pos==true || $pos2==true)
		   {
				$this->addError('e_mail', 'Por favor ingrese un correo que no pertenezca a Outlook o Hotmail');
		   }
	   
		}
       	
	}
	
	public function val_cedula($attributes) {
	   	
		if(!$this->hasErrors($attribute)) {

			$persona = PerPersona::model()->find('cedula=:cedula', array(':cedula'=>$this->cedula));
		   
			if($persona) {
			   
			   $usuario=UsuUsuario::model()->find('persona_id=:persona_id AND COALESCE(actualizadocdlp,false) = true and COALESCE(email_verificado,false)= true',array(':persona_id'=>$persona->persona_id));
			   
			   if($usuario) {
				   $this->addError('cedula', 'Cédula ya se encuentra registrada');
			   }
			   
		    }

		}
       	
	}
	
	public function val_cedula_adm($attributes) {
	   	
		if(!$this->hasErrors($attribute)) {

			$persona = PerPersona::model()->find('cedula=:cedula', array(':cedula'=>$this->cedula));
		   
			if($persona) {
			   
			   $usuario=UsuUsuario::model()->find('persona_id=:persona_id',array(':persona_id'=>$persona->persona_id));
			   
			   if($usuario) {
				   $modelAuthAssignment=AuthAssignment::model()->find('itemname != \'ActualizacionCenso\' and userid=:id',array(':id'=>(string)$usuario->usuario_id));
				   
				    if($modelAuthAssignment) {
						 $this->addError('cedula', 'Cédula con perfil administrativo<br/>Contacte con el Administrador.');
					}
			   }
			   
		    }

		}
       	
	}
	
	public function formatoCDLP($code){
		if($code!='') {
			$code = str_pad(ltrim($code, "0"), 10, "0",STR_PAD_LEFT);
		}
		return $code;
	}

	public function claveAleatoria(){
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$cad = "";
		for($i=0;$i<8;$i++) {
		$cad .= substr($str,rand(0,62),1);
		}
		return $cad;
	}	
	
	
	public function correoActualizacion($aquien,$aquiennombre,$mensaje)
	{
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		$mail = new JPhpMailer;
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['mailHost'];/*'172.16.0.20';*/
		$mail->Port = Yii::app()->params['mailPortSsl'];
		$mail->CharSet = "utf-8";
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';
		$mail->Username = Yii::app()->params['mailUsername'];/*'xmolina';*/
		$mail->Password = Yii::app()->params['mailUserPassw'];
		$mail->SetFrom(Yii::app()->params['mailRemitente'], Yii::app()->params['nombreRemitenteActualizacion']);
		
		$mail->Subject = Yii::app()->params['mailAsuntoActualizacion'];
		//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($mensaje);
		$mail->AddAddress(/*'xmolina@minvih.gob.ve'*/$aquien, $aquiennombre);
		
		if(!$mail->Send()) {
		   throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente intentelo mas tarde.',500);
		}
		else {
		   //echo 'Mail enviado!';
		}

	}
	public function claveRecuperacion($aquien,$aquiennombre,$mensaje)
	{
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		$mail = new JPhpMailer;
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['mailHost'];/*'172.16.0.20';*/
		$mail->Port = Yii::app()->params['mailPortSsl'];
		$mail->CharSet = "utf-8";
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';
		$mail->Username = Yii::app()->params['mailUsername'];/*'xmolina';*/
		$mail->Password = Yii::app()->params['mailUserPassw'];
		$mail->SetFrom(Yii::app()->params['mailRemitente'], Yii::app()->params['nombreRemitenteActualizacion']);
		
		$mail->Subject = Yii::app()->params['mailAsuntoActualizacion'];
		//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($mensaje);
		$mail->AddAddress(/*'xmolina@minvih.gob.ve'*/$aquien, $aquiennombre);
		
		if(!$mail->Send()) {
		   throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente intentelo mas tarde.',500);
		}
		else {
		   //echo 'Mail enviado!';
		}

	}	
}