<?php

/**
 * This is the model class for table "usu_usuario".
 *
 * The followings are the available columns in table 'usu_usuario':
 * @property integer $usuario_id
 * @property string $nb_usuario
 * @property string $contrasena
 * @property string $persona_id
 * @property boolean $st_usuario
 * @property string $direccion_id
 * @property string $ente_id
 * @property string $fe_expiracion
 * @property string $usu_aud_id
 * @property string $refugio_id
 * @property boolean $st_usuario_plan
 * @property string $fech_expiracion
 * @property string $departamento_id
 * @property boolean $responsable
 * @property string $contrasena_gmvvj
 * @property boolean $inmobiliaria
 *
 * The followings are the available model relations:
 * @property DocDocumentoGenerado[] $docDocumentoGenerados
 * @property AudAuditoria[] $audAuditorias
 * @property PerPersona $persona
 * @property RefRefugio $refugio
 * @property Ente $ente
 * @property UrbTorre[] $urbTorres
 * @property UrbManzana[] $urbManzanas
 * @property BitacoraDesactivacionBeneficio[] $bitacoraDesactivacionBeneficios
 * @property AnalistaPreasignacion[] $analistaPreasignacions
 */
class UsuUsuario extends CActiveRecord
{
	
	public $nueva;
	public $repetirnueva;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UsuUsuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usu_usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nb_usuario, contrasena', 'required'),
			
			array('repetirnueva,contrasena,nueva', 'required','on'=>array('cambio'),'message' => 'Debe escribir una contraseña valida'),
			array('nueva', 'compare', 'compareAttribute'=>'repetirnueva','message' => 'Las contraseñas nuevas no son iguales', 'on'=>array('cambio')),
			array('repetirnueva', 'compare', 'compareAttribute'=>'nueva','message' => 'Las contraseñas nuevas no son iguales', 'on'=>array('cambio',)),
			array('nueva', 'length','min'=>4,'on'=>array('cambio')),
			
			array('persona_id, st_usuario, direccion_id, ente_id, fe_expiracion, usu_aud_id, refugio_id, st_usuario_plan, fech_expiracion, departamento_id, responsable, contrasena_gmvvj, inmobiliaria', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('usuario_id, nb_usuario, contrasena, persona_id, st_usuario, direccion_id, ente_id, fe_expiracion, usu_aud_id, refugio_id, st_usuario_plan, fech_expiracion, departamento_id, responsable, contrasena_gmvvj, inmobiliaria', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'docDocumentoGenerados' => array(self::HAS_MANY, 'DocDocumentoGenerado', 'usuario_id'),
			'audAuditorias' => array(self::HAS_MANY, 'AudAuditoria', 'usuario_id'),
			'persona' => array(self::BELONGS_TO, 'PerPersona', 'persona_id'),
			'refugio' => array(self::BELONGS_TO, 'RefRefugio', 'refugio_id'),
			'ente' => array(self::BELONGS_TO, 'Ente', 'ente_id'),
			'urbTorres' => array(self::MANY_MANY, 'UrbTorre', 'responsable_torre(usuario_id, torre_id)'),
			'urbManzanas' => array(self::MANY_MANY, 'UrbManzana', 'responsable_manzana(usuario_id, manzana_id)'),
			'bitacoraDesactivacionBeneficios' => array(self::HAS_MANY, 'BitacoraDesactivacionBeneficio', 'usuario_id'),
			'analistaPreasignacions' => array(self::HAS_MANY, 'AnalistaPreasignacion', 'id_analista'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'usuario_id' => 'Usuario',
			'nb_usuario' => 'Nb Usuario',
			'contrasena' => 'Contrasena',
			'persona_id' => 'Persona',
			'st_usuario' => 'St Usuario',
			'direccion_id' => 'Direccion',
			'ente_id' => 'Ente',
			'fe_expiracion' => 'Fe Expiracion',
			'usu_aud_id' => 'Usu Aud',
			'refugio_id' => 'Refugio',
			'st_usuario_plan' => 'St Usuario Plan',
			'fech_expiracion' => 'Fech Expiracion',
			'departamento_id' => 'Departamento',
			'responsable' => 'Responsable',
			'contrasena_gmvvj' => 'Contrasena Gmvvj',
			'inmobiliaria' => 'Inmobiliaria',
			'nueva' => 'Nueva Contraseña',
			'repetirnueva' => 'Repetir Contraseña Nueva',
			'contrasena' => 'Contrase&ntilde;a',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('usuario_id',$this->usuario_id);
		$criteria->compare('nb_usuario',$this->nb_usuario,true);
		$criteria->compare('contrasena',$this->contrasena,true);
		$criteria->compare('persona_id',$this->persona_id,true);
		$criteria->compare('st_usuario',$this->st_usuario);
		$criteria->compare('direccion_id',$this->direccion_id,true);
		$criteria->compare('ente_id',$this->ente_id,true);
		$criteria->compare('fe_expiracion',$this->fe_expiracion,true);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('refugio_id',$this->refugio_id,true);
		$criteria->compare('st_usuario_plan',$this->st_usuario_plan);
		$criteria->compare('fech_expiracion',$this->fech_expiracion,true);
		$criteria->compare('departamento_id',$this->departamento_id,true);
		$criteria->compare('responsable',$this->responsable);
		$criteria->compare('contrasena_gmvvj',$this->contrasena_gmvvj,true);
		$criteria->compare('inmobiliaria',$this->inmobiliaria);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}