<?php

/**
 * This is the model class for table "gmvv.gr_gmvv_t".
 *
 * The followings are the available columns in table 'gmvv.gr_gmvv_t':
 * @property string $serial
 * @property string $familia_id
 * @property string $id
 * @property string $beneficiado_sigevih_id
 * @property string $fe_registro
 * @property string $nacionalidad
 * @property string $ci
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property string $f_nacimiento
 * @property string $sexo
 * @property string $edo_civil_id
 * @property string $nb_ed_civil
 * @property string $e_mail
 * @property integer $parentesco_id
 * @property string $parentesco_rol
 * @property integer $grado_instruccion_id
 * @property string $grado_instruccion
 * @property integer $enfermedad_id
 * @property string $enfermedad
 * @property integer $ocupacion_id
 * @property string $ocupacion
 * @property integer $trabajo_id
 * @property string $institucion
 * @property boolean $insc_faov
 * @property string $ingreso_mensual
 * @property string $telf_movil
 * @property string $telf_otro
 * @property string $mision_id
 * @property string $calidad_vivienda_id
 * @property boolean $estudia
 * @property boolean $estado_embarazo
 * @property string $id_clasificacion
 * @property boolean $trabaja
 * @property integer $geo_estado_id
 * @property string $estado
 * @property integer $geo_municipio_id
 * @property string $municipio
 * @property integer $geo_parroquia_id
 * @property string $parroquia
 * @property string $centro_poblado_id
 * @property string $centro_poblado
 * @property string $dir_urbanizacion_barrio
 * @property string $dir_av_calle_esq
 * @property string $dir_edf_casa_num
 * @property integer $titularidad_id
 * @property string $titularidad
 * @property string $baremo
 * @property integer $destino1_id
 * @property string $destino1
 * @property integer $destino2_id
 * @property string $destino2
 * @property string $tipo_solicitud_id
 * @property string $nb_solicitud
 * @property integer $fam_clasificacion_id
 * @property string $nb_clasificacion
 * @property boolean $st_censado
 * @property boolean $migrado
 * @property string $usu_aud_id
 * @property string $id_pais
 * @property integer $tiempo_pais
 * @property integer $n_grupo
 * @property integer $n_discapacitado
 * @property string $id_gr_gmvv_t
 */
class GrGmvvT extends CActiveRecord
{
	public $mensaje="";
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GrGmvvT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gmvv.gr_gmvv_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//rol jefe familia
			array('discapacitado, fam_nivel_riesgo_id, ci ,geo_estado_id, geo_municipio_id, geo_parroquia_id, primer_nombre, primer_apellido, edo_civil_id, sexo, f_nacimiento, telf_movil, grado_instruccion_id, ocupacion_id, ingreso_mensual, serial_cdlp, codigo_cdlp, dir_urbanizacion_barrio,dir_av_calle_esq,dir_edf_casa_num,nb_solicitudactual,estado_uno_id,municipio_uno_id,parroquia_uno_id,e_mail,tipo_urba, tipo_ave, tipo_edif','required','on'=>'jefefamilia'),
			array('ci','length','min'=>5,'max'=>8,'on'=>'jefefamilia'),
			array('primer_nombre,segundo_nombre,primer_apellido,segundo_apellido', 'match', 'pattern'=>'/^([a-zA-ZÁÉÍÓÚáéíóúñÑ\s])+$/', 'message' => 'Este campo puede contener solamente letras','on'=>'jefefamilia'),
			array('e_mail', 'email','on'=>'jefefamilia'),
			array('ingreso_mensual,honorarios_profesionales,bono_alimentacion,otros_ingresos', 'match',  //PARA VALIDAR CAMPOS CON FORMATO MONEDA
						'pattern' => '/^[0-9]\d{0,2}(\.[0-9]\d{2,2})*(\,\d{1,2})?$/',
						'message' => 'El valor del campo debe tener un formato 100.000,00','on'=>'jefefamilia'),
			array('ingreso_mensual,honorarios_profesionales,bono_alimentacion,otros_ingresos', 'montoMaximo', 'max'=>18,'on'=>'jefefamilia'),
			array('ci, parentesco_id, grado_instruccion_id, enfermedad_id, ocupacion_id, trabajo_id, geo_estado_id, geo_municipio_id, geo_parroquia_id, titularidad_id, destino1_id, destino2_id, fam_clasificacion_id, tiempo_pais, n_grupo, n_discapacitado', 'numerical', 'integerOnly'=>true,'on'=>'jefefamilia'),
			array('serial, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido', 'length', 'max'=>255,'on'=>'jefefamilia'),
			array('sexo', 'length', 'max'=>1,'on'=>'jefefamilia'),
			array('f_nacimiento','date','format'=>'dd/MM/yyyy','message'=>'El formato de Fecha es inválido (dd/mm/aaaa)','on'=>'jefefamilia'),
			
			//rol carga familiar
			array('discapacitado, parentesco_id, primer_nombre, primer_apellido, edo_civil_id, sexo, f_nacimiento, grado_instruccion_id, ocupacion_id','required','on'=>'cargafamiliar'),
			array('ci','length','min'=>5,'max'=>8,'on'=>'cargafamiliar'),
			array('f_nacimiento','date','format'=>'dd/MM/yyyy','message'=>'El formato de Fecha es inválido (dd/mm/aaaa)','on'=>'cargafamiliar'),
			array('primer_nombre,segundo_nombre,primer_apellido,segundo_apellido', 'match', 'pattern'=>'/^([a-zA-ZÁÉÍÓÚáéíóúñÑ\s])+$/', 'message' => 'Este campo puede contener solamente letras','on'=>'cargafamiliar'),
			array('e_mail', 'email','on'=>'cargafamiliar'),
			array('ingreso_mensual,honorarios_profesionales,bono_alimentacion,otros_ingresos', 'match',  //PARA VALIDAR CAMPOS CON FORMATO MONEDA
						'pattern' => '/^[0-9]\d{0,2}(\.[0-9]\d{2,2})*(\,\d{1,2})?$/',
						'message' => 'El valor del campo debe tener un formato 100.000,00','on'=>'cargafamiliar'),
			array('ingreso_mensual,honorarios_profesionales,bono_alimentacion,otros_ingresos', 'montoMaximo', 'max'=>18,'on'=>'cargafamiliar'),
			array('ci, parentesco_id, grado_instruccion_id, enfermedad_id, ocupacion_id, trabajo_id, geo_estado_id, geo_municipio_id, geo_parroquia_id, titularidad_id, destino1_id, destino2_id, fam_clasificacion_id, tiempo_pais, n_grupo, n_discapacitado', 'numerical', 'integerOnly'=>true,'on'=>'cargafamiliar'),
			array('serial, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido', 'length', 'max'=>255,'on'=>'cargafamiliar'),
			array('sexo', 'length', 'max'=>1,'on'=>'cargafamiliar'),
			//array('ci', 'valida_cedula','on'=>'cargafamiliar'),
			
			 
			array('serial_cdlp, codigo_cdlp, mision, estado_trabaja_id, municipio_trabaja_id, parroquia_trabaja_id, estado_estudia_id, municipio_estudia_id, parroquia_estudia_id, estado_terreno_id, municipio_terreno_id, parroquia_terreno_id, estado_uno_id, municipio_uno_id, parroquia_uno_id, estado_dos_id, municipio_dos_id, parroquia_dos_id, tiempo_admpublica_id, fam_sector_social_id, propietario_terreno, nro_conapdis, fam_nivel_riesgo_id, familia_id, id, beneficiado_sigevih_id, fe_registro, nacionalidad, ci, f_nacimiento, edo_civil_id, nb_ed_civil, e_mail, parentesco_rol, grado_instruccion, enfermedad, ocupacion, institucion, insc_faov, ingreso_mensual, telf_movil, telf_otro, mision_id, calidad_vivienda_id, estudia, estado_embarazo, id_clasificacion, trabaja, estado, municipio, parroquia, centro_poblado_id, centro_poblado, dir_urbanizacion_barrio, dir_av_calle_esq, dir_edf_casa_num, titularidad, baremo, destino1, destino2, tipo_solicitud_id, nb_clasificacion, st_censado, migrado, usu_aud_id, id_pais,tipo_urba,tipo_edif,tipo_ave, nb_solicitudactual,pertenece_avv,nombre_avv,somos_venezuela,mision_id2,mision2', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('serial, familia_id, id, beneficiado_sigevih_id, fe_registro, nacionalidad, ci, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, f_nacimiento, sexo, edo_civil_id, nb_ed_civil, e_mail, parentesco_id, parentesco_rol, grado_instruccion_id, grado_instruccion, enfermedad_id, enfermedad, ocupacion_id, ocupacion, trabajo_id, institucion, insc_faov, ingreso_mensual, telf_movil, telf_otro, mision_id, calidad_vivienda_id, estudia, estado_embarazo, id_clasificacion, trabaja, geo_estado_id, estado, geo_municipio_id, municipio, geo_parroquia_id, parroquia, centro_poblado_id, centro_poblado, dir_urbanizacion_barrio, dir_av_calle_esq, dir_edf_casa_num, titularidad_id, titularidad, baremo, destino1_id, destino1, destino2_id, destino2, tipo_solicitud_id, nb_solicitud, fam_clasificacion_id, nb_clasificacion, st_censado, migrado, usu_aud_id, id_pais, tiempo_pais, n_grupo, n_discapacitado, id_gr_gmvv_t ,tipo_urba,tipo_edif,tipo_ave,nb_solicitudactual,pertenece_avv,nombre_avv,somos_venezuela,mision_id2,mision2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parentesco' => array(self::BELONGS_TO, 'FamParentesco', 'parentesco_id'),
			
			'parentescoSigevih' => array(self::BELONGS_TO, 'FamParentesco', 'parentesco_id'),
			'gradoInstruccionSigevih' => array(self::BELONGS_TO, 'FamGradoInstruccion', 'grado_instruccion_id'),
			'enfermedadSigevih' => array(self::BELONGS_TO, 'FamEnfermedad', 'enfermedad_id'),
			'edoCivilSigevih' => array(self::BELONGS_TO, 'FamEdoCivil', 'edo_civil_id'),
			'institucionSigevih' => array(self::BELONGS_TO, 'PerTrabajo', 'trabajo_id'),
			'ocupacionSigevih' => array(self::BELONGS_TO, 'FamOcupacion', 'ocupacion_id'),
			'parentesco' => array(self::BELONGS_TO, 'FamParentesco', 'parentesco_id'),
			'estadoSigevih' => array(self::BELONGS_TO, 'GeoEstado', 'geo_estado_id'),
			'municipioSigevih' => array(self::BELONGS_TO, 'GeoMunicipio', 'geo_municipio_id'),
			'parroquiaSigevih' => array(self::BELONGS_TO, 'GeoParroquia', 'geo_parroquia_id'),
			'calidadViviendaSigevih' => array(self::BELONGS_TO, 'FamCalidadViviendaOrigen', 'calidad_vivienda_id'),
			'condicionViviendaSigevih' => array(self::BELONGS_TO, 'FamCondicionViviendaOrigen', 'titularidad_id'),
			'ciudadDestino1' => array(self::BELONGS_TO, 'GeoCiudad', 'destino1_id'),
			'ciudadDestino2' => array(self::BELONGS_TO, 'GeoCiudad', 'destino2_id'),

			'tipoSolicitud' => array(self::BELONGS_TO, 'FamTipoSolicitud', 'tipo_solicitud_id'),

			'cedula' => array(self::BELONGS_TO, 'Censado', 'ci'), 
			'censado_id' => array(self::BELONGS_TO, 'Censado', 'id_gr_gmvv_t'),  
			'beneficiadoSigevih' => array(self::BELONGS_TO, 'FamBeneficiado', 'beneficiado_sigevih_id'), 
			'cedula' => array(self::BELONGS_TO, 'BeneficiosSivihjefe', 'ci'),
			//nueva para traer datos de solicitud
			'estadoSolicitud' => array(self::BELONGS_TO, 'GeoEstado', 'estado_uno_id'),
			'municipioSolicitud' => array(self::BELONGS_TO, 'GeoMunicipio', 'municipio_uno_id'),
			'parroquiaSolicitud' => array(self::BELONGS_TO, 'GeoParroquia', 'parroquia_uno_id'),

			'tipoSolicitudGmvv' => array(self::BELONGS_TO, 'TipoSolicitudGmvv', 'nb_solicitudactual'),
			

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'serial' => 'Serial',
			'familia_id' => 'Familia',
			'id' => 'ID',
			'beneficiado_sigevih_id' => 'Beneficiado Sigevih',
			'fe_registro' => 'Fe Registro',
			'nacionalidad' => 'Nacionalidad',
			'ci' => 'Cédula',
			'primer_nombre' => 'Primer Nombre',
			'segundo_nombre' => 'Segundo Nombre',
			'primer_apellido' => 'Primer Apellido',
			'segundo_apellido' => 'Segundo Apellido',
			'f_nacimiento' => 'Fecha de Nacimiento',
			'sexo' => 'Género',
			'edo_civil_id' => 'Edo. Civil',
			'nb_ed_civil' => 'Nb Ed Civil',
			'e_mail' => 'Dirección de Correo Electrónico',
			'parentesco_id' => 'Parentesco',
			'parentesco_rol' => 'Parentesco Rol',
			'grado_instruccion_id' => 'Grado de Instrucción',
			'grado_instruccion' => 'Grado de Instrucción',
			'enfermedad_id' => 'Enfermedad',
			'enfermedad' => 'Enfermedad',
			'ocupacion_id' => 'Ocupación',
			'ocupacion' => 'Ocupacion',
			'trabajo_id' => 'Condición de Empleo',
			'institucion' => 'Institucion',
			'insc_faov' => '¿Cotiza en el FAOV/FAVV?',
			'ingreso_mensual' => 'Ingreso Mensual',
			'telf_movil' => 'N° de Celular',
			'telf_otro' => 'Otro N° de Telefono',
			'mision_id' => 'Especifique',
			'calidad_vivienda_id' => 'Calidad de la Vivienda',
			'estudia' => '¿Estudia?',
			'estado_embarazo' => '¿Estado de Embarazo?',
			'id_clasificacion' => 'Clasificación de Ingreso',
			'trabaja' => '¿Actualmente Labora?',
			'geo_estado_id' => 'Estado',
			'estado' => 'Estado',
			'geo_municipio_id' => 'Municipio',
			'municipio' => 'Municipio',
			'geo_parroquia_id' => 'Parroquia',
			'parroquia' => 'Parroquia',
			'centro_poblado_id' => 'Centro Poblado',
			'centro_poblado' => 'Centro Poblado',
			'dir_urbanizacion_barrio' => '<br>Urbanización/Barrio	',
			'dir_av_calle_esq' => '   <br>Avenida/Calle/Esquina',
			'dir_edf_casa_num' => '<br>Edf/Casa/Num',
			'titularidad_id' => 'Condición habitacional',
			'titularidad' => 'Titularidad',
			'baremo' => 'Baremo',
			'destino1_id' => 'Ciudad',
			'destino1' => 'Destino1',
			'destino2_id' => 'Ciudad',
			'destino2' => 'Destino2',
			'tipo_solicitud_id' => 'Tipo de Solicitud (Anterior)',
			'nb_solicitud' => 'Tipo de Solicitud (Anterior)',
			'fam_clasificacion_id' => 'Fam Clasificacion',
			'nb_clasificacion' => 'Nb Clasificacion',
			'st_censado' => 'St Censado',
			'migrado' => 'Migrado',
			'usu_aud_id' => 'Usu Aud',
			'id_pais' => 'País de Origen',
			'tiempo_pais' => 'Año de llegada al País',
			'n_grupo' => 'N Grupo',
			'n_discapacitado' => 'N Discapacitado',
			'id_gr_gmvv_t' => 'Id Gr Gmvv T',
			'mision' => '¿Presta servicios en alguna misión?',
			'fam_nivel_riesgo_id' => 'Nivel de Riesgo Vital',
			'discapacitado' => '¿Posee Discapacidad?',
			'nro_conapdis' => 'Número CONAPDIS',
			'propietario_terreno' => '¿Es usted propietario de algún terreno?',
			'fam_sector_social_id' => 'Sector al cual pertenece',
			'tiempo_admpublica_id' => 'Años en la Administración Pública',
			
			'estado_uno_id' => 'Estado',
			'municipio_uno_id' => 'Municipio',
			'parroquia_uno_id' => 'Parroquia',
			'estado_dos_id' => 'Estado',
			'municipio_dos_id' => 'Municipio',
			'parroquia_dos_id' => 'Parroquia',
			
			'estado_terreno_id' => 'Estado del terreno',
			'municipio_terreno_id' => 'Municipio del terreno',
			'parroquia_terreno_id' => 'Parroquia del terreno',
			
			'estado_estudia_id' => 'Estado donde usted estudia',
			'municipio_estudia_id' => 'Municipio donde usted estudia',
			'parroquia_estudia_id' => 'Parroquia doncde usted estudia',
			
			'estado_trabaja_id' => 'Estado donde usted trabaja',
			'municipio_trabaja_id' => 'Municipio donde usted trabaja',
			'parroquia_trabaja_id' => 'Parroquia donde usted trabaja',
			
			'honorarios_profesionales' => 'Honorarios profesionales',
			'bono_alimentacion' => 'Bono Alimentación',
			'otros_ingresos' => 'Otros Ingresos',
			
			'serial_cdlp' => 'Serial Carnet de la Patria',
			'codigo_cdlp' => 'Código Carnet de la Patria',
			'tipo_urba'=>'',
			'tipo_edif'=>'',
			'tipo_ave'=>'Avenida o Calle',
			'nb_solicitudactual'=>'Tipo de Solicitud (Actual)',
			'mision2'=>'¿Es o ha sido beneficiado por alguna misión? ',
			'mision_id2'=>'Especifique',
			'pertenece_avv'=>'¿Pertenece a una Asamblea de Viviendo Venezolano?',
			'nombre_avv'=>'Nombre de la AVV',


		
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('serial',$this->serial,true);
		$criteria->compare('familia_id',$this->familia_id,true);
		$criteria->compare('id',$this->id,true);
		$criteria->compare('beneficiado_sigevih_id',$this->beneficiado_sigevih_id,true);
		$criteria->compare('fe_registro',$this->fe_registro,true);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('ci',$this->ci,true);
		$criteria->compare('primer_nombre',$this->primer_nombre,true);
		$criteria->compare('segundo_nombre',$this->segundo_nombre,true);
		$criteria->compare('primer_apellido',$this->primer_apellido,true);
		$criteria->compare('segundo_apellido',$this->segundo_apellido,true);
		$criteria->compare('f_nacimiento',$this->f_nacimiento,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('edo_civil_id',$this->edo_civil_id,true);
		$criteria->compare('nb_ed_civil',$this->nb_ed_civil,true);
		$criteria->compare('e_mail',$this->e_mail,true);
		$criteria->compare('parentesco_id',$this->parentesco_id);
		$criteria->compare('parentesco_rol',$this->parentesco_rol,true);
		$criteria->compare('grado_instruccion_id',$this->grado_instruccion_id);
		$criteria->compare('grado_instruccion',$this->grado_instruccion,true);
		$criteria->compare('enfermedad_id',$this->enfermedad_id);
		$criteria->compare('enfermedad',$this->enfermedad,true);
		$criteria->compare('ocupacion_id',$this->ocupacion_id);
		$criteria->compare('ocupacion',$this->ocupacion,true);
		$criteria->compare('trabajo_id',$this->trabajo_id);
		$criteria->compare('institucion',$this->institucion,true);
		$criteria->compare('insc_faov',$this->insc_faov);
		$criteria->compare('ingreso_mensual',$this->ingreso_mensual,true);
		$criteria->compare('telf_movil',$this->telf_movil,true);
		$criteria->compare('telf_otro',$this->telf_otro,true);
		$criteria->compare('mision_id',$this->mision_id,true);
		$criteria->compare('calidad_vivienda_id',$this->calidad_vivienda_id,true);
		$criteria->compare('estudia',$this->estudia);
		$criteria->compare('estado_embarazo',$this->estado_embarazo);
		$criteria->compare('id_clasificacion',$this->id_clasificacion,true);
		$criteria->compare('trabaja',$this->trabaja);
		$criteria->compare('geo_estado_id',$this->geo_estado_id);
		$criteria->compare('estado',$this->estado,true);
		$criteria->compare('geo_municipio_id',$this->geo_municipio_id);
		$criteria->compare('municipio',$this->municipio,true);
		$criteria->compare('geo_parroquia_id',$this->geo_parroquia_id);
		$criteria->compare('parroquia',$this->parroquia,true);
		$criteria->compare('centro_poblado_id',$this->centro_poblado_id,true);
		$criteria->compare('centro_poblado',$this->centro_poblado,true);
		$criteria->compare('dir_urbanizacion_barrio',$this->dir_urbanizacion_barrio,true);
		$criteria->compare('dir_av_calle_esq',$this->dir_av_calle_esq,true);
		$criteria->compare('dir_edf_casa_num',$this->dir_edf_casa_num,true);
		$criteria->compare('titularidad_id',$this->titularidad_id);
		$criteria->compare('titularidad',$this->titularidad,true);
		$criteria->compare('baremo',$this->baremo,true);
		$criteria->compare('destino1_id',$this->destino1_id);
		$criteria->compare('destino1',$this->destino1,true);
		$criteria->compare('destino2_id',$this->destino2_id);
		$criteria->compare('destino2',$this->destino2,true);
		$criteria->compare('tipo_solicitud_id',$this->tipo_solicitud_id,true);
		$criteria->compare('nb_solicitud',$this->nb_solicitud,true);
		$criteria->compare('fam_clasificacion_id',$this->fam_clasificacion_id);
		$criteria->compare('nb_clasificacion',$this->nb_clasificacion,true);
		$criteria->compare('st_censado',$this->st_censado);
		$criteria->compare('migrado',$this->migrado);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('id_pais',$this->id_pais,true);
		$criteria->compare('tiempo_pais',$this->tiempo_pais);
		$criteria->compare('n_grupo',$this->n_grupo);
		$criteria->compare('n_discapacitado',$this->n_discapacitado);
		$criteria->compare('id_gr_gmvv_t',$this->id_gr_gmvv_t,true);
		
		$criteria->compare('tipo_urba',$this->tipo_urba);
		$criteria->compare('tipo_urba',$this->tipo_edif);
		$criteria->compare('tipo_urba',$this->tipo_ave);
		$criteria->compare('nb_solicitudactual',$this->nb_solicitudactual,true);

		$criteria->compare('pertenece_avv',$this->pertenece_avv);
		$criteria->compare('nombre_avv',$this->nombre_avv);
		$criteria->compare('somos_venezuela',$this->somos_venezuela);



		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function fecha() {		
		$tope = date("Y");
		$edad_max = 120;
		$fechas = array();
		
		for($a= $tope - $edad_max; $a<=$tope; $a++)
			$fechas[$a]=$a;
		
        return $fechas;
	}
	
	public function montoMaximo($attribute,$params) {

		if(!$this->hasErrors($attribute)) {
			
			$numero=str_replace('.', '', $this->$attribute);
			$numero=str_replace(',', '.', $numero);
			
			$numero = explode(".",$numero);

			if(strlen($numero[0])>$params['max']) {
				$this->addError($attribute, 'Número excede el valor máximo permitido');
			}
		
		}

	}
	
	public function beforeSave() {
		foreach ($this->attributes as $key => $value)
			if ($value=='')
				$this->$key = NULL;
			
		return parent::beforeSave();
	}

	public function valida_cedula($attribute,$params) {

		$cedu= PerPersona::model()->findByAttributes(array('cedula'=>$this->$attribute));

			 if($cedu) {
			 	$this->addError($attribute, 'ESTA CEDULA YA ESTA REGISTRADA');
	}	
}
}