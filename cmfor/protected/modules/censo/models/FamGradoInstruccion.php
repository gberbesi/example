<?php

/**
 * This is the model class for table "fam_grado_instruccion".
 *
 * The followings are the available columns in table 'fam_grado_instruccion':
 * @property integer $grado_instruccion_id
 * @property string $nombre
 * @property boolean $estatus
 * @property string $usu_aud_id
 *
 * The followings are the available model relations:
 * @property BarFamilia[] $barFamilias
 * @property FamBeneficiado[] $famBeneficiados
 * @property FamGradoAcademico[] $famGradoAcademicos
 * @property BarEducacion[] $barEducacions
 */
class FamGradoInstruccion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FamGradoInstruccion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fam_grado_instruccion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre', 'required'),
			array('estatus, usu_aud_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('grado_instruccion_id, nombre, estatus, usu_aud_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'barFamilias' => array(self::HAS_MANY, 'BarFamilia', 'grado_intruccion_id'),
			'famBeneficiados' => array(self::HAS_MANY, 'FamBeneficiado', 'grado_instruccion_id'),
			'famGradoAcademicos' => array(self::HAS_MANY, 'FamGradoAcademico', 'grado_instruccion_id'),
			'barEducacions' => array(self::HAS_MANY, 'BarEducacion', 'grado_instruccion_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'grado_instruccion_id' => 'Grado Instruccion',
			'nombre' => 'Nombre',
			'estatus' => 'Estatus',
			'usu_aud_id' => 'Usu Aud',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('grado_instruccion_id',$this->grado_instruccion_id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('estatus',$this->estatus);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}