<?php

/**
 * This is the model class for table "fam_condicion_vivienda_origen".
 *
 * The followings are the available columns in table 'fam_condicion_vivienda_origen':
 * @property integer $condicion_vivienda_id
 * @property string $nombre
 * @property boolean $st_condicion_vivienda
 * @property string $usu_aud_id
 */
class FamCondicionViviendaOrigen extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FamCondicionViviendaOrigen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fam_condicion_vivienda_origen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, st_condicion_vivienda, usu_aud_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('condicion_vivienda_id, nombre, st_condicion_vivienda, usu_aud_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'condicion_vivienda_id' => 'Condicion Vivienda',
			'nombre' => 'Nombre',
			'st_condicion_vivienda' => 'St Condicion Vivienda',
			'usu_aud_id' => 'Usu Aud',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('condicion_vivienda_id',$this->condicion_vivienda_id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('st_condicion_vivienda',$this->st_condicion_vivienda);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}