<?php

/**
 * This is the model class for table "fam_familia".
 *
 * The followings are the available columns in table 'fam_familia':
 * @property integer $familia_id
 * @property string $co_familia
 * @property boolean $st_familia
 * @property string $fe_registro
 * @property string $url_imagen
 * @property integer $procedencia
 * @property string $usu_aud_id
 * @property boolean $gmvv
 * @property boolean $reg_0800
 * @property string $respuesta_id
 * @property integer $n_grupo
 * @property integer $n_discapacitado
 * @property string $monto_viv_perdida
 * @property boolean $estatus
 * @property string $capacidad_inicial
 * @property string $fam_clasificacion_id
 * @property boolean $gmvo
 * @property integer $prioridad
 * @property integer $estatus_aten
 * @property string $plantiamiento
 * @property string $observacion
 * @property string $atendido
 * @property integer $forma_pago
 * @property string $pago_contado
 * @property string $n_pago
 * @property integer $credito_banco
 * @property string $n_deposito
 * @property string $fecha_deposito
 * @property boolean $migrado_inmobiliaria
 * @property integer $forma_de_pago
 *
 * The followings are the available model relations:
 * @property AdjAdjudicacionCp[] $adjAdjudicacionCps
 * @property FamFamiliaClasificacion $famClasificacion
 * @property BenBeneficio[] $benBeneficios
 * @property AdjAdjudicacion[] $adjAdjudicacions
 * @property FamFamiliaAvv[] $famFamiliaAvvs
 * @property RefRefugio[] $refRefugios
 * @property FamCiudadDestino[] $famCiudadDestinos
 * @property FamBeneficiado[] $famBeneficiados
 * @property FamCiudadDestino2[] $famCiudadDestino2s
 * @property DocFamilia[] $docFamilias
 * @property UrbVivienda[] $urbViviendas
 * @property UrbEtapa[] $urbEtapas
 */
class FamFamilia extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FamFamilia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'fam_familia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fe_registro', 'required'),
			array('procedencia, n_grupo, n_discapacitado, prioridad, estatus_aten, forma_pago, credito_banco, forma_de_pago', 'numerical', 'integerOnly'=>true),
			array('monto_viv_perdida, capacidad_inicial', 'length', 'max'=>20),
			array('co_familia, st_familia, url_imagen, usu_aud_id, gmvv, reg_0800, respuesta_id, estatus, fam_clasificacion_id, gmvo, plantiamiento, observacion, atendido, pago_contado, n_pago, n_deposito, fecha_deposito, migrado_inmobiliaria', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('familia_id, co_familia, st_familia, fe_registro, url_imagen, procedencia, usu_aud_id, gmvv, reg_0800, respuesta_id, n_grupo, n_discapacitado, monto_viv_perdida, estatus, capacidad_inicial, fam_clasificacion_id, gmvo, prioridad, estatus_aten, plantiamiento, observacion, atendido, forma_pago, pago_contado, n_pago, credito_banco, n_deposito, fecha_deposito, migrado_inmobiliaria, forma_de_pago', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'adjAdjudicacionCps' => array(self::HAS_MANY, 'AdjAdjudicacionCp', 'familia_id'),
			'famClasificacion' => array(self::BELONGS_TO, 'FamFamiliaClasificacion', 'fam_clasificacion_id'),
			'benBeneficios' => array(self::HAS_MANY, 'BenBeneficio', 'familia_id'),
			'adjAdjudicacions' => array(self::HAS_MANY, 'AdjAdjudicacion', 'familia_id'),
			'famFamiliaAvvs' => array(self::HAS_MANY, 'FamFamiliaAvv', 'familia_id'),
			'refRefugios' => array(self::MANY_MANY, 'RefRefugio', 'fam_familia_refugio(familia_id, refugio_id)'),
			'famCiudadDestinos' => array(self::HAS_MANY, 'FamCiudadDestino', 'familia_id'),
			'famBeneficiados' => array(self::MANY_MANY, 'FamBeneficiado', 'fam_familia_beneficiado(familia_id, beneficiado_id)','condition'=>'st_familia_beneficiado=TRUE'),
			'famCiudadDestino2s' => array(self::HAS_MANY, 'FamCiudadDestino2', 'familia_id'),
			'docFamilias' => array(self::HAS_MANY, 'DocFamilia', 'familia_id'),
			'urbViviendas' => array(self::MANY_MANY, 'UrbVivienda', 'adj_vivienda_familia(familia_id, vivienda_id)'),
			'urbEtapas' => array(self::MANY_MANY, 'UrbEtapa', 'adj_preadjudicacion(familia_id, etapa_id)'),
			'famFamiliaBeneficiado' => array(self::HAS_MANY,'FamFamiliaBeneficiado','familia_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'familia_id' => 'Familia',
			'co_familia' => 'Co Familia',
			'st_familia' => 'St Familia',
			'fe_registro' => 'Fe Registro',
			'url_imagen' => 'Url Imagen',
			'procedencia' => 'Procedencia',
			'usu_aud_id' => 'Usu Aud',
			'gmvv' => 'Gmvv',
			'reg_0800' => 'Reg 0800',
			'respuesta_id' => 'Respuesta',
			'n_grupo' => 'N Grupo',
			'n_discapacitado' => 'N Discapacitado',
			'monto_viv_perdida' => 'Monto Viv Perdida',
			'estatus' => 'Estatus',
			'capacidad_inicial' => 'Capacidad Inicial',
			'fam_clasificacion_id' => 'Fam Clasificacion',
			'gmvo' => 'Gmvo',
			'prioridad' => 'Prioridad',
			'estatus_aten' => 'Estatus Aten',
			'plantiamiento' => 'Plantiamiento',
			'observacion' => 'Observacion',
			'atendido' => 'Atendido',
			'forma_pago' => 'Forma Pago',
			'pago_contado' => 'Pago Contado',
			'n_pago' => 'N Pago',
			'credito_banco' => 'Credito Banco',
			'n_deposito' => 'N Deposito',
			'fecha_deposito' => 'Fecha Deposito',
			'migrado_inmobiliaria' => 'Migrado Inmobiliaria',
			'forma_de_pago' => 'Forma De Pago',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('familia_id',$this->familia_id);
		$criteria->compare('co_familia',$this->co_familia,true);
		$criteria->compare('st_familia',$this->st_familia);
		$criteria->compare('fe_registro',$this->fe_registro,true);
		$criteria->compare('url_imagen',$this->url_imagen,true);
		$criteria->compare('procedencia',$this->procedencia);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('gmvv',$this->gmvv);
		$criteria->compare('reg_0800',$this->reg_0800);
		$criteria->compare('respuesta_id',$this->respuesta_id,true);
		$criteria->compare('n_grupo',$this->n_grupo);
		$criteria->compare('n_discapacitado',$this->n_discapacitado);
		$criteria->compare('monto_viv_perdida',$this->monto_viv_perdida,true);
		$criteria->compare('estatus',$this->estatus);
		$criteria->compare('capacidad_inicial',$this->capacidad_inicial,true);
		$criteria->compare('fam_clasificacion_id',$this->fam_clasificacion_id,true);
		$criteria->compare('gmvo',$this->gmvo);
		$criteria->compare('prioridad',$this->prioridad);
		$criteria->compare('estatus_aten',$this->estatus_aten);
		$criteria->compare('plantiamiento',$this->plantiamiento,true);
		$criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('atendido',$this->atendido,true);
		$criteria->compare('forma_pago',$this->forma_pago);
		$criteria->compare('pago_contado',$this->pago_contado,true);
		$criteria->compare('n_pago',$this->n_pago,true);
		$criteria->compare('credito_banco',$this->credito_banco);
		$criteria->compare('n_deposito',$this->n_deposito,true);
		$criteria->compare('fecha_deposito',$this->fecha_deposito,true);
		$criteria->compare('migrado_inmobiliaria',$this->migrado_inmobiliaria);
		$criteria->compare('forma_de_pago',$this->forma_de_pago);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}