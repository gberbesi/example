<?php

/**
 * This is the model class for table "geo_terreno".
 *
 * The followings are the available columns in table 'geo_terreno':
 * @property integer $terreno_id
 * @property string $superficie
 * @property string $terreno_padre_id
 * @property string $direccion_id
 * @property string $nb_terreno
 * @property string $usu_aud_id
 * @property boolean $st_terreno
 * @property string $tenencia_id
 * @property double $sup_ha
 * @property integer $desindad_estimacion
 * @property integer $numero_viviendas_est
 * @property string $fecha_origen
 * @property string $num_dcrto
 * @property string $num_go
 * @property string $fecha_gaceta
 * @property string $id_propietario
 * @property string $fecha_inf_prefactibilidad
 * @property string $fecha_estudio_geo
 * @property string $implantacion
 * @property string $tipo_implantacion
 * @property string $tipo_fundacion
 * @property string $capacida_admisible_suelo
 * @property string $observaciones
 * @property integer $id_origen
 * @property integer $id_condicion
 * @property integer $id_ambito
 * @property string $superficie_transferida
 * @property integer $id_matriz
 * @property integer $id_tranfe
 * @property string $cod_terreno
 * @property integer $estatus_jur
 * @property integer $prefactivilidad_constructiva
 * @property string $fecha_levan_topo
 * @property string $fecha_est_geotec
 *
 * The followings are the available model relations:
 * @property ComunidadAvv[] $comunidadAvvs
 * @property GeoTerreno $terreno
 * @property GeoTerreno $geoTerreno
 * @property GeoTerrenoTenencia $tenencia
 * @property OrigenTerreno $idOrigen
 * @property CondicionTerreno $idCondicion
 * @property GeoTerrenoAmbito $idAmbito
 * @property Geomatriz $idMatriz
 * @property GeoTranfeInmobiliaria $idTranfe
 * @property PrefConstr $prefactivilidadConstructiva
 * @property GeoDireccion $direccion
 * @property GeoTerrenoPropietario[] $geoTerrenoPropietarios
 * @property GeoTerrenoCoordenada[] $geoTerrenoCoordenadas
 * @property UrbUrbanismo[] $urbUrbanismos
 * @property ProtocoloTerreno[] $protocoloTerrenos
 * @property GeoTerrenoLindero[] $geoTerrenoLinderos
 */
class GeoTerreno extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GeoTerreno the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'geo_terreno';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('desindad_estimacion, numero_viviendas_est, id_origen, id_condicion, id_ambito, id_matriz, id_tranfe, estatus_jur, prefactivilidad_constructiva', 'numerical', 'integerOnly'=>true),
			array('sup_ha', 'numerical'),
			array('superficie', 'length', 'max'=>10),
			array('terreno_padre_id, direccion_id, nb_terreno, usu_aud_id, st_terreno, tenencia_id, fecha_origen, num_dcrto, num_go, fecha_gaceta, id_propietario, fecha_inf_prefactibilidad, fecha_estudio_geo, implantacion, tipo_implantacion, tipo_fundacion, capacida_admisible_suelo, observaciones, superficie_transferida, cod_terreno, fecha_levan_topo, fecha_est_geotec', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('terreno_id, superficie, terreno_padre_id, direccion_id, nb_terreno, usu_aud_id, st_terreno, tenencia_id, sup_ha, desindad_estimacion, numero_viviendas_est, fecha_origen, num_dcrto, num_go, fecha_gaceta, id_propietario, fecha_inf_prefactibilidad, fecha_estudio_geo, implantacion, tipo_implantacion, tipo_fundacion, capacida_admisible_suelo, observaciones, id_origen, id_condicion, id_ambito, superficie_transferida, id_matriz, id_tranfe, cod_terreno, estatus_jur, prefactivilidad_constructiva, fecha_levan_topo, fecha_est_geotec', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'comunidadAvvs' => array(self::HAS_MANY, 'ComunidadAvv', 'terreno_id'),
			'terreno' => array(self::BELONGS_TO, 'GeoTerreno', 'terreno_id'),
			'geoTerreno' => array(self::HAS_ONE, 'GeoTerreno', 'terreno_id'),
			'tenencia' => array(self::BELONGS_TO, 'GeoTerrenoTenencia', 'tenencia_id'),
			'idOrigen' => array(self::BELONGS_TO, 'OrigenTerreno', 'id_origen'),
			'idCondicion' => array(self::BELONGS_TO, 'CondicionTerreno', 'id_condicion'),
			'idAmbito' => array(self::BELONGS_TO, 'GeoTerrenoAmbito', 'id_ambito'),
			'idMatriz' => array(self::BELONGS_TO, 'Geomatriz', 'id_matriz'),
			'idTranfe' => array(self::BELONGS_TO, 'GeoTranfeInmobiliaria', 'id_tranfe'),
			'prefactivilidadConstructiva' => array(self::BELONGS_TO, 'PrefConstr', 'prefactivilidad_constructiva'),
			'direccion' => array(self::BELONGS_TO, 'GeoDireccion', 'direccion_id'),
			'geoTerrenoPropietarios' => array(self::HAS_MANY, 'GeoTerrenoPropietario', 'terreno_id'),
			'geoTerrenoCoordenadas' => array(self::HAS_MANY, 'GeoTerrenoCoordenada', 'terreno_id'),
			'urbUrbanismos' => array(self::HAS_MANY, 'UrbUrbanismo', 'terreno_id'),
			'protocoloTerrenos' => array(self::HAS_MANY, 'ProtocoloTerreno', 'terreno_id'),
			'geoTerrenoLinderos' => array(self::HAS_MANY, 'GeoTerrenoLindero', 'terreno_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'terreno_id' => 'Terreno',
			'superficie' => 'Superficie',
			'terreno_padre_id' => 'Terreno Padre',
			'direccion_id' => 'Direccion',
			'nb_terreno' => 'Nb Terreno',
			'usu_aud_id' => 'Usu Aud',
			'st_terreno' => 'St Terreno',
			'tenencia_id' => 'Tenencia',
			'sup_ha' => 'Sup Ha',
			'desindad_estimacion' => 'Desindad Estimacion',
			'numero_viviendas_est' => 'Numero Viviendas Est',
			'fecha_origen' => 'Fecha Origen',
			'num_dcrto' => 'Num Dcrto',
			'num_go' => 'Num Go',
			'fecha_gaceta' => 'Fecha Gaceta',
			'id_propietario' => 'Id Propietario',
			'fecha_inf_prefactibilidad' => 'Fecha Inf Prefactibilidad',
			'fecha_estudio_geo' => 'Fecha Estudio Geo',
			'implantacion' => 'Implantacion',
			'tipo_implantacion' => 'Tipo Implantacion',
			'tipo_fundacion' => 'Tipo Fundacion',
			'capacida_admisible_suelo' => 'Capacida Admisible Suelo',
			'observaciones' => 'Observaciones',
			'id_origen' => 'Id Origen',
			'id_condicion' => 'Id Condicion',
			'id_ambito' => 'Id Ambito',
			'superficie_transferida' => 'Superficie Transferida',
			'id_matriz' => 'Id Matriz',
			'id_tranfe' => 'Id Tranfe',
			'cod_terreno' => 'Cod Terreno',
			'estatus_jur' => 'Estatus Jur',
			'prefactivilidad_constructiva' => 'Prefactivilidad Constructiva',
			'fecha_levan_topo' => 'Fecha Levan Topo',
			'fecha_est_geotec' => 'Fecha Est Geotec',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('terreno_id',$this->terreno_id);
		$criteria->compare('superficie',$this->superficie,true);
		$criteria->compare('terreno_padre_id',$this->terreno_padre_id,true);
		$criteria->compare('direccion_id',$this->direccion_id,true);
		$criteria->compare('nb_terreno',$this->nb_terreno,true);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('st_terreno',$this->st_terreno);
		$criteria->compare('tenencia_id',$this->tenencia_id,true);
		$criteria->compare('sup_ha',$this->sup_ha);
		$criteria->compare('desindad_estimacion',$this->desindad_estimacion);
		$criteria->compare('numero_viviendas_est',$this->numero_viviendas_est);
		$criteria->compare('fecha_origen',$this->fecha_origen,true);
		$criteria->compare('num_dcrto',$this->num_dcrto,true);
		$criteria->compare('num_go',$this->num_go,true);
		$criteria->compare('fecha_gaceta',$this->fecha_gaceta,true);
		$criteria->compare('id_propietario',$this->id_propietario,true);
		$criteria->compare('fecha_inf_prefactibilidad',$this->fecha_inf_prefactibilidad,true);
		$criteria->compare('fecha_estudio_geo',$this->fecha_estudio_geo,true);
		$criteria->compare('implantacion',$this->implantacion,true);
		$criteria->compare('tipo_implantacion',$this->tipo_implantacion,true);
		$criteria->compare('tipo_fundacion',$this->tipo_fundacion,true);
		$criteria->compare('capacida_admisible_suelo',$this->capacida_admisible_suelo,true);
		$criteria->compare('observaciones',$this->observaciones,true);
		$criteria->compare('id_origen',$this->id_origen);
		$criteria->compare('id_condicion',$this->id_condicion);
		$criteria->compare('id_ambito',$this->id_ambito);
		$criteria->compare('superficie_transferida',$this->superficie_transferida,true);
		$criteria->compare('id_matriz',$this->id_matriz);
		$criteria->compare('id_tranfe',$this->id_tranfe);
		$criteria->compare('cod_terreno',$this->cod_terreno,true);
		$criteria->compare('estatus_jur',$this->estatus_jur);
		$criteria->compare('prefactivilidad_constructiva',$this->prefactivilidad_constructiva);
		$criteria->compare('fecha_levan_topo',$this->fecha_levan_topo,true);
		$criteria->compare('fecha_est_geotec',$this->fecha_est_geotec,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}