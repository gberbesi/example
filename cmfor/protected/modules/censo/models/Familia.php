<?php

/**
 * This is the model class for table "censo.familia".
 *
 * The followings are the available columns in table 'censo.familia':
 * @property integer $familia_id
 * @property boolean $st_familia
 * @property string $fe_registro
 * @property string $usu_aud_id
 * @property integer $n_grupo
 * @property integer $n_discapacitado
 * @property string $monto_viv_perdida
 * @property string $capacidad_inicial
 * @property string $fam_clasificacion_id
 * @property string $direccion_id
 * @property string $direccion_destino
 * @property string $refugio_id
 * @property string $fecha_migracion
 * @property string $ingreso_familiar
 * @property string $planteamiento
 * @property integer $direccion_destino2
 */
class Familia extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Familia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'censo.familia';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fe_registro', 'required'),
			array('n_grupo, n_discapacitado, direccion_destino2', 'numerical', 'integerOnly'=>true),
			array('monto_viv_perdida, capacidad_inicial', 'length', 'max'=>20),
			array('ingreso_familiar', 'length', 'max'=>10),
			array('st_familia, usu_aud_id, fam_clasificacion_id, direccion_id, direccion_destino, refugio_id, fecha_migracion, planteamiento', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('familia_id, st_familia, fe_registro, usu_aud_id, n_grupo, n_discapacitado, monto_viv_perdida, capacidad_inicial, fam_clasificacion_id, direccion_id, direccion_destino, refugio_id, fecha_migracion, ingreso_familiar, planteamiento, direccion_destino2', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'famClasificacion' => array(self::BELONGS_TO, 'FamFamiliaClasificacion', 'fam_clasificacion_id'),
			'direccionDestino' => array(self::BELONGS_TO, 'Direccion', 'direccion_destino'),
			'direccion' => array(self::BELONGS_TO, 'Direccion', 'direccion_id'),
			'censados' => array(self::HAS_MANY, 'Censado', 'familia_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'familia_id' => 'Familia',
			'st_familia' => 'St Familia',
			'fe_registro' => 'Fe Registro',
			'usu_aud_id' => 'Usu Aud',
			'n_grupo' => 'N Grupo',
			'n_discapacitado' => 'N Discapacitado',
			'monto_viv_perdida' => 'Monto Viv Perdida',
			'capacidad_inicial' => 'Capacidad Inicial',
			'fam_clasificacion_id' => 'Fam Clasificacion',
			'direccion_id' => 'Direccion',
			'direccion_destino' => 'Direccion Destino',
			'refugio_id' => 'Refugio',
			'fecha_migracion' => 'Fecha Migracion',
			'ingreso_familiar' => 'Ingreso Familiar',
			'planteamiento' => 'Planteamiento',
			'direccion_destino2' => 'Direccion Destino2',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('familia_id',$this->familia_id);
		$criteria->compare('st_familia',$this->st_familia);
		$criteria->compare('fe_registro',$this->fe_registro,true);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('n_grupo',$this->n_grupo);
		$criteria->compare('n_discapacitado',$this->n_discapacitado);
		$criteria->compare('monto_viv_perdida',$this->monto_viv_perdida,true);
		$criteria->compare('capacidad_inicial',$this->capacidad_inicial,true);
		$criteria->compare('fam_clasificacion_id',$this->fam_clasificacion_id,true);
		$criteria->compare('direccion_id',$this->direccion_id,true);
		$criteria->compare('direccion_destino',$this->direccion_destino,true);
		$criteria->compare('refugio_id',$this->refugio_id,true);
		$criteria->compare('fecha_migracion',$this->fecha_migracion,true);
		$criteria->compare('ingreso_familiar',$this->ingreso_familiar,true);
		$criteria->compare('planteamiento',$this->planteamiento,true);
		$criteria->compare('direccion_destino2',$this->direccion_destino2);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}