<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	/*
	public function authenticate()
	{
		$users=array(
			// username => password
			'demo'=>'demo',
			'admin'=>'admin',
		);
		if(!isset($users[$this->username]))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if($users[$this->username]!==$this->password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
			$this->errorCode=self::ERROR_NONE;
		return !$this->errorCode;
	}
	*/
	
	private $_id;
	public function authenticate()
	{	Yii::import('application.modules.usuario.models.AuthAssignment',true);
		$uValido=false;
		$username=strtolower($this->username);
		$user=GenUsuUsuario::model()->find('LOWER(nb_usuario)=:username',array(':username'=>$username));
		$modelAuthAssignment2=AuthAssignment::model()->find('itemname = \'ActualizacionCenso\' and userid=:id',array(':id'=>(string)$user->usuario_id));

		if ($user){
			if($user->st_usuario){
				$user->scenario='auth';
				if ($user->fe_expiracion!=NULL){
					$user->diaActual=date('d-m-Y H:i');
					$user->scenario='confe';
					$user->fe_expiracion=date('d-m-Y H:i',strtotime($user->fe_expiracion));
						
					$uValido=$user->validate(array('fe_expiracion'));
				
				} else {
					$uValido=true;
				}
				
			}

			if (!$uValido){
				$user->st_usuario=0;
				$user->save(false);
			}

			
		}

		if($user===null){
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		} else if($user){
			
			if(!$user->validatePassword($this->password)) {
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			} else {
				
				if($user->st_usuario!=true) {
					$this->errorCode = 3;
				} elseif(!$modelAuthAssignment2) {
					$this->errorCode = 4;
				} elseif($user->actualizadocdlp!=true) {
					$this->errorCode = 5;
				} else {
					$this->_id=$user->usuario_id;
					$this->username=$user->nb_usuario;
					$this->errorCode=self::ERROR_NONE;
				}
			}                           
			
		}

		return $this->errorCode; //USUARIO REGISTRADO Y DESACTIVADO 
		
	}
	public function getId()
	{
		return $this->_id;
		
	}

	
}