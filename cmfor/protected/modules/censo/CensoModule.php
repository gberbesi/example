<?php

class CensoModule extends CWebModule
{
	public function init()
	{
		
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'censo.models.*',
			'censo.components.*',
		));
		
		$this->defaultController = 'censo';
		Yii::app()->errorHandler->errorAction = '/'.$this->id.'/error/error';
		//echo '<pre>';print_r($this->id);exit;
		//Yii::app()->defaultController='/censoreload/censo/';
		Yii::app()->user->loginUrl = array('/'.$this->id.'/usuario/login');
		
		
		
	}

	public function beforeControllerAction($controller, $action)
	{//echo Yii::app()->defaultController;exit;
		if($controller->id == 'usuario') {
			$controller->layout='/layouts/login';
		} else {
			$controller->layout='/layouts/main';
		}

		
		Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/censov2/daterangepicker.css');
		
		Yii::app()->clientScript->packages['maskedinput'] = array(
			'baseUrl'=>'js/censov2/', 
			'js'=>array('jquery.maskedinput.js'),
			'depends'=>array('jquery'),
		);
		
		
		Yii::app()->clientScript->packages['jquery'] = array(
			'baseUrl'=>'js/censov2/libreria/jquery/js', 
			'js'=>array('jquery-2.1.0.min.js')
		);
		
		Yii::app()->clientScript->packages['blockui'] = array(
			'baseUrl'=>'js/censov2/', 
			'js'=>array('jquery.blockUI.js'),
			'depends'=>array('jquery'),
		);
		
		Yii::app()->clientScript->packages['moment'] = array(
			'baseUrl'=>'js/censov2/', 
			'js'=>array('moment.min.js'),
			'depends'=>array('jquery'),
		);
		
		Yii::app()->clientScript->packages['daterangepicker'] = array(
			'baseUrl'=>'js/censov2/', 
			'js'=>array('daterangepicker.js'),
			'depends'=>array('moment'),
		);
		
		Yii::app()->clientScript->registerCoreScript('blockui');
		Yii::app()->clientScript->registerCoreScript('moment');
		Yii::app()->clientScript->registerCoreScript('daterangepicker');
		
		Yii::app()->clientScript->registerScript('utils', '
		
			function bloquear() {
			
				$.blockUI({ message: "<h1>Por favor espere...</h1>", css: { 
					border: "none", 
					padding: "15px", 
					backgroundColor: "#000", 
					"-webkit-border-radius": "10px", 
					"-moz-border-radius": "10px", 
					opacity: .5, 
					color: "#fff" 
				},
				baseZ: 2000
				}); 
				
			}
				
			$(document).ajaxStart(bloquear).ajaxStop($.unblockUI);
			
			$( "form" ).submit(function( event ) {
				bloquear();
			});
			
			$(".date").daterangepicker({
					singleDatePicker: true,
					locale: {
						format: "DD/MM/YYYY"
					},
					"locale": {
						"format": "DD/MM/YYYY",
						"separator": " - ",
						"applyLabel": "Seleccionar",
						"cancelLabel": "Cancelar",
						"fromLabel": "Desde",
						"toLabel": "hasta",
						"customRangeLabel": "Custom",
						"weekLabel": "W",
						"daysOfWeek": [
							"Do",
							"Lu",
							"Ma",
							"Mi",
							"Ju",
							"Vi",
							"Sa"
						],
						"monthNames": [
							"Enero",
							"Febrero",
							"Marzo",
							"Abril",
							"Mayo",
							"Junio",
							"Julio",
							"Agosto",
							"Septiembre",
							"Octubre",
							"Noviembre",
							"Diciembre"
						],
						"firstDay": 1
					},
					showDropdowns: true,
					minDate: moment().subtract(100, "years").format("DD/MM/YYYY"),
					maxDate: moment().add(100, "years").format("DD/MM/YYYY")
					
				});
			
		', CClientScript::POS_READY);
		
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
