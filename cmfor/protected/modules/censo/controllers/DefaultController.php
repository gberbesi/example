<?php

class DefaultController extends Controller
{
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control f CRUD operations
		);
	}
	
	public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index'),
                'users'=>array('@'),
            ),
            //array('allow','actions' => array('ActualizarDatos'), 'roles' => array('censo/Censado/ActualizarDatos')),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

	public function actionIndex()
	{
		$this->render('index');
	}
}