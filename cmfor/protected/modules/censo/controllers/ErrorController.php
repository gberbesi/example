<?php
/*
@Gabriel Cisneros
*/
class ErrorController extends Controller
{
	public function actionIndex()
	{

		$this->render('index');
	}
	
	public function actionError()
	{
		
	$this->layout='/layouts/error';
	
	Yii::import('application.modules.usuario.models.UsuUsuario');
	Yii::import('application.modules.usuario.models.PerPersona');
	 if($error=Yii::app()->errorHandler->error)
	 {
	 
	 //echo '<pre>';print_r(Yii::app()->params['correoReporteError']);exit;
	 $usuario=NULL;
	 $infoCliente = ((isset($_SERVER['HTTP_CLIENT_IP']))?'IP Compartida: '.$_SERVER['HTTP_CLIENT_IP'].' ':"").((isset($_SERVER['HTTP_X_FORWARDED_FOR']))?'IP PROXY: '.$_SERVER['HTTP_X_FORWARDED_FOR'].' ':"").((isset($_SERVER['REMOTE_ADDR']))?'IP: '.$_SERVER['REMOTE_ADDR'].' ':"").((isset($_SERVER['HTTP_USER_AGENT']))?'Navegador: '.$_SERVER['HTTP_USER_AGENT'].' ':"");
	 $order   = array("\r\n", "\n", "\r");
	 $replace = '<br />';
	 if(Yii::app()->user->id)
	 $usuario=UsuUsuario::model()->findByPk(Yii::app()->user->id);
		$mensaje='
	<div class="borderfomulario" style="padding:10px;border:#AD1818 2px solid; width:1000px; margin: 0px auto; margin-bottom: 1.5em; background: #F5F5F5;">
		<table>
		<tr>
		<th colspan="2"><div style="background:#FF4000">Error '.$error['code'].'</div></th>
		</tr>
		<tr style="background:#E6E6E6">
		 <td><b>Tipo</b></td><td><div>'.$error['type'].'</div></td>
		</tr>
		<tr style="background:#E6E6E6">
		 <td><b>Mensaje</b></td> <td><div>'.$error['message'].'</div></td>
		</tr>
		<tr style="background:#E6E6E6">
		 <td><b>Archivo</b></td> <td><div>'.$error['file'].'</div></td>
		</tr>
		<tr style="background:#E6E6E6">
		 <td><b>Linea</b></td> <td><div>'.$error['line'].'</div></td>
		</tr>
		<tr style="background:#E6E6E6">
		 <td><b>Traza</b></td> <td><div>'.str_replace($order,$replace,$error['trace']).'</div></td>
		</tr>
		<tr style="background:#E6E6E6">
		 <td><b>Traza</b></td> <td><div><pre>'.print_r($error,true).'</pre></div></td>
		</tr>
		<tr>
		 <td colspan="2"><div style="background:#045FB4; text-align:center; color:#fff;"><b>Datos del Cliente</b></div></td>
		</tr>
		<tr style="background:#E6E6E6">
		 <td><b>Usuario</b></td> <td><div>'.$usuario->nb_usuario.' '.(($usuario &&$usuario->persona)?$usuario->persona->nombre1:"").' '.
		 	(($usuario &&$usuario->persona)?$usuario->persona->apellido1:"").'</div></td>
		</tr>
		<tr style="background:#E6E6E6">
		 <td><b>PC</b></td> <td><div>'.$infoCliente.'</div></td>
		</tr>
	</div>	
		
	';
	
	
		if($error['message']!='CDbConnection failed to open the DB connection.') {
			
			Yii::import('application.extensions.phpmailer.JPhpMailer');
			$mail = new JPhpMailer;
			$mail->IsSMTP();
			$mail->Host = Yii::app()->params['mailHost'];//'172.16.0.20';
			$mail->Port = Yii::app()->params['mailPortSsl'];
			$mail->CharSet = "utf-8";
			$mail->SMTPAuth = true;
			$mail->SMTPSecure = 'ssl';
			$mail->Username = Yii::app()->params['mailUsername'];//'xmolina';
			$mail->Password = Yii::app()->params['mailUserPassw'];
			$mail->SetFrom(Yii::app()->params['mailRemitente'], Yii::app()->params['nombreRemitente']);
			
			$mail->Subject = Yii::app()->params['mailAsunto'];
			//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
			$mail->MsgHTML($mensaje);
			$correos=Yii::app()->params['correoReporteError'];
			foreach($correos as $correo) {		
				$mail->AddAddress($correo, 'Error');
			}
		
			if(!$mail->Send()) {
			   throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente intentelo mas tarde.',500);
			}
		}
		
		if($error['code']=='404') {
			$this->render('error404', $error);
		} elseif($error['code']=='403') {
			$this->render('error403', $error);
		} else {
			$this->render('error', $error);
		}
		
	
	 }
	
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}