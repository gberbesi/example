<?php

class PsmController extends Controller
{
	public function filters()
	{
		return array(
			'accessControl', // perform access control f CRUD operations
		);
	}
	
	
	public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('registro','certificado','buscarMunicipio','buscarMunicipioCiudad','buscarParroquia','cedula','delete'),
                'users'=>array('@'),
            ),
            //array('allow','actions' => array('ActualizarDatos'), 'roles' => array('censo/Censado/ActualizarDatos')),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
	

	public function actionRegistro($cedulajf=NULL,$persona=NULL,$familiaid=NULL) {
		
		$primerRegistro = false;
		
		if($familiaid=='') {
			$familiaid=NULL;
		}
		
		if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")) {
			
			$cedula=(string)$cedulajf;
			$personapsm = Censado::model()->findByAttributes(array('cedula'=>$cedula,'st_censado'=>true,'familia_id'=>$familiaid));
			
		} else {
			
			$idusuario = Yii::app()->user->id;
			
			$usuario = UsuUsuario::model()->with('persona')->findByPk($idusuario);
			if($usuario && $usuario->persona) {
				$cedula = $usuario->persona->cedula;
				$personapsm = Censado::model()->findByAttributes(array('cedula'=>$cedula,'st_censado'=>true,'familia_id'=>$familiaid));
			} else {
				//persona no tiene cedula o usuario no existe
				exit;
			}
		
		}


		if($persona==NULL) {
			$psm = new Censado;
			if($personapsm && $personapsm->familia_id!='') {
				$psm->familia_id = $personapsm->familia_id;
			} else {
				$psm->parentesco_id=12;
				$psm->cedula=$cedula;
				$primerRegistro = true;
			}
		} else {
			$psm = Censado::model()->findByPk($persona);
			if(!$personapsm || $personapsm->familia_id!=$psm->familia_id) {
				//persona no pertenece al grupo familiar
				if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")) {
					$this->redirect(array('censo/index','cedulajf'=>$cedulajf));
				} else {
					$this->redirect(array('censo/index'));
				}
				
				echo 'Error: 01'; exit;
			}
		}

		if($psm->parentesco_id==12) {
			$jefefamilia = true;
		} else {
			$jefefamilia = false;
		}
		
		if($jefefamilia) {
			$psm->scenario='jefefamilia';
		} else {
			$psm->scenario='cargafamiliar';
		}
		
		
		$familia = Familia::model()->findByPk($psm->familia_id);
		if($familia==NULL) {
			$familia = new Familia;
		} 

		
		if($primerRegistro) {
			$personatem=PerPersona::model()->findByAttributes(array('cedula'=>$cedula));

			$telefonotem=PerTelefono::model()->findByAttributes(array('persona_id'=>$personatem->persona_id));

			$psm->nombre1=$personatem->nombre1;
			$psm->nombre2=$personatem->nombre2;
			$psm->apellido1=$personatem->apellido1;
			$psm->apellido2=$personatem->apellido2;
			$psm->codigo_cdlp=$personatem->codigo_carnetdlp;
			$psm->fe_nac=$personatem->fe_nac;
			

			$psm->e_mail=$personatem->e_mail;
			$psm->telefono_1=$telefonotem->nu_telefono;
		} elseif($jefefamilia) {
			$personatem=PerPersona::model()->findByAttributes(array('cedula'=>$cedula));
			$psm->codigo_cdlp=$personatem->codigo_carnetdlp;
		}
		$psmDireccion = Direccion::model()->findByPk($familia->direccion_id);
		$tiposol=$psmDireccionDestino->tipoSolicitud->nombre;
		if($psmDireccion==NULL) {
			$psmDireccion = new Direccion;
		} 
		//$psmDireccion = new Direccion;
		$psmDireccionDestino = Direccion::model()->findByPk($familia->direccion_destino);
		if($psmDireccionDestino==NULL) {
			$psmDireccionDestino = new Direccion;
		} 
		
		//$psmDireccionDestino = new Direccion;
		
		$psm->ingreso_mensual=number_format($psm->ingreso_mensual,2,",", ".");
		$psm->honorarios_profesionales=number_format($psm->honorarios_profesionales,2,",", ".");
		$psm->bono_alimentacion=number_format($psm->bono_alimentacion,2,",", ".");
		$psm->otros_ingresos=number_format($psm->otros_ingresos,2,",", ".");
		
		$psm->fe_nac = implode("/",array_reverse(explode("-",$psm->fe_nac)));

		
		
		if(isset($_POST['Censado'])) {
			
			$psm->attributes=$_POST['Censado'];
			$psmDireccion->attributes=$_POST['Direccion'][0];
			$psmDireccionDestino->attributes=$_POST['Direccion'][1];
			
			if($primerRegistro) {
				$psm->cedula=$cedula;
			}
			
			$valido=true; 
			$valido=$psm->validate()&&$valido;
			$valido=$psmDireccion->validate()&&$valido;
			$valido=$psmDireccionDestino->validate()&&$valido;

			if(!$valido) {
				Yii::app()->clientScript->registerScript('modalError', "
					$('#errorModal').modal('show');
				", CClientScript::POS_READY);
			}
			
			if($valido) {
				Yii::import('application.modules.reportes.models.Somos',true);
				$psm->ingreso_mensual=str_replace('.', '', $psm->ingreso_mensual);
				$psm->ingreso_mensual=str_replace(',', '.', $psm->ingreso_mensual);
				
				$psm->honorarios_profesionales=str_replace('.', '', $psm->honorarios_profesionales);
				$psm->honorarios_profesionales=str_replace(',', '.', $psm->honorarios_profesionales);
				
				$psm->bono_alimentacion=str_replace('.', '', $psm->bono_alimentacion);
				$psm->bono_alimentacion=str_replace(',', '.', $psm->bono_alimentacion);
				
				$psm->otros_ingresos=str_replace('.', '', $psm->otros_ingresos);
				$psm->otros_ingresos=str_replace(',', '.', $psm->otros_ingresos);
				
				$psm->fe_nac = implode("-",array_reverse(explode("/",$psm->fe_nac)));
				
				$psmDireccion->save(false);
				$psmDireccionDestino->save(false);
				
				$familia->direccion_id = $psmDireccion->direccion_id;
				$familia->direccion_destino = $psmDireccionDestino->direccion_id;
				if($familia->isNewRecord) {
					$familia->fam_clasificacion_id = 6;
					$familia->fe_registro = new CDbExpression('NOW()');
				}
				
				if($psm->cedula!=''){
					$somovzla = Somos::model()->findAll('cedula=:cedula',array(':cedula'=>$psm->cedula));
				}
				if($somovzla!=null){

					$psm->somos_venezuela=true;
					
				}elseif($somovzla==null){

						
					$psm->somos_venezuela=false;			
					
				}

				

				$familia->save(false);
				
				$psm->familia_id = $familia->familia_id;
				$psm->save(false);
				$sql='UPDATE censo.censado
					  SET baremo_0800=(SELECT calcular_baremo_censo0800(';
					  
				$sql.=$cedula.'))
					  WHERE cedula='.$cedula;

				$sql.='and parentesco_id=12';

				$comando = Yii::app() ->db -> createCommand($sql);
                $comando->queryAll();

				
				
				if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")) {
					Yii::app()->user->setFlash('success', "¡ACTUALIZACIÓN EXITOSA!");
					$this->redirect(array('censo/index','cedulajf'=>$cedulajf));
				} else {
					Yii::app()->user->setFlash('success', "¡ACTUALIZACIÓN EXITOSA!");
					$this->redirect(array('censo/index'));
				}
				
			}
			
		}
		
		
		//echo '<pre>';print_r($gmvv->attributes);exit;
		$this->render('registro',array(
					'psm'=>$psm,
					'psmDireccion'=>$psmDireccion,
					'psmDireccionDestino'=>$psmDireccionDestino,
					'jefefamilia'=>$jefefamilia,
					'primerRegistro'=>$primerRegistro,
					'cedulajf'=>$cedulajf, 
					));
		 
	}
	
	
	
	public function actionBuscarMunicipio() {
		 
		$data=CHtml::listData(GeoMunicipio::model()->findAll(array(
				'condition'=>'geo_estado_id=:geo_estado_id AND nombre NOT LIKE \'OTRO\' AND nombre NOT LIKE \'OTRA\'',
				'params'=>array(':geo_estado_id'=>$_POST['estado_id']),
				'order'=>'nombre ASC' )) ,
				'geo_municipio_id', 'nombre' );
		echo CHtml::tag('option',array('value'=>''),'--SELECCIONE--',true);
		foreach($data as $value=>$name)
		{
			//print_r($data);
			
			echo CHtml::tag('option',
					array('value'=>$value),CHtml::encode($name),true);
		}
	}
	
	public function actionBuscarParroquia() {
		 
		$data=CHtml::listData(GeoParroquia::model()->findAll(array(
				'condition'=>'geo_municipio_id=:geo_municipio_id AND nombre NOT LIKE \'OTRO\' AND nombre NOT LIKE \'OTRA\'',
				'params'=>array(':geo_municipio_id'=>$_POST['municipio_id']),
				'order'=>'nombre ASC' )) ,
				'geo_parroquia_id', 'nombre' );
		echo CHtml::tag('option',array('value'=>''),'--SELECCIONE--',true);
		foreach($data as $value=>$name)
		{
			//print_r($data);
			
			echo CHtml::tag('option',
					array('value'=>$value),CHtml::encode($name),true);
		}
	}
	
	public function actionBuscarMunicipioCiudad() {
		
		//estado		
		$data=CHtml::listData(GeoMunicipio::model()->findAll(array(
				'condition'=>'geo_estado_id=:geo_estado_id AND nombre NOT LIKE \'OTRO\' AND nombre NOT LIKE \'OTRA\'',
				'params'=>array(':geo_estado_id'=>$_POST['estado_id']),
				'order'=>'nombre ASC' )) ,
				'geo_municipio_id', 'nombre' );
		
		$selectMunicipio = CHtml::tag('option',array('value'=>''),'--SELECCIONE--',true);

		foreach($data as $value=>$name)
		{
			//print_r($data);
			
			$selectMunicipio .= CHtml::tag('option',
					array('value'=>$value),CHtml::encode($name),true);
		}
		
		//ciudad
		$dataCiudad=CHtml::listData(GeoCiudad::model()->findAll(array(
				'condition'=>'estado_id=:estado_id AND estatus=1',
				'params'=>array(':estado_id'=>$_POST['estado_id']),
				'order'=>'nombre ASC' )) ,
				'geo_ciudad_id', 'nombre' );
		
		$selectCiudad = CHtml::tag('option',array('value'=>''),'--SELECCIONE--',true);

		foreach($dataCiudad as $value=>$name) {
			$selectCiudad .= CHtml::tag('option',
					array('value'=>$value),CHtml::encode($name),true);
		}
		
		
		echo CJSON::encode(array('municipio'=>$selectMunicipio,'ciudad'=>$selectCiudad));
	}
	
	public function actionCedula() {

		$cedula = trim($_POST['cedula']);

        header('Content-type: application/json');
        if(!($cedula=="" || !preg_match('/^[0-9]+$/', $cedula) || strlen($cedula)>10)) {

			$onidex = Onidex::model()->findByAttributes(array('cedula'=>$cedula));
			$cedu= Censado::model()->findByAttributes(array('cedula'=>$cedula));


			 if($cedu) {
				echo CJSON::encode(array(
					'nacionalidad'=>'',
					'cedula'=>'',
					'nombre1'=>'',
					'nombre2'=>'',
					'apellido1'=>'',
					'apellido2'=>'',
					'fecha_nac'=>'',
					'mensaje'=>'Usted ya se encuentra registrado en el registro 0800',
					'sexo'=>'',
					'edo_civil'=>'',
				));
			}
			else if($onidex) {
				echo CJSON::encode(array(
					'nacionalidad'=>$onidex->nac,
					'cedula'=>$onidex->cedula,
					'nombre1'=>$onidex->nombre1,
					'nombre2'=>$onidex->nombre2,
					'apellido1'=>$onidex->apellido1,
					'apellido2'=>$onidex->apellido2,
					'fecha_nac'=>'',
					'sexo'=>'',
					'edo_civil'=>'',
				));
			}
			exit;
		}
		/*
		[nac] => V
            [cedula] => 16857488
            [nacionalidad_acr] => VEN
            [nacionalidad] => VEN
            [nombre1] => Aidalys
            [nombre2] => Josefina
            [apellido1] => Aponte
            [apellido2] => Blanco
            [fecha_nac] => 1984-06-08
            [codobjecion] => 00
            [codoficina] => 01
            [edo_civil] => 5
            [naturalizado] => 0
            [sexo] => F
            [id] => 14124275
			*/
		echo CJSON::encode(array(
			'nacionalidad'=>'',
			'cedula'=>'',
			'nombre1'=>'',
			'nombre2'=>'',
			'apellido1'=>'',
			'apellido2'=>'',
			'fecha_nac'=>'',
			'sexo'=>'',
			'edo_civil'=>'',
		));
	}

	
	public function actionCertificado($familiaId) {
        //$this->layout='//layouts/main2';
        //$this->validarGetId($familiaId);
        $model = Censado::model()->findAll("familia_id=:familia_id ORDER BY parentesco_id in(12)desc, parentesco_id asc",array(':familia_id'=>$familiaId));
        $familiaDelUsuario = FALSE;
        $usuario = UsuUsuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>Yii::app()->user->id));//realidad
        if(!$model)
            $this->redirect(array('/'));
           
        foreach ($model AS $key => $value){
            if($usuario->persona->nacionalidad ==$value->nacionalidad &&  $usuario->persona->cedula==$value->cedula)
                $familiaDelUsuario = TRUE;
        }
        if(!$familiaDelUsuario && !Yii::app()->user->checkAccess("censo/Familia/SearchAdmin"))
        {
            $this->redirect(array('censo/index'));
        }   
        $auditoria = CensoFamiliaAudit::model()->find('familia_id=:familia_id',array(':familia_id'=>$model[0]->familia_id));
       
        $familia = $model[0]->familia;
        $this->render('certificado',array(
            'model'=>$model,'familia'=>$familia,
            'auditoria'=>$auditoria
        ));
        
        
    }
    public function actionDelete($persona,$cedulajf=NULL) {


    	$usuario = UsuUsuario::model()->with('persona')->findByPk(Yii::app()->user->id);
		if($usuario && $usuario->persona) {
			$personapsm = Censado::model()->findByAttributes(array('cedula'=>$usuario->persona->cedula));
		} else {
			//persona no tiene cedula o usuario no existe
			echo 'Error: 01'; exit;
		}

		$registroAborrar = Censado::model()->findByAttributes(array('censado_id'=>$persona));


		if(Yii::app()->user->checkAccess("censo/Familia/SearchAdmin") || $registroAborrar->familia_id==$personapsm->familia_id){

			$delete="DELETE FROM censo.censado WHERE censado_id = :censado_id";
			$command = Yii::app()->db->createCommand($delete);
			$command->bindParam(":censado_id",$persona);
			$command->execute();
		}
		
		if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")) {
			$this->redirect(array('censo/index&msg=Registro eliminado correctamente','cedulajf'=>$cedulajf));
		} else {
			$this->redirect(array('censo/index&msg=Registro eliminado correctamente'));
		}
		
	}

}