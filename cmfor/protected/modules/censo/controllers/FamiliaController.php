<?php
Yii::import('application.modules.familia.models.FamFamiliaClasificacion',true);
Yii::import('application.modules.familia.models.FamParentesco',true);
Yii::import('application.modules.usuario.models.UsuUsuario',true);
Yii::app()->getComponent("bootstrap");
class FamiliaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view','MigracionPlan','update','CedulaCensado'),
				'users'=>array('@'),
			),/*
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin',),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),*/
			array('allow','actions' => array('Admin'), 'roles' => array('censo/Familia/Admin')),
			array('allow','actions' => array('Search0800'), 'roles' => array('censo/Familia/Search0800')),
			array('allow','actions' => array('View'), 'roles' => array('censo/Familia/View')),
			array('allow','actions' => array('Search'), 'roles' => array('censo/Familia/Search')),
			array('allow','actions' => array('SearchCenso'), 'roles' => array('censo/Familia/SearchCenso')),
			array('allow','actions' => array('CedulaCensado'), 'roles' => array('censo/Familia/CedulaCensado')),
			array('allow','actions' => array('SearchAdmin'), 'roles' => array('censo/Familia/SearchAdmin')),
			array('allow','actions' => array('Migracion'), 'roles' => array('censo/Familia/Migracion')),
                        array('allow','actions' => array('Migracion0800'), 'roles' => array('censo/Familia/Migracion0800')),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		$this->layout='//layouts/emergenteCenso';
		if(isset($_GET['familiaId']))
			$familiaId = $_GET['familiaId'];
		elseif (isset($_POST['familiaId']))
			$familiaId = $_POST['familiaId'];
		$familia=$this->loadModel($familiaId);
		$model = $familia->censados;
		$this->render('view',array(
			'model'=>$model,
			'familia'=>$familia,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Familia;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Familia']))
		{
			$model->attributes=$_POST['Familia'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->familia_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($familiaId)
	{
		$model=$this->loadModel($familiaId);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Familia']))
		{
			$model->attributes=$_POST['Familia'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->familia_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Familia');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */

	public function actionAdmin($familiaId)
	{
	 //echo "<pre>";print_r(Yii::app()->user->getState('familia_id'));exit;
		$familia = Familia::model()->find("MD5('".Familia::key."'||familia_id)=:familia_id",array(':familia_id'=>$familiaId));
		//echo Yii::app()->user->getState('familia_id')."<pre>";print_r($familia);exit;
		$model=new Censado('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Censado']))
			$model->attributes=$_GET['Censado'];

		$this->render('admin',array(
			'model'=>$model,
			'familia'=>$familia,
			
		));
	}

	public function actionSearch0800($familia)
	{ 
	//print_r($familia); die();
	
	 //echo "<pre>";print_r(Yii::app()->user->getState('familia_id'));exit;
	 	$this->layout='//layouts/main2';
		//$model = new BusquedaPersonaCensos('search');
		$usuario = UsuUsuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>Yii::app()->user->id));//realidad
		$model=BusquedaPersonaCensos::model()->findAll('nacionalidad ilike :nacionalidad AND cedula =:cedula AND (fam_clasificacion_id =6 OR fam_clasificacion_id = 7) AND parentesco_id=12 AND st_censado = TRUE ORDER BY fam_clasificacion_id desc',
												array(':nacionalidad'=>$usuario->persona->nacionalidad,':cedula'=>(string)$usuario->persona->cedula));
//		if(isset($_GET['BusquedaPersonaCensos']))
//			$model->attributes=$_GET['BusquedaPersonaCensos'];


		if(!$model)
			$this->redirect(array('/'));
		$this->render('search',array(
			'model'=>$model,
			'familia'=>$familia,
			
		));
	}
	
	
	
	public function actionSearch()
	{ 
	
	
	 //echo "<pre>";print_r(Yii::app()->user->getState('familia_id'));exit;
	 	$this->layout='//layouts/main2';
		//$model = new BusquedaPersonaCensos('search');
		$usuario = UsuUsuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>Yii::app()->user->id));//realidad
		$model=BusquedaPersonaCensos::model()->findAll('nacionalidad ilike :nacionalidad AND cedula =:cedula AND (fam_clasificacion_id =6 OR fam_clasificacion_id = 7) AND parentesco_id=12 AND st_censado = TRUE ORDER BY fam_clasificacion_id desc',
												array(':nacionalidad'=>$usuario->persona->nacionalidad,':cedula'=>(string)$usuario->persona->cedula));
//		if(isset($_GET['BusquedaPersonaCensos']))
//			$model->attributes=$_GET['BusquedaPersonaCensos'];
//			
		if(!$model)
			$this->redirect(array('/'));
		$this->render('search',array(
			'model'=>$model,
		));
	}
	
	public function actionSearchAdmin()
	{
		$this->layout='//layouts/main';

		$model = new Cedula;
		
		$gmvv=false;
		$psm=false;
		
		if(isset($_POST['Cedula'])) {
			$model->attributes=$_POST['Cedula'];
			
			if($model->validate()) {
				
				$onidex = Onidex::model()->find('cedula=:cedula and nac=:nac',array(':cedula'=>$model->cedula,':nac'=>$_POST['Cedula']['nacionalidad']));
				if($onidex){
				
				$gmvvs = GrGmvvT::model()->findAll('ci=:cedula AND st_censado=TRUE',array(':cedula'=>$model->cedula));
				
				$psms = Censado::model()->findAll('cedula=:cedula AND st_censado=TRUE',array(':cedula'=>$model->cedula));
				
				$sigevihs = FamFamiliaBeneficiado::model()->with(array('famBeneficiado.persona','familias'))->findAll('cedula=:cedula AND st_familia_beneficiado=TRUE',array(':cedula'=>$model->cedula));
 }
			}
		}
		
		$this->render('cedula',array('model'=>$model,
									'onidex'=>$onidex,
									'gmvvs'=>$gmvvs,
									'psms'=>$psms,
									'sigevihs'=>$sigevihs,
									));

	}
	/*public function actionSearchCenso()
	{
		$usuario = UsuUsuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>Yii::app()->user->id));//realidad
		$model=BusquedaPersonaCensos::model()->findAll('nacionalidad ilike :nacionalidad AND cedula =:cedula AND (fam_clasificacion_id =6 OR fam_clasificacion_id = 7) AND st_censado = TRUE',array(':nacionalidad'=>$usuario->persona->nacionalidad,':cedula'=>(string)$usuario->persona->cedula));
		if($model){
			$this->redirect(array('search'));
		}
		else{
			$this->redirect(Yii::app()->createUrl('/'));
		}
	}*/
	public function actionCedulaCensado(){
       	Yii::import('application.modules.consulta_ben.models.BeneficiosSivihJefe');
       	Yii::import('application.modules.censo.models.BusquedaCensoPersona');
		header('Content-type: application/json');
		if(trim($_POST['cedula'])==""||trim($_POST['nac'])=="" || !is_numeric($_POST['cedula'])) {
			echo '{"nacionalidad":"ERROR","cedula":"","nombre1":"","nombre2":"","apellido1":"","apellido2":"","fecha_nac":"","sexo":"","edo_civil":""}';
			} 
		else {
				$mensajeError ='';
				$mensajeNotificacion = '';
               	/****************Busqueda en la base de datos de sivih_static los  datos de la persona***************/
               	$sql= "select * from onidex where cedula=".$_POST['cedula']." AND nac='".$_POST['nac']."'";
				$comando = Yii::app() -> onidex_db -> createCommand($sql);
				$Busqueda = $comando -> queryRow();

				$Busqueda['fecha_nac'] = (($Busqueda['fecha_nac'])?date("d/m/Y",strtotime($Busqueda['fecha_nac'])):null);
//				$Busqueda['status']=0;
//				
//				/***********Fin de la busqueda en la base de datos de sivih_static los  datos de la persona***********/
//				/***********Busqueda en la base de los beneficions de la persona***********/
//				$beneficio = BeneficiosSivihSiscavJefe::model()->count('cedula=:cedula AND nacionalidad=:nac',array(':cedula'=>$_POST['cedula'],':nac'=>$_POST['nac']));
//     			if($beneficio>0)
//     				$mensajeNotificacion.="<li>Esta persona posee un beneficio se podrá registrar pero es necesario regularizarlo</li>";
//
//				/***********Fin de la busqueda en la base de los beneficions de la persona***********/
//     				
//				/****************Busqueda en la base de datos del sistema si esta ya registrado***********************/
//				$beneficiario = FamBeneficiado::model()->with('persona')->find('cedula=:cedula AND nacionalidad=:nac',array(':cedula'=>$_POST['cedula'],':nac'=>$_POST['nac']));
//				if($beneficiario && $beneficiario->st_beneficiado == true){
//					$mensajeError.="<li>Esta cédula ya esta registrada como";
//					if ($beneficiario->jefe_familia == true){
//						$mensajeError.=" JEFE DE FAMILIA</li>";
//					}
//					else{
//						$mensajeError.=" CARGA FAMILIAR</li>";
//					}
//				}
//				/***********Fin de la busqueda en la base de datos del sistema si esta ya registrado******************/
//									  	
//			   /****************Busqueda en la base de datos del sistema si esta registrado en el esquema censo***********************/
//				$censado = BusquedaCensoPersona::model()->count('cedula=:cedula AND st_censado = TRUE',array(':cedula'=>$_POST['cedula']));
//				//$censado = 0;
//				$Busqueda['censado'] = $censado;
//				/***********Fin de la busqueda en la base de datos del sistema si esta registrado en el esquema censo******************/
//				
//			   	if($mensajeError!=''){
//					Yii::app()->user->setFlash('error', mb_strtoupper('<ul>'.$mensajeError.'</ul>',"utf-8"));
//					$Busqueda['tipo'] = 'error';
//				}
//				if($mensajeNotificacion!=''){
//					Yii::app()->user->setFlash('notice', mb_strtoupper('<ul>'.$mensajeNotificacion.'</ul>',"utf-8"));
//					$Busqueda['tipo'] = 'error';
//				}
//				$mensaje1 ='';	
//				foreach(Yii::app()->user->getFlashes() as $key => $message) {
//			         $mensaje1.=  '<div class="flash-' . $key . '" style="padding: 6px 0px 0px 0px;">' . $message . "</div>\n";
//			    }
//			    
//				$Busqueda['mensaje'] = $mensaje1;
//			   
			   
			   
			   /****************************FIN BUSQUEDA EN LA TABLA INHABILITADOS**************************************/
               echo CJSON::encode($Busqueda);

               }

               Yii::app()->end();
       }
       
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Familia the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Familia::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Familia $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='familia-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionMigracionPlan(){
		$estado = New Jefes0800('search');
		$this->layout='//layouts/column1';
		$vararray = array();
		Yii::import('application.modules.familia.models.Onidex',true);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['Jefes0800']))
		{
			$estado->attributes=$_POST['Jefes0800'];
			if(!isset($_POST['id']) ||(isset($_POST['id']) && count($_POST['id'])==0)){
				Yii::app()-> user-> setFlash('error', 'Debe de Seleccionar un Estado para realizar la migracion. Por favor verifique');
				$valido = false;
			}
			if(isset($_POST['id']) && count($_POST['id'])>0)
				$valido = true;				
			
			if($valido){
			
				$est_id=$_POST['id'];
				$this->render('carga');
				///////
				echo "<script type=\"text/javascript\">
							$('#mensajes').html('<b>Cargando los estados a Migrar:</b>');
						  </script>";
				///////
				#Conexion a Plan0800
				$connection=Yii::app()->Plan0800_db;
				$connection->active=true;
				#Conexion a Sigevih
				$conexion=str_replace (array('pgsql:',';') ,array('',' ') ,Yii::app()->Plan0800_db->connectionString).' user='.Yii::app()->Plan0800_db->username.' password='.Yii::app()->Plan0800_db->password;
				$connectionDB=Yii::app()->db;
				$connectionDB->active=true;
				$lest=count($est_id);
				for ($g=0;$g<$lest;$g++)#recorre los estados para buscar las familias
				{
					$modelBusqueda= Jefes0800::model()->findAll('migrado=\'false\'  and estado_domicilio ILIKE :estado_domicilio',array(':estado_domicilio'=>$est_id[$g]));
					$ctr=count($modelBusqueda);

					$cuantosvan = 0;
					$errores=0;
					echo "<script type=\"text/javascript\">
							jQuery('#yw0').progressbar({'value':".(0)."});
							</script>";
	                        flush();ob_flush();
					/////////////					
					//exit;
					
					if($ctr>0)  {
						echo "<script type=\"text/javascript\">
									$('#estado').html('<b>Generando Estado ".strtoupper($est_id[$g])." :</b>');
								  </script>";
						flush();ob_flush();
						//$model= Jefes0800::model()->findAll('migrado=\'false\' and estado_domicilio ILIKE :estado_domicilio',array(':estado_domicilio'=>$est_id[$g]));
						foreach ($modelBusqueda as $key=>$values){
							$item = new CargaMasivaPlan();
									
							//spl_autoload_unregister(array('YiiBase','autoload'));
							$item->nacionalidad=strtoupper($values->nac);
							$item->cedula=trim($values->cedula);
							$item->nombre1=trim($values->primer_nombre);
							$item->nombre2=trim($values->segundo_nombre);
							$item->apellido1=trim($values->primer_apellido);
							$item->apellido2=trim($values->segundo_apellido);
							$item->fe_nac=trim($values->fecha_nacimiento);
							$item->sexo=strtoupper(trim($values->sexo));							
							$item->e_mail=trim($values->e_mail);
							$item->fe_nac =date('d/m/Y',strtotime($item->fe_nac));
							if(!is_numeric($item->cedula))
								$item->cedula= NULL;

							$nacionalidadOriginal=strtoupper($values->nac);
							$onidex = Onidex::model()->findByAttributes(array('cedula'=>$item->cedula,'nac'=>$item->nacionalidad));
							if($onidex){
								$item->nacionalidad=$onidex->nac;
								//$item->cedula=$onidex->cedula;
								$item->nombre1=$onidex->nombre1;
								$item->nombre2=$onidex->nombre2;
								$item->apellido1=$onidex->apellido1;
								$item->apellido2=$onidex->apellido2;
								
								$values->nac=$onidex->nac;
								//$values->cedula=$onidex->cedula;
								$values->primer_nombre=$onidex->nombre1;
								$values->segundo_nombre=$onidex->nombre2;
								$values->primer_apellido=$onidex->apellido1;
								$values->segundo_apellido=$onidex->apellido2;
								if($onidex->fecha_nac){
									$item->fe_nac=date('d/m/Y',strtotime($onidex->fecha_nac));
									$values->fecha_nacimiento=date('d/m/Y',strtotime($onidex->fecha_nac));
								}
								else{
									$item->fe_nac=NULL;
									$values->fecha_nacimiento=NULL;
								}									
							}
							else{
								$nacio = $item->nacionalidad;
								
								if($nacio=='E')
									$nacio='V';
								else 
									$nacio='E';
									
								$onidex = Onidex::model()->findByAttributes(array('cedula'=>$item->cedula,'nac'=>$nacio));
								if($onidex){
									$item->nacionalidad=$onidex->nac;
									//$item->cedula=$onidex->cedula;
									$item->nombre1=$onidex->nombre1;
									$item->nombre2=$onidex->nombre2;
									$item->apellido1=$onidex->apellido1;
									$item->apellido2=$onidex->apellido2;
									
									$values->nac=$onidex->nac;
									//$values->cedula=$onidex->cedula;
									$values->primer_nombre=$onidex->nombre1;
									$values->segundo_nombre=$onidex->nombre2;
									$values->primer_apellido=$onidex->apellido1;
									$values->segundo_apellido=$onidex->apellido2;
									if($onidex->fecha_nac){
										$item->fe_nac=date('d/m/Y',strtotime($onidex->fecha_nac));
										$values->fecha_nacimiento=date('d/m/Y',strtotime($onidex->fecha_nac));
									}
									else{
										$item->fe_nac=NULL;
										$values->fecha_nacimiento=NULL;
									}									
							}
							}
							
							$correo = Correo::model()->find('cedula=:cedula',array(':cedula'=>$item->cedula));
							if($correo){
								$item->e_mail=$correo->correo;
							}	
							$item->nu_telefono1 = preg_replace('#[/( /) \s,.-]+#','', trim($values->telefono_fijo));
							$item->nu_telefono2 = preg_replace('#[/( /) \s,.-]+#','', trim($values->celular));
							
							if(!$item->nu_telefono1 && !$item->nu_telefono2){
								
								$item->scenario='ambTelef';
							}
							else{
								if($item->nu_telefono1 && $item->nu_telefono1!=0 ){
									$item->scenario='telef1';
								}
								if($item->nu_telefono2 && $item->nu_telefono2!=0 ){
									$item->scenario='telef12';
								}
							}
							$item->ingreso_mensual= trim($values->ingreso_grupo);
							if($item->ingreso_mensual<0)
								$item->ingreso_mensual=$item->ingreso_mensual*(-1);
								
							if($item->ingreso_mensual && is_numeric($item->ingreso_mensual))
								$item->ingreso_mensual=number_format($item->ingreso_mensual,2,",", ".");
							else 
								$item->ingreso_mensual= '0,00';
								
								
							$item->n_grupo=trim($values->miembros_grupo_fam);
							if(!$item->n_grupo)
								$item->n_grupo=1;
								
							$item->n_discapacitado=trim($values->cantidad_discapacitados);
							if(!$item->n_discapacitado)
								$item->n_discapacitado=0;
								
							$item->fe_registro = trim($values->fecha_registro);
							
							if(!$item->fe_registro || $item->fe_registro =='01/01/1970' || $item->fe_registro ==' ')
								$item->fe_registro =  date('d/m/Y');

							$item->sector = (is_numeric(trim($values->sector)))?$values->sector:NULL;
							$item->estadoO=trim($values->estado_domicilio);
							$item->municipioO=trim($values->municipio_domicilio);
							$item->parroquiaO=trim($values->parroquia_domicilio);
							$item->condicion_vivienda_id=trim($values->tenencia_vivienda);
							$item->calidad_vivienda_id=trim($values->condicion_vivienda);
							
							$item->estadoD=trim($values->estado_solicitud);
							$item->municipioD=trim($values->municipio_solicitud);
							$item->parroquiaD=trim($values->parroquia_solicitud);
							$item->tipo_solicitud_id=str_replace(',',".",trim($values->programa));

							$estadoO = GeoEstado::model()->find("to_ascii(LOWER(nombre),'latin1') ILIKE to_ascii(LOWER(:nombre),'latin1')",array(':nombre'=>strtolower(trim($item->estadoO))));
							if(!$estadoO){
								$item->estadoO= null;
							}
							else{
							$item->estadoO=$estadoO->geo_estado_id;}
			
							$municipioO = GeoMunicipio::model()->find("to_ascii(LOWER(nombre),'latin1') ILIKE to_ascii(LOWER(:nombre),'latin1') and geo_estado_id=:id",array(':nombre'=>strtolower(trim($item->municipioO)),':id'=>$item->estadoO));
							if(!$municipioO){	
								$item->municipioO= null;
							}
							else{
								$item->municipioO=$municipioO->geo_municipio_id;}
			
							$parroquiaO = GeoParroquia::model()->find('to_ascii(LOWER(nombre),\'latin1\') ILIKE to_ascii(LOWER(:nombre),\'latin1\') and geo_municipio_id=:id',array(':nombre'=>strtolower(trim($item->parroquiaO)),':id'=>$item->municipioO));
							if(!$parroquiaO){
								$item->parroquiaO=null;
							}
							else{
								$item->parroquiaO=$parroquiaO->geo_parroquia_id;}
							
							$cond = FamCondicionViviendaOrigen::model()->find("to_ascii(LOWER(nombre),'latin1') ILIKE to_ascii(LOWER(:nombre),'latin1')",array(':nombre'=>strtolower(trim($item->condicion_vivienda_id))));
							if(!$cond){
								$item->condicion_vivienda_id=NULL;
							}
							else
								$item->condicion_vivienda_id= $cond->condicion_vivienda_id;
								
							$calid = FamCalidadViviendaOrigen::model()->find("to_ascii(LOWER(nombre),'latin1') ILIKE to_ascii(LOWER(:nombre),'latin1')",array(':nombre'=>strtolower(trim($item->calidad_vivienda_id))));
							if(!$calid){
								$item->calidad_vivienda_id=NULL;
							}
							else
								$item->calidad_vivienda_id= $calid->calidad_vivienda_id;
	
							$estadoD = GeoEstado::model()->find("to_ascii(LOWER(nombre),'latin1') ILIKE to_ascii(LOWER(:nombre),'latin1')",array(':nombre'=>strtolower(trim($item->estadoD))));
							if(!$estadoD){
								$item->estadoD= null;
							}
							else{
								$item->estadoD=$estadoD->geo_estado_id;}
							
							$municipioD = GeoMunicipio::model()->find("to_ascii(LOWER(nombre),'latin1') ILIKE to_ascii(LOWER(:nombre),'latin1') and geo_estado_id=:id",array(':nombre'=>strtolower(trim($item->municipioD)),':id'=>$item->estadoD));
							if(!$municipioD){
								$item->municipioD= null;
							}
							else{
								$item->municipioD=$municipioD->geo_municipio_id;}
							
							$parroquiaD = GeoParroquia::model()->find("to_ascii(LOWER(nombre),'latin1') ILIKE to_ascii(LOWER(:nombre),'latin1') and geo_municipio_id=:id",array(':nombre'=>strtolower(trim($item->parroquiaD)),':id'=>$item->municipioD));
							if(!$parroquiaD){
								$item->parroquiaD=null;
							}
							else{
								$item->parroquiaD=$parroquiaD->geo_parroquia_id;}
										
							if(strtoupper($item->tipo_solicitud_id)=='OTROS')
								$item->tipo_solicitud_id='OTRO';
							$tpSolicitud= FamTipoSolicitud::model()->find("to_ascii(nombre,'latin1') ILIKE to_ascii(:nombre,'latin1')",array(':nombre'=>trim($item->tipo_solicitud_id)));
							if(!$tpSolicitud){
								$item->tipo_solicitud_id=NULL;
							}
							else
								$item->tipo_solicitud_id= $tpSolicitud->tipo_solicitud_id;
								
							if($item->validate()){
								//if($item->cedula=='81477518')
									//echo "<pre>"; print_r($item->attributes);exit;
									
								////////
							echo "<script type=\"text/javascript\">
									$('#mensajes').html('<b>Migrando la familia ".($key+1)." de ".$ctr." :</b>');
								  </script>";
								  flush();ob_flush();flush();ob_flush();
							////////
							////////
							echo "<script type=\"text/javascript\">
									//$('#estado').html('<b>Generando Estado ".strtoupper($est_id[$g])." :</b>');
								  </script>";
								  flush();ob_flush();flush();ob_flush();
							////////
									$transaction = Yii::app()->db->beginTransaction();
									try {
									/*Yii::import('application.modules.censo.models.Censado',true);
									Yii::import('application.modules.censo.models.Familia',true);
									Yii::import('application.modules.censo.models.Direccion',true);
									Yii::import('application.modules.familia.models.FamBeneficiadoClasificacion',true);*/
									$fam_clasificacion_id =6;
									$modelPersona = Censado::model()->with('familia')->find('cedula =:cedula and fam_clasificacion_id =:id',array(':cedula'=>$item->cedula,':id'=>$fam_clasificacion_id));
									if(!$modelPersona){
							
										$DireccionOrigen = new Direccion();
										$DireccionOrigen->estado_id=$item->estadoO;
										$DireccionOrigen->municipio_id=$item->municipioO;
										$DireccionOrigen->parroquia_id=$item->parroquiaO;
										$DireccionOrigen->direccion=" ";
										$DireccionOrigen->condicion_vivienda_id=$item->condicion_vivienda_id;
										$DireccionOrigen->calidad_vivienda_id=$item->calidad_vivienda_id;
										$DireccionOrigen->usu_aud_id=Yii::app()->user->id;
										$DireccionOrigen->save();
											
										$direccionDest = new Direccion();
										$direccionDest->estado_id=$item->estadoD;
										$direccionDest->municipio_id=$item->municipioD;
										$direccionDest->parroquia_id=$item->parroquiaD;
										$direccionDest->tipo_solicitud_id=$item->tipo_solicitud_id;
										$direccionDest->direccion=" ";
										$direccionDest->usu_aud_id=Yii::app()->user->id;
										$direccionDest->save();
			
										$familia = New Familia();
										$familia->st_familia=1;
										$familia->fe_registro= $item->fe_registro;
										$familia->n_grupo= $item->n_grupo;
										$familia->n_discapacitado= $item->n_discapacitado;
										$familia->fam_clasificacion_id = $fam_clasificacion_id;
										$familia->direccion_id = $DireccionOrigen->direccion_id;
										$familia->direccion_destino =$direccionDest->direccion_id;
										$familia->usu_aud_id=Yii::app()->user->id;
										$familia->save();
										
										$modelPersona=new Censado('0800');
										$modelPersona->cedula=trim($item->cedula);
										$modelPersona->nacionalidad=trim($nacionalidadOriginal);
										$modelPersona->nombre1=trim($item->nombre1);
										$modelPersona->nombre2=trim($item->nombre2);
										$modelPersona->apellido1=trim($item->apellido1);
										$modelPersona->apellido2=trim($item->apellido2);
										$modelPersona->fe_nac=trim($item->fe_nac);
										$modelPersona->sexo=trim($item->sexo);
										$modelPersona->e_mail=trim($item->e_mail);
										$modelPersona->ingreso_mensual=$item->ingreso_mensual;
										$modelPersona->parentesco_id = 12;
										$modelPersona->telefono_1= $item->nu_telefono1;
										$modelPersona->telefono_2= $item->nu_telefono1;
										$modelPersona->usu_aud_id=Yii::app()->user->id;
										$modelPersona->familia_id = $familia->familia_id;
										$modelPersona->st_censado = 1;
										$modelPersona->save();
										
										/*if($modelPersona->cedula !=''){
											$model = FamBeneficiado::model()->with('persona')->find('cedula=:cedula and st_beneficiado = TRUE',array(':cedula'=>$modelPersona->cedula));
											if($model){
												
												Familia::model()->updateAll(array('st_familia'=>false,'usu_aud_id'=>Yii::app()->user->id), 'familia_id=:familia_id',array(':familia_id'=>$modelPersona->familia_id));
												Censado::model()->updateAll(array('st_censado'=>false,'usu_aud_id'=>Yii::app()->user->id), 'cedula=:cedula',array(':cedula'=>$modelPersona->cedula));
												$benClas = FamBeneficiadoClasificacion::model()->find('beneficiado_id =:id AND fam_clasificacion_id=:id2',
																					array(':id'=>$model->beneficiado_id,':id2'=>$fam_clasificacion_id))	;
												if(!$benClas){
													$clasificacionBeneficiado = NEW FamBeneficiadoClasificacion();
													$clasificacionBeneficiado->beneficiado_id = $model->beneficiado_id;
													$clasificacionBeneficiado->fam_clasificacion_id = $fam_clasificacion_id;
													$clasificacionBeneficiado->usu_aud_id=Yii::app()->user->id;
													$clasificacionBeneficiado->save();
												}	
											}
										}*/
									}
									/************************************telefono******************************/
									

									Jefes0800::model()->updateAll(array('migrado'=>'true','mostrar'=>'false'),'cedula=:ced and nac=:nac',array(':ced'=>(string)$item->cedula,':nac'=>$nacionalidadOriginal));
									echo "<script type=\"text/javascript\">
												$('#archivos').html('<b>Migrado de la familia:</b>');
											  </script>";
											  flush();ob_flush();flush();ob_flush();
									//Yii::app()->user->setFlash('success', "Carga Masiva Registrada con Ã‰xito. Total Registro Agregador/Modificados: ".$i);
									$transaction->commit();
									//echo $transaction->errorCode();
								}
						
								catch (Exception $e) {
										//$e->getMessage()
									throw new CHttpException(400,$e->getLine().''.$e->getMessage());
									$transaction->rollBack();
								}

									////////
						echo "<script type=\"text/javascript\">
							  $('#mensajes').html('Se Migro la familia Exitosamente');
								  </script>";
								  flush();ob_flush();flush();ob_flush();
						////////
								
						}
						else{
								$errores=$errores+1;
								$html="";
								foreach ($item->getErrors() as $key=>$val){
									foreach ($val as $k =>$v){									
										$html.=$v."\n";
									}
								}

								echo "<script type=\"text/javascript\">
								  $('#mensajesError').html('Familias Con Errores, No pueden cargarse: ".$errores."');
									  </script>";
								  flush();ob_flush();flush();ob_flush();
								
								$values->mostrar="true";
								$values->mensaje=$html;
								$values->migrado="false";
								$values->save();
								
								//Jefes0800::model()->updateAll(array('mensaje'=>$html,'st_mostrar'=>true,'st_migrado'=>false),'cedula=:ced and nac=:nac',array(':ced'=>(string)$values->cedula,':nac'=>$values->nac));
							}
 						echo "<script type=\"text/javascript\">
								jQuery('#yw0').progressbar({'value':".((($cuantosvan)/$ctr)*100)."});
								</script>";
		                        flush();ob_flush();
						///////
						$cuantosvan=$cuantosvan+1;
							
						}							
					}
				}	
				exit;				
			}
		}
		
		$this->render('MigracionPlan',array('estado'=>$estado,
				'vararray'=>$vararray,
		));
		
	}
	
	public function actionMigracion($id,$censo){
		Yii::import('application.modules.familia.models.Pais',true);
		Yii::import('application.modules.familia.models.PerPersona',true);
		Yii::import('application.modules.familia.models.FamEdoCivil',true);
		Yii::import('application.modules.familia.models.ClasificacionIngreso',true);
		Yii::import('application.modules.familia.models.FamMision',true);
		
		$parentescoArray=CHtml::listData(FamParentesco::model()->findAll('estatus=true order by nombre'),'parentesco_id', 'nombre');
		$gradoArray=CHtml::listData(FamGradoInstruccion::model()->findAll('estatus=true order by nombre'),'grado_instruccion_id', 'nombre');
		$ocupacionArray=CHtml::listData(FamOcupacion::model()->findAll('estatus=true order by nombre'),'ocupacion_id', 'nombre');
		$discapacidadArray= CHtml::listData(FamEnfermedad::model()->findAll('estatus=true order by nombre'),'enfermedad_id', 'nombre');
		$estadoCivilArray=CHtml::listData(FamEdoCivil::model()->findAll('st_edo_civil=true'),'edo_civil_id', 'nombre');
		$pais= CHtml::listData(Pais::model()->findAll(array('order'=>'despai')),'id_pais', 'despai');
		$institucion = CHtml::listData(PerTrabajo::model()->findAll('tp_sector=3 ORDER BY nb_trabajo'), 'trabajo_id', 'nb_trabajo');
		
		$clasificacion= CHtml::listData(ClasificacionIngreso::model()->findAll('st_clasificacion=true order by nb_clasificacion asc'),'id_clasificacion', 'nb_clasificacion');
		$destinoArray2= CHtml::listData(GeoCiudad::model()->findAll(array('select'=>'lower(nombre) as nombre,geo_ciudad_id ','order'=>'nombre')),'geo_ciudad_id', 'nombre');
		
// 		instancia de modelos
		$direccionOrigen = New Direccion();
		$direccionDestino = New Direccion();
		$modelFamilia [] = NEW Censado();
		$cedulasUnicaFamilia = array();
		$familia = new Familia();
		
		if(!isset($_POST['Censado'])){
//		Buscar la familia seleccionada en la vista, para migrar
		$modelFamiliaCenso = BusquedaPersonaCensos::model()->findAll(array('condition'=>'familia_id=:id AND censo=:censo AND (migrado IS NULL OR migrado =  FALSE) AND (st_censado =  TRUE)',
																'params'=>array(':id'=>$id,':censo'=>(($censo)?TRUE:FALSE)),
																'order'=>'parentesco_id in(12, 1, 3, 4) DESC'));
//		Si la consulta no trae nada redirecciona al search nuevamente
		if(count($modelFamiliaCenso)==0)
			$this->redirect(array('search'));
			
		unset($modelFamilia);
		
// 		$model = NEW BusquedaCensoPersona();
		
		$jefe = FALSE;
		
		foreach ($modelFamiliaCenso AS $key => $value){
			$censado = NEW Censado();
			$censado->attributes = $value->attributes;
			
			$censado->fe_nac = date('d/m/Y',strtotime($censado->fe_nac));
			$censado->ingreso_mensual=number_format($censado->ingreso_mensual,2,",", ".");
			
			if($censado->jefe || $censado->parentesco_id ==12){
				$censado->jefe = TRUE;
				$censado->scenario ='jefeMigrar';
			}
			else{
				$censado->scenario ='cargaMigrar';
			}
			if($censado->nacionalidad =='S'){
				$censado->nacionalidad ='V';
				$censado->cedula = NULL;
			}
					
			$censado->validate();
			if($censado->cedula !=""){
				$existe=array_search($censado->cedula, $cedulasUnicaFamilia);
				if($existe==false){
						$cedulasUnicaFamilia[]=($censado->cedula);
						$modelFamilia[]= $censado;
				}
			}
			else 
				$modelFamilia[]= $censado;
			
				
		}
		unset($cedulasUnicaFamilia);
		unset($modelFamiliaCenso);
		}
//		Busca las direcciones "origen, destino"
		if($censo){
			$familia =Familia::model()->find('familia_id=:id',array(':id'=>(int)$id));
			
//			if($familia->direccion)
//				$direccionOrigen = $familia->direccion;
//			else{
// 			$direccionOrigen = NEW Direccion('origen');
			$direccionOrigen->scenario = 'origen';
			$direccionOrigen->estado_id = ($familia->direccion)?$familia->direccion->estado_id:NULL;
			$direccionOrigen->municipio_id = ($familia->direccion)?$familia->direccion->municipio_id:NULL;
			$direccionOrigen->parroquia_id = ($familia->direccion)?$familia->direccion->parroquia_id:NULL;
			$direccionOrigen->direccion = ($familia->direccion)?$familia->direccion->direccion:NULL;
			$direccionOrigen->condicion_vivienda_id = (($familia->direccion)?$familia->direccion->condicion_vivienda_id:NULL);
			$direccionOrigen->calidad_vivienda_id = (($familia->direccion)?$familia->direccion->calidad_vivienda_id:NULL);
//			}
			
			if($familia->fam_clasificacion_id ==6)
				$direccionDestino ->scenario = '0800';
			else 
				$direccionDestino ->scenario = 'gmvo';
// 			$direccionDestino = New Direccion($direccionDestino);
			if($familia->direccionDestino){
					$direccionDestino->estado_id = $familia->direccionDestino->estado_id;
					$direccionDestino->municipio_id = $familia->direccionDestino->municipio_id;
					$direccionDestino->parroquia_id = $familia->direccionDestino->parroquia_id;
					$direccionDestino->ciudad_destino_1 = ((isset($familia->direccionDestino->ciudad_destino_1))?$familia->direccionDestino->ciudad_destino_1:NULL);
					$direccionDestino->ciudad_destino_2 = ((isset($familia->direccionDestino->ciudad_destino_2))?$familia->direccionDestino->ciudad_destino_2:NULL);
					$direccionDestino->tipo_solicitud_id = ((isset($familia->direccionDestino->tipo_solicitud_id))?$familia->direccionDestino->tipo_solicitud_id:NULL);
			} 
			
		}
		else{
			$familia = GrGmvvT::model()->find('id=:id AND parentesco_id=12',array(':id'=>$id));
			$direccionOrigen->scenario='origen';
			$direccionDestino->scenario='gmvv';
			if($familia){
				$direccionOrigen->estado_id = $familia->geo_estado_id;
				$direccionOrigen->municipio_id = $familia->geo_municipio_id;
				$direccionOrigen->parroquia_id = $familia->geo_parroquia_id;
				$direccionOrigen->direccion = $familia->dir_urbanizacion_barrio.' '.$familia->dir_av_calle_esq.' '.$familia->dir_edf_casa_num;
				$direccionOrigen->condicion_vivienda_id = $familia->titularidad_id;
				$direccionOrigen->calidad_vivienda_id = NULL;
				
				
				$direccionDestino->ciudad_destino_1 = $familia->destino1_id;
				$direccionDestino->ciudad_destino_2 = $familia->destino2_id;	
			}
		}
		
// 		$model->unsetAttributes();  // clear any default values
// 		if(isset($_GET['BusquedaCensoPersona']))
// 			$model->attributes=$_GET['BusquedaCensoPersona'];
				
		if(isset($_POST['Censado'])){
			$valido = TRUE;
			$contJefe = 0;
			unset($cedulasUnicaRegistro);
			unset($modelFamilia);
			$cedulasUnicaRegistro=array();
			$direccionOrigen->attributes = $_POST['Direccion'][0];
			$direccionDestino->attributes = $_POST['Direccion'][1];
			foreach($_POST['Censado'] as $i=>$data) {
				$item = new Censado();
				if(isset($data)) {
					$item->attributes=$data;
					if($item->jefe){
						$item->scenario ='jefeMigrar';
						$item->parentesco_id =12;
					}
					else{
						$item->scenario ='cargaMigrar';
					}

					$valido=$item->validate()&&$valido;
					
					if($item->jefe || $item->parentesco_id ==12){
						if($contJefe==0){
							$item->jefe = TRUE;
							$contJefe++;
						}
						else{
							$contJefe++;
							$valid=false;
							$item->addError('parentesco_id', 'Parentesco debe ser diferente');
						}
					}
					
					if($item->cedula !=''){
						$existe=array_search($item->cedula, $cedulasUnicaRegistro);
						if($existe!==false){
							$valid=false;
							$item->addError('cedula', 'Cédula Repetida en la Familia');
						}
						else
							$cedulasUnicaRegistro[]=($item->cedula);
					}
						
					$modelFamilia[] = $item;
				}
			}
			//echo "<PRE>";print_r($modelFamilia[0]->getErrors());exit;
			
			if($contJefe==0){
				Yii::app()-> user-> setFlash('error', 'Debe de registrar un JEFE(A) FAMILIA');
				$valido=false;
			}elseif ($contJefe>1){
				Yii::app()-> user-> setFlash('error', 'Solo puede registrar un JEFE(A) FAMILIA');
				$valido=false;
			}
//			$direccionOrigen->scenario='origen';
			$valido = $direccionOrigen->validate()&& $valido;
			$valido = $direccionDestino->validate()&& $valido;
			if($valido){
				$transaction = Yii::app()->db->beginTransaction();
				try {
					Yii::import('application.modules.familia.models.FamFamilia',true);
					Yii::import('application.modules.familia.models.PerTelefono',true);
					Yii::import('application.modules.familia.models.PerInhabilitados',true);
					Yii::import('application.modules.familia.models.FamFamiliaDireccionOrigen',true);
					Yii::import('application.modules.familia.models.FamCiudadDestino',true);
					$i=0;
					//////////// Registrar familia////////////
					$familiaPublic = New FamFamilia();
					if($censo){
						$now = (string)microtime();
						$now = explode(' ', $now);
						$mm = explode('.', $now[0]);
						$mm = $mm[1];
						$now = $now[1];
						$ms = $now % 180;
						$ms = $ms < 10 ? "$ms" : $ms;
													 	//asigna el codigo hexadecimal compueta por la d/m/a h/m/s/ms
						$_codFamilia = dechex(date("j")).dechex(date("n")).dechex(date("y")).
										dechex(date("H")).dechex(date("i")).dechex(date("s")).dechex($ms);
						$familiaPublic->co_familia= $_codFamilia;
//						$familiaPublic->fe_registro= $familia->fe_registro;
						
					}
					else{
						$familiaPublic->co_familia= $familia->serial;
						
					}
					if($familia->fam_clasificacion_id==6){
						$familiaPublic->reg_0800 = true;
					}
					elseif ($familia->fam_clasificacion_id==7){
						$familiaPublic->gmvv = true;
					}
					else{
						$familiaPublic->gmvo = true;
					}
//					echo $familia->fam_clasificacion_id;exit();
					$familiaPublic->fe_registro= $familia->fe_registro;
					$familiaPublic->fam_clasificacion_id = $familia->fam_clasificacion_id;
					$familiaPublic->procedencia = 2;
					$familiaPublic->st_familia = TRUE;
//					$familiaPublic->direccion_id = $DirOrigen->direccion_id;
//					$familiaPublic->direccion_destino = $DirDestino->direccion_id;
					$familiaPublic->usu_aud_id=Yii::app()->user->id;
					$familiaPublic->save();
					
//					registrar direccion de origen
					if($direccionOrigen->condicion_vivienda_id =='')
						$direccionOrigen->condicion_vivienda_id =NULL;
					
					if($direccionOrigen->calidad_vivienda_id =='')
						$direccionOrigen->calidad_vivienda_id =NULL;
					$DirOrigen = NEW GeoDireccion();
					$DirOrigen ->attributes = $direccionOrigen->attributes;
					$DirOrigen ->usu_aud_id=Yii::app()->user->id;
					$DirOrigen->save();
					
					$familiaDireccionOrigen = new FamFamiliaDireccionOrigen;
					$familiaDireccionOrigen->direccion_id = $DirOrigen->direccion_id;
					$familiaDireccionOrigen ->condicion_vivienda_id = $direccionOrigen->condicion_vivienda_id;
					$familiaDireccionOrigen ->condicion_vivienda_id = $direccionOrigen->calidad_vivienda_id;
					$familiaDireccionOrigen ->familia_id = $familiaPublic->familia_id;
					$familiaDireccionOrigen->save();
					
//					Direccion de solicitud
					
					$familiaDireccionDestino = NEW FamCiudadDestino();
					$familiaDireccionDestino ->attributes = $direccionDestino->attributes;
					$familiaDireccionDestino ->geo_ciudad_id = $direccionDestino->ciudad_destino_1;
					$familiaDireccionDestino ->familia_id = $familiaPublic->familia_id;
					$familiaDireccionDestino ->usu_aud_id=Yii::app()->user->id;
					$familiaDireccionDestino->save();
					
					if(!$censo){
						$familiaDireccionDestino2 = NEW FamCiudadDestino();
						$familiaDireccionDestino2 ->attributes = $direccionDestino->attributes;
						$familiaDireccionDestino2 ->geo_ciudad_id = $direccionDestino->ciudad_destino_2;
						$familiaDireccionDestino2 ->usu_aud_id=Yii::app()->user->id;
						$familiaDireccionDestino2->save();
					}
					
					foreach ($modelFamilia as $key => $value){
						if(!is_numeric($value->cedula))
							$value->cedula = NULL;
						
						$persona=PerPersona::model()->find('cedula =:cedula',array(':cedula'=>$value->cedula));
						if(!$persona)
							$persona=new PerPersona;
						$persona->nacionalidad=$value->nacionalidad;
						$persona->cedula=$value->cedula;
						$persona->nombre1=$value->nombre1;
						$persona->nombre2=$value->nombre2;
						$persona->apellido1=$value->apellido1;
						$persona->apellido2=$value->apellido2;
						$persona->fe_nac=$value->fe_nac;
						$persona->sexo=$value->sexo;
					  	$persona->e_mail=$value->e_mail;
					  	$persona->id_pais=$value->id_pais;
					  	$persona->tiempo_pais=$value->tiempo_pais;
					   	$persona->usu_aud_id=Yii::app()->user->id;
						$persona->save();
//						if(!$persona->save()){  
//							echo "<PRE>";print_r($persona->attributes);print_r($persona->getErrors());exit;										   	
//						}
						
						$perBeneficio= FamBeneficiado::model()->find('persona_id=:id',array(':id'=>$persona->persona_id));
						if(isset($persona->persona_id)){
							if(!$perBeneficio)
								$perBeneficio = NEW FamBeneficiado();

							$perBeneficio->persona_id=$persona->persona_id;
//							$perBeneficio->familia_id = $familiaPublic->familia_id;
//									$perBeneficio-> st_beneficiado = TRUE;					
							$perBeneficio-> enfermedad_id = $value->enfermedad_id;
							$perBeneficio->grado_instruccion_id=  $value->grado_instruccion_id;
							$perBeneficio->estado_embarazo=  $value->estado_embarazo;
							$perBeneficio->estudia=  $value->estudia;
							$perBeneficio->ingreso_mensual = $value->ingreso_mensual;
							$perBeneficio->insc_faov = $value->insc_faov;
							$perBeneficio->trabaja = $value->trabaja;	
							$perBeneficio->edo_civil_id = $value->edo_civil_id;
							$perBeneficio->trabajo_id = $value->trabajo_id;
							$perBeneficio->ocupacion_id= $value->ocupacion_id;
							$perBeneficio-> id_clasificacion = $value->id_clasificacion;
							$perBeneficio->ocupacion_otro = $value->ocupacion_otro;
							$perBeneficio->usu_aud_id=Yii::app()->user->id;
							
							if($perBeneficio->id_clasificacion =='')
								$perBeneficio->id_clasificacion = NULL;

							if($perBeneficio->trabajo_id =='')
								$perBeneficio->trabajo_id = NULL;

							if($perBeneficio->id_clasificacion =='')
								$perBeneficio->id_clasificacion = NULL;
								
								$perBeneficio->save();
							if(isset($perBeneficio -> beneficiado_id)){
								$modelFamiliaBeneficiado = new FamFamiliaBeneficiado();
								$modelFamiliaBeneficiado ->beneficiado_id = $perBeneficio -> beneficiado_id;
								$modelFamiliaBeneficiado ->familia_id = $familiaPublic->familia_id;
								if($value->jefe)
									$modelFamiliaBeneficiado-> jefe_familia = 1;
								else 
									$modelFamiliaBeneficiado-> jefe_familia = 0;
								$modelFamiliaBeneficiado->usu_aud_id=Yii::app()->user->id;
								$modelFamiliaBeneficiado-> parentesco_id= $value-> parentesco_id;
								$modelFamiliaBeneficiado-> st_familia_beneficiado = TRUE;	
								$modelFamiliaBeneficiado->save();
							}
						}

						PerTelefono::model()->updateAll(array('usu_aud_id'=>NULL),'persona_id=:id',array(':id'=>$persona->persona_id));
						
						$tel = PerTelefono::model()->findByAttributes(array('persona_id'=>$persona->persona_id,'nu_telefono'=> $value->telefono_1));
						if(!$tel){
							$telPersonal = new PerTelefono('jefe');
							$telPersonal->persona_id = $persona->persona_id;
							$telPersonal->nu_telefono= $value->telefono_1;
							$telPersonal->usu_aud_id=Yii::app()->user->id;
							$telPersonal->save();
						}
						$tel = PerTelefono::model()->findByAttributes(array('persona_id'=>$persona->persona_id,'nu_telefono'=> $value->telefono_2));
						if(!$tel){
							$telPersonal = new PerTelefono('jefe');
							$telPersonal->persona_id = $persona->persona_id;
							$telPersonal->nu_telefono= $value->telefono_2;
							$telPersonal->usu_aud_id=Yii::app()->user->id;
							$telPersonal->save();
						}
						PerTelefono::model()->deleteAll('persona_id=:id AND usu_aud_id IS NULL',array(':id'=>$persona->persona_id));
						//////// Eliminacion de los censos
						if($persona->cedula !=""){
							$modelPersonaCensado = BusquedaPersonaCensos::model()->findAll(array('condition'=>'nacionalidad ILIKE :nacionalidad AND cedula=:cedula',
																'params'=>array('nacionalidad'=>(String)strtoupper($value->nacionalidad),':cedula'=>(String)$value->cedula)));
							foreach ($modelPersonaCensado AS $keyCensado => $valueCensado){
//								0800 0 GMVVO
								if($valueCensado->fam_clasificacion_id !=7){
									if($valueCensado->fam_clasificacion_id ==7)
										$modelFamiliaBeneficiado->reg_plan = TRUE;
//								gmvvo
									else
										$modelFamiliaBeneficiado->reg_gmvo = TRUE;
										
									if($valueCensado->parentesco_id==12){
										Censado::model()->updateAll(array('migrado'=>TRUE,/*'st_censado'=>FALSE,*/'usu_aud_id'=>Yii::app()->user->id), 
																			'familia_id=:familia_id',array(':familia_id'=>$valueCensado->familia_id));
										Familia::model()->updateAll(array('st_familia'=>FALSE,'usu_aud_id'=>Yii::app()->user->id), 
																			'familia_id=:familia_id',array(':familia_id'=>$valueCensado->familia_id));
									}
									else{
										Censado::model()->updateAll(array('migrado'=>TRUE,/*'st_censado'=>FALSE,*/'usu_aud_id'=>Yii::app()->user->id), 'censado_id =:censado_id',
											array(':censado_id'=>$value->censado_id));
							
									}
								}
//								gmvv
								else{
									$modelFamiliaBeneficiado->reg_gmvv = TRUE;
	//								si el censado es jefe inactivo la familia dnd aparece
									if($valueCensado->parentesco_id==12){
										GrGmvvT::model()->updateAll(array('migrado'=>TRUE,/*'st_censado'=>FALSE,*/'usu_aud_id'=>Yii::app()->user->id), 
																			'familia_id=:familia_id',array(':familia_id'=>$valueCensado->familia_id));
									}
									else{
										GrGmvvT::model()->updateAll(array('migrado'=>TRUE,/*'st_censado'=>FALSE,*/'usu_aud_id'=>Yii::app()->user->id), 
																			'id=:id',array(':id'=>$valueCensado->censado_id));
									}
								}
							}
						}
						elseif ($value->censado_id){
								if($familia->fam_clasificacion_id ==6)
									$modelFamiliaBeneficiado->reg_plan = TRUE;
								elseif ($familia->fam_clasificacion_id ==7)
									$modelFamiliaBeneficiado->reg_gmvv = TRUE;
								else
									$modelFamiliaBeneficiado->reg_gmvo = TRUE;
						}	
						
						$modelFamiliaBeneficiado->save();
						$i++;
					}
						$transaction->commit();
						
						Yii::app()->user->setFlash('success', "Migracion realizada con Éxito. Total Registro: ".$i);
						$this->redirect(array('/familia/FamFamilia/familiaUpdate','id'=>$familiaPublic->familia_id));
					}
					catch (Exception $e){
						throw new CHttpException(400,$e->getMessage());
						$transaction->rollBack();
					}
					exit;
 				}
		}/*echo "<pre>";
                                print_r($modelFamilia[0]->attributes);*/
		$this->render('migracion',array( 
							'familiaId'=>$id, 'censo' =>$censo,
							'modelFamilia'=>$modelFamilia,
							'familia'=>$familia,
							'parentescoArray'=>$parentescoArray,
							'gradoArray'=>$gradoArray,
							'ocupacionArray'=>$ocupacionArray,
							'discapacidadArray'=>$discapacidadArray,
							'estadoCivilArray'=>$estadoCivilArray,
							'pais'=>$pais,
							'institucion'=>$institucion,
							'clasificacion'=>$clasificacion,
							'direccionOrigen' =>$direccionOrigen,
							'direccionDestino'=>$direccionDestino,
							));
	
		
	}
        
	public function actionMigracion0800($id,$censo){
		Yii::import('application.modules.familia.models.Pais',true);
		Yii::import('application.modules.familia.models.PerPersona',true);
		Yii::import('application.modules.familia.models.FamEdoCivil',true);
		Yii::import('application.modules.familia.models.ClasificacionIngreso',true);
		Yii::import('application.modules.familia.models.FamMision',true);
		
		$parentescoArray=CHtml::listData(FamParentesco::model()->findAll('estatus=true order by nombre'),'parentesco_id', 'nombre');
		$gradoArray=CHtml::listData(FamGradoInstruccion::model()->findAll('estatus=true order by nombre'),'grado_instruccion_id', 'nombre');
		$ocupacionArray=CHtml::listData(FamOcupacion::model()->findAll('estatus=true order by nombre'),'ocupacion_id', 'nombre');
		$discapacidadArray= CHtml::listData(FamEnfermedad::model()->findAll('estatus=true order by nombre'),'enfermedad_id', 'nombre');
		$estadoCivilArray=CHtml::listData(FamEdoCivil::model()->findAll('st_edo_civil=true'),'edo_civil_id', 'nombre');
		$pais= CHtml::listData(Pais::model()->findAll(array('order'=>'despai')),'id_pais', 'despai');
		$institucion = CHtml::listData(PerTrabajo::model()->findAll('tp_sector=3 ORDER BY nb_trabajo'), 'trabajo_id', 'nb_trabajo');
		
		$clasificacion= CHtml::listData(ClasificacionIngreso::model()->findAll('st_clasificacion=true order by nb_clasificacion asc'),'id_clasificacion', 'nb_clasificacion');
		$destinoArray2= CHtml::listData(GeoCiudad::model()->findAll(array('select'=>'lower(nombre) as nombre,geo_ciudad_id ','order'=>'nombre')),'geo_ciudad_id', 'nombre');
		
// 		instancia de modelos
		$direccionOrigen = New Direccion();
		$direccionDestino = New Direccion();
		$modelFamilia [] = NEW Censado();
		$cedulasUnicaFamilia = array();
		$familia = new Familia();
		
		if(!isset($_POST['Censado'])){
//		Buscar la familia seleccionada en la vista, para migrar
		$modelFamiliaCenso = BusquedaPersonaCensos::model()->findAll(array('condition'=>'familia_id=:id AND censo=:censo AND (migrado IS NULL OR migrado =  FALSE) AND (st_censado =  TRUE)',
																'params'=>array(':id'=>$id,':censo'=>(($censo)?TRUE:FALSE)),
																'order'=>'parentesco_id in(12, 1, 3, 4) DESC'));
//		Si la consulta no trae nada redirecciona al search nuevamente
		if(count($modelFamiliaCenso)==0)
			$this->redirect(array('search'));
			
		unset($modelFamilia);
		
// 		$model = NEW BusquedaCensoPersona();
		
		$jefe = FALSE;
		
		foreach ($modelFamiliaCenso AS $key => $value){
			$censado = NEW Censado();
			$censado->attributes = $value->attributes;
			
			$censado->fe_nac = date('d/m/Y',strtotime($censado->fe_nac));
			$censado->ingreso_mensual=number_format($censado->ingreso_mensual,2,",", ".");
			
			if($censado->jefe || $censado->parentesco_id ==12){
				$censado->jefe = TRUE;
				$censado->scenario ='jefeMigrar';
			}
			else{
				$censado->scenario ='cargaMigrar';
			}
			if($censado->nacionalidad =='S'){
				$censado->nacionalidad ='V';
				$censado->cedula = NULL;
			}
					
			$censado->validate();
			if($censado->cedula !=""){
				$existe=array_search($censado->cedula, $cedulasUnicaFamilia);
				if($existe==false){
						$cedulasUnicaFamilia[]=($censado->cedula);
						$modelFamilia[]= $censado;
				}
			}
			else 
				$modelFamilia[]= $censado;
			
				
		}
		unset($cedulasUnicaFamilia);
		unset($modelFamiliaCenso);
		}
//		Busca las direcciones "origen, destino"
		if($censo){
			$familia =Familia::model()->find('familia_id=:id',array(':id'=>(int)$id));
			
//			if($familia->direccion)
//				$direccionOrigen = $familia->direccion;
//			else{
// 			$direccionOrigen = NEW Direccion('origen');
			$direccionOrigen->scenario = 'origen';
			$direccionOrigen->estado_id = ($familia->direccion)?$familia->direccion->estado_id:NULL;
			$direccionOrigen->municipio_id = ($familia->direccion)?$familia->direccion->municipio_id:NULL;
			$direccionOrigen->parroquia_id = ($familia->direccion)?$familia->direccion->parroquia_id:NULL;
			$direccionOrigen->direccion = ($familia->direccion)?$familia->direccion->direccion:NULL;
			$direccionOrigen->condicion_vivienda_id = (($familia->direccion)?$familia->direccion->condicion_vivienda_id:NULL);
			$direccionOrigen->calidad_vivienda_id = (($familia->direccion)?$familia->direccion->calidad_vivienda_id:NULL);
//			}
			
			if($familia->fam_clasificacion_id ==6)
				$direccionDestino ->scenario = '0800';
			else 
				$direccionDestino ->scenario = 'gmvo';
// 			$direccionDestino = New Direccion($direccionDestino);
			if($familia->direccionDestino){
					$direccionDestino->estado_id = $familia->direccionDestino->estado_id;
					$direccionDestino->municipio_id = $familia->direccionDestino->municipio_id;
					$direccionDestino->parroquia_id = $familia->direccionDestino->parroquia_id;
					$direccionDestino->ciudad_destino_1 = ((isset($familia->direccionDestino->ciudad_destino_1))?$familia->direccionDestino->ciudad_destino_1:NULL);
					$direccionDestino->ciudad_destino_2 = ((isset($familia->direccionDestino->ciudad_destino_2))?$familia->direccionDestino->ciudad_destino_2:NULL);
					$direccionDestino->tipo_solicitud_id = ((isset($familia->direccionDestino->tipo_solicitud_id))?$familia->direccionDestino->tipo_solicitud_id:NULL);
			} 
			
		}
		else{
			$familia = GrGmvvT::model()->find('id=:id AND parentesco_id=12',array(':id'=>$id));
			$direccionOrigen->scenario='origen';
			$direccionDestino->scenario='gmvv';
			if($familia){
				$direccionOrigen->estado_id = $familia->geo_estado_id;
				$direccionOrigen->municipio_id = $familia->geo_municipio_id;
				$direccionOrigen->parroquia_id = $familia->geo_parroquia_id;
				$direccionOrigen->direccion = $familia->dir_urbanizacion_barrio.' '.$familia->dir_av_calle_esq.' '.$familia->dir_edf_casa_num;
				$direccionOrigen->condicion_vivienda_id = $familia->titularidad_id;
				$direccionOrigen->calidad_vivienda_id = NULL;
				
				
				$direccionDestino->ciudad_destino_1 = $familia->destino1_id;
				$direccionDestino->ciudad_destino_2 = $familia->destino2_id;	
			}
		}
		
// 		$model->unsetAttributes();  // clear any default values
// 		if(isset($_GET['BusquedaCensoPersona']))
// 			$model->attributes=$_GET['BusquedaCensoPersona'];
				
		if(isset($_POST['Censado'])){
			$valido = TRUE;
			$contJefe = 0;
			unset($cedulasUnicaRegistro);
			unset($modelFamilia);
			$cedulasUnicaRegistro=array();
			$direccionOrigen->attributes = $_POST['Direccion'][0];
			$direccionDestino->attributes = $_POST['Direccion'][1];
			foreach($_POST['Censado'] as $i=>$data) {
				$item = new Censado();
				if(isset($data)) {
					$item->attributes=$data;
					if($item->jefe){
						$item->scenario ='jefeMigrar';
						$item->parentesco_id =12;
					}
					else{
						$item->scenario ='cargaMigrar';
					}

					$valido=$item->validate()&&$valido;
					
					if($item->jefe || $item->parentesco_id ==12){
						if($contJefe==0){
							$item->jefe = TRUE;
							$contJefe++;
						}
						else{
							$contJefe++;
							$valid=false;
							$item->addError('parentesco_id', 'Parentesco debe ser diferente');
						}
					}
					
					if($item->cedula !=''){
						$existe=array_search($item->cedula, $cedulasUnicaRegistro);
						if($existe!==false){
							$valid=false;
							$item->addError('cedula', 'Cédula Repetida en la Familia');
						}
						else
							$cedulasUnicaRegistro[]=($item->cedula);
					}
						
					$modelFamilia[] = $item;
				}
			}
			//echo "<PRE>";print_r($modelFamilia[0]->getErrors());exit;
			
			if($contJefe==0){
				Yii::app()-> user-> setFlash('error', 'Debe de registrar un JEFE(A) FAMILIA');
				$valido=false;
			}elseif ($contJefe>1){
				Yii::app()-> user-> setFlash('error', 'Solo puede registrar un JEFE(A) FAMILIA');
				$valido=false;
			}
//			$direccionOrigen->scenario='origen';
			$valido = $direccionOrigen->validate()&& $valido;
			$valido = $direccionDestino->validate()&& $valido;
			if($valido){
				$transaction = Yii::app()->db->beginTransaction();
				try {
					Yii::import('application.modules.familia.models.FamFamilia',true);
					Yii::import('application.modules.familia.models.PerTelefono',true);
					Yii::import('application.modules.familia.models.PerInhabilitados',true);
					Yii::import('application.modules.familia.models.FamFamiliaDireccionOrigen',true);
					Yii::import('application.modules.familia.models.FamCiudadDestino',true);
					$i=0;
					//////////// Registrar familia////////////
					$familiaPublic = New FamFamilia();
					if($censo){
						$now = (string)microtime();
						$now = explode(' ', $now);
						$mm = explode('.', $now[0]);
						$mm = $mm[1];
						$now = $now[1];
						$ms = $now % 180;
						$ms = $ms < 10 ? "$ms" : $ms;
													 	//asigna el codigo hexadecimal compueta por la d/m/a h/m/s/ms
						$_codFamilia = dechex(date("j")).dechex(date("n")).dechex(date("y")).
										dechex(date("H")).dechex(date("i")).dechex(date("s")).dechex($ms);
						$familiaPublic->co_familia= $_codFamilia;
//						$familiaPublic->fe_registro= $familia->fe_registro;
						
					}
					else{
						$familiaPublic->co_familia= $familia->serial;
						
					}
					if($familia->fam_clasificacion_id==6){
						$familiaPublic->reg_0800 = true;
					}
					elseif ($familia->fam_clasificacion_id==7){
						$familiaPublic->gmvv = true;
					}
					else{
						$familiaPublic->gmvo = true;
					}
//					echo $familia->fam_clasificacion_id;exit();
					$familiaPublic->fe_registro= $familia->fe_registro;
					$familiaPublic->fam_clasificacion_id = $familia->fam_clasificacion_id;
					$familiaPublic->procedencia = 2;
					$familiaPublic->st_familia = TRUE;
//					$familiaPublic->direccion_id = $DirOrigen->direccion_id;
//					$familiaPublic->direccion_destino = $DirDestino->direccion_id;
					$familiaPublic->usu_aud_id=Yii::app()->user->id;
					$familiaPublic->save();
					
//					registrar direccion de origen
					if($direccionOrigen->condicion_vivienda_id =='')
						$direccionOrigen->condicion_vivienda_id =NULL;
					
					if($direccionOrigen->calidad_vivienda_id =='')
						$direccionOrigen->calidad_vivienda_id =NULL;
					$DirOrigen = NEW GeoDireccion();
					$DirOrigen ->attributes = $direccionOrigen->attributes;
					$DirOrigen ->usu_aud_id=Yii::app()->user->id;
					$DirOrigen->save();
					
					$familiaDireccionOrigen = new FamFamiliaDireccionOrigen;
					$familiaDireccionOrigen->direccion_id = $DirOrigen->direccion_id;
					$familiaDireccionOrigen ->condicion_vivienda_id = $direccionOrigen->condicion_vivienda_id;
					$familiaDireccionOrigen ->condicion_vivienda_id = $direccionOrigen->calidad_vivienda_id;
					$familiaDireccionOrigen ->familia_id = $familiaPublic->familia_id;
					$familiaDireccionOrigen->save();
					
//					Direccion de solicitud
					
					$familiaDireccionDestino = NEW FamCiudadDestino();
					$familiaDireccionDestino ->attributes = $direccionDestino->attributes;
					$familiaDireccionDestino ->geo_ciudad_id = $direccionDestino->ciudad_destino_1;
					$familiaDireccionDestino ->familia_id = $familiaPublic->familia_id;
					$familiaDireccionDestino ->usu_aud_id=Yii::app()->user->id;
					$familiaDireccionDestino->save();
					
					if(!$censo){
						$familiaDireccionDestino2 = NEW FamCiudadDestino();
						$familiaDireccionDestino2 ->attributes = $direccionDestino->attributes;
						$familiaDireccionDestino2 ->geo_ciudad_id = $direccionDestino->ciudad_destino_2;
						$familiaDireccionDestino2 ->usu_aud_id=Yii::app()->user->id;
						$familiaDireccionDestino2->save();
					}
					
					foreach ($modelFamilia as $key => $value){
						if(!is_numeric($value->cedula))
							$value->cedula = NULL;
						
						$persona=PerPersona::model()->find('cedula =:cedula',array(':cedula'=>$value->cedula));
						if(!$persona)
							$persona=new PerPersona;
						$persona->nacionalidad=$value->nacionalidad;
						$persona->cedula=$value->cedula;
						$persona->nombre1=$value->nombre1;
						$persona->nombre2=$value->nombre2;
						$persona->apellido1=$value->apellido1;
						$persona->apellido2=$value->apellido2;
						$persona->fe_nac=$value->fe_nac;
						$persona->sexo=$value->sexo;
					  	$persona->e_mail=$value->e_mail;
					  	$persona->id_pais=$value->id_pais;
					  	$persona->tiempo_pais=$value->tiempo_pais;
					   	$persona->usu_aud_id=Yii::app()->user->id;
						$persona->save();
//						if(!$persona->save()){  
//							echo "<PRE>";print_r($persona->attributes);print_r($persona->getErrors());exit;										   	
//						}
						
						$perBeneficio= FamBeneficiado::model()->find('persona_id=:id',array(':id'=>$persona->persona_id));
						if(isset($persona->persona_id)){
							if(!$perBeneficio)
								$perBeneficio = NEW FamBeneficiado();

							$perBeneficio->persona_id=$persona->persona_id;
//							$perBeneficio->familia_id = $familiaPublic->familia_id;
//									$perBeneficio-> st_beneficiado = TRUE;					
							$perBeneficio-> enfermedad_id = $value->enfermedad_id;
							$perBeneficio->grado_instruccion_id=  $value->grado_instruccion_id;
							$perBeneficio->estado_embarazo=  $value->estado_embarazo;
							$perBeneficio->estudia=  $value->estudia;
							$perBeneficio->ingreso_mensual = $value->ingreso_mensual;
							$perBeneficio->insc_faov = $value->insc_faov;
							$perBeneficio->trabaja = $value->trabaja;	
							$perBeneficio->edo_civil_id = $value->edo_civil_id;
							$perBeneficio->trabajo_id = $value->trabajo_id;
							$perBeneficio->ocupacion_id= $value->ocupacion_id;
							$perBeneficio-> id_clasificacion = $value->id_clasificacion;
							$perBeneficio->ocupacion_otro = $value->ocupacion_otro;
							$perBeneficio->usu_aud_id=Yii::app()->user->id;
							
							if($perBeneficio->id_clasificacion =='')
								$perBeneficio->id_clasificacion = NULL;

							if($perBeneficio->trabajo_id =='')
								$perBeneficio->trabajo_id = NULL;

							if($perBeneficio->id_clasificacion =='')
								$perBeneficio->id_clasificacion = NULL;
								
								$perBeneficio->save();
							if(isset($perBeneficio -> beneficiado_id)){
								$modelFamiliaBeneficiado = new FamFamiliaBeneficiado();
								$modelFamiliaBeneficiado ->beneficiado_id = $perBeneficio -> beneficiado_id;
								$modelFamiliaBeneficiado ->familia_id = $familiaPublic->familia_id;
								if($value->jefe)
									$modelFamiliaBeneficiado-> jefe_familia = 1;
								else 
									$modelFamiliaBeneficiado-> jefe_familia = 0;
								$modelFamiliaBeneficiado->usu_aud_id=Yii::app()->user->id;
								$modelFamiliaBeneficiado-> parentesco_id= $value-> parentesco_id;
								$modelFamiliaBeneficiado-> st_familia_beneficiado = TRUE;	
								$modelFamiliaBeneficiado->save();
							}
						}

						PerTelefono::model()->updateAll(array('usu_aud_id'=>NULL),'persona_id=:id',array(':id'=>$persona->persona_id));
						
						$tel = PerTelefono::model()->findByAttributes(array('persona_id'=>$persona->persona_id,'nu_telefono'=> $value->telefono_1));
						if(!$tel){
							$telPersonal = new PerTelefono('jefe');
							$telPersonal->persona_id = $persona->persona_id;
							$telPersonal->nu_telefono= $value->telefono_1;
							$telPersonal->usu_aud_id=Yii::app()->user->id;
							$telPersonal->save();
						}
						$tel = PerTelefono::model()->findByAttributes(array('persona_id'=>$persona->persona_id,'nu_telefono'=> $value->telefono_2));
						if(!$tel){
							$telPersonal = new PerTelefono('jefe');
							$telPersonal->persona_id = $persona->persona_id;
							$telPersonal->nu_telefono= $value->telefono_2;
							$telPersonal->usu_aud_id=Yii::app()->user->id;
							$telPersonal->save();
						}
						PerTelefono::model()->deleteAll('persona_id=:id AND usu_aud_id IS NULL',array(':id'=>$persona->persona_id));
						//////// Eliminacion de los censos
						if($persona->cedula !=""){
							$modelPersonaCensado = BusquedaPersonaCensos::model()->findAll(array('condition'=>'nacionalidad ILIKE :nacionalidad AND cedula=:cedula',
																'params'=>array('nacionalidad'=>(String)strtoupper($value->nacionalidad),':cedula'=>(String)$value->cedula)));
							foreach ($modelPersonaCensado AS $keyCensado => $valueCensado){
//								0800 0 GMVVO
								if($valueCensado->fam_clasificacion_id !=7){
									if($valueCensado->fam_clasificacion_id ==7)
										$modelFamiliaBeneficiado->reg_plan = TRUE;
//								gmvvo
									else
										$modelFamiliaBeneficiado->reg_gmvo = TRUE;
										
									if($valueCensado->parentesco_id==12){
										Censado::model()->updateAll(array('migrado'=>TRUE,/*'st_censado'=>FALSE,*/'usu_aud_id'=>Yii::app()->user->id), 
																			'familia_id=:familia_id',array(':familia_id'=>$valueCensado->familia_id));
										Familia::model()->updateAll(array('st_familia'=>FALSE,'usu_aud_id'=>Yii::app()->user->id), 
																			'familia_id=:familia_id',array(':familia_id'=>$valueCensado->familia_id));
									}
									else{
										Censado::model()->updateAll(array('migrado'=>TRUE,/*'st_censado'=>FALSE,*/'usu_aud_id'=>Yii::app()->user->id), 'censado_id =:censado_id',
											array(':censado_id'=>$value->censado_id));
							
									}
								}
//								gmvv
								else{
									$modelFamiliaBeneficiado->reg_gmvv = TRUE;
	//								si el censado es jefe inactivo la familia dnd aparece
									if($valueCensado->parentesco_id==12){
										GrGmvvT::model()->updateAll(array('migrado'=>TRUE,'st_censado'=>FALSE,'usu_aud_id'=>Yii::app()->user->id), 
																			'familia_id=:familia_id',array(':familia_id'=>$valueCensado->familia_id));
									}
									else{
										GrGmvvT::model()->updateAll(array('migrado'=>TRUE,'st_censado'=>FALSE,'usu_aud_id'=>Yii::app()->user->id), 
																			'id=:id',array(':id'=>$valueCensado->censado_id));
									}
								}
							}
						}
						elseif ($value->censado_id){
								if($familia->fam_clasificacion_id ==6)
									$modelFamiliaBeneficiado->reg_plan = TRUE;
								elseif ($familia->fam_clasificacion_id ==7)
									$modelFamiliaBeneficiado->reg_gmvv = TRUE;
								else
									$modelFamiliaBeneficiado->reg_gmvo = TRUE;
						}	
						
						$modelFamiliaBeneficiado->save();
						$i++;
					}
						$transaction->commit();
						
						Yii::app()->user->setFlash('success', "Migracion realizada con Éxito. Total Registro: ".$i);
						$this->redirect(array('/familia/FamFamilia/familiaUpdate','id'=>$familiaPublic->familia_id));
					}
					catch (Exception $e){
						throw new CHttpException(400,$e->getMessage());
						$transaction->rollBack();
					}
					exit;
 				}
		}/*echo "<pre>";
                                print_r($modelFamilia[0]->attributes);*/
		$this->render('migracion0800',array( 
							'familiaId'=>$id, 'censo' =>$censo,
							'modelFamilia'=>$modelFamilia,
							'familia'=>$familia,
							'parentescoArray'=>$parentescoArray,
							'gradoArray'=>$gradoArray,
							'ocupacionArray'=>$ocupacionArray,
							'discapacidadArray'=>$discapacidadArray,
							'estadoCivilArray'=>$estadoCivilArray,
							'pais'=>$pais,
							'institucion'=>$institucion,
							'clasificacion'=>$clasificacion,
							'direccionOrigen' =>$direccionOrigen,
							'direccionDestino'=>$direccionDestino,
							));
	
		
	}        
        
}
