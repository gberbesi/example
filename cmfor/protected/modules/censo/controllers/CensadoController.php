<?php
Yii::import('application.modules.familia.models.FamParentesco',true);
Yii::import('application.modules.familia.models.FamMision',true);
Yii::import('application.modules.usuario.models.UsuUsuario',true);

Yii::app()->getComponent("bootstrap");
class CensadoController extends Controller
{
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
                'testLimit'=>0,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            /*array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('ValidarDatosMigracion','createcodig','captcha','vistaconsulta','NombrePersonas','CedulaCensado'),
                'users'=>array('*'),
            ),
			*/
			array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('createcodig','captcha','CedulaCensado'),
                'users'=>array('*'),
            ),
			
			/*
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),*/
//            array('allow','actions' => array('Delete'), 'roles' => array('censo/Censado/Delete')),
//            array('allow','actions' => array('Admin'), 'roles' => array('censo/Censado/Admin')),
//            array('allow','actions' => array('Update'), 'roles' => array('censo/Censado/Update')),
//            array('allow','actions' => array('View'), 'roles' => array('censo/Censado/View')),
//            array('allow','actions' => array('Create'), 'roles' => array('censo/Censado/Create')),
            //array('allow','actions' => array('ActualizarDatos'), 'roles' => array('censo/Censado/ActualizarDatos')),
            //array('allow','actions' => array('Cedula'), 'roles' => array('censo/Censado/Cedula')),
            //array('allow','actions' => array('NuevoIntegrante'), 'roles' => array('censo/Censado/NuevoIntegrante')),
            //array('allow','actions' => array('Certificado'), 'roles' => array('censo/Censado/Certificado')),
            //array('allow','actions' => array('ValidarDatos'), 'roles' => array('censo/Censado/ValidarDatos')),
            //array('allow','actions' => array('RegistroExitoso'), 'roles' => array('censo/Censado/RegistroExitoso')),
            //array('allow','actions' => array('CertificadoPreAdjudicacion'), 'roles' => array('censo/Censado/CertificadoPreAdjudicacion')),   
            //array('allow','actions' => array('Eliminar'), 'roles' => array('censo/Censado/Eliminar')),                
            array('allow','actions' => array('ConsultaCorreo'), 'roles' => array('censo/Censado/ConsultaCorreo')),
            //array('allow','actions' => array('CambioRol'), 'roles' => array('censo/Censado/CambioRol')),

            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    
    
        public function actionConsultaCorreo()
    {    
		$this->layout='//layouts/main';
		$modelPersona= new Censado('reiniciodedatos');
     //   $model = Censado::model()->find("MD5('".Familia::key."'||censado_id)=:censado_id",array(':censado_id'=>$id));
        Yii::import('application.modules.familia.models.PerInhabilitados',true);
        $PerPersona=0;
       if(isset($_POST['Censado'])){
		   $modelPersona->attributes=$_POST['Censado'];
		   if($modelPersona->validate(array('cedula','nacionalidad','nombre1','apellido1','e_mail'))){
		   
               $modelCensado = BusquedaPersonaCensos::model()->find('nacionalidad ILIKE :nacionalidad AND cedula =:cedula ', /* (**1) */ array(':nacionalidad' => $_POST['Censado']['nacionalidad'],
                    ':cedula' => trim($_POST['Censado']['cedula'])));
                 if( $modelCensado && $modelCensado->parentesco_id==12){
					 $usuario=UsuUsuario::model()->find("nb_usuario=:nb_usuario::text",array(':nb_usuario'=>$_POST['Censado']['cedula']));
                        $AuthAssignment=AuthAssignment::model()->find("userid=:userid::text",array(':userid'=>$usuario->usuario_id));
                        if($AuthAssignment->itemname=='ActualizacionCenso' || $AuthAssignment->itemname=='Registro0800' ){
                               $PerPersona=PerPersona::model()->updateAll(array('e_mail'=>$_POST['Censado']['e_mail']),'cedula=:cedula',array(':cedula'=>$_POST['Censado']['cedula']));
                            //$PerPersona=PerPersona::model()->find("cedula=:cedula",array(':cedula'=>$_POST['Censado']['cedula']));
                               

                        }
                 }else {
                       $modelCensado = PerPersona::model()->find('nacionalidad ILIKE :nacionalidad AND cedula =:cedula ',  array(':nacionalidad' => $_POST['Censado']['nacionalidad'], ':cedula' => trim($_POST['Censado']['cedula'])));
                        if( $modelCensado){
                               $usuario=UsuUsuario::model()->find("nb_usuario=:nb_usuario::text",array(':nb_usuario'=>$_POST['Censado']['cedula']));
                               $AuthAssignment=AuthAssignment::model()->find("userid=:userid::text",array(':userid'=>$usuario->usuario_id));
                               if($AuthAssignment->itemname=='ActualizacionCenso' || $AuthAssignment->itemname=='Registro0800' ){
                                      $PerPersona=PerPersona::model()->updateAll(array('e_mail'=>$_POST['Censado']['e_mail']),'cedula=:cedula',array(':cedula'=>$_POST['Censado']['cedula']));
                                   //$PerPersona=PerPersona::model()->find("cedula=:cedula",array(':cedula'=>$_POST['Censado']['cedula']));


                               }
                        }
                     
                 }
                 
                               if($PerPersona==1){
                                 //$PerPersona->e_mail=$_POST['Censado']['e_mail'];
                                 //$PerPersona->save(); 
                                // print_r($PerPersona->attributes); exit;
								
								 if($modelPersona->contrasena==1){
									$contrasena=$usuario->persona->claveAleatoria();
									UsuUsuario::model()->updateByPk($usuario->usuario_id, array('contrasena'=>md5($contrasena))); 

									$mensaje='<table border="0">
											  <tr>
												<th bgcolor="#DF4213" scope="col">&nbsp;</th>
											  </tr>
											  <tr>
												<td>
													<p>
													  <h4>Estimado '.$usuario->persona->nombre1.' '.$usuario->persona->apellido1.', Le informamos que se ha reiniciado la clave de acceso exitosamente al SIGEVIH. Se ha generado una clave aleatoria, le sugerimos ingresar al sistema y realizar el cambio de la misma.</h4>
													  <br />
														<b>Usuario</b>:'.$usuario->nb_usuario.'<br />
														<b>Contrase&ntilde;a</b>:'.$contrasena.'<br />
														<br />
														!Muchas gracias!<br />
														SIGEVIH
												   <p><h5>IMPORTANTE: Por favor no respondas este correo electr&oacute;nico.</h5></p>
												</td>
											  </tr>
											</table>';
											$modelPersona->correo($usuario->persona->e_mail,$usuario->persona->nombre1,$mensaje);									
								}		
								
                                 Yii::app()-> user-> setFlash('success', 'Se actualizaron los Datos correctamente del Usuario');
                               }   
                               
                               
           }

		}
        $this->render('consultacorreo',array(
            'modelPersona'=>$modelPersona,
        ));
    }
    
    
         public function actionCedulaCensado() {

                header('Content-type: application/json');


                if (trim($_POST['cedula']) == "" && trim($_POST['nac']) == "" && !is_numeric($_POST['cedula']) ) {
                    echo '{"datos":"ERROR"}';
                } else {
                    $mensajeError = '';
                    $mensajeNotificacion = '';
                    $Busqueda = array();
                   
                     $modelCensado = BusquedaPersonaCensos::model()->find('nacionalidad ILIKE :nacionalidad AND cedula =:cedula ',  array(':nacionalidad' => $_POST['nac'], ':cedula' => trim($_POST['cedula'])));
                     $modelCensado0800 = Censado::model()->find('st_censado=true and cedula = :cedula',array(':cedula'=>$_POST['cedula']));
                     $modelCensadoGMVV = GrGmvvT::model()->find('ci=:ci and st_censado = true',array(':ci'=>$_POST['cedula']));
                    $Busqueda['censo0800']=(($modelCensado0800)?'<span class="si">Sí</span>':'<span class="no">No</span>');
                    $Busqueda['censoGmvv']=(($modelCensadoGMVV)?'<span class="si">Sí</span>':'<span class="no">No</span>');               
                 if(false&&$modelCensado!=""){
                  if($modelCensado->parentesco_id==12){
                        $usuario=UsuUsuario::model()->find("nb_usuario=:nb_usuario",array(':nb_usuario'=>$modelCensado->cedula));
                       
                        if($usuario){
                        $AuthAssignment=AuthAssignment::model()->find("userid=:userid::text",array(':userid'=>$usuario->usuario_id));
                             if($AuthAssignment->itemname=='ActualizacionCenso' || $AuthAssignment->itemname=='Registro0800' ){
                                $PerPersona=PerPersona::model()->find("cedula=:cedula",array(':cedula'=>$modelCensado->cedula));
                                $Busqueda['nombre1'] = $PerPersona->nombre1;
                                $Busqueda['nombre2']= $modelCensado->nombre2;
                                $Busqueda['apellido1'] = $PerPersona->apellido1; 
                                $Busqueda['apellido2'] = $modelCensado->apellido2;
                                $Busqueda['correo']=$PerPersona->e_mail;
                                $Busqueda['telefono_1']=$modelCensado->telefono_1;
                                $Busqueda['telefono_2']=$modelCensado->telefono_2;

                            
                                }else{ 
                                    $mensajeError = "Usuario con perfil administrativo";
                                }
                        
                        }
                        else{ 
                            $mensajeError = "Persona no posee Usuario Activo";
                                //$mensajeNotificacion="Persona no posee Usuario Activo";
                        }
                 }else{
                            $mensajeError = "Persona no Registrada en ningun censo como Jefe de Familia ni posee usuario en sistema";
                        }
                       
                    } 
                 else {
                        $modelCensado = PerPersona::model()->find('nacionalidad ILIKE :nacionalidad AND cedula =:cedula ',  array(':nacionalidad' => $_POST['nac'], ':cedula' => trim($_POST['cedula'])));

                        if($modelCensado){
                            $usuario=UsuUsuario::model()->find("persona_id=:persona_id",array(':persona_id'=>$modelCensado->persona_id));

                            if($usuario){
                            $AuthAssignment=AuthAssignment::model()->find("userid=:userid::text",array(':userid'=>$usuario->usuario_id));
                                 if($AuthAssignment->itemname=='ActualizacionCenso' ){
                                    $PerPersona=$modelCensado;
                                    $Busqueda['nombre1'] = $PerPersona->nombre1;
                                    $Busqueda['nombre2']= $modelCensado->nombre2;
                                    $Busqueda['apellido1'] = $PerPersona->apellido1; 
                                    $Busqueda['apellido2'] = $modelCensado->apellido2;
                                    $Busqueda['correo']=$PerPersona->e_mail;
                                   // $Busqueda['telefono_1']=$modelCensado->telefono_1;
                                    //$Busqueda['telefono_2']=$modelCensado->telefono_2;

                                    }

                                }else{ 
                                    $mensajeError = "Usuario con perfil administrativo";
                                }
                        }else{
                            $mensajeError = "Persona no posee usuario en sistema";
                        }
                        
                    }

                    if ($mensajeError != '') {
                        Yii::app()->user->setFlash('error', mb_strtoupper('<ul>' . $mensajeError . '</ul>', "utf-8"));
                        $Busqueda['tipo'] = 'error';
                    }

                    if ($mensajeNotificacion != '') {
                        Yii::app()->user->setFlash('success', mb_strtoupper('<ul>' . $mensajeNotificacion . '</ul>', "utf-8"));
                        $Busqueda['tipo'] = 'error';
                    }

                    $mensaje1 = '';
                    foreach (Yii::app()->user->getFlashes() as $key => $message) {
                        $mensaje1.= '<div class="flash-' . $key . '" style="padding: 6px 0px 0px 0px;">' . $message . "</div>\n";
                    }

                    $Busqueda['mensaje'] = $mensaje1;

                    echo CJSON::encode($Busqueda);
                }

                Yii::app()->end();
    }
    
    public function actionValidarDatos()
    {
        $resultado=array();
        $resultado['error'] = "no";
        $resultado['menajeError'] = "<div class='flash-error'>Por favor corrija los siguientes errores de ingreso:<br>
                                    <ul>";

               
               
               
                if(isset($_POST['Censado'])){
            $model = NEW Censado();
            foreach ($_POST['Censado'] AS $key =>$value)
                $model->attributes = $value;
            if($model->parentesco_id ==12)
                $model->scenario ='jefe0800';
            else
                $model->scenario ='carga0800';
               
            if(!$model->validate()){
                $resultado['error'] = "si";
                $errores='';
                foreach ($model->getErrors() as $key=>$value) {
                   
                    $error="";
                    foreach ($value as $keyI => $valueI){
                        $error .=$valueI." \n";
                    }
                    $resultado[$key] =$error;
                   
                    $errores.='<li>'.$error.'</li>';
                }   
                                  
                $resultado['menajeError'].=$errores.'</ul></div>';
               
                echo CJSON::encode($resultado);
                Yii::app()->end();
            }
            echo CJSON::encode($resultado);
        }
    }
    
    public function actionValidarDatosMigracion()
    {
        $resultado=array();
        $resultado['error'] = "no";
        $resultado['menajeError'] = "<div class='flash-error'>Por favor corrija los siguientes errores de ingreso:<br>
                                    <ul>";
        if(isset($_POST['Censado'])){
            $model = NEW Censado();
            foreach ($_POST['Censado'] AS $key =>$value)
                $model->attributes = $value;
            if($model->parentesco_id ==12)
                $model->scenario ='jefeMigrar';
            else
                $model->scenario ='cargaMigrar';
               
            if(!$model->validate()){
                $resultado['error'] = "si";
                $errores='';
                foreach ($model->getErrors() as $key=>$value) {
                   
                    $error="";
                    foreach ($value as $keyI => $valueI){
                        $error .=$valueI." \n";
                    }
                    $resultado[$key] =$error;
                   
                    $errores.='<li>'.$error.'</li>';
                }   
                                  
                $resultado['menajeError'].=$errores.'</ul></div>';
               
                echo CJSON::encode($resultado);
                Yii::app()->end();
            }
            echo CJSON::encode($resultado);
        }
    }
    
    public function actionView($id)
    {
        $model = Censado::model()->find("MD5('".Familia::key."'||censado_id)=:censado_id",array(':censado_id'=>$id));
       
        $this->render('view',array(
            'model'=>$model,
        ));
    }


 public function actionCambioRol($familiaId)
    {
        
        $model= new Censado();
       $modelf = Censado::model()->find("parentesco_id=12 and familia_id=:familia_id",array(':familia_id'=>$familiaId));
     //  echo "<pre>"; print_r($modelf->cedula); die();
       
        if(isset($_POST['Censado']) && $_POST['Censado']['parentesco_id']!="")
        {  
                 $validar=true;
         
			 $model=Censado::model()->find("cedula=:cedula",array(':cedula'=>$_POST['Censado']['cedula']));
			 //    echo "<pre>";print_r($model->attributes); die();
                if($validar){

								$transaction=Yii::app()->db->beginTransaction();

								$model->attributes=$_POST['Censado'];
							try{


									Yii::import("application.modules.consulta_ben.models.BeneficiosSivihSiscavJefe",true);
									Yii::import("application.modules.familia.models.FamCiudadDestino",true);
									Yii::import("application.modules.familia.models.FamFamiliaBeneficiado",true);
									Yii::import("application.modules.usuario.models.PerTelefono",true);
                                                                        Yii::import("application.modules.usuario.models.PerPersona",true);
									Yii::import("application.modules.usuario.models.AuthItem",true);

									
									$estatus="ACTIVO";
									$consultaBeneficio=BeneficiosSivihSiscavJefe::model()->find('cedula=:cedula and estatus_desactivacion=:estatus_desactivacion',
										array(':cedula'=>  strtoupper($model->cedula), 'estatus_desactivacion'=>$estatus));
						//	print_r($consultaBeneficio->attributes); exit;
									 if(!$consultaBeneficio && $modelf!="")
									 {       
											    $true=true;

											 $familias=Familias::model()->find('cedula=:cedula',array(':cedula'=>  strtoupper($modelf->cedula)));
										   if($familias){
											 if($familias->parentesco_id==12)
											 {  
													  $personanf=Familias::model()->find('cedula=:cedula',array(':cedula'=>  strtoupper($model->cedula)));
													  $personavf=Familias::model()->find('cedula=:cedula',array(':cedula'=>  strtoupper($modelf->cedula)));
													  $famNj=FamBeneficiado::model()->find('persona_id=:persona_id',array(':persona_id'=>$personanf->persona_id));
													  $famVj=FamBeneficiado::model()->find('persona_id=:persona_id',array(':persona_id'=>$personavf->persona_id));

														$famBeNJ=FamFamiliaBeneficiado::model()-> updateAll(array('jefe_familia'=>$true),"beneficiado_id=:beneficiado_id ",
														array(':beneficiado_id'=>$famNj->beneficiado_id));
														$famBeVJ=FamFamiliaBeneficiado::model()-> updateAll(array('jefe_familia'=>false),"beneficiado_id=:beneficiado_id ",
														array(':beneficiado_id'=>$famVj->beneficiado_id));
											 
											 }
											   Yii::import("application.modules.usuario.models.AuthAssignment",true);
											   Yii::import("application.modules.usuario.models.UsuUsuario",true);

												$persona=PerPersona::model()->find('cedula=:cedula',array(':cedula'=>  strtoupper($modelf->cedula)));
												$usuario=UsuUsuario::model()->find('nb_usuario=:nb_usuario',array(':nb_usuario'=>  strtoupper($modelf->cedula)));
												//print_r($usuario); die();
												if($usuario){
													//print_r('ddd'); die(); 
												$permiso=AuthAssignment::model()->findAll('userid=:userid',array(':userid'=>  strtoupper($usuario->usuario_id)));
												//print_r(count($permiso)); die();
												$actualiza="ActualizacionCenso";
													if(count($permiso)>1){
													  $pe=AuthAssignment::model()->deleteAll('userid=:userid::text  and itemname=:itemname::text',array(':userid'=>$usuario->usuario_id,':itemname'=>$actualiza));
												     }
													if(count($permiso)==1){
														//print_r('ddd'); die();
														$pe=AuthAssignment::model()->deleteAll('userid=:userid::text',array(':userid'=>$usuario->usuario_id));
														$peU=UsuUsuario::model()->deleteAll('usuario_id=:usuario_id',array(':usuario_id'=>$usuario->usuario_id));
														//print_r($peU); die();

				 
													}
												}
													
											}        
                                                                                //         print_r($_POST['Censado']); die();
											if($_POST['Censado']['e_mail'] /*&& $_POST['Censado']['telefono_1']*/)
											{
											 //  print_r('ddd'); die();
												$modelPersona= new PerPersona;
                                                                                                $telefono= new PerTelefono;
												$telefono->attributes=$_POST['Censado']['telefono_1'];
							
												$valido=TRUE;
												$creado=false;
												$nu_telefono= NULL;
												$modelUsu= new UsuUsuario;
												$contrasena=$modelPersona->claveAleatoria();

												       Yii::import('application.modules.familia.models.PerInhabilitados',true);
													  $personaUsuario = PerPersona::model()->find('cedula =:cedula',array(':cedula'=>$model->cedula));
												  //print_r($personaUsuario->attributes);die();

														if(!$personaUsuario){
															$personaUsuario = NEW PerPersona();
															$personaUsuario->nacionalidad = $model->nacionalidad;
															$personaUsuario->cedula = $model->cedula;
															$personaUsuario->nombre1 = $model->nombre1;
															$personaUsuario->apellido1 = $model->apellido1;
															$personaUsuario->fe_nac = $model->fe_nac;
															$personaUsuario->e_mail = $model->e_mail;
															$personaUsuario->save();
														
														}
														else{
															
															
															$personaUsuario->nombre1 = $model->nombre1;
															$personaUsuario->apellido1 = $model->apellido1;
															$personaUsuario->fe_nac = $model->fe_nac;
															$personaUsuario->e_mail= $model->e_mail;
															$personaUsuario->save();
														
																
														}
															
														
														if($telefono->nu_telefono !=''){
															
															$buscar   = array("(", ")", "-");					
															$nu_telefono = str_replace($buscar, '', $telefono->nu_telefono);													
															$modelTelf=PerTelefono::model()->find('persona_id=:persona_id AND nu_telefono=:nu_telefono',array(':persona_id'=>$personaUsuario->persona_id,':nu_telefono'=>$nu_telefono));
															
															if(!$modelTelf){
																$telef = NEW PerTelefono();
																$telef->persona_id = $personaUsuario->persona_id;
																$telef->nu_telefono = $telefono->nu_telefono;
																$telef->save();
																$telefono->nu_telefono = $telef->nu_telefono;
															}
														}
														
														$modelUsu->nb_usuario = (string)$modelf->cedula;
														$usuario = UsuUsuario::model()->find('persona_id=:persona_id',array(':persona_id'=>$personaUsuario->persona_id));
                                                                                                           /*         if($usuario){
															$usuario->contrasena = $contrasena;
                                                                                                                    }
															if(!$usuario){
																if($usuario->st_usuario== FALSE){
																	$usuario->st_usuario = TRUE;
																	AuthAssignment::model()->deleteAll('userid=:userid',array(':userid'=>(string)$usuario->usuario_id));
																} 
															}	*/														
															
														//else{
															$usuExiste = UsuUsuario::model()->find('nb_usuario=:nb_usuario',array(':nb_usuario'=>$modelUsu->nb_usuario));
                                                                                                                    //  print_r($usuExiste); exit;
															if($usuExiste)
																$nbUsurio= (($modelUsu->nacionalidad=='V')?1:0).$modelUsu->nb_usuario;
															else 
																$nbUsurio = $model->cedula;
																//print_r($nbUsurio);die();
															$usuario = NEW UsuUsuario();
															$usuario->persona_id = $personaUsuario->persona_id;
															$usuario->nb_usuario = $model->cedula;
															$usuario->contrasena=md5($contrasena);
															$usuario->usu_aud_id=Yii::app()->user->id;													
															$creado=true;
															if(	$usuario->save()){
																
																$AuthAssignment= new AuthAssignment; 
																$AuthAssignment->userid=$usuario->usuario_id; 
																$AuthAssignment->itemname="ActualizacionCenso";
																$AuthAssignment->save();
															}
															
														
															
														//}
												
														

														$user=UsuUsuario::model()->findByAttributes(array('nb_usuario'=>$usuario->nb_usuario));   
													
																$id_jefe=12;
																if($usuario->nb_usuario!="" || $usuario->nb_usuario!=null){
																$jefe=Censado::model()-> updateAll(array('parentesco_id'=>$id_jefe),"cedula=:cedula ",
																	array(':cedula'=>$model->cedula));
																
																$viejo=Censado::model()-> updateAll(array('parentesco_id'=>$_POST['Censado']['parentesco_id']),"cedula=:cedula ",
																	array(':cedula'=>$modelf->cedula));										
																	Yii::app()->user->setFlash('error', "Se Realizo el cambio de Usuario Exitosamantemante");

																}
																	
									
																		$permiso = AuthAssignment::model()->model()->findByAttributes(array('itemname'=>'ActualizacionCenso','userid'=>(string)$usuario->usuario_id));
																		
                                                                                                                                                
																		$mensaje='<table border="0">
																  <tr>
																	<th bgcolor="#990000" scope="col">&nbsp;</th>
																  </tr>
																  <tr>
																	<td>
																		<p>
																		  <h4>Estimado(a) '.$user->persona->nombre1.' '.$user->persona->apellido1.', 
																			su solicitud de usuario para actualización de datos, se ha realizado exitosamente, 
																			le informamos que se ha generado una clave aleatoria, sugerimos ingresar al sistema y realizar el cambio de la misma. 
																		  <br />
																			<b>Usuario</b>: '.$usuario->nb_usuario.'<br />
																			<b>Contrase&ntilde;a</b>: '.$contrasena.'<br />
																			<br />
																				Puede ingresar al sistema a través de la dirección:                                         
																				https://sigevih.minhvi.gob.ve/index.php?r=site/loginCenso
																				
																				<br>
																			¡Muchas gracias!<br />
																			MINHVI
																	   <p><h5>IMPORTANTE: Recuerde que estos datos son confidenciales e intransferibles. Por favor no responder a esta direcci&oacute;n de correo, es una cuenta de correo no monitoreada.</h5></p>
																	</td>
																  </tr>
																</table>';
																		if(!$permiso){
																			$auth=Yii::app()->authManager;
																			$auth->assign('ActualizacionCenso',$usuario->usuario_id);    
																			}
																		
																			
                                                                                                                  Yii::app()->user->setFlash('success', " ");
														if ($creado){
														if($usuario->persona->e_mail !='')
															$personaUsuario->correoActualizacion($usuario->persona->e_mail,$usuario->persona->nombre1,$mensaje);
													//print_r($personaUsuario->attributes); die();
												
															$msmTelefono = NEW Mensajes();
															$msm = "MINHVI informa: Su usuario y contraseña para ingresar al Sistema de Actualización de Datos en GMVV y/o 0800 Mi Hogar son:\nUsuario: ".$usuario->nb_usuario."\nContraseña: ".$contrasena;
															$msmTelefono->mensaje = $msm;
															$msmTelefono->sistema = 'SIGEVIH';
															$msmTelefono->numero = $telefono->nu_telefono;
															$msmTelefono->save();
															//print_r($msmTelefono->attributes); die();
															
															
														
													}
														Yii::app()->user->setFlash('success', "Se Realizo exitosamente el cambio de Jefe de Familia y fue creado el usuario para el nuevo Jefe, fue enviado un correo electronico
															con la clave y usuario
															. <br><b>En caso de no visualizar el correo en su bandeja de entrada, verifique la bandeja de SPAM</br>");
                                                                                                                		
											$transaction->commit();	
                                                                                            $this->redirect(array('censado/ActualizarDatos&familiaId=','id'=>$familiaId));
												}													
											}
												  else {
														//$mensajeNotificacion="Persona Posee un Beneficio Activo"; 
														//Yii::app()->user->setFlash('error', "Jefe de familia posee un beneficio activo ");
														$model->addError('parentesco_id','Familia Posee Beneficio');
													}
																		
													
											
                                                                                       // censo/censado/ActualizarDatos&familiaId=1657848
											
											

								 } //try
								 catch (Exception $e) {
									   $transaction->rollBack();
                                                                            throw $e;
									}
										
                        }
						
				    
                        }
						 else {
							$model->addError('parentesco_id','Seleccione el parentesco');
						}
                
                
        
					
        $this->render('cambiorol',array(
            'model'=>$model, 'familiaId'=>$familiaId,
            'modelf'=>$modelf
        ));
    }

public function actionNombrePersonas(){
        if(isset($_POST['censado_id']) && $_POST['censado_id']!=""){
            $datosPersona = array();
            $modelJf= Censado::model()->findByAttributes(array('cedula'=>$_POST['censado_id']));
            if($modelJf){
                $datosPersona['nombres']=strtoupper($modelJf->nombre1.' '.$modelJf->nombre2);
                $datosPersona['apellidos']=strtoupper($modelJf->apellido1.' '.$modelJf->apellido2);
                //$datosPersona['sexo']=strtoupper($modelJf->sexo);
               // $datosPersona['edad']=PerPersona::model()->edad($modelJf->fe_nac);
                //$datosPersona['fecha']=Yii::app()->dateFormatter->format("dd/MM/yyyy", $modelJf->fe_nac);
            }
			//print_r($modelJf); die();
            echo CJSON::encode($datosPersona);
        }
    }
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new Censado('carga');

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Censado']))
        {
            $model->attributes=$_POST['Censado'];
            //$model->usu_aud_id=Yii::app()->user->id;
            if($model->save())
                $this->redirect(array('familia/admin'));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    public function actionCreatecodig()
    { 

        $this->layout='//layouts/emergente';

        Yii::import("application.modules.consulta_ben.models.BeneficiosSivihSiscavJefe",true);
        
        $model=new Censado('verificarCodigo');
      
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Censado']))
        {
            $model->attributes=$_POST['Censado'];
            //$model->usu_aud_id=Yii::app()->user->id;
          //  print_r("hola");die();  
           // echo "<pre>"; print_r($model->codigo);die();

          
         //   echo "<pre>"; print_r($consultaBeneficio);die();
             $valido=true; 
             $valido=$model->validate(array('verifyCode')) && $valido;
            
    if($valido) {
               $valido=$model->validate(array('codigo')) && $valido;

                            if($valido){
                                $consultaBeneficio=BeneficiosSivihSiscavJefe::model()->find('codigo_beneficio=:codigo_beneficio',array(':codigo_beneficio'=>  strtoupper($model->codigo)));

                                if($consultaBeneficio==""){
                                        Yii::app()->user->setFlash('error', "Error al Introducir Código");
                                       
                                    }else { $this->render('vistaconsulta',array('consultaBeneficio'=>$consultaBeneficio));
                                         Yii::app()->end();
                                     }
                           }
       }


        }

        $this->render('createcodig',array(
            'model'=>$model,
        ));
    }



    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        //$model=$this->loadModel($id);
        $model = Censado::model()->find("MD5('".Familia::key."'||censado_id)=:censado_id",array(':censado_id'=>$id));
        if($model->parentesco_id == 12)
            $model->scenario='jefe';
        else
            $model->scenario='carga';

        $model->ingreso_mensual=number_format($model->ingreso_mensual,2,",", ".");
        $model->fe_nac = date('d/m/Y',strtotime($model->fe_nac));
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Censado']))
        {
            $model->attributes=$_POST['Censado'];
            if($model->parentesco_id == 12)
                $model->scenario='jefe';
            else
                $model->scenario='carga';
               
            //$model->usu_aud_id=Yii::app()->user->id;
           
            if($model->save())
                $this->redirect(array('familia/admin','id'=>$model->familia_id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }
    public function actionActualizarDatos($familiaId)
    {
        //$model=$this->loadModel($id);
        $this->layout='//layouts/column2Censo';
        $this->validarGetId($familiaId);
        $model = Censado::model()->findAll("familia_id=:familia_id AND st_censado = TRUE ORDER BY parentesco_id in(12)desc, parentesco_id asc",array(':familia_id'=>$familiaId));
        if(!$model)
            $this->redirect(array('/'));
       
        /*if($model->parentesco_id == 12)
            $model->scenario='jefe';
        else
            $model->scenario='carga';

        $modelFamilia->ingreso_familiar=number_format($modelFamilia->ingreso_familiar,2,",", ".");
        $model->ingreso_mensual=number_format($model->ingreso_mensual,2,",", ".");
        $model->fe_nac= Yii::app()->dateFormatter->format("dd/MM/yyyy", $model->fe_nac);
        $auditoria = GmvvGrGmvvTAudit::model()->find('familia_id=:familia_id',array(':familia_id'=>$model->familia_id));
       
    */    // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $cedulasUnicaFamilia = array();
        $jefe = TRUE;
        $famClasificacionId=7;
        $familiaDelUsuario = FALSE;
        $usuario = UsuUsuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>Yii::app()->user->id));//realidad
        $countConjuge = 0;
        $countPadre = 0;
        $countMadre = 0;
        foreach ($model AS $key => $value){
            $censado = NEW Censado();
            $censado->censadoId = $value->censado_id;
            $censado->attributes = $value->attributes;
           
            $censado->fe_nac= Yii::app()->dateFormatter->format("dd/M/yyyy", $censado->fe_nac);
           
            $censado->ingreso_mensual=number_format($censado->ingreso_mensual,2,",", ".");
            $censado->honorarios_profesionales=number_format($censado->honorarios_profesionales,2,",", ".");
            $censado->bono_alimentacion=number_format($censado->bono_alimentacion,2,",", ".");
            $censado->otros_ingresos=number_format($censado->otros_ingresos,2,",", ".");
               
            if($censado->nacionalidad =='S'){
                $censado->nacionalidad ='V';
                $censado->cedula = NULL;
            }
            if($censado->parentesco_id==12 && $jefe){
                $censado->scenario ='jefe0800';
                $jefe=FALSE;
            }
            else{
                $censado->scenario ='carga0800';
            }
                   
            $censado->validate();
           
            if($censado->cedula !=""){
                $existe=array_search($censado->cedula, $cedulasUnicaFamilia);
                if($existe==false){
                        $cedulasUnicaFamilia[]=($censado->cedula);
                        $familia[]= $censado;
                }
            }
            else
                $familia[]= $censado;
           
            if($usuario->persona->nacionalidad ==$censado->nacionalidad &&  $usuario->persona->cedula==$censado->cedula)
                $familiaDelUsuario = TRUE;
           
            if($censado->parentesco_id==1){
                //CONJUGE
                $countConjuge++;
                if($countConjuge>1){
                    $valido=false;
                    $censado->addError('parentesco_id', 'Ya existe otro integrante como CONYUJE');
                }
                           
            }
            elseif ($censado->parentesco_id==4){
                //PADRE

                $countPadre++;
                if($countPadre>1){
                    $valido=false;
                    $censado->addError('parentesco_id', 'Ya existe otro integrante como PADRE');
                }
            }
            elseif ($censado->parentesco_id==3){
                //MADRE
                $countMadre++;
                if($countMadre>1){
                    $valido=false;
                    $censado->addError('parentesco_id', 'Ya existe otro integrante como MADRE');
                }
            }
           
        }
        if(!$familiaDelUsuario && !Yii::app()->user->checkAccess("censo/Familia/SearchAdmin"))
        {
            $this->redirect(array('/'));
        }
           
        $auditoria = CensoFamiliaAudit::model()->find('familia_id=:familia_id',array(':familia_id'=>$familia[0]->familia_id));
       
//        $mismoJefeVariasFamilia = GrGmvvT::model()->findAll('ci=:ci AND nacionalidad ILIKE :nacionalidad AND familia_id!=:familia_id AND st_censado =TRUE',
//                                            array(':ci'=>$model[0]->ci, ':nacionalidad'=>$model[0]->nacionalidad,':familia_id'=>$model[0]->familia_id));
//        foreach ($mismoJefeVariasFamilia as $keyMJ =>$valueMJ){
//            GrGmvvT::model()->updateAll(array('st_censado'=>false,'usu_audit_id'=>Yii::app()->user->id),
//                                        'familia_id=:familia_id',array(':familia_id'=>$valueMJ->familia_id));
//        }

        //$direccion = $model[0]->familia;
        $modelFamilia = $model[0]->familia;
        if($modelFamilia->direccion)
            $modelDireccionOrigen = $modelFamilia->direccion;
        else {
            $modelDireccionOrigen = NEW Direccion();
           
        }
       
    if($modelFamilia->direccionDestino)
        $modelDireccionDestino = $modelFamilia->direccionDestino;
    else
        $modelDireccionDestino = NEW Direccion();
           
       

        if(isset($_POST['Censado']))
        {
            $valido= TRUE;
            $modelDireccionOrigen->attributes= $_POST['Direccion'][0];
           
            $valido = TRUE;
            $contJefe = 0;
            unset($cedulasUnicaFamilia);
            unset($familia);
            $familia=array();
            $cedulasUnicaFamilia=array();
            $jefe = TRUE;
            $countConjuge = 0;
            $countPadre = 0;
            $countMadre = 0;   
            foreach($_POST['Censado'] as $i=>$data) {
                $item = new Censado();
               
                if(isset($data)) {
                    $item->attributes=$data;
                    if($item->parentesco_id ==12 && $jefe){
                       
                        $item->parentesco_id =12;
                        $jefe=FALSE;
                    }
                    else{
                        if($item->parentesco_id ==12)
                            $item->parentesco_id =NULL;
                       
                    }
                    if($item->parentesco_id =="")$item->parentesco_id = NULL;
                    if($item->edo_civil_id =="")$item->edo_civil_id = NULL;
                       
                     $item-> estudia  = ($item-> estudia)?TRUE:FALSE;
                     $item-> estado_embarazo  = ($item-> estado_embarazo)?TRUE:FALSE;
                     $item-> trabaja = ($item-> trabaja)?$item-> trabaja:NULL;
                     $item-> id_clasificacion  = ($item-> id_clasificacion)?$item-> id_clasificacion:NULL;
//echo $item->scenario;
                    if($item->parentesco_id ==12)
                        $item->scenario ='jefe0800';
                    else
                        $item->scenario ='carga0800';
                       
                    $valido=$item->validate()&&$valido;
//                    print_r($item->getErrors());
//                    echo "<pre>";print_r($item->attributes);exit;
                    if($item->parentesco_id ==12){
                        $item->scenario ='jefe0800';
                        if($contJefe==0){
                            $contJefe++;
                        }
                        else{
                            $contJefe++;
                            $valido=false;
                            $item->addError('parentesco_id', 'Parentesco debe ser diferente');
                        }
                    }
                    else
                        $item->scenario ='carga0800';
                   
                    if($item->cedula !=''){
                        $existe=array_search($item->cedula, $cedulasUnicaFamilia);
                        if($existe!==false){
                            $valido=false;
                            $item->addError('cedula', 'Cédula Repetida en la Familia');
                        }
                        else
                            $cedulasUnicaFamilia[]=($item->cedula);
                    }
                    $familia[] = $item;
                   
                   
                    if($item->parentesco_id==1){
                        //CONJUGE
                        $countConjuge++;
                        if($countConjuge>1){
                            $valido=false;
                            $item->addError('parentesco_id', 'Ya existe otro integrante como CONYUJE');
                        }
                           
                    }
                    elseif ($item->parentesco_id==4){
                        //PADRE
                        $countPadre++;
                        if($countPadre>1){
                            $valido=false;
                            $item->addError('parentesco_id', 'Ya existe otro integrante como PADRE');
                        }
                    }
                    elseif ($item->parentesco_id==3){
                        //MADRE
                        $countMadre++;
                        if($countMadre>1){
                            $valido=false;
                            $item->addError('parentesco_id', 'Ya existe otro integrante como MADRE');
                        }
                    }
                   
                   
                }
            }
           
           
            if($contJefe==0){
                Yii::app()-> user-> setFlash('error', 'Debe de registrar un JEFE(A) FAMILIA');
                $valido=false;
            }elseif ($contJefe>1){
                Yii::app()-> user-> setFlash('error', 'Solo puede registrar un JEFE(A) FAMILIA');
                $valido=false;
            }
                       
            $modelDireccionOrigen->usu_aud_id=Yii::app()->user->id;
            //$valido= false;
            $valido = $modelDireccionOrigen->validate()&&$valido;
            if(isset($_POST['Direccion'][1])){
                $modelDireccionDestino->attributes= $_POST['Direccion'][1];
                $valido = $modelDireccionDestino->validate()&&$valido;
            }
            if($valido){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modelDireccionOrigen->save();
                    $modelDireccionDestino->save();
                    $modelFamilia->direccion_id=$modelDireccionOrigen->direccion_id;
                    $modelFamilia->direccion_destino=$modelDireccionDestino->direccion_id;                  
                    $modelFamilia->usu_aud_id=Yii::app()->user->id;
                    $modelFamilia->save();
                    Yii::import('application.modules.familia.models.FamFamilia',true);
                    Yii::import('application.modules.familia.models.PerTelefono',true);
                    Yii::import('application.modules.familia.models.PerInhabilitados',true);
                    $i=0;
                    $cantIntegrantes=0;
                    $cantidadSueldo = 0;
                    $gradoInstJefe =0;
                    $caractJEfe ='';
                    $edJEfe = 0;
                    $nuDisc = 0;
                    $conPareja='FALSE';
                    $conHijo = 'FALSE';
                    $idGmVV='';
                    $cedulas = array();
                   
                    foreach ($familia as $key => $value){
                       
                        if($value->censadoId !=''){
                            $persona = Censado::model()->find('censado_id=:censado_id',array(':censado_id'=>$value->censadoId));
                            if($value->parentesco_id==12)
                                $personaScenerario='jefe0800';
                            else    
                                $personaScenerario = 'carga0800';
                            $persona-> attributes  = $value-> attributes;
                             $persona->censado_id = $value->censadoId;
                             $persona->censadoId = $value->censadoId;
                             $persona-> familia_id  = $value-> familia_id;
                             $persona->st_censado = TRUE;
                             $persona->usu_aud_id=Yii::app()->user->id;
                             $persona->scenario =$personaScenerario;
                             $persona->save();
                        }
                        else{
                            if($value->parentesco_id==12)
                                $personaScenerario='jefe0800';
                            else    
                                $personaScenerario = 'carga0800';
                               
                            $persona = NEW Censado($personaScenerario);
                            $persona-> attributes  = $value-> attributes;
                             $persona-> familia_id  =$familiaId;
                             $persona->st_censado = TRUE;
                             $persona->usu_aud_id=Yii::app()->user->id;
                             //print_r($persona->attributes);exit;
                             $persona->save();
                            
                        }
                         $idGmVV.="'".$persona->censado_id."',";
                        
                         if($value->cedula !=''){
                             $ingreso_mensual=str_replace('.', '', $value->ingreso_mensual);
                            $ingreso_mensual=str_replace(',', '.', $ingreso_mensual);
                            if($ingreso_mensual =="")
                                $ingreso_mensual = NULL;
                                                             
                             $modelPersona= PerPersona::model()->findByAttributes(array('cedula'=>(int)$value->cedula));
                            if($modelPersona){
                               
                                PerPersona::model()->updateAll(array(
                                                            'nacionalidad'=>$value->nacionalidad,
                                                            'nombre1'=>$value->nombre1,
                                                            'nombre2'=>$value->nombre2,
                                                            'apellido1'=>$value->apellido1,
                                                            'apellido2'=>$value->apellido2,
                                                            'fe_nac'=>$value->fe_nac,
                                                            'sexo'=>$value->sexo,
                                                            'e_mail'=>$value->e_mail,
                                                            ),
                                                    'persona_id=:persona_id',array(':persona_id'=>$modelPersona->persona_id));
                                if($modelPersona->famBeneficiados){
                                    FamBeneficiado::model()->updateAll(array(
                                                            'enfermedad_id' => (($value->enfermedad_id!="")?$value->enfermedad_id:NULL),
                                                            'grado_instruccion_id' => (($value->grado_instruccion_id!="")?$value->grado_instruccion_id :NULL),
                                                            'ocupacion_id' => (($value->ocupacion_id!="")?$value->ocupacion_id :NULL),
                                                            'ingreso_mensual' => $ingreso_mensual,
                                                            'edo_civil_id' => (($value->edo_civil_id!="")?$value->edo_civil_id :NULL),
                                                            'trabajo_id'=>(($value->trabajo_id!="")?$value->trabajo_id :NULL),
                                                            'insc_faov'=>$value->insc_faov,
                                                            ),
                                                    'persona_id=:persona_id',array(':persona_id'=>$modelPersona->persona_id));
                                /*$modelBeneficiado = FamFamiliaBeneficiado::model()->width('famBeneficiado')->findByAttributes(array('persona_id'=>$modelPersona->persona_id, 'st_familia_beneficiado = TRUE'));
                                if($modelBeneficiado){
                                    FamFamiliaBeneficiado::model()->updateAll(array(
                                                            $famiBeneficiado->parentesco_id = $rol->parentesco_id
                                                            ),
                                                    'beneficiado_id=:beneficiado_id',array(':beneficiado_id'=>$modelBeneficiado->beneficiado_id));
                                }*/
                                }
                               
                            }
                            $persona = GrGmvvT::model()->findAll('ci=:ci',array(':ci'=>(string)$value->cedula));
                            foreach ($persona as $key => $value2){
                               
                                    $edCivil = FamEdoCivil::model()->find('edo_civil_id=:edo_civil_id',array(':edo_civil_id'=>(int)$value->edo_civil_id));
                                    if($edCivil)
                                        $value2->nb_ed_civil = $edCivil->nombre;
                                    else
                                        $value2->nb_ed_civil = '';
       
                                       
                                    $gInstr = FamGradoInstruccion::model()->find('grado_instruccion_id=:grado_instruccion_id',array(':grado_instruccion_id'=>(int)$value->grado_instruccion_id));
                                    if($gInstr)
                                        $value2->grado_instruccion = $gInstr->nombre;
                                    else
                                        $value2->grado_instruccion = '';
                                       
                                    $enf = FamEnfermedad::model()->find('enfermedad_id=:enfermedad_id',array(':enfermedad_id'=>(int)$value->enfermedad_id));
                                    if($enf)
                                        $value2->enfermedad = $enf->nombre;
                                    else
                                        $value2->enfermedad = '';
                                       
                                    $trabajo = PerTrabajo::model()->find('trabajo_id=:trabajo_id',array(':trabajo_id'=>(int)$value->trabajo_id));
                                    if($trabajo)
                                        $value2->institucion = $trabajo->nb_trabajo;
                                    else
                                        $value2->institucion = '';
                                       
                                    $ocupacion = FamOcupacion::model()->find('ocupacion_id=:ocupacion_id',array(':ocupacion_id'=>(int)$value->ocupacion_id));
                                    if($ocupacion)
                                        $value2->ocupacion = $ocupacion->nombre;
                                    else
                                        $value2->ocupacion = '';
                                       
                                    GrGmvvT::model()->updateAll(array(
                                                                'nacionalidad'  => $value-> nacionalidad,
                                                                 'primer_nombre'  => $value-> nombre1,
                                                                 'segundo_nombre'  => $value-> nombre2,
                                                                 'primer_apellido'  => $value-> apellido1,
                                                                 'segundo_apellido'  => $value-> apellido2,
                                                                 'f_nacimiento'  => $value-> fe_nac,
                                                                 'sexo'  => $value-> sexo,
                                                                 'edo_civil_id'  => $value-> edo_civil_id,
                                                                'nb_ed_civil'  => $value2-> nb_ed_civil,
                                                                 'e_mail'  => $value-> e_mail,
                                                                 'grado_instruccion_id'  => $value-> grado_instruccion_id,
                                                                 'grado_instruccion'  => $value2-> grado_instruccion,
                                                                 'enfermedad_id'  => $value-> enfermedad_id,
                                                                 'enfermedad'  => $value2-> enfermedad,
                                                                 'ocupacion_id'  => $value-> ocupacion_id,
                                                                 'ocupacion'  => $value2-> ocupacion,
                                                                 'trabajo_id'  => $value-> trabajo_id,
                                                                 'institucion'  => $value2-> institucion,
                                                                 'insc_faov'  => $value-> insc_faov,
                                                                 'ingreso_mensual'  => $ingreso_mensual,
                                                                 'telf_movil'  => $value-> telefono_1,
                                                                 'telf_otro'  => $value-> telefono_2,
                                                                ),
                                                        'id_gr_gmvv_t=:id_gr_gmvv_t',array(':id_gr_gmvv_t'=>$value2->id_gr_gmvv_t));
                            }
                         }
                            
                    }   
                        // jasuarez se comento el update debido a que todas las personas que se eliminaban de censo de 0800 se les cambia era de estatus a false y no permitia registrar nuevamente en el censo 
                    // motivado a este se anexa e
                 /*   Censado::model()->updateAll(array('st_censado'=>false,'usu_audit_id'=>Yii::app()->user->id),
                                        'familia_id=:familia_id AND censado_id not IN('.trim($idGmVV,',').')',
                                        array(':familia_id'=>$familia[0]->familia_id));*/

                     

                        Censado::model()->deleteAll('familia_id=:familia_id AND censado_id not IN('.trim($idGmVV,',').')',
                                        array(':familia_id'=>$familia[0]->familia_id));


                     $n_grupo=Censado::model()->countBySql("select COUNT(familia_id) from censo.censado  where familia_id=$familiaId");
//print_r($n_grupo); die();
                     $actualizagrupo=Familia::model()->updateAll(array('n_grupo'=>$n_grupo),'familia_id=:familia_id',array(':familia_id'=>$familiaId));


                    $ingresoFamiliar=Censado::model()->countBySql("SELECT sum(ingreso_mensual) as suma from censo.censado where familia_id=$familiaId");

                     $actualizaingreso=Familia::model()->updateAll(array('ingreso_familiar'=>$ingresoFamiliar),'familia_id=:familia_id',array(':familia_id'=>$familiaId));
                    
                $transaction->commit();
                Yii::app()->user->setFlash('success', "Actualización Exitosa de sus datos");
                $this->redirect(array('RegistroExitoso','familiaId'=>$familiaId));
            }
            catch (Exception $e){
                throw new CHttpException(400,$e->getMessage());
                $transaction->rollBack();
            }
        }
               
    }
    
    $this->render('actualizarDatos',array(
            'model'=>$model,
            'familia'=>$familia,
            'modelFamilia'=>$modelFamilia,
            'modelDireccionOrigen'=>$modelDireccionOrigen,
            'modelDireccionDestino'=>$modelDireccionDestino,
            'familiaId'=>$familiaId,
            'auditoria'=>$auditoria,
        ));
    }
    public function actionRegistroExitoso($familiaId)
    {
        $this->layout='//layouts/main2';
        $this->validarGetId($familiaId);
        //$model = GrGmvvT::model()->find("(familia_id)=:familia_id and parentesco_id =12",array(':familia_id'=>109241125));
        if(Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")){
            $model = Censado::model()->find("familia_id=:familia_id and parentesco_id =12",
                    array(':familia_id'=>$familiaId));
        }
        else{
            $usuario = UsuUsuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>Yii::app()->user->id));//realidad
            $model = Censado::model()->find("familia_id=:familia_id and parentesco_id =12 AND cedula=:cedula AND nacionalidad=:nacionalidad",
                    array(':familia_id'=>$familiaId,
                          ':cedula'=>$usuario->persona->cedula,
                          ':nacionalidad'=>$usuario->persona->nacionalidad));
        }
       
        if(!$model && !Yii::app()->user->checkAccess("censo/Familia/SearchAdmin"))
        {
            $this->redirect(array('/'));
        }
       
        $this->render('registroExitoso',array(
            'model'=>$model,
            'familiaId'=>$familiaId
        ));
    }   
    public function actionCertificado($familiaId)
    {
        $this->layout='//layouts/main2';
        $this->validarGetId($familiaId);
        $model = Censado::model()->findAll("familia_id=:familia_id AND st_censado = TRUE ORDER BY parentesco_id in(12)desc, parentesco_id asc",array(':familia_id'=>$familiaId));
        $familiaDelUsuario = FALSE;
        $usuario = UsuUsuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>Yii::app()->user->id));//realidad
        if(!$model)
            $this->redirect(array('/'));
           
        foreach ($model AS $key => $value){
            if($usuario->persona->nacionalidad ==$value->nacionalidad &&  $usuario->persona->cedula==$value->cedula)
                $familiaDelUsuario = TRUE;
        }
        if(!$familiaDelUsuario && !Yii::app()->user->checkAccess("censo/Familia/SearchAdmin"))
        {
            $this->redirect(array('/'));
        }   
        $auditoria = CensoFamiliaAudit::model()->find('familia_id=:familia_id',array(':familia_id'=>$model[0]->familia_id));
       
        $familia = $model[0]->familia;
        $this->render('certificado',array(
            'model'=>$model,'familia'=>$familia,
            'auditoria'=>$auditoria
        ));
        
        
    }   
    
    public function actionCertificadoPreAdjudicacion()
    {
        $this->layout='//layouts/main2';
        $familiaDelUsuario = FALSE;
        $usuario = UsuUsuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>Yii::app()->user->id));//realidad
        
        //Yii::import("application.modules.consulta_ben.models.BeneficiosSivihSiscavJefe",true);
        Yii::import("application.modules.adjudicacion.models.BenBeneficio",true);
        
        
        $consultaBeneficio=Comun::busquedaAdjudicado0800($usuario->persona->cedula,$usuario->persona->nacionalidad);

        $model = PerPersona::model()->findAll('nacionalidad=:nacionalidad and cedula=:cedula',array(':nacionalidad'=>$usuario->persona->nacionalidad,':cedula'=>$usuario->persona->cedula));
        
        
        if(!$model||!$consultaBeneficio)
            $this->redirect(array('/'));
         
        $codigo="";
        //generar codigo aleatoreo en caso de no poseerlo
        if($consultaBeneficio->codigo_beneficio==NULL){
            $i=true;
            while ($i) {
                $codigo=  strtoupper(Comun::claveAleatoria());
                if(!BenBeneficio::model()->find('codigo_beneficio=:codigo_beneficio',array(':codigo_beneficio'=>$codigo))) {
                    BenBeneficio::model()->updateByPk($consultaBeneficio->beneficio_id, array('codigo_beneficio'=>$codigo));
                    $i=false;
                }
            }
            
        }
        else $codigo=$consultaBeneficio->codigo_beneficio;
        
        
        //$codigo=md5($consultaBeneficio->familia_id.'-'.$consultaBeneficio->beneficio_id);

        $this->render('certificadoPreAdjudicacion',array(
            'model'=>$model,
            'consultaBeneficio'=>$consultaBeneficio,
            'codigo'=>$codigo,
        ));
    }      
    
    
    public function actionNuevoIntegrante()
    {
        $this->layout='//layouts/emergenteCenso';
        $model=NEW Censado('carga0800');
        $id = $_POST['id'];
       
        $this->render('nuevoIntegrante',array(
            'model'=>$model,
            'id'=>$id
        ));
    }
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
//    public function actionDelete($id)
//    {
//        $model = $this->loadModel($id);
//        if($model)
//            Censado::model()->updateAll(array('st_censado'=>false/*,'usu_aud_id'=>Yii::app()->user->id*/),'censado_id=:censado_id',array(':censado_id'=>$model->censado_id));
//       
//        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
//        if(!isset($_GET['ajax']))
//            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
//    }
    public function actionEliminar($id=NULL){
//      SI el id, viene del searchAdmin
        if(isset($_GET['id'])){
            $model = Censado::model()->findByPk($id);
        }
        else{
            $modelUsuario = UsuUsuario::model()->findByPk(Yii::app()->user->id);
            $model = Censado::model()->find('nacionalidad=:nacionalidad AND cedula=:cedula',array(':nacionalidad'=>$modelUsuario->persona->nacionalidad,':cedula'=>$modelUsuario->persona->cedula));
            $id = $model->censado_id;
        }
        
        if(!$model)
            $this->redirect(array('/'));
            
        $this->performAjaxValidation($model);
        //print_r($_POST['Censado']); exit;
        if(isset($_POST['Censado']))
        {
            $model->attributes=$_POST['Censado'];
            if($model->motivo_eliminacion_beneficiado_id!=''){
                $transaction = Yii::app()->db->beginTransaction();
                try {
//                  nucleo familiar
                    $familia = $model->familia;
            //echo "<pre>";       print_r($familia->attributes); exit;
                    $direccion_id= $familia->direccion_id;
                    $direccion_solicitud_id= $familia->direccion_destino;
                    foreach ($familia->censados AS $key =>$value){
                        $modelHistorico = new HistoricoEliminadoCarga();
                        $modelHistorico->familia_id = $value->familia_id;
                        $modelHistorico->nacionalidad = $value->nacionalidad;
                        $modelHistorico->cedula = $value->cedula;
                        if($value->parentesco_id==12){
                            $modelHistorico->motivo_eliminacion_beneficiado_id = $model->motivo_eliminacion_beneficiado_id;
                            $modelHistorico->jefe= TRUE;
                        }
                            
                        $modelHistorico->censado_id = $value->censado_id;
                                                $modelHistorico->usu_aud_id=Yii::app()->user->id;
                        $modelHistorico->save();            
                    } 
                   
                        Yii::import("application.modules.consulta_ben.models.BeneficiosSivihSiscavJefe",true);
                        Yii::import("application.modules.familia.models.FamCiudadDestino",true);
                        Yii::import("application.modules.familia.models.FamFamiliaBeneficiado",true);
                             Yii::import("application.modules.usuario.models.PerTelefono",true);

                        

                        $consultaBeneficio=BeneficiosSivihSiscavJefe::model()->find('cedula=:cedula',array(':cedula'=>  strtoupper($model->cedula)));
                   //  print_r($consultaBeneficio); die();
                         if(!$consultaBeneficio)
                         {          $true=true;

                                 $familias=Familias::model()->find('cedula=:cedula',array(':cedula'=>  strtoupper($model->cedula)));
                              //print_r($familias); die();

                               if($familias){
                                //print_r($familias->attributes); die();
                                 if($familias->parentesco_id==12)
                                 {
                                    FamCiudadDestino::model()->deleteAll('familia_id=:familia_id',array(':familia_id'=>$familias->familia_id));                              
                                    FamFamiliaBeneficiado::model()->deleteAll('familia_id=:familia_id',array(':familia_id'=>$familias->familia_id));                        
                                    FamFamilia::model()->deleteAll('familia_id=:familia_id',array(':familia_id'=>$familias->familia_id));
                                 
                                 }
                                 else
                                 {
                                    //print_r($familias->cedula); die();
                                    $persona=Familias::model()->find('cedula=:cedula',array(':cedula'=>  strtoupper($model->cedula)));
                                    PerTelefono::model()->deleteAll('persona_id=:persona_id',array(':persona_id'=>$persona->persona_id));
                                    PerPersona::model()->deleteAll('cedula=:cedula',array(':cedula'=>$persona->persona_id));

                                   
                                 }
                             }
                        }
                      




      //              eliminacion de la familia 0800

                    Censado::model()->deleteAll('familia_id=:familia_id',array(':familia_id'=>$model->familia_id));
                    Familia::model()->deleteAll('familia_id=:familia_id',array(':familia_id'=>$model->familia_id));
                    Direccion::model()->deleteAll('direccion_id=:direccion_id',array(':direccion_id'=>$direccion_id));
                    Direccion::model()->deleteAll('direccion_id=:direccion_id',array(':direccion_id'=>$direccion_solicitud_id));
                        
    //              Buscar los registro del censado en la gmvv
                    $modelCensado=GrGmvvT::model()->count('nacionalidad ilike :nacionalidad AND ci =:ci AND parentesco_id=12',
                                                array(':nacionalidad'=>(string)strtoupper($model->nacionalidad),':ci'=>(string)$model->cedula));
//                  Buscar el usuario del censado
                    $modelU = UsuUsuario::model()->with('persona')->find('cedula=:cedula AND nacionalidad=:nacionalidad',array(':cedula'=>$model->cedula,':nacionalidad'=>$model->nacionalidad));   
//                  si el censado esta registrado solamente en el 0800, se elimina su usuario,
                    if($modelU){
                        if($modelCensado==0){
                            $modelAu =AuthAssignment::model()->find('userid=:userid AND itemname=\'ActualizacionCenso\'',array(':userid'=>(string)$modelU->usuario_id));
                            if ($modelAu)
                                $modelAu->delete();
                                
                            $modelUs = UsuUsuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>$modelU->usuario_id));
                            if($modelUs)
                                $modelUs->delete();
                        }
                    
    //                  si es el mismo jefe que esta eliminado
    //                  para indicar habilitar el boton de volver al search de busqueda 
                        if($modelU->usuario_id == Yii::app()->user->id){
    //                      Si el censado solo esta registrado en el 0800 como jefe de familia
    //                      se declara la variable $redireccion como 0 que indica salir del sistema
    //                      se declara la variable $redireccion como 1 que indica que el usuario puede ser redireccionado a censo/familia/search
                            if($modelCensado==0)
                                $redireccion = 0;
                            else 
                                $redireccion = 1;
                        }
                        else{
    //                      El usuario que esta eliminado al censado como jefe de familia del 0800 no es el censado
    //                      se declara la variable $redireccion como 2 que indica que el usuario puede ser redireccionado a censo/familia/searchAdmin
                            $redireccion = 2;
                        }
                    }
                    else{
                        $redireccion = 2;
                    }
                    $transaction->commit();
                    $this->render('mensajeJefeEliminado',array('model'=>$model,'redireccion'=>$redireccion));
                }
                catch (Exception $e){
                    throw new CHttpException(400,$e->getMessage());
                    $transaction->rollBack();
                }
                exit;
            }
            else
                $model->addError('motivo_eliminacion_beneficiado_id', 'No puede ser nulo');
        }
        $this->render('eliminar',array(
            'model'=>$model,
            'id'=>$id
        ));
    }
    

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Censado');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Censado('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Censado']))
            $model->attributes=$_GET['Censado'];

        $this->render('admin',array(
            'model'=>$model
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Censado the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Censado::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Censado $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='censado-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
       
       
   
    public function actionCedula()

       {Yii::import('application.modules.consulta_ben.models.BeneficiosSivihJefe',true);
       Yii::import('application.modules.consulta_ben.models.BeneficiosSivihSiscavJefe',true);
       
        header('Content-type: application/json');
        if(trim($_POST['cedula'])==""||trim($_POST['nac'])=="" || !is_numeric($_POST['cedula']))
        {
            echo '{"nacionalidad":"ERROR","cedula":"","nombre1":"","nombre2":"","apellido1":"","apellido2":"","fecha_nac":"","sexo":"","edo_civil":""}';
            }
        else {
                $mensajeError ='';
                $mensajeNotificacion = '';
                   /****************Busqueda en la base de datos de sivih_static los  datos de la persona***************/
                   $sql= "select * from onidex where cedula=".$_POST['cedula']." AND nac='".$_POST['nac']."'";
                $comando = Yii::app() -> onidex_db -> createCommand($sql);
                $Busqueda = $comando -> queryRow();

                $Busqueda['fecha_nac'] = (($Busqueda['fecha_nac'])?Yii::app()->dateFormatter->format("dd/M/yyyy", $Busqueda['fecha_nac']):null);
                $Busqueda['status']=0;
                               
                               
            $benSivih = BeneficiosSivihJefe::model()->find('cedula=:cedula AND (status_beneficio=\'ACTIVO\' OR  status_beneficio=\'POR REGURALIZAR\' OR  status_beneficio=\'\' OR  status_beneficio is null)',array(':cedula'=>$_POST['cedula']));
                /******************* Fin de la Busqueda del beneficio en el sivih ************************************/
                /*************Busqueda del beneficio en siscav *****************************************************/
                $cedula=$_POST['cedula'];
               
                $siscav="select * from beneficios_siscav where cedula=$cedula AND (nb_estado='ACTIVO' OR nb_estado='POR REGULARIZAR');";//BUSCAR BENEFICIOS EN SISCAV
                        
                                $connection=Yii::app()->db;
                         $command=$connection->createCommand($siscav);
                         $siscav=$command->queryAll();
   
                if($benSivih AND $siscav){
                    $Busqueda['beneficioSivih'] = true;}
                elseif($benSivih AND !$siscav){
                    $Busqueda['beneficioSivih'] = true;}
                elseif(!$benSivih AND $siscav){
                    $Busqueda['beneficioSivih'] = true;}
                else{
                    $Busqueda['beneficioSivih'] = false;}
                /*************Fin Busqueda del beneficio en siscav *****************************************************/
               
               
               
                /****************Busqueda en la base de datos del sistema si esta ya registrado***********************/
               
                $sqls= "select u.st_familia_beneficiado, u.jefe_familia
                       
                        from fam_familia_beneficiado as u
                            join fam_beneficiado b on u.beneficiado_id = b.beneficiado_id
                            join per_persona p on p.persona_id = b.persona_id 
                       
                        where st_familia_beneficiado = true and cedula=:cedula";
                $comando = Yii::app() ->db -> createCommand($sqls);
               
                $comando -> bindParam(":cedula",$_POST['cedula'],PDO::PARAM_STR);
                //$comando -> bindParam(":nacionalidad",$_POST['nac'],PDO::PARAM_STR);
                $fila = $comando -> queryRow();

                     if ($fila['jefe_familia'] == true && $fila['st_familia_beneficiado'] == true)
                        $mensajeError1="JEFE";
                    if ($fila['jefe_familia'] == null && $fila['st_familia_beneficiado'] == true)
                          $mensajeError2="CARGA";
                                       
                                       
                                       
                                        $sqlss= "select u.st_familia_beneficiado, u.jefe_familia
                       
                        from fam_familia_beneficiado as u
                            join fam_beneficiado b on u.beneficiado_id = b.beneficiado_id
                            join per_persona p on p.persona_id = b.persona_id 
                       
                        where st_familia_beneficiado = false and cedula=:cedula";
                $comandos = Yii::app() ->db -> createCommand($sqlss);
               
                $comandos -> bindParam(":cedula",$_POST['cedula'],PDO::PARAM_STR);
                //$comando -> bindParam(":nacionalidad",$_POST['nac'],PDO::PARAM_STR);
                $filas = $comandos -> queryRow();

                     if ($filas['jefe_familia'] == null && $filas['st_familia_beneficiado'] == false)
                        $mensajeError1="";
                    if ($filas['jefe_familia'] == null && $filas['st_familia_beneficiado'] == false)
                          $mensajeError2="";
                   
                /***********Fin de la busqueda en la base de datos del sistema si esta ya registrado******************/
               
                                /// VALIDACION DE BUSQUEDA DIFRENTE GRUPO FAMILIAR
                                $dataProvider = Censado::model()->find('nacionalidad ILIKE :nacionalidad AND cedula=:cedula AND st_censado = TRUE AND familia_id!=:familia_id',
                                        array('nacionalidad'=>$_POST['nac'],':cedula'=>trim($_POST['cedula']),':familia_id'=>$_POST['familia'] ));

                
                if($dataProvider){
                   
                    if ($dataProvider->parentesco_id==12){
                        $mensajeError1="JEFE";
                    }
                    else{
                        $mensajeError2="CARGA";
                    }
                }/////
                        
                                //
                                 /// VALIDACION DE BUSQUEDA DENTRO DEL MISMO GRUPO FAMILIAR
                          $dataProviderh = Censado::model()->find('nacionalidad ILIKE :nacionalidad AND cedula=:cedula AND st_censado = TRUE AND familia_id=:familia_id',
                                        array('nacionalidad'=>$_POST['nac'],':cedula'=>trim($_POST['cedula']),':familia_id'=>$_POST['familia'] ));
                
                if($dataProviderh){
                   
                    if ($dataProviderh->parentesco_id==12){
                        $mensajeError1="JEFE";
                    }  else {
                                            $mensajeError2="CARGA";
                                        }
                                       
                }///////////////
              ///VALIDACION HACIA GMVV               
                  /*  $dataProvider = GrGmvvT::model()->find('nacionalidad ILIKE :nacionalidad AND ci=:cedula AND st_censado = TRUE AND familia_id!=:familia_id',
                                        array('nacionalidad'=>$_POST['nac'],':cedula'=>trim($_POST['cedula']),':familia_id'=>$_POST['familia'] ));
                
                if($dataProvider){
                   
                    if ($dataProvider->parentesco_id==12){
                        $mensajeError1="JEFE";
                    }
                    else{
                        $mensajeError2="CARGA";
                    }
                }*/


                $buscar_beneficiado=BeneficiosSivihSiscavJefe::model()->find('cedula=:cedula  and estatus_desactivacion=\'ACTIVO\'',array(':cedula'=>$_POST['cedula']));
                        
                        if($buscar_beneficiado){
                                                    $_POST['nac']=="";
                                                    $_POST['cedula']="";
                                                    $_POST['nombre1'] = "";
                                                           
                                                    $mensajeError="Usted Posee Un Beneficio de Vivienda Activo";
                                                //}
                        }
                               
                               

                /***********Fin de la busqueda en la base de datos del sistema si esta ya registrado******************/
                                         
                /****************************BUSQUEDA EN LA TABLA INHABILITADOS**************************************/
            /*             
              $inhabilitados = PerInhabilitados::model()->find('cedula=:cedula AND nacionalidad=:nac AND estatus=true',array(':cedula'=>$_POST['cedula'],':nac'=>$_POST['nac']));         
               if($inhabilitados)
                   $mensajeNotificacion.="<li>Esta persona se encuentra inhabilitada, se podrá registrar con estatus inhabilitado</li>";
               */
                               


                    if($mensajeError!=''){
                                 
                        $mensajeError="Usted Posee Un Beneficio de Vivienda Activo";
                               
                                    Yii::app()->user->setFlash('error', mb_strtoupper('<ul>'.$mensajeError.'</ul>',"utf-8"));
                                     $Busqueda['tipo'] = 'error';
                                }

                               
                   if($mensajeError1!='' && $mensajeError2!=''){
                                   $mensajeError= "<li>Esta cédula ya esta registrada como Jefe de Familia y Carga Familiar ";
                               
                                    Yii::app()->user->setFlash('error', mb_strtoupper('<ul>'.$mensajeError.'</ul>',"utf-8"));
                    $Busqueda['tipo'] = 'error';
                                }
                               
                                if($mensajeError1!='' && $mensajeError2==''){
                                   $mensajeError= "<li>Esta cédula ya esta registrada como Jefe de Familia";
                               
                                    Yii::app()->user->setFlash('error', mb_strtoupper('<ul>'.$mensajeError.'</ul>',"utf-8"));
                    $Busqueda['tipo'] = 'error';
                                }
                               
                                 if($mensajeError1=='' && $mensajeError2!=''){
                                   $mensajeError= "<li>Esta cédula ya esta registrada como Carga Familiar";
                               
                                    Yii::app()->user->setFlash('notice', mb_strtoupper('<ul>'.$mensajeError.'</ul>',"utf-8"));
                    $Busqueda['tipo'] = 'notice';
                                }
                   
               
                if($mensajeNotificacion!=''){
                    Yii::app()->user->setFlash('notice', mb_strtoupper('<ul>'.$mensajeNotificacion.'</ul>',"utf-8"));
                    $Busqueda['tipo'] = 'error';
                }
                $mensaje1 ='';   
                foreach(Yii::app()->user->getFlashes() as $key => $message) {
                     $mensaje1.=  '<div class="flash-' . $key . '" style="padding: 6px 0px 0px 0px;">' . $message . "</div>\n";
                }
               
                $Busqueda['mensaje'] = $mensaje1;
              
              
              
               /****************************FIN BUSQUEDA EN LA TABLA INHABILITADOS**************************************/
               echo CJSON::encode($Busqueda);

               }

               Yii::app()->end();
       }  
     private function validarGetId($id){
               $id= preg_replace("[^0-9]", "", $id);
            if(is_numeric($id)){
                   if($id<1)
                       $this->redirect(array('/'));
                   if(strlen($id)>18)
                       $this->redirect(array('/'));
            }
            else{
               
                $this->redirect(array('/'));
            }
       }
}