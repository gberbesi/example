<?php

class UsuarioController extends Controller
{
	public $recu;
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
				'testLimit'=>1,
			)
		);
	}
				

	public function actionLogin() { 
		if(!Yii::app()->user->isGuest) {
			//$this->redirect(array('/'.$this->module->id.'/censo/index'));
			
			$usuario = UsuUsuario::model()->with('persona')->findByPk(Yii::app()->user->id);
			if($usuario && $usuario->persona) {
				$cedula = (string)$usuario->persona->cedula;
			}

			$personagmvv = GrGmvvT::model()->findByAttributes(array('ci'=>$cedula));
			$personapsm = Censado::model()->findByAttributes(array('cedula'=>$cedula));

			if(!$personagmvv && !$personapsm) {
				$this->redirect(array('/'.$this->module->id.'/censo/chose2'));
			
			} elseif($personapsm || $personagmvv) {

				$this->redirect(array('/'.$this->module->id.'/censo/chose'));
			}
		}
		
		Yii::import('application.modules.reportes.models.CdlpWebService', true);
		Yii::import('application.modules.reportes.models.nusoap_client', true);
		
		//modelos registro
		$modelUsuario=new UsuUsuario();
		$modelPersona=new PerPersona();
		$modelTelefono=new PerTelefono();
		
		
		
		//var_dump($modelPersona->flag!='43');exit;
		//echo '<pre>';print_r($webService->buscarCDLPCedulaCodigo('V17428863','0001449670'));exit;
		
		//modelos login
		$model=new LoginForm();
		$Captcha=new Captcha();
		
		//modelos recuperar
		$modelRecuperar=new RecuperarForm();
		
		// if it is ajax validation request

		//$valida=$this->performAjaxValidation($modelPersona,$modelTelefono);

		//logica login
		if(isset($_POST['LoginForm'])) {
			$model->attributes=$_POST['LoginForm'];
			$Captcha->attributes=$_POST['Captcha'];
			// validate user input and redirect to the previous page if valid

			$idusuario = Yii::app()->user->id;
			
			

			if($Captcha->validate()&&$model->validate() && $model->login()) {

				$usuario = UsuUsuario::model()->with('persona')->findByPk($idusuario);
				if($usuario && $usuario->persona) {
					$cedula = (string)$usuario->persona->cedula;
				}

				$personagmvv = GrGmvvT::model()->findByAttributes(array('ci'=>$cedula));
				$personapsm = Censado::model()->findByAttributes(array('cedula'=>$cedula));

				if(!$personagmvv && !$personapsm) {
					$this->redirect(array('/'.$this->module->id.'/censo/chose2'));
				
				} elseif($personapsm || $personagmvv) {

					$this->redirect(array('/'.$this->module->id.'/censo/chose'));
				}
			}


			/*	if($Captcha->validate()&&$model->validate() && $model->login()) {
				$this->redirect(Yii::app()->user->getReturnUrl(array('/'.$this->module->id.'/censo/index')));
			}*/
		}
		
		//logica registro
		$mostrarCatcha = true;
		$mensajeGeneral = NULL;
		if(isset($_POST['PerPersona'])) {
			
			$modelPersona->scenario= 'regitroCenso';
			
			$webService = new CdlpWebService;

			
			$Captcha->attributes=$_POST['Captcha'];
			$modelPersona->attributes=$_POST['PerPersona'];
			$modelTelefono->attributes=$_POST['PerTelefono'];
			
		
			$valido = true;
			$valido = $Captcha->validate() && $valido;
			if($valido) {
				$valido = $modelPersona->validate(array('codigo_carnetdlp','nacionalidad','cedula')) && $valido;
				
				if($valido) {
					$respService = $webService->buscarCDLPCedulaCodigo($modelPersona->nacionalidad.str_pad(trim($modelPersona->cedula),8,'0',STR_PAD_LEFT),$modelPersona->codigo_carnetdlp);
					if($respService['codigo_respuesta']===1 && $modelPersona->flag!=43) {
						$modelPersona->nombre1 = $respService['data']['nombre1'];
						$modelPersona->apellido1 = $respService['data']['apellido1'];
						$modelPersona->fe_nac = implode("/",array_reverse(explode("-",$respService['data']['fecha_nacimiento'])));
						$modelPersona->e_mail = $respService['data']['correo'];
						$modelPersona->flag = '43'; //bandera para saber que valido cedula con codigo del carnet
						$modelTelefono->nu_telefono =  $respService['data']['telf1'];
						
						$valido = $modelPersona->validate() && $valido;
						$valido = $modelTelefono->validate() && $valido;
						$valido = false;
						$mostrarCatcha = false;
						$Captcha->verifyCode = Yii::app()->session['Yii.CCaptchaAction.'.Yii::app()->getId().'.'.$this->getUniqueId().'.captcha'];
					} elseif($respService['codigo_respuesta']!==1) {
						if(isset($respService['codigo_respuesta'])) {
							$valido = false;
							$modelPersona->flag=NULL;
							$modelPersona->addError('cedula','');
							$modelPersona->addError('codigo_carnetdlp','');
							$mensajeGeneral = 'Los datos suministrados no coinciden, Por favor Verifique';
						} else {
							$valido = false;
							$modelPersona->flag=NULL;
							$modelPersona->addError('cedula','');
							$modelPersona->addError('codigo_carnetdlp','');
							$mensajeGeneral = 'En estos momentos el servicio no se encuentra disponible. Por favor, intente más tarde';
						}
						
					} else {
						$modelPersona->nombre2 = $respService['data']['nombre2'];
						$modelPersona->apellido2 = $respService['data']['apellido2'];
						$modelPersona->sexo = $respService['data']['genero'];
					}
				} else {
					$modelPersona->flag=NULL;
				}
				
				if($valido) {
					$valido = $modelPersona->validate() && $valido;
					$valido = $modelTelefono->validate() && $valido;
				}
			}
			
			
			
			if($valido) {
				
				$transaction = Yii::app()->db->beginTransaction();
				try {

					$modelUsuario->contrasena = $modelPersona->claveAleatoria();
					
					$personaUsuario = PerPersona::model()->find('cedula =:cedula', array(':cedula' => $modelPersona->cedula));
					
					if (!$personaUsuario) {
						
						$modelPersona->fe_nac = implode("-",array_reverse(explode("/",$modelPersona->fe_nac)));
						$modelPersona->save(FALSE);
						
						$personaUsuario = $modelPersona;

					} else {

						$personaUsuario->nacionalidad = $modelPersona->nacionalidad;
						$personaUsuario->nombre1 = $modelPersona->nombre1;
						$personaUsuario->apellido1 = $modelPersona->apellido1;
						$personaUsuario->fe_nac = implode("-",array_reverse(explode("/",$modelPersona->fe_nac)));
						$personaUsuario->e_mail = $modelPersona->e_mail;
						$personaUsuario->codigo_carnetdlp = $modelPersona->codigo_carnetdlp;
						
						$personaUsuario->nombre2 = $modelPersona->nombre2;
						$personaUsuario->apellido2 = $modelPersona->apellido2;
						$personaUsuario->sexo = $modelPersona->sexo;
						
						$personaUsuario->save(FALSE);

					}
					
					if ($modelTelefono->nu_telefono != '') {

						$buscar = array("(", ")", "-");
						$nu_telefono = str_replace($buscar, '', $modelTelefono->nu_telefono);
						$modelTelf = PerTelefono::model()->find('persona_id=:persona_id ORDER BY telefono_id', array(':persona_id' => $personaUsuario->persona_id));
						if (!$modelTelf) {
							$telef = NEW PerTelefono();
							$telef->persona_id = $personaUsuario->persona_id;
							$telef->nu_telefono = $nu_telefono;
							$telef->save();
						} else {
							$modelTelf->nu_telefono = $nu_telefono;
							$modelTelf->persona_id = $personaUsuario->persona_id;
							$modelTelf->save();
						}
					}
					
					
					$usuExiste = UsuUsuario::model()->find('nb_usuario=:nb_usuario AND persona_id!=:persona_id', array(':nb_usuario' => $personaUsuario->codigo_carnetdlp,':persona_id'=>$personaUsuario->persona_id));
					if ($usuExiste) {
						do {
							$personaUsuario->codigo_carnetdlp = '0' . $personaUsuario->codigo_carnetdlp;
						} while(UsuUsuario::model()->find('nb_usuario=:nb_usuario AND persona_id!=:persona_id', array(':nb_usuario' => $personaUsuario->codigo_carnetdlp,':persona_id'=>$personaUsuario->persona_id)));
						$nbUsurio = $personaUsuario->codigo_carnetdlp;
					} else {
						$nbUsurio =  $personaUsuario->codigo_carnetdlp;
					}
					
					$usuario = UsuUsuario::model()->find('persona_id=:persona_id', array(':persona_id' => $personaUsuario->persona_id));
					if ($usuario) {

						$usuario->st_usuario = TRUE;
						$usuario->nb_usuario = $nbUsurio;
						$usuario->contrasena = md5($modelUsuario->contrasena);
						$usuario->usu_aud_id = 0;
						$usuario->actualizadocdlp=true;
						$usuario->save();
				

					} else {
						

						$usuario = NEW UsuUsuario();
						$usuario->persona_id = $personaUsuario->persona_id;
						$usuario->nb_usuario = $nbUsurio;
						$usuario->contrasena = md5($modelUsuario->contrasena);
						$usuario->actualizadocdlp=true;
						$usuario->usu_aud_id = 0;
						$usuario->st_usuario = TRUE;
						$usuario->save();
						
						AuthAssignment::model()->deleteAll('userid=:userid', array(':userid' => (string) $usuario->usuario_id));
						
						$auth = Yii::app()->authManager;
						$auth->assign('ActualizacionCenso', $usuario->usuario_id);
						
					}
					$usuario->cod_activacion = md5($modelUsuario->contrasena.$modelPersona->e_mail.$usuario->persona_id);
					$usuario->email_verificado = false;
					$usuario->save(false);


					
					
					
					//GrGmvvT::model()->updateAll(array('e_mail' => $modelPersona->e_mail, 'telf_movil' => $nu_telefono), 'ci=:ci', array(':ci' => $modelPersona->cedula));
					//Censado::model()->updateAll(array('e_mail' => $modelPersona->e_mail, 'telefono_1' => $nu_telefono), 'cedula=:cedula', array(':cedula' => $modelPersona->cedula));
					$urlLogin = Yii::app()->getBaseUrl(true).'/index.php?r='.$this->module->id.'/'.$this->id.'/'.$this->action->id.'#contact-us';
					$mensaje = '<table border="0">
						  <tr>
							<th bgcolor="#990000" scope="col">&nbsp;</th>
						  </tr>
						  <tr>
							<td>
								<p>
								  <h4>Estimado(a) ' . $usuario->persona->nombre1 . ' ' . $usuario->persona->apellido1 . ', 
									su solicitud de usuario para registro o actualización de los censos de la GMVV, se ha realizado exitosamente.   </h4></p>
							<p>Para finalizar con el proceso por favor confirma tu direcci&oacute;n de email haciendo click en el siguiente enlace: </p>
							<p><a href="http://'.$_SERVER["SERVER_NAME"].$this->createUrl('usuario/activaremail',array('id'=>$usuario->cod_activacion)).'" target="_blank">http://'.$_SERVER["SERVER_NAME"].$this->createUrl('usuario/activaremail',array('id'=>$usuario->cod_activacion)).'</a></p>
							<p>

									  
									Le informamos que se ha generado una clave aleatoria, sugerimos ingresar al sistema y realizar el cambio de la misma. 
								  <br />
									<b>Usuario</b>: ' . $usuario->nb_usuario . '<br />
									<b>Contrase&ntilde;a</b>: ' . $modelUsuario->contrasena . '<br />
									<br />
										Puede ingresar al sistema a través de la dirección:                                         
										'.$urlLogin.'
										
										<br>
									¡Muchas gracias!<br />
									MINHVI </p>
							   <p><h5>IMPORTANTE: Recuerde que estos datos son confidenciales e intransferibles. Por favor no responder a esta direcci&oacute;n de correo, es una cuenta de correo no monitoreada.</h5></p>
							</td>
						  </tr>
						</table>';

					

				
					
					
					if ($usuario->persona->e_mail != '') {
                            $modelPersona->correoActualizacion($usuario->persona->e_mail, $user->persona->nombre1, $mensaje);
					}

					$msmTelefono = NEW Mensajes();
					$msm = "MINHVI informa: Su usuario y contraseña para ingresar al Sistema de Actualización de Datos en GMVV y/o 0800 Mi Hogar son:\nUsuario: " . $usuario->nb_usuario . "\nContraseña: " . $modelUsuario->contrasena;
					$msmTelefono->mensaje = $msm;
					$msmTelefono->sistema = 'SIGEVIH';
					$msmTelefono->numero = $nu_telefono;
					$msmTelefono->save();
					
					
					$transaction->commit();
					
						
					Yii::app()->user->setFlash('success', "Registro exitoso. Verifique su usuario y contraseña en su correo electrónico y/o teléfono celular. \\n\\nEn caso de no visualizar el correo en su bandeja de entrada, verifique la bandeja de SPAM\\n");
					
					$this->redirect(array('login#contact-us'));
					exit;
				} catch (Exception $e) {
					$transaction->rollBack();
					throw new CHttpException($e->getLine(), $e->getMessage());
				}
				
			}
			//echo '<pre>';print_r(Yii::app()->session['Yii.CCaptchaAction.'.Yii::app()->getId().'.'.$this->getUniqueId().'.captcha']);exit;

			Yii::app()->clientScript->registerScript('modal', "
		
				$('#modal_registro').modal('show');
				
			", CClientScript::POS_READY);
			
		}
		
		
		
		if(isset($_POST['RecuperarForm'])) {
			$modelRecuperar->attributes=$_POST['RecuperarForm'];
			$Captcha->attributes=$_POST['Captcha'];
			// validate user input and redirect to the previous page if valid


			if($Captcha->validate()&&$modelRecuperar->validate() ) {

			$recu=true;
		$urlLogin = Yii::app()->getBaseUrl(true).'/index.php?r='.$this->module->id.'/'.$this->id.'/'.$this->action->id.'#contact-us';
				//var_dump($modelRecuperar->cedula);exit();
		//$_POST['RecuperarForm'];
		$consultaper = PerPersona::model()->findByAttributes(array('cedula'=>(int)$modelRecuperar->cedula));
		$consultausu = UsuUsuario::model()->findByAttributes(array('persona_id'=>$consultaper->persona_id));

		//var_dump($consultaper);exit();
		if($consultaper && $consultausu->email_verificado ){

			//$consultaper->e_mail;
			$consultausu = UsuUsuario::model()->findByAttributes(array('persona_id'=>$consultaper->persona_id));
			
			if(strlen($consultausu->nb_usuario)!=10) {
				$consultausu->nb_usuario = str_pad(ltrim($consultausu->nb_usuario, "0"), 10, "0",STR_PAD_LEFT);
				$consultaper->codigo_carnetdlp=$consultausu->nb_usuario;
				$consultaper->save(false);
			}
			
			$recupe=$modelRecuperar->claveAleatoria();

			$consultausu->contrasena = md5($recupe);

			$consultausu->save(false);

			$mensaje = '<table border="0">
						  <tr>
							<th bgcolor="#990000" scope="col">&nbsp;</th>
						  </tr>
						  <tr>
							<td>
								<p>
								  <h4>Estimado(a) ' . $consultaper->nombre1 . ' ' . $consultaper->apellido1 . ', 
									su contraseña ha sido recuperada exitosamente!!!<br>
									Le informamos que se ha generado una clave aleatoria, sugerimos ingresar al sistema y realizar el cambio de la misma. 
								  <br />
									<b>Usuario</b>: ' . $consultausu->nb_usuario . '<br />
									<b>Contrase&ntilde;a</b>: ' .$recupe. '<br />
									<br />
										Puede ingresar al sistema a través de la dirección:                                         
										'.$urlLogin.'
										
										<br>
									¡Muchas gracias!<br />
									MINHVI
							   <p><h5>IMPORTANTE: Recuerde que estos datos son confidenciales e intransferibles. Por favor no responder a esta direcci&oacute;n de correo, es una cuenta de correo no monitoreada.</h5></p>
							</td>
						  </tr>
						</table>';

			//var_dump($consultausu);exit();

					if ($consultaper->e_mail != '') {
                            $consultaper->claveRecuperacion($consultaper->e_mail, $consultaper->nombre1, $mensaje);
					}
					$email=$consultaper->e_mail;
					$email = explode('@', $email);
					$len = strlen($email[0]);
					$emailc=substr($email[0], 0, $len-4).'****@'.$email[1];
					


					Yii::app()->user->setFlash('success', " Verifique nuevamente su usuario y contraseña en su correo electrónico ".$emailc."  \\n\\nEn caso de no visualizar el correo en su bandeja de entrada, verifique la bandeja de SPAM\\n");
		//cierre if


		}elseif(!$consultaper){
			Yii::app()->user->setFlash('error', "Esta Cédula No se encuentra Registrada, por favor verifíquela.");
		}elseif(!$consultausu->actualizadocdlp){
			Yii::app()->user->setFlash('error', "Usuario desactualizado, ingrese a traves de la opción Registro de Usuario y complete el formulario, sus datos de ingreso serán enviados a su dirección de correo electrónico");
		}		elseif(!$consultausu->email_verificado){
			Yii::app()->user->setFlash('error', "Su correo electrónico ".$emailc." aun no ha sido confirmado, ingrese a su bandeja de entrada o a la bandeja de SPAM y presione el enlace suministrado. Si no ha recibido el mensaje presione nuevamente el botón Registro de Usuario.");
		}
			



			}
			if(!$recu){
			Yii::app()->clientScript->registerScript('modal', "
		
				$('#modal_recuperar').modal('show');
				
			", CClientScript::POS_READY);
		}

		}
		
		
		if($mostrarCatcha) {
			$Captcha->verifyCode='';
		}
		
		
		// display the login form
		$this->render('login',array(
				'model'=>$model,
				'Captcha'=>$Captcha,
				'mostrarCatcha'=>$mostrarCatcha,
				'modelPersona'=>$modelPersona,
				'modelTelefono'=>$modelTelefono,
				'valida'=>$valida,
				'mensajeGeneral'=>$mensajeGeneral,
				'modelRecuperar'=>$modelRecuperar,
		));
	}
	
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->user->getReturnUrl(array('/'.$this->module->id.'/censo/index')));
	}
	
	public function actionActivarEmail($id)
	{
		$fff=UsuUsuario::model()->find('cod_activacion=:cod_activacion', array(':cod_activacion' => $id));

		if($fff){
				$fff->cod_activacion=null;
				$fff->email_verificado=true;
				$fff->save(false);

				Yii::app()->user->setFlash('success', " Ya puedes ingresar al sistema con tus datos de acceso \\n");

		}else{

			Yii::app()->user->setFlash('error', "Código no existe o Usuario ya fue activado.");
		}

		$this->redirect(array('/'.$this->module->id.'/usuario/login'));
	}
	public function actionRegistro() {
		
	}
		
	protected function performAjaxValidation($modelPersona,$modelTelefono){

		
	if($_POST['PerPersona']['cedula']!="" && $_POST['PerPersona']['fe_nac']!=""){


if( preg_match('/[0-9]/', $_POST['PerPersona']['cedula'])) {


	

			$ci=PerPersona::model()->find("cedula=".$_POST['PerPersona']['cedula']." and fe_nac='".$_POST['PerPersona']['fe_nac']."'");
			$id=Onidex::model()->find("cedula=".$_POST['PerPersona']['cedula']." and fecha_nac='".$_POST['PerPersona']['fe_nac']."'");

			if($ci==null&&$id==null){

				return "Cédula y fecha de nacimiento no coinciden";
			}

			else if($ci){
				$modelPersona->attributes=$_POST['PerPersona'];
				$modelTelefono->attributes=$_POST['PerTelefono'];
				$modelPersona->nombre1=$ci->nombre1;
				$modelPersona->apellido1=$ci->apellido1;
				$modelPersona->e_mail=$ci->e_mail;
				$modelPersona->validate();
				
			}else if($id){
				$modelPersona->attributes=$_POST['PerPersona'];
				$modelTelefono->attributes=$_POST['PerTelefono'];
				$modelPersona->nombre1=$id->nombre1;
				$modelPersona->apellido1=$id->apellido1;
				$modelPersona->validate();
			}

			
		}
	}
	}
public function actionRecover() { 
	$modelUsuario=new UsuUsuario();
		$modelPersona=new PerPersona();
		$modelTelefono=new PerTelefono();
			
			      $this->layout='/layouts/recoverla';
			      /*
		$mensaje = '<table border="0">
						  <tr>
							<th bgcolor="#990000" scope="col">&nbsp;</th>
						  </tr>
						  <tr>
							<td>
								<p>
								  <h4>Estimado(a) ' . $usuario->persona->nombre1 . ' ' . $usuario->persona->apellido1 . ', 
									su solicitud de usuario para registro o actualización de los censos de la GMVV, se ha realizado exitosamente, 
									le informamos que se ha generado una clave aleatoria, sugerimos ingresar al sistema y realizar el cambio de la misma. 
								  <br />
									<b>Usuario</b>: ' . $usuario->nb_usuario . '<br />
									<b>Contrase&ntilde;a</b>: ' . $modelUsuario->contrasena . '<br />
									<br />
										Puede ingresar al sistema a través de la dirección:                                         
										'.$urlLogin.'
										
										<br>
									¡Muchas gracias!<br />
									MINHVI
							   <p><h5>IMPORTANTE: Recuerde que estos datos son confidenciales e intransferibles. Por favor no responder a esta direcci&oacute;n de correo, es una cuenta de correo no monitoreada.</h5></p>
							</td>
						  </tr>
						</table>';
*/

			$this->render('recover',array('modelPersona'=>$modelPersona,
				'modelTelefono'=>$modelTelefono,));
	 
					}


	
	

	



	 public function actionCedulaCensado() {


        Yii::import('application.modules.censo.models.BusquedaPersonaCensos', true);
        yii::import('application.modules.consulta_ben.models.BeneficiosSivihSiscavJefe', true); /* MODIFICACION "IMPORTACION PARA VEFICICACION DE BENEFICIARIOS" */
        Yii::import('application.modules.familia.models.Onidex', true);
        Yii::import('application.modules.censoreload.models.PerPersona', true);
        header('Content-type: application/json');
        
        //VALOR ES TRUE CUANDO NO SE DEBE CONSIDERAR EN CASO QUE POSEA UN BENEFICIO, DADO QUE SE LE VA A ASIGNAR EL ROL DE ACTUALIZACIONCENSO
        if (isset($_POST['valor']) && $_POST['valor'] == 1 ) $valor = true;
        else $valor = false;
        
        if (trim($_POST['cedula']) == "" && trim($_POST['nac']) == "" && !is_numeric($_POST['cedula']) && trim($_POST['fe_nac']) == "") {
            echo '{"datos":"ERROR"}';
        } else {
            $mensajeError = '';
            $mensajeNotificacion = '';
            $Busqueda = array();
            $onidex = Onidex::model()->find('nac ILIKE :nac AND cedula =:cedula AND to_char(fecha_nac, \'DD/MM/YYYY\')=:fecha_nac', array(':nac' => $_POST['nac'],
                ':cedula' => trim($_POST['cedula']),
                ':fecha_nac' => $_POST['fe_nac']));

$ci=PerPersona::model()->find("cedula=".$_POST['cedula']."and fe_nac='".$_POST['fe_nac']."'");

          if($ci){
          			 $Busqueda['nombre1'] = $ci->nombre1;
                	$Busqueda['apellido1'] = $ci->apellido1;
                	$Busqueda['e_mail'] = $ci->e_mail;
                	$Busqueda['mensaje'] = "";


                echo CJSON::encode($Busqueda);

          }
           else if ($onidex) {


                $Busqueda['nombre1'] = $onidex->nombre1;
                $Busqueda['apellido1'] = $onidex->apellido1;
                $Busqueda['mensaje'] = "";

                echo CJSON::encode($Busqueda);
        }else {
                
                 $Busqueda['nombre1'] = "";
                	$Busqueda['apellido1'] = "";
                	$Busqueda['e_mail'] = "";
                	$Busqueda['mensaje'] = "Los datos suministrados no coinciden, Por favor Verifique";
                	 echo CJSON::encode($Busqueda);
            }

        Yii::app()->end();
    }
}
	
}