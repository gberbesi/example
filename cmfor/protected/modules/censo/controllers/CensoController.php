<?php

class CensoController extends Controller
{
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control f CRUD operations
		);
	}
	
	public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index','clave','chose','chose2'),
                'users'=>array('@'),
            ),
            //array('allow','actions' => array('ActualizarDatos'), 'roles' => array('censo/Censado/ActualizarDatos')),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
	
	

	public function actionIndex($cedulajf=NULL)
	{
		
		if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")) {
			
			$cedula=(string)$cedulajf;
		
		} else {
			
			$idusuario = Yii::app()->user->id;
			
			$usuario = UsuUsuario::model()->with('persona')->findByPk($idusuario);
			if($usuario && $usuario->persona) {
				$cedula=(string)$usuario->persona->cedula;
			} else {
				//persona no tiene cedula o usuario no existe
				exit;
			}
			
		}
		
		

		//0800
		$censoAll = Censado::model()->findAllByAttributes(array('cedula'=>$cedula));
		$familias_id = array();
		foreach($censoAll as $key=>$cen) {
			if (in_array($cen->familia_id, $familias_id)) {
				unset($censoAll[$key]);
			} else {
				$familias_id[] = $cen->familia_id;
			}
		}
		/*
		$censo = Censado::model()->findByAttributes(array('cedula'=>$cedula,'parentesco_id'=>12));
		$censoFamilia=array();
		if($censo) {
			$censoFamilia = Censado::model()->findAll(array('condition'=>'familia_id=:familia_id','params'=>array(':familia_id'=>$censo->familia_id),'order'=>'parentesco_id=12 DESC'));
		}
		*/
		
		//GMVV
		$gmvvAll = GrGmvvT::model()->findAllByAttributes(array('ci'=>$cedula));
		$familias_id = array();
		foreach($gmvvAll as $key=>$gm) {
			if (in_array($gm->familia_id, $familias_id)) {
				unset($gmvvAll[$key]);
			} else {
				$familias_id[] = $gm->familia_id;
			}
		}
		
		/*
		$gmvv = GrGmvvT::model()->findByAttributes(array('ci'=>$cedula,'parentesco_id'=>12));
		$gmvvFamilia=array();
		if($gmvv) {
			$gmvvFamilia = GrGmvvT::model()->findAll(array('condition'=>'familia_id=:familia_id','params'=>array(':familia_id'=>$gmvv->familia_id),'order'=>'parentesco_id=12 DESC'));
		}
		*/
		
		//echo '<pre>';print_r($gmvv->attributes);exit;
		$this->render('index',array(
					'censoAll'=>$censoAll,
					//'censoFamilia'=>$censoFamilia,
					'gmvvAll'=>$gmvvAll,
					//'gmvvFamilia'=>$gmvvFamilia,
					'cedulajf'=>$cedulajf,
					));
	}
	
	
	public function actionClave() {


        $model = new UsuUsuario('cambio');

        if (isset($_POST['UsuUsuario'])) {
            $valido = true;
            $model->attributes = $_POST['UsuUsuario'];
            $valido = $model->validate(array('repetirnueva', 'contrasena', 'nueva')) && $valido;
            $user = UsuUsuario::model()->findByAttributes(array('usuario_id' => Yii::app()->user->id));
            if ($user) {
                if ($model->contrasena != "" && ($user->contrasena != md5($model->contrasena))) {//die('1');
                    $model->addError('contrasena', 'Contraseña actual incorrecta.');
                    $valido = false;
                }
            }
            if ($valido) {
                $transaction = $model->dbConnection->beginTransaction();
                try {
                    UsuUsuario::model()->updateAll(
                            array('contrasena' => md5($model->nueva), 'usu_aud_id' => Yii::app()->user->id), 'usuario_id=:usuario_id', array(':usuario_id' => Yii::app()->user->id));

                    $transaction->commit(); //die();
                    Yii::app()->user->setFlash('success', "Cambio de clave exitoso.");
                } catch (Exception $e) {
                    $transaction->rollBack();
                    throw new CHttpException($e->getCode(), $e->getMessage());
                }
                $this->redirect(array('clave'));
            }
        }

        $this->render('clave', array(
            'model' => $model,
        ));
    }

    public function actionChose($cedulajf=NULL){
	
		
		if($cedulajf!=NULL && Yii::app()->user->checkAccess("censo/Familia/SearchAdmin")) {
			
			$cedula=(string)$cedulajf;
		
		} else {
			
			$idusuario = Yii::app()->user->id;
			
			$usuario = UsuUsuario::model()->with('persona')->findByPk($idusuario);
			if($usuario && $usuario->persona) {
				$cedula=(string)$usuario->persona->cedula;
			
			} else {
				//persona no tiene cedula o usuario no existe
				exit;
			}
			$censoAll = Censado::model()->findAllByAttributes(array('cedula'=>$cedula));
			$gmvvAll = GrGmvvT::model()->findAllByAttributes(array('ci'=>$cedula));
			if($censoAll!=null){

			$this->redirect(array('/'.$this->module->id.'/censo/chose2'));

			}elseif ($gmvvAll!=null){
				$this->redirect(array('/'.$this->module->id.'/censo/chose2'));
			}
		
			$this->render('chose');
	 
}}



public function actionChose2($cedulajf=NULL){
	
		
	
			$this->render('chose2');
	 
}
}