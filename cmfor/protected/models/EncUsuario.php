<?php

/**
 * This is the model class for table "encuesta.enc_usuario".
 *
 * The followings are the available columns in table 'encuesta.enc_usuario':
 * @property string $enc_usuario_id
 * @property string $usuario
 * @property string $clave
 * @property integer $id_vocero
 * @property integer $id_promotor
 */
class EncUsuario extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EncUsuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.enc_usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usuario, clave', 'required'),
			array('id_vocero, id_promotor', 'numerical', 'integerOnly'=>true),
			array('usuario, clave', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('enc_usuario_id, usuario, clave, id_vocero, id_promotor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'enc_usuario_id' => 'Enc Usuario',
			'usuario' => 'Usuario',
			'clave' => 'Clave',
			'id_vocero' => 'Id Vocero',
			'id_promotor' => 'Id Promotor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('enc_usuario_id',$this->enc_usuario_id,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('clave',$this->clave,true);
		$criteria->compare('id_vocero',$this->id_vocero);
		$criteria->compare('id_promotor',$this->id_promotor);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}