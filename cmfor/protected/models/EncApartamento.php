<?php

/**
 * This is the model class for table "encuesta.enc_apartamento".
 *
 * The followings are the available columns in table 'encuesta.enc_apartamento':
 * @property string $enc_apartamento_id
 * @property integer $piso_id
 * @property integer $urbanismo
 * @property integer $etapa
 * @property integer $torre
 * @property string $nomesclatura
 */
class EncApartamento extends CActiveRecord
{	
	public $apartamento;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.enc_apartamento';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('piso_id, urbanismo, etapa, torre, nomesclatura', 'required'),
			array('piso_id, urbanismo, etapa, torre', 'numerical', 'integerOnly'=>true),
			array('nomesclatura', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('enc_apartamento_id, piso_id, urbanismo, etapa, torre, nomesclatura', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'enc_apartamento_id' => 'ID',
			'piso_id' => 'Piso',
			'urbanismo' => 'Urbanismo',
			'etapa' => 'Etapa',
			'torre' => 'Torre',
			'nomesclatura' => 'Nomenclatura',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('enc_apartamento_id',$this->enc_apartamento_id,true);
		$criteria->compare('piso_id',$this->piso_id);
		$criteria->compare('urbanismo',$this->urbanismo);
		$criteria->compare('etapa',$this->etapa);
		$criteria->compare('torre',$this->torre);
		$criteria->compare('nomesclatura',$this->nomesclatura,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EncApartamento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
