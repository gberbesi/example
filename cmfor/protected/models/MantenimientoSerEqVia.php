<?php

/** 
 * This is the model class for table "encuesta.mantenimiento_ser_eq_via".
 *
 * The followings are the available columns in table 'encuesta.mantenimiento_ser_eq_via':
 * @property string $mantenimiento_urb_id
 * @property integer $enc_urbanismo_id
 * @property boolean $parques_plazas
 * @property boolean $canchas_deportivas
 * @property boolean $educativa_prescolar
 * @property boolean $educativa_básica_media
 * @property boolean $escuela_tecnica_robinsoniana
 * @property boolean $infocentro
 * @property boolean $locales_industria
 * @property boolean $locales_comercios
 * @property boolean $centros_comunicación
 * @property boolean $centros_salud
 * @property boolean $centro_diagnóstico
 * @property boolean $módulos_policiales
 * @property boolean $casa_omunitaria
 * @property boolean $centros_religiosos
 * @property boolean $centros_culturales
 * @property boolean $centros_electorales
 * @property boolean $avenidas_calles_internas
 * @property boolean $aceras_brocales
 * @property boolean $vialidad_acceso_urbanismo
 */
class MantenimientoSerEqVia extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MantenimientoSerEqVia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.mantenimiento_ser_eq_via';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('mantenimiento_urb_id, enc_urbanismo_id, parques_plazas, canchas_deportivas, educativa_prescolar, educativa_básica_media, escuela_tecnica_robinsoniana, infocentro, locales_industria, locales_comercios, centros_comunicación, centros_salud, centro_diagnóstico, módulos_policiales, casa_omunitaria, centros_religiosos, centros_culturales, centros_electorales, avenidas_calles_internas, aceras_brocales, vialidad_acceso_urbanismo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mantenimiento_urb_id' => 'Mantenimiento Urb',
			'enc_urbanismo_id' => 'Enc Urbanismo',
			'parques_plazas' => 'Parques Plazas',
			'canchas_deportivas' => 'Canchas Deportivas',
			'educativa_prescolar' => 'Unidad Educativa Preescolar',
			'educativa_básica_media' => 'Unidad Educativa Básica Media',
			'escuela_tecnica_robinsoniana' => 'Escuela Técnica Robinsoniana',
			'infocentro' => 'Infocentro',
			'locales_industria' => 'Locales Industriales',
			'locales_comercios' => 'Locales Comerciales',
			'centros_comunicación' => 'Centros de Comunicación',
			'centros_salud' => 'Centros de Salud',
			'centro_diagnóstico' => 'Centros Diagnósticos',
			'módulos_policiales' => 'Módulos Policiales',
			'casa_omunitaria' => 'Casa Comunitaria',
			'centros_religiosos' => 'Centros Religiosos',
			'centros_culturales' => 'Centros Culturales',
			'centros_electorales' => 'Centros Electorales',
			'avenidas_calles_internas' => 'Avenidas Calles Internas',
			'aceras_brocales' => 'Aceras Brocales',
			'vialidad_acceso_urbanismo' => 'Vialidad Acceso Urbanismo',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mantenimiento_urb_id',$this->mantenimiento_urb_id,true);
		$criteria->compare('enc_urbanismo_id',$this->enc_urbanismo_id);
		$criteria->compare('parques_plazas',$this->parques_plazas);
		$criteria->compare('canchas_deportivas',$this->canchas_deportivas);
		$criteria->compare('educativa_prescolar',$this->educativa_prescolar);
		$criteria->compare('educativa_básica_media',$this->educativa_básica_media);
		$criteria->compare('escuela_tecnica_robinsoniana',$this->escuela_tecnica_robinsoniana);
		$criteria->compare('infocentro',$this->infocentro);
		$criteria->compare('locales_industria',$this->locales_industria);
		$criteria->compare('locales_comercios',$this->locales_comercios);
		$criteria->compare('centros_comunicación',$this->centros_comunicación);
		$criteria->compare('centros_salud',$this->centros_salud);
		$criteria->compare('centro_diagnóstico',$this->centro_diagnóstico);
		$criteria->compare('módulos_policiales',$this->módulos_policiales);
		$criteria->compare('casa_omunitaria',$this->casa_omunitaria);
		$criteria->compare('centros_religiosos',$this->centros_religiosos);
		$criteria->compare('centros_culturales',$this->centros_culturales);
		$criteria->compare('centros_electorales',$this->centros_electorales);
		$criteria->compare('avenidas_calles_internas',$this->avenidas_calles_internas);
		$criteria->compare('aceras_brocales',$this->aceras_brocales);
		$criteria->compare('vialidad_acceso_urbanismo',$this->vialidad_acceso_urbanismo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}