<?php

/**
 * This is the model class for table "encuesta.enc_piso".
 *
 * The followings are the available columns in table 'encuesta.enc_piso':
 * @property string $enc_piso_id
 * @property integer $nu_piso
 * @property integer $torre_id
 * @property integer $id_uurbanismo
 * @property integer $etapa
 */
class EncPiso extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.enc_piso';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nu_piso, torre_id, id_uurbanismo, etapa', 'required'),
			array('nu_piso, torre_id, id_uurbanismo, etapa', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('enc_piso_id, nu_piso, torre_id, id_uurbanismo, etapa', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'enc_piso_id' => 'Enc Piso',
			'nu_piso' => 'Nu Piso',
			'torre_id' => 'Torre',
			'id_uurbanismo' => 'Id Uurbanismo',
			'etapa' => 'Etapa',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('enc_piso_id',$this->enc_piso_id,true);
		$criteria->compare('nu_piso',$this->nu_piso);
		$criteria->compare('torre_id',$this->torre_id);
		$criteria->compare('id_uurbanismo',$this->id_uurbanismo);
		$criteria->compare('etapa',$this->etapa);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EncPiso the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
