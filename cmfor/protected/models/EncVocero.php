<?php

/**
 * This is the model class for table "encuesta.enc_vocero".
 *
 * The followings are the available columns in table 'encuesta.enc_vocero':
 * @property string $enc_vocero_id
 * @property string $nacionalidad
 * @property integer $cedula
 * @property string $codigo_cdlp
 * @property string $nombre1
 * @property string $nombre2
 * @property string $apellido1
 * @property string $apellido2
 * @property integer $telefono1
 * @property string $correo
 * @property integer $torre
 * @property integer $id_urbanismo
 * @property string $etapa
 * @property integer $usuario_id
 * @property string $tipo
 */
class EncVocero extends CActiveRecord
{
public $arrayvoce_tipi;
public $arrayvoce_nom;
public $arrayvoce_ape;
public $arrayvoce_cedu;
public $arrayvoce_tel;
public $arrayvoce_corr;
public $arrayvoce_torr;
public $arrayvoce_apto;
public $arrayvoce_votos;
public $arrayvoce_nac;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EncVocero the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.enc_vocero';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('enc_vocero_id, nacionalidad, cedula, codigo_cdlp, nombre1, nombre2, apellido1, apellido2, telefono1, correo, torre, id_urbanismo, etapa, usuario_id, tipo,votos,apartamento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'enc_vocero_id' => 'Enc Vocero',
			'nacionalidad' => 'Nacionalidad',
			'cedula' => 'Cedula',
			'codigo_cdlp' => 'Codigo Cdlp',
			'nombre1' => 'Nombre',
			'nombre2' => 'Nombre2',
			'apellido1' => 'Apellido',
			'apellido2' => 'Apellido2',
			'telefono1' => 'Teléfono',
			'correo' => 'Correo',
			'torre' => 'Torre',
			'id_urbanismo' => 'Id Urbanismo',
			'etapa' => 'Etapa',
			'usuario_id' => 'Usuario',
			'tipo' => 'Tipo',
			'votos'=>'Cantidad de Votos Obtenidos',
			'apartamento'=>'Apartamento en el que reside',
			
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('enc_vocero_id',$this->enc_vocero_id,true);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('cedula',$this->cedula);
		$criteria->compare('codigo_cdlp',$this->codigo_cdlp,true);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('telefono1',$this->telefono1);
		$criteria->compare('correo',$this->correo,true);
		$criteria->compare('torre',$this->torre);
		$criteria->compare('id_urbanismo',$this->id_urbanismo);
		$criteria->compare('etapa',$this->etapa,true);
		$criteria->compare('usuario_id',$this->usuario_id);
		$criteria->compare('tipo',$this->tipo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}