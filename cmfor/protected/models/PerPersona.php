<?php

/**
 * This is the model class for table "per_persona".
 *
 * The followings are the available columns in table 'per_persona':
 * @property integer $persona_id
 * @property string $id_comite
 * @property string $nacionalidad
 * @property integer $cedula
 * @property string $nombre1
 * @property string $nombre2
 * @property string $apellido1
 * @property string $apellido2
 * @property string $fe_nac
 * @property string $sexo
 * @property string $e_mail
 * @property string $usu_aud_id
 * @property string $pasaporte
 * @property string $id_pais
 * @property integer $tiempo_pais
 * @property boolean $inhabilitado
 * @property boolean $cosolicitante
 * @property integer $per_telefono
 * @property string $codigo_carnetdlp
 */
class PerPersona extends CActiveRecord
{	
	public $sector;
	public $civil;
	public $porigen;
	public $aingre;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PerPersona the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.per_persona';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nacionalidad, nombre1, apellido1', 'required'),
			array('cedula, tiempo_pais', 'numerical', 'integerOnly'=>true),
			array('nacionalidad, sexo', 'length', 'max'=>1),
			array('id_comite, nombre2, apellido2, fe_nac, e_mail, usu_aud_id, pasaporte, id_pais, inhabilitado, cosolicitante, codigo_carnetdlp', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('persona_id, id_comite, nacionalidad, cedula, nombre1, nombre2, apellido1, apellido2, fe_nac, sexo, e_mail, usu_aud_id, pasaporte, id_pais, tiempo_pais, inhabilitado, cosolicitante, codigo_carnetdlp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'persona_id' => 'Persona',
			'id_comite' => 'Id Comite',
			'nacionalidad' => 'Nacionalidad',
			'cedula' => 'Cedula',
			'nombre1' => 'Primer Nombre',
			'nombre2' => 'Segundo Nombre',
			'apellido1' => 'Primer Apellido',
			'apellido2' => 'Segundo Apellido',
			'fe_nac' => 'Fecha Nacimiento',
			'sexo' => 'Género',
			'e_mail' => 'E Mail',
			'usu_aud_id' => 'Usu Aud',
			'pasaporte' => 'Pasaporte',
			'id_pais' => 'País De Origen',
			'tiempo_pais' => 'Tiempo Pais',
			'inhabilitado' => 'Inhabilitado',
			'cosolicitante' => 'Cosolicitante',
			'codigo_carnetdlp' => 'Codigo Carnet',
			'per_telefono'=>'Teléfono',

			'porigen'=>'País De Origen',
			'aingre'=>'Año de Ingreso',
			'edocivil'=>'Estado Civil'

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('persona_id',$this->persona_id);
		$criteria->compare('id_comite',$this->id_comite,true);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('cedula',$this->cedula);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('fe_nac',$this->fe_nac,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('e_mail',$this->e_mail,true);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('pasaporte',$this->pasaporte,true);
		$criteria->compare('id_pais',$this->id_pais,true);
		$criteria->compare('tiempo_pais',$this->tiempo_pais);
		$criteria->compare('inhabilitado',$this->inhabilitado);
		$criteria->compare('cosolicitante',$this->cosolicitante);
		$criteria->compare('codigo_carnetdlp',$this->codigo_carnetdlp,true);
		$criteria->compare('per_telefono',$this->per_telefono,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}