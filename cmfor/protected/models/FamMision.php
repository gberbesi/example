<?php

/**
 * This is the model class for table "encuesta.fam_mision".
 *
 * The followings are the available columns in table 'encuesta.fam_mision':
 * @property integer $mision_id
 * @property string $nombre
 * @property boolean $estatus
 * @property string $usu_aud_id
 * @property string $cod_mision
 */
class FamMision extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.fam_mision';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, estatus, usu_aud_id, cod_mision', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('mision_id, nombre, estatus, usu_aud_id, cod_mision', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mision_id' => 'Mision',
			'nombre' => 'Nombre',
			'estatus' => 'Estatus',
			'usu_aud_id' => 'Usu Aud',
			'cod_mision' => 'Cod Mision',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mision_id',$this->mision_id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('estatus',$this->estatus);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);
		$criteria->compare('cod_mision',$this->cod_mision,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FamMision the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
