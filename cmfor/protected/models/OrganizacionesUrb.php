<?php

/**
 * This is the model class for table "encuesta.organizaciones_urb".
 *
 * The followings are the available columns in table 'encuesta.organizaciones_urb':
 * @property string $organizaciones_urb_id
 * @property integer $enc_urbanismo_id
 * @property integer $organizacion_comunal
 * @property string $nacionalidad
 * @property integer $cedula
 * @property string $nombre1
 * @property string $nombre2
 * @property string $apellido1
 * @property string $apellido2
 * @property integer $telefono1
 * @property integer $telefono2
 * @property string $correo
 */
class OrganizacionesUrb extends CActiveRecord
{
	public $arrayci;
	public $arraynb;
	public $arrayap;
	public $arraycr;
	public $arraytlf;
	public $arraymesa;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OrganizacionesUrb the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.organizaciones_urb';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('organizaciones_urb_id, enc_urbanismo_id, organizacion_comunal, nacionalidad, cedula, nombre1, nombre2, apellido1, apellido2, telefono1, telefono2, correo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'organizaciones_urb_id' => 'Organizaciones Urb',
			'enc_urbanismo_id' => 'Enc Urbanismo',
			'organizacion_comunal' => 'Organizacion Comunal',
			'nacionalidad' => 'Nacionalidad',
			'cedula' => 'Cédula',
			'nombre1' => 'Nombre',
			'nombre2' => 'Nombre2',
			'apellido1' => 'Apellido',
			'apellido2' => 'Apellido2',
			'telefono1' => 'Teléfono',
			'telefono2' => 'Telefono2',
			'correo' => 'Correo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('organizaciones_urb_id',$this->organizaciones_urb_id,true);
		$criteria->compare('enc_urbanismo_id',$this->enc_urbanismo_id);
		$criteria->compare('organizacion_comunal',$this->organizacion_comunal);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('cedula',$this->cedula);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('telefono1',$this->telefono1);
		$criteria->compare('telefono2',$this->telefono2);
		$criteria->compare('correo',$this->correo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}