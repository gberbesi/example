<?php

/**
 * This is the model class for table "encuesta.enc_torre".
 *
 * The followings are the available columns in table 'encuesta.enc_torre':
 * @property string $enc_torre_id
 * @property integer $id_urbanismo
 * @property string $nom_torre
 * @property integer $nr_etapa
 * @property integer $tipo_vivienda
 * @property integer $cant_pisos
 
 * @property integer $apartamento_pisos
 */
class EncTorre extends CActiveRecord
{

	public $arraycant_pisos;
	public $arrayapartamento_pisos;
	public $arraynom_torre;
	public $arraynom_comite;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EncTorre the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.enc_torre';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('enc_torre_id, id_urbanismo, nom_torre, nr_etapa, tipo_vivienda, cant_pisos, apartamento_pisos', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'enc_torre_id' => 'Enc Torre',
			'id_urbanismo' => 'Id Urbanismo',
			'nom_torre' => 'Nom Torre',
			'nr_etapa' => 'Nr Etapa',
			'tipo_vivienda' => 'Tipo Vivienda',
			'cant_pisos' => 'Cantidad de Pisos',
			'nom_comite'=>'Nombre del Comite',
			
			'apartamento_pisos' => 'Apartamentos por Pisos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('enc_torre_id',$this->enc_torre_id,true);
		$criteria->compare('id_urbanismo',$this->id_urbanismo);
		$criteria->compare('nom_torre',$this->nom_torre,true);
		$criteria->compare('nr_etapa',$this->nr_etapa);
		$criteria->compare('tipo_vivienda',$this->tipo_vivienda);
		$criteria->compare('cant_pisos',$this->cant_pisos);
		
		$criteria->compare('apartamento_pisos',$this->apartamento_pisos);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}