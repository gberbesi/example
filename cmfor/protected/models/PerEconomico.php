<?php

/**
 * This is the model class for table "encuesta.per_economico".
 *
 * The followings are the available columns in table 'encuesta.per_economico':
 * @property integer $socioe_id
 * @property integer $persona_id
 * @property boolean $estudia
 * @property integer $gradoins
 * @property integer $ocupacion
 * @property integer $ingreso
 * @property integer $honorarios
 * @property boolean $discapacidad
 * @property string $conapdis
 * @property integer $enfermedad
 * @property integer $trabaja
 * @property integer $condicionemp
 * @property integer $tiempoadmin
 * @property integer $ingretipo
 * @property boolean $faov
 * @property boolean $pertenecemision
 * @property boolean $beneficiomision
 * @property integer $id_pertemision
 * @property integer $id_benefmision
 * @property integer $geo_estado_estudia
 * @property integer $geo_municipio_estudia
 * @property integer $geo_parroquia_estudia
 * @property integer $geo_estado_trabaja
 * @property integer $geo_municipio_trabaha
 * @property integer $geo_parroquia_trabaja
 * @property boolean $deporte
 * @property integer $id_deporte
 * @property string $usu_aud_id
 */
class PerEconomico extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.per_economico';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('persona_id', 'required'),
			array('persona_id, gradoins, ocupacion, ingreso, honorarios, enfermedad, trabaja, condicionemp, tiempoadmin, ingretipo, id_pertemision, id_benefmision, geo_estado_estudia, geo_municipio_estudia, geo_parroquia_estudia, geo_estado_trabaja, geo_municipio_trabaha, geo_parroquia_trabaja, id_deporte', 'numerical', 'integerOnly'=>true),
			array('estudia, discapacidad, conapdis, faov, pertenecemision, beneficiomision, deporte, usu_aud_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('socioe_id, persona_id, estudia, gradoins, ocupacion, ingreso, honorarios, discapacidad, conapdis, enfermedad, trabaja, condicionemp, tiempoadmin, ingretipo, faov, pertenecemision, beneficiomision, id_pertemision, id_benefmision, geo_estado_estudia, geo_municipio_estudia, geo_parroquia_estudia, geo_estado_trabaja, geo_municipio_trabaha, geo_parroquia_trabaja, deporte, id_deporte, usu_aud_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'socioe_id' => 'Socioe',
			'persona_id' => 'Persona',
			'estudia' => 'Estudia',
			'gradoins' => 'Gradoins',
			'ocupacion' => 'Ocupacion',
			'ingreso' => 'Ingreso',
			'honorarios' => 'Honorarios',
			'discapacidad' => 'Discapacidad',
			'conapdis' => 'Conapdis',
			'enfermedad' => 'Enfermedad',
			'trabaja' => 'Trabaja',
			'condicionemp' => 'Condicionemp',
			'tiempoadmin' => 'Tiempoadmin',
			'ingretipo' => 'Ingretipo',
			'faov' => 'Faov',
			'pertenecemision' => 'Pertenecemision',
			'beneficiomision' => 'Beneficiomision',
			'id_pertemision' => 'Id Pertemision',
			'id_benefmision' => 'Id Benefmision',
			'geo_estado_estudia' => ' Estado ',
			'geo_municipio_estudia' => ' Municipio ',
			'geo_parroquia_estudia' => ' Parroquia ',
			'geo_estado_trabaja' => ' Estado ',
			'geo_municipio_trabaja' => ' Municipio ',
			'geo_parroquia_trabaja' => ' Parroquia ',
			'deporte' => 'Deporte',
			'id_deporte' => 'Id Deporte',
			'gradoins'=>'Grado Instrucción',
			'honorarios'=>'Honorarios Profesionales',
			'usu_aud_id' => 'Usu Aud',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('socioe_id',$this->socioe_id);
		$criteria->compare('persona_id',$this->persona_id);
		$criteria->compare('estudia',$this->estudia);
		$criteria->compare('gradoins',$this->gradoins);
		$criteria->compare('ocupacion',$this->ocupacion);
		$criteria->compare('ingreso',$this->ingreso);
		$criteria->compare('honorarios',$this->honorarios);
		$criteria->compare('discapacidad',$this->discapacidad);
		$criteria->compare('conapdis',$this->conapdis,true);
		$criteria->compare('enfermedad',$this->enfermedad);
		$criteria->compare('trabaja',$this->trabaja);
		$criteria->compare('condicionemp',$this->condicionemp);
		$criteria->compare('tiempoadmin',$this->tiempoadmin);
		$criteria->compare('ingretipo',$this->ingretipo);
		$criteria->compare('faov',$this->faov);
		$criteria->compare('pertenecemision',$this->pertenecemision);
		$criteria->compare('beneficiomision',$this->beneficiomision);
		$criteria->compare('id_pertemision',$this->id_pertemision);
		$criteria->compare('id_benefmision',$this->id_benefmision);
		$criteria->compare('geo_estado_estudia',$this->geo_estado_estudia);
		$criteria->compare('geo_municipio_estudia',$this->geo_municipio_estudia);
		$criteria->compare('geo_parroquia_estudia',$this->geo_parroquia_estudia);
		$criteria->compare('geo_estado_trabaja',$this->geo_estado_trabaja);
		$criteria->compare('geo_municipio_trabaha',$this->geo_municipio_trabaha);
		$criteria->compare('geo_parroquia_trabaja',$this->geo_parroquia_trabaja);
		$criteria->compare('deporte',$this->deporte);
		$criteria->compare('id_deporte',$this->id_deporte);
		$criteria->compare('usu_aud_id',$this->usu_aud_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PerEconomico the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
