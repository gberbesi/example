<?php

/**
 * This is the model class for table "geo_ciudad".
 *
 * The followings are the available columns in table 'geo_ciudad':
 * @property integer $geo_ciudad_id
 * @property string $nombre
 * @property integer $estatus
 * @property boolean $zona_fronteriza
 * @property integer $usuario_id
 * @property integer $estado_id
 */
class GeoCiudad extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GeoCiudad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'geo_ciudad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('geo_ciudad_id, nombre, zona_fronteriza, usuario_id', 'required'),
			array('geo_ciudad_id, estatus, usuario_id, estado_id', 'numerical', 'integerOnly'=>true),
			array('nombre', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('geo_ciudad_id, nombre, estatus, zona_fronteriza, usuario_id, estado_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'geo_ciudad_id' => 'Geo Ciudad',
			'nombre' => 'Nombre',
			'estatus' => 'Estatus',
			'zona_fronteriza' => 'Zona Fronteriza',
			'usuario_id' => 'Usuario',
			'estado_id' => 'Estado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('geo_ciudad_id',$this->geo_ciudad_id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('estatus',$this->estatus);
		$criteria->compare('zona_fronteriza',$this->zona_fronteriza);
		$criteria->compare('usuario_id',$this->usuario_id);
		$criteria->compare('estado_id',$this->estado_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}