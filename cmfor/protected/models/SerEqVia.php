<?php
 
/**
 * This is the model class for table "encuesta.ser_eq_via".
 *
 * The followings are the available columns in table 'encuesta.ser_eq_via':
 * @property string $organizaciones_urb_id
 * @property integer $enc_urbanismo_id
 * @property boolean $acueducto
 * @property boolean $pozo_profundo
 * @property boolean $tanque
 * @property boolean $camión_tanque
 * @property boolean $red_cloacas
 * @property boolean $laguna_oxidación
 * @property boolean $planta_aguas_servidas
 * @property boolean $pozo_séptico
 * @property boolean $aerea
 * @property boolean $subterranea
 * @property boolean $por_tubería
 * @property boolean $bombona_individual
 * @property boolean $tanque_almacenamiento
 * @property boolean $metro
 * @property boolean $metrobus
 * @property boolean $autobuses
 * @property boolean $camionetas
 * @property boolean $rustico
 * @property boolean $ferrocarril
 * @property boolean $ninguno
 * @property boolean $avenidas_calles_internas
 * @property boolean $aceras_brocales
 * @property boolean $vialidad_acceso_urbanismo
 * @property boolean $parques_plazas
 * @property boolean $canchas_deportivas
 * @property boolean $educativa_prescolar
 * @property boolean $educativa_básica_media
 * @property boolean $escuela_técnica_robinsoniana
 * @property boolean $infocentro
 * @property boolean $locales_industria
 * @property boolean $locales_comercios
 * @property boolean $centros_comunicación
 * @property boolean $centros_salud
 * @property boolean $centro_diagnóstico
 * @property boolean $módulos_policiales
 * @property boolean $casa_comunitaria
 * @property boolean $centros_religiosos
 * @property boolean $centros_culturales
 * @property boolean $centros_electorales
 */
class SerEqVia extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SerEqVia the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'encuesta.ser_eq_via';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		
		return array(
			/*array('enc_urbanismo_id, acueducto, pozo_profundo, tanque, camión_tanque, red_cloacas, laguna_oxidación, planta_aguas_servidas, pozo_séptico, aerea, subterranea, por_tubería, bombona_individual, tanque_almacenamiento, metro, metrobus, autobuses, camionetas, rustico, ferrocarril, ninguno, avenidas_calles_internas, aceras_brocales, vialidad_acceso_urbanismo, parques_plazas, canchas_deportivas, educativa_prescolar, educativa_básica_media, escuela_técnica_robinsoniana, infocentro, locales_industria, locales_comercios, centros_comunicación, centros_salud, centro_diagnóstico, módulos_policiales, casa_comunitaria, centros_religiosos, centros_culturales, centros_electorales', 'required'),
			/*array('enc_urbanismo_id', 'numerical', 'integerOnly'=>true),*/
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('organizaciones_urb_id, enc_urbanismo_id, acueducto, pozo_profundo, tanque, camión_tanque, red_cloacas, laguna_oxidación, planta_aguas_servidas, pozo_séptico, aerea, subterranea, por_tubería, bombona_individual, tanque_almacenamiento, metro, metrobus, autobuses, camionetas, rustico, ferrocarril, ninguno, avenidas_calles_internas, aceras_brocales, vialidad_acceso_urbanismo, parques_plazas, canchas_deportivas, educativa_prescolar, educativa_básica_media, escuela_técnica_robinsoniana, infocentro, locales_industria, locales_comercios, centros_comunicación, centros_salud, centro_diagnóstico, módulos_policiales, casa_comunitaria, centros_religiosos, centros_culturales, centros_electorales', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'organizaciones_urb_id' => 'Organizaciones Urb',
			'enc_urbanismo_id' => 'Enc Urbanismo',
			'acueducto' => 'Acueducto',
			'pozo_profundo' => 'Pozo Profundo',
			'tanque' => 'Tanque',
			'camión_tanque' => 'Camión Tanque',
			'red_cloacas' => 'Red Cloacas',
			'laguna_oxidación' => 'Laguna Oxidación',
			'planta_aguas_servidas' => 'Planta Aguas Servidas',
			'pozo_séptico' => 'Pozo Séptico',
			'aerea' => 'Aerea',
			'subterranea' => 'Subterranea',
			'por_tubería' => 'Por Tubería',
			'bombona_individual' => 'Bombona Individual',
			'tanque_almacenamiento' => 'Tanque Almacenamiento',
			'metro' => 'Metro',
			'metrobus' => 'Metrobus',
			'autobuses' => 'Autobuses',
			'camionetas' => 'Camionetas',
			'rustico' => 'Rustico',
			'ferrocarril' => 'Ferrocarril',
			'ninguno' => 'Ninguno',
			'avenidas_calles_internas' => 'Avenidas Calles Internas',
			'aceras_brocales' => 'Aceras Brocales',
			'vialidad_acceso_urbanismo' => 'Vialidad Acceso Urbanismo',
			'parques_plazas' => 'Parques Plazas',
			'canchas_deportivas' => 'Canchas Deportivas',
			'educativa_prescolar' => 'Unidad Educativa Preescolar',
			'educativa_básica_media' => 'Unidad Educativa Básica Media',
			'escuela_técnica_robinsoniana' => 'Escuela Técnica Robinsoniana',
			'infocentro' => 'Infocentro',
			'locales_industria' => 'Locales Industriales',
			'locales_comercios' => 'Locales Comerciales',
			'centros_comunicación' => 'Centros de Comunicación',
			'centros_salud' => 'Centros de Salud',
			'centro_diagnóstico' => 'Centros Diagnósticos',
			'módulos_policiales' => 'Módulos Policiales',
			'casa_comunitaria' => 'Casa Comunitaria',
			'centros_religiosos' => 'Centros Religiosos',
			'centros_culturales' => 'Centros Culturales',
			'centros_electorales' => 'Centros Electorales',

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('organizaciones_urb_id',$this->organizaciones_urb_id,true);
		$criteria->compare('enc_urbanismo_id',$this->enc_urbanismo_id);
		$criteria->compare('acueducto',$this->acueducto);
		$criteria->compare('pozo_profundo',$this->pozo_profundo);
		$criteria->compare('tanque',$this->tanque);
		$criteria->compare('camión_tanque',$this->camión_tanque);
		$criteria->compare('red_cloacas',$this->red_cloacas);
		$criteria->compare('laguna_oxidación',$this->laguna_oxidación);
		$criteria->compare('planta_aguas_servidas',$this->planta_aguas_servidas);
		$criteria->compare('pozo_séptico',$this->pozo_séptico);
		$criteria->compare('aerea',$this->aerea);
		$criteria->compare('subterranea',$this->subterranea);
		$criteria->compare('por_tubería',$this->por_tubería);
		$criteria->compare('bombona_individual',$this->bombona_individual);
		$criteria->compare('tanque_almacenamiento',$this->tanque_almacenamiento);
		$criteria->compare('metro',$this->metro);
		$criteria->compare('metrobus',$this->metrobus);
		$criteria->compare('autobuses',$this->autobuses);
		$criteria->compare('camionetas',$this->camionetas);
		$criteria->compare('rustico',$this->rustico);
		$criteria->compare('ferrocarril',$this->ferrocarril);
		$criteria->compare('ninguno',$this->ninguno);
		$criteria->compare('avenidas_calles_internas',$this->avenidas_calles_internas);
		$criteria->compare('aceras_brocales',$this->aceras_brocales);
		$criteria->compare('vialidad_acceso_urbanismo',$this->vialidad_acceso_urbanismo);
		$criteria->compare('parques_plazas',$this->parques_plazas);
		$criteria->compare('canchas_deportivas',$this->canchas_deportivas);
		$criteria->compare('educativa_prescolar',$this->educativa_prescolar);
		$criteria->compare('educativa_básica_media',$this->educativa_básica_media);
		$criteria->compare('escuela_técnica_robinsoniana',$this->escuela_técnica_robinsoniana);
		$criteria->compare('infocentro',$this->infocentro);
		$criteria->compare('locales_industria',$this->locales_industria);
		$criteria->compare('locales_comercios',$this->locales_comercios);
		$criteria->compare('centros_comunicación',$this->centros_comunicación);
		$criteria->compare('centros_salud',$this->centros_salud);
		$criteria->compare('centro_diagnóstico',$this->centro_diagnóstico);
		$criteria->compare('módulos_policiales',$this->módulos_policiales);
		$criteria->compare('casa_comunitaria',$this->casa_comunitaria);
		$criteria->compare('centros_religiosos',$this->centros_religiosos);
		$criteria->compare('centros_culturales',$this->centros_culturales);
		$criteria->compare('centros_electorales',$this->centros_electorales);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}