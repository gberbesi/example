<?php

/**
 * This is the model class for table "urbanismo".
 *
 * The followings are the available columns in table 'urbanismo':
 * @property string $id_urbanismo
 * @property integer $id_estado
 * @property integer $id_municipio
 * @property integer $id_parroquia
 * @property integer $id_ciudad
 * @property string $nb_vocero
 * @property string $ape_vocero
 * @property string $tlf_vocero
 * @property string $cor_vocero
 * @property string $nb_urbanismo
 */
class Urbanismo extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Urba the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'encuesta.enc_urbanismo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('estado, municipio, parroquia, urbanismo_nombre', 'required'),
            array('estado, municipio, parroquia, sector, etapa_cant, cant_torres', 'numerical', 'integerOnly'=>true),
            array('urbanismo_nombre, direccion, calle', 'length', 'max'=>100),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('enc_urbanismo_id, estado, municipio, parroquia, sector, urbanismo_nombre, etapa_cant, cant_torres, direccion, calle', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'enc_urbanismo_id' => 'Enc Urbanismo',
            'estado' => 'Estado',
            'municipio' => 'Municipio',
            'parroquia' => 'Parroquia',
            'sector' => 'Sector',
            'urbanismo_nombre' => 'Urbanismo Nombre',
            'etapa_cant' => 'Etapa Cant',
            'cant_torres' => 'Cant Torres',
            'direccion' => 'Direccion',
            'calle' => 'Calle',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
        public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('enc_urbanismo_id',$this->enc_urbanismo_id,true);
        $criteria->compare('estado',$this->estado);
        $criteria->compare('municipio',$this->municipio);
        $criteria->compare('parroquia',$this->parroquia);
        $criteria->compare('sector',$this->sector);
        $criteria->compare('urbanismo_nombre',$this->urbanismo_nombre,true);
        $criteria->compare('etapa_cant',$this->etapa_cant);
        $criteria->compare('cant_torres',$this->cant_torres);
        $criteria->compare('direccion',$this->direccion,true);
        $criteria->compare('calle',$this->calle,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}