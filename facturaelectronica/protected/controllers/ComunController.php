<?php

class ComunController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
    public function actionBuscarSaime(){
	 	if(isset($_POST['cedula'])&&isset($_POST['nacionalidad']))
	 	{
			 $cedula=$_POST['cedula'];
			 $nacionalidad=$_POST['nacionalidad'];
			 if ($personaSici=PersonaGen::model()->find('per_cedula=:nu_cedula and per_nacionalidad=:tx_nacionalidad',array(':nu_cedula'=>$cedula,':tx_nacionalidad'=>$nacionalidad))){	 	
				 echo $personaSici->per_nombre1.'/'.$personaSici->per_nombre2.'/'.$personaSici->per_apellido1.'/'.$personaSici->per_apellido2.'/'.$personaSici->per_nacionalidad.'/'.$personaSici->per_correo.'/'.$personaSici->per_telefono1;
			 }
			 else{
				 $personaSaime=Onidex::model()->find('cedula=:CI and nac=:nac',array(':CI'=>$cedula,':nac'=>$nacionalidad));
				 if ($personaSaime)
				 	echo $personaSaime->nombre1.'/'.$personaSaime->nombre2.'/'.$personaSaime->apellido1.'/'.$personaSaime->apellido2.'/'.$personaSaime->nac;
			 }
	 	}
	 	else echo '////';
	}

//    public function actionBuscarOnidex(){
//	 	if(isset($_POST['cedula']))
//	 	{
//	 		$datosPersona=array();
//	 		$datosPersona['per_nombre1']="";
//	 		$datosPersona['per_nombre2']="";
//	 		$datosPersona['per_apellido1']="";
//	 		$datosPersona['per_apellido2']="";
//	 		$datosPersona['per_correo']="";
//	 		$datosPersona['per_telefono1']="";	
//	 		$datosPersona['per_telefono2']="";
//	 		$datosPersona['per_telefono3']="";
//	 		$datosPersona['per_nacionalidad']="";
//	 		$datosPersona['per_sexo']="";
//	 		$datosPersona['estado_civil_id']="";
//	 		$datosPersona['per_fecha_nacimiento']="";
//	 		$datosPersona['dir_estado_id']="";
//	 		$datosPersona['dir_ciudad_id']="";
//			$datosPersona['dir_municipio_id']="";
//			
//			 $cedula=$_POST['cedula'];
//			 if(is_numeric($cedula)){
//
//				$condicion='per_cedula=:per_cedula';
//				$parametro=array('per_cedula'=>$cedula);
//				 if ($persona=PersonaGen::model()->with('aseMiembroFamilias.direccion')->find($condicion,$parametro)){
//			 		$datosPersona['per_nombre1']=$persona->per_nombre1;
//			 		$datosPersona['per_nombre2']=$persona->per_nombre2;
//			 		$datosPersona['per_apellido1']=$persona->per_apellido1;
//			 		$datosPersona['per_apellido2']=$persona->per_apellido2;
//			 		$datosPersona['per_correo']=$persona->per_correo;
//			 		$datosPersona['per_telefono1']=$persona->per_telefono1;
//			 		$datosPersona['per_telefono2']=$persona->per_telefono2;
//			 		$datosPersona['per_telefono3']=$persona->per_telefono3;
//					$datosPersona['per_nacionalidad']=$persona->per_nacionalidad;
//					$datosPersona['per_sexo']=$persona->per_sexo;
//					$datosPersona['estado_civil_id']=$persona->estado_civil_id;
//					$fechaNacimiento=date("m-d-Y", strtotime($persona->per_fecha_nacimiento));				
//					$datosPersona['per_fecha_nacimiento']=$fechaNacimiento;
//
////					$datosPersona['dir_estado_id']=$persona->aseMiembroFamilias[0]->direccion->dir_estado_id;
////					$datosPersona['dir_ciudad_id']=$persona->aseMiembroFamilias[0]->direccion->dir_ciudad_id;
////					$datosPersona['dir_municipio_id']=$persona->aseMiembroFamilias[0]->direccion->dir_municipio_id;
//				 }
//				 else{
//					 $persona=Onidex::model()->find('cedula=:CI',array(':CI'=>$cedula));
//					 if ($persona){
//				 		$datosPersona['per_nombre1']=$persona->nombre1;
//				 		$datosPersona['per_nombre2']=$persona->nombre2;
//				 		$datosPersona['per_apellido1']=$persona->apellido1;
//				 		$datosPersona['per_apellido2']=$persona->apellido2;
//				 		$datosPersona['per_nacionalidad']=$persona->nac;
//				 		$fechaNacimiento=date("d-m-Y", strtotime($persona->fecha_nac));
//				 		$datosPersona['per_fecha_nacimiento']=$fechaNacimiento;
//					 }
//				 }			 	
//			 }
//			 echo CJSON::encode($datosPersona);
//	 	}
//	}

    public function actionBuscarTitular(){
    	Yii::import('application.modules.amparado.models.GenPersona');
    	Yii::import('application.modules.amparado.models.AseMiembroFamilia');    	
	 	if(isset($_POST['cedula']))
	 	{
	 		$datosPersona=array();
	 		$datosPersona['per_nombre1']="";
	 		$datosPersona['per_nombre2']="";
	 		$datosPersona['per_apellido1']="";
	 		$datosPersona['per_apellido2']="";
	 		$datosPersona['per_correo']="";
	 		$datosPersona['per_telefono1']="";	
	 		$datosPersona['per_telefono3']="";
	 		$datosPersona['per_nacionalidad']="";
	 		$datosPersona['per_sexo']="";
	 		$datosPersona['per_fecha_nacimiento']="";
		 	$datosPersona['per_nacionalidad']="";
		 	$datosPersona['id_persona']="";
		 	$datosPersona['institucion_id']="";
		 	$num=0;
			
			 $cedula=$_POST['cedula'];
			 
			 if(is_numeric($cedula)){
				$persona=AseMiembroFamilia::model()->with(array(
					'persona'=>array(
						'select'=>
				 		'	per_cedula,per_nombre1,per_nombre2,per_apellido1,
					 		per_apellido2,per_correo,per_telefono1,per_telefono3,
					 		per_nacionalidad,per_sexo,per_fecha_nacimiento
				 		',
				 	'condition'=>'per_cedula=:per_cedula', 
				 	'params'=>array(':per_cedula'=>$cedula)
				),
				'familia'=>array(
					'select'=>'institucion_id'
				)
				))->find(array(
					'select'=>'estatus_id,parentesco_id',
				 	'condition'=>'parentesco_id=1 AND estatus_id=1'
				));
				 if($persona){
				 	$datosPersona['cedula']=$cedula;
			 		$datosPersona['per_nombre1']=$persona->persona->per_nombre1;
			 		$datosPersona['per_nombre2']=$persona->persona->per_nombre2;
			 		$datosPersona['per_apellido1']=$persona->persona->per_apellido1;
			 		$datosPersona['per_apellido2']=$persona->persona->per_apellido2;
			 		$datosPersona['per_correo']=$persona->persona->per_correo;
			 		$datosPersona['per_telefono1']=$persona->persona->per_telefono1;
			 		$datosPersona['per_telefono3']=$persona->persona->per_telefono3;
					$datosPersona['per_nacionalidad']=$persona->persona->per_nacionalidad;
					$datosPersona['per_sexo']=$persona->persona->per_sexo;
					$datosPersona['id_persona']=$persona->persona->id_persona;
					$datosPersona['id_institucion']=$persona->familia->institucion_id;
					$fechaNacimiento=date("d-m-Y", strtotime($persona->persona->per_fecha_nacimiento));
					$datosPersona['per_fecha_nacimiento']=$fechaNacimiento;
					echo CJSON::encode($datosPersona);
				 }else{
					
				 }
			 }
			 
	 	}
	}

    public function actionBuscarOnidex(){
    	Yii::import('application.modules.usuario.models.Onidex');
        Yii::import('application.modules.usuario.models.GenPersona');
        Yii::import('application.modules.registro.models.Visitante');
	 	if(isset($_POST['cedula']))
	 	{
	 		$datosPersona=array();
	 		$datosPersona['per_nombre1']="";
	 		$datosPersona['per_nombre2']="";
	 		$datosPersona['per_apellido1']="";
	 		$datosPersona['per_apellido2']="";
	 		$datosPersona['per_correo']="";
	 		$datosPersona['per_telefono1']="";	
	 		$datosPersona['per_telefono2']="";
	 		$datosPersona['per_telefono3']="";
	 		$datosPersona['per_nacionalidad']="";
	 		$datosPersona['per_sexo']="";
	 		$datosPersona['estado_civil_id']="";
	 		$datosPersona['per_fecha_nacimiento']="";
		 	$datosPersona['cedula']="";
		 	$datosPersona['id_tipo_visitante']="";                                                
		 	$datosPersona['id_mesa_trabajo']="";
		 	$datosPersona['ponencia']="";
		 	$datosPersona['asistencia1']="";
		 	$datosPersona['asistencia2']="";    
		 	$datosPersona['asistencia3']="";         
                        $datosPersona['registrado']=false;  
			
			 $cedula=$_POST['cedula'];
			 $nacionalidad=$_POST['nacionalidad'];
			 
			 if(is_numeric($cedula)){

				$persona3=GenPersona::model()->find('cedula='.$cedula);
                                $datosPersona['nacionalidad']=$nacionalidad;
                                $datosPersona['cedula']=$cedula;
				if($persona3  ){
					 	$datosPersona['cedula']=$cedula;
				 		$datosPersona['per_nombre1']=$persona3->nombre1;
				 		$datosPersona['per_nombre2']=$persona3->nombre2;
				 		$datosPersona['per_apellido1']=$persona3->apellido1;
				 		$datosPersona['per_apellido2']=$persona3->apellido2;
				 		$datosPersona['per_nacionalidad']=$persona3->nacionalidad;
				 		$datosPersona['per_sexo']=$persona3->sexo;
                                                $datosPersona['per_correo']=$persona3->correo;
				 		$fechaNacimiento=date("d-m-Y", strtotime($persona3->fecha_nacimiento));
				 		$datosPersona['per_fecha_nacimiento']=$fechaNacimiento;                                    
                                                
                                                $modelVisitante = Visitante::model()->find('id_persona=:id_persona',array(':id_persona'=>$persona3->persona_id));
                                                if($modelVisitante){
                                                    $datosPersona['id_mesa_trabajo']=$modelVisitante->id_mesa_trabajo;
                                                    $datosPersona['id_tipo_visitante']=$modelVisitante->id_tipo_visitante;                        
                                                    $datosPersona['ponencia']=$modelVisitante->ponencia;
                                                    $datosPersona['asistencia1']=$modelVisitante->asistencia1;
                                                    $datosPersona['asistencia2']=$modelVisitante->asistencia2;    
                                                    $datosPersona['asistencia3']=$modelVisitante->asistencia3;      
                                                    $datosPersona['registrado']=true; 
                                                }
                                                
                                                
				 }elseif ($persona2=Onidex::model()->find('cedula=:CI and nac=:nac',array(':CI'=>$cedula,':nac'=>$nacionalidad))){
                                    
					 	$datosPersona['cedula']=$cedula;
				 		$datosPersona['per_nombre1']=  mb_strtoupper($persona2->nombre1,'utf-8');
				 		$datosPersona['per_nombre2']=mb_strtoupper($persona2->nombre2,'utf-8');
				 		$datosPersona['per_apellido1']=mb_strtoupper($persona2->apellido1,'utf-8');
				 		$datosPersona['per_apellido2']=mb_strtoupper($persona2->apellido2,'utf-8');
				 		$datosPersona['per_nacionalidad']=$persona2->nacionalidad;
				 		$datosPersona['per_sexo']=$persona2->sexo;
				 		$a=$datosPersona['estado_civil_id']=$persona2->edo_civil;
				 		if($a=='5' || $a=='1'){
				 			$datosPersona['estado_civil_id']=$persona2->edo_civil=4;
				 		}
				 		if($a=='6' || $a=='2' ){
				 			$datosPersona['estado_civil_id']=$persona2->edo_civil=1;
				 		}
				 		if($a=='4' || $a=='8' ){
				 			$datosPersona['estado_civil_id']=$persona2->edo_civil=3;
				 		}
				 		if($a=='7' && $a=='3' ){
				 			$datosPersona['estado_civil_id']=$persona2->edo_civil=5;
				 		}
				 		
				 		$fechaNacimiento=date("d-m-Y", strtotime($persona2->fecha_nac));
				 		$datosPersona['per_fecha_nacimiento']=$fechaNacimiento;
				 }
			 }
			 echo CJSON::encode($datosPersona);
	 	}
	}
	
	public function actionCiudad()  {
		$data=CHtml::listData(GenCiudad::model()->findAll('estado_id=:estado_id AND ciu_status=true',
		array(':estado_id'=>(int)$_POST['dir_estado_id'])) , 'id_ciudad', 'ciu_nombre' );
		echo CHtml::tag('option',array('value' => ''),'--Seleccione--',true);
	    foreach($data as $value=>$name){
	       echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
		}
    }
	
	public function actionMunicipio()  {
		$data=CHtml::listData(GenCiudad::model()->with('municipio')->together(true)->
		findAll('t.estado_id=:estado_id AND id_ciudad=:id_ciudad AND ciu_status=true',
		array(':estado_id'=>(int)$_POST['dir_estado_id'],':id_ciudad'=>
		(int)$_POST['dir_ciudad_id'])),'municipio.id_municipio', 'municipio.mun_nombre' );
		echo CHtml::tag('option',array('value' => ''),'--Seleccione--',true);
	    foreach($data as $value=>$name){
	       echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
		}
    }
	
	public function actionBuscarUnaInstitucion(){
		if(isset($_POST['mFamilia']))
	 	{
	 		Yii::import('application.modules.amparado.models.InsInstitucion');
	 		$datosInstitucion=array();
	 		$datosInstitucion['id_institucion']="";
	 		$datosInstitucion['institucion']="";

	 		$idMiembroFamilia=$_POST['mFamilia'][0];

	 		$data=AseMiembroFamilia::model()->with(array(
		 		'familia'=>array('select'=>'institucion_id'),
	 			'familia.institucion'=>array('select'=>'ins_nombre')
	 		))->find(array(
		 			'select'=>'id_miembro_familia,familia_id',
		 			'condition'=>'id_miembro_familia=:id_miembro_familia',
		 			'params'=>array(':id_miembro_familia'=>$idMiembroFamilia)
	 		));
	 		$datosInstitucion['id_institucion']=$data->familia->institucion_id;
	 		$datosInstitucion['institucion']=$data->familia->institucion->ins_nombre;
	 		echo CJSON::encode($datosInstitucion);
	 	}
	}

	public function actionBuscarMultiInstitucion(){
 		Yii::import('application.modules.amparado.models.InsInstitucion');
		$mFamilia=array();
		$datosInstitucion=array();
		$datosInstitucion['id_institucion']="";
		$datosInstitucion['institucion']="";
		$activo=false;
		$num1=0;
		$num2=0;
		if(isset($_POST['mFamilia']))
	 	{
			$mFamilia=$_POST['mFamilia'];
			foreach ($mFamilia as $idMiembroFamilia) {
		 		$data=AseMiembroFamilia::model()->with(array(
			 		'familia'=>array('select'=>'institucion_id'),
		 			'familia.institucion'=>array('select'=>'ins_nombre')
		 		))->find(array(
			 			'select'=>'id_miembro_familia,familia_id,estatus_id',
			 			'condition'=>'id_miembro_familia=:id_miembro_familia AND estatus_id=:estatus_id',
			 			'params'=>array(':id_miembro_familia'=>$idMiembroFamilia,':estatus_id'=>1)
	 			));

		 		if(isset($data)){
		 			$activo=true;
		 			if($num1==0){
			 			$datosInstitucion['id_institucion'][$num1]=$data->familia->institucion_id;
			 			$datosInstitucion['institucion'][$num1++]=$data->familia->institucion->ins_nombre;
		 			}else{
 			 			if(array_search($data->familia->institucion->ins_nombre, $datosInstitucion)){
				 			$datosInstitucion['id_institucion'][$num1]=$data->familia->institucion_id;
				 			$datosInstitucion['institucion'][$num1++]=$data->familia->institucion->ins_nombre;		 			
			 			}		 			
		 			}
		 		}
			}
			//echo '<pre>'; print_r($datosInstitucion); die();
			//echo ($activo)?'existe un activo':'inactivo'; die();

			if(!$activo){
				$datosInstitucion['id_institucion']="";
				$datosInstitucion['institucion']="";
		 		$data=InsInstitucion::model()->findAll(); //array('select'=>'id_institucion,ins_nombre')

				foreach ($data as $value) {
		 			$datosInstitucion['id_institucion'][$num2]=$value->id_institucion;
		 			$datosInstitucion['institucion'][$num2]=$value->ins_nombre;
		 			$num2++;
				}
				
			}
			//echo '<pre>'; print_r($datosInstitucion);	die();
			echo CJSON::encode($datosInstitucion);
	 	}
	}
}