<?php
/* @var $this BonoController */
/* @var $model Bono */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bono-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_afiliado'); ?>
		<?php echo $form->textField($model,'id_afiliado'); ?>
		<?php echo $form->error($model,'id_afiliado'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nro_documento'); ?>
		<?php echo $form->textField($model,'nro_documento'); ?>
		<?php echo $form->error($model,'nro_documento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_bono'); ?>
		<?php echo $form->textField($model,'total_bono'); ?>
		<?php echo $form->error($model,'total_bono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_comision'); ?>
		<?php echo $form->textField($model,'total_comision'); ?>
		<?php echo $form->error($model,'total_comision'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'concepto'); ?>
		<?php echo $form->textField($model,'concepto'); ?>
		<?php echo $form->error($model,'concepto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->textField($model,'fecha'); ?>
		<?php echo $form->error($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'estado'); ?>
		<?php echo $form->checkBox($model,'estado'); ?>
		<?php echo $form->error($model,'estado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->