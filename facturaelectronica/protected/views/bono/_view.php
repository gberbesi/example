<?php
/* @var $this BonoController */
/* @var $data Bono */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_bono')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_bono), array('view', 'id'=>$data->id_bono)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_afiliado')); ?>:</b>
	<?php echo CHtml::encode($data->id_afiliado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nro_documento')); ?>:</b>
	<?php echo CHtml::encode($data->nro_documento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_bono')); ?>:</b>
	<?php echo CHtml::encode($data->total_bono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_comision')); ?>:</b>
	<?php echo CHtml::encode($data->total_comision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('concepto')); ?>:</b>
	<?php echo CHtml::encode($data->concepto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('estado')); ?>:</b>
	<?php echo CHtml::encode($data->estado); ?>
	<br />

	*/ ?>

</div>