<?php
/* @var $this BonoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bonos',
);

$this->menu=array(
	array('label'=>'Create Bono', 'url'=>array('create')),
	array('label'=>'Manage Bono', 'url'=>array('admin')),
);
?>

<h1>Bonos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
