<?php
/* @var $this BonoController */
/* @var $model Bono */

$this->breadcrumbs=array(
	'Bonos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Bono', 'url'=>array('index')),
	array('label'=>'Create Bono', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#bono-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Bonos</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bono-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_bono',
		'id_afiliado',
		'nro_documento',
		'total_bono',
		'total_comision',
		'concepto',
		/*
		'fecha',
		'estado',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
