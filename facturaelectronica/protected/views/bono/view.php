<?php
/* @var $this BonoController */
/* @var $model Bono */

$this->breadcrumbs=array(
	'Bonos'=>array('index'),
	$model->id_bono,
);

$this->menu=array(
	array('label'=>'List Bono', 'url'=>array('index')),
	array('label'=>'Create Bono', 'url'=>array('create')),
	array('label'=>'Update Bono', 'url'=>array('update', 'id'=>$model->id_bono)),
	array('label'=>'Delete Bono', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_bono),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Bono', 'url'=>array('admin')),
);
?>

<h1>View Bono #<?php echo $model->id_bono; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_bono',
		'id_afiliado',
		'nro_documento',
		'total_bono',
		'total_comision',
		'concepto',
		'fecha',
		'estado',
	),
)); ?>
