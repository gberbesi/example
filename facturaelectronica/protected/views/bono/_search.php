<?php
/* @var $this BonoController */
/* @var $model Bono */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_bono'); ?>
		<?php echo $form->textField($model,'id_bono'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_afiliado'); ?>
		<?php echo $form->textField($model,'id_afiliado'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nro_documento'); ?>
		<?php echo $form->textField($model,'nro_documento'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total_bono'); ?>
		<?php echo $form->textField($model,'total_bono'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total_comision'); ?>
		<?php echo $form->textField($model,'total_comision'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'concepto'); ?>
		<?php echo $form->textField($model,'concepto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha'); ?>
		<?php echo $form->textField($model,'fecha'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'estado'); ?>
		<?php echo $form->checkBox($model,'estado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->