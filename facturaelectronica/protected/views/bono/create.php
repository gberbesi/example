<?php
/* @var $this BonoController */
/* @var $model Bono */

$this->breadcrumbs=array(
	'Bonos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Bono', 'url'=>array('index')),
	array('label'=>'Manage Bono', 'url'=>array('admin')),
);
?>

<h1>Create Bono</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>