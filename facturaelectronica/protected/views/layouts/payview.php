<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>Factura E | </title>

    <!-- Bootstrap -->

    <link href="themes/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="themes/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="themes/gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="themes/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="themes/gentelella/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="themes/gentelella/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="themes/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="themes/gentelella/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="">
    <div class="container body">
      <div class="main_container">

        <div class="right_col" role="main" style="background-color:#eeeeee">
    
        <?php echo $content; ?>
      
      </div>
    </div>
  </div>

    <!-- jQuery -->
    <!--<script src="themes/gentelella/vendors/jquery/dist/jquery.min.js"></script>-->
    <!-- Bootstrap -->
    <script src="themes/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="themes/gentelella/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="themes/gentelella/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="themes/gentelella/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="themes/gentelella/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="themes/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="themes/gentelella/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="themes/gentelella/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="themes/gentelella/vendors/Flot/jquery.flot.js"></script>
    <script src="themes/gentelella/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="themes/gentelella/vendors/Flot/jquery.flot.time.js"></script>
    <script src="themes/gentelella/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="themes/gentelella/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="themes/gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="themes/gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="themes/gentelella/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="themes/gentelella/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="themes/gentelella/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="themes/gentelella/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="themes/gentelella/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="themes/gentelella/vendors/moment/min/moment.min.js"></script>
    <script src="themes/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="themes/gentelella/build/js/custom.min.js"></script>
  
  </body>
</html>
