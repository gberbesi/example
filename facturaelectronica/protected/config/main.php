<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.


	$defaultController = array('defaultController'=>'usuario/usuario/index');
 
$main = array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Venta de Frutas',
	'language'=>'es',
	//'defaultController'=>'usuario/Usuario/index',
    //'defaultController'=>'reportes/Consolidado/Index',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'zii.widgets.jui.CJuiInputWidget',
		'zii.widgets.grid.CGridColumn',
		'zii.widgets.grid.CGridView',
		'zii.widgets.jui.CJuiWidget',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123456',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			//'ipFilters'=>array('120.0.0.1','::1'),
		),
		'usuario',
		'rol',
		'reportes',
        'registro',
        'facturas',
        'reportes',
        'compras'
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'loginUrl'=>array('/usuario/Usuario/login')
		),
		


		'db'=>array(
			'connectionString' => 'mysql:host=172.16.1.26;dbname=naby',
			//'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'secreta',
			'charset' => 'utf8',
		),



		
		'authManager'=>array(
			'class'=>'CDbAuthManager',
			'connectionID'=>'db',
			'defaultRoles'=>array('authenticated', 'guest'),
	        'itemTable'=>'authitem', // Tabla que contiene los elementos de autorizacion
	        'itemChildTable'=>'authitemchild', // Tabla que contiene los elementos padre-hijo
	        'assignmentTable'=>'authassignment', // Tabla que contiene la signacion usuario-autorizacion
		),

		
/*
	     'authManager'=>array(
			'class'=>'CDbAuthManager',
			'connectionID'=>'db',
			'defaultRoles'=>array('authenticated', 'guest'),
	        'itemTable'=>'authitem', // Tabla que contiene los elementos de autorizacion
	        'itemChildTable'=>'authitemchild', // Tabla que contiene los elementos padre-hijo
	        'assignmentTable'=>'authassignment', // Tabla que contiene la signacion usuario-autorizacion
		),
*/
		'session' => array(
                        'class' => 'CDbHttpSession',
                        'connectionID' => 'db',
						'sessionTableName' => 'session',
						'autoCreateSessionTable'=> true,
						'timeout' => 3600,//tiempo en segundos
    	),	

		'metadata'=>array('class'=>'Metadata'),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			 'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),*/

			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
                //'adminEmail'=>'webmaster@example.com',
                // ##configuracion de servidor de correo##
               // 'mailHost'=>'172.16.254.49',
               // 'mailPortSsl'=>'465',
               // 'mailUsername'=>'sigevih@minhvi.gob.ve',
               // 'mailUserPassw'=>'s?g381c',

                // ##configuracion de datos de correo##
               // 'mailRemitente'=>'no-reply@minhvi.gob.ve',
                //'mailAsunto'=>utf8_decode('MINHVI'),
               // 'nombreRemitente'=>utf8_decode('MINHVI'),
                //'correoReporteError'=>array('htorrealba@minvih.gob.ve'),

            //'adminEmail'=>'webmaster@example.com',
           // 'RutaUsuarios'=>'/archivos/usuarios/',
           // 'RutaDocumentos'=>'/archivos/descargas/',

	),
);

if ((Yii::app() instanceof CWebApplication)) { //CConsoleApplication
	$main=array_merge($main,$defaultController);
}

return $main;
