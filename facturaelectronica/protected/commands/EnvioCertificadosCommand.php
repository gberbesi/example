<?php

class EnvioCertificadosCommand extends CConsoleCommand {
	
	
	public function actionEnviar($idBatch) {
		ini_set('memory_limit', '1G');
		
		Yii::import('application.modules.registro.models.EnvioCertificados',true);
		Yii::import('application.modules.registro.models.CertificadoParticipante',true);
		Yii::import('application.modules.registro.models.Visitante',true);
		Yii::import('application.modules.registro.models.MesaTrabajo',true);
		Yii::import('application.modules.registro.models.GenPersona',true);
		Yii::import('application.modules.registro.models.TipoVisitante',true);
		$batch = EnvioCertificados::model()->findByPk($idBatch);
		
		if($batch->tipo_certificado == 1) {
				
			$visitantes = Visitante::model()->with(array('idPersona','idMesaTrabajo','idTipoVisitante'))->findAll('t.id_mesa_trabajo=:id_mesa_trabajo',array(':id_mesa_trabajo'=>$batch->id_certificado));
			
			$pdf = Yii::createComponent('application.extensions.mpdf60.mpdf');
			
			$total = count($visitantes);
			$envioDeCorreo = false;
			$yaSeEnvio = NULL;
			$mpdf=NULL;
			$nombre=NULL;
			$mail = NULL;
			$transaction = NULL;
			foreach($visitantes as $key=>$visitante) {
				$yaSeEnvio = CertificadoParticipante::model()->find('id_visitante=:id_visitante AND tipo_certificado=:tipo_certificado AND id_certificado=:id_certificado',array(':id_visitante'=>$visitante->id_visitante,':tipo_certificado'=>1,':id_certificado'=>$visitante->id_mesa_trabajo));
				if(!$yaSeEnvio) {
				
					$mpdf=new mPDF('c',array(870.00,673.00),'','','0','0','0','0','0','0');
					
					$mpdf->WriteHTML('<img src="images/certificados/certificadoM'.$visitante->id_mesa_trabajo.'.jpg" />');
					
					$nombre = mb_strtoupper(trim($visitante->idPersona->nombre1)).
								((strlen(mb_strtoupper(trim($visitante->idPersona->nombre2)))>0)?' '.substr(mb_strtoupper(trim($visitante->idPersona->nombre2)),0,1).'.':'').
								((strlen(mb_strtoupper(trim($visitante->idPersona->apellido1)))>0)?' '.mb_strtoupper(trim($visitante->idPersona->apellido1)):'').
								((strlen(mb_strtoupper(trim($visitante->idPersona->apellido2)))>0)?' '.substr(mb_strtoupper(trim($visitante->idPersona->apellido2)),0,1).'.':'');
			
					$mpdf->WriteFixedPosHTML('<div style="font-size:120px;text-align:center;">'.$nombre.'</div>',380,270,480,60,'auto');
								
					$mpdf->Output("certificados/Certificado - ".$visitante->idMesaTrabajo->descripcion." ".$visitante->idPersona->cedula." - ".$nombre.".pdf","F");
					
					Yii::import('application.extensions.phpmailer.JPhpMailer');
					$mail = new JPhpMailer;
					$mail->IsSMTP();
					$mail->Host = 'smtp.gmail.com';
					$mail->Port = '465';
					$mail->CharSet = "utf-8";
					$mail->SMTPAuth = true;
					$mail->SMTPSecure = 'ssl';
					$mail->Username = 'simposiominhvi@gmail.com';
					$mail->Password = 'simposiominhvi*';
					$mail->SetFrom('no-reply@minvih.com', 'MINHVI');
					
					$mail->Subject = 'Certificado '.$visitante->idMesaTrabajo->descripcion." ".$nombre;
					$mail->AltBody = "¡Gracias por su asistencia!
									En el siguiente vínculo electrónico tendrán acceso al material digital de las diferentes exposiciones del simposio.
									http://www.minhvi.gob.ve/index.php/institucion/servicios-2/descargas/category/7-simposio";
					//$mail->AllowEmpty = true;
					$mail->MsgHTML('¡Gracias por su asistencia! <br/>
									En el siguiente vínculo electrónico tendrán acceso al material digital de las diferentes exposiciones del simposio.<br/>
									http://www.minhvi.gob.ve/index.php/institucion/servicios-2/descargas/category/7-simposio');
					//$mail->AddAddress('xavieremv@gmail.com', $nombre);
					$mail->AddAddress(trim($visitante->idPersona->correo), $nombre);
					
					$mail->AddAttachment("certificados/Certificado - ".$visitante->idMesaTrabajo->descripcion." ".$visitante->idPersona->cedula." - ".$nombre.".pdf");
					
					if(!$mail->Send()) {
						$envioDeCorreo = false;
					   //throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente intentelo mas tarde.',500);
					}
					else {
						$envioDeCorreo = true;
					   //echo 'Mail enviado!';
					}
					
					unlink("certificados/Certificado - ".$visitante->idMesaTrabajo->descripcion." ".$visitante->idPersona->cedula." - ".$nombre.".pdf");
					
					$transaction = Yii::app()->db->beginTransaction();
					
					$batch->updated_time 		= new CDbExpression('NOW()');
					$batch->porcentaje_envio 	= ($key+1)/$total;
					$batch->save(false);
					
					if($envioDeCorreo) {
						$certificadoParticipante = new CertificadoParticipante;
						$certificadoParticipante->id_visitante		= $visitante->id_visitante;
						$certificadoParticipante->tipo_certificado	= 1;
						$certificadoParticipante->id_certificado	= $visitante->id_mesa_trabajo;
						$certificadoParticipante->save();
					}
					
					$transaction->commit();
				
				} else {
					
					$transaction = Yii::app()->db->beginTransaction();
					
					$batch->updated_time 		= new CDbExpression('NOW()');
					$batch->porcentaje_envio 	= ($key+1)/$total;
					$batch->save(false);
					
					$transaction->commit();
					
				}
				
			}
			
		} else if($batch->tipo_certificado == 2) {
			$visitantes = Visitante::model()->with(array('idPersona','idMesaTrabajo','idTipoVisitante'))->findAll('(asistencia1::int + asistencia2::int + asistencia3::int) > 1');
			
			$pdf = Yii::createComponent('application.extensions.mpdf60.mpdf');
			
			$total = count($visitantes);
			$envioDeCorreo = false;
			$yaSeEnvio = NULL;
			$mpdf=NULL;
			$nombre=NULL;
			$mail = NULL;
			$transaction = NULL;
			foreach($visitantes as $key=>$visitante) {
				$yaSeEnvio = CertificadoParticipante::model()->find('id_visitante=:id_visitante AND tipo_certificado=:tipo_certificado AND id_certificado=:id_certificado',array(':id_visitante'=>$visitante->id_visitante,':tipo_certificado'=>2,':id_certificado'=>1));
				if(!$yaSeEnvio) {
				
					$mpdf=new mPDF('c',array(870.00,673.00),'','','0','0','0','0','0','0');
					
					$mpdf->WriteHTML('<img src="images/certificados/certificado.jpg" />');
					
					$nombre = mb_strtoupper(trim($visitante->idPersona->nombre1)).
								((strlen(mb_strtoupper(trim($visitante->idPersona->nombre2)))>0)?' '.substr(mb_strtoupper(trim($visitante->idPersona->nombre2)),0,1).'.':'').
								((strlen(mb_strtoupper(trim($visitante->idPersona->apellido1)))>0)?' '.mb_strtoupper(trim($visitante->idPersona->apellido1)):'').
								((strlen(mb_strtoupper(trim($visitante->idPersona->apellido2)))>0)?' '.substr(mb_strtoupper(trim($visitante->idPersona->apellido2)),0,1).'.':'');
			
					$mpdf->WriteFixedPosHTML('<div style="font-size:120px;text-align:center;">'.$nombre.'</div>',380,270,480,60,'auto');
								
					$mpdf->Output("certificados/Certificado - Simposio ".$visitante->idPersona->cedula." - ".$nombre.".pdf","F");
					
					Yii::import('application.extensions.phpmailer.JPhpMailer');
					$mail = new JPhpMailer;
					$mail->IsSMTP();
					$mail->Host = 'smtp.gmail.com';
					$mail->Port = '465';
					$mail->CharSet = "utf-8";
					$mail->SMTPAuth = true;
					$mail->SMTPSecure = 'ssl';
					$mail->Username = 'simposiominhvi@gmail.com';
					$mail->Password = 'simposiominhvi*';
					$mail->SetFrom('no-reply@minvih.com', 'MINHVI');
					
					$mail->Subject = 'Certificado Simposio '.$nombre;
					$mail->AltBody = "¡Gracias por su asistencia!
									En el siguiente vínculo electrónico tendrán acceso al material digital de las diferentes exposiciones del simposio.
									http://www.minhvi.gob.ve/index.php/institucion/servicios-2/descargas/category/7-simposio";
					//$mail->AllowEmpty = true;
					$mail->MsgHTML('¡Gracias por su asistencia! <br/>
									En el siguiente vínculo electrónico tendrán acceso al material digital de las diferentes exposiciones del simposio.<br/>
									http://www.minhvi.gob.ve/index.php/institucion/servicios-2/descargas/category/7-simposio');
					//$mail->AddAddress('xavieremv@gmail.com', $nombre);
					$mail->AddAddress(trim($visitante->idPersona->correo), $nombre);
					
					$mail->AddAttachment("certificados/Certificado - Simposio ".$visitante->idPersona->cedula." - ".$nombre.".pdf");
					
					if(!$mail->Send()) {
						$envioDeCorreo = false;
					   //throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente intentelo mas tarde.',500);
					}
					else {
						$envioDeCorreo = true;
					   //echo 'Mail enviado!';
					}
					
					unlink("certificados/Certificado - Simposio ".$visitante->idPersona->cedula." - ".$nombre.".pdf");
					
					$transaction = Yii::app()->db->beginTransaction();
					
					$batch->updated_time 		= new CDbExpression('NOW()');
					$batch->porcentaje_envio 	= ($key+1)/$total;
					$batch->save(false);
					
					if($envioDeCorreo) {
						$certificadoParticipante = new CertificadoParticipante;
						$certificadoParticipante->id_visitante		= $visitante->id_visitante;
						$certificadoParticipante->tipo_certificado	= 2;
						$certificadoParticipante->id_certificado	= 1;
						$certificadoParticipante->save();
					}
					
					$transaction->commit();
				
				} else {
					
					$transaction = Yii::app()->db->beginTransaction();
					
					$batch->updated_time 		= new CDbExpression('NOW()');
					$batch->porcentaje_envio 	= ($key+1)/$total;
					$batch->save(false);
					
					$transaction->commit();
					
				}
				
			}
		}
	}
	
	
}