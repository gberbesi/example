<?php

/*
 Modelo para las funciones comunes o mas usadas en el sistema
 */
class FuncionesGlobales extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Telefono the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tx_numero', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tx_numero', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tx_numero' => 'Tx Numero',
		);
	}

	public static function datosusuario(){
		///////// Para obtener el id Usuario con la finalidad enviar el id para actualizar datos //////////////
		Yii::import('application.modules.usuario.models.GenPersona',true);
		Yii::import('application.modules.usuario.models.Usuario',true);
		$usuario=Usuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>yii::app()->user->id));
		return $usuario->persona;
	}

	public static function datosinspector(){
		///////// Para obtener el id Usuario con la finalidad enviar el id para actualizar datos //////////////
		Yii::import('application.modules.usuario.models.Usuario',true);
		Yii::import('application.modules.usuario.models.Inspectores',true);		
		$usuario=Usuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>yii::app()->user->id));
		$inspector=Inspectores::model()->find('persona_id=:persona_id',array(':persona_id'=>$usuario->persona_id));
		return $inspector;
	}

	public static function evu_fecha_historico($fecha_historico_ant){

//print_r("aqui".$fecha_historico_ant); die();


		$dia = strtoupper(date('l', strtotime($fecha_historico_ant)));

//print_r($dia); die();
		if($dia=='MONDAY '){
			$prox_fecha = strtotime('+8 day',strtotime(date('d-m-Y')));
			$prox_fecha = date('d-m-Y',$prox_fecha);
			if($prox_fecha==date('d-m-Y')){	$permiso=1;	}
			if($prox_fecha!=date('d-m-Y')){
				$dias_transcurridos	= (strtotime($fecha_historico_ant)-strtotime(date('d-m-Y')))/86400;
				$dias_transcurridos = abs($dias_transcurridos); $dias_transcurridos = floor($dias_transcurridos);

				if( $dias_transcurridos >= 8 ){
					$permiso=1;
				}
				if( $dias_transcurridos < 8 ){
					$permiso=0;
				}
			}	
				return $permiso;
		} 
		if($dia=='TUESDAY'){
			$prox_fecha = strtotime('+7 day',strtotime($fecha_historico_ant));
			$prox_fecha = date('d-m-Y',$prox_fecha);
			if($prox_fecha==date('d-m-Y')){	$permiso=1;	}
			if($prox_fecha!=date('d-m-Y')){	
				$dias_transcurridos	= (strtotime($fecha_historico_ant)-strtotime(date('d-m-Y')))/86400;
				$dias_transcurridos = abs($dias_transcurridos); $dias_transcurridos = floor($dias_transcurridos);

				if( $dias_transcurridos >= 7 ){
					$permiso=1;
				}
				if( $dias_transcurridos < 7 ){
					$permiso=0;
				}				
			} 	
				return $permiso;
		}
		if($dia=='WEDNESDAY'){
			$prox_fecha = strtotime('+6 day',strtotime($fecha_historico_ant));
			$prox_fecha = date('d-m-Y',$prox_fecha);
			if($prox_fecha==date('d-m-Y')){	$permiso=1; }
			if($prox_fecha!=date('d-m-Y')){	
				$dias_transcurridos	= (strtotime($fecha_historico_ant)-strtotime(date('d-m-Y')))/86400;
				$dias_transcurridos = abs($dias_transcurridos); $dias_transcurridos = floor($dias_transcurridos);		
				if( $dias_transcurridos >= 6 ){
					$permiso=1;
				}
				if( $dias_transcurridos < 6 ){
					$permiso=0;
				}	
			}	
				return $permiso;
		}
		if($dia=='THURSDAY'){
			$prox_fecha = strtotime('+5 day',strtotime($fecha_historico_ant));
			$prox_fecha = date('d-m-Y',$prox_fecha);
			if($prox_fecha==date('d-m-Y')){	$permiso=1;	}
			if($prox_fecha!=date('d-m-Y')){
				$dias_transcurridos	= (strtotime($fecha_historico_ant)-strtotime(date('d-m-Y')))/86400;
				$dias_transcurridos = abs($dias_transcurridos); $dias_transcurridos = floor($dias_transcurridos);		
				if( $dias_transcurridos >= 5 ){
					$permiso=1;
				}
				if( $dias_transcurridos < 5 ){
					$permiso=0;
				}					
			}	
				return $permiso;
		}
		if($dia=='FRIDAY'){
			$prox_fecha = strtotime('+4 day',strtotime($fecha_historico_ant));
			$prox_fecha = date('d-m-Y',$prox_fecha);
			if($prox_fecha==date('d-m-Y')){	$permiso=1;	}
			if($prox_fecha!=date('d-m-Y')){
				$dias_transcurridos	= (strtotime($fecha_historico_ant)-strtotime(date('d-m-Y')))/86400;
				$dias_transcurridos = abs($dias_transcurridos); $dias_transcurridos = floor($dias_transcurridos);		
				if( $dias_transcurridos >= 4 ){
					$permiso=1;
				}
				if( $dias_transcurridos < 4 ){
					$permiso=0;
				}			
			}	
				return $permiso;
		}
		if($dia=='SATURDAY'){
			$prox_fecha = strtotime('+3 day',strtotime($fecha_historico_ant));
			$prox_fecha = date('d-m-Y',$prox_fecha);
			if($prox_fecha==date('d-m-Y')){	$permiso=1;	}
			if($prox_fecha!=date('d-m-Y')){
				$dias_transcurridos	= (strtotime($fecha_historico_ant)-strtotime(date('d-m-Y')))/86400;
				$dias_transcurridos = abs($dias_transcurridos); $dias_transcurridos = floor($dias_transcurridos);		
				if( $dias_transcurridos >= 3 ){
					$permiso=1;
				}
				if( $dias_transcurridos < 3 ){
					$permiso=0;
				}				
			}	
				return $permiso;
		}
		if($dia=='SUNDAY'){
			$prox_fecha = strtotime('+2 day',strtotime($dia));
			$prox_fecha = date('d-m-Y',$prox_fecha);
			if($prox_fecha==date('d-m-Y')){	$permiso=1;	}
			if($prox_fecha!=date('d-m-Y')){
				$dias_transcurridos	= (strtotime($fecha_historico_ant)-strtotime(date('d-m-Y')))/86400;
				$dias_transcurridos = abs($dias_transcurridos); $dias_transcurridos = floor($dias_transcurridos);		
				if( $dias_transcurridos >= 2 ){
					$permiso=1;
				}
				if( $dias_transcurridos < 2 ){
					$permiso=0;
				}				
			}	
				return $permiso;
		}
	}


	public static function solicitud($persona_id){
		///////// Para saber si el usuario tiene una solicitud //////////////
		Yii::import('application.modules.solicitud.models.Solicitud',true);
		$solicitud=Solicitud::model()->find('persona_id=:persona_id AND estatus = true',array(':persona_id'=>$persona_id));
		($solicitud)? $solicitud:$solicitud=false;
		return $solicitud;
	}

	public static function pre_venta($persona_id){
		///////// Para saber si el usuario tiene una preventa //////////////
		Yii::import('application.modules.solicitud.models.Solicitud',true);
		Yii::import('application.modules.pre_venta.models.PreVenta',true);
		$solicitud=null;
		$solicitud=Solicitud::model()->find('persona_id=:persona_id AND estatus = true',array(':persona_id'=>$persona_id));
		$pre_venta=null;		
		if(!is_null($solicitud)){
			$pre_venta=PreVenta::model()->find('solicitud_id=:solicitud_id AND estatus = true',array(':solicitud_id'=>$solicitud->solicitud_id));
		}
		($pre_venta)? $pre_venta:$pre_venta=false;
		return $pre_venta;
	}


	public static function treeview(){
		///////// Para obtener la direccion y verificar donde estamos situados //////////////
		  $url=substr($_SERVER['REQUEST_URI'],strpos($_SERVER['REQUEST_URI'],'='));
                  $url=str_replace('=', '', $url);
                  if(substr($url,0,strpos($url,'&'))){$url=substr($url,0,strpos($url,'&'));}
		  return $url;
	}

	public static function certificado(){

		Yii::import('application.modules.usuario.models.GenPersona',true);
		Yii::import('application.modules.usuario.models.Usuario',true);
		$usuario=Usuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>yii::app()->user->id));

		if($usuario->persona->carga_finalizada==true) {
                     return $certificado=true;
		}
		else {
		   return $certificado=false;
		}

	}

    public static function correo($aquien,$aquiennombre,$mensaje,$certificado_adjunto='')
	{
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		$mail = new JPhpMailer;
		$mail->IsSMTP();
		$mail->Host = Yii::app()->params['mailHost'];/*'172.16.0.20';*/
		$mail->Port = Yii::app()->params['mailPortSsl'];
		$mail->CharSet = "utf-8";
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'ssl';
		$mail->Username = Yii::app()->params['mailUsername'];
		$mail->Password = Yii::app()->params['mailUserPassw'];
		$mail->SetFrom(Yii::app()->params['mailRemitente'], Yii::app()->params['nombreRemitente']);
		if($certificado_adjunto!=''){ $mail->AddAttachment($certificado_adjunto); }
		$mail->Subject = Yii::app()->params['mailAsunto'];
		//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($mensaje);
		$mail->AddAddress($aquien, $aquiennombre);
		
		if(!$mail->Send()) {
                     return $correo=false;
		}
		else {
		   return $correo=true;
		}
	}        
        
        
        
	public static function textcorreo($text,$persona){


            $mensaje='
				<table width="100%" bgcolor="#eeeeee" cellpadding="0" cellspacing="0" border="0">
				<tbody>
				    <tr>
				        <td>
				            <table cellpadding="0" cellspacing="0" width="600" border="0" align="center">
				                <tbody>
				                    <tr>
				                        <td>

				                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
				                                <tbody>
				                                    <table style="margin:0; font-size:5px; line-height:5px;" bgcolor="#00c0ef" width="600" cellpadding="0" cellspacing="0"><tr><td height="20">&nbsp;</td></tr></table>
				                                    <tr>
				                                        <td colspan="3" rowspan="3" bgcolor="#FFFFFF" style="padding:0">
				                                            <!-- inicio contenido -->
				                                            <br>
				                                            <p style="text-align:center; text-transform:uppercase; font-size:24px; line-height:30px; font-weight:bold-cursive; color:#484a42;">
				                                                MINHVI
				                                            </p>
				                                            <!-- inicio articulos -->
				                                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
				                                                <tbody>
				                                                    <tr valign="top">
				                                                        <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
				                                                        <td colspan="3">
				                                                            <b><a style="display:block; margin:5px 5px 5px;background:#eeeeee;">INFORMA:</a></b>
				                                                            <p style="font-size:14px; line-height:22px; font-weight:bold; color:#333333; margin:5px 5px 5px;"><a style="color:#6c7e44; text-decoration:none;">Se&ntilde;or(a):&nbsp;'.$persona->apellido1." ".$persona->nombre1.'</a></p>
				                                                            <p style="align:justify; margin:5px 5px 10px; font-size:12px; line-height:18px; color:#333333;">'.$text.'</p>
				                                                        </td>
				                                                        <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
				                                                    </tr>
				                                                </tbody>
				                                            </table>
				                                            <!-- /fin articulos -->
				                                            <table style="margin:0; font-size:5px; line-height:5px;" bgcolor="#DDDDDD" width="600" cellpadding="0" cellspacing="0"><tr><td height="4">&nbsp;</td></tr></table>
				                                            <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#FFFFFF">
				                                                <tbody>
				                                                    <tr valign="top">
				                                                        <td width="30"><p style="margin:0; font-size:1px; line-height:1px;">&nbsp;</p></td>
				                                                        <td width="400">
				                                                            <p style="margin:5px 5px 5px; font-weight:bold; color:#333333; font-size:14px; line-height:15px;">Contacto</p>
				                                                            <p style="margin:5px 5px 5px; color:#333333; font-size:11px; line-height:15px;">Direcci&oacute;n:  Av. Francisco de Miranda, Torre del Ministerio del Poder Popular para Hábitat y Vivienda,
Municipio Chacao, Edo. Miranda, Venezuela.</p>                                                                                
				                                                            <p style="margin:5px 5px 5px; color:#333333; font-size:11px; line-height:15px;">Tel&eacute;fono 0212-274.71.81 / 72.11 <br>
				                                                                Ir al sitio: <a href="http://www.minhvi.gob.ve/index.php" target="_blank" style="color:#3399cc; text-decoration:none; font-weight:bold;">www.minhvi.gob.ve</a>
				                                                            </p>
				                                                        </td>
				                                                    </tr>
				                                                </tbody>
				                                            </table>
				                                            <!-- fin contenido --> 
				                                            <table style="margin:0; font-size:5px; line-height:5px;" bgcolor="#DDDDDD" width="600" cellpadding="0" cellspacing="0"><tr><td height="20">&nbsp;</td></tr></table>
				                                        </td>
				                                    </tr>
				                                </tbody>
				                            </table>
				                        </td>
				                    </tr>
				                </tbody>
				            </table>
				            <!-- end main block -->
				        </td>
				    </tr>
				</tbody>
				</table>';

		  return $mensaje;
	}        

        
	public static function claveAleatoria(){
		$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$cad = "";
		for($i=0;$i<10;$i++) {
		$cad .= substr($str,rand(0,62),1);
		}
		return $cad;
	}


	public static function certificado_preasignacion($model){
		Yii::import('application.modules.factura_mercancia.models.Automovil',true);
		Yii::import('application.modules.factura_mercancia.models.DesgloceFactura',true);
		Yii::import('application.modules.factura_mercancia.models.Factura',true);
		Yii::import('application.modules.tablas_referencias.models.Estado',true);
		Yii::import('application.modules.tablas_referencias.models.Color',true);
		Yii::import('application.modules.tablas_referencias.models.Direccion',true);
		Yii::import('application.modules.tablas_referencias.models.Municipio',true);
		Yii::import('application.modules.tablas_referencias.models.Parroquia',true);
		Yii::import('application.modules.infraestructura.models.Almacen',true);
		Yii::import('application.modules.usuario.models.GenPersona',true);
		Yii::import('application.modules.solicitud.models.Solicitud',true);
		Yii::import('application.modules.tablas_referencias.models.SituacionPreVenta',true);
		Yii::import('application.modules.vitrina.models.Vehiculo',true);
		Yii::import('application.modules.pre_venta.models.Certificados',true);

		//print_r($model->attributes); die();


			$header='
					<table border="0" width="100%" style="margin:0px;">
			            <tr>
			                <td align="left">
			              		<img width = "1024" heigth = "100" src="'.Yii::app()->request->baseUrl.'/images/img_banner_footer_documentos/banner.png" />
			   				</td>
			            </tr>
			        </table>'; 

			$html='Señor(a)'.$model->solicitud->persona->apellido1.' '.$model->solicitud->persona->nombre1.' portadora de la cedula de identidad'.$model->solicitud->persona->cedula.'.';

				$footer='
					<div class="letra" style="text-align:right;font-size:10px;">
					    Pág. {PAGENO}/{nbpg}
					</div>
					<div style="text-align:center;border-top-width:2px;border-top-style:solid;border-top-color:#AD1818;font-size:10px;font-family: arial;">
					   MINISTERIO DEL PODER POPULAR PARA INDUSTRIA Y COMERCIO - Av. Urdaneta esquina de Ibarra, Edificio Central. Caracas - Venezuela<br>
					    Telfs: (0212) 596.7616 / (0212) 596.7612 / (0212) 596.760 / (0212) 596.7668 <br>
					    <span align="center"> RIF.: G-200118628 <span>
					</div>';

			$url_certificado_pre_venta=YiiBase::getPathOfAlias("webroot").Yii::app()->params['RutaCertificados']."certificado_".$model->solicitud->persona->cedula."_".date('d-m-Y').'.pdf';

			$pdf = Yii::createComponent('application.extensions.mpdf53.mpdf');
			$mpdf=new mPDF('utf-8','LETTER','','',15,15,25,12,7,7);
			$mpdf->SetHTMLHeader($header,'O');
			$mpdf->SetHTMLHeader($header,'E');
			$mpdf->SetHTMLFooter($footer,'O');
			$mpdf->SetHTMLFooter($footer,'E');
			$mpdf->WriteHTML("$html");
			$mpdf->Output($url_certificado_pre_venta, 'F');			
			//$mpdf->Output('Comprobante.pdf','I');
			//exit;

			$model_certificado=new Certificados;
			$model_certificado->archivo_certificado=$url_certificado_pre_venta;
			$model_certificado->fecha=date('d-m-Y');
			$model_certificado->persona_id=$model->solicitud->persona->persona_id;
			$model_certificado->pre_venta_id=$model->pre_venta_id;
			$model_certificado->estatus=true;
			$model_certificado->solicitud_id=$model->solicitud->solicitud_id;
		
				if($model_certificado->save()) {
                    return $url_certificado_pre_venta;
				}
				else {
					unlink($url_certificado_pre_venta);
		   			return $url_certificado_pre_venta=false;
				}


		
	}


        
}