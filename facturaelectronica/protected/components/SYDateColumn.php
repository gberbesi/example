<?php

Yii::import('zii.widgets.grid.CDataColumn');

class SYDateColumn extends CDataColumn
{

	/**
	 * if filter is false then no show filter
	 * else if filter is 'range' string then show from input to input
	 * else if filter is 'single' string then show input
	 * @var mixed 
	 */
	public $filter='range';
	
	public $language = false;
	
	/**
	 * jquery-ui theme name
	 * @var string
	 */
	public $theme = 'base';
	
	public $fromText = false;
	
	public $toText = false;
	
	public $dateFormat = 'dd-mm-yy';
	
	
	public $dateLabelStyle="width:30%;display:block;float:left;";
	
	public $dateOptions = array('changeMonth' => 'true', 'changeYear' => 'true');

	/**
	 * Renders the filter cell content.
	 */
	protected function renderFilterCellContent()
	{
		if($this->filter!==true && $this->grid->filter!==null && $this->name!==null && strpos($this->name,'.')===false)
		{
			if ($this->filter=='range') {
							echo CHtml::tag('div', array(), "<span>". $this->fromText ."</span>" . CHtml::activeTextField($this->grid->filter, $this->name.'_range[from]', array('class'=>'filter-date form-control text-center','placeholder'=>'Día/Mes/Año','onKeyUp'=>'this.value=formateafecha(this.value)'))    );
							echo CHtml::tag('div', array(), "<span>". $this->toText ."</span>". CHtml::activeTextField($this->grid->filter, $this->name.'_range[to]', array('class'=>'filter-date form-control text-center','placeholder'=>'Día/Mes/Año','onKeyUp'=>'this.value=formateafecha(this.value)')));
						}
						else {
							echo CHtml::tag('div', array(), CHtml::activeTextField($this->grid->filter, $this->name.'_range[to]', array('class'=>'filter-date')));
						}
			$options=CJavaScript::encode($this->dateOptions);

			if ($this->language!==false) {
$js=<<<EOD
$(filter_date).datepicker(jQuery.extend({dateFormat:'{$this->dateFormat}'}, jQuery.datepicker.regional['{$this->language}'], {$options}));
EOD;
}
else {
$js=<<<EOD
$(filter_date).datepicker(jQuery.extend({dateFormat:'{$this->dateFormat}'}, {$options}));
EOD;
}
$js=<<<EOD
var filter_date='#{$this->grid->id} input[class="filter-date"]';
$('body').delegate(filter_date, 'mousedown', function(){
{$js}
});
EOD;
		}
		else
			parent::renderFilterCellContent();
	}

}
?>
