<?php 
class SiscamHcmConnection extends CActiveRecord 
{
 
    private static $siscam = null;
 
    protected static function getSiscamHcmDbConnection()
    {
        if (self::$siscam !== null)
            return self::$siscam;
        else
        	{
            self::$siscam = Yii::app()->dbSiscam;
            if (self::$siscam instanceof CDbConnection)
            {
                self::$siscam->setActive(true);
                return self::$siscam;
            }
            else
               throw new CDbException(Yii::t('yii','Active Record requires a "dbSiscam" CDbConnection application component'));
        }
    }
}

/*
class UrbUrbanismo extends  SigevihConnection
{
    
    ///**********************************CONEXION***********************************

    public function getDbConnection()
    {
        return self::getSiscamHcmDbConnection();
    }*/
    /**********************************CONEXION***********************************/


?>
