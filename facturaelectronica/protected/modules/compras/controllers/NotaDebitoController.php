<?php

class NotaDebitoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','tipoBusqueda','BusquedaPro','DetalleMaterial3','BuscarCst','Anulacion','Anular','Reportid','Fact','Fact2','Fact3','Fact4','Fact5','Fact6','Fact7','Fact8','Fact12','Fact14','Fact15','Fact16','Fact17','Fact18'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{	
		yii::import('application.modules.facturas.models.Descuento',true);
		yii::import('application.modules.compras.models.FacturaCompra',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.compras.models.TablaProductoComprand',true);


		$model=new NotaDebito;
		$des=new Descuento;
		$fc=new FacturaCompra;
		$pdc=new Productos;
		$tpnd=new TablaProductoComprand;
			//var_dump($_POST);exit;
		$fc->unsetAttributes();  // clear any default values
		if(isset($_GET['FacturaCompra']))
			$modelPro->attributes=$_GET['FacturaCompra'];

		if(isset($_POST['NotaDebito']))
		{
			$model->numref=$_POST['NotaDebito']['id_factura'];

			$cod=explode(",", $_POST['NotaDebito']['arraycodp']);
			$art=explode(",", $_POST['NotaDebito']['arrayart']);
			$pre=explode(",", $_POST['NotaDebito']['arrayprecp']);
			$cnt=explode(",", $_POST['NotaDebito']['arraycantp']);
			$tot=explode(",", $_POST['NotaDebito']['arraytotp']);
			$des=explode(",", $_POST['NotaDebito']['arraydesp']);
			$ivp=explode(",", $_POST['NotaDebito']['arrayivp']);
			$idp=explode(",", $_POST['NotaDebito']['arrayidp']);
			$idpvd = $_POST['NotaDebito']['idpv'];

			$time=count($cod);

			$model->attributes=$_POST['NotaDebito'];
			$model->cambio=$_POST['NotaDebito']['cambio'];
			$model->id_moneda=$_POST['NotaDebito']['mone'];
			if($model->save())

            	for($i=1;$i<$time;$i++){

            		$tnd=new TablaProductoComprand;
            		$tnd->codigo_produc=$cod[$i];
            		$tnd->nombre_produc=$art[$i];
            		$tnd->costo_producto=$pre[$i];
            		$tnd->cantidad=$cnt[$i];
            		$tnd->total=$tot[$i];
            		$tnd->descuento=$des[$i];
            		$tnd->iv=$ivp[$i];
            		$tnd->id_factura=$model->id_factura;
            		$tnd->id_provedor=$idpvd;
            		$tnd->id_producto=$idp[$i];
            		$tnd->id_nota=$model->id_nota;
            		$id =$model->id_nota;
            		$idfac=$model->id_factura;
            		Yii::app()->user->setState('idruta',$id);
            		Yii::app()->user->setState('idfac',$idfac);
            		$tnd->save(false);
            	}
				$this->redirect(array('Fact'));
		}

		$this->render('create',array(
			'model'=>$model,
			'des'=>$des,
			'fc'=>$fc,
			'pdc'=>$pdc
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NotaDebito']))
		{
			$model->attributes=$_POST['NotaDebito'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_nota));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{	
		$model=$this->loadModel($id);

		$ndbt= NotaDebito::model()->find('id_nota='.$id.' and anulado=1');
		
		$idnt= $ndbt->id_nota;
		if($idnt){
		$ndcdt= NotaDebito::model()->find('id_ndbt='.$idnt);

		$conse= $ndcdt->consecutivo;
		}
		
		
	
		if(!$conse){
			$con=1;
			$model->eliminado=1;
			$model->save(false);	
		}else{
			$con= 0;
			
	
		}
		echo CJSON::encode(array('conse'=>$conse,'ndcdt'=>$ndcdt,'con'=>$con));
			
		//$this->loadModel($id)->delete();

		
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('NotaDebito');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{	

		yii::import('application.modules.usuario.models.Usuario');
		yii::import('application.modules.facturas.models.Impresion');
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$form=Impresion::model()->find('institucion_id='.$empreid);
		$ruta=$form->ruta;
		$vista=$form->vista;
		$model=new NotaDebito('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NotaDebito']))
			$model->attributes=$_GET['NotaDebito'];
		
		$this->render('admin',array(
			'model'=>$model,
			'ruta'=>$ruta,
			'vista'=>$vista,
		));
	}
	
	public function actionTipobusqueda()
	{
	
		yii::import('application.modules.compras.models.FacturaCompra',true);
		yii::import('application.modules.compras.models.TablaProductoCompra',true);
		$facturac=FacturaCompra::model()->find('id_factura_compra='.$_POST['id_factura']);
		if(!$facturac){
		$facturac=1;
		$prove=1;
		$tapro=1;
		}else{
		$tapro=TablaProductoCompra::model()->findAll('id_factura='.$_POST['id_factura']);
		$idpro=$facturac->id_provedor;
		$prove=Proveedores::model()->find('id_proveedor='.$idpro);
		}
		echo CJSON::encode(array('factura'=>$facturac,'proveedor'=>$prove,'tapro'=>$tapro));

	}
	public function actionBusquedapro()
	{	
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.facturas.models.ListaImpuesto',true);
		$prod=Productos::model()->find('codigo="'.$_POST['articulo'].'"');
	if(!$prod){
		$prod=1;
		$idim='0';
	}else{
		$idim=$prod->lista_impuesto_id;
	}	
	if($idim=='0'){
		$imp['monto']=0;
	}else{
		$imp=ListaImpuesto::model()->find('id_impuesto='.$idim);
	}	
	echo CJSON::encode(array('prod'=>$prod,'imp'=>$imp));
}
public function actionReportid(){
  		$id=$_POST['id'];
  		echo CJSON::encode($id);
		Yii::app()->user->setState('idruta',$id);
		$nc=NotaDebito::model()->find('id_nota='.$id);
  		$idfac=$nc->id_factura;
		Yii::app()->user->setState('idfac',$idfac);
  	}
public function actionDetalleMaterial3()
	{ 
	Yii::import("application.modules.registro.models.Productos", true); 
		$prod=Productos::model()->findByPk($_POST['articulo']);
		$idim=$prod->lista_impuesto_id;
		if($idim=='0'){
			$imp['monto']=0;
		}else{
			$imp=ListaImpuesto::model()->find('id_impuesto='.$idim);
		}

		
		echo CJSON::encode(array('prod'=>$prod,'imp'=>$imp));
		
	}
	public function actionBuscarCst()
  	{
    	$csc = NotaDebito::model()->find('consecutivo='.$_POST['consecutivo'].' and cedula='.$_POST['ci']);
    	if(!$csc){
      		$csc=1;
 		   }else{
   		 	$csc=0;
  		  }
    	echo CJSON::encode(array('csc'=>$csc));
    
  	}
  
	public function actionAnular(){

  		$csc = NotaDebito::model()->find('id_nota='.$_POST['id_nota'].' and anulado=1');

    	if(!$csc){
      	$csc=1;
   		 }else{
    	$csc=0;
   		 }

    	echo CJSON::encode(array('csc'=>$csc));

  	}
	public function actionAnulacion()
	{

		$ndbt= NotaDebito::model()->find('id_nota='.$_POST['id_nota']);	
		$tpc= TablaProductoComprand::model()->findAll('id_nota='.$_POST['id_nota']);	
		$ncdt=new NotaCredito;	
	
		$l=count($tpc);
			
			
		//var_dump($tpc[0]);exit;
		$ncdt->actualiza=$ndbt->actualiza;
		$ncdt->consecutivo=$_POST['consecutivo'];
		$ncdt->descuento=$ndbt->descuento;
		$ncdt->cambio=$ndbt->cambio;
		$ncdt->fecha_nota=$ndbt->fecha_nota;
		$ncdt->cedula=$ndbt->cedula;
		$ncdt->id_factura=$ndbt->id_factura;
		$ncdt->id_ndbt=$_POST['id_nota'];
		$ncdt->id_moneda=$ndbt->id_moneda;
		$ncdt->iv=$ndbt->iv;
		$ncdt->motivo=$_POST['motivo'];
		$ncdt->nombre_cliente=$ndbt->nombre_cliente;
		$ncdt->otroimpuesto=$ndbt->otroimpuesto;
		$ncdt->referencia=$ndbt->referencia;
		$ncdt->subtotal=$ndbt->subtotal;
		$ncdt->total=$ndbt->total;
		$ncdt->docref="ND";
		$ncdt->numref=$_POST['id_nota'];
		$ndbt->anulado=1;
		$ndbt->save(false);
		$ncdt->save(false);
		foreach ($tpc as $key => $value) {
		$tp=new TablaProductoCompranc;
		//var_dump($tpc[0]);exit;
		$tp->id_factura=$tpc[$key]->id_factura;
		$tp->id_nota=$tpc[$key]->id_nota;
		$tp->id_provedor=$tpc[$key]->id_provedor;
		$tp->id_producto=$tpc[$key]->id_producto;
		$tp->codigo_produc=$tpc[$key]->codigo_produc;
		$tp->nombre_produc=$tpc[$key]->nombre_produc;
		$tp->costo_producto=$tpc[$key]->costo_producto;
		$tp->cantidad=$tpc[$key]->cantidad;
		$tp->total=$tpc[$key]->total;
		$tp->descuento=$tpc[$key]->descuento;
		$tp->iv=$tpc[$key]->iv;
		$tp->id_nota=$ncdt->id_nota;
		$tp->save(false);
			
	
		}
		
		
		echo CJSON::encode(array('debito'=>$ndbt,'credito'=>$ncdt));
	}
	public function actionFact(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$nct=NotaDebito::model()->find('id_nota='.$id);
		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;

		$this->render('Fact',
			   array(
				     'model'=>$model,
					 'tpc'=>$tpc,
					 'nct'=>$nct,
					 'contribuyente'=>$contribuyente,
					 'cliente'=>$cliente,
		));
	}
	public function actionFact4(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$nct=NotaDebito::model()->find('id_nota='.$id);
		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;

		$this->render('Fact4',
			   array(
				     'model'=>$model,
					 'tpc'=>$tpc,
					 'nct'=>$nct,
					 'contribuyente'=>$contribuyente,
					 'cliente'=>$cliente,
		));
	}
	public function actionFact2(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaDebito::model()->find('id_nota='.$id);

		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact2',array(
			'model'=>$model,
			'tpc'=>$tpc,
			'nct'=>$nct,
			'contribuyente'=>$contribuyente,
			'cliente'=>$cliente,
			'provincia'=>$provincia,
			'canton'=>$canton,
			'distrito'=>$distrito,
		));
	}
	public function actionFact3(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$nct=NotaDebito::model()->find('id_nota='.$id);
		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;

		$this->render('Fact3',
			   array(
				     'model'=>$model,
					 'tpc'=>$tpc,
					 'nct'=>$nct,
					 'contribuyente'=>$contribuyente,
					 'cliente'=>$cliente,
		));
	}
	public function actionFact8(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaDebito::model()->find('id_nota='.$id);
		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact8',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionFact5(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaDebito::model()->find('id_nota='.$id);

		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact5',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionFact6(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaDebito::model()->find('id_nota='.$id);

		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact6',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
		public function actionFact7(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaDebito::model()->find('id_nota='.$id);

		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact7',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionFact12(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaDebito::model()->find('id_nota='.$id);

		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact12',array(
			'model'=>$model,
			'tpc'=>$tpc,
			'nct'=>$nct,
			'contribuyente'=>$contribuyente,
			'cliente'=>$cliente,
			'provincia'=>$provincia,
			'canton'=>$canton,
			'distrito'=>$distrito,
		));
	}
	public function actionFact14(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$nct=NotaDebito::model()->find('id_nota='.$id);
		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;

		$this->render('Fact14',
			   array(
				     'model'=>$model,
					 'tpc'=>$tpc,
					 'nct'=>$nct,
					 'contribuyente'=>$contribuyente,
					 'cliente'=>$cliente,
		));
	}
	public function actionFact15(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaDebito::model()->find('id_nota='.$id);

		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact15',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionFact16(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaDebito::model()->find('id_nota='.$id);

		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact16',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionFact17(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaDebito::model()->find('id_nota='.$id);

		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact17',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionFact18(){
		yii::import('application.modules.compras.models.TablaProductoComprand',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaDebito::model()->find('id_nota='.$id);

		$tpc= TablaProductoComprand::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact18',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}


	public function loadModel($id)
	{
		$model=NotaDebito::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param NotaDebito $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='nota-debito-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
