<?php

class NotaCreditoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','tipoBusqueda','BusquedaPro','DetalleMaterial3','Fact','BuscarCst','Anulacion','Anular','Fact2','Fact3','Fact4','Fact5','Fact6','Fact7','Fact8','Fact12','Fact16','Fact14','Fact15','Fact17','Fact18','Reportid'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{	
		yii::import('application.modules.facturas.models.Descuento',true);
		yii::import('application.modules.compras.models.FacturaCompra',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);


		$model=new NotaCredito;
		$des=new Descuento;
		$fc=new FacturaCompra;
		$pdc=new Productos;
		$tpnc=new TablaProductoCompranc;

		$fc->unsetAttributes();  // clear any default values
		if(isset($_GET['FacturaCompra']))
			$modelPro->attributes=$_GET['FacturaCompra'];

		if(isset($_POST['NotaCredito']))
		{

			
			$cod=explode(",", $_POST['NotaCredito']['arraycodp']);
			$art=explode(",", $_POST['NotaCredito']['arrayart']);
			$pre=explode(",", $_POST['NotaCredito']['arrayprecp']);
			$cnt=explode(",", $_POST['NotaCredito']['arraycantp']);
			$tot=explode(",", $_POST['NotaCredito']['arraytotp']);
			$des=explode(",", $_POST['NotaCredito']['arraydesp']);
			$ivp=explode(",", $_POST['NotaCredito']['arrayivp']);
			$idp=explode(",", $_POST['NotaCredito']['arrayidp']);


			$time=count($cod);

			$model->attributes=$_POST['NotaCredito'];
			$idpvd = $_POST['NotaCredito']['idpv'];
			$model->id_moneda = $_POST['NotaCredito']['mone'];
			
			if($model->save())

            	for($i=1;$i<$time;$i++){
            		$tnc=new TablaProductoCompranc;
            		$tnc->codigo_produc=$cod[$i];
            		$tnc->nombre_produc=$art[$i];
            		$tnc->costo_producto=$pre[$i];
            		$tnc->cantidad=$cnt[$i];
            		$tnc->total=$tot[$i];
            		$tnc->descuento=$des[$i];
            		$tnc->iv=$ivp[$i];
            		$tnc->id_factura=$model->id_factura;
            		$tnc->id_provedor=$idpvd;
            		$tnc->id_producto=$idp[$i];
            		$tnc->id_nota=$model->id_nota;
            		$id =$model->id_nota;
            		$idfac=$model->id_factura;
            		Yii::app()->user->setState('idruta',$id);
            		Yii::app()->user->setState('idfac',$idfac);
            		$tnc->save(false);
            	}
            	
				$this->redirect(array('Fact'));
			}

		$this->render('create',
			   array(
					  'model'=>$model,
					  'des'=>$des,
					  'fc'=>$fc,
					  'pdc'=>$pdc
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NotaCredito']))
		{
			$model->attributes=$_POST['NotaCredito'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_nota));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		
		$model=$this->loadModel($id);
			$ndbt= NotaCredito::model()->find('id_nota='.$id.' and anulado=1');		
			$idnt= $ndbt->id_nota;
		if($idnt){
			$ndcdt= NotaDebito::model()->find('id_ncdt='.$idnt);
			$conse= $ndcdt->consecutivo;
		}	
		if(!$conse){
			$con=1;
			$model->eliminado=1;
			$model->save(false);	
		}else{
			$con= 0;
	
		}
		echo CJSON::encode(array('conse'=>$conse,'ndcdt'=>$ndcdt,'con'=>$con));
			
		//$this->loadModel($id)->delete();

		
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('NotaCredito');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{	
		yii::import('application.modules.usuario.models.Usuario');
		yii::import('application.modules.facturas.models.Impresion');
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$form=Impresion::model()->find('institucion_id='.$empreid);
		$ruta=$form->ruta;
		$vista=$form->vista;		
		$model=new NotaCredito('search');		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NotaCredito']))
			$model->attributes=$_GET['NotaCredito'];

		$this->render('admin',array(
			'model'=>$model,
			'ruta'=>$ruta,
			'vista'=>$vista,

		));
	}
	public function actionBuscarCst()
  	{
  		
    	$csc = NotaCredito::model()->find('consecutivo='.$_POST['consecutivo'].' and cedula='.$_POST['ci']);

    	if(!$csc){
      	$csc=1;
    }else{
    	$csc=0;
    }

    	echo CJSON::encode(array('csc'=>$csc));
    
  	}
  	public function actionReportid(){
  		$id=$_POST['id'];
		Yii::app()->user->setState('idruta',$id);
		$nc=NotaCredito::model()->find('id_nota='.$id);
  		$idfac=$nc->id_factura;
		Yii::app()->user->setState('idfac',$idfac);
  	}
  	public function actionAnular(){

  		$csc = NotaCredito::model()->find('id_nota='.$_POST['id_nota'].' and anulado=1');

    	if(!$csc){
      	$csc=1;
   		 }else{
    	$csc=0;
   		 }

    	echo CJSON::encode(array('csc'=>$csc));

  	}
	public function actionFact(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$nct=NotaCredito::model()->find('id_nota='.$id);
		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;

		$this->render('Fact',
			   array(
				     'model'=>$model,
					 'tpc'=>$tpc,
					 'nct'=>$nct,
					 'contribuyente'=>$contribuyente,
					 'cliente'=>$cliente,
		));
	}
	public function actionFact4(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$nct=NotaCredito::model()->find('id_nota='.$id);
		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;

		$this->render('Fact4',
			   array(
				     'model'=>$model,
					 'tpc'=>$tpc,
					 'nct'=>$nct,
					 'contribuyente'=>$contribuyente,
					 'cliente'=>$cliente,
		));
	}
	public function actionFact2(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaCredito::model()->find('id_nota='.$id);
		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact2',array(
			'model'=>$model,
			'tpc'=>$tpc,
			'nct'=>$nct,
			'contribuyente'=>$contribuyente,
			'cliente'=>$cliente,
			'provincia'=>$provincia,
			'canton'=>$canton,
			'distrito'=>$distrito,
		));
	}
	public function actionFact3(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$nct=NotaCredito::model()->find('id_nota='.$id);
		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;

		$this->render('Fact3',
			   array(
				     'model'=>$model,
					 'tpc'=>$tpc,
					 'nct'=>$nct,
					 'contribuyente'=>$contribuyente,
					 'cliente'=>$cliente,
		));
	}
	public function actionFact8(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaCredito::model()->find('id_nota='.$id);

		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact8',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionFact5(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaCredito::model()->find('id_nota='.$id);

		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact5',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionFact6(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaCredito::model()->find('id_nota='.$id);

		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact6',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
		public function actionFact7(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaCredito::model()->find('id_nota='.$id);

		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact7',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionFact12(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaCredito::model()->find('id_nota='.$id);

		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact12',array(
			'model'=>$model,
			'tpc'=>$tpc,
			'nct'=>$nct,
			'contribuyente'=>$contribuyente,
			'cliente'=>$cliente,
			'provincia'=>$provincia,
			'canton'=>$canton,
			'distrito'=>$distrito,
		));
	}
	public function actionFact14(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$nct=NotaCredito::model()->find('id_nota='.$id);
		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;

		$this->render('Fact14',
			   array(
				     'model'=>$model,
					 'tpc'=>$tpc,
					 'nct'=>$nct,
					 'contribuyente'=>$contribuyente,
					 'cliente'=>$cliente,
		));
	}
	public function actionFact15(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaCredito::model()->find('id_nota='.$id);

		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact15',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionFact16(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaCredito::model()->find('id_nota='.$id);

		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact16',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionFact17(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaCredito::model()->find('id_nota='.$id);

		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact17',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionFact18(){
		yii::import('application.modules.compras.models.TablaProductoCompranc',true);
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.registro.models.UnidadMedida',true);
		yii::import('application.modules.facturas.models.Moneda',true);
		Yii::import("application.modules.usuario.models.Usuario", true);
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;
		$id=Yii::app()->user->getState('idruta');
		$idfac=Yii::app()->user->getState('idfac');
		$contribuyente=Contribuyente::model()->find('institucion_id='.$empreid);
		$factura=FacturaCompra::model()->find('id_factura_compra='.$idfac);
		$idprov=$factura->id_provedor;
		$cliente=Proveedores::model()->find('id_proveedor='.$idprov);
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$nct=NotaCredito::model()->find('id_nota='.$id);

		$tpc= TablaProductoCompranc::model()->findAll('id_factura='.$idfac.' and id_nota='.$id);
		$ids=14;
		$this->render('Fact18',
			   array( 'model'=>$model,
					  'tpc'=>$tpc,
					  'nct'=>$nct,
					  'contribuyente'=>$contribuyente,
					  'cliente'=>$cliente,
					  'provincia'=>$provincia,
					  'canton'=>$canton,
					  'distrito'=>$distrito,
				));
	}
	public function actionTipobusqueda()
	{
		yii::import('application.modules.compras.models.FacturaCompra',true);
		yii::import('application.modules.compras.models.TablaProductoCompra',true);
			$facturac=FacturaCompra::model()->find('id_factura_compra='.$_POST['id_factura']);
		if(!$facturac){
			$facturac=1;
			$prove=1;
			$tapro=1;
		}else{
			$tapro=TablaProductoCompra::model()->findAll('id_factura='.$_POST['id_factura']);
			$idpro=$facturac->id_provedor;
			$prove=Proveedores::model()->find('id_proveedor='.$idpro);
		}
			echo CJSON::encode(array('factura'=>$facturac,'proveedor'=>$prove,'tapro'=>$tapro));

	}
	public function actionBusquedapro()
	{	
		yii::import('application.modules.registro.models.Productos',true);
		yii::import('application.modules.facturas.models.ListaImpuesto',true);
			$prod=Productos::model()->find('codigo="'.$_POST['articulo'].'"');
		if(!$prod){
			$prod=1;
			$idim='0';
		}else{
			$idim=$prod->lista_impuesto_id;
		}		
		if($idim=='0'){
			$imp['monto']=0;
		}else{
			$imp=ListaImpuesto::model()->find('id_impuesto='.$idim);
		}	
			echo CJSON::encode(array('prod'=>$prod,'imp'=>$imp));
}
public function actionDetalleMaterial3()
	{ 
	Yii::import("application.modules.registro.models.Productos", true); 
		$prod=Productos::model()->findByPk($_POST['articulo']);
		$idim=$prod->lista_impuesto_id;
		if($idim=='0'){
			$imp['monto']=0;
		}else{
			$imp=ListaImpuesto::model()->find('id_impuesto='.$idim);
		}		
		echo CJSON::encode(array('prod'=>$prod,'imp'=>$imp));		
	}
	public function actionAnulacion(){
		$ndbt= NotaCredito::model()->find('id_nota='.$_POST['id_nota']);
		$tpc= TablaProductoCompranc::model()->findAll('id_nota='.$_POST['id_nota']);
		
		$ncdt=new NotaDebito;		
		$ncdt->actualiza=$ndbt->actualiza;
		$ncdt->consecutivo=$_POST['consecutivo'];
		$ncdt->descuento=$ndbt->descuento;
		$ncdt->cambio=$ndbt->cambio;
		$ncdt->fecha_nota=$ndbt->fecha_nota;
		$ncdt->cedula=$ndbt->cedula;
		$ncdt->id_factura=$ndbt->id_factura;
		$ncdt->id_ncdt=$_POST['id_nota'];
		$ncdt->id_moneda=$ndbt->id_moneda;
		$ncdt->iv=$ndbt->iv;
		$ncdt->motivo=$_POST['motivo'];
		$ncdt->nombre_cliente=$ndbt->nombre_cliente;
		$ncdt->otroimpuesto=$ndbt->otroimpuesto;
		$ncdt->referencia=$ndbt->referencia;
		$ncdt->subtotal=$ndbt->subtotal;
		$ncdt->total=$ndbt->total;
		$ncdt->docref="NC";
		$ncdt->numref=$_POST['id_nota'];
		$ndbt->anulado=1;
		$ndbt->save(false);
		$ncdt->save(false);
		foreach ($tpc as $key => $value) {
		$tp=new TablaProductoCompranda;
		//var_dump($tpc[0]);exit;
		$tp->id_factura=$tpc[$key]->id_factura;
		$tp->id_nota=$tpc[$key]->id_nota;
		$tp->id_provedor=$tpc[$key]->id_provedor;
		$tp->id_producto=$tpc[$key]->id_producto;
		$tp->codigo_produc=$tpc[$key]->codigo_produc;
		$tp->nombre_produc=$tpc[$key]->nombre_produc;
		$tp->costo_producto=$tpc[$key]->costo_producto;
		$tp->cantidad=$tpc[$key]->cantidad;
		$tp->total=$tpc[$key]->total;
		$tp->descuento=$tpc[$key]->descuento;
		$tp->iv=$tpc[$key]->iv;
		$tp->id_nota=$ncdt->id_nota;
		$tp->save(false);
			
	
		}
		echo CJSON::encode(array('debito'=>$ndbt,'credito'=>$ncdt));
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return NotaCredito the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=NotaCredito::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	/**
	 * Performs the AJAX validation.
	 * @param NotaCredito $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='nota-credito-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
