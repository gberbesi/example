<?php

class AjusteInventarioController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request 
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		Yii::import("application.modules.registro.models.Productos", true); 
		Yii::import("application.modules.registro.models.Identificacion", true); 
		Yii::import("application.modules.registro.models.TipoCodigo", true); 
		Yii::import("application.modules.registro.models.Clientes",true);
		Yii::import("application.modules.registro.models.Productos",true);
		Yii::import("application.modules.registro.models.ListaImpuesto",true);
		Yii::import("application.modules.facturas.models.TablaProducto",true);

		$model=new AjusteInventario;
		$modelPro=new Productos; 

		$modelClie=new Proveedores;
		$modelIde=new Identificacion;
		$tablaProducto=new TablaProducto;
		$TablaProductoProforma=new TablaProductoCompra;

	    $clientes=ListaImpuesto::model()->findAll();
	    $clientes2=Clientes::model()->findAll();
	    $clientes3=Productos::model()->findAll();
		
  		$modelClie->unsetAttributes();  // clear any default values
		if(isset($_GET['Clientes']))
			$modelClie->attributes=$_GET['Clientes']; 
		
		$modelPro->unsetAttributes();  // clear any default values
		if(isset($_GET['Productos']))
			$modelPro->attributes=$_GET['Productos'];

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AjusteInventario']))
		{
			
			$model->attributes=$_POST['AjusteInventario'];
			$model->consecutivo=125;
			$model->total2=$_POST['AjusteInventario']['total2'];
			$model->motivo=$_POST['AjusteInventario']['motivo'];
			$model->tipo_cambio=$_POST['AjusteInventario']['tipo_cambio'];
			$model->iv=$_POST['AjusteInventario']['iv'];
			$model->subtotal=$_POST['AjusteInventario']['subtotal'];
			$model->fecha=$_POST['AjusteInventario']['fecha'];

			
			$tablaProducto->attributes=$_POST['TablaProducto'];
			$tablaProducto->codigo_produc=$_POST['TablaProducto']['codigo_produc'];

			$arrayid=explode(",", $_POST['TablaProducto']['id_producto']); 
			$arraycodigo=explode(",", $_POST['TablaProducto']['codigo_produc']); 
			$arraynombre=explode(",", $_POST['TablaProducto']['nombre_produc']);
			$arraytotal=explode(",", $_POST['TablaProducto']['total']);
			$arraycantidad=explode(",", $_POST['TablaProducto']['cantidad']);
			$arraycosto=explode(",", $_POST['TablaProducto']['costo_producto']);
			$arraydescuento=explode(",", $_POST['TablaProducto']['descuento']);
			$arrayiv=explode(",", $_POST['TablaProducto']['iv']);
		
			$valido=$model->validate();
			if($valido){
				
			if( $model->total2!=""){
			$model->save();
			
			if($_POST['AjusteInventario']['accion']==0){
				$this->redirect(array('create&guard=1')); 
			}
			if($_POST['AjusteInventario']['accion']==1){

				foreach ($arraycodigo as $key => $value) {
              		 		
      				$TablaProductoProforma=Productos::model()->findByPk($arrayid[$key]);
      				$TablaProductoProforma->cantidad+=$arraycantidad[$key];
      				if($_POST['AjusteInventario']['moneda']==1){
      					$TablaProductoProforma->precioc_id=$arraycosto[$key];
      				}else{
      					$TablaProductoProforma->precios_id=$arraycosto[$key];
      				}
 		
              		$TablaProductoProforma->save(false);             		
              	}

				$this->redirect(array('create&guard=1')); 
			}
			if($_POST['AjusteInventario']['accion']==2){

								foreach ($arraycodigo as $key => $value) {
              		 		
      				$TablaProductoProforma=Productos::model()->findByPk($arrayid[$key]);
      				$TablaProductoProforma->cantidad-=$arraycantidad[$key];
      				if($_POST['AjusteInventario']['moneda']==1){
      					$TablaProductoProforma->precioc_id=$arraycosto[$key];
      				}else{
      					$TablaProductoProforma->precios_id=$arraycosto[$key];
      				}
 		
              		$TablaProductoProforma->save(false);             		
              	}

				$this->redirect(array('create&guard=1')); 
			}
			
          
          }				

				
			}
		}

		$this->render('create',array(
			'model'=>$model,'modelPro'=>$modelPro,'modelClie'=>$modelClie,'clientes'=>$clientes,'clientes2'=>$clientes2,'clientes3'=>$clientes3,'tablaProducto'=>$tablaProducto,
		'estadoListData'=>$estadoListData)); 

	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AjusteInventario']))
		{
			$model->attributes=$_POST['AjusteInventario'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AjusteInventario');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AjusteInventario('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AjusteInventario']))
			$model->attributes=$_GET['AjusteInventario'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AjusteInventario the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AjusteInventario::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AjusteInventario $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ajuste-inventario-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
