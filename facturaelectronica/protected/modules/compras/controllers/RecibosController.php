<?php

class RecibosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','Tipobusqueda','Tipobusqueda2','DetalleFactura','DetalleProvee'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Recibos;
		$provee=new FacturaCompra;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
				$provee->unsetAttributes();  // clear any default values
		if(isset($_GET['Recibos']))
			$provee->attributes=$_GET['Recibos']; 

		if(isset($_POST['Recibos']))
		{
			$model->attributes=$_POST['Recibos'];
		$idfc=((int)$_POST['Recibos']['id_facturac']);
		$fc=FacturaCompra::model()->find('id_factura_compra='.$idfc);
	
			if($model->save()){
					$fc->saldo-=$_POST['Recibos']['abono'];

			$fc->save();
				

			

				$this->redirect(array('view','id'=>$model->id_recibo));
				}
		}

		$this->render('create',array(
			'model'=>$model,
			'provee'=>$provee
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Recibos']))
		{
			$model->attributes=$_POST['Recibos'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_recibo));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Recibos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Recibos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Recibos']))
			$model->attributes=$_GET['Recibos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionTipobusqueda()
	{
	

		$facturac=FacturaCompra::model()->find('id_factura_compra='.$_POST['id_facturac']);

		if(!$facturac){
		$facturac=1;
		}
		echo CJSON::encode($facturac);

	}
		public function actionTipobusqueda2()
	{
		

		$provee=Proveedores::model()->find('id_proveedor='.$_POST['proveed']);

		if(!$provee){
		$provee=1;
		}
		echo CJSON::encode($provee);

	}
		public function actionDetalleFactura()
	{

		$factc=FacturaCompra::model()->findByPk($_POST['factura']);
		
		
		//echo CJSON::encode($clientes);


		echo CJSON::encode($factc);
		

		
	}
		public function actionDetalleProvee()
	{

		$provee=Proveedores::model()->findByPk($_POST['provee']);
		
		
		//echo CJSON::encode($clientes);


		echo CJSON::encode($provee);
		

		
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Recibos the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Recibos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Recibos $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='recibos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
