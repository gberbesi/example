<?php

class ProveedoresController extends Controller 
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1'; 

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','admin2','DetalleMaterial2'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','SelectMunicipio','SelectParroquia','delete','Restaurar'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','admin2'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
 
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() 
	{		
		$model=new Proveedores;
        $estadoListData = CHtml::listData(Provincia::model()->findAll(), 'id_provincia', 'provincia');

        
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Proveedores']))
		{		
			Yii::import("application.modules.usuario.models.Usuario", true); 
			$idu=Yii::app()->user->id;
			$usuario=Usuario::model()->find('usuario_id='.$idu);
			$idins=$usuario->institucion_id;
			

			$model->attributes=$_POST['Proveedores'];
			$model->id_institucion=$idins;
			
			$valido=$model->validate();

			if($valido){
							
				$model->save();
				$this->redirect(array('create&guardado=1'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'estadoListData'=>$estadoListData,

		));
	}

	public function actionSelectMunicipio() {

        $id = (int) $_POST ['Proveedores']['id_provincia'];
        
         $lista = CHtml::listData(Canton::model()->findAll('provincia_id =:provincia_id', array(':provincia_id'=>$id)), 'id_canton', 'canton');

         echo CHtml::tag( array());

        foreach ($lista as $valor=>$municipio) {
            echo CHtml::tag('option', array('value'=>$valor), CHtml::encode($municipio), true);
        }

        
    }

     public function actionSelectParroquia() {
        $id = (int) $_POST ['Proveedores']['id_canton'];
        $lista = CHtml::listData(Distrito::model()->findAll('canton_id =:canton_id', array(':canton_id'=>$id)), 'id_distrito', 'distrito');

        echo CHtml::tag( array());

        foreach ($lista as $valor => $parroquia) {
            echo CHtml::tag('option', array('value'=>$valor), CHtml::encode($parroquia), true);
        }
     
}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{		
		$model=$this->loadModel($id);
		$estadoListData = CHtml::listData(Provincia::model()->findAll(), 'id_provincia', 'provincia');
		$lista = CHtml::listData(Distrito::model()->findAll('canton_id =:canton_id', array(':canton_id'=>$id)), 'id_distrito', 'distrito');
        $lista2 = CHtml::listData(Canton::model()->findAll('provincia_id =:provincia_id', array(':provincia_id'=>$id)), 'id_canton', 'canton');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Proveedores']))
		{
			$model->attributes=$_POST['Proveedores'];
			$valido=$model->validate();
			if($valido){
				Yii::app()->user->setFlash('success','Registro Exitoso');
				$model->save();
				$this->redirect(array('admin','id'=>$model->id_proveedor));
			}
		}

		$this->render('update',array(
			'model'=>$model,
			'estadoListData'=>$estadoListData,
			'lista2'=>$lista2,
			'lista'=>$lista
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->st_proveedor=0;
			$model->save();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	public function actionRestaurar($id)
	{
		$model =$this->loadModel($id);
			$model->st_proveedor=true;
			$model->save();
			
			if(!isset($_GET['ajax'])){
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Proveedores');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Proveedores('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Proveedores']))
			$model->attributes=$_GET['Proveedores'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionAdmin2()
	{
		$model=new Proveedores('search');
		$modelClie=new Proveedores('');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Proveedores']))
			$model->attributes=$_GET['Proveedores'];

		$this->render('admin2',array(
			'model'=>$model,
			'modelClie'=>$modelClie
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Proveedores the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Proveedores::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Proveedores $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='Proveedores-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

public function actionDetalleMaterial2()
	{

		$Proveedores=Proveedores::model()->find('identificacion='.$_POST['id_proveedor']);
		//echo CJSON::encode($Proveedores);

		if($Proveedores){
			$esta=1;
		}else{
			$esta=0;
		}

		echo CJSON::encode($esta);
		
	}	

}
