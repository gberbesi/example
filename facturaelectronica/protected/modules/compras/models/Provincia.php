<?php

/**
 * This is the model class for table "provincia".
 *
 * The followings are the available columns in table 'provincia':
 * @property integer $id_provincia
 * @property string $provincia
 * @property integer $id_canton
 * @property integer $id_distrito
 *
 * The followings are the available model relations:
 * @property Canton[] $cantons
 * @property Clientes[] $clientes
 */
class Provincia extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'provincia';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_provincia, provincia, id_canton, id_distrito', 'required'),
            array('id_provincia, id_canton, id_distrito', 'numerical', 'integerOnly'=>true),
            array('provincia', 'length', 'max'=>20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_provincia, provincia, id_canton, id_distrito', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cantons' => array(self::HAS_MANY, 'Canton', 'id_provincia'),
            'clientes' => array(self::HAS_MANY, 'Clientes', 'id_provincia'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_provincia' => 'Id Provincia',
            'provincia' => 'Provincia',
            'id_canton' => 'Id Canton',
            'id_distrito' => 'Id Distrito',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_provincia',$this->id_provincia);
        $criteria->compare('provincia',$this->provincia,true);
        $criteria->compare('id_canton',$this->id_canton);
        $criteria->compare('id_distrito',$this->id_distrito);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Provincia the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}