<?php

/**
 * This is the model class for table "tabla_producto_compranc".
 *
 * The followings are the available columns in table 'tabla_producto_compranc':
 * @property integer $id
 * @property integer $id_factura
 * @property integer $id_provedor
 * @property integer $id_producto
 * @property string $codigo_produc
 * @property string $nombre_produc
 * @property integer $costo_producto
 * @property integer $cantidad
 * @property integer $total
 * @property integer $descuento
 * @property integer $iv
 */
class TablaProductoCompranc extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tabla_producto_compranc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_factura, id_provedor, codigo_produc, nombre_produc, costo_producto, cantidad, total, descuento, iv', 'required'),
			array('id_factura, id_provedor, id_producto, costo_producto, cantidad, total, descuento, iv', 'numerical', 'integerOnly'=>true),
			array('codigo_produc', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_factura, id_provedor, id_producto, codigo_produc, nombre_produc, costo_producto, cantidad, total, descuento, iv', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_factura' => 'Id Factura',
			'id_provedor' => 'Id Provedor',
			'id_producto' => 'Id Producto',
			'codigo_produc' => 'Codigo Produc',
			'nombre_produc' => 'Nombre Produc',
			'costo_producto' => 'Costo Producto',
			'cantidad' => 'Cantidad',
			'total' => 'Total',
			'descuento' => 'Descuento',
			'iv' => 'Iv',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_factura',$this->id_factura);
		$criteria->compare('id_provedor',$this->id_provedor);
		$criteria->compare('id_producto',$this->id_producto);
		$criteria->compare('codigo_produc',$this->codigo_produc,true);
		$criteria->compare('nombre_produc',$this->nombre_produc,true);
		$criteria->compare('costo_producto',$this->costo_producto);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('total',$this->total);
		$criteria->compare('descuento',$this->descuento);
		$criteria->compare('iv',$this->iv);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TablaProductoCompranc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
