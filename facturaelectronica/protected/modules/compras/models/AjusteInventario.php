<?php

/**
 * This is the model class for table "ajuste_inventario".
 *
 * The followings are the available columns in table 'ajuste_inventario':
 * @property integer $id
 * @property integer $consecutivo
 * @property string $fecha
 * @property string $moneda
 * @property double $monto
 * @property string $accion
 * @property integer $st_inventario
 */
class AjusteInventario extends CActiveRecord
{
	    public $ivt;
     public $id_codigo_producto2;
      public $precio_aimpu;
       public $id_descuento;
        public $total;
           public $otros_impuestos_id;
          
         
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ajuste_inventario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('consecutivo, fecha, moneda, monto, accion', 'required'),
			array('consecutivo, st_inventario', 'numerical', 'integerOnly'=>true),
			//array('monto', 'numerical'),
			//array('fecha', 'length', 'max'=>14),
			array('moneda', 'length', 'max'=>10),
			array('accion', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, consecutivo, fecha, moneda, accion, st_inventario,motivo,tipo_cambio,total2,iv,subtotal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'consecutivo' => 'Consecutivo',
			'fecha' => 'Fecha',
			'moneda' => 'Moneda',
			'accion' => 'Accion',
			'st_inventario' => 'St Inventario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('consecutivo',$this->consecutivo);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('moneda',$this->moneda,true);
		$criteria->compare('accion',$this->accion,true);
		$criteria->compare('st_inventario',$this->st_inventario);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AjusteInventario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
