<?php

/**
 * This is the model class for table "recibos".
 *
 * The followings are the available columns in table 'recibos':
 * @property integer $id_recibo
 * @property string $fecha
 * @property integer $id_proveedor
 * @property string $observaciones
 * @property integer $id_moneda
 * @property string $saldo
 * @property string $abono
 */
class Recibos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'recibos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha, id_proveedor, observaciones, id_moneda ,abono,id_facturac', 'required'),
			
			array('id_proveedor, id_moneda', 'numerical', 'integerOnly'=>true),
			array('fecha', 'length', 'max'=>22),
			array('saldo, abono', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_recibo, fecha, id_proveedor, observaciones, id_moneda, saldo, abono', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_recibo' => 'Id Recibo',
			'fecha' => 'Fecha',
			'id_proveedor' => 'Id Proveedor',
			'observaciones' => 'Observaciones',
			'id_moneda' => 'Id Moneda',
			'saldo' => 'Saldo',
			'abono' => 'Abono',
			'id_facturac'=> 'Factura'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_recibo',$this->id_recibo);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('id_proveedor',$this->id_proveedor);
		$criteria->compare('observaciones',$this->observaciones,true);
		$criteria->compare('id_moneda',$this->id_moneda);
		$criteria->compare('saldo',$this->saldo,true);
		$criteria->compare('abono',$this->abono,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Recibos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
