<?php

/**
 * This is the model class for table "nota_credito".
 *
 * The followings are the available columns in table 'nota_credito':
 * @property string $id_nota
 * @property string $consecutivo
 * @property string $id_factura
 * @property integer $referencia
 * @property string $motivo
 * @property string $fecha_nota
 * @property string $cedula
 * @property string $nombre_cliente
 * @property integer $id_moneda
 * @property integer $cambio
 * @property string $subtotal
 * @property string $descuento
 * @property string $iv
 * @property string $otroimpuesto
 * @property string $total
 */ 
class NotaCredito extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $codigart;
	public $art;
	public $cant;
	public $costo;
	public $descu;
	public $tot;
	public $idpro;
	public $ivt;
	public $act;
	public $arrayidp;
	public $arraycodp;
	public $arrayart;
	public $arrayprecp;
	public $arraycantp;
	public $arraytotp;
	public $arraydesp;
	public $arrayivp;
	public $idpt;
	public $idpv;
	public $conse;
	public $mone;
	public function tableName()
	{
		return 'nota_credito';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cedula', 'required'),
			array('referencia, id_moneda', 'numerical', 'integerOnly'=>true),
			array('consecutivo, id_factura, cedula, subtotal, descuento, iv, otroimpuesto, total', 'length', 'max'=>20),
			array('motivo', 'length', 'max'=>200),
			array('fecha_nota', 'length', 'max'=>11),
			array('nombre_cliente', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_nota, consecutivo, id_factura, referencia, motivo, fecha_nota, cedula, nombre_cliente, id_moneda, cambio, subtotal, descuento, iv, otroimpuesto, total', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_nota' => 'Id Nota',
			'consecutivo' => 'Consecutivo',
			'id_factura' => 'Id Factura',
			'referencia' => 'Referencia',
			'motivo' => 'Motivo',
			'fecha_nota' => 'Fecha Nota',
			'cedula' => 'Cedula',
			'nombre_cliente' => 'Nombre Cliente',
			'id_moneda' => 'Moneda',
			'cambio' => 'Tipo Cambio',
			'subtotal' => 'Subtotal',
			'descuento' => 'Descuento',
			'iv' => 'I.V.',
			'otroimpuesto' => 'Otros Impuestos',
			'total' => 'Total',
			'art'=>'Artículo',
			'cant'=>'Cantidad',
			'costo'=>'Precio A.impu',
			'descu'=>'Descuento',
			'tot'=>'Total'

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_nota',$this->id_nota,true);
		$criteria->compare('consecutivo',$this->consecutivo,true);
		$criteria->compare('id_factura',$this->id_factura,true);
		$criteria->compare('referencia',$this->referencia);
		$criteria->compare('motivo',$this->motivo,true);
		$criteria->compare('fecha_nota',$this->fecha_nota,true);
		$criteria->compare('cedula',$this->cedula,true);
		$criteria->compare('nombre_cliente',$this->nombre_cliente,true);
		$criteria->compare('id_moneda',$this->id_moneda);
		$criteria->compare('cambio',$this->cambio);
		$criteria->compare('subtotal',$this->subtotal,true);
		$criteria->compare('descuento',$this->descuento,true);
		$criteria->compare('iv',$this->iv,true);
		$criteria->compare('otroimpuesto',$this->otroimpuesto,true);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('eliminado',0);

		$sort=new CSort();
             $sort->defaultOrder='id_nota DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>$sort,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NotaCredito the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
