<?php

/**
 * This is the model class for table "proveedores".
 *
 * The followings are the available columns in table 'proveedores':
 * @property integer $id_proveedor
 * @property integer $st_proveedor
 * @property integer $id_institucion
 * @property string $proveedores
 * @property string $telefono
 * @property string $fax
 * @property string $correo
 * @property string $apartado
 * @property string $web
 * @property string $correo2
 * @property string $pais
 * @property integer $id_provincia
 * @property integer $id_canton
 * @property integer $id_distrito
 * @property string $direccion
 * @property string $identificacion
 * @property integer $id_tipo_identificacioj
 * @property integer $area1
 * @property integer $area2
 * @property integer $canton
 */
class Proveedores extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proveedores';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_tipo_identificacioj,proveedores, telefono, correo, web, pais, id_provincia, id_canton, id_distrito, direccion, identificacion, area1', 'required'),
			array('st_proveedor, id_institucion, id_provincia, id_canton, id_distrito, id_tipo_identificacioj, area1, area2, canton', 'numerical', 'integerOnly'=>true),
			array('proveedores, pais', 'length', 'max'=>50),
			array('telefono, fax, apartado, identificacion', 'length', 'max'=>20),
			array('correo, web, correo2, direccion', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_proveedor, st_proveedor, id_institucion, proveedores, telefono, fax, correo, apartado, web, correo2, pais, id_provincia, id_canton, id_distrito, direccion, identificacion, id_tipo_identificacioj, area1, area2, canton', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_proveedor' => 'Id Proveedor',
			'st_proveedor' => 'St Proveedor',
			'id_institucion' => 'Id Institucion',
			'proveedores' => 'Proveedores',
			'telefono' => 'Telefono',
			'fax' => 'Fax',
			'correo' => 'Correo',
			'apartado' => 'Apartado',
			'web' => 'Web',
			'correo2' => 'Correo2',
			'pais' => 'Pais',
			'id_provincia' => 'Id Provincia',
			'id_canton' => 'Id Canton',
			'id_distrito' => 'Id Distrito',
			'direccion' => 'Direccion',
			'identificacion' => 'Identificacion',
			'id_tipo_identificacioj' => 'Id Tipo Identificacioj',
			'area1' => 'Area1',
			'area2' => 'Area2',
			'canton' => 'Canton',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;


        $tipo=$_GET['d'];
        if($tipo==1){
            $criteria->compare('st_proveedor',0);
        }else if(!$tipo){
           $criteria->compare('st_proveedor',1);
        }

     
        
      

        $criteria->addSearchCondition('identificacion',$this->id_proveedor,true, 'OR');
        $criteria->addSearchCondition('proveedores',$this->id_proveedor,true, 'OR');


         
         $sort=new CSort();
             $sort->defaultOrder='id_proveedor DESC';
              $sort->attributes=array('identificacion'=>array(
                                    'asc'=>'identificacion',
                                    'desc'=>'identificacion DESC',
                                ),
                                'id_proveedor'=>array(
                                    'asc'=>'id_proveedor',
                                    'desc'=>'id_proveedor DESC',
                                ),
                                'proveedores'=>array(
                                    'asc'=>'proveedores',
                                    'desc'=>'proveedores DESC',
                                ),
                                

                                
            );

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Proveedores the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
