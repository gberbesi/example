<?php
/* @var $this FacturaCompraController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Factura Compras',
);

$this->menu=array(
	array('label'=>'Create FacturaCompra', 'url'=>array('create')),
	array('label'=>'Manage FacturaCompra', 'url'=>array('admin')),
);
?>

<h1>Factura Compras</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
