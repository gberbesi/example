<?php
/* @var $this FacturaCompraController */
/* @var $data FacturaCompra */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_factura_compra')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_factura_compra), array('view', 'id'=>$data->id_factura_compra)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('factura_compra')); ?>:</b>
	<?php echo CHtml::encode($data->factura_compra); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_provedor')); ?>:</b>
	<?php echo CHtml::encode($data->id_provedor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('institucion_id')); ?>:</b>
	<?php echo CHtml::encode($data->institucion_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_moneda')); ?>:</b>
	<?php echo CHtml::encode($data->id_moneda); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_codigo_producto')); ?>:</b>
	<?php echo CHtml::encode($data->id_codigo_producto); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('dias')); ?>:</b>
	<?php echo CHtml::encode($data->dias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_descuento')); ?>:</b>
	<?php echo CHtml::encode($data->id_descuento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total')); ?>:</b>
	<?php echo CHtml::encode($data->total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iv')); ?>:</b>
	<?php echo CHtml::encode($data->iv); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('otros_impuestos_id')); ?>:</b>
	<?php echo CHtml::encode($data->otros_impuestos_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->observaciones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_cliente')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_cliente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_articulo')); ?>:</b>
	<?php echo CHtml::encode($data->id_articulo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precio_aimpu')); ?>:</b>
	<?php echo CHtml::encode($data->precio_aimpu); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subtotal')); ?>:</b>
	<?php echo CHtml::encode($data->subtotal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total2')); ?>:</b>
	<?php echo CHtml::encode($data->total2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descuento2')); ?>:</b>
	<?php echo CHtml::encode($data->descuento2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('credito')); ?>:</b>
	<?php echo CHtml::encode($data->credito); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('efec_monto')); ?>:</b>
	<?php echo CHtml::encode($data->efec_monto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tj_nm')); ?>:</b>
	<?php echo CHtml::encode($data->tj_nm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tj_ref')); ?>:</b>
	<?php echo CHtml::encode($data->tj_ref); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tj_monto')); ?>:</b>
	<?php echo CHtml::encode($data->tj_monto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ch_bnc')); ?>:</b>
	<?php echo CHtml::encode($data->ch_bnc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ch_ref')); ?>:</b>
	<?php echo CHtml::encode($data->ch_ref); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ch_monto')); ?>:</b>
	<?php echo CHtml::encode($data->ch_monto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trs_bnc')); ?>:</b>
	<?php echo CHtml::encode($data->trs_bnc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trs_ref')); ?>:</b>
	<?php echo CHtml::encode($data->trs_ref); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trs_monto')); ?>:</b>
	<?php echo CHtml::encode($data->trs_monto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ot_ref')); ?>:</b>
	<?php echo CHtml::encode($data->ot_ref); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ot_monto')); ?>:</b>
	<?php echo CHtml::encode($data->ot_monto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_payment')); ?>:</b>
	<?php echo CHtml::encode($data->total_payment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_completo')); ?>:</b>
	<?php echo CHtml::encode($data->total_completo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vuelto')); ?>:</b>
	<?php echo CHtml::encode($data->vuelto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contingencia')); ?>:</b>
	<?php echo CHtml::encode($data->contingencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipoexoneracion')); ?>:</b>
	<?php echo CHtml::encode($data->tipoexoneracion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('documentoexo')); ?>:</b>
	<?php echo CHtml::encode($data->documentoexo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('instituexo')); ?>:</b>
	<?php echo CHtml::encode($data->instituexo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaexo')); ?>:</b>
	<?php echo CHtml::encode($data->fechaexo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('impuestoexo')); ?>:</b>
	<?php echo CHtml::encode($data->impuestoexo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('porcentajeexo')); ?>:</b>
	<?php echo CHtml::encode($data->porcentajeexo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('n_conting')); ?>:</b>
	<?php echo CHtml::encode($data->n_conting); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_conting')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_conting); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('razon_cont')); ?>:</b>
	<?php echo CHtml::encode($data->razon_cont); ?>
	<br />

	*/ ?>

</div>