<?php
/* @var $this FacturaCompraController */
/* @var $model FacturaCompra */

$this->breadcrumbs=array(
	'Factura Compras'=>array('index'),
	$model->id_factura_compra=>array('view','id'=>$model->id_factura_compra),
	'Update',
);

$this->menu=array(
	array('label'=>'List FacturaCompra', 'url'=>array('index')),
	array('label'=>'Create FacturaCompra', 'url'=>array('create')),
	array('label'=>'View FacturaCompra', 'url'=>array('view', 'id'=>$model->id_factura_compra)),
	array('label'=>'Manage FacturaCompra', 'url'=>array('admin')),
);
?>

<h1>Update FacturaCompra <?php echo $model->id_factura_compra; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>