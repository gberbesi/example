<?php
/* @var $this ProveedoresController */
/* @var $model Proveedores */

$this->breadcrumbs=array(
	'Proveedores'=>array('index'),
	$model->id_proveedor=>array('view','id'=>$model->id_proveedor),
	'Update',
);

$this->menu=array(
	array('label'=>'List Proveedores', 'url'=>array('index')),
	array('label'=>'Create Proveedores', 'url'=>array('create')),
	array('label'=>'View Proveedores', 'url'=>array('view', 'id'=>$model->id_proveedor)),
	array('label'=>'Manage Proveedores', 'url'=>array('admin')),
);
?>



<?php $this->renderPartial('_form', array('model'=>$model,'estadoListData'=>$estadoListData)); ?>