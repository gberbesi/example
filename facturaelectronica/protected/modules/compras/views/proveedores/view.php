<?php
/* @var $this ProveedoresController */
/* @var $model Proveedores */

$this->breadcrumbs=array(
	'Proveedores'=>array('index'),
	$model->id_proveedor,
);

$this->menu=array(
	array('label'=>'List Proveedores', 'url'=>array('index')),
	array('label'=>'Create Proveedores', 'url'=>array('create')),
	array('label'=>'Update Proveedores', 'url'=>array('update', 'id'=>$model->id_proveedor)),
	array('label'=>'Delete Proveedores', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_proveedor),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Proveedores', 'url'=>array('admin')),
);
?>

<h1>View Proveedores #<?php echo $model->id_proveedor; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_proveedor',
		'st_proveedor',
		'id_institucion',
		'proveedores',
		'telefono',
		'fax',
		'correo',
		'apartado',
		'web',
		'correo2',
		'pais',
		'id_provincia',
		'id_canton',
		'id_distrito',
		'direccion',
		'identificacion',
		'id_tipo_identificacioj',
		'area1',
		'area2',
		'canton',
	),
)); ?>
