<?php
/* @var $this ProveedoresController */
/* @var $data Proveedores */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_proveedor')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_proveedor), array('view', 'id'=>$data->id_proveedor)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('st_proveedor')); ?>:</b>
	<?php echo CHtml::encode($data->st_proveedor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_institucion')); ?>:</b>
	<?php echo CHtml::encode($data->id_institucion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('proveedores')); ?>:</b>
	<?php echo CHtml::encode($data->proveedores); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono')); ?>:</b>
	<?php echo CHtml::encode($data->telefono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fax')); ?>:</b>
	<?php echo CHtml::encode($data->fax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('correo')); ?>:</b>
	<?php echo CHtml::encode($data->correo); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('apartado')); ?>:</b>
	<?php echo CHtml::encode($data->apartado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('web')); ?>:</b>
	<?php echo CHtml::encode($data->web); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('correo2')); ?>:</b>
	<?php echo CHtml::encode($data->correo2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pais')); ?>:</b>
	<?php echo CHtml::encode($data->pais); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_provincia')); ?>:</b>
	<?php echo CHtml::encode($data->id_provincia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_canton')); ?>:</b>
	<?php echo CHtml::encode($data->id_canton); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_distrito')); ?>:</b>
	<?php echo CHtml::encode($data->id_distrito); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('identificacion')); ?>:</b>
	<?php echo CHtml::encode($data->identificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_tipo_identificacioj')); ?>:</b>
	<?php echo CHtml::encode($data->id_tipo_identificacioj); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('area1')); ?>:</b>
	<?php echo CHtml::encode($data->area1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('area2')); ?>:</b>
	<?php echo CHtml::encode($data->area2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('canton')); ?>:</b>
	<?php echo CHtml::encode($data->canton); ?>
	<br />

	*/ ?>

</div>