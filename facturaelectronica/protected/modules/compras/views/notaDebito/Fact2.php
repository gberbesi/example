<?php 
      $fact=Yii::app()->request->requestUri;
      $server=$_SERVER['HTTP_HOST'];
      $url=$server.$fact;
      $moneda=Moneda::model()->find('id_moneda='.$nct->id_moneda);
      $anuladondbt=$nct->id_ncdt;
      $header=
         '<div colspan="12" style="background: #e1e1e1;  height:20px;"></div>
         <table style="border:1px solid #eeeeee;">
         <tr> 
         <td rowspan="5" width="300px"> <img src="images/fotos/'.$contribuyente->foto.'.png"/ width="200px"></td> 
         <td style="font-size:20px; margin-top:-20px; padding-left:200px;width:500px"> Nota Débito</td><br>
         </tr>
         <tr>               
         <td style="font-size:14px; padding-left:200px;"> '.$contribuyente->nombre.' </td><br>
         </tr>
         <tr>               
         <td style="font-size:14px; padding-left:200px;">'.$provincia->provincia.' '.$canton->canton.' '.$distrito->distrito.'</td><br>
         </tr>
         <tr>           
         <td style="font-size:14px; padding-left:200px;">Telf: '.$contribuyente->telefono.' </td><br>
         </tr>           
         <tr>              
         <td style="font-size:14px; padding-left:200px;">Correo: '.$contribuyente->correo.'</td><br>
         </tr>
         </table>';
    
      $html='
      <br><br><br><br><br>
      <table style="border:1px solid #e1e1e1; border-collapse: collapse;">';
    
      $html.='
              <tr style="background: #e1e1e1;">
      		    <td width="550px"><b style="font-size:18px;">Nota de Débito</b></td>      		    
      		    <td width="200px" align="right"><b>Nº: '.$nct->id_nota.'</b></td>
              </tr>';

      $html.='
      		   <tr>
             <td> <b style="font-size:14px;">Nombre:</b>'.$cliente->proveedores.'</td>      		    
      		   <td> <b style="font-size:14px;">Fecha:</b>'.$nct->fecha_nota.'</td> 
             </tr>
          ';
      		if($cliente->identificacion!=0){
      		$html.='<tr>
          		    <td > <b style="font-size:14px;">Cédula: </b>'.$cliente->identificacion.'</td>';
                  if(!$anuladondbt){
                  $html.='<td > <b style="font-size:14px;">Ref. Factura: </b>'.$nct->id_factura.'</td>';
                }else{
                   $html.='<td > <b style="font-size:14px;">Ref. Nota Crédito: </b>'.$nct->id_ncdt.'</td>';
                }
          		    $html.='</tr><br>
          		    <tr>
          		    <td colspan="2"> <b style="font-size:14px;">Teléfono:</b>'.$cliente->telefono.'</td>
          		    </tr><br>
          		    <tr>
          		    <td > <b style="font-size:14px;">Dirección:</b>'.$cliente->direccion.'</td>
          		    <td > <b style="font-size:14px;">Moneda:</b>'.$moneda->aka.'</td>
          		   
          		    </tr>';
          }
        $html.='</tr>
        </table>
        <br>
        <table style="border-collapse: collapse; border:1px solid #e1e1e1;" >
          <tr style=" background: #e1e1e1; ">
      		    <td width="60%" align="right"> <b style="font-size:14px;">Código</b></td>
      		    <td width="60%" align="right"> <b style="font-size:14px;">Descripción</b></td>
      		    <td width="60%" align="right"> <b style="font-size:14px;">Unid.Medida</b></td>
      		    <td width="60%" align="right"> <b style="font-size:14px;">Cantidad</b></td>
         	    <td width="60%" align="right"> <b style="font-size:14px;">PrecioUni. </b></td>
      		    <td width="20%" align="right"> <b style="font-size:14px;">Descuento</b></td>
       		    <td width="20%" align="right"> <b style="font-size:14px;">I.V.</b></td>
      		    <td width="20%" align="right"> <b style="font-size:14px;">OtroImp.</b></td>
      		    <td width="60%" align="right"> <b style="font-size:14px;">TOTAL </b></td>
      		</tr>';

      		$totalf=0;
          $iva=0;
          $subt=0;
          $desc=0;
           foreach ($tpc as $key => $value) {
    
        $codigo=((int)$tpc[$key]->codigo_produc);

        $prod=Productos::model()->find('codigo='.$codigo.'');
        $uni=UnidadMedida::model()->find('id_um='.$prod->unidad_medida_id.'');
        
        $html.='<tr > <td width="60%" align="right" > <b style="font-size:14px;">'.$value->codigo_produc.'</td>';
        $html.='<td width="60%" align="right"> <b style="font-size:14px;">'.$prod->descripcion.'</td>'; 
        $html.='<td width="60%" align="right"> <b style="font-size:14px;">'.$uni->um.'</td>';
        $html.='</b><td width="20%" align="right"> <b style="font-size:14px;">'.$value->cantidad.'</td>';     
        $html.='</b><td width="60%" align="right"> <b style="font-size:14px;">'.$value->costo_producto.'</td>';
        $html.='</b><td width="20%" align="right"> <b style="font-size:14px;">'.$value->descuento.'</td>';
        $html.='</b><td width="60%" align="right"> <b style="font-size:14px;">'.$value->iv.'</td>';
        $html.='</b><td width="60%" align="right"> <b style="font-size:14px;">0</td>';
        $html.='</b><td width="60%" align="right"> <b style="font-size:14px;">'.$value->total.'</td>';
        $html.='</b></tr>';
                $totalf+=$value->total;
                $iv=(($value->total*$value->iv)/100);
                $iva+=$iv;
                $subt+=$value->costo_producto*$value->cantidad;
                $desc=$value->descuento;
                $descu+=$desc;               
        }
        $totalfin+=$totalf+$iva;
      	$html.='      		
        </table>
        <p><b style="font-size:14px;">Motivo de la Nota de Débito:</b>  <br>'.$nct->motivo.'</p>
        <br><br>
        <table style="margin-left:550px;">
            <tr align="left">
      		    <td  colspan="2"> <b style="font-size:14px;">Sub total:  </b></td>
      	      <td>'.$subt.'</td>
      	    </tr>
      	    <tr>
        	      <td colspan="2" > <b style="font-size:14px;">Descuento:  </b></td>
       		      <td>'.$descu.'</td>
       		  </tr>
      	    <tr>
         	  <td colspan="2"> <b style="font-size:14px;">I.V:  </b></td>
				<td>'.$iva.'</td>
				</tr>
      	<tr>
      	   	<td colspan="2"> <b style="font-size:14px;">Otros Impu:  </b></td><br>
      			<td> 0</td>
      	</tr>
      	<tr>
      	 	<td colspan="2"> <b style="font-size:14px;"><b>Total:  </b></td>
      	    <td>'.$totalfin.'</td>      	    
		    </tr>   	 
        </table>
      
	     <div style="margin-left:520px;"><br><br><barcode code="'.$url.'" type="QR" class="barcode" size="1.2" error="M" /></div>
      
  ';
 
 

?>
<?php 
	if($vista == 1)
	{
		echo '<div class="form">';
		?>
<div class="printable">
		 		<?php echo $header.$html.$footer; ?>
		 	</div>
		 	<!-- Cierre de la clase para imprimir  -->
		
		<div style ="align:center; position:fixed;bottom:0;">
        <table>
              <tr>
              	<td>
	
	
		<?php 
						$this->widget('zii.widgets.jui.CJuiButton', array(
		                            'cssFile'=> Yii::app()->request->baseUrl.'/css/jquery.css',
					    			'buttonType'=>'button',
		                            'name'=>'Imprimir',
								    'caption'=>'Imprimir',
								    'onclick'=>'js:function(){$(".printable").print();}',
									
		
					));?>
					</td>
	
			</tr>
		</table>
        </div>
        <?php 
        echo '</div>';
	}
	else
	{
		
		$pdf = Yii::createComponent('application.extensions.mpdf53.mpdf');
		//$mpdf=new mPDF('c',array(100,200),10,10,10,22,15,9);
		$mpdf=new mPDF('c','A4','',10,15,10,30,15,9);
		$mpdf->SetHTMLHeader($header);
		$mpdf->SetHTMLFooter($footer);
    $mpdf->SetTitle('NotaDebito N#'.$nct->id_nota);
    $mpdf->SetFooter(' {DATE j/m/Y}|Pg{PAGENO}/{nbpg}|'.$cliente->proveedores.'');
    $mpdf->WriteHTML($html);
    $mpdf->Output('NotaDebito#'.$nct->id_nota.'.pdf','I');
		exit;
	}
	
?>