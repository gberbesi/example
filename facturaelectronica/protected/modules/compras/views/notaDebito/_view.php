<?php
/* @var $this NotaDebitoController */
/* @var $data NotaDebito */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_nota')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_nota), array('view', 'id'=>$data->id_nota)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('consecutivo')); ?>:</b>
	<?php echo CHtml::encode($data->consecutivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_factura')); ?>:</b>
	<?php echo CHtml::encode($data->id_factura); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('referencia')); ?>:</b>
	<?php echo CHtml::encode($data->referencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('motivo')); ?>:</b>
	<?php echo CHtml::encode($data->motivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_nota')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_nota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula')); ?>:</b>
	<?php echo CHtml::encode($data->cedula); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_cliente')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_cliente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_moneda')); ?>:</b>
	<?php echo CHtml::encode($data->id_moneda); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cambio')); ?>:</b>
	<?php echo CHtml::encode($data->cambio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subtotal')); ?>:</b>
	<?php echo CHtml::encode($data->subtotal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descuento')); ?>:</b>
	<?php echo CHtml::encode($data->descuento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iv')); ?>:</b>
	<?php echo CHtml::encode($data->iv); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('otroimpuesto')); ?>:</b>
	<?php echo CHtml::encode($data->otroimpuesto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total')); ?>:</b>
	<?php echo CHtml::encode($data->total); ?>
	<br />

	*/ ?>

</div>