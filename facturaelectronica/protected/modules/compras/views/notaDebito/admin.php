  <h1 style="color:#2a3f54;"> &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-file"></span><span style="color:black;"> Nota Débito Compra</span> </h1>
<nav class="breadcrumb" style="background-color:white; padding-left: 10px;margin-left: 30px; font-size: 14px;">
  <a class="breadcrumb-item" href="#" style="color:black  "><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;<span style="color:#22C7FC" >Inicio</span> </a><a>/</a>
  <span class="breadcrumb-item active" style="color:black"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Notas Débito Compra</span>  </span>
</nav>

<?php
/* @var $this NotaDebitoController */
/* @var $model NotaDebito */


Yii::app()->clientScript->registerScript('search99', "

$('.search99-form form').submit(function(){
	$('#nota-debito-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



<div class="search11-form col-md-12">
    <?php $this->renderPartial('_search',array('model'=>$model,)); ?>
 </div>	
</div><!-- search-form -->
<div class="table table-striped table-bordered table-condensed table-responsive" cellspacing="0" cellpadding="3" rules="all" style="background-color:none;border-color:#CCCCCC;border-width:1px;border-style:None;border-collapse:collapse; font-size: 14px;">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'nota-debito-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'itemsCssClass'=>'table table-striped table-responsive  text-center',
                        'pagerCssClass'=>'pagination pull-right',
                        'afterAjaxUpdate'=>'function(){$.getScript("themes/classic/plugins/archivos_js/campo-numero.js");$.getScript("themes/classic/plugins/archivos_js/campo-letra.js");
                                $.getScript("themes/classic/plugins/archivos_js/script_select2.js");}',
		'columns'=>array(
		array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">Id</p>',
 'name'=>'id_nota',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  'value'=>'$data->id_nota',
    ),
		array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">Doc Ref.</p>',
 'name'=>'docred',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  'value'=>'$data->docref',
    ),
		array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">Num Ref.</p>',
 'name'=>'numref',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  'value'=>'$data->id_factura',
    ),	
		array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">Consecutivo</p>',
 'name'=>'consecutivo',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  'value'=>'$data->consecutivo',
    ),
		array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">Identificación</p>',
 'name'=>'id_factura',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  'value'=>'$data->cedula',
    ),
		array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">Proveedor</p>',
 'name'=>'referencia',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  'value'=>'$data->nombre_cliente',
    ),	
		array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">Motivo</p>',
 'name'=>'motivo',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  'value'=>'$data->motivo',
    ),
		array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">Fecha</p>',
 'name'=>'fecha_nota',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  'value'=>'$data->fecha_nota',
    ),
		array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">Monto</p>',
 'name'=>'total',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  'value'=>'$data->total',
    ),
	array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">Anulado</p>',
 'name'=>'anulado',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  'value'=>'($data->anulado=="0")?"NO":"SI"',
    ),


 array(
                                'header'=>"",
                                'htmlOptions'=>array('class'=>'col-lg-1 text-center'),
                                'class'=>'CButtonColumn',
                                'template' => '{borrar}',
                                'buttons'=>array(

                                                 'borrar' => array(
                                 'label'=>'',
                               'visible'=>''.!$tipo.'',
                              'options'=>array('class'=>'fa fa-times eliminar','style'=>'margin-left:10px;color:red;font-size:20px;', 'data-toggle'=>"tooltip",'title'=>'Eliminar',),
                                'imageUrl'=>false,
                                'url'=>'Yii::app()->createUrl("compras/notaDebito/delete", array("id" => $data->id_nota))',
                               // 'click'=>'function(){return confirm("¿Desea Eliminar Este Tipo de Producto?");}',
                                ),
                                                              
    )),
  array(
                                          'header'=>"",
                                                    'htmlOptions'=>array('class'=>'col-md-1 col-lg-1 text-center','style'=>'color:red;'),
                                         'value'=>'"<a data-toggle=\"tooltip\" onclick=\"selectedAlumno3(".$data->id_nota.")\" title=\"Opciones Notas\" href=\"#\"><i class=\" glyphicon glyphicon-download-alt\" style=\"color:black;font-size:20px; \"></i></a>"',
                                                    'type'=>'raw',
                                         ),

		
	),
)); ?>
</div>
<div class="modal fade" id="impFac"  role="dialog" style="margin-top: 10%">
    <div class="modal-dialog" id="forma" >
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Opciones Notas</h4>
        </div>
        <div class="modal-body" id="listo">
         
            
            </div>
            
<br><br><br><br><br><br><br><br>

             <div class="modal-footer">
          <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
       
      </div>
      
    </div>
    <div class="modal fade" id="anular"  role="dialog" style="margin-top: 10%">
    <div class="modal-dialog" >
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Datos Anulación</h4>
        </div>
        <div class="modal-body col-md-12" id="cont">
         <?php $form=$this->beginWidget('CActiveForm', array('id'=>'nota-debito-form','enableAjaxValidation'=>false,)); ?>
         <div class="col col-md-6">
         	<?php echo $form->labelEx($model,'consecutivo');?>
            <?php echo $form->textField($model,'consecutivo',array('type'=>'text','id'=>'consecutivo','class'=>'form-control','required'=>true)); ?></div>
                <div class="col col-md-12">
         	<?php echo $form->labelEx($model,'motivo');?>
            <?php echo $form->textArea($model,'motivo',array('type'=>'text','id'=>'motivo','class'=>'form-control')); ?></div>
                <div style="display:none">
         	<?php echo $form->labelEx($model,'id_nota');?>
            <?php echo $form->textArea($model,'id_nota',array('type'=>'text','id'=>'nota','class'=>'form-control')); ?></div>
           
            </div>
             <br><br><br><br><br><br><br><br><br><br><br>

            <?php $this->endWidget()?>
            


             <div class="modal-footer">
             	<button type="button" class="btn btn-dark" onclick="anulado()">Guardar</button>
          		<button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
       		 </div>
        </div>
       
      </div>
      
    </div>
  
  


<script>
function selectedAlumno3($id){
 $('#impFac').modal();
 
var ruta = <?php  echo CJSON::encode( $ruta)?>;
var vista = <?php  echo CJSON::encode( $vista)?>;
alert($id);

 
var html='<a href="index.php?r=compras/notaDebito/'+ruta+' " target="_blank"><button type="button" class="btn btn-dark col-md-8 col-xs-12 col-md-offset-2 "  > Ver Nota</button></a><a href="index.php?r=compras/notaDebito/'+vista+'"><button type="button" class="btn btn-dark col-md-8 col-xs-12 col-md-offset-2 ">Imprimir Nota</button></a><button type="button" onclick="anulacion('+$id+')" class="btn btn-dark col-md-8 col-xs-12 col-md-offset-2 ">Anular con Nota de Crédito</button></a><br>';
 $('#listo').html(html); 
  $.ajax({ 
      method: "POST", 
      //dataType: "json",//trae arry
      dataType: "html",//trae html
      url: "<?php echo CController::createUrl('/compras/notaDebito/Reportid') ?>",
      //async: false,

      data: {id:$id}//valosr q pasa por post para la consulta 
    })
    .done(function(msg) {
  
});

} 
function anulado(){
	
	  var id =$('#nota').val();
  var cons = $("#consecutivo").val();
  var moti = $("#motivo").val();
  if(cons==''){
    swal("Atención","Consecutivo es requerido","warning");

    event.preventDefault();
  }else if(moti==''){
    swal("Atención","Motivo es requerido","warning");

    event.preventDefault();
  }
  else if (cons!='' && moti!=''){
         $.ajax({ 
      method: "POST", 
      dataType: "json",
      url: "<?php echo CController::createUrl('/compras/notaDebito/Anulacion') ?>",
     data: {id_nota: id,consecutivo: cons, motivo: moti}
    })
    .done(function(msg) {
       $('#anular').modal('hide');
    swal("Atención","Nota Débito "+id+" ha sido anulada","success");
    $('#nota').val('');
     $("#consecutivo").val('');
     $("#motivo").val('');
     jQuery('#nota-debito-grid').yiiGridView('update');

    });
  // location.href='index.php?r=compras/notaCredito/admin';
  }
}

function selectedAlumno5($id){

  location.href='index.php?r=facturas/factura/Update&id='+$id;
}
function anulacion($id){

  var id =$id;
 $.ajax({ 
      method: "POST", 
      dataType: "json",
      url: "<?php echo CController::createUrl('/compras/notaDebito/Anular') ?>",
     data: {id_nota: id}
    })
    .done(function(msg) {
      console.log(msg.csc);
            if(msg.csc==0){
        swal("Atención","Esta nota de Crédito Ya se encuenta anulada","error");
      }else{
        $('#anular').modal();
        $('#impFac').modal('hide');
        $('#nota').val($id);
      }
    });
   
   
}

jQuery(function($) {

jQuery(document).on('click','.eliminar',function(event) {
    var th = this;
event.preventDefault(); 
    swal({
      title: "¿Eliminar?",
      text: "¿Está seguro que desea borrar este elemento?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        jQuery('#nota-debito-grid').yiiGridView('update', {
            type: 'POST',
            dataType: "json",
            url: jQuery(th).attr('href'),
            success: function(msg) {
              console.log(msg.conse);


                if(msg.con!=0) {
                   jQuery('#nota-debito-grid').yiiGridView('update');

                    swal("Borrado exitoso", {
                      icon: "success",
                    }); 
                } else {
                    swal("Atención!","Este registro no puede ser eliminado porque se encuentra asociado a una nota de crédito con consecutivo "+msg.conse+" </b>", {
                      icon: "error",
                    }); 
                }              
            },
            error: function(XHR) {
            }
        });        
      } else {
        //swal("Your imaginary file is safe!");
      }
    });    
});
});
</script>