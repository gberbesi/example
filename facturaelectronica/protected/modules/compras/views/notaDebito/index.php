<?php
/* @var $this NotaCreditoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Nota Debito',
);

$this->menu=array(
	array('label'=>'Create NotaDebito', 'url'=>array('create')),
	array('label'=>'Manage NotaDebito', 'url'=>array('admin')),
);
?>

<h1>Nota Debito</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
