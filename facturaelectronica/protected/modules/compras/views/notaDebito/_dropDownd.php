<?php
	$atributoAtt = preg_replace('/\[\w+\]/', '', $atributo);
?>
<div class="form-group <?php if(isset($_POST[get_class($model)])) echo $model->hasErrors($atributoAtt)?'has-error':'has-success'; ?>">
	<?php echo $form->labelEx($model,$atributo); ?>
	<?php echo $form->dropDownList($model,$atributo,$list,array('empty'=>'--SELECCIONE--','placeholder'=>'--SELECCIONE--','class'=>'form-control','style'=>'width:100%','disabled'=>true,'onchange'=>$onchange)); ?>
	<?php echo $form->error($model,$atributo,array('class'=>'help-block text-center')); ?>
</div>