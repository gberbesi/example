<?php 
/* @var $this FacturaController */ 
/* @var $model Factura */
/* @var $form CActiveForm */
 date_default_timezone_set('America/Costa_Rica');  

?>

<script src="js/sweetalert.min.js"></script>
    
     <h1 style="color:#2a3f54;"> &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-file"></span><span style="color:black;"> Nota Débito Compra</span> </h1>


<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
   'id'=>'nota-debito-form',
   'enableAjaxValidation'=>false, 
)); ?>
 
<nav class="breadcrumb" style="background-color:white; padding-left: 10px;margin-left: 30px; font-size: 14px;">
  <a class="breadcrumb-item" href="#" style="color:black  "><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;<span style="color:#22C7FC" >Inicio</span> </a><a>/</a>
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('compras/notaDebito/admin');?>" style="color:black"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Notas Débito Compra</span> </a><a>/</a>
  
  <span class="breadcrumb-item active" style="color:black"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Registro</span>  </span>
</nav>

 <div class="col-md-3" style="margin-right: 80%">
      
        
        <?php echo $this->renderPartial('_nfield',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'consecutivo',
        'id'=>'consecutivo',
        
      ),true);?>      
    </div>


	<div class="col-md-3  col-sm-6 col-xs-12 ">
               <label for="fullname">Buscar Factura*</label>
          <div class="input-group">
                            
                           <?php echo $form->textField($model,'id_factura',array('type'=>'text','id'=>'id_factura','class'=>'form-control')); ?>
                             <?php echo $form->error($model,'id_factura',array('class'=>'btn-xs alert-danger text-center')); ?>
                             <span class="input-group-btn">
                                              <button type="button" class="btn btn-primary" data-toggle="modal" onclick="tipobusqueda()"><span class="fa fa-search-plus"></span></button>
                                          </span>

<?php echo $form->textField($model,'id_factura',array('type'=>'text','id'=>'id_factura2','class'=>'form-control','placeholder'=>"",'readOnly'=>true,'style'=>'display:none')); ?>
                             <?php echo $form->error($model,'id_factura',array('class'=>'btn-xs alert-danger text-center')); ?>
                             
                                             
                                          

                          </div>
                          </div>
               <div class="col-md-3">
			
				<?php $tiporef = array('1'=>'Corrige Monto','0'=>'Otros')?>
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'referencia',
				'list'=>$tiporef,),true);?>			
		</div>
		<div class="col-md-3">
			
				
				<?php echo $this->renderPartial('_field',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'motivo',
      ),true);?>			
		</div>
		<div class="col-md-3">
			
				
				<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'fecha_nota',
				'value'=>date('j/n/Y')),true);?>			
		</div>
			<div class="col-md-3">
			
				
				<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'cedula',
				'id'=>'cedula'),true);?>			
		</div>
			<div class="col-md-3">
			
				
				<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'nombre_cliente',
				'id'=>'nombre'),true);?>			
		</div>
      <div>
      
        
        <?php echo $this->renderPartial('_fieldh',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'idpv',
        'id'=>'idpv'),true);?>      
    </div>
			<div class="col-md-3">
			<label>Moneda</label>
		
                  <select class="form-control select2" style="width:100%" id="moneda" name="NotaDebito[moneda]" disabled >
					<option value="1" id="cl">Colones (Costa Rica)</option>
					<option value="2" id="dl">Dólares (Estados Unidos)</option>
					</select>	
		</div>
      <div class="col-md-3">
      
        
        <?php echo $this->renderPartial('_field',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'mone'      
        ),true);?>      
    </div>
		<div class="col-md-3">
			
				
				<?php echo $this->renderPartial('_field',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'cambio'      
        ),true);?>			
		</div>
		<div class="col-md-3  col-sm-6 col-xs-12 ">
               <label for="fullname">Código de Artículo*</label>
          <div class="input-group">
                            
                           <?php echo $form->textField($model,'codigart',array('type'=>'text','id'=>'codigart','class'=>'form-control')); ?>
                             <?php echo $form->error($model,'codigart',array('class'=>'btn-xs alert-danger text-center')); ?>
                             <span class="input-group-btn">
                                              <button type="button" class="btn btn-primary" data-toggle="modal" onclick="prod()"><span class="fa fa-search-plus"></span></button>
                                          </span>

			<?php echo $form->textField($model,'codigart',array('type'=>'text','id'=>'idpro','class'=>'form-control','placeholder'=>"",'readOnly'=>true,'style'=>'display:none')); ?>
                             <?php echo $form->error($model,'codigart',array('class'=>'btn-xs alert-danger text-center')); ?>
                             
                                             
                                          

                          </div>
                          </div>
        <div class="col-md-3">
			
				
				<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'art',),true);?>			
		</div>
		<div class="col-md-1">
			
				
				<?php echo $this->renderPartial('_nfield',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'cant',
        'on'=>'sumar()',
				'value'=>1),true);?>			
		</div>
		<div class="col-md-2">
			
				
				<?php echo $this->renderPartial('_field',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'costo',
        'on'=>'calcula()',
      ),true);?>			
		</div>
		<div class="col-md-3">
			<?php $descu=CHtml::listData(Descuento::model()->findAll(),'porcentaje', 'descuento') ?>
				
				<?php echo $this->renderPartial('_dropDown',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'descu',
        'on'=>'calcular()',
				'list'=>$descu),true);?>			
		</div>
		<div class="col-md-2">
			
				
				<?php echo $this->renderPartial('_fieldReadOnly',array(
				'model'=>$model,
				'form'=>$form,											
				'atributo'=>'tot',),true);?>			
		</div>
	

	 <div class="col-md-3 offset-md-3  ">
    <label for="fullname">Actualiza*</label>
      <div class="input-group">
      <select class="form-control select2" style="width:100%" id="actualiza" name="NotaDebito[act]" >
          <option value="1" id="noacc">Sin Acción</option>
          <option value="2" id="nosal">No sale del Inventario</option>
          <option value="3" id="sale">Sale del Inventario</option>
          </select> 
 
                <?php echo $form->error($model,'actualiza',array('class'=>'btn-xs alert-danger text-center')); ?>
               <span class="input-group-btn">
                    <button type="button" class="btn btn-success" onClick="añadirPro()"><span class="fa fa-plus"></span></button>
                </span>
          </div><br>
          </div>
            <div >       
        <?php echo $this->renderPartial('_fieldh',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'ivt',
        'id'=>'ivt'),true);?>      
    </div>
          <div class="col-md-12 col-sm-12 col-xs-12  ">
      <div class="table table-responsive" id="productotal">
<table class="table table-striped table-responsive">
<tr style="color:#22C7FC;background-color: rgb(238, 238, 238); font-weight: bold">
  <td>id</td>
  <td>Código</td>
  <td>Artículo</td>
  <td>Cantidad</td>
  <td>Precio Antes </td>
  <td>Descuento</td>
  <td>Total</td>
  <td style="display:none"></td>
  <td style="display:none"></td>
  <td>Actualiza Inventario</td>
  <td></td>
  <td></td>
</tr>
 
</table> 
    </div>
  </div><br>
  <div class="col-md-2 col-md-offset-2">
      
        
        <?php echo $this->renderPartial('_fieldReadOnly',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'subtotal',
        'value'=>0,
        'id'=>'subtotal'),true);?>      
    </div>
      <div class="col-md-2">        
        <?php echo $this->renderPartial('_fieldReadOnly',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'descuento',
        'id'=>'descuento',
        'value'=>0),true);?>      
    </div>
<div class="col-md-2">        
        <?php echo $this->renderPartial('_fieldReadOnly',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'iv',
        'value'=>0,
        'id'=>'iv'),true);?>      
    </div>
    <div class="col-md-2">        
        <?php echo $this->renderPartial('_fieldReadOnly',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'otroimpuesto',
        'value'=>0,
        'id'=>'otroimpuesto'),true);?>      
    </div>
    <div class="col-md-2">       
        <?php echo $this->renderPartial('_fieldReadOnly',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'total',
        'value'=>0,
        'id'=>'total'),true);?>      
    </div>
     <div>       
        <?php echo $this->renderPartial('_fieldh',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'arrayidp',
        'value'=>0,
        'id'=>'arrayidp'),true);?>      
    </div>
     <div>       
        <?php echo $this->renderPartial('_fieldh',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'idpro',
        'value'=>0,
        'id'=>'idpro'),true);?>      
    </div>
	<div>       
        <?php echo $this->renderPartial('_fieldh',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'arraycodp',
        'value'=>0,
        'id'=>'arraycodp'),true);?>      
    </div>
    <div>       
        <?php echo $this->renderPartial('_fieldh',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'arrayart',
        'value'=>0,
        'id'=>'arrayart'),true);?>      
    </div>
    <div>       
        <?php echo $this->renderPartial('_fieldh',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'arrayprecp',
        'value'=>0,
        'id'=>'arrayprecp'),true);?>      
    </div>
    <div>       
        <?php echo $this->renderPartial('_fieldh',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'arraycantp',
        'value'=>0,
        'id'=>'arraycantp'),true);?>      
    </div>
    <div>       
        <?php echo $this->renderPartial('_fieldh',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'arraytotp',
        'value'=>0,
        'id'=>'arraytotp'),true);?>      
    </div>
       <div>       
        <?php echo $this->renderPartial('_fieldh',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'arraydesp',
        'value'=>0,
        'id'=>'arraydesp'),true);?>      
    </div>
       <div>       
        <?php echo $this->renderPartial('_fieldh',array(
        'model'=>$model,
        'form'=>$form,                      
        'atributo'=>'arrayivp',
        'value'=>0,
        'id'=>'arrayivp'),true);?>      
    </div>
	<div class="col-md-3 col-md-offset-9 ">
    <br>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Facturar' : 'Actualizar',array('class'=>'btn btn-primary btn-lg btn-block')); ?>
	</div>

<?php $this->endWidget(); ?>

</div>
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
     
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
           <h2 class="modal-title" id="exampleModal2Label" style="color:#22C7FC; font-weight:bold;">Añadir Cliente</h2>
      </div>
      
      <div class="modal-body">
<div class="table table-responsive table-striped">
 <?php $this->renderpartial('application.modules.compras.views.facturaCompra.admin2',array('model'=>$fc)); ?>
    
  
    

</div>

<div class="modal-footer">
        <button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button>
      </div>
</div>

                  </div><!-- /.box-body -->
            </div>
</div>
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
         <h2 class="modal-title" id="exampleModal3Label" style='color:#22C7FC;font-weight:bold'>Agregar Producto</h2>
      </div>
      
      <div class="modal-body">
<div class="table table-responsive table-striped">
    <?php $this->renderPartial('application.modules.registro.views.productos.admin2',array('model'=>$pdc)); ?>
    
</div>
<div class="modal-footer">
        <button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button>
      </div>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->

</div>
</div>

<!-- form -->
<script>

  $("#nota-debito-form").submit(function( event ) {
    
    var t = $("#total").val();
    var motivo = $("#NotaDebito_motivo").val();
    var cedula =$("#cedula").val();
    var conse = $("#consecutivo").val();
    var f = $("#id_factura2").val();  
    var rsp; 
    $.ajax({ 
      method: "POST", 
      dataType: "json",
      url: "<?php echo CController::createUrl('/compras/notaDebito/BuscarCst') ?>",
     data: {consecutivo: conse,ci:cedula},
    })
    .done(function(msg) {  
      if(msg.csc==0){
          $("#consecutivo").val('');
          swal("Atención","Este Consecutivo ya Existe","error");
      }
    });
  
    if(t =='0'){
      swal("Atención","No se puede generar una N.C con un Monto total en 0","error");
      event.preventDefault();
    }
    if(motivo ==''){
      swal("Atención","No se puede generar una N.C. sin Motivo","error");
      event.preventDefault();
    }
    if(cedula==''){
      swal("Atención","No se puede generar una N.C. sin Seleccionar Una Factura","error");
      event.preventDefault();
    }
    if(conse == '' || conse ==0){
      if(conse==''){
    swal("Atención","No se puede generar una N.C. sin Consecutivo ","error");
      event.preventDefault();}
      if($("#consecutivo").val()){
    swal("Atención","igual consecutivo","error");
      event.preventDefault();}

    }
     if(f == ''){
    swal("Atención","No se puede generar una N.C. sin Seleccionar Una Factura","error");
      event.preventDefault();
    }else if ( t!='' && motivo!='' && cedula!='' && conse!='' && f!=''){
      var tabla= $('#productotal table tr');
      var vuelta = tabla.length;
    for (i=1;i<vuelta;i++){
      var id= $('#productotal table tr').eq(''+i+'').find('td').eq(0).text();
      var codigo= $('#productotal table tr').eq(''+i+'').find('td').eq(1).text();
      var articulo= $('#productotal table tr').eq(''+i+'').find('td').eq(2).text();
      var cantidad= $('#productotal table tr').eq(''+i+'').find('td').eq(3).text();
      var precio= $('#productotal table tr').eq(''+i+'').find('td').eq(4).text();
      var descuento= $('#productotal table tr').eq(''+i+'').find('td').eq(8).text();
      var total= $('#productotal table tr').eq(''+i+'').find('td').eq(6).text();
      var iv= $('#productotal table tr').eq(''+i+'').find('td').eq(9).text();
      var c = precio * cantidad;
      if(descuento=='0'){
          var ivo = ((iv * 100)/c);
       }else{
        c = c -descuento;
          var ivo = ((iv * 100)/c);
       }
       
       var idpd= $("#arrayidp").val();
       var codp= $("#arraycodp").val();
       var art= $("#arrayart").val();
       var precp= $("#arrayprecp").val();
       var cantp= $("#arraycantp").val();
       var totp= $("#arraytotp").val();
       var desp= $("#arraydesp").val();
       var ivp= $("#arrayivp").val();
       if(idpd=='0'){
       var idpd= $("#arrayidp").val(id);
       var codp= $("#arraycodp").val(codigo);
       var art= $("#arrayart").val(articulo);
       var cantp= $("#arraycantp").val(cantidad);
       var precp= $("#arrayprecp").val(precio);
       var totp= $("#arraytotp").val(total);
       var desp= $("#arraydesp").val(descuento);
       var ivp= $("#arrayivp").val(ivo);
      
       }else if (idpd!='0'){
        var idpd= $("#arrayidp").val(idpd+','+id);
       var codp= $("#arraycodp").val(codp+','+codigo);
       var art= $("#arrayart").val(art+','+articulo);
       var cantp= $("#arraycantp").val(cantp+','+cantidad);
       var precp= $("#arrayprecp").val(precp+','+precio);
       var totp= $("#arraytotp").val(totp+','+total);
       var desp= $("#arraydesp").val(desp+','+descuento);
       var ivp= $("#arrayivp").val(ivp+','+ivo);

       }

  }
      
    }
  });
var idtabular=1;
    function tipobusqueda(){
   var proveed="";
   var id_factura =$('#id_factura').val(); 
   if(id_factura!=''){
    if(isNaN(id_factura)){
      swal("Atención","Este campo solo puede contener numeros","warning");
      $('#id_factura').val('');
      $('#id_factura2').val('');
       $('#productotal').html('<table class="table table-striped table-responsive"><tr style="color:#22C7FC;background-color: rgb(238, 238, 238); font-weight: bold"><td>id</td><td>Código</td><td>Artículo</td><td>Cantidad</td><td>Precio Antes </td><td>Descuento</td><td>Total</td><td></td><td></td><td>Actualiza Inventario</td></tr></table> ');
  
   $('#descuento').val(0);
      $('#iv').val(0);
      $('#otroimpuesto').val(0);
      $('#total').val(0); 
      $('#subtotal').val(0);
      $('#cedula').val('');
      $('#nombre').val('');
   

    }else if(!isNaN(id_factura)){
 $.ajax({ 
      method: "POST", 
      dataType: "json",
      url: "<?php echo CController::createUrl('/compras/notaDebito/Tipobusqueda') ?>",
     data: {id_factura: id_factura}
    })
    .done(function(msg) {  

    if(msg.factura==1){
      swal("No encontrado!!", "Esta Factura no se encuentra registrada","warning");
      $('#id_factura').val("");
      $('#productotal').html('<table class="table table-striped table-responsive"><tr style="color:#22C7FC;background-color: rgb(238, 238, 238); font-weight: bold"><td>id</td><td>Código</td><td>Artículo</td><td>Cantidad</td><td>Precio Antes </td><td>Descuento</td><td>Total</td><td></td><td></td><td>Actualiza Inventario</td></tr></table> ');
      $('#descuento').val(0);
      $('#iv').val(0);
      $('#otroimpuesto').val(0);
      $('#total').val(0); 
      $('#subtotal').val(0);
      $('#cedula').val('');
      $('#nombre').val('');
    }else{
     
     $('#id_factura2').val(msg.factura.id_factura_compra); 
     $('#cedula').val(msg.proveedor.identificacion);
     $('#nombre').val(msg.proveedor.proveedores);
     $('#subtotal').val(msg.factura.subtotal);
     $('#descuento').val(msg.factura.id_descuento);
     $('#total').val(msg.factura.total2);
     $('#iv').val(msg.factura.iv);
     $('#idpv').val(msg.proveedor.id_proveedor);
     var productos = (msg.tapro);
     var vueltas = productos.length;
     var x=0;
     $('#productotal').html('<table class="table table-striped table-responsive"><tr style="color:#22C7FC;background-color: rgb(238, 238, 238); font-weight: bold"><td>id</td><td>Código</td><td>Artículo</td><td>Cantidad</td><td>Precio Antes </td><td>Descuento</td><td>Total</td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td>Actualiza Inventario</td><td></td><td></td></tr></table> ');
     for(i=1;i<=vueltas;i++){
    
      var $idpro = (msg.tapro[x].id);
      var $codigo = (msg.tapro[x].codigo_produc);
      var $articulo = (msg.tapro[x].nombre_produc);
      var $cantidad = (msg.tapro[x].cantidad);
      var $precio = (msg.tapro[x].costo_producto);
      var $descta = (msg.tapro[x].descuento);
      var $iv = (msg.tapro[x].iv);
      var $tott = (msg.tapro[x].total);
      var $idf = (msg.tapro[x].id_factura);  
      var p =   ($precio * $cantidad);
      var $dest = (((p)*$descta)/100); 
      var $ivst = (((parseInt($tott * $cantidad))*$iv)/100);
      var tott = p + $ivst -$dest;


      $('#productotal table').append('<tr id='+idtabular+'><td>'+$idpro+'</td><td>'+$codigo+'</td><td>'+$articulo+'</td><td>'+$cantidad+'</td><td>'+$precio+'</td><td>'+$descta+'</td><td>'+tott+'</td><td style="display:none">'+$idf+'</td><td style="display:none">'+$dest+'</td><td style="display:none">'+$ivst+'</td><td>0</td><td><a href=# onclick="eliminar('+idtabular+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Modificar" ></span></a></td><td><a href=# onclick="modificar('+idtabular+')"><span class="glyphicon glyphicon-pencil" ></span></a></td></tr>');
    x++;
    idtabular++;
  }

      var moneda =(msg.factura.id_moneda);
    if(moneda==1){
      $('#cl').attr('selected','selected');
      $('#NotaDebito_cambio').val('1.00');
      $('#NotaDebito_cambio').attr('readOnly','readOnly');
      $('#NotaDebito_mone').val(1);
      $('#dl').removeAttr('selected');
    }else if(moneda==2){
      $('#dl').attr('selected','selected');
      $('#NotaDebito_cambio').removeAttr('readOnly');
      $('#NotaDebito_cambio').val('');
      $('#cl').removeAttr('selected');
      $('#NotaDebito_mone').val(2);
    }
    } 

      }); 
     }
   }else{
    $('#exampleModal2').modal();
   }
     }
  function selectedAlumno2($id){
    var id_factura = $id;
    $("#id_factura").val($id);
    $("#id_factura2").val($id);
     $.ajax({ 
      method: "POST", 
      dataType: "json",
      url: "<?php echo CController::createUrl('/compras/notaDebito/Tipobusqueda') ?>",
     data: {id_factura: id_factura}
    })
    .done(function(msg) {
     $('#id_factura2').val(msg.factura.id_factura_compra); 
     $('#cedula').val(msg.proveedor.identificacion);
     $('#nombre').val(msg.proveedor.proveedores);
     $('#subtotal').val(msg.factura.subtotal);
     $('#descuento').val(msg.factura.id_descuento);
     $('#total').val(msg.factura.total2);
     $('#iv').val(msg.factura.iv);
     $('#idpv').val(msg.proveedor.id_proveedor);
     var productos = (msg.tapro);
     var vueltas = productos.length;
     var x=0;
  $('#productotal').html('<table class="table table-striped table-responsive"><tr style="color:#22C7FC;background-color: rgb(238, 238, 238); font-weight: bold"><td>id</td><td>Código</td><td>Artículo</td><td>Cantidad</td><td>Precio Antes </td><td>Descuento</td><td>Total</td><td style="display:none"></td><td style="display:none"></td><td style="display:none"></td><td>Actualiza Inventario</td><td></td><td></td></tr></table> ');
    for(i=1;i<=vueltas;i++){
    
      var $idpro = (msg.tapro[x].id);
      var $codigo = (msg.tapro[x].codigo_produc);
      var $articulo = (msg.tapro[x].nombre_produc);
      var $cantidad = (msg.tapro[x].cantidad);
      var $precio = (msg.tapro[x].costo_producto);
      var $descta = (msg.tapro[x].descuento);
      var $iv = (msg.tapro[x].iv);
      var $tott = (msg.tapro[x].total);
      var $idf = (msg.tapro[x].id_factura);  
      var p =   ($precio * $cantidad);
      var $dest = (((p)*$descta)/100); 
      var $ivst = (((parseInt($tott * $cantidad))*$iv)/100);
      var tott = p + $ivst -$dest;
      $('#productotal table').append('<tr id='+idtabular+'><td>'+$idpro+'</td><td>'+$codigo+'</td><td>'+$articulo+'</td><td>'+$cantidad+'</td><td>'+$precio+'</td><td>'+$descta+'</td><td>'+tott+'</td><td style="display:none">'+$idf+'</td><td style="display:none">'+$dest+'</td><td style="display:none">'+$ivst+'</td><td>0</td><td><a href=# onclick="eliminar('+idtabular+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Modificar" ></span></a></td><td><a href=# onclick="modificar('+idtabular+')"><span class="glyphicon glyphicon-pencil" ></span></a></td></tr>');
    x++;
idtabular++;
  }
       var moneda =(msg.factura.id_moneda);
    if(moneda==1){
      $('#cl').attr('selected','selected');
      $('#NotaDebito_cambio').val('1.00');
      $('#NotaDebito_cambio').attr('readOnly','readOnly');
      $('#dl').removeAttr('selected');
    }else if(moneda==2){
      $('#dl').attr('selected','selected');
      $('#NotaDebito_cambio').removeAttr('readOnly');
      $('#NotaDebito_cambio').val('');
      $('#cl').removeAttr('selected');
    } 
  var proveed=(msg.id_provedor);
     $('#exampleModal2').modal('hide');   
     }); 
  }
  function prod(){
   var articulo=$('#codigart').val();
   if(articulo!=''){
   $.ajax({ 
      method: "POST", 
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/compras/notaDebito/Busquedapro') ?>",
      //async: false,

      data: {articulo: articulo}
    })
    .done(function(msg) {
   
      if(msg.prod==1){
      swal("No encontrado!!", "Este Producto no se encuentra registrado","warning");
      $('#codigart').val("");
      $('#NotaDebito_art').val("");
      $('#NotaDebito_costo').val("");
      $('#NotaDebito_tot').val("");
      $('#ivt').val("");

       }else{
        var $moneda = $("#moneda").val(); 
     if($moneda==1){//colones
      $('#NotaDebito_costo').val(msg.prod.precioc_id);
      $('#NotaDebito_tot').val(msg.prod.precioc_id);
     }else{
      $('#NotaDebito_costo').val(msg.prod.precios_id);
      $('#NotaDebito_tot').val(msg.prod.precios_id);
     }
      $('#codigart').val(msg.prod.codigo);
      $('#NotaDebito_art').val(msg.prod.descripcionc);
      $('#ivt').val(msg.imp.monto);
      $('#idpro').val(msg.prod.id_productos);
      $('#NotaDebito_cant').val(1);
       }
     
});

   }else{
    $('#exampleModal3').modal();
   }

}
function selectedAlumno3(boton) {
  $.ajax({
      method: "POST",
      dataType: "json",
      url: "<?php echo CController::createUrl('/compras/notaDebito/DetalleMaterial3') ?>",
      data: { articulo: boton}
    })
    .done(function( msg ) {
       var $moneda = $("#moneda").val(); 
          if($moneda==1){
      $('#NotaDebito_costo').val(msg.prod.precioc_id);
      $('#NotaDebito_tot').val(msg.prod.precioc_id);
     }else{
      $('#NotaDebito_costo').val(msg.prod.precios_id);
      $('#NotaDebito_tot').val(msg.prod.precios_id);
     }
      $('#codigart').val(msg.prod.codigo);
      $('#NotaDebito_art').val(msg.prod.descripcionc);
      $('#idpro').val(msg.prod.id_productos);
      $('#ivt').val(msg.imp.monto);
      $('#NotaDebito_cant').val(1);
      $('#exampleModal3').modal('hide');//cierra la pantalla modal
    });
}

function sumar(){
    var $cantidad= $("#NotaDebito_cant").val();
    var codigo = $("#NotaDebito_art").val();
    var prec = $("#NotaDebito_costo").val();
    var desc = $("#NotaDebito_descu").val();
    if($cantidad<=0){
      swal("Error","Cantidad No puede ser menor o igual a 0","warning");
      $("#NotaDebito_cant").val(1);
    }else{
      if(codigo==''){
        $("#NotaDebito_tot").val(0);
      
    }else if(codigo!='' && prec !='' && desc !='0'){
        var total = parseInt(prec)*parseInt($cantidad);
        var tt = ((total * desc )/100);
        var preci = (total - tt);
        $("#NotaDebito_tot").val(preci);
    }else if(codigo!='' && prec !='' && desc =='0'){
        var total = parseInt(prec)*parseInt($cantidad);
        $("#NotaDebito_tot").val(total);
    }
    }   
}
function calcular(){
    var $precio = $("#NotaDebito_costo").val();
    var $desct = $("#NotaDebito_descu").val();
    var $cant = $("#NotaDebito_cant").val();
    var $t= $("#NotaDebito_tot").val();
    var $cod = $("#NotaDebito_art").val();
    if($cod!='' && $precio!='' && $cant!='' && $desct=='0'){
        var t= $precio * $cant ;
        $("#NotaDebito_tot").val(t);
        var tt = ((t*$desct)/100);
        var ttt= t - tt ;
     }else if($cod!='' && $precio!='' && $cant!='' && $desct!='0'){
        var t= $precio * $cant ;
        var tt = ((t*$desct)/100);
        var ttt= t - tt ;
        
        $("#NotaDebito_tot").val(ttt);
     }else if ($cod=='' ){
       $("#NotaDebito_descu").val(0);
       $("#NotaDebito_tot").val('');
     }
}
function calcula(){
     var $precio = $("#NotaDebito_costo").val();
     var $desct = $("#NotaDebito_descu").val();
     var $cant = $("#NotaDebito_cant").val();
     var $cod = $("#NotaDebito_art").val();
  if(isNaN($precio) ){
     swal("Atención","Este campo solo acepta números","warning");
     $("#NotaDebito_costo").val('');
     $("#NotaDebito_tot").val('');
  }else if($precio <0){
      swal("Atención","Precio no puede ser Menor a 0","warning");
      $("#NotaDebito_costo").val('');
  }else if($cod!='' && $precio!='' &&$cant!=''  &&$desct=='0'){  
      var t = ($precio * $cant);
      $("#NotaDebito_tot").val(t);
  }else if($cod!='' && $precio!='' &&$cant!='' &&$desct!='0') {
     var t = ($precio * $cant);
     var d = ((t * $desct)/100);
     var tt = t - d ;
    $("#NotaDebito_tot").val(tt);
  }
}
function añadirPro(){
    var $codigo = $("#codigart").val();
    var $articulo = $("#NotaDebito_art").val();
    var $cantidad= $("#NotaDebito_cant").val();
    var $precio= $("#NotaDebito_costo").val();
    var $precit=$("#NotaDebito_tot").val();
    var $descuento= $("#NotaDebito_descu").val();
    var $total= $("#NotaDebito_tot").val();
    var $subtotal= $("#subtotal").val();
    var $descuentot= $("#descuento").val();
    var $idpro= $("#idpro").val();
    var $iv= $("#ivt").val();
    var $tt= $("#total").val();
    var $prec= $("#subtotal").val();
    var $iva = $("#iv").val();
    var actual= $('#actualiza').val();
    var total= $('#productotal table tr');
    var $vacio ="";
    if(actual==1){
     actual='0';
    }else if(actual==2){
       actual='NO';
    }else if(actual==3){
       actual='SI';
    }
    if($articulo!='' && $codigo!='' && $precio!=''){
      var vueltas=total.length;//tamaño array
      var p = ($precio * $cantidad); //precio 
      var desct=((p* $descuento)/100);//cantidad descuento
      var pd = p - desct;
      if (desct!=0){
      var ivf= Math.round(((pd* $iv)/100));//cantidad iva
      }else{
      var ivf= Math.round(((p* $iv)/100));//cantidad iva   
      }
      var dsc= p - desct; // precio*cantidad - descuentocant (total)
      var t = Math.round(p + ivf - desct); //precio*cantidad + ivacant - descuento 
    if($tt=='0'){
      $("#subtotal").val(p);
      $("#descuento").val(desct);
      $("#iv").val(ivf);
      $("#total").val(t);
    } else if($tt!='0'){
      $ttt=parseInt($tt)+t;
      $prect=parseInt($prec)+p;
      $desct2=parseInt($descuentot)+parseInt(desct);
      $ivte=parseInt($iva)+parseInt(ivf);
      $("#descuento").val($desct2);
      $("#subtotal").val($prect);
      $("#iv").val($ivte);
      $("#total").val($ttt);
}
    for (var i=1; i<vueltas; i++) {
      var id= $('#productotal table tr').eq(''+i+'').find('td').eq(0).text();
      var codigo= $('#productotal table tr').eq(''+i+'').find('td').eq(1).text();
      var articulo= $('#productotal table tr').eq(''+i+'').find('td').eq(2).text();
      var cantidad= $('#productotal table tr').eq(''+i+'').find('td').eq(3).text();
      var precio= $('#productotal table tr').eq(''+i+'').find('td').eq(4).text();
      var descuento= $('#productotal table tr').eq(''+i+'').find('td').eq(8).text();
      var total= $('#productotal table tr').eq(''+i+'').find('td').eq(6).text();
      var iv= $('#productotal table tr').eq(''+i+'').find('td').eq(9).text();


if(id==$idpro){

if(descuento==$descuento && precio==$precio){

    var idtr=$('#productotal table tr').eq(''+i+'').attr('id');
    $cantidad=parseInt($cantidad)+parseInt(cantidad);
    t=t+parseInt(total);
    $subtotal=$subtotal-p;
    ivf = Math.round(parseInt(ivf) +parseInt(iv))  ;
    
    $("#"+idtr).remove();
}else{
}
}
}
    var $idf='';
  $('#productotal table').append('<tr id='+idtabular+'><td>'+$idpro+'</td><td>'+$codigo+'</td><td>'+$articulo+'</td><td>'+$cantidad+'</td><td>'+$precio+'</td><td>'+$descuento+'</td><td>'+t+'</td><td >'+$idf+'</td><td >'+desct+'</td><td >'+ivf+'</td><td>'+actual+'</td><td><a href=# onclick="eliminar('+idtabular+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Modificar" ></span></a></td><td><a href=# onclick="modificar('+idtabular+')"><span class="glyphicon glyphicon-pencil" ></span></a></td></tr>');
    idtabular++;


    }else{
      swal("Atención","Faltan Datos Para Agregar este producto","error");
    }
   

    var $codigo = $("#codigart").val($vacio);
    var $articulo = $("#NotaDebito_art").val($vacio);
    var $cantidad= $("#NotaDebito_cant").val(1);
    var $precio= $("#NotaDebito_costo").val($vacio);
    var $descuento= $("#NotaDebito_descu").val($vacio);
    var $total= $("#NotaDebito_tot").val($vacio);
    $("#ivt").val($vacio);
}

function eliminar($id){
    var id= $('#productotal table tr#'+$id+'').find('td').eq(0).text();
    var codigo= $('#productotal table tr#'+$id+'').find('td').eq(1).text();
    var articulo= $('#productotal table tr#'+$id+'').find('td').eq(2).text();
    var cantidad= $('#productotal table tr#'+$id+'').find('td').eq(3).text();
    var precio= $('#productotal table tr#'+$id+'').find('td').eq(4).text();
    var total= $('#productotal table tr#'+$id+'').find('td').eq(6).text();
    var descuento= $('#productotal table tr#'+$id+'').find('td').eq(8).text();
    var iv= $('#productotal table tr#'+$id+'').find('td').eq(9).text();
    var subtt =$('#subtotal').val();
    var ttv =$('#total').val();
    var desct =$('#descuento').val();
    var ivt  = $('#iv').val();
    var c = precio * cantidad;
    desct = desct - descuento;
    subtt = subtt - c ;
    ttv = ttv - total ;
    ivt = ivt - iv;

    $('#descuento').val(desct);
    $('#subtotal').val(subtt);
    $('#total').val(ttv);
    $('#iv').val(ivt);
    $("#"+$id).remove();
}

function modificar ($id){
    var id= $('#productotal table tr#'+$id+'').find('td').eq(0).text();
    var codigo= $('#productotal table tr#'+$id+'').find('td').eq(1).text();
    var articulo= $('#productotal table tr#'+$id+'').find('td').eq(2).text();
    var cantidad= $('#productotal table tr#'+$id+'').find('td').eq(3).text();
    var precio= $('#productotal table tr#'+$id+'').find('td').eq(4).text();
    var porc= $('#productotal table tr#'+$id+'').find('td').eq(5).text();
    var descuento= $('#productotal table tr#'+$id+'').find('td').eq(8).text();
    var total= $('#productotal table tr#'+$id+'').find('td').eq(6).text();
    
    var iv= $('#productotal table tr#'+$id+'').find('td').eq(9).text();
    var subtt =$('#subtotal').val();
    var ttv =$('#total').val();
    var desct =$('#descuento').val();
    var ivt  = $('#iv').val();

    var c = precio * cantidad;
    var cd = c - descuento;
    desct = desct - descuento;
    subtt = subtt - c ;
    ttv = ttv - total ;
    ivt = ivt - iv;
    pc = Math.round(total - iv);
    if(descuento!=0){
    var ivo = Math.round(((iv * 100)/cd));
    }else{
    var ivo = Math.round(((iv * 100)/c));
    }
    $('#descuento').val(desct);
    $('#subtotal').val(subtt);
    $('#total').val(ttv);
    $('#iv').val(ivt);
    $('#codigart').val(codigo);
    $('#NotaDebito_art').val(articulo);
    $('#NotaDebito_cant').val(cantidad);
    $('#NotaDebito_costo').val(precio);
    $("#NotaDebito_descu").val(porc);
    $('#NotaDebito_tot').val(pc);
    $('#ivt').val(ivo);
    $("#"+$id).remove();
}

 

</script>