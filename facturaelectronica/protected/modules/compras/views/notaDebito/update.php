<?php
/* @var $this NotaDebitoController */
/* @var $model NotaDebito */

$this->breadcrumbs=array(
	'Nota Debitos'=>array('index'),
	$model->id_nota=>array('view','id'=>$model->id_nota),
	'Update',
);

$this->menu=array(
	array('label'=>'List NotaDebito', 'url'=>array('index')),
	array('label'=>'Create NotaDebito', 'url'=>array('create')),
	array('label'=>'View NotaDebito', 'url'=>array('view', 'id'=>$model->id_nota)),
	array('label'=>'Manage NotaDebito', 'url'=>array('admin')),
);
?>

<h1>Update NotaDebito <?php echo $model->id_nota; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>