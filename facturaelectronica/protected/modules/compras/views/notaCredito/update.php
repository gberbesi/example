<?php
/* @var $this NotaCreditoController */
/* @var $model NotaCredito */

$this->breadcrumbs=array(
	'Nota Creditos'=>array('index'),
	$model->id_nota=>array('view','id'=>$model->id_nota),
	'Update',
);

$this->menu=array(
	array('label'=>'List NotaCredito', 'url'=>array('index')),
	array('label'=>'Create NotaCredito', 'url'=>array('create')),
	array('label'=>'View NotaCredito', 'url'=>array('view', 'id'=>$model->id_nota)),
	array('label'=>'Manage NotaCredito', 'url'=>array('admin')),
);
?>

<h1>Update NotaCredito <?php echo $model->id_nota; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>