<?php
/* @var $this NotaCreditoController */
/* @var $model NotaCredito */

$this->breadcrumbs=array(
	'Nota Creditos'=>array('index'),
	$model->id_nota,
);

$this->menu=array(
	array('label'=>'List NotaCredito', 'url'=>array('index')),
	array('label'=>'Create NotaCredito', 'url'=>array('create')),
	array('label'=>'Update NotaCredito', 'url'=>array('update', 'id'=>$model->id_nota)),
	array('label'=>'Delete NotaCredito', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_nota),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage NotaCredito', 'url'=>array('admin')),
);
?>

<h1>View NotaCredito #<?php echo $model->id_nota; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_nota',
		'consecutivo',
		'id_factura',
		'referencia',
		'motivo',
		'fecha_nota',
		'cedula',
		'nombre_cliente',
		'id_moneda',
		'cambio',
		'subtotal',
		'descuento',
		'iv',
		'otroimpuesto',
		'total',
	),
)); ?>
