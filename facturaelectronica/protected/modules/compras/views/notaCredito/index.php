<?php
/* @var $this NotaCreditoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Nota Creditos',
);

$this->menu=array(
	array('label'=>'Create NotaCredito', 'url'=>array('create')),
	array('label'=>'Manage NotaCredito', 'url'=>array('admin')),
);
?>

<h1>Nota Creditos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
