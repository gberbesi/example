<?php
/* @var $this NotaCreditoController */
/* @var $model NotaCredito */

$this->breadcrumbs=array(
	'Nota Creditos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List NotaCredito', 'url'=>array('index')),
	array('label'=>'Manage NotaCredito', 'url'=>array('admin')),
);
?>



<?php $this->renderPartial('_form', array('model'=>$model,'fc'=>$fc,'pdc'=>$pdc)); ?>