<div class="form-group col-md-<?php echo $size ?>;">
	<?php echo $form->labelEx($model,$atributo); ?>
	<?php echo $form->textField($model,$atributo,array('class'=>'form-control','id'=>$id,'value'=>$value,'onChange'=>$on));?>
	<?php echo $form->error($model,$atributo,array('class'=>'help-block text-center')); ?>
</div>