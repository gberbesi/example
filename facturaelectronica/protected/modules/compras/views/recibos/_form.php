<?php
/* @var $this RecibosController */
/* @var $model Recibos */
/* @var $form CActiveForm */
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'recibos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

   <div class="col-lg-offset-3 col-xs-offset-0 col-xs-12 col-lg-6" style="font-size: 18px" >
   	<table class="table table-responsive " >
   		

   		<tr>
     		   <td>
     		   	<?php echo $form->labelEx($model,'id_facturac'); ?>
     		   	<div class="input-group">
     		   		<input type="text" id="id_facturac" class="form-control" placeholder="" name="Recibos[id_facturac]" maxlength="20">
     		   		<span class="input-group-btn">
     		   		<button type="button" class="btn btn-primary" data-toggle="modal" onclick="tipobusqueda();"><span class="fa fa-search-plus"></span></button>
					<input type="text" id="id_cliente" class="form-control" placeholder="" readonly="1" style="display:none" name="Recibos[id_factura]" value="0">
					</span>
				</div>	
     		   </td>
     		   <td>
     	<?php echo $form->labelEx($model,'fecha'); ?>
		<?php echo $form->textField($model,'fecha',array('class'=>'form-control','value'=>date('j/n/Y H:i:s'),'readOnly'=>true)); ?>
		<?php echo $form->error($model,'fecha'); ?>
     		   </td>
   		</tr>
   		<tr>
   		<td>
   			<?php echo $form->labelEx($model,'nombre'); ?>
		<?php echo $form->textField($model,'nombre',array('type'=>'text','id'=>'nombre','class'=>'form-control','readOnly'=>true)); ?>
		<?php echo $form->error($model,'nombre'); ?>
   		</td>	
   		<td >
   			<?php echo $form->labelEx($model,'identificacion'); ?>
		<?php echo $form->textField($model,'identificacion',array('type'=>'text','id'=>'proveed','class'=>'form-control','readOnly'=>true)); ?>
		<?php echo $form->error($model,'identificacion'); ?>
   		</td>	

   		<td style="display: none">
   			<?php echo $form->labelEx($model,'id_proveedor'); ?>
		<?php echo $form->textField($model,'id_proveedor',array('type'=>'text','id'=>'id_provedor','class'=>'form-control','readOnly'=>true)); ?>
		<?php echo $form->error($model,'id_proveedor'); ?>
   		</td>	
   		     
   		</tr>
   		<tr>
   		<td colspan="2">
   			<?php echo $form->labelEx($model,'observaciones'); ?>
		<?php echo $form->textField($model,'observaciones',array('class'=>'form-control','style'=>'width:100%')); ?>
		<?php echo $form->error($model,'observaciones'); ?>
   		</td>	
   		
   		    
   		</tr>
   		<tr>
   		<td>
   			<label>Moneda</label>
		
                  <select class="form-control select2" style="width:100%" id="id_moneda" name="Recibos[id_moneda]" disabled>
					<option value="1" id="cl">Colones (Costa Rica)</option>
					<option value="2" id="dl">Dólares (Estados Unidos)</option>
					</select>
                        <?php echo $form->error($model,'id_moneda',array('class'=>'btn-xs alert-danger text-center')); ?>
   		</td>	
   		<td>
   			<label>Tipo Cambio</label>
		<?php echo $form->textField($model,'cambio',array('class'=>'form-control','value'=>'1.00','readOnly'=>true)); ?>
		<?php echo $form->error($model,'cambio'); ?>
   		</td>	
   		    
     	</tr>
   		<td>
   			<?php echo $form->labelEx($model,'saldo'); ?>
		<?php echo $form->textField($model,'saldo',array('class'=>'form-control','readOnly'=>true,'id'=>'saldo')); ?>
		<?php echo $form->error($model,'saldo'); ?>
			
		</td>	
   		<td>
   			<?php echo $form->labelEx($model,'abono'); ?>
		<?php echo $form->textField($model,'abono',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'abono'); ?>
			
		</td>	
		<tr>
		<td>
			<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
</td>
		</tr>
  
   		
   	</table>
   </div>



	</div>



	

	

<?php $this->endWidget(); ?>
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
     
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
           <h2 class="modal-title" id="exampleModal2Label" style="color:#22C7FC; font-weight:bold;">Añadir Cliente</h2>
      </div>
      
      <div class="modal-body">
<div class="table table-responsive table-striped">
 <?php $this->renderpartial('application.modules.compras.views.facturaCompra.admin2',array('model'=>$provee)); ?>
    
  
    

</div>

<div class="modal-footer">
        <button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button>
      </div>
</div>

                  </div><!-- /.box-body -->
            </div>
</div>
<!-- form -->

<script>
	  function tipobusqueda(){
   var proveed="";
   var id_facturac=$('#id_facturac').val();
   if(id_facturac!=''){
    if(!isNaN(id_facturac)){
 $.ajax({ 
      method: "POST", 
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/compras/recibos/Tipobusqueda') ?>",
     
      //async: false,

      data: {id_facturac: id_facturac}
    })
    .done(function(msg) {
    	
     $('#nombre').val(msg.nombre_cliente);
     $('#id_provedor').val(msg.id_provedor);
       var moneda =(msg.id_moneda);
    if(moneda==1){
    	$('#saldo').val("₡ "+msg.saldo);
    	$('#cl').attr('selected','selected');
    	$('#dl').removeAttr('selected');
    }else if(moneda==2){
    	$('#saldo').val("$ "+msg.saldo);
    	$('#dl').attr('selected','selected');
    	$('#cl').removeAttr('selected');
    }
 
	var proveed=(msg.id_provedor);

	  $.ajax({ 
      method: "POST", 
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/compras/recibos/Tipobusqueda2') ?>",
     
      //async: false,

      data: {proveed: proveed}
    })
    .done(function(msg2) {
 
    	    $('#proveed').val(msg2.identificacion);

     }); 
     });  

     }else{
      swal("No encontrado!!", "Este Cliente no se encuentra registrado","warning");
      $('#id_facturac').val("");
     }
   }else{
    $('#exampleModal2').modal();
   }
    

  }
      
function selectedAlumno2(boton) {//boton es lo q atrae del forech
 var provee="";
  $.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/compras/Recibos/detalleFactura') ?>",
      //async: false,
      data: { factura: boton}//valosr q pasa por post para la consulta
    })
    .done(function( msg ) {//retorno de la funcion detallematerial
    	var saldo = (msg.saldo);
    	if((saldo<=0)){
    		swal("Error","Error","warning");
    		 $('#exampleModal2').modal('hide');//cierra la pantalla modal
    	}else{
    	$('#id_provedor').val(msg.id_provedor);
    	$('#id_facturac').val(msg.id_factura_compra);
    	
    	var provee = (msg.id_provedor);
    	  $.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/compras/Recibos/detalleProvee') ?>",
      //async: false,
      data: { provee: provee}//valosr q pasa por post para la consulta
    })
    .done(function( msg2 ) {//retorno de la funcion detallematerial
    	console.log(msg2.id_provedor);
    	$('#nombre').val(msg2.proveedores);
    	$('#proveed').val(msg2.identificacion);
    	    var moneda =(msg.id_moneda);
    if(moneda==1){
    	$('#saldo').val("₡ "+msg.saldo);
    	$('#cl').attr('selected','selected');
    	$('#dl').removeAttr('selected');
    }else if(moneda==2){
    	$('#saldo').val("$ "+msg.saldo);
    	$('#dl').attr('selected','selected');
    	$('#cl').removeAttr('selected');
    }

    	 $('#exampleModal2').modal('hide');//cierra la pantalla modal
  
     
    });}     
    });
}
</script>
