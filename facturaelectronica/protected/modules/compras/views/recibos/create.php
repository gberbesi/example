<?php
/* @var $this RecibosController */
/* @var $model Recibos */

$this->breadcrumbs=array(
	'Reciboses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Recibos', 'url'=>array('index')),
	array('label'=>'Manage Recibos', 'url'=>array('admin')),
);
?>

<div class="box-header with-border">
                 <h3 class="box-title"><h1 style="color:#2a3f54;"> &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-file"></span><span style="color:black">  Recibos <?php echo $msg ;?> </span></h1></h3>
                <nav class="breadcrumb" style="background-color:white; padding-left: 10px;margin-left: 30px; font-size: 14px;">
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('usuario/usuario/index');?>" style="color:black"><span class="glyphicon glyphicon-home"></span><span style="color:#22C7FC">&nbsp;&nbsp;Inicio</span> </a><a>/</a>
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('compras/recibos/vpopmail_add_domain(domain, dir, uid, gid)');?>" style="color:black"><span class="glyphicon glyphicon-file"></span><span style="color:#22C7FC">&nbsp;&nbsp;Recibos</span> </a><a>/</a>
  <a class="breadcrumb-item active" style="color:black"><span class="glyphicon glyphicon-file"></span><span style="color:#22C7FC">&nbsp;&nbsp;Registro</span> </a>
   
</nav>                  
                </div>  

<?php $this->renderPartial('_form', array('model'=>$model,'provee'=>$provee)); ?>