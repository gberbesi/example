<?php
/* @var $this RecibosController */
/* @var $data Recibos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_recibo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_recibo), array('view', 'id'=>$data->id_recibo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_proveedor')); ?>:</b>
	<?php echo CHtml::encode($data->id_proveedor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->observaciones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_moneda')); ?>:</b>
	<?php echo CHtml::encode($data->id_moneda); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saldo')); ?>:</b>
	<?php echo CHtml::encode($data->saldo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('abono')); ?>:</b>
	<?php echo CHtml::encode($data->abono); ?>
	<br />


</div>