<?php
/* @var $this RecibosController */
/* @var $model Recibos */

$this->breadcrumbs=array(
	'Reciboses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Recibos', 'url'=>array('index')),
	array('label'=>'Create Recibos', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#recibos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
 <div class="box-header with-border">
                 <h3 class="box-title"><h1 style="color:#2a3f54;"> &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-file"></span><span style="color:black">  Recibos <?php echo $msg ;?> </span></h1></h3>
                <nav class="breadcrumb" style="background-color:white; padding-left: 10px;margin-left: 30px; font-size: 14px;">
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('usuario/usuario/index');?>" style="color:black"><span class="glyphicon glyphicon-home"></span><span style="color:#22C7FC">&nbsp;&nbsp;Inicio</span> </a><a>/</a>
  <a class="breadcrumb-item active" style="color:black"><span class="glyphicon glyphicon-user"></span><span style="color:#22C7FC">&nbsp;&nbsp;Recibos</span> </a>
   
</nav>                  
                </div>  




<div class="search-form" >
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<div class="table table-striped  table-condensed table-responsive" cellspacing="0" cellpadding="3" rules="all" style="background-color:transparent;border-color:#CCCCCC;border-width:1px;border-style:none;border-collapse:collapse;  font-size: 14px;">
<?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'usuario-grid',
                      'dataProvider'=>$model->search(),
                        //'filter'=>$model,
                        'itemsCssClass'=>'table table-striped  text-center',
                        'pagerCssClass'=>'pagination pull-right',
                        'afterAjaxUpdate'=>'function(){$.getScript("themes/classic/plugins/archivos_js/campo-numero.js");$.getScript("themes/classic/plugins/archivos_js/campo-letra.js");
                                $.getScript("themes/classic/plugins/archivos_js/script_select2.js");}',
                        'pager'=>array(
                            'htmlOptions'=>array('class'=>'pagination pagination-sm no-margin'),
                            'cssFile'=>'',
                            'hiddenPageCssClass'=>'disabled',
                            'selectedPageCssClass'=>'active',
                            'header'=>''),
                            'ajaxUrl'=>Yii::app()->createUrl("/compras/recibos/admin"),
                                    'columns'=>array(
		 array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>ID</p>",
                                                    'name'=>'id_recibo',
                                                    'htmlOptions'=>array('class'=>'col-md-0 '),
                                                    'value'=>'$data->id_recibo',
                                                    //'filter'=>CHtml::activeTextField($model,'id_productos',array('class'=>'form-control campo-letra ','placeholder'=>'ID','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),
		 array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Número Recibo</p>",
                                                    'name'=>'id_recibo',
                                                    'htmlOptions'=>array('class'=>'col-md-0 '),
                                                    'value'=>'$data->id_recibo',
                                                    //'filter'=>CHtml::activeTextField($model,'id_productos',array('class'=>'form-control campo-letra ','placeholder'=>'ID','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),
		 array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Referencia</p>",
                                                    'name'=>'id_recibo',
                                                    'htmlOptions'=>array('class'=>'col-md-0 '),
                                                    'value'=>'$data->id_recibo',
                                                    //'filter'=>CHtml::activeTextField($model,'id_productos',array('class'=>'form-control campo-letra ','placeholder'=>'ID','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),
		   array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Identificacion</p>",
                                                    'name'=>'id_proveedor',
                                                    'htmlOptions'=>array('class'=>'col-md-0 '),
                                                    'value'=>'$data->id_proveedor',
                                                    //'filter'=>CHtml::activeTextField($model,'id_productos',array('class'=>'form-control campo-letra ','placeholder'=>'ID','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),
		    array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Proveedor</p>",
                                                    'name'=>'id_proveedor',
                                                    'htmlOptions'=>array('class'=>'col-md-0 '),
                                                    'value'=>'$data->id_proveedor',
                                                    //'filter'=>CHtml::activeTextField($model,'id_productos',array('class'=>'form-control campo-letra ','placeholder'=>'ID','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),
		 array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Fecha</p>",
                                                    'name'=>'fecha',
                                                    'htmlOptions'=>array('class'=>'col-md-0 '),
                                                    'value'=>'$data->fecha',
                                                    //'filter'=>CHtml::activeTextField($model,'id_productos',array('class'=>'form-control campo-letra ','placeholder'=>'ID','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),
				 array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Moneda</p>",
                                                    'name'=>'id_moneda',
                                                    'htmlOptions'=>array('class'=>'col-md-0 '),
                                                    'value'=>'$data->id_moneda',
                                                    //'filter'=>CHtml::activeTextField($model,'id_productos',array('class'=>'form-control campo-letra ','placeholder'=>'ID','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),
		
						 array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Monto</p>",
                                                    'name'=>'Abono',
                                                    'htmlOptions'=>array('class'=>'col-md-0 '),
                                                    'value'=>'$data->abono',
                                                    //'filter'=>CHtml::activeTextField($model,'id_productos',array('class'=>'form-control campo-letra ','placeholder'=>'ID','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),
						  array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Estado</p>",
                                                    'name'=>'st_recibo',
                                                    'htmlOptions'=>array('class'=>'col-md-0 '),
                                                    'value'=>'$data->st_recibo',
                                                    //'filter'=>CHtml::activeTextField($model,'id_productos',array('class'=>'form-control campo-letra ','placeholder'=>'ID','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),
		array(
			'class'=>'CButtonColumn',
		),
	),
));

 ?>
 </div>