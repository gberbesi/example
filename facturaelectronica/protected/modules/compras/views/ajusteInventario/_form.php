<?php 
/* @var $this FacturaController */ 
/* @var $model Factura */
/* @var $form CActiveForm */
 date_default_timezone_set('America/Costa_Rica');  

?>

  
   <script src="js/sweetalert.min.js"></script>
    
     <h1 style="color:#2a3f54;"> &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-file"></span><span style="color:black;"> factura compra</span> </h1>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'factura-form',

    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false, 
)); ?>

 
<nav class="breadcrumb" style="background-color:white; padding-left: 10px;margin-left: 30px; font-size: 14px;">
  <a class="breadcrumb-item" href="#" style="color:black  "><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;<span style="color:#22C7FC" >Inicio</span> </a><a>/</a>
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('facturas/factura/admin');?>" style="color:black"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Facturación</span> </a><a>/</a>
  
  <span class="breadcrumb-item active" style="color:black"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Registro</span>  </span>
</nav>

<div class="form "  style="font-size: 14px; padding-left: 30px;">

  <div class="tab-content">
    <div id="facturas" class="tab-pane fade in active ">
    



   <br>

<div class="col-md-3 col-sm-6 col-xs-12 form-group ">
      <label for="fullname">accion*</label> 
<select id="accion" class="form-control select2" name="AjusteInventario[accion]">
		<option value="0">No Seleccionado</option>
		<option value="1">Entrada de producto</option>
		<option value="2">Salida de producto</option>

	</select>
	 </div>


 <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
      <label for="fullname">Moneda*</label> 
              
                  <?php echo $form->dropDownList($model,'moneda',CHtml::listData(Moneda::model()->findAll(),'id_moneda', 'moneda'),array('class'=>'form-control select2','style'=>'width:100%','id'=>'id_moneda','onChange'=>'selectedAlumno8()')) ?>
                        <?php echo $form->error($model,'moneda',array('class'=>'btn-xs alert-danger text-center')); ?>


    </div>

     <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
      <label for="fullname">tipo cambio*</label> 
              
                 
<?php echo $form->textField($model,'tipo_cambio',array('type'=>'text','id'=>'tipo_cambio','class'=>'form-control',"value"=>"1,00","readonly"=>true)); ?>
                    
   <?php echo $form->error($model,'tipo_cambio',array('class'=>'btn-xs alert-danger text-center')); ?>

    </div>
 

    <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
    <label for="fullname">Fecha*</label>
                
<?php echo $form->textField($model,'fecha',array('type'=>'text','id'=>'fecha','class'=>'form-control','value'=>date('j/n/Y'),'readOnly'=>true)); ?>
                    
   <?php echo $form->error($model,'fecha',array('class'=>'btn-xs alert-danger text-center')); ?>
                    
      <br>
    </div> 

<div class="col-md-2 col-sm-6 col-xs-12 ">
               <label for="fullname">Código Articulo*</label>
          <div class="input-group">
                            
                           <?php echo $form->textField($model,'id_codigo_producto2',array('type'=>'text','id'=>'id_codigo_producto','class'=>'form-control','placeholder'=>"")); ?>
                             <?php echo $form->error($model,'id_codigo_producto2',array('class'=>'btn-xs alert-danger text-center')); ?>
                             <span class="input-group-btn">
                                              <button type="button" class="btn btn-primary" data-toggle="modal" onclick="busquedapro()" ><span class="fa fa-search-plus"></span></button>
                                          </span>
                          </div>
                          </div>

     <div class="col-md-5 col-sm-6 col-xs-12 form-group ">
     <label for="fullname">Artículo*</label>
      <?php echo $form->textField($modelPro,'id_productos',array('type'=>'text','id'=>'id_articulo','class'=>'form-control','disabled'=>true)); ?>
                <?php echo $form->error($modelPro,'id_productos',array('class'=>'btn-xs alert-danger text-center')); ?>
       
    </div>

    <div class="col-md-2 col-lg-1 col-sm-6 col-xs-12 form-group ">
    <label for="fullname">Cantidad*</label>
     <?php echo $form->numberField($model,'cantidad',array('type'=>'text','id'=>'cantidad', 'value'=>1,'class'=>'form-control','onChange '=>'selectedAlumno4()')); ?>
                <?php echo $form->error($model,'cantidad',array('class'=>'btn-xs alert-danger text-center')); ?> 
        
    </div>
     <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
     <label for="fullname" style="font-size: 13px">Precio a.Impuesto*</label>
      <?php echo $form->textField($model,'precio_aimpu',array('type'=>'text','id'=>'precio_aimpu','class'=>'form-control','onBlur '=>'selectedAlumno5()')); ?>
                <?php echo $form->error($model,'precio_aimpu',array('class'=>'btn-xs alert-danger text-center')); ?>
     
       
    </div>
 <?php
    if(Yii::app()->user->name!='admin'){
      $var='display:none';
    }
     ?>

    
    <div class="col-md-2 offset-md-3  ">
               <label for="fullname">Total*</label>
          <div class="input-group">
                            
             <?php echo $form->textField($model,'total',array('type'=>'text','id'=>'total','class'=>'form-control','placeholder'=>"",'disabled'=>true)); ?>
               <?php echo $form->error($model,'total',array('class'=>'btn-xs alert-danger text-center')); ?>
               <span class="input-group-btn">
                    <button type="button" class="btn btn-success" onClick="selectedAlumno7()"><span class="fa fa-plus"></span></button>
                </span>
          </div><br>
          </div>

    <div class="col-md-12 col-sm-12 col-xs-12 form-group ">
      <div class="table table-responsive" id="productotal">
<table class="table table-striped table-responsive">
<tr style="color:#22C7FC;background-color: rgb(238, 238, 238); font-weight: bold">

<td>id</td>
<td>Código</td>
<td>Artículo</td>
  <td>Cantidad</td>
  <td>Precio Antes </td>
  <td>Total</td>
  <td></td>
  <td></td>
  <td></td>

</tr>
 
</table> 
    </div>
  </div><br>
  <div class="col-md-12">



 <div class="col-md-3 col-sm-6 col-xs-12   ">
    <label for="fullname">Sub Total*</label>
     <?php echo $form->textField($model,'subtotal',array('type'=>'text','id'=>'subtotal','class'=>'form-control','readonly'=>true)); ?>
                <?php echo $form->error($model,'subtotal',array('class'=>'btn-xs alert-danger text-center')); ?>
     
    </div>

    <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
    <label for="fullname">I.V</label>
     <?php echo $form->textField($model,'iv',array('type'=>'text','id'=>'iv','class'=>'form-control','readonly'=>true)); ?>
                <?php echo $form->error($model,'iv',array('class'=>'btn-xs alert-danger text-center')); ?>
        
    </div><br\>

    <div class="col-md-3 col-sm-6 col-xs-12   ">
    <label for="fullname">Otros Impuestos</label>
     <?php echo $form->textField($model,'otros_impuestos_id',array('type'=>'text','id'=>'fullname','class'=>'form-control','readonly'=>true)); ?>
                <?php echo $form->error($model,'otros_impuestos_id',array('class'=>'btn-xs alert-danger text-center')); ?>
     
    </div>
      <div class="col-md-3  col-sm-6 col-xs-12   form-group " style="margin-right: 100px; ">
    <label for="fullname">Total*</label>
     <?php echo $form->textField($model,'total2',array('type'=>'text','id'=>'total2','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
                <?php echo $form->error($model,'total2',array('class'=>'btn-xs alert-danger text-center')); ?>
     
    </div>


    <div class="col-md-1 col-sm-6 col-xs-12 col-lg-6 form-group  ">
    <label for="fullname">Motivo*</label>
     <?php echo $form->textarea($model,'motivo',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                <?php echo $form->error($model,'motivo',array('class'=>'btn-xs alert-danger text-center')); ?>
      
    </div>
 

    <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">tabla iv*</label>
     <?php echo $form->textField($model,'ivt',array('type'=>'text','id'=>'ivt','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($model,'ivt',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

    <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">tabla iv*</label>
     <?php echo $form->textField($modelPro,'id_productos',array('type'=>'text','id'=>'id_productos','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($modelPro,'id_productos',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

    
<!-- tabular  -->
<div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">*Id Articulo</label>
     <?php echo $form->textField($tablaProducto,'id_producto',array('type'=>'text','id'=>'id_producto','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'id_producto',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>
<div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">tabla producto*</label>
     <?php echo $form->textField($tablaProducto,'codigo_produc',array('type'=>'text','id'=>'codigo_produc','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'codigo_produc',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

        <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">tabla nombre produc*</label>
     <?php echo $form->textField($tablaProducto,'nombre_produc',array('type'=>'text','id'=>'nombre_produc','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'nombre_produc',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

            <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">tabla total produc*</label>
     <?php echo $form->textField($tablaProducto,'total',array('type'=>'text','id'=>'totalp','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'total',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>


        <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">costo*</label>
     <?php echo $form->textField($tablaProducto,'costo_producto',array('type'=>'text','id'=>'costo_productop','class'=>'form-control','readonly'=>true,'placeholder'=>"",'maxlength'=>8)); ?>
            <?php echo $form->error($tablaProducto,'costo_producto',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div> 

            <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">cantidad*</label>
     <?php echo $form->textField($tablaProducto,'cantidad',array('type'=>'text','id'=>'cantidadp','class'=>'form-control','readonly'=>true)); ?>
            <?php echo $form->error($tablaProducto,'cantidad',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

     <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">descuento*</label>
     <?php echo $form->textField($tablaProducto,'descuento',array('type'=>'text','id'=>'descuentop','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'descuento',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

         <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">descuento*</label>
     <?php echo $form->textField($tablaProducto,'iv',array('type'=>'text','id'=>'ivp','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'iv',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

    <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">Tipo de Documento:*</label>
<?php echo $form->textField($tablaProducto,'tipoexoneracion',array('type'=>'text','id'=>'tipoexoneracionv','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>

            <?php echo $form->error($tablaProducto,'tipoexoneracion',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

    <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">*Número de Documento</label>
     <?php echo $form->textField($tablaProducto,'documentoexo',array('type'=>'text','id'=>'documentoexov','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'documentoexo',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

        <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">*Nombre de la Institución</label>
     <?php echo $form->textField($tablaProducto,'instituexo',array('type'=>'text','id'=>'instituexov','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'instituexo',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

        <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">*Fecha Emisión</label>
     <?php echo $form->dateField($tablaProducto,'fechaexo',array('type'=>'text','id'=>'fechaexov','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'fechaexo',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

        <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">*Monto del Impuesto</label>
     <?php echo $form->textField($tablaProducto,'impuestoexo',array('type'=>'text','id'=>'impuestoexov','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'impuestoexo',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

        <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">*Porcentaje de Compra</label>
     <?php echo $form->textField($tablaProducto,'porcentajeexo',array('type'=>'text','id'=>'porcentajeexov','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'porcentajeexo',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>
  

   
<!-- tabular  -->

</div>
   <div class="row buttons col-md-3  col-xs-12 offset-md-2 pull-right " ><br>  
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Facturar' : 'Save',array('class'=>'btn btn-dark btn-block btn-lg ','style'=>'color:white;background-color:#0f70cd')); ?>
    </div>



      
</div>

 
</div>
<!-- /.box -->

</div>
</div>
<?php $this->endWidget()?>
<div id="modales" >
<!-- Modal 1-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
           <h2 class="modal-title" id="exampleModal3Label" style='color:#22C7FC;font-weight:bold'>Agregar Impuesto</h2>
      </div>
      
      <div class="modal-body">
<div class="table table-responsive table-striped">
    
    
    <table class="table">
  <thead>
    <tr style='color:#22C7FC;font-weight:bold'>
      <th scope="col">Añadir</th>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Porcentaje</th>
      
    </tr>
  </thead>

  <?php foreach ($clientes as $key => $value) { 
   echo "<tbody>";
    echo "<tr>";
     echo" <td>    <div class='container'>";
       echo "<button type= 'button' class='btn btn-dark' onClick='selectedAlumno(".$value->id_impuesto.")' id='addInput' name='".$key."' >";
       echo "<span class='glyphicon glyphicon-plus' aria-hidden='true'></span>";
         
      echo "</button>";
  
     echo "<br/>";
      echo "</div></td>";
     echo "<td name='id' id=".$value->id_impuesto.">  ".$value->id_impuesto." </td>";
     echo "<td name='cliente' id=".$value->impuesto_nombre."> ".$value->impuesto_nombre."</td>";
    echo " <td name=identificacion id=".$value->monto." >".$value->monto."</td>";
   
    echo "</tr>";
   
  echo "</tbody>";

  } ?> 
  
    

</table>
</div>
<div class="modal-footer">
        <button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button>
      </div>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->

</div>
</div>

<!-- Modal 3 -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
         <h2 class="modal-title" id="exampleModal3Label" style='color:#22C7FC;font-weight:bold'>Agregar Producto</h2>
      </div>
      
      <div class="modal-body">
<div class="table table-responsive table-striped">
    <?php $this->renderPartial('application.modules.registro.views.productos.admin2',array('model'=>$modelPro)); ?>
    
</div>
<div class="modal-footer">
        <button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button>
      </div>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->

</div>
</div>




<!-- Modal 2 -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
     
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
           <h2 class="modal-title" id="exampleModal2Label" style="color:#22C7FC; font-weight:bold;">Añadir Cliente</h2>
      </div>
      
      <div class="modal-body">
<div class="table table-responsive table-striped">
 <?php $this->renderPartial('application.modules.compras.views.proveedores.admin2',array('model'=>$modelClie)); ?>
    
  
    

</div>

<div class="modal-footer">
        <button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button>
      </div>
</div>

                  </div><!-- /.box-body -->
            </div>
</div>
</div><?php //echo Chtml::encode($form->textField($tablaProducto,'porcentajeexo',array('type'=>'text','id'=>'porcentajeexo','class'=>'form-control','size'=>'100%','onblur'=>'desexo()'))) ; exit;?>
<!-- form -->
<?php $idta=1;?>;
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
 <script>
     

function selectedAlumno(boton) {//boton es lo q atrae del forech

var prueba=$('#genderM:checked').val();//0 producto
var prueba2=$('#genderF:checked').val();//1 servicio

  $.ajax({ 
      method: "POST", 
      //dataType: "json",//trae arry
      dataType: "html",//trae html
      url: "<?php echo CController::createUrl('/facturas/factura/DetalleMaterial') ?>",
      //async: false,

      data: {id_cliente: boton,prueba:prueba, prueba2:prueba2}//valosr q pasa por post para la consulta
    })
    .done(function(msg) {//retorno de la funcion detallematerial


   $('#dynamicDiv').html(msg);//imprime con html un string directo al iv y brra el anterior

  var tablaiv= $('#dynamicDiv table tr').eq(1).find('td').eq(2).text();
 var precioc=$('#preciosinin_puesto').val();
 var precios=$('#preciossinin_puesto').val();
 var tablaivf= $('#dynamicDiv table tr').eq(1).find('td').eq(0).text();
  


if(precioc!=''&& tablaiv!=''){
  var preciofc=((parseInt(precioc)*parseInt(tablaiv))/100);
  var tablaiv= $('#dynamicDiv table tr').eq(1).find('td').eq(2).text();

  $('#precio_finalc_id').val(parseInt(preciofc)+parseInt(precioc));
   }


    if(precios!=''&& tablaiv!=''){

    var preciofs=((parseInt(precios)*parseInt(tablaiv))/100);

     $('#precio_finals_id').val(parseInt(preciofs)+parseInt(precios));
  }
  if(tablaiv==''){
    $('#precio_finals_id').val(precios);
    $('#precio_finalc_id').val(precioc);

  }

$('#lista_impuesto_id').val(tablaivf);

$('#exampleModal').modal('hide');//cierra la pantalla modal

    });
}

function selectedAlumno2(boton) {//boton es lo q atrae del forech

  $.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/facturas/factura/detalleMaterial2') ?>",
      //async: false,
      data: { id_cliente: boton}//valosr q pasa por post para la consulta
    })
    .done(function( msg ) {//retorno de la funcion detallematerial
      //console.log(msg);
      //material = msg;
      //console.log(event);
      //acionAgregar(msg);
      //alert( "Data Saved: " + msg );
      //console.log(msg);

      $('#nombre_cliente').val(msg.clientes);//imprime los valores con el json reutiliza los anteriores
      $('#id_provedor').val(msg.id_cliente);
      $('#identificacion').val(msg.identificacion);




     // $('#dynamicDiv').html(msg);//imprime con html un string directo al iv y brra el anterior

      $('#exampleModal2').modal('hide');//cierra la pantalla modal
    });
}
function selectedAlumno3(boton) {//boton es lo q atrae del forech

  $.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/facturas/factura/detalleMaterial3') ?>",
      //async: false,
      data: { id_cliente: boton}//valosr q pasa por post para la consulta
    })
    .done(function( msg ) {//retorno de la funcion detallematerial
      //console.log(msg);
      //material = msg;
      //console.log(event);
      //acionAgregar(msg);
      //alert( "Data Saved: " + msg );
      //
     //console.log(msg);

     var $moneda = document.getElementById("id_moneda").value; 
   


     if($moneda==1){//colones

      $('#precio_aimpu').val(msg.precioc_id);
      $('#total').val(msg.precioc_id);
     }else{
      $('#precio_aimpu').val(msg.precios_id);
      $('#total').val(msg.precios_id);
     }
      $('#id_codigo_producto').val(msg.codigo);//imprime los valores con el json reutiliza los anteriores
      $('#id_articulo').val(msg.descripcionc);
      
      //$('#total').val(msg.preciosinin_puesto);
      

      $('#ivt').val(msg.lista_impuesto_id);
      $('#id_productos').val(msg.id_productos);
    
     
     // $('#dynamicDiv').html(msg);//imprime con html un string directo al iv y brra el anterior

      $('#exampleModal3').modal('hide');//cierra la pantalla modal
    });
}
function selectedAlumno4() {//boton es lo q atrae del forech
 
var $monto = document.getElementById("cantidad").value;
var $precio = document.getElementById("precio_aimpu").value;

  if($monto<=0)
    {
      swal("Atencion!", "La cantidad ingresada debe ser mayor a 0","error");
      $('#cantidad').val(1);
        }else{
           
              var $total =($monto*$precio);
              $('#total').val($total);

        }
    }

function selectedAlumno5() {//boton es lo q atrae del forech

var $monto = document.getElementById("cantidad").value;
var $precio = document.getElementById("precio_aimpu").value;


//alert(isNaN($precio));
if(!isNaN($precio)){
 
              var $total =($monto*$precio);
              $('#total').val($total);

}else{

  swal('dato incorrecto!','el campo precio solo acepta numero','error');
  $('#precio_aimpu').val('');
}
  
}
function selectedAlumno6() {//boton es lo q atrae del forech

var $monto = document.getElementById("cantidad").value;
var $precio = document.getElementById("precio_aimpu").value;


var $total =($monto*$precio);
var $descuento=((($total*$descuento)/100)-($total))*-1;

if(!isNaN($descuento)){
$('#total').val($descuento);
}else{

  swal('dato incorrecto!','el campo descuento solo acepta numero','error');
  $('#id_descuento').val('');
}
  
}

var idtabular=1;


function selectedAlumno7() {//boton es lo q atrae del forech
//principales
var $codigo = document.getElementById("id_codigo_producto").value;
var $articulo = document.getElementById("id_articulo").value;
var $cantidad= document.getElementById("cantidad").value;
var $precio= document.getElementById("precio_aimpu").value;

var $total= document.getElementById("total").value;
var $subtotal= document.getElementById("subtotal").value;

var $idpro= document.getElementById("id_productos").value;
var $vacio ="";
//segundarios
var $total2= document.getElementById("total2").value;
var $ivs2= document.getElementById("iv").value;//ataja el iva total descontado del precio
var $ivs= document.getElementById("ivt").value;//porcentaje del producto o servicio


//leer tabla para supar productos iguales

var total= $('#productotal table tr');

var vueltas=total.length;

for (var i=1; i<vueltas; i++) {
  
var id= $('#productotal table tr').eq(''+i+'').find('td').eq(0).text();
var codigo= $('#productotal table tr').eq(''+i+'').find('td').eq(1).text();
var articulo= $('#productotal table tr').eq(''+i+'').find('td').eq(2).text();
var cantidad= $('#productotal table tr').eq(''+i+'').find('td').eq(3).text();
var precio= $('#productotal table tr').eq(''+i+'').find('td').eq(4).text();
var total= $('#productotal table tr').eq(''+i+'').find('td').eq(5).text();
var iv= $('#productotal table tr').eq(''+i+'').find('td').eq(6).text();


if(id==$idpro){

if(precio==$precio){
 var idtr=$('#productotal table tr').eq(''+i+'').attr('id');

$cantidad=parseInt($cantidad)+parseInt(cantidad);
$total=parseInt($total)+parseInt(total);
$subtotal=$subtotal-(cantidad*precio);
$ivs2=$ivs2-(((parseInt(total)*parseInt(iv))/100)); 
$("#"+idtr).remove();
}else{


}

}

} 

//fin de productos iguales 

if($ivs==1){ 
$ivs=13;
}
if($ivs==2){
$ivs=10;
}
if($ivs2==""){
  $ivs2=0;
}
if($total2==""){
  $total2=0;
}
 
if($codigo!="" && $articulo !="" && $precio !="" ){  
  
//tabular tabla

$('#productotal table').append('<tr id='+idtabular+'><td>'+$idpro+'</td><td>'+$codigo+'</td><td>'+$articulo+'</td><td>'+$cantidad+'</td><td>'+$precio+'</td><td>'+$total+'</td><td style="display:none">'+$ivs+'</td><td><a href=# onclick="eliminar('+idtabular+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Modificar" ></span></a></td><td><a href=# onclick="modificar('+idtabular+')"><span class="glyphicon glyphicon-pencil" ></span></a></td></tr>');

//fin tablña tabular


//tabular modal

idtabular++;



//fin tabular modal
//vaciar campos
$('#id_codigo_producto').val($vacio);
$('#id_articulo').val($vacio);
$('#cantidad').val(1);
$('#precio_aimpu').val($vacio);
$('#id_descuento').val(0);  
$('#total').val($vacio);
$('#ivt').val($vacio);
//fin vaciado de campos


//calculo de campos finales 
var $precio_final=(parseInt($total2)+parseInt($total));
var $iv_final=((parseInt($total)*parseInt($ivs))/100);
var $finaliv=(parseInt($iv_final)+parseInt($ivs2));
if($subtotal==''){
var $subtotalt=parseInt($precio)*parseInt($cantidad);
$('#subtotal').val(parseInt($subtotalt));
}else{
var $subtotalt=(parseInt($subtotal)+(parseInt($precio)*parseInt($cantidad)) );
$('#subtotal').val(parseInt($subtotalt));
}


$('#iv').val(parseInt($finaliv));
$('#total2').val((parseInt($subtotalt)+parseInt($finaliv)));

//fin de calculos

//alerta de error
}else{
  swal('Faltan Datos!','Faltan datos para agregar este producto','warning');
}
//fin de alerta 

}
function selectedAlumno8() {//boton es lo q atrae del forech

var $moneda = document.getElementById("id_codigo_producto").value;
var $moneda2 = document.getElementById("id_moneda").value;
if(isNaN($moneda)){
 $letra=1;
}else{
	$letra=0;
}
if($moneda2==1){
$('#tipo_cambio').val("1,00"); 
$('#tipo_cambio').attr('readonly', 'readonly');

}else{
	$('#tipo_cambio').val(""); 
$('#tipo_cambio').removeAttr('readonly');
}
 $.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",
      url: "<?php echo CController::createUrl('/facturas/factura/detalleMaterial4') ?>",
      //async: false,
      data: { id_cliente: $moneda,letra:$letra}//valosr q pasa por post para la consulta
    })
    .done(function( msg ) {//retorno de la funcion detallematerial

     
     if($moneda2==1){//colones

      $('#precio_aimpu').val(msg.precioc_id);

     }else{
      $('#precio_aimpu').val(msg.precios_id);

     }
           
     
     // $('#dynamicDiv').html(msg);//imprime con html un string directo al iv y brra el anterior

    });
  
}
function selectedAlumno9() {

   var colonesfinal= $('#preciosinin_puesto').val();
  var tablaiv= $('#dynamicDiv table tr').eq(1).find('td').eq(2).text();
 
 if(parseInt(colonesfinal)>0){


if(isNaN(colonesfinal)){

 swal("Atencion!", "Precio a.Impu ₡ no puede ser letra","error");
  $( "#preciosinin_puesto" ).val('');

 }else{
   if(tablaiv!=''){

 var preciofs=((parseInt(colonesfinal)*parseInt(tablaiv))/100);

 var colonesfinal3=parseInt(colonesfinal)+parseInt(preciofs) ; 

$('#precio_finalc_id').val(colonesfinal3);

   }else{

     var colonesfinal3=colonesfinal; 

$('#precio_finalc_id').val(colonesfinal3);
   }
 

 }  
 }else{
swal("Atencion!", "Precio a.Impu ₡ no puede ser menor a 0","error");
  $( "#preciosinin_puesto" ).val('');
  $('#precio_finalc_id').val('');
  
 }


}

function selectedAlumno10() {

   var colonesfinal= $('#preciossinin_puesto').val();
  var tablaiv= $('#dynamicDiv table tr').eq(1).find('td').eq(2).text();
 
if(parseInt(colonesfinal)>0){



   if(isNaN(colonesfinal)){

 swal("Atencion!", "Precio a.Impu $ no puede ser letra","error");
  $( "#preciossinin_puesto" ).val('');

 }else{

 if(tablaiv!=''){

 var preciofs=((parseInt(colonesfinal)*parseInt(tablaiv))/100);

 var colonesfinal3=parseInt(colonesfinal)+parseInt(preciofs) ; 

 $('#precio_finals_id').val(colonesfinal3);

   }else{
      var colonesfinal3=colonesfinal;
      $('#precio_finals_id').val(colonesfinal3);
  
   } 

 }
 }else{
 swal("Atencion!", "Precio a.Impu $ no puede ser menor a 0","error");
  $( "#preciossinin_puesto" ).val('');
  $('#precio_finals_id').val('');

 }
}



$( "#factura-form" ).submit(function( event ) {

//#facturas #productos #clientes
var href=$('.active a').attr('href');
//comienzo factura 



  if($('#total2').val()!='')
   {

if($('#accion').val()==0){
	swal('Atención!','Debe seleccionar una acción','warning');
	event.preventDefault(); 
}
    var total= $('#productotal table tr');//tamaño de la tabla
    //campos q almacenan string para guardas en la bbdd
    var arrayid="";
    var arraycodigo="";
    var arrayarticulo="";
    var arraycantidad="";
    var arrayprecio="";
    var arraydescuento="";
    var arraytotal="";
    var arrayiv="";
//fin campos q almacenan string para guardas en la bbdd
//campos q almasenaran el string
var arraytipoexoneracion='';
var arraydocumentoexo='';
var arrayinstituexo='';
var arrayfechaexo='';
var arrayimpuestoexo='';
var arrayporcentajeexo='';
//fin campos q almasenaran el string

    var vueltas=total.length;//cantidad de vueltas del for

   for (var i=1; i<vueltas; i++) {
  
//valores tabulares de la tabla
var idarticulo= $('#productotal table tr').eq(''+i+'').find('td').eq(0).text();
var codigo= $('#productotal table tr').eq(''+i+'').find('td').eq(1).text();
var articulo= $('#productotal table tr').eq(''+i+'').find('td').eq(2).text();
var cantidad= $('#productotal table tr').eq(''+i+'').find('td').eq(3).text();
var precio= $('#productotal table tr').eq(''+i+'').find('td').eq(4).text();
var descuento= $('#productotal table tr').eq(''+i+'').find('td').eq(5).text();
var total= $('#productotal table tr').eq(''+i+'').find('td').eq(6).text();
var iv= $('#productotal table tr').eq(''+i+'').find('td').eq(7).text();

if(arrayid==""){

arrayid=idarticulo;

}else if(arrayid!=""){

arrayid=arrayid+","+idarticulo;

}

if(arraycodigo==""){

arraycodigo=codigo;

}else if(arraycodigo!=""){

arraycodigo=arraycodigo+","+codigo;

}

if(arrayarticulo==""){

arrayarticulo=articulo;

}else if(arrayarticulo!=""){

arrayarticulo=arrayarticulo+","+articulo;

}



if(arraycantidad==""){

arraycantidad=cantidad;

}else if(arraycantidad!=""){

arraycantidad=arraycantidad+","+cantidad;

}  


if(arrayprecio==""){

arrayprecio=precio;

}else if(arrayprecio!=""){

arrayprecio=arrayprecio+","+precio;

}


if(arraydescuento==""){

arraydescuento=descuento;

}else if(arraydescuento!=""){

arraydescuento=arraydescuento+","+descuento;

}

if(arraytotal==""){

arraytotal=total;

}else if(arraytotal!=""){

arraytotal=arraytotal+","+total;

}

if(arrayiv==""){

arrayiv=iv;

}else if(arrayiv!=""){

arrayiv=arrayiv+","+iv;

}
// finvalores tabulares de la tabla


//fin valores tabulares de la modal

}


//asigna array de la tabla al campo
$('#id_producto').val(arrayid);
$('#codigo_produc').val(arraycodigo);
$('#nombre_produc').val(arrayarticulo);
$('#totalp').val(arraytotal);
$('#cantidadp').val(arraycantidad);
$('#costo_productop').val(arrayprecio);
$('#descuentop').val(arraydescuento);
$('#ivp').val(arrayiv);
//fin asignacion array de la tabla al campo


   }else{
 
      swal('Atención!','No puede generar un documento con monto total en cero','warning');
      event.preventDefault();     
     
   }
 
  //factura 
});

function eliminar(id)
{

var codigo= $('#productotal table tr#'+id+'').find('td').eq(1).text();
var articulo= $('#productotal table tr#'+id+'').find('td').eq(2).text();
var cantidad= $('#productotal table tr#'+id+'').find('td').eq(3).text();
var precio= $('#productotal table tr#'+id+'').find('td').eq(4).text();
var total= $('#productotal table tr#'+id+'').find('td').eq(5).text();
var iv=$('#productotal table tr#'+id+'').find('td').eq(6).text();
//tabular
var subtotalt=$('#subtotal').val();
var totalt=$('#total2').val();
var ivat=$('#iv').val();
var descuentot=$('#id_descuentot').val();
//subtotal
var $subtotal=((subtotalt)-((precio)*(cantidad)));
$('#subtotal').val($subtotal);



//total
var $totalconiva= ((parseInt(total)*parseInt(iv))/100);
var $totalconivaf=parseInt($totalconiva)+parseInt(total); 

var $total=(parseInt(totalt)-parseInt($totalconivaf));

$('#total2').val(parseInt($total));

//iv
var $iva=parseInt(ivat)-parseInt($totalconiva); 
$('#iv').val($iva);






$("#"+id).remove(); 
}

function modificar(id)
{
var codigo= $('#productotal table tr#'+id+'').find('td').eq(1).text();
var articulo= $('#productotal table tr#'+id+'').find('td').eq(2).text();
var cantidad= $('#productotal table tr#'+id+'').find('td').eq(3).text();
var precio= $('#productotal table tr#'+id+'').find('td').eq(4).text();
var total= $('#productotal table tr#'+id+'').find('td').eq(5).text();
var iv=$('#productotal table tr#'+id+'').find('td').eq(6).text();

//tabular
var subtotalt=$('#subtotal').val();
var totalt=$('#total2').val();
var ivat=$('#iv').val();



$('#id_codigo_producto').val(codigo);
$('#id_articulo').val(articulo);
$('#cantidad').val(cantidad);
$('#precio_aimpu').val(precio);

$('#total').val(total);
$('#ivt').val(iv);

//subtotal
var $subtotal=(parseInt(subtotalt)-(parseInt(precio)*parseInt(cantidad)));

$('#subtotal').val($subtotal);

//total
var $totalconiva=((parseInt(total)*parseInt(iv))/100);
var $totalconivaf=parseInt($totalconiva)+parseInt(total); 

var $total=(parseInt(totalt)-parseInt($totalconivaf));

$('#total2').val(parseInt($total));

//iv
var $iva=parseInt(ivat)-parseInt($totalconiva); 
$('#iv').val($iva);



$("#"+id).remove(); 

}

function selectedAlumno11(){

  var cedula=$( "#identificacioncedula" ).val();

$.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/facturas/factura/detalleMaterial5') ?>", 
      //async: false,
      data: { id_cliente: cedula}//valosr q pasa por post para la consulta
    })
    .done(function( msg ) {

if(msg==1){
  swal("Atencion!", "identificacion no se puede repetir","error");
  $( "#identificacioncedula" ).val('');
}
    });


}

function eliminarp(){
var tablaiv= $('#dynamicDiv table tr').eq(1).find('td').eq(2).text();
 var precioc=$('#preciosinin_puesto').val();
 var preciocf=$('#precio_finalc_id').val();

 var precios=$('#preciossinin_puesto').val();
 var preciosf=$('#precio_finals_id').val();




 var impuestoc=(parseInt(precioc)*parseInt(tablaiv))/100;
 var impuestos=(parseInt(precios)*parseInt(tablaiv))/100;

if(precios!=''){
   $('#precio_finals_id').val(parseInt(preciosf)-parseInt(impuestos));
}
if(precioc!=''){
 $('#precio_finalc_id').val(parseInt(preciocf)-parseInt(impuestoc));

}

  $("#1").remove(); 
}

function selectedAlumno20() {

  var codigo= $( "#procodigo" ).val();

 $.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/registro/productos/detalleMaterial2') ?>", 
      //async: false,
      data: { id_cliente: codigo}//valosr q pasa por post para la consulta
    })
    .done(function( msg ) {

if(msg==1){
  swal("Atención!", "Código del Producto ya existe","error");
  $( "#procodigo" ).val('');
}
    });
 

}


function busquedapro(){
   var identificacion=$('#id_codigo_producto').val();
   if(identificacion!=''){

   $.ajax({ 
      method: "POST", 
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/facturas/factura/Busquedapro') ?>",
      //async: false,

      data: {id_cliente: identificacion} 
    })
    .done(function(msg) {
      if(msg==1){
      swal("No encontrado!!", "Este Producto no se encuentra registrado","warning");
      $('#id_codigo_producto').val("");
       }else{
        var $moneda = document.getElementById("id_moneda").value; 

     if($moneda==1){//colones

      $('#precio_aimpu').val(msg.precioc_id);
      $('#total').val(msg.precioc_id);
     }else{
      $('#precio_aimpu').val(msg.precios_id);
      $('#total').val(msg.precios_id);
     }
      $('#id_codigo_producto').val(msg.codigo);//imprime los valores con el json reutiliza los anteriores
      $('#id_articulo').val(msg.descripcionc);
      
      //$('#total').val(msg.preciosinin_puesto);
      

      $('#ivt').val(msg.lista_impuesto_id);
      $('#id_productos').val(msg.id_productos);
       }
     
});

   }else{
    $('#exampleModal3').modal();
   }

}

 </script>

<?php if($_GET['id']){ ?>

 <script > 
$( document ).ready(function() {
var $_GET = <?php echo json_encode($_GET); ?>;
  $.ajax({ 
      method: "POST", 
    
      dataType: "json",
      url: "<?php echo CController::createUrl('/facturas/factura/Updatetabular') ?>",
      //async: false,

      data: {id_cliente: $_GET['id']}
    })
    .done(function(msg) {
      console.log(msg);
    for(var i=0;i<msg.length;i++){
$('#productotal table').append('<tr id='+idtabular+'><td>'+msg[i].id_producto+'</td><td>'+msg[i].codigo_produc+'</td><td>'+msg[i].nombre_produc+'</td><td>'+msg[i].cantidad+'</td><td>'+msg[i].costo_producto+'</td><td>'+msg[i].descuento+'</td><td>'+msg[i].total+'</td><td style="display:none">'+msg[i].iv+'</td><td><a href=# onclick="eliminar('+idtabular+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Modificar" ></span></a></td><td><a href=# onclick="modificar('+idtabular+')"><span class="glyphicon glyphicon-pencil" ></span></a></td><td><a  class="fa fa-pencil-square-o" data-toggle="modal" href=# onclick="exoneracion('+idtabular+','+msg[i].iv+')" ></a> </td></tr>');

//fin tablña tabular

idtabular++;
}
});


 });</script>

<?php } ?>
 
<?php if($_GET['guard']==1){ ?>
  <script>
$( document ).ready(function() {

 swal("Atencion!", "Proforma guardada con exito","success");
});
</script>
  <?php } ?>


<?php if($_GET['update']==1){ ?>
  <script>
$( document ).ready(function() {
 swal("Atencion!", "Proforma actualizada con exito","success");

});
</script>
  <?php } ?>


  