<?php
/* @var $this AjusteInventarioController */
/* @var $model AjusteInventario */

$this->breadcrumbs=array(
	'Ajuste Inventarios'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AjusteInventario', 'url'=>array('index')),
	array('label'=>'Manage AjusteInventario', 'url'=>array('admin')),
);
?>


<?php $this->renderPartial('_form', array(
			'model'=>$model,'modelPro'=>$modelPro,'modelClie'=>$modelClie,'clientes'=>$clientes,'clientes2'=>$clientes2,'clientes3'=>$clientes3,'tablaProducto'=>$tablaProducto,
		'estadoListData'=>$estadoListData)); ?> 