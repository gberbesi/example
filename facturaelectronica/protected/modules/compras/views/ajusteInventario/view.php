<?php
/* @var $this AjusteInventarioController */
/* @var $model AjusteInventario */

$this->breadcrumbs=array(
	'Ajuste Inventarios'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AjusteInventario', 'url'=>array('index')),
	array('label'=>'Create AjusteInventario', 'url'=>array('create')),
	array('label'=>'Update AjusteInventario', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AjusteInventario', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AjusteInventario', 'url'=>array('admin')),
);
?>

<h1>View AjusteInventario #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'consecutivo',
		'fecha',
		'moneda',
		'monto',
		'accion',
		'st_inventario',
	),
)); ?>
