<?php
/* @var $this AjusteInventarioController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ajuste Inventarios',
);

$this->menu=array(
	array('label'=>'Create AjusteInventario', 'url'=>array('create')),
	array('label'=>'Manage AjusteInventario', 'url'=>array('admin')),
);
?>

<h1>Ajuste Inventarios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
