<?php
/* @var $this AjusteInventarioController */
/* @var $model AjusteInventario */

$this->breadcrumbs=array(
	'Ajuste Inventarios'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AjusteInventario', 'url'=>array('index')),
	array('label'=>'Create AjusteInventario', 'url'=>array('create')),
	array('label'=>'View AjusteInventario', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AjusteInventario', 'url'=>array('admin')),
);
?>

<h1>Update AjusteInventario <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>