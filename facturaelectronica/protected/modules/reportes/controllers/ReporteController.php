<?php

class ReporteController extends Controller
{
    public function actionAdmin()
    {

        $this->render('admin');
    }

    public function actionCreate()
    {
        $this->render('create');
    }

    public function actionReportes()

    {
        Yii::import("application.modules.registro.models.Clientes", true); 
        Yii::import("application.modules.facturas.models.Factura", true); 
        $modelclie=new Clientes();
        $clientes = CHtml::listData(Clientes::model()->findAll(),  'id_cliente','clientes');
        $facturas=Factura::model()->findAll('id_cliente=105');
        $this->layout="//layouts/column1";
        $this->render('reportes',array('clientes'=>$clientes,'modelclie'=>$modelclie));
    }
    public function actionLisclie()
    {
     /*    ?><script>alert("lisclie")</script>
<?php*/
        Yii::import("application.modules.registro.models.Clientes", true); 
        $Clientes=Clientes::model()->findAll();

        $this->render('lisclie',array(
            'Clientes'=>$Clientes,
        ));
        
    }
    public function actionLisfac()
    {
     /*    ?><script>alert("lisclie")</script>
<?php*/ Yii::import("application.modules.registro.models.Clientes", true); 
        Yii::import("application.modules.facturas.models.Factura", true); 
        $facturas=Factura::model()->findAll();

        $this->render('lisfac',array(
            'facturas'=>$facturas,
            'Clientes'=>$Clientes,
        ));
        
    }
    public function actionLisfaclie($id)
    {
     /*    ?><script>alert("lisclie")</script>
<?php*/ Yii::import("application.modules.registro.models.Clientes", true); 
        Yii::import("application.modules.facturas.models.Factura", true); 
        $facturas=Factura::model()->findAll('id_cliente='.$id.'');
        foreach ($facturas as $key => $value) {
            var_dump($facturas[$key]->id_factura);
                 }
        
        $this->render('lisfaclie',array(
            'facturas'=>$facturas,
            'Clientes'=>$Clientes,
        ));
        
    }
    public function actionLisprodclie($id)
    {
        Yii::import("application.modules.registro.models.Clientes", true); 
        Yii::import("application.modules.facturas.models.TablaProducto", true); 
        Yii::import("application.modules.facturas.models.Productos", true); 

      
        $cliente=Clientes::model()->find('id_cliente='. $id.'');
        $sql="SELECT id, id_factura, id_cliente, id_producto, codigo_produc, nombre_produc, costo_producto, SUM( cantidad ) AS cantidad, SUM( total ) AS total, SUM( descuento ) AS descuento, SUM( iv ) AS IV FROM  tabla_producto WHERE id_cliente =".$id." GROUP BY codigo_produc";
        $comando = Yii::app()->db->createCommand($sql);
        $productos= $comando->queryAll();
       

        $cedula=$cliente->identificacion;
        $nombre=$cliente->clientes;

              $this->render('lisprodclie',array(
            
            'productos'=>$productos,
            'cedula'=>$cedula,
            'nombre'=>$nombre,
            
        ));
        
    }
    
    public function actionLisprod()
    {
     /*    ?><script>alert("lisclie")</script>
<?php*/ Yii::import("application.modules.registro.models.Productos", true); 
         
        $productos=Productos::model()->findAll();

        $this->render('lisprod',array(
            'productos'=>$productos,
            
        ));
        
    }
     public function actionLisfacex()
    {
 Yii::import("application.modules.facturas.models.Factura", true); 
 Yii::import("application.modules.facturas.models.Clientes", true); 

                          $facturas=Factura::model()->findAll();
                          
$client=$facturas[0]->id_cliente;
                



    //EXEL 

                                   $phpExcelPath = Yii::getPathOfAlias('ext.Excel.PHPExcel.Classes');
                                    $phpExcelPath2 = Yii::getPathOfAlias('application.extensions.Excel.PHPExcel.Classes.PHPExcel');
                                    spl_autoload_unregister(array('YiiBase','autoload'));
                                    include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
                                    include($phpExcelPath2 . DIRECTORY_SEPARATOR . 'IOFactory.php');
                                    spl_autoload_register(array('YiiBase','autoload'));
                                    $objPHPExcel = PHPExcel_IOFactory::load("file/Listadofacturas.xls");
                                    $objPHPExcel->setActiveSheetIndex(0);  //set first sheet as active
                                    $objPHPExcel->getSheet(0)->setTitle('censo'.date("d-m-Y"));
                                    $celda=13;
                 foreach ($facturas as $key => $value) {
                   
                   if($facturas[$key]['dias']=='0'){
                    $condicion="Contado";
                   }else if($value->dias!='0'){
                    $condicion="Credito";
                   }
                   $fecha=$value->fecha;
  
                   $arrayfec=explode(" ", $fecha); 
                   $fechav=$arrayfec[0];
                   $client=$facturas[$key]->id_cliente;
                   $clientes=Clientes::model()->findAll('id_cliente='.$client.' LIMIT 1' );
                   $cedula=$clientes[0]['identificacion'];
                   $nombre=$clientes[0]['clientes'];
   


                  
                   
                
                  

                   
                 
                $objPHPExcel->getActiveSheet()->getStyle('A'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$celda, $condicion);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('B'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$celda, (string)$value->factura,PHPExcel_Cell_DataType::TYPE_STRING);  
                
                  
                $objPHPExcel->getActiveSheet()->getStyle('C'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$celda, $fechav);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('D'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$celda, $cedula);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('E'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$celda, $nombre);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('F'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$celda, $value->subtotal );  

                  
                $objPHPExcel->getActiveSheet()->getStyle('G'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$celda, $value->id_descuento);  

                 
                $objPHPExcel->getActiveSheet()->getStyle('H'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$celda, 'Exento');  

                 
                $objPHPExcel->getActiveSheet()->getStyle('I'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$celda, $value->iv);  

                 
                $objPHPExcel->getActiveSheet()->getStyle('j'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('j'.$celda, $value->subtotal);

                 
                $objPHPExcel->getActiveSheet()->getStyle('K'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('K'.$celda, $value->total2);  
                $celda++;
}

                                    $objPHPExcel->getActiveSheet()->setTitle('Reporte_Listado_Factura');                                        
                                    $objPHPExcel->setActiveSheetIndex(0);
                                    header('Content-Type: application/vnd.ms-excel');
                                    header('Content-Disposition: attachment;filename="Reporte('.date('dmY').').xls"');
                                    header('Cache-Control: max-age=0');
                            
                                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                                    $objWriter->save('php://output');




    //FIN EXEL


            //$this->render('Reporte_AVV',array('avv'=>$fila)); 
    }
    public function actionLisfacliex($id)
    {
 Yii::import("application.modules.facturas.models.Factura", true); 
 Yii::import("application.modules.facturas.models.Clientes", true); 

                          $facturas=Factura::model()->findAll('id_cliente='.$id.'');
                          
$client=$facturas[0]->id_cliente;
                



    //EXEL 

               $phpExcelPath = Yii::getPathOfAlias('ext.Excel.PHPExcel.Classes');
                $phpExcelPath2 = Yii::getPathOfAlias('application.extensions.Excel.PHPExcel.Classes.PHPExcel');
                spl_autoload_unregister(array('YiiBase','autoload'));
                include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
                include($phpExcelPath2 . DIRECTORY_SEPARATOR . 'IOFactory.php');
                spl_autoload_register(array('YiiBase','autoload'));
                $objPHPExcel = PHPExcel_IOFactory::load("file/Listadofacxclientes.xls");
                $objPHPExcel->setActiveSheetIndex(0);  //set first sheet as active
                $objPHPExcel->getSheet(0)->setTitle('censo'.date("d-m-Y"));
                $celda=13;
                 foreach ($facturas as $key => $value) {
                   
                   if($facturas[$key]['dias']=='0'){
                    $condicion="Contado";
                   }else if($value->dias!='0'){
                    $condicion="Credito";
                   }
                   $fecha=$value->fecha;
  
                   $arrayfec=explode(" ", $fecha); 
                   $fechav=$arrayfec[0];
                   $client=$facturas[$key]->id_cliente;
                   $clientes=Clientes::model()->findAll('id_cliente='.$id.' LIMIT 1' );
                   $nombre=$clientes->clientes;
                   $cedula=$clientes[0]['identificacion'];
                   $nombre=$clientes[0]['clientes'];
 
                $objPHPExcel->getActiveSheet()->getStyle('A'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$celda, $cedula);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('B'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$celda,$nombre );  
                
                  
                $objPHPExcel->getActiveSheet()->getStyle('C'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$celda, (string)$value->factura,PHPExcel_Cell_DataType::TYPE_STRING);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('D'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$celda, $condicion);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('E'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$celda, $fechav);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('F'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$celda, $value->subtotal );  

                  
                $objPHPExcel->getActiveSheet()->getStyle('G'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$celda, $value->id_descuento);  

                 
                $objPHPExcel->getActiveSheet()->getStyle('H'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$celda, $value->iv);  

                 
                $objPHPExcel->getActiveSheet()->getStyle('I'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('I'.$celda, $value->iv);  

                 
                $objPHPExcel->getActiveSheet()->getStyle('j'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('j'.$celda, $value->total2);

                 
              
                $celda++;
}

                                    $objPHPExcel->getActiveSheet()->setTitle('Reporte');                                        
                                    $objPHPExcel->setActiveSheetIndex(0);
                                    header('Content-Type: application/vnd.ms-excel');
                                    header('Content-Disposition: attachment;filename="Reporte ('.date('dmY').').xls"');
                                    header('Cache-Control: max-age=0');
                            
                                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                                    $objWriter->save('php://output');




    //FIN EXEL


            //$this->render('Reporte_AVV',array('avv'=>$fila)); 
    }
    public function actionLisprodcliex($id)
    {
       Yii::import("application.modules.facturas.models.TablaProducto", true); 
       Yii::import("application.modules.registro.models.Clientes", true); 
       Yii::import("application.modules.registro.models.Productos", true); 

        
        $sql="SELECT id, id_factura, id_cliente, id_producto, codigo_produc, nombre_produc, costo_producto, SUM( cantidad ) AS cantidad, SUM( total ) AS total, SUM( descuento ) AS descuento, SUM( iv ) AS IV FROM  tabla_producto WHERE id_cliente =".$id." GROUP BY codigo_produc";
        $comando = Yii::app()->db->createCommand($sql);
        $productos= $comando->queryAll();
       
                            
        $client=$productos[0]->id_cliente;
                      



    //EXEL prodxclientes

               $phpExcelPath = Yii::getPathOfAlias('ext.Excel.PHPExcel.Classes');
                $phpExcelPath2 = Yii::getPathOfAlias('application.extensions.Excel.PHPExcel.Classes.PHPExcel');
                spl_autoload_unregister(array('YiiBase','autoload'));
                include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
                include($phpExcelPath2 . DIRECTORY_SEPARATOR . 'IOFactory.php');
                spl_autoload_register(array('YiiBase','autoload'));
                $objPHPExcel = PHPExcel_IOFactory::load("file/Listadoprodxclientes.xls");
                $objPHPExcel->setActiveSheetIndex(0);  //set first sheet as active
                $objPHPExcel->getSheet(0)->setTitle('censo'.date("d-m-Y"));
                $celda=13;
                $vueltas=0;
                 foreach ($productos as $key => $value) {
                   $arrayfec=explode(" ", $fecha); 
                   $fechav=$arrayfec[0];
                   $client=$productos[$key]->id_cliente;
                   $clientes=Clientes::model()->find('id_cliente='. $id.'');
                   $nombre=$clientes->clientes;
                   $cedula=$clientes['identificacion'];
                   $nombre=$clientes['clientes'];
                   $pprod=$productos[$key]['total']/$productos[$key]['cantidad'];
                   $dprod=$productos[$key]['descuento']/$productos[$key]['cantidad'];
                   $idprod=$productos[$key]['id_producto'];
                   $items=Productos::model()->find('id_productos='.$idprod.'');
                   $util=$items->precio_finalc_id-$items->precioc_id;
                if($vueltas<1){
                $objPHPExcel->getActiveSheet()->getStyle('A'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$celda, $cedula);  
                
                  
                $objPHPExcel->getActiveSheet()->getStyle('B'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$celda,$nombre );  
                }
                  
                $objPHPExcel->getActiveSheet()->getStyle('C'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$celda, $productos[$key]['nombre_produc']);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('D'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$celda, $productos[$key]['cantidad']);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('E'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$celda, $pprod);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('F'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$celda, $dprod);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('G'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$celda, $util);  

                 
                $objPHPExcel->getActiveSheet()->getStyle('H'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$celda, $productos[$key]['total']);

                 
              
                $celda++;
                $vueltas++;
}

            $objPHPExcel->getActiveSheet()->setTitle('Reporte');                                        
            $objPHPExcel->setActiveSheetIndex(0);
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Reporte ('.date('dmY').').xls"');
            header('Cache-Control: max-age=0');
    
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }
     public function actionLisprodex()
    {
         Yii::import("application.modules.facturas.models.Factura", true); 
         Yii::import("application.modules.registro.models.Productos", true); 

         $productos=Productos::model()->findAll();
                          
            $phpExcelPath = Yii::getPathOfAlias('ext.Excel.PHPExcel.Classes');
            $phpExcelPath2 = Yii::getPathOfAlias('application.extensions.Excel.PHPExcel.Classes.PHPExcel');
            spl_autoload_unregister(array('YiiBase','autoload'));
            include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
            include($phpExcelPath2 . DIRECTORY_SEPARATOR . 'IOFactory.php');
            spl_autoload_register(array('YiiBase','autoload'));
            $objPHPExcel = PHPExcel_IOFactory::load("file/Listadoproductos.xls");
            $objPHPExcel->setActiveSheetIndex(0);  //set first sheet as active
            $objPHPExcel->getSheet(0)->setTitle('Productos'.date("d-m-Y"));
            $celda=13;

                 foreach ($productos as $key => $value) {
                    if($value->tipo_producto==1){
                        $tipo='Servicio';
                      }else{
                        $tipo="Producto";
                      }
        
                $objPHPExcel->getActiveSheet()->getStyle('A'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$celda,$value->codigo,PHPExcel_Cell_DataType::TYPE_STRING);  
                
                  
                $objPHPExcel->getActiveSheet()->getStyle('B'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$celda,$value->descripcionc,PHPExcel_Cell_DataType::TYPE_STRING);  
                
                  
                $objPHPExcel->getActiveSheet()->getStyle('C'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$celda, $value->precio_finalc_id);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('D'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$celda, $value->precioc_id);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('E'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$celda,$value->precio_finals_id);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('F'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$celda, $value->precios_id );  

                  
                $objPHPExcel->getActiveSheet()->getStyle('G'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$celda, $tipo);  

                 
               

               
                $celda++;
}

                                    $objPHPExcel->getActiveSheet()->setTitle('Reporte_Listado_Productos');                                        
                                    $objPHPExcel->setActiveSheetIndex(0);
                                    header('Content-Type: application/vnd.ms-excel');
                                    header('Content-Disposition: attachment;filename="Reporte('.date('dmY').').xls"');
                                    header('Cache-Control: max-age=0');
                            
                                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                                    $objWriter->save('php://output');




    //FIN EXEL


            //$this->render('Reporte_AVV',array('avv'=>$fila)); 
    }

    

   public function actionWordclie()
{
      Yii::import("application.modules.registro.models.Clientes", true); 


         $clientes=Clientes::model()->findAll();

      $this->renderpartial('liscliedoc',array('clientes'=>$clientes,));
//$phpdocx = new phpdocx("mytemplate.docx");
}

   public function actionWordfac()
{
      Yii::import("application.modules.facturas.models.Factura", true); 
      Yii::import("application.modules.registro.models.Clientes", true); 

         $facturas=Factura::model()->findAll();

      $this->render('lisfacdoc',array('facturas'=>$facturas,));
//$phpdocx = new phpdocx("mytemplate.docx");
}
   public function actionWordprod()
{
     
      Yii::import("application.modules.registro.models.Productos", true); 

         $productos=Productos::model()->findAll();

      $this->render('lisprodoc',array('productos'=>$productos,));
//$phpdocx = new phpdocx("mytemplate.docx");
}
   public function actionWordclieprod($id)
  {

      Yii::import("application.modules.facturas.models.TablaProducto", true); 
      Yii::import("application.modules.registro.models.Clientes", true); 
      Yii::import("application.modules.registro.models.Productos", true); 
      $cliente=Clientes::model()->find('id_cliente='. $id.'');
      $sql="SELECT id, id_factura, id_cliente, id_producto, codigo_produc, nombre_produc, costo_producto, SUM( cantidad ) AS cantidad, SUM( total ) AS total, SUM( descuento ) AS descuento, SUM( iv ) AS IV FROM  tabla_producto WHERE id_cliente =".$id." GROUP BY codigo_produc";
      $comando = Yii::app()->db->createCommand($sql);
      $productos= $comando->queryAll();
      $cedula=$cliente->identificacion;
      $nombre=$cliente->clientes;
      $this->render('lisprocliedoc',array('productos'=>$productos,'cedula'=>$cedula,'nombre'=>$nombre));
  }
 public function actionWordfaclie($id)
  {
    Yii::import("application.modules.registro.models.Clientes", true); 
    Yii::import("application.modules.facturas.models.Factura", true); 


    $factura=Factura::model()->findAll('id_cliente='.$id.'');
    $cliente=Clientes::model()->find('id_cliente='.$id.'');

    $cedula=$cliente->identificacion;
    $nombre=$cliente->clientes;
    $this->render('lisfacliedoc',array('factura'=>$factura,'cedula'=>$cedula,'nombre'=>$nombre));

}



    public function actionLiscliex()
    {
         
         Yii::import("application.modules.registro.models.Clientes", true); 

         $clientes=Clientes::model()->findAll();
                          
            $phpExcelPath = Yii::getPathOfAlias('ext.Excel.PHPExcel.Classes');
            $phpExcelPath2 = Yii::getPathOfAlias('application.extensions.Excel.PHPExcel.Classes.PHPExcel');
            spl_autoload_unregister(array('YiiBase','autoload'));
            include($phpExcelPath . DIRECTORY_SEPARATOR . 'PHPExcel.php');
            include($phpExcelPath2 . DIRECTORY_SEPARATOR . 'IOFactory.php');
            spl_autoload_register(array('YiiBase','autoload'));
            $objPHPExcel = PHPExcel_IOFactory::load("file/Listadoclientes.xls");
            $objPHPExcel->setActiveSheetIndex(0);  //set first sheet as active
            $objPHPExcel->getSheet(0)->setTitle('Clientes'.date("d-m-Y"));
            $celda=13;

                 foreach ($clientes as $key => $value) {
                   
                $objPHPExcel->getActiveSheet()->getStyle('A'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$celda,$value->clientes,PHPExcel_Cell_DataType::TYPE_STRING);  
                
                  
                $objPHPExcel->getActiveSheet()->getStyle('B'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$celda,$value->identificacion,PHPExcel_Cell_DataType::TYPE_STRING);  
                
                  
                $objPHPExcel->getActiveSheet()->getStyle('C'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$celda, $value->telefono);  

                  
                $objPHPExcel->getActiveSheet()->getStyle('D'.$celda)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$celda, $value->correo);  

                  
               

                 
               

               
                $celda++;
}

                                    $objPHPExcel->getActiveSheet()->setTitle('Reporte_Listado_Productos');                                        
                                    $objPHPExcel->setActiveSheetIndex(0);
                                    header('Content-Type: application/vnd.ms-excel');
                                    header('Content-Disposition: attachment;filename="Reporte('.date('dmY').').xls"');
                                    header('Cache-Control: max-age=0');
                            
                                    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                                    $objWriter->save('php://output');




    //FIN EXEL


            //$this->render('Reporte_AVV',array('avv'=>$fila)); 
    }
        

    public function actionIndex()
    {
        $this->render('index');
    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}