<?php
	require_once dirname(__FILE__).'/PHPWord-master/src/PhpWord/Autoloader.php';
	\PhpOffice\PhpWord\Autoloader::register();
	use PhpOffice\PhpWord\PhpWord;
	use PhpOffice\PhpWord\Style\Font;

	$documento = new PhpWord();
	    $estilo_tabla = array(
			'borderColor' => '000000',
			'borderSize' => '5',
			'cellMargin' => '20',
			
	);
 
    $estilo_celda=array('borderColor'=>'ffffff');
	$documento->addFontStyle('rStyle', array('bold'=>true,  'size'=>10));
	$documento->addFontStyle('cStyle', array('bold'=>true,  'size'=>16));
	$fontStyle = new \PhpOffice\PhpWord\Style\Font();
	$fontStyle->setBold(true);

    // Add a section to the document.
    $section = $documento->addSection();
	$section->addText('Ventas de Productos por Cliente ','cStyle');
	/*Titulo y Declaracion de Tabla*/
	$documento->addTableStyle('mitabla',$estilo_tabla);
    // Set up our table.
    $table = $section->addTable('mitabla',$table_style);
	$table->addRow();
	$table->addCell(2200)->addText('Identificación','rStyle');
	$table->addCell(2200)->addText('Nombre','rStyle');
	$table->addCell(2200)->addText('Descripción','rStyle');
	$table->addCell(2200)->addText('Cantidad','rStyle');
	$table->addCell(2200)->addText('Precio Promedio','rStyle');
	$table->addCell(2200)->addText('Descuento Promedio','rStyle');
	$table->addCell(2200)->addText('Utilidad','rStyle');
	$table->addCell(2200)->addText('Total','rStyle');
	
	/* Primera Fila*/
	$vueltas=0;
	foreach ($productos as $key => $value) {
	$idpro=$productos[$key]['id_producto'];
	$producto=Productos::model()->find('id_productos='.$idpro.'');
	if($producto->tipo_producto==1){
		$tipo="Servicio";
	}else{
		$tipo="Producto";
	}
	
	$pprod=$productos[$key]['total']/$productos[$key]['cantidad'];
 	$dprod=$productos[$key]['descuento']/$productos[$key]['cantidad'];
 	$idprod=$productos[$key]['id_producto'];
 	$items=Productos::model()->find('id_productos='.$idprod.'');
 	$util=$items->precio_finalc_id-$items->precioc_id;
 	$totalc+=$productos[$key]['cantidad'];
 	$total+=$productos[$key]['total'];
 	$totalp+=$pprod;
 	$totald+=$dprod;
 	$totalu+=$util;

	$table->addRow();
	if($vueltas<1){
	$table->addCell(2200)->addText(htmlspecialchars($cedula));
	$table->addCell(2200)->addText(htmlspecialchars($nombre));
	}else{
	$table->addCell(2200)->addText(htmlspecialchars(' '));
	$table->addCell(2200)->addText(htmlspecialchars(' '));
	}
	$table->addCell(2200)->addText(htmlspecialchars($productos[$key]['nombre_produc']));
	$table->addCell(2200)->addText(htmlspecialchars($productos[$key]['cantidad']));
	$table->addCell(2200)->addText(htmlspecialchars($pprod));
	$table->addCell(2200)->addText(htmlspecialchars($dprod));
	$table->addCell(2200)->addText(htmlspecialchars($util));
	$table->addCell(2200)->addText(htmlspecialchars($productos[$key]['total']));

	$vueltas++;

}

	$table->addRow();
	$table->addCell(2200)->addText(htmlspecialchars(' '));
	$table->addCell(2200)->addText(htmlspecialchars(' '));
	$table->addCell(2200)->addText('TOTALES ','rStyle');
	$table->addCell(2200)->addText(htmlspecialchars($totalc),'rStyle');
	$table->addCell(2200)->addText(htmlspecialchars($totalp),'rStyle');
	$table->addCell(2200)->addText(htmlspecialchars($totald),'rStyle');
	$table->addCell(2200)->addText(htmlspecialchars($totalu),'rStyle');
	$table->addCell(2200)->addText(htmlspecialchars($total),'rStyle');

// --- Guardamos el documento
	$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($documento, 'Word2007');
	$objWriter->save('Ventas de Productos por Cliente.docx');

	header("Content-Disposition: attachment; filename='Ventas de Productos por Cliente.docx'");
	echo file_get_contents('Ventas de Productos por Cliente.docx');
	readfile('Ventas de Productos por Cliente.docx');
	unlink('Ventas de Productos por Cliente.docx');
        
?>