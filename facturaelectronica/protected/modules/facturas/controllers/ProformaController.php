<?php

class ProformaController extends Controller 
{ 
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','prof','prof2','prof3','prof4','prof5','prof6','prof7','prof8','prof12','prof14','prof15','prof16','prof17','prof18'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
{
		Yii::import("application.modules.registro.models.Productos", true); 
		Yii::import("application.modules.registro.models.Identificacion", true); 
		Yii::import("application.modules.registro.models.TipoCodigo", true); 
		Yii::import("application.modules.registro.models.TablaProducto",true);

        $estadoListData = CHtml::listData(Provincia::model()->findAll(),  'id_provincia','provincia');
   
		$model=new Proforma;
		$modelPro=new Productos;
		$modelClie=new Clientes;
		$modelIde=new Identificacion;
		$tablaProducto=new TablaProducto;
		$TablaProductoProforma=new TablaProductoProforma;

	    $clientes=ListaImpuesto::model()->findAll();
	    $clientes2=Clientes::model()->findAll();
	    $clientes3=Productos::model()->findAll();
		
  		$modelClie->unsetAttributes();  // clear any default values
		if(isset($_GET['Clientes']))
			$modelClie->attributes=$_GET['Clientes']; 
		
		$modelPro->unsetAttributes();  // clear any default values
		if(isset($_GET['Productos']))
			$modelPro->attributes=$_GET['Productos'];
			
		
		if(isset($_POST['Proforma']))
		{	
			//tabular productos
			$tablaProducto->attributes=$_POST['TablaProducto'];
			$tablaProducto->codigo_produc=$_POST['TablaProducto']['codigo_produc'];

			$arrayid=explode(",", $_POST['TablaProducto']['id_producto']); 
			$arraycodigo=explode(",", $_POST['TablaProducto']['codigo_produc']); 
			$arraynombre=explode(",", $_POST['TablaProducto']['nombre_produc']);
			$arraytotal=explode(",", $_POST['TablaProducto']['total']);
			$arraycantidad=explode(",", $_POST['TablaProducto']['cantidad']);
			$arraycosto=explode(",", $_POST['TablaProducto']['costo_producto']);
			$arraydescuento=explode(",", $_POST['TablaProducto']['descuento']);
			$arrayiv=explode(",", $_POST['TablaProducto']['iv']);
			//modales tabulares
			$arraytipoexoneracion=explode(",", $_POST['TablaProducto']['tipoexoneracion']);
			$arraydocumentoexo=explode(",", $_POST['TablaProducto']['documentoexo']);
			$arrayinstituexo=explode(",", $_POST['TablaProducto']['instituexo']);
			$arrayfechaexo=explode(",", $_POST['TablaProducto']['fechaexo']);
			$arrayimpuestoexo=explode(",", $_POST['TablaProducto']['impuestoexo']);
			$arrayporcentajeexo=explode(",", $_POST['TablaProducto']['porcentajeexo']);

			//proforma

			$model->attributes=$_POST['Proforma'];
			
			$valido=$model->validate();
			if($valido){
			if( $model->total2!=""){
			$model->save();

			foreach ($arraycodigo as $key => $value) {
              		$TablaProductoProforma=new TablaProductoProforma;
      				$TablaProductoProforma->id_producto=$arrayid[$key];
              		$TablaProductoProforma->id_factura=$model->id_proforma;
              		$TablaProductoProforma->codigo_produc=$arraycodigo[$key];
              		$TablaProductoProforma->nombre_produc=$arraynombre[$key];
              		$TablaProductoProforma->total=$arraytotal[$key];
              		$TablaProductoProforma->cantidad=$arraycantidad[$key];
              		$TablaProductoProforma->costo_producto=$arraycosto[$key];
              		$TablaProductoProforma->descuento=$arraydescuento[$key]; 
              		$TablaProductoProforma->iv=$arrayiv[$key]; 
              		$TablaProductoProforma->id_cliente=$model->id_cliente;
              		//modal
              		$TablaProductoProforma->tipoexoneracion=$arraytipoexoneracion[$key];
              		$TablaProductoProforma->documentoexo=$arraydocumentoexo[$key];
              		$TablaProductoProforma->instituexo=$arrayinstituexo[$key];
              		$TablaProductoProforma->fechaexo=$arrayfechaexo[$key];
              		$TablaProductoProforma->impuestoexo=$arrayimpuestoexo[$key];
              		$TablaProductoProforma->porcentajeexo=$arrayporcentajeexo[$key];

              		$TablaProductoProforma->save(false);             		
              	}

          $this->redirect(array('create&guard=1')); 
          }				

				
			}
		}

		if(isset($_POST['Productos'])) 
		{
			$modelPro->attributes=$_POST['Productos'];
			
			$valido=$modelPro->validate();
		    
			if($valido){
               
				
				if($modelPro->precio_finalc_id!=0){
					$modelPro->save();
					$this->redirect(array('//registro/productos/admin'));
				}

				
			}
			
		}
	if(isset($_POST['Clientes']))
		{
			$modelClie->attributes=$_POST['Clientes'];
			$valido=$modelClie->validate();
		    
			if($valido){
               
				$modelClie->save();
				Yii::app()->user->setFlash('success','Registro Exitoso');
				if($modelClie->identificacion!=""){
				$this->redirect(array('//registro/clientes/admin'));
				}
			}
			
		}
		/*if(isset($_POST['TablaProducto']))
		{
			

			$tablaProducto->attributes=$_POST['TablaProducto'];
			$valido=$tablaProducto->validate();
		}*/
		$this->render('create',array(
			'model'=>$model,'modelPro'=>$modelPro,'modelClie'=>$modelClie,'clientes'=>$clientes,'clientes2'=>$clientes2,'clientes3'=>$clientes3,'tablaProducto'=>$tablaProducto,
		'estadoListData'=>$estadoListData)); 

	}
 
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		//var_dump($model);exit();
		Yii::import("application.modules.registro.models.Productos", true); 
		Yii::import("application.modules.registro.models.Identificacion", true); 
		Yii::import("application.modules.registro.models.TipoCodigo", true); 
		Yii::import("application.modules.registro.models.TablaProducto",true);

        $estadoListData = CHtml::listData(Provincia::model()->findAll(),  'id_provincia','provincia');
   
		$modelPro=new Productos;
		$modelClie=new Clientes;
		$modelIde=new Identificacion;
		$tablaProducto=new TablaProducto;
		$TablaProductoProforma=new TablaProductoProforma;

	    $clientes=ListaImpuesto::model()->findAll();
	    $clientes2=Clientes::model()->findAll();
	    $clientes3=Productos::model()->findAll();
		
  		$modelClie->unsetAttributes();  // clear any default values
		if(isset($_GET['Clientes']))
			$modelClie->attributes=$_GET['Clientes']; 
		
		$modelPro->unsetAttributes();  // clear any default values
		if(isset($_GET['Productos']))
			$modelPro->attributes=$_GET['Productos'];
			
		
		if(isset($_POST['Proforma']))
		{	
			//tabular productos
			$tablaProducto->attributes=$_POST['TablaProducto'];
			$tablaProducto->codigo_produc=$_POST['TablaProducto']['codigo_produc'];

			$arrayid=explode(",", $_POST['TablaProducto']['id_producto']); 
			$arraycodigo=explode(",", $_POST['TablaProducto']['codigo_produc']); 
			$arraynombre=explode(",", $_POST['TablaProducto']['nombre_produc']);
			$arraytotal=explode(",", $_POST['TablaProducto']['total']);
			$arraycantidad=explode(",", $_POST['TablaProducto']['cantidad']);
			$arraycosto=explode(",", $_POST['TablaProducto']['costo_producto']);
			$arraydescuento=explode(",", $_POST['TablaProducto']['descuento']);
			$arrayiv=explode(",", $_POST['TablaProducto']['iv']);
			//modales tabulares
			$arraytipoexoneracion=explode(",", $_POST['TablaProducto']['tipoexoneracion']);
			$arraydocumentoexo=explode(",", $_POST['TablaProducto']['documentoexo']);
			$arrayinstituexo=explode(",", $_POST['TablaProducto']['instituexo']);
			$arrayfechaexo=explode(",", $_POST['TablaProducto']['fechaexo']);
			$arrayimpuestoexo=explode(",", $_POST['TablaProducto']['impuestoexo']);
			$arrayporcentajeexo=explode(",", $_POST['TablaProducto']['porcentajeexo']);

			//proforma

			$model->attributes=$_POST['Proforma'];
			
			$valido=$model->validate();
			if($valido){
			if( $model->total2!=""){
			$model->save();  
			$TablaProforma=TablaProductoProforma::model()->findAll('id_factura='.$model->id_proforma);
			foreach ($TablaProforma as $key => $value) {
				$TablaProforma[$key]->delete();
			}

			foreach ($arraycodigo as $key => $value) {
              		$TablaProductoProforma=new TablaProductoProforma;
      				$TablaProductoProforma->id_producto=$arrayid[$key];
              		$TablaProductoProforma->id_factura=$model->id_proforma;
              		$TablaProductoProforma->codigo_produc=$arraycodigo[$key];
              		$TablaProductoProforma->nombre_produc=$arraynombre[$key];
              		$TablaProductoProforma->total=$arraytotal[$key];
              		$TablaProductoProforma->cantidad=$arraycantidad[$key];
              		$TablaProductoProforma->costo_producto=$arraycosto[$key];
              		$TablaProductoProforma->descuento=$arraydescuento[$key]; 
              		$TablaProductoProforma->iv=$arrayiv[$key]; 
              		$TablaProductoProforma->id_cliente=$model->id_cliente;
              		//modal
              		$TablaProductoProforma->tipoexoneracion=$arraytipoexoneracion[$key];
              		$TablaProductoProforma->documentoexo=$arraydocumentoexo[$key];
              		$TablaProductoProforma->instituexo=$arrayinstituexo[$key];
              		$TablaProductoProforma->fechaexo=$arrayfechaexo[$key];
              		$TablaProductoProforma->impuestoexo=$arrayimpuestoexo[$key];
              		$TablaProductoProforma->porcentajeexo=$arrayporcentajeexo[$key];

              		$TablaProductoProforma->save(false);             		
              	}

          $this->redirect(array('create&update=1')); 
          }				

				
			}
		}

		if(isset($_POST['Productos'])) 
		{
			$modelPro->attributes=$_POST['Productos'];
			
			$valido=$modelPro->validate();
		    
			if($valido){
               
				
				if($modelPro->precio_finalc_id!=0){
					$modelPro->save();
					$this->redirect(array('//registro/productos/admin'));
				}

				
			}
			
		}
	if(isset($_POST['Clientes']))
		{
			$modelClie->attributes=$_POST['Clientes'];
			$valido=$modelClie->validate();
		    
			if($valido){
               
				$modelClie->save();
				Yii::app()->user->setFlash('success','Registro Exitoso');
				if($modelClie->identificacion!=""){
				$this->redirect(array('//registro/clientes/admin'));
				}
			}
			
		}
		/*if(isset($_POST['TablaProducto']))
		{
			

			$tablaProducto->attributes=$_POST['TablaProducto'];
			$valido=$tablaProducto->validate();
		}*/
		$this->render('create',array(
			'model'=>$model,'modelPro'=>$modelPro,'modelClie'=>$modelClie,'clientes'=>$clientes,'clientes2'=>$clientes2,'clientes3'=>$clientes3,'tablaProducto'=>$tablaProducto,
		'estadoListData'=>$estadoListData));
	}
	public function actionProf($id)
	{
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($proforma->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');

		$this->render('Prof',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'contribuyente'=>$contribuyente

				));
	}
		public function actionProf2($id)
	{
		Yii::import("application.modules.facturas.models.Proforma", true); 
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($proforma->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');
		$this->render('Prof2',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'contribuyente'=>$contribuyente
				));
	}
	public function actionProf3($id)
	{
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($proforma->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');

		$this->render('Prof3',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'contribuyente'=>$contribuyente

				));
	}
	public function actionProf4($id)
	{	
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($proforma->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');
		$impresion=Impresion::model()->find('id=1');
		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Prof4',array('id'=>$id,
			'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'impresion'=>$impresion,
					'contribuyente'=>$contribuyente

	));
	}
	public function actionProf5($id)
	{
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$fact=$proforma->proforma;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');

	 
		$this->render('Prof5',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente
	));
	}
		public function actionProf6($id)
	{	
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($proforma->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$this->render('Prof6',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente
	));
	}
	public function actionProf7($id)
	{	
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($proforma->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		
		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Prof7',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}
	public function actionProf8($id)
	{	
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($proforma->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$this->render('Prof8',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}
	public function actionProf12($id)
	{	
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$this->render('Prof12',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}
	public function actionProf14($id)
	{	
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($proforma->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$this->render('Prof14',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}
	public function actionProf15($id)
	{	
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($proforma->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$this->render('Prof15',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente
	));
	}
	public function actionProf16($id)
	{	
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($proforma->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$this->render('Prof16',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente
	));
	}
	public function actionProf17($id)
	{	
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($proforma->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$this->render('Prof17',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente
	));
	}
	public function actionProf18($id)
	{	
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$proforma=Proforma::model()->findByPk($id);
		$idcli=intval($proforma->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($proforma->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProductoProforma::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		$this->render('Prof18',array('id'=>$id,
					'proforma'=>$proforma,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente
	));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Proforma');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin() 
	{
		$form=Impresion::model()->find();
		$ruta=$form->rutapr;
		$vista=$form->vistapr;

		$model=new Proforma('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Proforma']))
			$model->attributes=$_GET['Proforma'];
		$nopago=Factura::model()->findAll('factura="" and dias="0"');

		$this->render('admin',array(
			'model'=>$model,
			'form'=>$form,
			'ruta'=>$ruta,
			'vista'=>$vista
		));

	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Proforma the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Proforma::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Proforma $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='proforma-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
