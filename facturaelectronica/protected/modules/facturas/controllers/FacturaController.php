<?php

class FacturaController extends Controller 
{ 
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1'; 

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','Create2','detalleMaterial','detalleMaterial2','detalleMaterial3','DetalleMaterial4','DetalleMaterial5','Payment','Fact','SelectMunicipio','SelectParroquia','Fact2','Fact3','Fact4','Fact5','Fact6','Fact7','Fact8','Fact12','Fact14','Fact15','Fact16','Fact17','Fact18','DetalleMaterial15','Formato','Tipobusqueda','Busquedapro','Updatetabular','Cierrex','Reportest','Cierrez','Cierre2','Cierre3'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','Create2'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() 
	
	{
		Yii::import("application.modules.registro.models.Productos", true); 
		Yii::import("application.modules.usuario.models.Usuario", true); 
		Yii::import("application.modules.registro.models.Identificacion", true); 
		Yii::import("application.modules.registro.models.TipoCodigo", true); 
		Yii::import("application.modules.registro.models.TablaProducto",true);
		$uid=Yii::app()->user->id;
		$cliente=Usuario::model()->find('usuario_id='.$uid.'');
		$insid=$cliente->institucion_id;
		$fecha=date('j/m/Y');
		$cierre=Cierre::model()->find('institucion_id='.$insid.' and st_cierre=1 and fecha_cierre="'.$fecha.'" and tipo_cierre=1');
	
		if($cierrex){
			
			$this->redirect(array('admin'));
		}else{

        $estadoListData = CHtml::listData(Provincia::model()->findAll(),  'id_provincia','provincia');
   
		$model=new Factura;
		$modelPro=new Productos;
		$modelClie=new Clientes;
		$modelIde=new Identificacion;
		$tablaProducto=new TablaProducto;

	    $clientes=ListaImpuesto::model()->findAll();
	    $clientes2=Clientes::model()->findAll();
	    $clientes3=Productos::model()->findAll();
		
  		$modelClie->unsetAttributes();  // clear any default values
		if(isset($_GET['Clientes']))
			$modelClie->attributes=$_GET['Clientes'];
		
		$modelPro->unsetAttributes();  // clear any default values
		if(isset($_GET['Productos']))
			$modelPro->attributes=$_GET['Productos'];
			
		
		if(isset($_POST['Factura']))
		{
			
			$tablaProducto->attributes=$_POST['TablaProducto'];
			$tablaProducto->codigo_produc=$_POST['TablaProducto']['codigo_produc'];

			$arrayid=explode(",", $_POST['TablaProducto']['id_producto']); 
			$arraycodigo=explode(",", $_POST['TablaProducto']['codigo_produc']); 
			$arraynombre=explode(",", $_POST['TablaProducto']['nombre_produc']);
			$arraytotal=explode(",", $_POST['TablaProducto']['total']);
			$arraycantidad=explode(",", $_POST['TablaProducto']['cantidad']);
			$arraycosto=explode(",", $_POST['TablaProducto']['costo_producto']);
			$arraydescuento=explode(",", $_POST['TablaProducto']['descuento']);
			$arrayiv=explode(",", $_POST['TablaProducto']['iv']);
			//modales tabulares
			$arraytipoexoneracion=explode(",", $_POST['TablaProducto']['tipoexoneracion']);
			$arraydocumentoexo=explode(",", $_POST['TablaProducto']['documentoexo']);
			$arrayinstituexo=explode(",", $_POST['TablaProducto']['instituexo']);
			$arrayfechaexo=explode(",", $_POST['TablaProducto']['fechaexo']);
			$arrayimpuestoexo=explode(",", $_POST['TablaProducto']['impuestoexo']);
			$arrayporcentajeexo=explode(",", $_POST['TablaProducto']['porcentajeexo']);

			$model->attributes=$_POST['Factura'];	

		    $valido=$model->validate();
		    #################Guardado Tabular#####################
			/*if($valido){      
              	$model->save();
              	foreach ($arraycodigo as $key => $value) {
              		$tablaProducto=new TablaProducto;
              		$tablaProducto->id_factura=$model->id_factura;
              		$tablaProducto->codigo_produc=$arraycodigo[$key];
              		$tablaProducto->nombre_produc=$arraynombre[$key];
              		$tablaProducto->total=$arraytotal[$key];
              		$tablaProducto->cantidad=$arraycantidad[$key];
              		$tablaProducto->costo_producto=$arraycosto[$key];
              		$tablaProducto->descuento=$arraydescuento[$key]; 
              		$tablaProducto->iv=$arrayiv[$key]; 
              		//modal
              		$tablaProducto->tipoexoneracion=$arraytipoexoneracion[$key];
              		$tablaProducto->documentoexo=$arraydocumentoexo[$key];
              		$tablaProducto->instituexo=$arrayinstituexo[$key];
              		$tablaProducto->fechaexo=$arrayfechaexo[$key];
              		$tablaProducto->impuestoexo=$arrayimpuestoexo[$key];
              		$tablaProducto->porcentajeexo=$arrayporcentajeexo[$key];

              		$tablaProducto->save(false);             		
              	}*/ 	               
  #################Guardado Tabular#####################
            Yii::app()->user->setState('factura',$_POST['Factura']);
            Yii::app()->user->setState('arrayid',$arrayid);
            Yii::app()->user->setState('arraycodigo',$arraycodigo);
			Yii::app()->user->setState('arraynombre',$arraynombre);
			Yii::app()->user->setState('arraytotal',$arraytotal);
			Yii::app()->user->setState('arraycantidad',$arraycantidad);
			Yii::app()->user->setState('arraycosto',$arraycosto);
			Yii::app()->user->setState('arraydescuento',$arraydescuento);
			Yii::app()->user->setState('arrayiv',$arrayiv);
			Yii::app()->user->setState('arraytipoexoneracion',$arraytipoexoneracion);
			Yii::app()->user->setState('arraydocumentoexo',$arraydocumentoexo);
			Yii::app()->user->setState('arrayinstituexo',$arrayinstituexo);
			Yii::app()->user->setState('arrayfechaexo',$arrayfechaexo);
			Yii::app()->user->setState('arrayimpuestoexo',$arrayimpuestoexo);
			Yii::app()->user->setState('arrayporcentajeexo',$arrayporcentajeexo);
if( $model->total2!=""){
          $this->redirect(array('payment')); 
          }				
		}

		if(isset($_POST['Productos']))
		{
			$modelPro->attributes=$_POST['Productos'];
			
			$valido=$modelPro->validate();
		    
			if($valido){
               
				$modelPro->save();
				Yii::app()->user->setFlash('success','Registro Exitoso');

				if($modelPro->precio_finalc_id!=0){
					$this->redirect(array('//registro/productos/admin'));
				}

				
			}
			
		}
	if(isset($_POST['Clientes']))
		{
			$modelClie->attributes=$_POST['Clientes'];
			$valido=$modelClie->validate();
		    
			if($valido){
               
				$modelClie->save();
				Yii::app()->user->setFlash('success','Registro Exitoso');
				if($modelClie->identificacion!=""){
				$this->redirect(array('//registro/clientes/admin'));
				}
			}
			
		}
		/*if(isset($_POST['TablaProducto']))
		{
			

			$tablaProducto->attributes=$_POST['TablaProducto'];
			$valido=$tablaProducto->validate();
		}*/

		$this->render('create',array(
			'model'=>$model,'modelPro'=>$modelPro,'modelClie'=>$modelClie,'clientes'=>$clientes,'clientes2'=>$clientes2,'clientes3'=>$clientes3,'tablaProducto'=>$tablaProducto,
		'estadoListData'=>$estadoListData));
		}

	}

	public function actionSelectMunicipio() {
        $id = (int)$_POST['Clientes']['id_provincia'];
        //echo "<script>console.log(".$id.")</script>";
	
         $lista = CHtml::listData(Canton::model()->findAll('provincia_id =:provincia_id', array(':provincia_id'=>$id)), 'id_canton', 'canton');

          echo CHtml::tag( array());

        foreach ($lista as $valor=>$municipio) {
            echo CHtml::tag('option', array('value'=>$valor), CHtml::encode($municipio), true);
        }

       
    }

     public function actionSelectParroquia() {
        $id = (int) $_POST ['Clientes']['id_canton'];
      
        $lista = CHtml::listData(Distrito::model()->findAll('canton_id =:canton_id', array(':canton_id'=>$id)), 'id_distrito', 'distrito');

         echo CHtml::tag( array());

        foreach ($lista as $valor => $parroquia) {
            echo CHtml::tag('option', array('value'=>$valor), CHtml::encode($parroquia), true);
        }

       
    }

public function actionCreate2()
	{
		
		Yii::import("application.modules.registro.models.Productos", true); 
		//Yii::import("application.modules.registro.views.Productos", true); 
		//Yii::import("application.modules.registro.views.Productos._search", true); 
		$modelfac=new Factura;
		$producto=new Productos;
		$productos=Productos::model()->findAll();



		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Factura']))
		{
			$model->attributes=$_POST['Factura'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_factura));
		}

		$this->render('create2',array(
			'model'=>$modelfac,'producto'=>$productos
		));
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		Yii::import("application.modules.registro.models.Productos", true); 
		Yii::import("application.modules.registro.models.Identificacion", true); 
		Yii::import("application.modules.registro.models.TipoCodigo", true); 
		Yii::import("application.modules.registro.models.TablaProducto",true);
		Yii::import("application.modules.usuario.models.Usuario", true); 
		$uid=Yii::app()->user->id;
		$cliente=Usuario::model()->find('usuario_id='.$uid.'');
		$insid=$cliente->institucion_id;
		$fecha=date('j/m/Y');

		$cierre=Cierre::model()->find('institucion_id='.$insid.' and st_cierre=1 and fecha_cierre="'.$fecha.'"');
		
		if($cierrex){
			
			$this->redirect(array('admin'));
		}else{


        $estadoListData = CHtml::listData(Provincia::model()->findAll(),  'id_provincia','provincia');
   
		$model=new Factura;

		$Proforma=Proforma::model()->find('id_proforma='.$id);
        
		$model->id_codigo_producto=$Proforma->id_codigo_producto; 
		$model->id_cliente=$Proforma->id_cliente;
		$model->nombre_cliente=$Proforma->nombre_cliente;
		$model->id_moneda=$Proforma->id_moneda;
		$model->id_articulo=$Proforma->id_articulo;
		$model->credito=$Proforma->credito;
		$model->dias=$Proforma->dias;
		$model->fecha=$Proforma->fecha;
		$model->cantidad=$Proforma->cantidad;
		$model->precio_aimpu=$Proforma->precio_aimpu;
		$model->id_descuento=$Proforma->id_descuento;
		$model->contingencia=$Proforma->contingencia;
		$model->subtotal=$Proforma->subtotal;
		$model->iv=$Proforma->iv;
		$model->otros_impuestos_id=$Proforma->otros_impuestos_id;
		$model->total2=$Proforma->total2;
		$model->observaciones=$Proforma->observaciones;
		$model->fecha_conting=$Proforma->fecha_conting;
		$model->n_conting=$Proforma->n_conting;
		$model->razon_cont=$Proforma->razon_cont;

		$modelPro=new Productos;
		$modelClie=new Clientes;
		$modelIde=new Identificacion;
		$tablaProducto=new TablaProducto;

	    $clientes=ListaImpuesto::model()->findAll();
	    $clientes2=Clientes::model()->findAll();
	    $clientes3=Productos::model()->findAll();
		
  		$modelClie->unsetAttributes();  // clear any default values
		if(isset($_GET['Clientes']))
			$modelClie->attributes=$_GET['Clientes'];
		
		$modelPro->unsetAttributes();  // clear any default values
		if(isset($_GET['Productos']))
			$modelPro->attributes=$_GET['Productos'];
			
		
		if(isset($_POST['Factura']))
		{
			
			$tablaProducto->attributes=$_POST['TablaProducto'];
			$tablaProducto->codigo_produc=$_POST['TablaProducto']['codigo_produc'];

			$arrayid=explode(",", $_POST['TablaProducto']['id_producto']); 
			$arraycodigo=explode(",", $_POST['TablaProducto']['codigo_produc']); 
			$arraynombre=explode(",", $_POST['TablaProducto']['nombre_produc']);
			$arraytotal=explode(",", $_POST['TablaProducto']['total']);
			$arraycantidad=explode(",", $_POST['TablaProducto']['cantidad']);
			$arraycosto=explode(",", $_POST['TablaProducto']['costo_producto']);
			$arraydescuento=explode(",", $_POST['TablaProducto']['descuento']);
			$arrayiv=explode(",", $_POST['TablaProducto']['iv']);
			//modales tabulares
			$arraytipoexoneracion=explode(",", $_POST['TablaProducto']['tipoexoneracion']);
			$arraydocumentoexo=explode(",", $_POST['TablaProducto']['documentoexo']);
			$arrayinstituexo=explode(",", $_POST['TablaProducto']['instituexo']);
			$arrayfechaexo=explode(",", $_POST['TablaProducto']['fechaexo']);
			$arrayimpuestoexo=explode(",", $_POST['TablaProducto']['impuestoexo']);
			$arrayporcentajeexo=explode(",", $_POST['TablaProducto']['porcentajeexo']);

			$model->attributes=$_POST['Factura'];	

		    $valido=$model->validate();

			/*if($valido){      
              	$model->save();
              	foreach ($arraycodigo as $key => $value) {
              		$tablaProducto=new TablaProducto;
              		$tablaProducto->id_factura=$model->id_factura;
              		$tablaProducto->codigo_produc=$arraycodigo[$key];
              		$tablaProducto->nombre_produc=$arraynombre[$key];
              		$tablaProducto->total=$arraytotal[$key];
              		$tablaProducto->cantidad=$arraycantidad[$key];
              		$tablaProducto->costo_producto=$arraycosto[$key];
              		$tablaProducto->descuento=$arraydescuento[$key]; 
              		$tablaProducto->iv=$arrayiv[$key]; 
              		//modal
              		$tablaProducto->tipoexoneracion=$arraytipoexoneracion[$key];
              		$tablaProducto->documentoexo=$arraydocumentoexo[$key];
              		$tablaProducto->instituexo=$arrayinstituexo[$key];
              		$tablaProducto->fechaexo=$arrayfechaexo[$key];
              		$tablaProducto->impuestoexo=$arrayimpuestoexo[$key];
              		$tablaProducto->porcentajeexo=$arrayporcentajeexo[$key];

              		$tablaProducto->save(false);             		
              	}*/ 	               

            Yii::app()->user->setState('factura',$_POST['Factura']);
            Yii::app()->user->setState('arrayid',$arrayid);
            Yii::app()->user->setState('arraycodigo',$arraycodigo);
			Yii::app()->user->setState('arraynombre',$arraynombre);
			Yii::app()->user->setState('arraytotal',$arraytotal);
			Yii::app()->user->setState('arraycantidad',$arraycantidad);
			Yii::app()->user->setState('arraycosto',$arraycosto);
			Yii::app()->user->setState('arraydescuento',$arraydescuento);
			Yii::app()->user->setState('arrayiv',$arrayiv);
			Yii::app()->user->setState('arraytipoexoneracion',$arraytipoexoneracion);
			Yii::app()->user->setState('arraydocumentoexo',$arraydocumentoexo);
			Yii::app()->user->setState('arrayinstituexo',$arrayinstituexo);
			Yii::app()->user->setState('arrayfechaexo',$arrayfechaexo);
			Yii::app()->user->setState('arrayimpuestoexo',$arrayimpuestoexo);
			Yii::app()->user->setState('arrayporcentajeexo',$arrayporcentajeexo);
if( $model->total2!=""){
          $this->redirect(array('payment')); 
          }				
		}

		if(isset($_POST['Productos']))
		{
			$modelPro->attributes=$_POST['Productos'];
			
			$valido=$modelPro->validate();
		    
			if($valido){
               
				$modelPro->save();
				Yii::app()->user->setFlash('success','Registro Exitoso');

				if($modelPro->precio_finalc_id!=0){
					$this->redirect(array('//registro/productos/admin'));
				}

				
			}
			
		}
	if(isset($_POST['Clientes']))
		{
			$modelClie->attributes=$_POST['Clientes'];
			$valido=$modelClie->validate();
		    
			if($valido){
               
				$modelClie->save();
				Yii::app()->user->setFlash('success','Registro Exitoso');
				if($modelClie->identificacion!=""){
				$this->redirect(array('//registro/clientes/admin'));
				}
			}
			
		}
		/*if(isset($_POST['TablaProducto']))
		{
			

			$tablaProducto->attributes=$_POST['TablaProducto'];
			$valido=$tablaProducto->validate();
		}*/
		$this->render('create',array(
			'model'=>$model,'modelPro'=>$modelPro,'modelClie'=>$modelClie,'clientes'=>$clientes,'clientes2'=>$clientes2,'clientes3'=>$clientes3,'tablaProducto'=>$tablaProducto,
		'estadoListData'=>$estadoListData));
	}
}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	public function actionCierrex($id)
	{	
		date_default_timezone_set('America/Costa_Rica'); 
		Yii::import("application.modules.usuario.models.Usuario", true); 
		$modcierre=new Cierre();
		$usuarioid=$id;
		$usuario=Usuario::model()->find('usuario_id='.$id);
		$empreid=$usuario->institucion_id;
		$fechac=date('j/n/Y')."%";
		$fecha=date('j/n/Y');
		$tipo=$_GET['tipo'];
		$facturas=Factura::model()->findAll('fecha LIKE "'.$fechac.'" and institucion_id='.$id.' and id_usuario='.$id);

       	foreach ($facturas as $key => $value) {
			$total+=$facturas[$key]->total2;
		}
		//Cierre tipo 1 Cierre Z
		$cierre=Cierre::model()->find('institucion_id='.$empreid.' and tipo_cierre=1 and st_cierre=1 and fecha_cierre="'.$fecha.'"');
		
		if($cierre){
		$this->redirect(array('admin'));

		}else{
			$modcierre->institucion_id=$empreid;
			$modcierre->id_usuario=$usuarioid;
			
			if($tipo=='Z'){
					$modcierre->fecha_cierre=date('j/n/Y');
					$modcierre->total_ins=$total;
					$modcierre->tipo_cierre=1;
					$modcierre->save();
			$this->redirect(array('cierrez&empre='.$empreid));
			}else{
					$cierrex=Cierre::model()->findAll('institucion_id='.$empreid.' and tipo_cierre=2  and fecha_cierre like "'.$fechac.'" and id_usuario='.$id);
					$tamaño= count($cierrex); 
					$totalp=0;
					if($tamaño==0){
						$totalp=$total;
						$modcierre->total_ins=$totalp;
					}else if($tamaño>0){
							for($i=0;$i<$tamaño;$i++){
							$totalp+=$cierrex[$i]->total_ins;				
							}

							$totalp=$total-$totalp;
							$modcierre->total_ins=$totalp;
		
					}		
			$modcierre->fecha_cierre=date('j/n/Y H:i:s');
			$modcierre->tipo_cierre=2;
			$modcierre->save();
			$this->redirect(array('cierre3&empre='.$empreid));
			}
			
		
			}
		
		
		

	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Factura');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	public function actionCierrez($empre)
	{	
		
		Yii::import("application.modules.usuario.models.Usuario", true); 
		$fechac=date('j/n/Y')."%";
		$facturasc=Factura::model()->findAll('fecha LIKE "'.$fechac.'" and institucion_id='.$empre.' and id_moneda=1');
		$facturass=Factura::model()->findAll('fecha LIKE "'.$fechac.'" and institucion_id='.$empre.' and id_moneda=2');
		$this->render('cierre',array('facturasc'=>$facturasc,'facturass'=>$facturass));
	}
	public function actionCierre2($empre)
	{	
		
		Yii::import("application.modules.usuario.models.Usuario", true); 
		$fechac=date('j/n/Y')."%";
		$facturasc=Factura::model()->findAll('fecha LIKE "'.$fechac.'" and institucion_id='.$empre.' and id_moneda=1');
		$facturass=Factura::model()->findAll('fecha LIKE "'.$fechac.'" and institucion_id='.$empre.' and id_moneda=2');
		$this->render('cierre',array('facturasc'=>$facturasc,'facturass'=>$facturass));
	}
	public function actionCierre3($empre)
	{	
		
		Yii::import("application.modules.usuario.models.Usuario", true); 
		$fechac=date('j/n/Y')."%";
		$cierre=Cierre::model()->findAll('fecha_cierre LIKE "'.$fechac.'" and institucion_id='.$empre);
		$facturasc=Factura::model()->findAll('fecha LIKE "'.$fechac.'" and institucion_id='.$empre.' and id_moneda=1');
		$facturass=Factura::model()->findAll('fecha LIKE "'.$fechac.'" and institucion_id='.$empre.' and id_moneda=2');
		$this->render('cierrex',array('facturasc'=>$facturasc,'facturass'=>$facturass,'cierre'=>$cierre));
	}
	public function actionPayment()
	{	
		$this->layout ='//layouts/payview';
		Yii::import("application.modules.usuario.models.Usuario", true);
		$userid=Yii::app()->user->id;
		$usuario=Usuario::model()->find('usuario_id='.$userid.'');
		$insid=$usuario->institucion_id;

		
		$modelfac=new Factura;
		$tablaProducto=new TablaProducto;

		$modelo=Yii::app()->user->getState('factura');
		$total=$modelo['total2'];

		if(isset($_POST['Factura']))
		{
/*VALORES QUE VIENEN POR SESSION PARA GUARDAR EN MODELO FACTURA! */
		$modelfac->attributes=$_POST['Factura'];
		$modelfac->id_codigo_producto=$modelo['id_codigo_producto'];
		$modelfac->id_cliente=$modelo['id_cliente'];
		$modelfac->institucion_id=$insid;
		$modelfac->nombre_cliente=$modelo['nombre_cliente'];
		$modelfac->id_moneda=$modelo['id_moneda'];
		$modelfac->id_articulo=$model['id_articulo'];
		$modelfac->credito=$modelo['credito'];
		$modelfac->dias=$modelo['dias'];
		$modelfac->fecha=$modelo['fecha'];
		$modelfac->cantidad=$modelo['cantidad'];
		$modelfac->precio_aimpu=$modelo['precio_aimpu'];
		$modelfac->id_descuento=$modelo['id_descuento'];
		$modelfac->contingencia=$modelo['contingencia'];
		$modelfac->subtotal=$modelo['subtotal'];
		$modelfac->iv=$modelo['iv'];
		$modelfac->otros_impuestos_id=$modelo['otros_impuestos_id'];
		$modelfac->total2=$modelo['total2'];
		$modelfac->observaciones=$modelo['observaciones'];
		$modelfac->fecha_conting=$modelo['fecha_conting'];
		$modelfac->n_conting=$modelo['n_conting'];
		$modelfac->razon_cont=$modelo['razon_cont'];
		$modelfac->id_usuario=$userid;
		$modelfac->save();
  /*VALORES QUE VIENEN POR SESSION PARA GUARDAR EN MODELO TABLAPRODUCTO! */
          $arrayid=Yii::app()->user->getState('arrayid');
  		  $arraycodigo=Yii::app()->user->getState('arraycodigo');
		  $arraynombre=Yii::app()->user->getState('arraynombre');
		  $arraytotal=Yii::app()->user->getState('arraytotal');
		  $arraycantidad=Yii::app()->user->getState('arraycantidad');
		  $arraycosto=Yii::app()->user->getState('arraycosto');
		  $arraydescuento=Yii::app()->user->getState('arraydescuento');
		  $arrayiv=Yii::app()->user->getState('arrayiv');
		  $arraytipoexoneracion=Yii::app()->user->getState('arraytipoexoneracion');
		  $arraydocumentoexo=Yii::app()->user->getState('arraydocumentoexo');
		  $arrayinstituexo=Yii::app()->user->getState('arrayinstituexo');
		  $arrayfechaexo=Yii::app()->user->getState('arrayfechaexo');
		  $arrayimpuestoexo=Yii::app()->user->getState('arrayimpuestoexo');
		  $arrayporcentajeexo=Yii::app()->user->getState('arrayporcentajeexo');
  /*BUCLE PARA GUARDAR TODOS LOS PRODUCTOS EN  TABLAPRODUCTO! */

			foreach ($arraycodigo as $key => $value) {
              		$tablaProducto=new TablaProducto;         		
              		$tablaProducto->id_factura=$modelfac->id_factura;
              		$tablaProducto->id_cliente=$modelfac->id_cliente;
              		$tablaProducto->id_producto=$arrayid[$key];
              		$tablaProducto->codigo_produc=$arraycodigo[$key];
              		$tablaProducto->nombre_produc=$arraynombre[$key];
              		$tablaProducto->total=$arraytotal[$key];
              		$tablaProducto->cantidad=$arraycantidad[$key];
              		$tablaProducto->costo_producto=$arraycosto[$key];
              		$tablaProducto->descuento=$arraydescuento[$key]; 
              		$tablaProducto->iv=$arrayiv[$key]; 
              		//modal
              		$tablaProducto->tipoexoneracion=$arraytipoexoneracion[$key];
              		$tablaProducto->documentoexo=$arraydocumentoexo[$key];
              		$tablaProducto->instituexo=$arrayinstituexo[$key];
              		$tablaProducto->fechaexo=$arrayfechaexo[$key];
              		$tablaProducto->impuestoexo=$arrayimpuestoexo[$key];
              		$tablaProducto->porcentajeexo=$arrayporcentajeexo[$key];

              		$tablaProducto->save(false); 

              	}


			if($modelfac->save()){
					Yii::app()->user->setState('idruta',$modelfac->id_factura);  
				$this->redirect(array('Fact'));
				}

		}


		$this->render('payment',array(
			'model'=>$modelfac,'total'=>$total
		));
	}

	public function actionFact() 
	{
		$id=Yii::app()->user->getState('idruta');
		

		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');

		$this->render('Fact',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'contribuyente'=>$contribuyente	
						));
	}
	public function actionFact2()
	{
		$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Factura", true); 
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$this->render('Fact2',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'contribuyente'=>$contribuyente
				));
	}
	public function actionFact3()
	{$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Factura", true); 
		$factura=Factura::model()->findByPk($id);
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$this->render('Fact3',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos));
	}
	public function actionFact4()
	{	$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$impresion=Impresion::model()->find('id=1');
		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Fact4',array('id'=>$id,
			'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'impresion'=>$impresion,
					'contribuyente'=>$contribuyente

	));
	}
	public function actionFact5()
	{
		$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');

		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Fact5',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}
	public function actionFact6()
	{	$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		
		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Fact6',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente
	));
	}
	public function actionFact7()
	{	$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		
		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Fact7',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}
	public function actionFact8()
	{	$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		
		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Fact8',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}
	public function actionFact12()
	{	$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		
		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Fact12',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}
	public function actionFact14()
	{	
		$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		
		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Fact14',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}
	public function actionFact15()
	{	$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		
		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Fact15',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}
	public function actionFact16()
	{	$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		
		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Fact16',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}
	public function actionFact17()
	{	$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		
		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Fact17',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}
		public function actionFact18()
	{	$id=Yii::app()->user->getState('idruta');
		Yii::import("application.modules.facturas.models.Contribuyente", true); 
		$contribuyente=Contribuyente::model()->find();
		$factura=Factura::model()->findByPk($id);
		$fact=$factura->factura;
		$idcli=intval($factura->id_cliente);
		$cliente=Clientes::model()->find('id_cliente='.$idcli.'');
		$idpro=$cliente->id_provincia;
		$idcan=$cliente->id_canton;
		$iddis=$cliente->id_distrito;
		$idmo=intval($factura->id_moneda);
		$moneda=Moneda::model()->find('id_moneda='.$idmo.'');
		$productos=TablaProducto::model()->findAll('id_factura='.$id.'');
		$provincia=Provincia::model()->find('id_provincia='.$idpro.'');
		$canton=Canton::model()->find('id_canton='.$idcan.'');
		$distrito=Distrito::model()->find('id_distrito='.$iddis.'');
		
		Yii::import("application.modules.facturas.models.Factura", true); 
		$this->render('Fact18',array('id'=>$id,
					'factura'=>$factura,
					'idcli'=>$idcli,
					'cliente'=>$cliente,
					'idmo'=>$idmo,
					'moneda'=>$moneda,
					'productos'=>$productos,
					'provincia'=>$provincia,
					'canton'=>$canton,
					'distrito'=>$distrito,
					'contribuyente'=>$contribuyente


	));
	}



	

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{	

		Yii::import("application.modules.usuario.models.Usuario", true); 
		$userid=Yii::app()->user->id;
		$empresa=Usuario::model()->find('usuario_id='.$userid);
		$empreid=$empresa->institucion_id;		
		$fecha=date('j/n/Y');	
		$cierre=Cierre::model()->find('institucion_id='.$empreid.' and fecha_cierre="'.$fecha.'" and tipo_cierre=1');	
		
		if($cierre){		
		if($cierre->fecha_cierre!=$fecha){			
			$cierre->st_cierre=0;
			$cierre->save();
			$cierreX=true;
		}else{
			$cierreX=true;
		}	  
		}else{
			$cierreX=false;
		}
		$form=Impresion::model()->find('institucion_id='.$empreid);
		$ruta=$form->ruta;
		$vista=$form->vista;
		$model=new Factura('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Factura']))
			$model->attributes=$_GET['Factura'];
		$nopago=Factura::model()->findAll('factura="" and dias="0"');

		$this->render('admin',array(
			'model'=>$model,
			'form'=>$form,
			'ruta'=>$ruta,
			'vista'=>$vista,
			'cierreX'=>$cierreX
		));
	}
	public function actionFormato()
	{
	$model=new Formatos();
	$modelfac=new Factura();
	$impresion=new Impresion();
	if(isset($_POST['Impresion']))
		{
			$ruta=$_POST['Impresion']['ruta'];
			$formato=$_POST['Impresion']['formato'];
			$vista=$_POST['Impresion']['vista'];
			$rutapr=$_POST['Impresion']['rutapr'];
			$vistapr=$_POST['Impresion']['vistapr'];
			$formatopr=$_POST['Impresion']['formato'];


			if($formato==1){
				$formato="Tiquete";
			}else if($formato==0){
				$formato="Factura";
			}
			$sql="UPDATE impresion SET ruta='".$ruta."',formato='".$formato."',vista='".$vista."',rutapr='".$rutapr."',vistapr='".$vistapr."',formatopr='".$formatopr."' WHERE id=1";
			$comando = Yii::app()->db->createCommand($sql);
            $comando->execute();
            $this->redirect('index.php?r=facturas/factura/formato&id=1');
             
		}

		$this->render('formato',array(
			'model'=>$model,
			'modelfac'=>$modelfac,
			'impresion'=>$impresion
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Factura the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Factura::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Factura $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='factura-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	public function actionDetalleMaterial()
	{
	
		$clientes=ListaImpuesto::model()->findByPk($_POST['id_cliente']);

if($_POST['prueba']=='0')
{
if($clientes->impuesto_nombre=='Impuesto de Ventas'){
		$tabla='
<table class="table table-responsive table-striped" >
 				<tr style="color:#22C7FC;background-color: transparent;font-weight: bold;  ">
                           <td >ID</td>
                            <td colspan="2">Nombre</td>
                            <td>Porcentaje</td>
                            <td></td>
                          </tr>
		    <tr id=1>
		      <td>'.$clientes->id_impuesto.'</td>
		      <td colspan="2">'.$clientes->impuesto_nombre.'</td>
		      <td>'.$clientes->monto.'</td>
		      <td > <a href=# onclick="eliminarp('.$value->id_impuesto.')"><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="eliminar" ></span></a></td>
		    </tr>
		
                     </table> 
                       ';	
	}

if($clientes->impuesto_nombre=='Impuesto de Servicio'){

?>
<script>
swal('Atenci贸n!','Este impuesto solo puede ser agregado a un servicio','warning'); 
</script>

 <?php

$tabla='  <table class="table table-responsive table-striped"  >
   <tr style="color:#22C7FC;background-color: transparent;font-weight: bold;  ">
 			<td>ID</td>
            <td colspan="2">Nombre</td>
            <td>Porcentaje</td>
   </tr>
                           </table> 
                           <br>';

}	

}
if($_POST['prueba2']=='1')
{
if($clientes->impuesto_nombre=='Impuesto de Servicio'){
		$tabla='
<table class="table table-responsive table-striped" >
 				<tr style="color:#22C7FC;background-color: transparent;font-weight: bold;  ">
                           <td >ID</td>
                            <td colspan="2">Nombre</td>
                            <td>Porcentaje</td>
                            <td></td>
                          </tr>
		    <tr id=1>
		      <td>'.$clientes->id_impuesto.'</td>
		      <td colspan="2">'.$clientes->impuesto_nombre.'</td>
		      <td>'.$clientes->monto.'</td>
		      <td > <a href=# onclick="eliminarp('.$value->id_impuesto.')"><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="eliminar" ></span></a></td>
		    </tr>
		
                     </table> 
                       ';	
	}

if($clientes->impuesto_nombre=='Impuesto de Ventas'){

?>
<script>
swal('Atenci贸n!','Este impuesto solo puede ser agregado a un producto','warning');
</script>

 <?php

$tabla=' <table class="table table-responsive table-striped"  >
   <tr style="color:#22C7FC;background-color: transparent;font-weight: bold;  ">
 			<td>ID</td>
            <td colspan="2">Nombre</td>
            <td>Porcentaje</td>
   </tr>
                           </table> 
                           <br>';

}	

}
                   
echo $tabla;
		
	}
	public function actionDetalleMaterial2()
	{

		$clientes=Clientes::model()->findByPk($_POST['id_cliente']);
		//echo CJSON::encode($clientes);


		echo CJSON::encode($clientes);
		
	}

		public function actionDetalleMaterial3()
	{ 
	Yii::import("application.modules.registro.models.Productos", true); 
		$clientes=Productos::model()->findByPk($_POST['id_cliente']);
		//echo CJSON::encode($clientes);


		echo CJSON::encode($clientes);
		
	}
	public function actionDetalleMaterial4()
	{
	Yii::import("application.modules.registro.models.Productos", true); 

if($_POST['letra']==1){
$clientes=Productos::model()->find('codigo="'.$_POST['id_cliente'].'"');
}else{
	$clientes=Productos::model()->find('codigo='.$_POST['id_cliente']);
}


		echo CJSON::encode($clientes);
		
	}
	public function actionDetalleMaterial5()
	{

		$clientes=Clientes::model()->find('identificacion='.$_POST['id_cliente']);
		//echo CJSON::encode($clientes);

		if($clientes){
			$esta=1;
		}else{
			$esta=0;
		}

		echo CJSON::encode($esta); 
		
	}	

public function actionDetalleMaterial15()
	{


$subtotaltabular=($_POST['precio']*$_POST['cantidad']);

$subtotalfin= ($_POST['subtotal']-$subtotaltabular);	
	
		echo CJSON::encode($subtotalfin);
		
	}
public function actionTipobusqueda()
	{

$clientes=Clientes::model()->find('identificacion='.$_POST['id_cliente']);

if(!$clientes){
$clientes=1;
}
		echo CJSON::encode($clientes);

	}
public function actionBusquedapro()
	{
$clientes=Productos::model()->find('codigo="'.$_POST['id_cliente'].'"');
if(!$clientes){
$clientes=1;
}
echo CJSON::encode($clientes);
}

public function actionUpdatetabular()
{

			$clientes=TablaProductoProforma::model()->findAll('id_factura='.$_POST['id_cliente']);
			
			if($clientes){
			
				}
			else{
				$clientes=2;
			}

			echo CJSON::encode($clientes);
}	

public function actionReportest()
{
$id=$_POST['id'];
Yii::app()->user->setState('idruta',$id);  
}


}

