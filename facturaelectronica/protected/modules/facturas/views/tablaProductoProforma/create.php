<?php
/* @var $this TablaProductoProformaController */
/* @var $model TablaProductoProforma */

$this->breadcrumbs=array(
	'Tabla Producto Proformas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TablaProductoProforma', 'url'=>array('index')),
	array('label'=>'Manage TablaProductoProforma', 'url'=>array('admin')),
);
?>

<h1>Create TablaProductoProforma</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>