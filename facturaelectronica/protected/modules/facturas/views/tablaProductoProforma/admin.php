<?php
/* @var $this TablaProductoProformaController */
/* @var $model TablaProductoProforma */

$this->breadcrumbs=array(
	'Tabla Producto Proformas'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List TablaProductoProforma', 'url'=>array('index')),
	array('label'=>'Create TablaProductoProforma', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#tabla-producto-proforma-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Tabla Producto Proformas</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tabla-producto-proforma-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'id_factura',
		'id_cliente',
		'id_producto',
		'codigo_produc',
		'nombre_produc',
		/*
		'costo_producto',
		'cantidad',
		'total',
		'descuento',
		'iv',
		'documentoexo',
		'tipoexoneracion',
		'instituexo',
		'fechaexo',
		'impuestoexo',
		'porcentajeexo',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
