<?php
/* @var $this TablaProductoProformaController */
/* @var $data TablaProductoProforma */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_factura')); ?>:</b>
	<?php echo CHtml::encode($data->id_factura); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_cliente')); ?>:</b>
	<?php echo CHtml::encode($data->id_cliente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_producto')); ?>:</b>
	<?php echo CHtml::encode($data->id_producto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo_produc')); ?>:</b>
	<?php echo CHtml::encode($data->codigo_produc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_produc')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_produc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('costo_producto')); ?>:</b>
	<?php echo CHtml::encode($data->costo_producto); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total')); ?>:</b>
	<?php echo CHtml::encode($data->total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descuento')); ?>:</b>
	<?php echo CHtml::encode($data->descuento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iv')); ?>:</b>
	<?php echo CHtml::encode($data->iv); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('documentoexo')); ?>:</b>
	<?php echo CHtml::encode($data->documentoexo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipoexoneracion')); ?>:</b>
	<?php echo CHtml::encode($data->tipoexoneracion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('instituexo')); ?>:</b>
	<?php echo CHtml::encode($data->instituexo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaexo')); ?>:</b>
	<?php echo CHtml::encode($data->fechaexo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('impuestoexo')); ?>:</b>
	<?php echo CHtml::encode($data->impuestoexo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('porcentajeexo')); ?>:</b>
	<?php echo CHtml::encode($data->porcentajeexo); ?>
	<br />

	*/ ?>

</div>