<?php
/* @var $this TablaProductoProformaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tabla Producto Proformas',
);

$this->menu=array(
	array('label'=>'Create TablaProductoProforma', 'url'=>array('create')),
	array('label'=>'Manage TablaProductoProforma', 'url'=>array('admin')),
);
?>

<h1>Tabla Producto Proformas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
