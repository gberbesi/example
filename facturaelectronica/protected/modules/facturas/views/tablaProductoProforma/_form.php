<?php
/* @var $this TablaProductoProformaController */
/* @var $model TablaProductoProforma */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tabla-producto-proforma-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_factura'); ?>
		<?php echo $form->textField($model,'id_factura'); ?>
		<?php echo $form->error($model,'id_factura'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_cliente'); ?>
		<?php echo $form->textField($model,'id_cliente'); ?>
		<?php echo $form->error($model,'id_cliente'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_producto'); ?>
		<?php echo $form->textField($model,'id_producto'); ?>
		<?php echo $form->error($model,'id_producto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'codigo_produc'); ?>
		<?php echo $form->textField($model,'codigo_produc',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'codigo_produc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nombre_produc'); ?>
		<?php echo $form->textArea($model,'nombre_produc',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'nombre_produc'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'costo_producto'); ?>
		<?php echo $form->textField($model,'costo_producto'); ?>
		<?php echo $form->error($model,'costo_producto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cantidad'); ?>
		<?php echo $form->textField($model,'cantidad'); ?>
		<?php echo $form->error($model,'cantidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total'); ?>
		<?php echo $form->textField($model,'total'); ?>
		<?php echo $form->error($model,'total'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descuento'); ?>
		<?php echo $form->textField($model,'descuento'); ?>
		<?php echo $form->error($model,'descuento'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'iv'); ?>
		<?php echo $form->textField($model,'iv'); ?>
		<?php echo $form->error($model,'iv'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'documentoexo'); ?>
		<?php echo $form->textField($model,'documentoexo'); ?>
		<?php echo $form->error($model,'documentoexo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipoexoneracion'); ?>
		<?php echo $form->textField($model,'tipoexoneracion',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'tipoexoneracion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'instituexo'); ?>
		<?php echo $form->textField($model,'instituexo',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'instituexo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fechaexo'); ?>
		<?php echo $form->textField($model,'fechaexo'); ?>
		<?php echo $form->error($model,'fechaexo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'impuestoexo'); ?>
		<?php echo $form->textField($model,'impuestoexo'); ?>
		<?php echo $form->error($model,'impuestoexo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'porcentajeexo'); ?>
		<?php echo $form->textField($model,'porcentajeexo'); ?>
		<?php echo $form->error($model,'porcentajeexo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->