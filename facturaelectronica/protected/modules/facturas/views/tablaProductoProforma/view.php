<?php
/* @var $this TablaProductoProformaController */
/* @var $model TablaProductoProforma */

$this->breadcrumbs=array(
	'Tabla Producto Proformas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TablaProductoProforma', 'url'=>array('index')),
	array('label'=>'Create TablaProductoProforma', 'url'=>array('create')),
	array('label'=>'Update TablaProductoProforma', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TablaProductoProforma', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TablaProductoProforma', 'url'=>array('admin')),
);
?>

<h1>View TablaProductoProforma #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_factura',
		'id_cliente',
		'id_producto',
		'codigo_produc',
		'nombre_produc',
		'costo_producto',
		'cantidad',
		'total',
		'descuento',
		'iv',
		'documentoexo',
		'tipoexoneracion',
		'instituexo',
		'fechaexo',
		'impuestoexo',
		'porcentajeexo',
	),
)); ?>
