<?php
/* @var $this TablaProductoProformaController */
/* @var $model TablaProductoProforma */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_factura'); ?>
		<?php echo $form->textField($model,'id_factura'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_cliente'); ?>
		<?php echo $form->textField($model,'id_cliente'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_producto'); ?>
		<?php echo $form->textField($model,'id_producto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'codigo_produc'); ?>
		<?php echo $form->textField($model,'codigo_produc',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre_produc'); ?>
		<?php echo $form->textArea($model,'nombre_produc',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'costo_producto'); ?>
		<?php echo $form->textField($model,'costo_producto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cantidad'); ?>
		<?php echo $form->textField($model,'cantidad'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total'); ?>
		<?php echo $form->textField($model,'total'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descuento'); ?>
		<?php echo $form->textField($model,'descuento'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'iv'); ?>
		<?php echo $form->textField($model,'iv'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'documentoexo'); ?>
		<?php echo $form->textField($model,'documentoexo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tipoexoneracion'); ?>
		<?php echo $form->textField($model,'tipoexoneracion',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'instituexo'); ?>
		<?php echo $form->textField($model,'instituexo',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fechaexo'); ?>
		<?php echo $form->textField($model,'fechaexo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'impuestoexo'); ?>
		<?php echo $form->textField($model,'impuestoexo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'porcentajeexo'); ?>
		<?php echo $form->textField($model,'porcentajeexo'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->