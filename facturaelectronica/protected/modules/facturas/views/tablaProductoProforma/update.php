<?php
/* @var $this TablaProductoProformaController */
/* @var $model TablaProductoProforma */

$this->breadcrumbs=array(
	'Tabla Producto Proformas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TablaProductoProforma', 'url'=>array('index')),
	array('label'=>'Create TablaProductoProforma', 'url'=>array('create')),
	array('label'=>'View TablaProductoProforma', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TablaProductoProforma', 'url'=>array('admin')),
);
?>

<h1>Update TablaProductoProforma <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>