
<?php
/* @var $this OpermvController */
/* @var $model Opermv */
'<pre>';

$this->breadcrumbs=array(
    'Opermvs'=>array('index'),
    'Manage',
);

$this->menu=array(
    array('label'=>'List Opermv', 'url'=>array('index')),
    array('label'=>'Create Opermv', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#opermv-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
<style>
    .grid-view table.items th a{

        color:black;
    }

</style>

<h1>Facturas</h1>
<body style="color:black">


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:block">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->
<a href="/facutra_ultima/index.php?r=facturas/opermv/create" ><button align="right" class="btn btn-primary " style="float: right;">Nuevo</button></a>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'opermv-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(

        'id_operti',
        'id_opermv',
         array(
                'name'=>'id_articulo',
                'value'=>'($data->idArticulo->nombre)',
                'filter'=>true,

                ),
           array(
                'name'=>'Cedula',
                'value'=>'($data->idOperti->idCliente->doc_identificacion)',
                'filter'=>true
        ),
               array(
                'name'=>'Cliente',
                'value'=>'($data->idOperti->idCliente->nombre)',
                'filter'=>true,
        ),
        
          array(
                'name'=>'fechayhora',
                
                'value'=>'Yii::app()->dateFormatter->format("dd/mm/yyyy",strtotime($data->fechayhora))',
                'filter'=>true
        ),
          array(
                'name'=>'Tipo Moneda',
                
                'value'=>'($data->idOperti->cobroses[0]->idTipoMoneda->simbolo)',
                'filter'=>true
        ),
          array(
                'name'=>'Tipo Pago',
                
                'value'=>'($data->idOperti->cobroses[0]->idTipoFormaDePago->descripcion)',
                'filter'=>true
        ),
          array(
                'name'=>'Monto',
                
                'value'=>'($data->idOperti->cobroses[0]->monto)',
                'filter'=>true
        ),
          
         

              
          /*
          array(
                'name'=>'Saldo',
                
                'value'=>'($data->idOperti->cobroses[0]->idTipoFormaDePago->descripcion)',
                'filter'=>true
        ),
       array(
                'name'=>'Estado De hacienda',
                
                'value'=>'($data->idOperti->cobroses[0]->idTipoFormaDePago->descripcion)',
                'filter'=>true
        ),
       array(
                'name'=>'Estado Correo',
                
                'value'=>'($data->idOperti->cobroses[0]->idTipoFormaDePago->descripcion)',
                'filter'=>true
        ),
                 
        'id_almacen',
        'id_vendedor',
        'costounit',
        
        'preciooriginal',
        'dsctounit',
        'preciofin',
        'dsctoprc',
        'dsctomtolinea',
        'cantidad',
        'montoneto',
        'totimpuesto',
        'notas',
        
        'cant_devuelta',
        'cant_facturada',
        'es_materia_prima',
        'id_articulo_padre',
        'id_unidad',
       */
        array(
            'class'=>'CButtonColumn',
        ),
    ),

)); ?>
</body>

<script src="themes/gentelella/vendors/jquery/dist/jquery.min.js"></script>