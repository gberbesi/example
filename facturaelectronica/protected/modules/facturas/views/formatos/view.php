
<?php
/* @var $this OpermvController */
/* @var $model Opermv */

$this->breadcrumbs=array(
    'Opermvs'=>array('index'),
    $model->id_opermv,
);

$this->menu=array(
    array('label'=>'List Opermv', 'url'=>array('index')),
    array('label'=>'Create Opermv', 'url'=>array('create')),
    array('label'=>'Update Opermv', 'url'=>array('update', 'id'=>$model->id_opermv)),
    array('label'=>'Delete Opermv', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_opermv),'confirm'=>'Are you sure you want to delete this item?')),
    array('label'=>'Manage Opermv', 'url'=>array('admin')),
);
?>

<h1>View Opermv #<?php echo $model->id_opermv; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data'=>$model,
    'attributes'=>array(
        'id_operti',
        'id_opermv',
        'id_articulo',
        'id_almacen',
        'id_vendedor',
        'costounit',
        'preciooriginal',
        'dsctounit',
        'preciofin',
        'dsctoprc',
        'dsctomtolinea',
        'cantidad',
        'montoneto',
        'totimpuesto',
        'notas',
        'fechayhora',
        'cant_devuelta',
        'cant_facturada',
        'es_materia_prima',
        'id_articulo_padre',
        'id_unidad',
    ),
)); ?>
<script src="themes/gentelella/vendors/jquery/dist/jquery.min.js"></script>