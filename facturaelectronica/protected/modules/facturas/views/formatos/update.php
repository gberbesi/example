
<?php
/* @var $this OpermvController */
/* @var $model Opermv */

$this->breadcrumbs=array(
    'Opermvs'=>array('index'),
    $model->id_opermv=>array('view','id'=>$model->id_opermv),
    'Update',
);

$this->menu=array(
    array('label'=>'List Opermv', 'url'=>array('index')),
    array('label'=>'Create Opermv', 'url'=>array('create')),
    array('label'=>'View Opermv', 'url'=>array('view', 'id'=>$model->id_opermv)),
    array('label'=>'Manage Opermv', 'url'=>array('admin')),
);
?>

<h1>Update Opermv <?php echo $model->id_opermv; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>