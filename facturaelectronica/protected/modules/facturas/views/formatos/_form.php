
<?php
/* @var $this OpermvController */
/* @var $model Opermv */
/* @var $form CActiveForm */
?>
<style>
    
 li .active {
background-color: red;
  
}


</style>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'opermv-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>
    
    <p class="note">Los campos con <span class="required">*</span> son obligatorios.</p>

    <p>BREADSCRUM AQUI </p>

    <?php echo $form->errorSummary($model); ?>
    

<!--
    <div class="row">
        <?php echo $form->labelEx($model,'id_operti'); ?>
        <?php echo $form->textField($model,'id_operti'); ?>
        <?php echo $form->error($model,'id_operti'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model,'id_articulo'); ?>
        <?php echo $form->textField($model,'id_articulo'); ?>
        <?php echo $form->error($model,'id_articulo'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'id_almacen'); ?>
        <?php echo $form->textField($model,'id_almacen'); ?>
        <?php echo $form->error($model,'id_almacen'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'id_vendedor'); ?>
        <?php echo $form->textField($model,'id_vendedor'); ?>
        <?php echo $form->error($model,'id_vendedor'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'costounit'); ?>
        <?php echo $form->textField($model,'costounit'); ?>
        <?php echo $form->error($model,'costounit'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'preciooriginal'); ?>
        <?php echo $form->textField($model,'preciooriginal'); ?>
        <?php echo $form->error($model,'preciooriginal'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'dsctounit'); ?>
        <?php echo $form->textField($model,'dsctounit'); ?>
        <?php echo $form->error($model,'dsctounit'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'preciofin'); ?>
        <?php echo $form->textField($model,'preciofin'); ?>
        <?php echo $form->error($model,'preciofin'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'dsctoprc'); ?>
        <?php echo $form->textField($model,'dsctoprc'); ?>
        <?php echo $form->error($model,'dsctoprc'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'dsctomtolinea'); ?>
        <?php echo $form->textField($model,'dsctomtolinea'); ?>
        <?php echo $form->error($model,'dsctomtolinea'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'cantidad'); ?>
        <?php echo $form->textField($model,'cantidad'); ?>
        <?php echo $form->error($model,'cantidad'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'montoneto'); ?>
        <?php echo $form->textField($model,'montoneto'); ?>
        <?php echo $form->error($model,'montoneto'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'totimpuesto'); ?>
        <?php echo $form->textField($model,'totimpuesto'); ?>
        <?php echo $form->error($model,'totimpuesto'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'notas'); ?>
        <?php echo $form->textField($model,'notas',array('size'=>60,'maxlength'=>250)); ?>
        <?php echo $form->error($model,'notas'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'fechayhora'); ?>
        <?php echo $form->textField($model,'fechayhora'); ?>
        <?php echo $form->error($model,'fechayhora'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'cant_devuelta'); ?>
        <?php echo $form->textField($model,'cant_devuelta'); ?>
        <?php echo $form->error($model,'cant_devuelta'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'cant_facturada'); ?>
        <?php echo $form->textField($model,'cant_facturada'); ?>
        <?php echo $form->error($model,'cant_facturada'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'es_materia_prima'); ?>
        <?php echo $form->textField($model,'es_materia_prima'); ?>
        <?php echo $form->error($model,'es_materia_prima'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'id_articulo_padre'); ?>
        <?php echo $form->textField($model,'id_articulo_padre'); ?>
        <?php echo $form->error($model,'id_articulo_padre'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'id_unidad'); ?>
        <?php echo $form->textField($model,'id_unidad'); ?>
        <?php echo $form->error($model,'id_unidad'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>
-->
<?php $this->endWidget(); ?>


<div class="container" style="margin-left: 10%; max-width: 84%;">
  
 

  <ul class="nav nav-tabs">
    <li  class="active"><a data-toggle="tab" href="#facturas" style="color:cyan; font-size: 17px;" ><strong>Facturas</strong></a></li>
    <li><a data-toggle="tab" href="#productos" style="color: cyan; font-size: 17px;"><strong>Productos</strong> </a></li>
    <li><a data-toggle="tab" href="#clientes" style="color: cyan; font-size: 17px;"><strong>Clientes</strong></a></li>
    
  </ul>

  <div class="tab-content">
    <div id="facturas" class="tab-pane fade in active ">
      <h3>Facturas</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    </div>
    <div id="productos" class="tab-pane fade">
      <h3>Productos</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
    <div id="clientes" class="tab-pane fade">
      <h3>Clientes</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
    <div id="menu3" class="tab-pane fade">
      <h3>Menu 3</h3>
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
  </div>
</div>

</div><!-- form -->