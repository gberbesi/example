
<?php
/* @var $this OpermvController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Opermvs',
);

$this->menu=array(
    array('label'=>'Create Opermv', 'url'=>array('create')),
    array('label'=>'Manage Opermv', 'url'=>array('admin')),
);
?>

<h1>Opermvs</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_view',
)); ?>