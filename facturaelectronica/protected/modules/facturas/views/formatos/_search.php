<?php
/* @var $this OpermvController */
/* @var $model Opermv */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>

 
     <div class="row">
        <?php echo $form->label($model,'id_articulo'); ?>
        <?php echo $form->textField($model,'id_articulo'); ?>
    </div>
<!--
    
    <div class="row">
        <?php echo $form->label($model,'id_opermv'); ?>
        <?php echo $form->textField($model,'id_opermv'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'id_articulo'); ?>
        <?php echo $form->textField($model,'id_articulo'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'id_almacen'); ?>
        <?php echo $form->textField($model,'id_almacen'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'id_vendedor'); ?>
        <?php echo $form->textField($model,'id_vendedor'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'costounit'); ?>
        <?php echo $form->textField($model,'costounit'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'preciooriginal'); ?>
        <?php echo $form->textField($model,'preciooriginal'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'dsctounit'); ?>
        <?php echo $form->textField($model,'dsctounit'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'preciofin'); ?>
        <?php echo $form->textField($model,'preciofin'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'dsctoprc'); ?>
        <?php echo $form->textField($model,'dsctoprc'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'dsctomtolinea'); ?>
        <?php echo $form->textField($model,'dsctomtolinea'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'cantidad'); ?>
        <?php echo $form->textField($model,'cantidad'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'montoneto'); ?>
        <?php echo $form->textField($model,'montoneto'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'totimpuesto'); ?>
        <?php echo $form->textField($model,'totimpuesto'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'notas'); ?>
        <?php echo $form->textField($model,'notas',array('size'=>60,'maxlength'=>250)); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'fechayhora'); ?>
        <?php echo $form->textField($model,'fechayhora'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'cant_devuelta'); ?>
        <?php echo $form->textField($model,'cant_devuelta'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'cant_facturada'); ?>
        <?php echo $form->textField($model,'cant_facturada'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'es_materia_prima'); ?>
        <?php echo $form->textField($model,'es_materia_prima'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'id_articulo_padre'); ?>
        <?php echo $form->textField($model,'id_articulo_padre'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model,'id_unidad'); ?>
        <?php echo $form->textField($model,'id_unidad'); ?>
    </div>
-->
    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->