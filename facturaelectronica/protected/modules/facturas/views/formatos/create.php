<?php
/* @var $this OpermvController */
/* @var $model Opermv */

$this->breadcrumbs=array(
    'Opermvs'=>array('index'),
    'Create',
);

$this->menu=array(
    array('label'=>'List Opermv', 'url'=>array('index')),
    array('label'=>'Manage Opermv', 'url'=>array('admin')),
);
?>

<h1>Facturas</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
<script src="themes/gentelella/vendors/jquery/dist/jquery.min.js"></script>