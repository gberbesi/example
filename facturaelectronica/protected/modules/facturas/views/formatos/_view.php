
<?php
/* @var $this OpermvController */
/* @var $data Opermv */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_opermv')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id_opermv), array('view', 'id'=>$data->id_opermv)); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_operti')); ?>:</b>
    <?php echo CHtml::encode($data->id_operti); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_articulo')); ?>:</b>
    <?php echo CHtml::encode($data->id_articulo); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_almacen')); ?>:</b>
    <?php echo CHtml::encode($data->id_almacen); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_vendedor')); ?>:</b>
    <?php echo CHtml::encode($data->id_vendedor); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('costounit')); ?>:</b>
    <?php echo CHtml::encode($data->costounit); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('preciooriginal')); ?>:</b>
    <?php echo CHtml::encode($data->preciooriginal); ?>
    <br />

    <?php /*
    <b><?php echo CHtml::encode($data->getAttributeLabel('dsctounit')); ?>:</b>
    <?php echo CHtml::encode($data->dsctounit); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('preciofin')); ?>:</b>
    <?php echo CHtml::encode($data->preciofin); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('dsctoprc')); ?>:</b>
    <?php echo CHtml::encode($data->dsctoprc); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('dsctomtolinea')); ?>:</b>
    <?php echo CHtml::encode($data->dsctomtolinea); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
    <?php echo CHtml::encode($data->cantidad); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('montoneto')); ?>:</b>
    <?php echo CHtml::encode($data->montoneto); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('totimpuesto')); ?>:</b>
    <?php echo CHtml::encode($data->totimpuesto); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('notas')); ?>:</b>
    <?php echo CHtml::encode($data->notas); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('fechayhora')); ?>:</b>
    <?php echo CHtml::encode($data->fechayhora); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('cant_devuelta')); ?>:</b>
    <?php echo CHtml::encode($data->cant_devuelta); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('cant_facturada')); ?>:</b>
    <?php echo CHtml::encode($data->cant_facturada); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('es_materia_prima')); ?>:</b>
    <?php echo CHtml::encode($data->es_materia_prima); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_articulo_padre')); ?>:</b>
    <?php echo CHtml::encode($data->id_articulo_padre); ?>
    <br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('id_unidad')); ?>:</b>
    <?php echo CHtml::encode($data->id_unidad); ?>
    <br />

    */ ?>

</div>