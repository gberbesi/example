<br>
 <h1 style="color:#2a3f54;"> &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-user"></span><span style="color:black">  Contribuyentes </span></h1>
<nav class="breadcrumb" style="background-color:white; padding-left: 10px;margin-left: 30px; font-size: 14px;">
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('usuario/usuario/index');?>" style="color:black"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Inicio </a><a>/</a>
  <span class="breadcrumb-item active" style="color:black"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Contribuyente  </span> 
</nav>   

<?php $form=$this->beginWidget('CActiveForm', array('id'=>'contribuyente-form',
  'htmlOptions' => array('enctype' => 'multipart/form-data'),)); 
 date_default_timezone_set('America/Costa_Rica'); 
?>


         <div class="col-md-3 col-sm-5 col-xs-12 form-group ">
           
                <label for="fullname">*Nombre / Razón Social: </label>
           <?php echo $form->textField($model,'nombre',array('type'=>'text','id'=>'nombre','class'=>'form-control','value'=>'EnergyJW S.A.','required'=>true,'readonly'=>true)); ?>
            <?php echo $form->error($model,'nombre',array('class'=>'btn-xs alert-danger text-center')); ?> 
          </div>

<div class="col-md-3 col-sm-6 col-xs-12 form-group ">
           
        <label for="fullname">Nombre de Fantasía / Abreviado:</label>
   <?php echo $form->textField($model,'nombre_abre',array('type'=>'text','id'=>'nombre_abre','class'=>'form-control')); ?>
    <?php echo $form->error($model,'nombre_abre',array('class'=>'btn-xs alert-danger text-center')); ?> 
  </div>

      <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
   
		   <label for="fullname">Tipo de Identificación:</label>
		   <?php echo $form->dropDownList($model,'tipo_identificacion',CHtml::listData(Identificacion::model()->findAll(),'id_identificacion', 'identificacion'),array('class'=>'form-control select2 ','style'=>'width:100%','readonly'=>true)) ?>
		   <?php echo $form->error($model,'tipo_identificacion',array('class'=>'btn-xs alert-danger text-center')); ?> 
  </div>

  <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
           
        <label for="fullname">*Identificación:</label>
   <?php echo $form->numberField($model,'identificacion',array('id'=>'identificacion','class'=>'form-control','value'=>'3101728333','readonly'=>true)); ?>
    <?php echo $form->error($model,'identificacion',array('class'=>'btn-xs alert-danger text-center')); ?> 
  </div>

   <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
           
        <label for="fullname">Contacto:</label>
   <?php echo $form->textField($model,'contacto',array('type'=>'text','id'=>'contacto','class'=>'form-control')); ?>
    <?php echo $form->error($model,'contacto',array('class'=>'btn-xs alert-danger text-center')); ?> 
  </div>


 <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
           
        <label for="fullname">Moneda de la Compañía:</label>
   <?php echo $form->dropDownList($model,'moneda',CHtml::listData(Moneda::model()->findAll(),'id_moneda', 'moneda'),array('class'=>'form-control select2','style'=>'width:100%','id'=>'moneda')) ?>
    <?php echo $form->error($model,'moneda',array('class'=>'btn-xs alert-danger text-center')); ?> 
  </div>


   <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
           
        <label for="fullname">*Correo Electrónico:</label>
   <?php echo $form->emailField($model,'correo',array('id'=>'correo','class'=>'form-control')); ?>
    <?php echo $form->error($model,'correo',array('class'=>'btn-xs alert-danger text-center')); ?> 
  </div>


<div class="col-md-3 col-sm-6 col-xs-12 form-group ">
           
        <label for="fullname">Página Web:</label>
   <?php echo $form->urlField($model,'pagina',array('id'=>'pagina','class'=>'form-control')); ?>
    <?php echo $form->error($model,'pagina',array('class'=>'btn-xs alert-danger text-center')); ?> 
  </div> 

<div class="col-md-3 col-sm-6 col-xs-12 form-group ">
		 <label for="fullname"> País:</label>        
		<?php echo $form->dropDownList($model,'pais',CHtml::listData(Pais::model()->findAll(),'id_pais', 'pais'),array('class'=>'form-control select2','style'=>'width:100%','required'=>true)) ?>
		<?php echo $form->error($model,'pais',array('class'=>'btn-xs alert-danger text-center')); ?>
</div> 

                        <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                            <label for="fullname"> *Provincia</label>
                            <?php
                            if ($model->isNewRecord) {
                                $model->provincia=1;
                            }
                            
                                    echo $form->dropDownList($model, 'provincia',$estadoListData, array( 
                                         'class'=>'form-control select2',
                                         'style'=>'width:100%',
                                         'required'=>true,
                                       

                                        'ajax'=>array(
                                            'type'=>'POST',
                                            'url'=>$this->createUrl('SelectMunicipio'),
                                            'update'=>'#' . CHtml::activeId($model, 'canton'),
                                            
                                        ),
                                    ));
                                    ?>
                            <?php echo $form->error($model,'provincia',array('class'=>'btn-xs alert-danger text-center'));
                             ?>
                          </div>

<div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                          <label for="fullname"> *Cantón</label>
                           <?php 

                               if(!$model->isNewRecord){
                                   
                                    $lista1=Canton::model()->find('id_canton=:id_canton',array(':id_canton'=>$model->canton));
                                   
                                   
                                    $model->canton=$lista1->id_canton;

                                  }
                                  elseif($model->isNewRecord){
                                    $list=Canton::model()->find('id_canton=1');
                                    $model->canton=$list->id_canton;

                                  }



                            ?>

                           <?php
                                  echo $form->dropDownList($model, 'canton',CHtml::listData(Canton::model()->findAll('provincia_id =:provincia_id', array(':provincia_id'=>$model->provincia)), 'id_canton', 'canton'), 
                                    array(
                                        'class'=>'form-control select2',
                                         'style'=>'width:100%',
                                         'required'=>true,
                                         

                                        'ajax'=>array(
                                            'type'=>'POST',
                                            'url'=>$this->createUrl('SelectParroquia'),
                                            'update'=>'#' . CHtml::activeId($model, 'distrito'),
                                            
                                      ),
                                  ));
                                  
                                  ?>
                          <?php echo $form->error($model,'id_canton',array('class'=>'btn-xs alert-danger text-center')); ?>
                          </div>


                          <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                          <label for="fullname"> *Distrito</label>

                          <?php

                            if (!$model->isNewRecord ){
                                   
                                    $lista2=Distrito::model()->find('id_distrito=:id_distrito',array(':id_distrito'=>$model->id_distrito));
                                   
                                   
                                    $model->distrito=$lista2->id_distrito;
                                  }
                                  else{
                                    $lista2=Distrito::model()->find('id_distrito=1');
                                    $model->distrito=$lista2->id_distrito;
                                  }



                           ?>

                           <?php echo $form->dropDownList($model, 'distrito',CHtml::listData(Distrito::model()->findAll('canton_id =:canton_id', array(':canton_id'=>$model->canton)), 'id_distrito', 'distrito'), array('class'=>'form-control select2','style'=>'width:100%' ,'required'=>true)); ?>

                          <?php echo $form->error($model,'distrito',array('class'=>'btn-xs alert-danger text-center')); ?>
                         </div>                  
   <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
           
        <label for="fullname">Teléfono:</label>
   <?php echo $form->numberField($model,'telefono',array('id'=>'telefono','class'=>'form-control')); ?>
    <?php echo $form->error($model,'telefono',array('class'=>'btn-xs alert-danger text-center')); ?> 
  </div>

   <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
           
        <label for="fullname">Fax:</label>
   <?php echo $form->textField($model,'fax',array('type'=>'text','id'=>'fax','class'=>'form-control')); ?>
    <?php echo $form->error($model,'fax',array('class'=>'btn-xs alert-danger text-center')); ?> 
  </div>

    <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
           
        <label for="fullname">Fecha Inscripción:</label>
   <?php echo $form->textField($model,'fecha_ins',array('id'=>'fecha_ins','class'=>'form-control','readonly'=>true,'value'=>date('j/n/Y H:i:s'))); ?> 
    <?php echo $form->error($model,'fecha_ins',array('class'=>'btn-xs alert-danger text-center')); ?> 
  </div>

      <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
           
        <label for="fullname">*Dirección:</label>
   <?php echo $form->textField($model,'direccion',array('type'=>'text','id'=>'direccion','class'=>'form-control')); ?>
    <?php echo $form->error($model,'direccion',array('class'=>'btn-xs alert-danger text-center')); ?> 
  </div>

 <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
           
        <label for="fullname">Logo:</label>
   <?php echo $form->fileField($model,'foto',array('type'=>'text','id'=>'files','class'=>'form-control','accept'=>'image/*')); ?>
    <?php echo $form->error($model,'foto',array('class'=>'btn-xs alert-danger text-center')); ?> 
    <output id="list" ></output>
  </div>
         <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
           
        <label for="fullname">Cuentas Bancarias:</label>
   <?php echo $form->textField($model,'cuenta_banc',array('type'=>'text','id'=>'cuenta_banc','class'=>'form-control')); ?>
    <?php echo $form->error($model,'cuenta_banc',array('class'=>'btn-xs alert-danger text-center')); ?> 
  </div>


                       <div class="col-md-7  col-sm-6 col-xs-12  pull-right">
                        <div class="col-md-4  col-md-offset-5 ">
                          
                            <?php echo CHtml::submitButton('Guardar',array('class'=>"btn btn-dark btn-block  btn-lg",'style'=>'background-color:#ff333a;font-weight:bold;')); ?>
                          </div>
 
                          
                        </div>

<?php $this->endWidget(); ?>


<script src="js/sweetalert.min.js"></script>

<script>
function archivo(evt) { 
      var files = evt.target.files; // FileList object
       
        //Obtenemos la imagen del campo "file". 
      for (var i = 0, f; f = files[i]; i++) {         
           //Solo admitimos imágenes.
           if (!f.type.match('image.*')) {
                continue;
           }
       
           var reader = new FileReader();
           
           reader.onload = (function(theFile) {
               return function(e) {
               // Creamos la imagen.
                      document.getElementById("list").innerHTML = ['<img height="100" width="100" class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                      if(theFile.type=="image/jpeg"){
                      	swal("Atencion!", "Solo se aceptan imagenes .png","error");
                      }
               };
           })(f);
 
           reader.readAsDataURL(f);
       }
}
             
      document.getElementById('files').addEventListener('change', archivo, false);

</script>

<?php if($_GET['guardado']==1){
  ?>
  <script>
   swal("Guardado!", "Su actualizacion fue guardad exitosamente!", "success");
  </script>
  <?php
} ?>