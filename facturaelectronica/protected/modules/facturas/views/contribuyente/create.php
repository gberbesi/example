<?php
/* @var $this ContribuyenteController */
/* @var $model Contribuyente */

$this->breadcrumbs=array(
	'Contribuyentes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Contribuyente', 'url'=>array('index')),
	array('label'=>'Manage Contribuyente', 'url'=>array('admin')),
);
?>


<?php $this->renderPartial('_form', array('model'=>$model,'estadoListData'=>$estadoListData)); ?>