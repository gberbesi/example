<?php
/* @var $this ContribuyenteController */
/* @var $model Contribuyente */

$this->breadcrumbs=array(
	'Contribuyentes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Contribuyente', 'url'=>array('index')),
	array('label'=>'Create Contribuyente', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#contribuyente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Contribuyentes</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'contribuyente-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_contribuyente',
		'nombre',
		'nombre_abre',
		'tipo_identificacion',
		'identificacion',
		'contacto',
		/*
		'moneda',
		'correo',
		'pagina',
		'pais',
		'provincia',
		'canton',
		'distrito',
		'telefono',
		'fax',
		'fecha_ins',
		'direccion',
		'cuenta_banc',
		'foto',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
