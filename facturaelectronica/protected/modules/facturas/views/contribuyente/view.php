<?php
/* @var $this ContribuyenteController */
/* @var $model Contribuyente */

$this->breadcrumbs=array(
	'Contribuyentes'=>array('index'),
	$model->id_contribuyente,
);

$this->menu=array(
	array('label'=>'List Contribuyente', 'url'=>array('index')),
	array('label'=>'Create Contribuyente', 'url'=>array('create')),
	array('label'=>'Update Contribuyente', 'url'=>array('update', 'id'=>$model->id_contribuyente)),
	array('label'=>'Delete Contribuyente', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_contribuyente),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Contribuyente', 'url'=>array('admin')),
);
?>

<h1>View Contribuyente #<?php echo $model->id_contribuyente; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_contribuyente',
		'nombre',
		'nombre_abre',
		'tipo_identificacion',
		'identificacion',
		'contacto',
		'moneda',
		'correo',
		'pagina',
		'pais',
		'provincia',
		'canton',
		'distrito',
		'telefono',
		'fax',
		'fecha_ins',
		'direccion',
		'cuenta_banc',
		'foto',
	),
)); ?>
