<?php
/* @var $this ContribuyenteController */
/* @var $model Contribuyente */

$this->breadcrumbs=array(
	'Contribuyentes'=>array('index'),
	$model->id_contribuyente=>array('view','id'=>$model->id_contribuyente),
	'Update',
);

$this->menu=array(
	array('label'=>'List Contribuyente', 'url'=>array('index')),
	array('label'=>'Create Contribuyente', 'url'=>array('create')),
	array('label'=>'View Contribuyente', 'url'=>array('view', 'id'=>$model->id_contribuyente)),
	array('label'=>'Manage Contribuyente', 'url'=>array('admin')),
);
?>

<h1>Update Contribuyente <?php echo $model->id_contribuyente; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>