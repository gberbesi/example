<?php
/* @var $this ContribuyenteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Contribuyentes',
);

$this->menu=array(
	array('label'=>'Create Contribuyente', 'url'=>array('create')),
	array('label'=>'Manage Contribuyente', 'url'=>array('admin')),
);
?>

<h1>Contribuyentes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
