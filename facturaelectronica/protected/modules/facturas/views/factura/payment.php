<script src="themes/gentelella/vendors/jquery/dist/jquery.min.js"></script>
<script src="js/sweetalert.min.js"></script>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'factura-form', 
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
));

 ?>

 <?php $algo=$_GET['id'];?>;
<div class="container">
  <h1 align="center">Métodos De Pago</h1>
  <div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse"  data-parent="#accordion" href="#collapse1">
        <h4 class="panel-title" align="center" style="color:#22C7FC;font-weight:bold;">
          EFECTIVO
        </h4></a>
      </div>
      <div id="collapse1" class="panel-collapse collapse in">
        <div class="panel-body">
        	


          
                            <div class="col col-lg-2 col-xs-12 col-md-12 col-lg-offset-5 " align="center">
                            	<label for="fullname">Monto</label>
                              
                           <?php echo $form->textField($model,'efec_monto',array('type'=>'text','id'=>'efec_monto','class'=>'form-control text-center text-center','onblur'=>'selectedAlumno6()','value'=>0)); ?></div>
                             <?php echo $form->error($model,'efec_monto',array('class'=>'btn-xs alert-danger text-center')); ?>
                        
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><div class="panel-heading">

        <h4 class="panel-title"  align="center" style="color:#22C7FC;font-weight:bold;">
          TARJETA
        </h4></a>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">
        	<div class="col col-lg-2 col-xs-12 col-md-12 col-lg-offset-3" align="center">
        	<label for="fullname">N° Tarjeta</label>
                           <?php echo $form->textField($model,'tj_nm',array('type'=>'text','id'=>'tj_nm','class'=>'form-control text-center text-center','placeholder'=>"")); ?>
                             <?php echo $form->error($model,'tj_nm',array('class'=>'btn-xs alert-danger text-center')); ?>
                         </div>
                         <div class="col col-lg-2 col-xs-12 col-md-12 " align="center">
                         <label for="fullname">Referencia</label>
                           <?php echo $form->textField($model,'tj_ref',array('type'=>'text','id'=>'tj_ref','class'=>'form-control text-center text-center','placeholder'=>"")); ?>
                             <?php echo $form->error($model,'tj_ref',array('class'=>'btn-xs alert-danger text-center')); ?>
                         </div>
                            <div class="col col-lg-2 col-xs-12 col-md-12 " align="center">
                         <label for="fullname">Monto</label>                          
                           <?php echo $form->textField($model,'tj_monto',array('type'=>'text','id'=>'tj_monto','class'=>'form-control text-center text-center','placeholder'=>"",'onblur'=>'selectedAlumno6()','value'=>0)); ?>
                             <?php echo $form->error($model,'tj_monto',array('class'=>'btn-xs alert-danger text-center')); ?>
                         </div>
                         </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
        <h4 class="panel-title"  align="center" style="color:#22C7FC;font-weight:bold;">
          CHEQUES
        </h4></a>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">   
        	<div class="col col-lg-2 col-xs-12 col-md-12 col-lg-offset-3" align="center">
               <label for="fullname">Banco</label>                           
                           <?php echo $form->textField($model,'ch_bnc',array('type'=>'text','id'=>'ch_bnc','class'=>'form-control text-center text-center','placeholder'=>"")); ?>
                             <?php echo $form->error($model,'ch_bnc',array('class'=>'btn-xs alert-danger text-center')); ?>
                         </div>
                         <div class="col col-lg-2 col-xs-12 col-md-12 " align="center">
                         <label for="fullname">Referencia</label>
                           <?php echo $form->textField($model,'ch_ref',array('type'=>'text','id'=>'ch_ref','class'=>'form-control text-center text-center','placeholder'=>"")); ?>
                             <?php echo $form->error($model,'ch_ref',array('class'=>'btn-xs alert-danger text-center')); ?>
                             </div>
                             <div class="col col-lg-2 col-xs-12 col-md-12 " align="center">
                         <label for="fullname">Monto</label>
                           <?php echo $form->textField($model,'ch_monto',array('type'=>'text','id'=>'ch_monto','class'=>'form-control text-center text-center','placeholder'=>"",'onblur'=>'selectedAlumno6()','value'=>0)); ?>
                             <?php echo $form->error($model,'ch_monto',array('class'=>'btn-xs alert-danger text-center')); ?>           </div>
                         </div>
                          </div>
      </div>
    
     <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
        <h4 class="panel-title"  align="center" style="color:#22C7FC;font-weight:bold;">
          TRANSFERENCIAS
        </h4></a>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
        <div class="panel-body">
      	<div class="col col-lg-2 col-xs-12 col-md-12 col-lg-offset-3" align="center">
        	<label for="fullname">Banco</label>          
                           <?php echo $form->textField($model,'trs_bnc',array('type'=>'text','id'=>'trs_bnc','class'=>'form-control text-center text-center','placeholder'=>"")); ?>
                             <?php echo $form->error($model,'trs_bnc',array('class'=>'btn-xs alert-danger text-center')); ?>
                         </div>
      	<div class="col col-lg-2 col-xs-12 col-md-12" align="center">
                         <label for="fullname">Referencia</label>
                           <?php echo $form->textField($model,'trs_ref',array('type'=>'text','id'=>'trs_ref','class'=>'form-control text-center text-center','placeholder'=>"")); ?>
                             <?php echo $form->error($model,'trs_ref',array('class'=>'btn-xs alert-danger text-center')); ?>
                         </div>
      	<div class="col col-lg-2 col-xs-12 col-md-12 " align="center">
                         <label for="fullname">Monto</label>
                           <?php echo $form->textField($model,'trs_monto',array('type'=>'text','id'=>'trs_monto','class'=>'form-control text-center text-center','placeholder'=>"",'onblur'=>'selectedAlumno6()','value'=>0)); ?>
                             <?php echo $form->error($model,'trs_monto',array('class'=>'btn-xs alert-danger text-center')); ?></div>
                         </div>
      </div>
    </div>
     <div class="panel panel-default">
      <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><h4 class="panel-title"  align="center" style="color:#22C7FC;font-weight:bold;">
          OTRO
        </h4></a>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
        <div class="panel-body">
      	<div class="col col-xs-12 col-md-12 col-lg-2  col-lg-offset-4" align="center">
        	<label for="fullname">Referencia</label>
                           <?php echo $form->textField($model,'ot_ref',array('type'=>'text','id'=>'ot_ref','class'=>'form-control text-center text-center','placeholder'=>"")); ?>
                             <?php echo $form->error($model,'ot_ref',array('class'=>'btn-xs alert-danger text-center')); ?>
                         </div>
      	<div class="col col-lg-2 col-xs-12 col-md-12 " align="center">
                         <label for="fullname">Monto</label>
                           <?php echo $form->textField($model,'ot_monto',array('type'=>'text','id'=>'ot_monto','class'=>'form-control text-center text-center','placeholder'=>"",'onblur'=>'selectedAlumno6()','value'=>0)); ?>
	                             <?php echo $form->error($model,'ot_monto',array('class'=>'btn-xs alert-danger text-center')); ?>
    	</div>                     
        </div>
      </div>
    </div>
    <br>
    <div class="col col-lg-2 col-xs-12 col-md-12 col-lg-offset-5" align="center">
    	<label for="fullname">TOTAL FACTURA</label>                      
                           <?php echo $form->textField($model,'total_payment',array('type'=>'text','id'=>'total_payment','class'=>'form-control text-center text-center','placeholder'=>"",'value'=>$total,'readonly'=>true)); ?>
                             <?php echo $form->error($model,'total_payment',array('class'=>'btn-xs alert-danger text-center')); ?>
                         
          
    </div>
    <div class="col col-lg-2 col-xs-12 col-md-12 col-lg-offset-5"  align="center">
    	<label for="fullname">TOTAL PAGADO</label>                       
                           <?php echo $form->textField($model,'total_completo',array('type'=>'text','id'=>'total_completo','class'=>'form-control text-center text-center','value'=>0,'readonly'=>true)); ?>
                             <?php echo $form->error($model,'total_completo',array('class'=>'btn-xs alert-danger text-center')); ?>
                         

    </div>
    <div class="col col-lg-2 col-xs-12 col-md-12 col-lg-offset-5"  align="center">
    	<label for="fullname">VUELTO</label>                       
                           <?php echo $form->textField($model,'vuelto',array('type'=>'text','id'=>'vuelto','class'=>'form-control text-center text-center','value'=>0,'readonly'=>true)); ?>
                             <?php echo $form->error($model,'vuelto',array('class'=>'btn-xs alert-danger text-center')); ?>
                         
          
    </div>

    <?php echo  $form->textField($model,'nombre_cliente',array('type'=>'text','id'=>'idfac','style'=>'display:none','value'=>$algo)); ?>
      <?php echo $form->textField($model,'factura',array('type'=>'text','id'=>'factura','class'=>'form-control text-center text-center','placeholder'=>"",'style'=>'display:none')); ?>

<div class="col col-md-3 col-xs-12 col-lg-2  col-lg-offset-5 " ><br>  
        <?php         echo CHtml::submitButton($model->isNewRecord ? 'Pagar' : 'Save',array('class'=>'btn btn-dark btn-lg','style'=>'width:100%;','onclick'=>'ventana();')); ?>
    </div>
  </div> 

<?php $this->endWidget()?>  


<script src="themes/gentelella/vendors/jquery/dist/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script> 
   
function ventana(){
  var $id = document.getElementById("idfac").value;

var $monto = document.getElementById("efec_monto").value;
var $tarjeta = document.getElementById("tj_monto").value;
var $cheques = document.getElementById("ch_monto").value;
var $tranfertencias = document.getElementById("trs_monto").value;
var $otro = document.getElementById("ot_monto").value;
var $total = document.getElementById("total_completo").value;
var $costo= document.getElementById("total_payment").value;
var $vuelto=document.getElementById("vuelto").value;
if(isNaN($monto)||isNaN($tarjeta)||isNaN($cheques)||isNaN($tranfertencias)||isNaN($otro)){
 swal("Atención!", "El monto de abono no puede ser letras","error");
  event.preventDefault();
}else if($total==0){

  swal("Atención","El monto cancelado no puede ser 0","warning");
  event.preventDefault();
}else if($vuelto<0){
  swal("Atención","El monto cancelado no puede ser menor al Total","warning");
  event.preventDefault();
  
}else{
    window.open("index.php?r=facturas/factura/admin");
}

}

function selectedAlumno6() {//boton es lo q atrae del forech

var $monto = document.getElementById("efec_monto").value;
var $tarjeta = document.getElementById("tj_monto").value;
var $cheques = document.getElementById("ch_monto").value;
var $tranfertencias = document.getElementById("trs_monto").value;
var $otro = document.getElementById("ot_monto").value;

var $monto2 = document.getElementById("total_payment").value;

if($monto==''){
  $monto=0;
}
if($tarjeta==''){
  $tarjeta=0;
}
if($cheques==''){
  $cheques=0;
}
if($tranfertencias==''){ 
  $tranfertencias=0;
}
if($otro==''){
  $otro=0;
}
if(isNaN($monto)||isNaN($tarjeta)||isNaN($cheques)||isNaN($tranfertencias)||isNaN($otro)){
  swal("Atención!", "El monto de abono no puede ser letras","error");
}else{

var total=parseInt($monto)+parseInt($tarjeta)+parseInt($cheques)+parseInt($tranfertencias)+parseInt($otro);


if($monto2>total){

var $vuelto=(parseInt($monto2)-parseInt(total));
var $vuelto = $vuelto*(-1);

}else if(total>$monto2){

  var $vuelto=(parseInt(total)-parseInt($monto2));

}else if(total == $monto2){
  var $vuelto=0;
}

$('#total_completo').val(total);


var $fac=(Math.floor((Math.random() * 1000000000000) + 200000000));
$('#factura').val($fac);
$('#vuelto').val($vuelto);
}




  
}


</script> 