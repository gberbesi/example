                <div class="box-header with-border">
                
                  <h1 style="color:#2a3f54;"> &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-wrench"></span><span style="color:black;"> Factura</span> </h1>
                  <nav class="breadcrumb" style="background-color:white; padding-left: 10px;margin-left: 30px; font-size: 14px;">
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('usuario/usuario/index');?>" style="color:black  "><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;<span style="color:#22C7FC" >Inicio</span> </a><a>/</a>
  <a class="breadcrumb-item" href="#" style="color:black"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Facturación</span> </a><span>/</span><a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('usuario/usuario/index');?>" style="color:black  "><span class="glyphicon glyphicon-cog"></span>&nbsp;&nbsp;<span style="color:#22C7FC" >Formato</span> </a><a></a>

</nav>
                 
               
							</br>
         
		

    <?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'formatos-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>   
								


                  <div class="table table-striped table-bordered table-condensed table-responsive" cellspacing="0" cellpadding="3" rules="all" style="background-color:#eeeeee;border-color:#CCCCCC;border-width:1px;border-style:None;border-collapse:collapse; font-size: 14px;">
                    <?php echo $form->dropDownList($impresion,'formato',array("1"=>"Tiquete","0"=>"Factura"),array('class'=>'form-control  ','id'=>'formato2','style'=>'width:40%;margin-left:40%','onchange'=>'mostrar();',)); ?><br>
                    <div id="tique" style="display:block;">
                <table class="table table-responsive" >
                  <tr><td colspan="3" align="center" style="font-size: 15px;color:#22C7FC;font-weight: bold">Formato de tiquete para ver con detalle haga click sobre la imagen<br></td></tr>
                   <td align="center" style="font-size: 15px;color:#22C7FC;font-weight: bold" >Sin logo: <?php echo $form->radioButton($impresion,'ruta',array('type'=>'radio','class'=>'flat','id'=>'sinlg')); ?>
                            <br><img src="images/tique.jpg" width="300px" onclick="tipo7();" id="tick"></td>
                            <td align="center" style="font-size: 15px;color:#22C7FC;font-weight: bold" >Con logo: <?php echo $form->radioButton($impresion,'ruta',array('type'=>'radio','class'=>'flat','id'=>'conlg')); ?>
                            <br><img src="images/tiquelg.jpg" width="300px" onclick="tipo8();" id="ticklg"></td>
                          </table>
                            
                    </div>
                    
                 <div id="forma" style="display:none;">
                  

                        <table class="table table-responsive">
                          <tr><td colspan="3" align="center" style="font-size: 15px;color:#22C7FC;font-weight: bold">Seleccione la factura de su preferencia, para ver con detalle haga click sobre la imagen<br></td></tr>
                          <tr>
                            <td align="center" style="font-size: 15px;color:#22C7FC;font-weight: bold" >Amarillo: <?php echo $form->radioButton($impresion,'ruta',array('type'=>'radio','class'=>'flat','id'=>'amarilloo')); ?>
                            <br><img src="images/amarillo.jpg" width="300px" onclick="tipo2();" id="amarillo"></td>
                            <td align="center" style="font-size: 15px;color:#22C7FC;font-weight: bold" >Azul: <?php echo $form->radioButton($impresion,'ruta',array('type'=>'radio','class'=>'flat','id'=>'azull')); ?>
                              <br><img src="images/azul.jpg" width="300px" onclick="tipo3();" id="azul">  </td>
                              <td align="center" style="font-size: 15px;color:#22C7FC;font-weight: bold" >Turquesa: <?php echo $form->radioButton($impresion,'ruta',array('type'=>'radio','class'=>'flat','id'=>'turquesaa')); ?>
                                <br><img src="images/turquesa.jpg" width="270px" onclick="tipo4();" id="turquesa"> </td>
                          </tr>
                          <tr>
                            <td align="center" style="font-size: 15px;color:#22C7FC;font-weight: bold">  Turquesa 2: <?php echo $form->radioButton($impresion,'ruta',array('type'=>'radio','class'=>'flat','id'=>'turquesa22')); ?>
                              <br><img src="images/turquesa2.jpg" width="300px" onclick="tipo5();" id="turquesa2"></td>
                            <td align="center" style="font-size: 15px;color:#22C7FC;font-weight: bold">Gris: <?php echo $form->radioButton($impresion,'ruta',array('type'=>'radio','class'=>'flat','id'=>'griss')); ?>
                              <br><img src="images/gris.jpg" width="300px" onclick="tipo6();" id="gris">
                            </td>                    
                         
                        </tr>

                        </table>
                        </div>
                  </div>
                  <div  style="display: none" >
                    <p style="color:black;">Seleccione Su Ruta Muestra: </p>
                        <?php echo $form->textField($impresion,'ruta',array('id'=>'ruta2')) ?>
                     </div>
                   <div style="display: none"  >
                    <p style="color:black;">Seleccione Su Vista Descarga: </p>
                        <?php echo $form->textField($impresion,'vista',array('id'=>'vista2')) ?>
                     </div>
                      <div  style="display: none" >
                    <p style="color:black;">Seleccione Su Ruta Muestra: </p>
                        <?php echo $form->textField($impresion,'rutapr',array('id'=>'ruta3')) ?>
                     </div>
                   <div style="display: none"  >
                    <p style="color:black;">Seleccione Su Vista Descarga: </p>
                        <?php echo $form->textField($impresion,'vistapr',array('id'=>'vista3')) ?>
                     </div>
                  </div>
                  </div>
                  <div class="row buttons">
                    
        <?php echo CHtml::submitButton($impresion->isNewRecord ? 'Guardar' : 'Save',array('class'=>'btn btn-dark btn-lg pull-right')); ?>
    </div>
                  <?php $this->endWidget(); ?>
<!-- Modal Amarillo-->
   <div class="modal fade" id="facmul" name='' role="dialog">
    <div class="modal-dialog" id="forma" name=''>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Formato De Factura PDF</h4>
        </div>
        <div class="modal-body" id="listo">
         
            <p>amarillo</p>
            </div><br><br><br></p>

             <div class="modal-footer">
          <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
       
      </div>
      
    </div>
  </div>
<!--Modal amarillo -->

<script src="themes/gentelella/vendors/jquery/dist/jquery.min.js"></script>
<script src="js/sweetalert.min.js"></script> 
<script>
  function mostrar(){
   
   
   var tipof = $("#formato2").val();

  
   if(tipof==1)  {
     $("#forma").css('display','none');
     $("#tique").css('display','block');
     $("#vista").val('Fact'); 
     $("#ruta2").val('Fact3');
     var ruta2=$("#ruta2").val();
     var vista=$("#vista").val();

  
}else if(tipof==0){
  $("#tique").css('display','none')
  $("#forma").css('display','block'); 
  var ruta =$("#ruta").val();
  
    if(ruta=="Fact7&id"){
     $("#vista2").val('Fact17'); 
     $("#ruta2").val('Fact7');
     $("#vista3").val('Prof17'); 
     $("#ruta3").val('Prof7');
        }
    if(ruta=="Fact2&id"){
     $("#vista2").val('Fact12'); 
     $("#ruta2").val('Fact2');
     $("#vista3").val('Prof12'); 
     $("#ruta3").val('Prof2');
    }
    if(ruta=="Fact5&id"){
     $("#vista2").val('Fact15'); 
     $("#ruta2").val('Fact5');
     $("#vista3").val('Prof15'); 
     $("#ruta3").val('Prof5');
    }
    if(ruta=="Fact8&id"){
     $("#vista2").val('Fact18'); 
     $("#ruta2").val('Fact8');
     $("#vista3").val('Prof18'); 
    $("#ruta3").val('Prof8');
    }
    if(ruta=="Fact6&id"){
     $("#vista2").val('Fact16'); 
     $("#ruta2").val('Fact6');
     $("#vista3").val('Prof16'); 
      $("#ruta3").val('Prof6');
    }
    }
  }
  function tipo2(){
    var amarillo =$('#amarillo').attr("src");
    var html ='<img src="'+amarillo+'" width="100%"></p>';
    $('#facmul').modal();
   $('#listo').html(html); 
  }
   function tipo3(){
    var azul =$('#azul').attr("src");
    var html ='<img src="'+azul+'" width="100%"></p>';
    $('#facmul').modal();
   $('#listo').html(html); 
  }function tipo4(){
    var turquesa =$('#turquesa').attr("src");
    var html ='<img src="'+turquesa+'" width="100%"></p>';
    $('#facmul').modal();
   $('#listo').html(html); 
  }function tipo5(){
    var turquesa2 =$('#turquesa2').attr("src");
    var html ='<img src="'+turquesa2+'" width="100%"></p>';
    $('#facmul').modal();
   $('#listo').html(html); 
  }function tipo6(){
    var gris =$('#gris').attr("src");
    var html ='<img src="'+gris+'" width="100%"></p>';
    $('#facmul').modal();
   $('#listo').html(html); 
  }function tipo7(){
    var tique =$('#tick').attr("src");
    var html ='<img src="'+tique+'" width="100%"></p>';
    $('#facmul').modal();
   $('#listo').html(html); 
  }function tipo8(){
    var tiquelg =$('#ticklg').attr("src");
    var html ='<img src="'+tiquelg+'" width="100%"></p>';
    $('#facmul').modal();
   $('#listo').html(html); 
  }
  
  
$( "#formatos-form" ).submit(function( event ) {
 
 
  var tipof = $("#formato2").val();

  
   if(tipof==1)  {
     $("#forma").css('display','none');
     var prueba7=$('#sinlg:checked').val();
     var prueba8=$('#conlg:checked').val();
     if(prueba7){
     $("#ruta2").val('Fact'); 
     $("#vista2").val('Fact3');
      $("#ruta3").val('Prof'); 
     $("#vista3").val('Prof3');
   }else if (prueba8){
     $("#ruta2").val('Fact4'); 
     $("#vista2").val('Fact14');
     $("#ruta3").val('Prof4'); 
     $("#vista3").val('Prof14');
   }
    
   }else{
    var prueba=$('#amarilloo:checked').val();
    var prueba2=$('#azull:checked').val();
    var prueba3=$('#turquesaa:checked').val();
    var prueba4=$('#turquesa22:checked').val();
    var prueba5=$('#griss:checked').val();
    if(prueba){
  
     $("#ruta2").val('Fact7'); 
     $("#vista2").val('Fact17');
     $("#ruta3").val('Prof7'); 
     $("#vista3").val('Prof17');
   
    }
    else if(prueba2){
      $("#ruta2").val('Fact5'); 
      $("#vista2").val('Fact15'); 
      $("#ruta3").val('Prof5'); 
      $("#vista3").val('Prof15'); 
   // event.preventDefault();  
    }
    else if(prueba3){
      $("#ruta2").val('Fact6');
      $("#vista2").val('Fact16');
      $("#ruta3").val('Prof6');
      $("#vista3").val('Prof16');  
   // event.preventDefault();  
    }
    else if(prueba4){
      $("#ruta2").val('Fact8'); 
      $("#vista2").val('Fact18'); 
      $("#ruta3").val('Prof8'); 
      $("#vista3").val('Prof18'); 
     // event.preventDefault();  
    }
    else if(prueba5){
      $("#ruta2").val('Fact2'); 
      $("#vista2").val('Fact12'); 
      $("#ruta3").val('Prof2'); 
      $("#vista3").val('Prof12');
      //event.preventDefault();  
    }
   }
});
</script>
<?php if($_GET['id']==1){ ?>

 <script > swal("Guardado!", "Registro guardado exitosamente!", "success");</script>

<?php } ?>