<?php
/* @var $this FacturaController */
/* @var $model Factura */

$this->breadcrumbs=array(
	'Facturas'=>array('index'),
	'Create', 
);

$this->menu=array(
	array('label'=>'List Factura', 'url'=>array('index')),
	array('label'=>'Manage Factura', 'url'=>array('admin')), 
);
?>



<?php $this->renderPartial('_form', array('model'=>$model,'modelPro'=>$modelPro,'modelClie'=>$modelClie,'clientes'=>$clientes,'clientes2'=>$clientes2,'clientes3'=>$clientes3,'tablaProducto'=>$tablaProducto,'estadoListData'=>$estadoListData)); ?> 