<?php
Yii::import("application.modules.registro.models.Productos", true);
Yii::import("application.modules.registro.controllers.ProductosController", true);
?>
<h1> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-shopping-cart"></span>  &nbsp;&nbsp;&nbsp; Productos </h1>

 <br><br>
<nav class="breadcrumb" style="background-color:white; padding-left: 100px;">
  <a class="breadcrumb-item" href="#" style="color:#22C7FC"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;Inicio </a><a>/</a>
  <a class="breadcrumb-item" href="#" style="color:#22C7FC"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;&nbsp;Productos </a><a>/</a>
  <span class="breadcrumb-item active" style="color:#22C7FC"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;&nbsp;Registro  </span>
</nav>

 <div class="col-md-12 col-sm-12 col-xs-12" role="main">
          <div class="">
            
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel" style="background-color: #eeeeee">
               
                  <div class="x_content">


                    <br />
                   

                
                <?php $form=$this->beginWidget('CActiveForm', array('id'=>'productos-form','enableAjaxValidation'=>false,'htmlOptions' => array('enctype' => 'multipart/form-data'))); ?>

      
                
<!--
                     
-->
                      <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                         <label for="fullname">Código del Producto*</label>
                            <?php echo $form->textField($model,'codigo',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                            <?php echo $form->error($model,'codigo',array('class'=>'btn-xs alert-danger text-center')); ?>
                        
                      </div>

                      <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                         <label for="fullname">Tipo  *</label>
                           

                      <p>
                        Producto: <?php echo $form->radioButton($model,'tipo_producto',array('type'=>'radio','class'=>'flat','id'=>'genderM')); ?>&nbsp;&nbsp;&nbsp;&nbsp;

                        Servicio: <?php echo $form->radioButton($model,'tipo_producto',array('type'=>'radio','class'=>'flat','id'=>'genderF')); ?>

                         <?php echo $form->error($model,'tipo_producto',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </p>

                 
                      </div>


                        <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                         <label for="fullname">Descripción Corta * </label>
                            <?php echo $form->textField($model,'descripcionc',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                            <?php echo $form->error($model,'descripcionc',array('class'=>'btn-xs alert-danger text-center')); ?>
                        
                      </div>






                       <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                           <label for="fullname">Descripción * </label>
                            <?php echo $form->textField($model,'descripcion',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                            <?php echo $form->error($model,'descripcion',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                       
                            <label for="fullname">Unidad de Medida * </label>
                            <?php echo $form->textField($model,'unidad_medida_id',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                            <?php echo $form->error($model,'unidad_medida_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                       
                      </div>
 <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                       
                            <label for="fullname">Nombre para tabular  *</label>
                          
                            <?php echo $form->textField($model,'nombre',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                            <?php echo $form->error($model,'nombre',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                      <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                         <label for="fullname">Costo Compra ₡* </label>
                            <?php echo $form->textField($model,'precioc_id',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                            <?php echo $form->error($model,'precioc_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                       <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                           <label for="fullname">Precio Antes ₡* </label>
                            <?php echo $form->textField($model,'precios_id',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                            <?php echo $form->error($model,'precios_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>


                        <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                       
                            <label for="fullname">Costo Compra $ * </label>
                            <?php echo $form->textField($model,'preciosinin_puesto',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                            <?php echo $form->error($model,'preciosinin_puesto',array('class'=>'btn-xs alert-danger text-center')); ?>
                       
                      </div>

                      <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                         <label for="fullname">Precio antes de Impuesto $ * </label>
                            <?php echo $form->textField($model,'preciossinin_puesto',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                            <?php echo $form->error($model,'preciossinin_puesto',array('class'=>'btn-xs alert-danger text-center')); ?>
                        
                      </div>






                      <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                        <button class="btn btn-dark" type="reset">Agregar</button>
                      </div>
                      <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                         <label for="fullname">Precio Final ‎₡ * </label>
                            <?php echo $form->textField($model,'precio_finalc_id',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                            <?php echo $form->error($model,'precio_finalc_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                       <div class="col-md-2 col-sm-6 col-xs-12 form-group has-feedback">
                           <label for="fullname">Precio Final $ * </label>
                            <?php echo $form->textField($model,'precio_finals_id',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                            <?php echo $form->error($model,'precio_finals_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>
                    

                    


                   <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-9">
                          <?php echo CHtml::link('<span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span>  Regresar',array('/registro/productos/admin'),array('id'=>'bt1','class'=>"btn btn-dark")); ?>
      
                          
                          

                           <?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-dark')); ?>
     
                        </div>
                      </div>


                        </div>
                      </div>

                    
                  </div>
                </div>
              </div>
            </div>

          

             
              
              <div>
<?php $this->endWidget(); ?>






<script>
    function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }

    function soloNumeros(e){

  var key = window.Event ? e.which : e.keyCode

  return (key >= 48 && key <= 57)

}
</script>



<script src="themes/gentelella/vendors/jquery/dist/jquery.min.js"></script>

?>   

