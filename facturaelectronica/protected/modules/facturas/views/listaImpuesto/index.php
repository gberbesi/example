<?php
/* @var $this ListaImpuestoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Lista Impuestos',
);

$this->menu=array(
	array('label'=>'Create ListaImpuesto', 'url'=>array('create')),
	array('label'=>'Manage ListaImpuesto', 'url'=>array('admin')),
);
?>

<h1>Lista Impuestos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
