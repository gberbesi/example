                <div class="box-header with-border">
                
                  <h1 style="color:#2a3f54;"> &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-file"></span><span style="color:black;"> Descuentos</span> </h1>
                  <nav class="breadcrumb" style="background-color:white; padding-left: 10px;margin-left: 30px; font-size: 14px;">
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('usuario/usuario/index');?>" style="color:black  "><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;<span style="color:#22C7FC" >Inicio</span> </a><a>/</a>
  <a class="breadcrumb-item" href="#" style="color:black"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Descuento</span> </a><a></a>

</nav> 
                 
               
							</br>
<?php
            Yii::app()->clientScript->registerScript('search4', "

                $('.search4-form form').submit(function(){console.log('hola');
                $('#descuento-grid').yiiGridView('update', {
                data: $(this).serialize()
                   });
                 return false;
                   });
                ");
                ?>

						      <div class="search4-form col-md-12">
                <?php $this->renderPartial('_search',array(
                     'model'=>$model,
                     )); ?>

                </div>	
<div class="table table-striped table-bordered table-condensed table-responsive" cellspacing="0" cellpadding="3" rules="all" style="background-color:none;border-color:#CCCCCC;border-width:1px;border-style:None;border-collapse:collapse; font-size: 14px;">

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'descuento-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'itemsCssClass'=>'table table-striped table-responsive  text-center',
                        'pagerCssClass'=>'pagination pull-right',
                        'afterAjaxUpdate'=>'function(){$.getScript("themes/classic/plugins/archivos_js/campo-numero.js");$.getScript("themes/classic/plugins/archivos_js/campo-letra.js");
                                $.getScript("themes/classic/plugins/archivos_js/script_select2.js");}',
	'columns'=>array(
	array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">ID</p>',
 'name'=>'id_descuento',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'),
  'value'=>'$data->id_descuento',
    ),
	array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">Descuento</p>',
 'name'=>'descuento',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'),
  'value'=>'$data->descuento',
    ),	 
	array(
      'header'=>'<p style="color:#22C7FC;font-weight:bold" align="center">Porcentaje</p>',
 'name'=>'porcentaje',
 
 'htmlOptions'=>array('class'=>'col-md-0 text-center'),
  'value'=>'$data->porcentaje',
    ),		
		
                                            array(
                                                    'header'=>"",
                                                    'htmlOptions'=>array('class'=>'col-lg-3 text-center'),
                                                    'class'=>'CButtonColumn',
                                                    'template' => '{update}{borrar}',
                                                    'buttons'=>array(
                                                              
                                                                    
           
                                'update' => array(
                                'label'=>'',
                                'visible'=>'Yii::app()->user->checkAccess("root")',
                                'options'=>array('class'=>'fa fa-pencil','data-toggle'=>"tooltip",'title'=>'Editar','style'=>'color:black;font-size:20px;'),
                                'imageUrl'=>false,
                                ),
								'borrar' => array(
                    			'label'=>'',
                                //'visible'=>'$data->estatus != "true" && Yii::app()->user->checkAccess("factura_mercancia/Factura/delete")',
                              'options'=>array('class'=>'fa fa-times borrado','style'=>'margin-left:10px;color:red;font-size:20px;', 'data-toggle'=>"tooltip",'title'=>'Eliminar',),
                                'imageUrl'=>false,
                                'url'=>'Yii::app()->createUrl("facturas/descuento/delete", array("id" => $data->id_descuento))',
                               // 'click'=>'function(){return confirm("¿Desea Eliminar Este Tipo de Producto?");}',
                                ),
))
	),
)); ?>
</div></div>

<script src="js/sweetalert.min.js"></script>
<script>

/*<![CDATA[*/
jQuery(function($) {

jQuery(document).on('click','.borrado',function(event) {
    var th = this;
event.preventDefault(); 
    swal({
      title: "¿Eliminar?",
      text: "¿Está seguro que desea borrar este elemento?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        jQuery('#descuento-grid').yiiGridView('update', {
            type: 'POST',
            url: jQuery(th).attr('href'),
            success: function(data) {
                if(data!='error') {
                   jQuery('#descuento-grid').yiiGridView('update');

                    swal("Borrado exitoso", {
                      icon: "success",
                    }); 
                } else {
                    swal("Este producto esta presente en una o mas Facturas", {
                      icon: "error",
                    }); 
                }
                
            },
            error: function(XHR) {

            }
        });


        
      } else {
        //swal("Your imaginary file is safe!");
      }
    });


    
    
});
});
/*]]>*/
</script>

