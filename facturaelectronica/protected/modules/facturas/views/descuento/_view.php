<?php
/* @var $this DescuentoController */
/* @var $data Descuento */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_descuento')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_descuento), array('view', 'id'=>$data->id_descuento)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descuento')); ?>:</b>
	<?php echo CHtml::encode($data->descuento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('porcentaje')); ?>:</b>
	<?php echo CHtml::encode($data->porcentaje); ?>
	<br />


</div>