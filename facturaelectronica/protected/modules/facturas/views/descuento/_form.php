<br>
<script src="js/sweetalert.min.js"></script>
<script src="themes/gentelella/vendors/jquery/dist/jquery.min.js"></script>
 <h1 style="color:#2a3f54;"> &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-user"></span><span style="color:black">  Descuentos </span></h1>
<nav class="breadcrumb" style="background-color:white; padding-left: 10px;margin-left: 30px; font-size: 14px;">
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('usuario/usuario/index');?>" style="color:black"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Inicio </a><a>/</a>
  <span class="breadcrumb-item active" style="color:black"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Descuentos </span>
</nav>   

<?php $form=$this->beginWidget('CActiveForm', array(  
	'id'=>'descuento-form',
	'enableAjaxValidation'=>false,
)); ?>

		   <div class="col-md-3 col-md-offset-5 col-sm-5 col-xs-12 form-group ">
		   <label for="fullname">*Nombre: </label>
           <?php echo $form->textField($model,'descuento',array('type'=>'text','id'=>'descuento','class'=>'form-control','required'=>true)); ?>
            <?php echo $form->error($model,'descuento',array('class'=>'btn-xs alert-danger text-center')); ?> 
          </div><br><br><br><br>

          	<div class="col-md-3 col-md-offset-5 col-sm-5 col-xs-12 form-group ">
		   <label for="fullname">*Porcentaje: </label>
           <?php echo $form->textField($model,'porcentaje',array('type'=>'text','id'=>'porcentaje','class'=>'form-control','required'=>true,'value'=>'0')); ?>
            <?php echo $form->error($model,'porcentaje',array('class'=>'btn-xs alert-danger text-center')); ?> 
          </div>

<br><br><br><br> 
                       <div class="col-md-7  col-sm-6 col-xs-12  pull-right">
                        <div class="col-md-3 col-md-offset-1 col-xs-12">
                          
                            <?php echo CHtml::submitButton('Guardar',array('class'=>"btn btn-dark btn-block  btn-lg",'style'=>'background-color:#ff333a;font-weight:bold;')); ?>
                          </div>
 
                          
                        </div>

<?php $this->endWidget(); ?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>

$("#descuento-form").submit(function( event ) {

	var porcentaje=$('#porcentaje').val();
	if(porcentaje<=0){
		swal("Error!", "El porcentaje debe ser mayor a 0 !", "error");
		event.preventDefault();
	}
if(porcentaje>=100){
	swal("Error!", "El porcentaje debe ser menor a 100!", "error");
	event.preventDefault();
}
});

	 
</script>