<?php
/* @var $this ProformaController */
/* @var $model Proforma */

$this->breadcrumbs=array(
	'Proformas'=>array('index'),
	$model->id_proforma,
);

$this->menu=array(
	array('label'=>'List Proforma', 'url'=>array('index')),
	array('label'=>'Create Proforma', 'url'=>array('create')),
	array('label'=>'Update Proforma', 'url'=>array('update', 'id'=>$model->id_proforma)),
	array('label'=>'Delete Proforma', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_proforma),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Proforma', 'url'=>array('admin')),
);
?>

<h1>View Proforma #<?php echo $model->id_proforma; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_proforma',
		'proforma',
		'id_cliente',
		'id_moneda',
		'fecha',
		'id_codigo_producto',
		'dias',
		'cantidad',
		'id_descuento',
		'total',
		'iv',
		'otros_impuestos_id',
		'observaciones',
		'nombre_cliente',
		'id_articulo',
		'precio_aimpu',
		'subtotal',
		'total2',
		'descuento2',
		'credito',
		'efec_monto',
		'tj_nm',
		'tj_ref',
		'tj_monto',
		'ch_bnc',
		'ch_ref',
		'ch_monto',
		'trs_bnc',
		'trs_ref',
		'trs_monto',
		'ot_ref',
		'ot_monto',
		'total_payment',
		'total_completo',
		'vuelto',
		'contingencia',
		'tipoexoneracion',
		'documentoexo',
		'instituexo',
		'fechaexo',
		'impuestoexo',
		'porcentajeexo',
		'n_conting',
		'fecha_conting',
		'razon_cont',
	),
)); ?>
