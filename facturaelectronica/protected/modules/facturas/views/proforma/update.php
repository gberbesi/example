<?php
/* @var $this ProformaController */
/* @var $model Proforma */

$this->breadcrumbs=array(
	'Proformas'=>array('index'),
	$model->id_proforma=>array('view','id'=>$model->id_proforma),
	'Update',
);

$this->menu=array(
	array('label'=>'List Proforma', 'url'=>array('index')),
	array('label'=>'Create Proforma', 'url'=>array('create')),
	array('label'=>'View Proforma', 'url'=>array('view', 'id'=>$model->id_proforma)),
	array('label'=>'Manage Proforma', 'url'=>array('admin')),
);
?>

<h1>Update Proforma <?php echo $model->id_proforma; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>