<?php
/* @var $this FacturaController */
/* @var $model Factura */
/* @var $form CActiveForm */
?>
		<div class="wide form">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'action'=>Yii::app()->createUrl($this->route),
			'method'=>'get',
		)); ?>
		<div class="col col-lg-4 col-xs-12" >
			<label style='color:#22C7FC;font-weight:bold'>Busqueda Cliente o Factura:</label>
		<?php echo $form->textField($model,'proforma',array('style'=>'width:100%','class'=>'form-control','placeholder'=>'Ingrese Nombre o Factura')); ?></div>
		<div class="col col-lg-2 col-xs-12">
			<br>
		<?php echo CHtml::submitButton('BUSCAR', array('class'=>'btn btn-dark btn-lg btn-block')); ?>
	</div>
		<div class="col col-lg-2 col-xs-12">
			<label style='color:#22C7FC;font-weight:bold'>Fecha Desde:</label>
		<?php echo $form->textField($model,'fecha',array('type'=>'text','id'=>'fecha','class'=>'form-control','placeholder'=>date('j/n/Y'))); ?></div>
		<div class="col col-lg-2 col-xs-12">
			<label style='color:#22C7FC;font-weight:bold'>Fecha Hasta:</label><?php echo $form->textField($model,'dias',array('type'=>'text','id'=>'fecha','class'=>'form-control','placeholder'=>date('j/n/Y'))); ?></div>
			   <div class="col col-lg-2 col-xs-12 "><br>
                                <?php
                                    echo CHtml::link('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo ',array('proforma/create'),array('id'=>'bt1','class'=>"btn  btn-block btn-lg",'style'=>'background-color: #99cc33;color:white' ));
                                ?>
                             </div>
<?php $this->endWidget(); ?>
</div><br><!-- search-form -->