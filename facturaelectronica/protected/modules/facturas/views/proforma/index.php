<?php
/* @var $this ProformaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Proformas',
);

$this->menu=array(
	array('label'=>'Create Proforma', 'url'=>array('create')),
	array('label'=>'Manage Proforma', 'url'=>array('admin')),
);
?>

<h1>Proformas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
