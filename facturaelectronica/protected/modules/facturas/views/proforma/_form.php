<?php 
/* @var $this FacturaController */ 
/* @var $model Factura */
/* @var $form CActiveForm */
 date_default_timezone_set('America/Costa_Rica');  

?>

  
   <script src="js/sweetalert.min.js"></script>
    
     <h1 style="color:#2a3f54;"> &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-file"></span><span style="color:black;"> Proforma</span> </h1>


<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'factura-form',

    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false, 
)); ?>

 
<nav class="breadcrumb" style="background-color:white; padding-left: 10px;margin-left: 30px; font-size: 14px;">
  <a class="breadcrumb-item" href="#" style="color:black  "><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;<span style="color:#22C7FC" >Inicio</span> </a><a>/</a>
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('facturas/factura/admin');?>" style="color:black"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Facturación</span> </a><a>/</a>
  
  <span class="breadcrumb-item active" style="color:black"><span class="glyphicon glyphicon-file"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Registro</span>  </span>
</nav>

<div class="form "  style="font-size: 14px; padding-left: 30px;">
  <?php// var_dump($modelPro); ?>

  <ul class="nav nav-tabs">
    <li  class="active"><a data-toggle="tab" href="#facturas" style="color:#22C7FC; font-size: 17px;" ><strong>Facturas</strong></a></li>
    <li><a data-toggle="tab" href="#productos" style="color: #22C7FC; font-size: 17px;"><strong>Productos</strong> </a></li>
    <li><a data-toggle="tab" href="#clientes" style="color: #22C7FC; font-size: 17px;"><strong>Clientes</strong></a></li>
    
  </ul>

  <div class="tab-content">
    <div id="facturas" class="tab-pane fade in active ">
    



   <br>

          <div class="col-md-2  col-sm-6 col-xs-12 ">
               <label for="fullname">Cédula*</label>
          <div class="input-group">
                            
                           <?php echo $form->textField($model,'id_codigo_producto',array('type'=>'text','id'=>'identificacion','class'=>'form-control')); ?>
                             <?php echo $form->error($model,'id_codigo_producto',array('class'=>'btn-xs alert-danger text-center')); ?>
                             <span class="input-group-btn">
                                              <button type="button" class="btn btn-primary" data-toggle="modal" onclick="tipobusqueda()"><span class="fa fa-search-plus"></span></button>
                                          </span>

<?php echo $form->textField($model,'id_cliente',array('type'=>'text','id'=>'id_cliente','class'=>'form-control','placeholder'=>"",'readOnly'=>true,'style'=>'display:none')); ?>
                             <?php echo $form->error($model,'id_codigo_producto',array('class'=>'btn-xs alert-danger text-center')); ?>
                             
                                             
                                          

                          </div>
                          </div>
          <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
    <label for="fullname">Nombre del Cliente*</label>
     <?php echo $form->textField($model,'nombre_cliente',array('type'=>'text','id'=>'nombre_cliente','class'=>'form-control','readOnly'=>true)); ?>
                <?php echo $form->error($model,'nombre_cliente',array('class'=>'btn-xs alert-danger text-center')); ?>
        
    </div>


 <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
      <label for="fullname">Moneda*</label>
              


                  <?php echo $form->dropDownList($model,'id_moneda',CHtml::listData(Moneda::model()->findAll(),'id_moneda', 'moneda'),array('class'=>'form-control select2','style'=>'width:100%','id'=>'id_moneda','onBlur'=>'selectedAlumno8()')) ?>
                        <?php echo $form->error($model,'id_moneda',array('class'=>'btn-xs alert-danger text-center')); ?>


    </div>
     <div class="col-md-1 col-sm-6 col-xs-2 form-group ">
      <label for="fullname">Crédito</label>
                <?php echo $form->checkbox($model,'credito',array('type'=>'text','id'=>'dias2','class'=>'form-control','onclick'=>'habilitaCompras()','style'=>'size:10px;')); ?>
                <?php echo $form->error($model,'credito',array('class'=>'btn-xs alert-danger text-center')); ?>
    </div>
  
    <div class="col-md-1 col-sm-6 col-xs-10 form-group ">
    <label for="fullname">&nbsp;&nbsp;Días</label>
   <?php echo $form->numberField($model,'dias',array('type'=>'text','id'=>'dias','class'=>'form-control','readOnly'=>true)); ?>
                <?php echo $form->error($model,'dias',array('class'=>'btn-xs alert-danger text-center')); ?>
    </div>
    <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
    <label for="fullname">Fecha*</label>
                
<?php echo $form->textField($model,'fecha',array('type'=>'text','id'=>'fecha','class'=>'form-control','value'=>date('j/n/Y H:i:s'),'readOnly'=>true)); ?>
                    
   <?php echo $form->error($model,'fecha',array('class'=>'btn-xs alert-danger text-center')); ?>
                    
      <br>
    </div> 

<div class="col-md-2 col-sm-6 col-xs-12 ">
               <label for="fullname">Código Articulo*</label>
          <div class="input-group">
                            
                           <?php echo $form->textField($model,'id_codigo_producto2',array('type'=>'text','id'=>'id_codigo_producto','class'=>'form-control','placeholder'=>"")); ?>
                             <?php echo $form->error($model,'id_codigo_producto2',array('class'=>'btn-xs alert-danger text-center')); ?>
                             <span class="input-group-btn">
                                              <button type="button" class="btn btn-primary" data-toggle="modal" onclick="busquedapro()" ><span class="fa fa-search-plus"></span></button>
                                          </span>
                          </div>
                          </div>
     <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
     <label for="fullname">Artículo*</label>
      <?php echo $form->textField($model,'id_articulo',array('type'=>'text','id'=>'id_articulo','class'=>'form-control','disabled'=>true)); ?>
                <?php echo $form->error($model,'id_articulo',array('class'=>'btn-xs alert-danger text-center')); ?>
       
    </div>
    <div class="col-md-2 col-lg-1 col-sm-6 col-xs-12 form-group ">
    <label for="fullname">Cantidad*</label>
     <?php echo $form->numberField($model,'cantidad',array('type'=>'text','id'=>'cantidad', 'value'=>1,'class'=>'form-control','onBlur '=>'selectedAlumno4()')); ?>
                <?php echo $form->error($model,'cantidad',array('class'=>'btn-xs alert-danger text-center')); ?> 
        
    </div>
     <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
     <label for="fullname" style="font-size: 13px">Precio a.Impuesto*</label>
      <?php echo $form->textField($model,'precio_aimpu',array('type'=>'text','id'=>'precio_aimpu','class'=>'form-control','onBlur '=>'selectedAlumno5()')); ?>
                <?php echo $form->error($model,'precio_aimpu',array('class'=>'btn-xs alert-danger text-center')); ?>
     
       
    </div>
 <?php
    if(Yii::app()->user->name!='admin'){
      $var='display:none';
    }
     ?>
    <div class="col-md-2 col-sm-6 col-xs-12 pull-left form-group " style="<?php echo $var;?>">
    <label for="fullname">Descuento</label>
     <?php echo $form->dropDownList($model,'id_descuento',CHtml::listData(Descuento::model()->findAll(),'porcentaje', 'descuento'),array('class'=>'form-control select2','style'=>'width:100%','id'=>'id_descuento','onChange'=>'selectedAlumno6()')) ?>
                <?php echo $form->error($model,'id_descuento',array('class'=>'btn-xs alert-danger text-center')); ?>
    <br>  
    </div>
    
    <div class="col-md-2 offset-md-3  ">
               <label for="fullname">Total*</label>
          <div class="input-group">
                            
             <?php echo $form->textField($model,'total',array('type'=>'text','id'=>'total','class'=>'form-control','placeholder'=>"",'disabled'=>true)); ?>
               <?php echo $form->error($model,'total',array('class'=>'btn-xs alert-danger text-center')); ?>
               <span class="input-group-btn">
                    <button type="button" class="btn btn-success" onClick="selectedAlumno7()"><span class="fa fa-plus"></span></button>
                </span>
          </div><br>
          </div>

    <div class="col-md-12 col-sm-12 col-xs-12 form-group ">
      <div class="table table-responsive" id="productotal">
<table class="table table-striped table-responsive">
<tr style="color:#22C7FC;background-color: rgb(238, 238, 238); font-weight: bold">

<td>id</td>
<td>Código</td>
<td>Artículo</td>
  <td>Cantidad</td>
  <td>Precio Antes </td>
  <td>Descuento</td>
  <td>Total</td>
  <td></td>
  <td></td>
  <td></td>

</tr>
 
</table> 
    </div>
  </div><br>
  <div class="col-md-12">
<div class="col-md-1 col-sm-6 col-xs-2   ">
    <label for="fullname" style="font-size: 12px;">Contingencia</label>
       <?php echo $form->checkbox($model,'contingencia',array('type'=>'text','id'=>'contingencia','class'=>'form-control','data-toggle'=>'modal','data-target'=>'#exampleModalC','style'=>'size:10px;')); ?>
                <?php echo $form->error($model,'contingencia',array('class'=>'btn-xs alert-danger text-center')); ?>
     
    </div>
   
<div class="col-md-1 col-sm-6 col-xs-12   ">
    <label for="fullname">Descuento</label>
     <?php echo $form->textField($model,'id_descuento',array('type'=>'text','id'=>'id_descuentot','class'=>'form-control','readonly'=>true)); ?>
                <?php echo $form->error($model,'id_descuento',array('class'=>'btn-xs alert-danger text-center')); ?>
     
    </div>

 <div class="col-md-2 col-sm-6 col-xs-12   ">
    <label for="fullname">Sub Total*</label>
     <?php echo $form->textField($model,'subtotal',array('type'=>'text','id'=>'subtotal','class'=>'form-control','readonly'=>true)); ?>
                <?php echo $form->error($model,'subtotal',array('class'=>'btn-xs alert-danger text-center')); ?>
     
    </div>

    <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
    <label for="fullname">I.V</label>
     <?php echo $form->textField($model,'iv',array('type'=>'text','id'=>'iv','class'=>'form-control','readonly'=>true)); ?>
                <?php echo $form->error($model,'iv',array('class'=>'btn-xs alert-danger text-center')); ?>
        
    </div><br\>

    <div class="col-md-2 col-sm-6 col-xs-12   ">
    <label for="fullname">Otros Impuestos</label>
     <?php echo $form->textField($model,'otros_impuestos_id',array('type'=>'text','id'=>'fullname','class'=>'form-control','readonly'=>true)); ?>
                <?php echo $form->error($model,'otros_impuestos_id',array('class'=>'btn-xs alert-danger text-center')); ?>
     
    </div>
      <div class="col-md-2  col-sm-6 col-xs-12   form-group " style="margin-right: 100px; ">
    <label for="fullname">Total*</label>
     <?php echo $form->textField($model,'total2',array('type'=>'text','id'=>'total2','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
                <?php echo $form->error($model,'total2',array('class'=>'btn-xs alert-danger text-center')); ?>
     
    </div>


    <div class="col-md-1 col-sm-6 col-xs-12 col-lg-6 form-group  ">
    <label for="fullname">Observaciones*</label>
     <?php echo $form->textarea($model,'observaciones',array('type'=>'text','id'=>'fullname','class'=>'form-control')); ?>
                <?php echo $form->error($model,'observaciones',array('class'=>'btn-xs alert-danger text-center')); ?>
      
    </div>
     <div class="col-md-1 col-sm-6 col-xs-2   "  style="display: none">
    <label for="fullname">Fecha Conting</label>
 <?php echo $form->dateField($model,'fecha_conting',array('type'=>'text','id'=>'fecha_conti','class'=>'form-control','readonly'=>true)); ?>
                <?php echo $form->error($model,'fecha_conting',array('class'=>'btn-xs alert-danger text-center')); ?>
     
    </div>
    <div class="col-md-1 col-sm-6 col-xs-2   " style="display: none" >
    <label for="fullname">N# Conting</label>
 <?php echo $form->textField($model,'n_conting',array('type'=>'text','id'=>'n_conting','class'=>'form-control','readonly'=>true)); ?>
                <?php echo $form->error($model,'n_conting',array('class'=>'btn-xs alert-danger text-center')); ?>
     
    </div>
    <div class="col-md-1 col-sm-6 col-xs-2   " style="display: none" >
    <label for="fullname">Razon Conting</label>
 <?php echo $form->textField($model,'razon_cont',array('type'=>'text','id'=>'razon_con','class'=>'form-control','readonly'=>true)); ?>
                <?php echo $form->error($model,'razon_cont',array('class'=>'btn-xs alert-danger text-center')); ?>
     
    </div>

    <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">tabla iv*</label>
     <?php echo $form->textField($model,'ivt',array('type'=>'text','id'=>'ivt','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($model,'ivt',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

    <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">tabla iv*</label>
     <?php echo $form->textField($modelPro,'id_productos',array('type'=>'text','id'=>'id_productos','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($modelPro,'id_productos',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

    
<!-- tabular  -->
<div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">*Id Articulo</label>
     <?php echo $form->textField($tablaProducto,'id_producto',array('type'=>'text','id'=>'id_producto','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'id_producto',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>
<div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">tabla producto*</label>
     <?php echo $form->textField($tablaProducto,'codigo_produc',array('type'=>'text','id'=>'codigo_produc','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'codigo_produc',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

        <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">tabla nombre produc*</label>
     <?php echo $form->textField($tablaProducto,'nombre_produc',array('type'=>'text','id'=>'nombre_produc','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'nombre_produc',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

            <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">tabla total produc*</label>
     <?php echo $form->textField($tablaProducto,'total',array('type'=>'text','id'=>'totalp','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'total',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>


        <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">costo*</label>
     <?php echo $form->textField($tablaProducto,'costo_producto',array('type'=>'text','id'=>'costo_productop','class'=>'form-control','readonly'=>true,'placeholder'=>"",'maxlength'=>8)); ?>
            <?php echo $form->error($tablaProducto,'costo_producto',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div> 

            <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">cantidad*</label>
     <?php echo $form->textField($tablaProducto,'cantidad',array('type'=>'text','id'=>'cantidadp','class'=>'form-control','readonly'=>true)); ?>
            <?php echo $form->error($tablaProducto,'cantidad',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

     <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">descuento*</label>
     <?php echo $form->textField($tablaProducto,'descuento',array('type'=>'text','id'=>'descuentop','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'descuento',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

         <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">descuento*</label>
     <?php echo $form->textField($tablaProducto,'iv',array('type'=>'text','id'=>'ivp','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'iv',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

    <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">Tipo de Documento:*</label>
<?php echo $form->textField($tablaProducto,'tipoexoneracion',array('type'=>'text','id'=>'tipoexoneracionv','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>

            <?php echo $form->error($tablaProducto,'tipoexoneracion',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

    <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">*Número de Documento</label>
     <?php echo $form->textField($tablaProducto,'documentoexo',array('type'=>'text','id'=>'documentoexov','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'documentoexo',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

        <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">*Nombre de la Institución</label>
     <?php echo $form->textField($tablaProducto,'instituexo',array('type'=>'text','id'=>'instituexov','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'instituexo',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

        <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">*Fecha Emisión</label>
     <?php echo $form->dateField($tablaProducto,'fechaexo',array('type'=>'text','id'=>'fechaexov','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'fechaexo',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

        <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">*Monto del Impuesto</label>
     <?php echo $form->textField($tablaProducto,'impuestoexo',array('type'=>'text','id'=>'impuestoexov','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'impuestoexo',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>

        <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none">
    <label for="fullname">*Porcentaje de Compra</label>
     <?php echo $form->textField($tablaProducto,'porcentajeexo',array('type'=>'text','id'=>'porcentajeexov','class'=>'form-control','readonly'=>true,'placeholder'=>"")); ?>
            <?php echo $form->error($tablaProducto,'porcentajeexo',array('class'=>'btn-xs alert-danger text-center')); ?>    
    </div>
  

   
<!-- tabular  -->

</div>
   <div class="row buttons col-md-3  col-xs-12 offset-md-2 pull-right " ><br>  
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Facturar' : 'Save',array('class'=>'btn btn-dark btn-block btn-lg ','style'=>'color:white;background-color:#0f70cd')); ?>
    </div>



      
</div>

    <div id="productos" class="tab-pane fade">
    <br>
            
 <div class="col-md-3  col-xs-12 form-group  ">
                         <label for="fullname">Código del Producto</label>
                            <?php echo $form->textField($modelPro,'codigo',array('type'=>'text','id'=>'procodigo','class'=>'form-control','placeholder'=>'Código','onblur'=>'selectedAlumno20()')); ?> 
                            <?php echo $form->error($modelPro,'codigo',array('class'=>'btn-xs alert-danger text-center')); ?>
                        
                      </div>


                         <div class="col-md-3  col-xs-12 form-group  ">
                       
                            <label for="fullname">Tipo Código  </label>
                          


                             
                        <?php echo $form->dropDownList($modelPro,'tipo_codigo_id',CHtml::listData(TipoCodigo::model()->findAll(),'id_tipo_codigo', 'tipo_codigo'),array('class'=>'form-control select2','style'=>'width:100%','id'=>'tipo_codigo_id')) ?>
                        <?php echo $form->error($modelPro,'tipo_codigo_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                    
                       
                      </div>

                        <div class="col-md-3   col-xs-12 form-group  " >
                         <label for="fullname">Tipo  </label>
                           

                      <p>
                        Producto: <?php echo $form->radioButton($modelPro,'tipo_producto',array('type'=>'radio','class'=>'flat','id'=>'genderM','value'=>0)); ?>&nbsp;&nbsp;&nbsp;&nbsp;
                      
                        Servicio: <?php echo $form->radioButton($modelPro,'tipo_producto',array('type'=>'radio','class'=>'flat','id'=>'genderF')); ?>

                         <?php echo $form->error($modelPro,'tipo_producto',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </p>

                 
                      </div>
                        <div class="col-md-3  col-xs-12 form-group  ">
                         <label for="fullname">Descripción Corta  </label>
                            <?php echo $form->textField($modelPro,'descripcionc',array('type'=>'text','id'=>'descripcioncc','class'=>'form-control','placeholder'=>'Descripción Corta')); ?>
                            <?php echo $form->error($modelPro,'descripcionc',array('class'=>'btn-xs alert-danger text-center')); ?>
                        
                      </div>


                       <div class="col-md-6  col-xs-12 form-group  ">
                           <label for="fullname">Descripción  </label>
                            <?php echo $form->textField($modelPro,'descripcion',array('type'=>'text','id'=>'descripcionll','class'=>'form-control','placeholder'=>'Descripción')); ?>
                            <?php echo $form->error($modelPro,'descripcion',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                      



                      <div class="col-md-3  col-xs-12 form-group  ">
                       
                            <label for="fullname">Unidad de Medida  </label>
                          


                             
                        <?php echo $form->dropDownList($modelPro,'unidad_medida_id',CHtml::listData(UnidadMedida::model()->findAll(),'id_um', 'um'),array('class'=>'form-control select2','style'=>'width:100%')) ?>
                        <?php echo $form->error($modelPro,'unidad_medida_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                    
                       
                      </div>

                       <div class="col-md-6 col-xs-12 form-group ">
                     
                      
                           <div id="dynamicDiv">

                        <table class="table table-responsive table-striped" >
                          <thead align="center" style="color:#22C7FC;background-color: transparent;font-weight: bold;">Lista Impuesto</thead>
                          <tr style="color:#22C7FC;background-color: transparent;font-weight: bold;  ">
                            <td >ID</td>
                            <td colspan="2">Nombre</td>
                            <td>Porcentaje</td>
                          </tr>
                         </table> 
                         
                    </div>
                      </div>

 

                      <div class="col-md-1    col-xs-12 form-group  ">
                         <label for="fullname">Costo ₡ </label>
                            <?php echo $form->textField($modelPro,'precioc_id',array('type'=>'text','id'=>'precioc_id','class'=>'form-control','placeholder'=>'0')); ?><br>
                            <?php echo $form->error($modelPro,'precioc_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                           
                      </div>

                       <div class="col-md-2  col-xs-12 form-group  ">
                           <label for="fullname" style="font-size: 13px;">CostoA.Impuesto ₡ </label>
                            <?php echo $form->textField($modelPro,'preciosinin_puesto',array('type'=>'text','id'=>'preciosinin_puesto','class'=>'form-control','placeholder'=>'0','onBlur'=>"selectedAlumno9()")); ?><br>
                            <?php echo $form->error($modelPro,'preciosinin_puesto',array('class'=>'btn-xs alert-danger text-center')); ?>
                            
                      </div>


                        <div class="col-lg-1  col-xs-12 form-group  ">
                       
                            <label for="fullname">Costo $  </label>
                            <?php echo $form->textField($modelPro,'precios_id',array('type'=>'text','id'=>'precios_id','class'=>'form-control','placeholder'=>'0')); ?><br>
                            <?php echo $form->error($modelPro,'precios_id',array('class'=>'btn-xs alert-danger text-center')); ?>

                       
                       
                      </div>

                      <div class="col-md-2  col-xs-12 form-group  ">
                         
                         <label for="fullname" style="font-size: 13px;">Precio a.Impuesto $  </label>
                            <?php echo $form->textField($modelPro,'preciossinin_puesto',array('type'=>'text','id'=>'preciossinin_puesto','class'=>'form-control','placeholder'=>'0','onBlur'=>'selectedAlumno10()')); ?><br>
                            <?php echo $form->error($modelPro,'preciossinin_puesto',array('class'=>'btn-xs alert-danger text-center')); ?>
                          <br><br><br>  
                      </div>


<div class="col-md-3   col-xs-12 form-group pull-left ">
  <br> 
   <button type="button" class="btn btn-dark btn-block btn-lg" data-toggle="modal" data-target="#exampleModal">
  Agregar
</button>
<br><br>
</div>



                      <div class="col-md-2 col-sm-6 col-xs-12 col-md-offset-1 form-group  ">
                         <label for="fullname">Precio Final ‎₡  </label>
                            <?php echo $form->textField($modelPro,'precio_finalc_id',array('type'=>'text','id'=>'precio_finalc_id','class'=>'form-control','placeholder'=>'0','readonly'=>true)); ?>
                            <?php echo $form->error($modelPro,'precio_finalc_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                       <div class="col-md-2 col-sm-6 col-xs-12  form-group  ">
                           <label for="fullname">Precio Final $  </label>
                            <?php echo $form->textField($modelPro,'precio_finals_id',array('type'=>'text','id'=>'precio_finals_id','class'=>'form-control','placeholder'=>'0','readonly'=>true)); ?>
                            <?php echo $form->error($modelPro,'precio_finals_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                      <div class="col-md-2 col-sm-6 col-xs-12 form-group " style="display:none;">
                           <label for="fullname">tabular oculto $  </label>
                            <?php echo $form->textField($modelPro,'lista_impuesto_id',array('type'=>'text','id'=>'lista_impuesto_id','class'=>'form-control','placeholder'=>"")); ?>
                            <?php echo $form->error($modelPro,'lista_impuesto_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>






                          <div class="col-md-7  col-sm-6 col-xs-12  pull-right">
                         <div class="col-md-4  col-md-offset-5  "><?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-dark btn-block btn-lg','style'=>'font-weight:bold;background-color:#ff333a;')); ?></div>
                         <div class="col-md-3"><?php echo CHtml::link('<span class="glyphicon glyphicon-step-backward " aria-hidden="true"></span>  Ir Atrás',array('/registro/productos/admin'),array('id'=>'bt1','class'=>"btn btn-dark btn-block  btn-lg",'style'=>';font-weight:bold;')); ?></div>

            </div>        
          </div>
 

   <div id="clientes" class="tab-pane fade">
      <h3>Clientes</h3>
      
    <div class="col-md-4 col-sm-6 col-xs-12 form-group ">
                       
                            <label for="fullname">Tipo de Identificación </label>
                          
                          

                             
                        <?php echo $form->dropDownList($modelClie,'id_tipo_identificacioj',CHtml::listData(Identificacion::model()->findAll(),'id_identificacion', 'identificacion'),array('class'=>'form-control select2','style'=>'width:100%')) ?>
                        <?php echo $form->error($modelClie,'id_tipo_identificacioj',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                      <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
                         <label for="fullname">Identificación </label>
                            <?php echo $form->textField($modelClie,'identificacion',array('type'=>'text','id'=>'identificacioncedula','class'=>'form-control','onBlur'=>'selectedAlumno11()')); ?>
                            <?php echo $form->error($modelClie,'identificacion',array('class'=>'btn-xs alert-danger text-center')); ?>
                        
                      </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
                         <label for="fullname">Nombre  </label>
                            <?php echo $form->textField($modelClie,'clientes',array('type'=>'text','id'=>'clientes','class'=>'form-control','placeholder'=>'Nombre')); ?>
                            <?php echo $form->error($modelClie,'clientes',array('class'=>'btn-xs alert-danger text-center')); ?>
                        
                        </div>
                         <div class="col-md-1 col-sm-6 col-xs-12 form-group ">
                           <label for="fullname">Area  </label>
                            <?php echo $form->textField($modelClie,'area1',array('type'=>'numeric','id'=>'fullname','class'=>'form-control','value'=>'506','style'=>'padding:0')); ?>
                            <?php echo $form->error($modelClie,'area1',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                        <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                       
                            <label for="fullname">Telefono  </label>
                            <?php echo $form->textField($modelClie,'telefono',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Teléfono')); ?>
                            <?php echo $form->error($modelClie,'telefono',array('class'=>'btn-xs alert-danger text-center')); ?>
                       
                      </div>
                      <div class="col-md-2 col-sm-6 col-xs-12 form-group  " >
                         <label for="fullname">Area </label>
                            <?php echo $form->textField($modelClie,'area2',array('type'=>'text','id'=>'fullname','class'=>'form-control','value'=>'506','style'=>'font-size: 12px;')); ?>
                            <?php echo $form->error($modelClie,'area2',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>
                      <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
                           <label for="fullname"> Fax  </label>
                            <?php echo $form->textField($modelClie,'fax',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Fax')); ?>
                            <?php echo $form->error($modelClie,'fax',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>
                        <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
                       
                            <label for="fullname"> Email  </label>
                            <?php echo $form->emailField($modelClie,'correo',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Email','email'=>true)); ?>
                            <?php echo $form->error($modelClie,'correo',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>
                      <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                         <label for="fullname">Apartado  </label>
                            <?php echo $form->textField($modelClie,'apartado',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Apartado')); ?>
                            <?php echo $form->error($modelClie,'apartado',array('class'=>'btn-xs alert-danger text-center')); ?>
                        
                      </div>
                      <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                         <label for="fullname"> Web</label>
                            <?php echo $form->urlField($modelClie,'web',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Web','url'=>true)); ?>
                            <?php echo $form->error($modelClie,'web',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>
                       <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                           <label for="fullname"> Pais</label>
                                 
                        <?php echo $form->dropDownList($modelClie,'pais',CHtml::listData(Pais::model()->findAll(),'id_pais', 'pais'),array('class'=>'form-control select2','style'=>'width:100%')) ?>
                        <?php echo $form->error($modelClie,'pais',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>


                      <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                            <label for="fullname"> *Provincia</label>
                            <?php  

                            if ($modelClie->isNewRecord) {
                              $modelClie->id_provincia=1;
                            }

                            ?>
                            <?php
                                    echo $form->dropDownList($modelClie, 'id_provincia', $estadoListData, array(
                                         'class'=>'form-control select2',
                                         'style'=>'width:100%',
                                         'empty'=>'',
                                      
                                         

                                        'ajax'=>array(
                                            'type'=>'POST',
                                            'url'=>$this->createUrl('SelectMunicipio'),
                                            'update'=>'#' . CHtml::activeId($modelClie, 'id_canton')
                                        ),
                                    ));
                                    
                                    ?>
                            <?php echo $form->error($modelClie,'id_provincia',array('class'=>'btn-xs alert-danger text-center'));
                             ?>
                          </div>


                         <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                          <label for="fullname"> *Canton</label>

                           <?php  
                            if (!$model->isNewRecord ){
                                   
                                    $lista1=Canton::model()->find('id_canton=:id_canton',array(':id_canton'=>$model->id_canton));
                                   
                                   
                                    $model->id_canton=$lista1->id_canton;

                                   
                                  }else{
                                    $modelClie->id_canton=1;
                                  }
                          ?>





                           <?php
                                  echo $form->dropDownList($modelClie, 'id_canton',CHtml::listData(Canton::model()->findAll('provincia_id =:provincia_id', array(':provincia_id'=>$modelClie->id_provincia)), 'id_canton', 'canton'), array(
                                        'class'=>'form-control select2',
                                         'style'=>'width:100%',
                                         

                                      'ajax'=>array(
                                          'type'=>'POST',
                                          'url'=>$this->createUrl('SelectParroquia'),
                                          'update'=>'#' . CHtml::activeId($modelClie, 'id_distrito')
                                      ),
                                  ));
                                  ?>
                          <?php echo $form->error($modelClie,'id_canton',array('class'=>'btn-xs alert-danger text-center')); ?>
                          </div>
                          

                          <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                          <label for="fullname"> *Distrito</label>

                           <?php echo $form->dropDownList($modelClie, 'id_distrito',CHtml::listData(Distrito::model()->findAll('canton_id =:canton_id', array(':canton_id'=>$modelClie->id_canton)), 'id_distrito', 'distrito')

                            ,array('class'=>'form-control select2','style'=>'width:100%')); ?>

                          <?php echo $form->error($modelClie,'id_distrito',array('class'=>'btn-xs alert-danger text-center')); ?>
                         </div>





                         
                      <div class="col-md-4 col-sm-6 col-xs-12 form-group ">
                           <label for="fullname"> Email Nº2  </label>
                            <?php echo $form->emailField($modelClie,'correo2',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Email Nº2','email'=>true)); ?>
                            <?php echo $form->error($modelClie,'correo2',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>
                         <div class="col-md-8 col-sm-6 col-xs-12 form-group ">
                           <label for="fullname">  Dirección  </label>
                            <?php echo $form->textArea($modelClie,'direccion',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Dirección','size'=>'100%')); ?>
                            <?php echo $form->error($modelClie,'direccion',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>
               
                    <div class="col-md-7  col-sm-6 col-xs-12  pull-right">
                        <div class="col-md-4  col-md-offset-5 ">
                          
                            <?php echo CHtml::submitButton('Guardar',array('class'=>"btn btn-dark btn-block  btn-lg",'style'=>'background-color:#ff333a;font-weight:bold;')); ?>

                          </div>
                        <div class="col-md-3">
                          <?php echo CHtml::link('<span class="glyphicon glyphicon-step-backward " aria-hidden="true"></span>  Ir Atrás',array('/registro/clientes/admin'),array('id'=>'bt1','class'=>'btn btn-dark btn-block btn-lg','style'=>'font-weight:bold;')); ?>
                           </div>
                          
                        </div>
                      

             
          
</div>
</div>
<!-- /.box -->

</div>
</div>
<?php $this->endWidget()?>
<div id="modales" >
<!-- Modal 1-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
           <h2 class="modal-title" id="exampleModal3Label" style='color:#22C7FC;font-weight:bold'>Agregar Impuesto</h2>
      </div>
      
      <div class="modal-body">
<div class="table table-responsive table-striped">
    
    
    <table class="table">
  <thead>
    <tr style='color:#22C7FC;font-weight:bold'>
      <th scope="col">Añadir</th>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Porcentaje</th>
      
    </tr>
  </thead>

  <?php foreach ($clientes as $key => $value) { 
   echo "<tbody>";
    echo "<tr>";
     echo" <td>    <div class='container'>";
       echo "<button type= 'button' class='btn btn-dark' onClick='selectedAlumno(".$value->id_impuesto.")' id='addInput' name='".$key."' >";
       echo "<span class='glyphicon glyphicon-plus' aria-hidden='true'></span>";
         
      echo "</button>";
  
     echo "<br/>";
      echo "</div></td>";
     echo "<td name='id' id=".$value->id_impuesto.">  ".$value->id_impuesto." </td>";
     echo "<td name='cliente' id=".$value->impuesto_nombre."> ".$value->impuesto_nombre."</td>";
    echo " <td name=identificacion id=".$value->monto." >".$value->monto."</td>";
   
    echo "</tr>";
   
  echo "</tbody>";

  } ?> 
  
    

</table>
</div>
<div class="modal-footer">
        <button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button>
      </div>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->

</div>
</div>

<!-- Modal 3 -->
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
         <h2 class="modal-title" id="exampleModal3Label" style='color:#22C7FC;font-weight:bold'>Agregar Producto</h2>
      </div>
      
      <div class="modal-body">
<div class="table table-responsive table-striped">
    <?php $this->renderPartial('application.modules.registro.views.productos.admin2',array('model'=>$modelPro)); ?>
    
</div>
<div class="modal-footer">
        <button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button>
      </div>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->

</div>
</div>
<!-- Modal Contingencia -->
<div class="modal fade" id="exampleModalC" tabindex="-1" role="dialog" aria-labelledby="exampleModalCLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
        <h2 class="modal-title" id="exampleModalCLabel" style="color:#22C7FC"> Datos de Referencia de Contingencia</h2>
      </div>
      
      <div class="modal-body">
<div class="table table-responsive table-striped">
  <div class="col-md-6 col-sm-6 col-xs-12 form-group ">
  <label for="fullname">*Número</label>
      <?php echo $form->textField($model,'n_conting',array('type'=>'text','id'=>'nu_conting','class'=>'form-control',)); ?>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 form-group ">
  <label for="fullname">*Fecha Emisión</label>
      <?php echo $form->dateField($model,'fecha_conting',array('type'=>'text','id'=>'f_emis','class'=>'form-control',)); ?>
      
    </div>
    <div class="col-md-12  col-sm-6 col-xs-12 form-group ">
  <label for="fullname">*Razón</label>
      <?php echo $form->textArea($model,'razon_cont',array('type'=>'text','id'=>'c_razon','class'=>'form-control',)); ?>
    </div>

    
</div>
<div class="modal-footer">
   <button type="button" class="btn btn-dark btn-lg" onclick="contin();" >Guardar</button>
        <button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button>
      </div>
                  </div><!-- /.box-body -->
            </div><!-- /.box -->

</div>
</div>

<!-- Modal 10 -->



<!-- Modal 2 -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModal2Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
     
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="color:red">&times;</span>
        </button>
           <h2 class="modal-title" id="exampleModal2Label" style="color:#22C7FC; font-weight:bold;">Añadir Cliente</h2>
      </div>
      
      <div class="modal-body">
<div class="table table-responsive table-striped">
 <?php $this->renderPartial('application.modules.registro.views.clientes.admin2',array('model'=>$modelClie)); ?>
    
  
    

</div>

<div class="modal-footer">
        <button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button>
      </div>
</div>

                  </div><!-- /.box-body -->
            </div>
</div>
</div><?php //echo Chtml::encode($form->textField($tablaProducto,'porcentajeexo',array('type'=>'text','id'=>'porcentajeexo','class'=>'form-control','size'=>'100%','onblur'=>'desexo()'))) ; exit;?>
<!-- form -->
<?php $idta=1;?>;
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
 <script>
     

function selectedAlumno(boton) {//boton es lo q atrae del forech

var prueba=$('#genderM:checked').val();//0 producto
var prueba2=$('#genderF:checked').val();//1 servicio

  $.ajax({ 
      method: "POST", 
      //dataType: "json",//trae arry
      dataType: "html",//trae html
      url: "<?php echo CController::createUrl('/facturas/factura/DetalleMaterial') ?>",
      //async: false,

      data: {id_cliente: boton,prueba:prueba, prueba2:prueba2}//valosr q pasa por post para la consulta
    })
    .done(function(msg) {//retorno de la funcion detallematerial


   $('#dynamicDiv').html(msg);//imprime con html un string directo al iv y brra el anterior

  var tablaiv= $('#dynamicDiv table tr').eq(1).find('td').eq(2).text();
 var precioc=$('#preciosinin_puesto').val();
 var precios=$('#preciossinin_puesto').val();
 var tablaivf= $('#dynamicDiv table tr').eq(1).find('td').eq(0).text();
  


if(precioc!=''&& tablaiv!=''){
  var preciofc=((parseInt(precioc)*parseInt(tablaiv))/100);
  var tablaiv= $('#dynamicDiv table tr').eq(1).find('td').eq(2).text();

  $('#precio_finalc_id').val(parseInt(preciofc)+parseInt(precioc));
   }


    if(precios!=''&& tablaiv!=''){

    var preciofs=((parseInt(precios)*parseInt(tablaiv))/100);

     $('#precio_finals_id').val(parseInt(preciofs)+parseInt(precios));
  }
  if(tablaiv==''){
    $('#precio_finals_id').val(precios);
    $('#precio_finalc_id').val(precioc);

  }

$('#lista_impuesto_id').val(tablaivf);

$('#exampleModal').modal('hide');//cierra la pantalla modal

    });
}

function selectedAlumno2(boton) {//boton es lo q atrae del forech

  $.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/facturas/factura/detalleMaterial2') ?>",
      //async: false,
      data: { id_cliente: boton}//valosr q pasa por post para la consulta
    })
    .done(function( msg ) {//retorno de la funcion detallematerial
      //console.log(msg);
      //material = msg;
      //console.log(event);
      //acionAgregar(msg);
      //alert( "Data Saved: " + msg );
      //console.log(msg);

      $('#nombre_cliente').val(msg.clientes);//imprime los valores con el json reutiliza los anteriores
      $('#id_cliente').val(msg.id_cliente);
      $('#identificacion').val(msg.identificacion);




     // $('#dynamicDiv').html(msg);//imprime con html un string directo al iv y brra el anterior

      $('#exampleModal2').modal('hide');//cierra la pantalla modal
    });
}
function selectedAlumno3(boton) {//boton es lo q atrae del forech

  $.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/facturas/factura/detalleMaterial3') ?>",
      //async: false,
      data: { id_cliente: boton}//valosr q pasa por post para la consulta
    })
    .done(function( msg ) {//retorno de la funcion detallematerial
      //console.log(msg);
      //material = msg;
      //console.log(event);
      //acionAgregar(msg);
      //alert( "Data Saved: " + msg );
      //
     //console.log(msg);

     var $moneda = document.getElementById("id_moneda").value; 
   


     if($moneda==1){//colones

      $('#precio_aimpu').val(msg.precioc_id);
      $('#total').val(msg.precioc_id);
     }else{
      $('#precio_aimpu').val(msg.precios_id);
      $('#total').val(msg.precios_id);
     }
      $('#id_codigo_producto').val(msg.codigo);//imprime los valores con el json reutiliza los anteriores
      $('#id_articulo').val(msg.descripcionc);
      
      //$('#total').val(msg.preciosinin_puesto);
      

      $('#ivt').val(msg.lista_impuesto_id);
      $('#id_productos').val(msg.id_productos);
    
     
     // $('#dynamicDiv').html(msg);//imprime con html un string directo al iv y brra el anterior

      $('#exampleModal3').modal('hide');//cierra la pantalla modal
    });
}
function selectedAlumno4() {//boton es lo q atrae del forech
 
var $monto = document.getElementById("cantidad").value;
var $precio = document.getElementById("precio_aimpu").value;
var $descuento= $('#id_descuento').val();


  if($monto<=0)
    {
      swal("Atencion!", "La cantidad ingresada debe ser mayor a 0","error");
      $('#cantidad').val(1);
        }else{
            if($descuento!=''){
              var $total =($monto*$precio);
              var $descuento=((($total*$descuento)/100)-($total))*-1;
              $('#total').val($descuento);
              console.log($descuento);
            }else{
              var $total =($monto*$precio);
              $('#total').val($total);

             }
        }
    }

function selectedAlumno5() {//boton es lo q atrae del forech

var $monto = document.getElementById("cantidad").value;
var $precio = document.getElementById("precio_aimpu").value;
var $descuento= $('#id_descuento').val();


//alert(isNaN($precio));
if(!isNaN($precio)){
 

   if($descuento!=''){
              var $total =($monto*$precio);
              var $descuento=((($total*$descuento)/100)-($total))*-1;
              $('#total').val($descuento);
              console.log($descuento);
            }else{
              var $total =($monto*$precio);
              $('#total').val($total);

             }


}else{

  swal('dato incorrecto!','el campo precio solo acepta numero','error');
  $('#precio_aimpu').val('');
}
  
}
function selectedAlumno6() {//boton es lo q atrae del forech

var $monto = document.getElementById("cantidad").value;
var $precio = document.getElementById("precio_aimpu").value;
var $descuento= document.getElementById("id_descuento").value;

var $total =($monto*$precio);
var $descuento=((($total*$descuento)/100)-($total))*-1;

if(!isNaN($descuento)){
$('#total').val($descuento);
}else{

  swal('dato incorrecto!','el campo descuento solo acepta numero','error');
  $('#id_descuento').val('');
}
  
}

var idtabular=1;


function selectedAlumno7() {//boton es lo q atrae del forech
//principales
var $codigo = document.getElementById("id_codigo_producto").value;
var $articulo = document.getElementById("id_articulo").value;
var $cantidad= document.getElementById("cantidad").value;
var $precio= document.getElementById("precio_aimpu").value;
var $descuento= document.getElementById("id_descuento").value;
var $total= document.getElementById("total").value;
var $subtotal= document.getElementById("subtotal").value;
var $descuentot= document.getElementById("id_descuentot").value;
var $idpro= document.getElementById("id_productos").value;
var $vacio ="";
//segundarios
var $total2= document.getElementById("total2").value;
var $ivs2= document.getElementById("iv").value;//ataja el iva total descontado del precio
var $ivs= document.getElementById("ivt").value;//porcentaje del producto o servicio


//leer tabla para supar productos iguales

var total= $('#productotal table tr');

var vueltas=total.length;

for (var i=1; i<vueltas; i++) {
  
var id= $('#productotal table tr').eq(''+i+'').find('td').eq(0).text();
var codigo= $('#productotal table tr').eq(''+i+'').find('td').eq(1).text();
var articulo= $('#productotal table tr').eq(''+i+'').find('td').eq(2).text();
var cantidad= $('#productotal table tr').eq(''+i+'').find('td').eq(3).text();
var precio= $('#productotal table tr').eq(''+i+'').find('td').eq(4).text();
var descuento= $('#productotal table tr').eq(''+i+'').find('td').eq(5).text();
var total= $('#productotal table tr').eq(''+i+'').find('td').eq(6).text();
var iv= $('#productotal table tr').eq(''+i+'').find('td').eq(7).text();


if(id==$idpro){

if(descuento==$descuento && precio==$precio){
 var idtr=$('#productotal table tr').eq(''+i+'').attr('id');

$cantidad=parseInt($cantidad)+parseInt(cantidad);
$total=parseInt($total)+parseInt(total);
$subtotal=$subtotal-(cantidad*precio);
$ivs2=$ivs2-(((parseInt(total)*parseInt(iv))/100)); 
$("#"+idtr).remove();
}else{


}

}

} 

//fin de productos iguales 

if($ivs==1){ 
$ivs=13;
}
if($ivs==2){
$ivs=10;
}
if($ivs2==""){
  $ivs2=0;
}
if($total2==""){
  $total2=0;
}
 
if($codigo!="" && $articulo !="" && $precio !="" ){  
  
//tabular tabla

$('#productotal table').append('<tr id='+idtabular+'><td>'+$idpro+'</td><td>'+$codigo+'</td><td>'+$articulo+'</td><td>'+$cantidad+'</td><td>'+$precio+'</td><td>'+$descuento+'</td><td>'+$total+'</td><td style="display:none">'+$ivs+'</td><td><a href=# onclick="eliminar('+idtabular+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Modificar" ></span></a></td><td><a href=# onclick="modificar('+idtabular+')"><span class="glyphicon glyphicon-pencil" ></span></a></td><td><a  class="fa fa-pencil-square-o" data-toggle="modal" href=# onclick="exoneracion('+idtabular+','+$ivs+')" ></a> </td></tr>');

//fin tablña tabular


//tabular modal


var modales=
'<div class="modal fade" id="exoneracion'+idtabular+'" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h2 class="modal-title" id="exampleModal3Label"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color:red">&times;</span></button><h5 style="color: #22C7FC; " >DATOS DE EXONERACIÓN PRODUCTO #'+idtabular+'</h5></h2></div><div class="modal-body"><div class="table table-responsive table-striped"><div class="col-md-6 col-sm-6 col-xs-12 form-group "><label for="fullname">  Tipo de Documento:  </label><select class="form-control  " id="tipoexoneracion" style="width:100%" name="TablaProducto[tipoexoneracion]"><option value="1">Compras Autorizadas</option><option value="2">Venta Exentas a Diplomáticos</option><option value="3">Orden de Compra (instituciones Públicas y Otros organismos)</option><option value="4">Exenciones Dirección General de Hacienda</option><option value="5">Zonas Francas</option><option value="6">Otros</option></select><?php  echo $form->error($tablaProducto,'tipoexoneracion',array('class'=>'btn-xs alert-danger text-center')); ?></div><div class="col-md-6 col-sm-6 col-xs-12 form-group "><label for="fullname">  *Número de Documento </label><?php echo $form->textField($tablaProducto,'documentoexo',array('type'=>'text','id'=>'documentoexo','class'=>'form-control','size'=>'100%')); ?><?php echo $form->error($tablaProducto,'documentoexo',array('class'=>'btn-xs alert-danger text-center')); ?></div><div class="col-md-6 col-sm-6 col-xs-12 form-group "><label for="fullname">  *Nombre de la Institución </label><?php echo $form->textField($tablaProducto,'instituexo',array('type'=>'text','id'=>'instituexo','class'=>'form-control','size'=>'100%')); ?><?php echo $form->error($tablaProducto,'instituexo',array('class'=>'btn-xs alert-danger text-center')); ?></div> <div class="col-md-6 col-sm-6 col-xs-12 form-group "><label for="fullname">  *Fecha Emisión </label><input id="fechaexo" class="form-control" placeholder="" name="TablaProducto[fechaexo]" type="date"><?php echo $form->error($tablaProducto,'fechaexo',array('class'=>'btn-xs alert-danger text-center')); ?></div><div class="col-md-6 col-sm-6 col-xs-12 form-group "><label for="fullname">  *Monto del Impuesto  </label><?php echo $form->textField($tablaProducto,"impuestoexo",array('type'=>'text','id'=>"impuestoexo",'class'=>'form-control','size'=>'100%','readonly'=>true)); ?><?php echo $form->error($tablaProducto,'impuestoexo',array('class'=>'btn-xs alert-danger text-center')); ?></div><div class="col-md-6 col-sm-6 col-xs-12 form-group "><label for="fullname">  *Porcentaje de Compra  </label><input type="text" id="porcentajeexo" class="form-control" size="100%" onblur="desexo(\''+idtabular+'\')" name="TablaProducto[porcentajeexo]" /><?php echo $form->error($tablaProducto,'porcentajeexo',array('class'=>'btn-xs alert-danger text-center')); ?></div>  <div class="col-md-8 col-sm-6 col-xs-12 form-group "  style="display:none"><label for="fullname">  *oculto factura  </label><?php echo $form->textField($model,'idfactura',array('type'=>'text','id'=>'idfactura','class'=>'form-control','size'=>'100%','disabled'=>true)); ?><?php echo $form->error($model,'idfactura',array('class'=>'btn-xs alert-danger text-center')); ?></div><div class="col-md-8 col-sm-6 col-xs-12 form-group " style="display:none"><label for="fullname">  *oculto iva  </label><?php echo $form->textField($model,'ivafactura',array('type'=>'text','id'=>"ivafactura",'class'=>'form-control','size'=>'100%','disabled'=>true)); ?><?php echo $form->error($model,'ivafactura',array('class'=>'btn-xs alert-danger text-center')); ?></div></div><div class="modal-footer"><a href="#" onclick="modexo('+idtabular+')"><button  class="btn btn-dark btn-lg" style="color:white;background-color:#990033">Guardar</button></a><button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button></div></div></div></div></div>';
$('#modales').append(modales);

idtabular++;



//fin tabular modal
//vaciar campos
$('#id_codigo_producto').val($vacio);
$('#id_articulo').val($vacio);
$('#cantidad').val(1);
$('#precio_aimpu').val($vacio);
$('#id_descuento').val(0);  
$('#total').val($vacio);
$('#ivt').val($vacio);
//fin vaciado de campos


//calculo de campos finales 
var $precio_final=(parseInt($total2)+parseInt($total));
var $iv_final=((parseInt($total)*parseInt($ivs))/100);
var $finaliv=(parseInt($iv_final)+parseInt($ivs2));
if($subtotal==''){
var $subtotalt=parseInt($precio)*parseInt($cantidad);
$('#subtotal').val(parseInt($subtotalt));
}else{
var $subtotalt=(parseInt($subtotal)+(parseInt($precio)*parseInt($cantidad)) );
$('#subtotal').val(parseInt($subtotalt));
}
if($descuentot==''){
 var $descuentof=((parseInt($descuento)*(parseInt($precio)*parseInt($cantidad)))/100);
 $('#id_descuentot').val(parseInt($descuentof));
}else{
 var $descuentoneto= ((parseInt($descuento)*(parseInt($precio)*parseInt($cantidad)))/100);
  var $descuentof=(parseInt($descuentoneto)+parseInt($descuentot));
  $('#id_descuentot').val(parseInt($descuentof));
}

$('#iv').val(parseInt($finaliv));
$('#total2').val((parseInt($subtotalt)+parseInt($finaliv))-parseInt($descuentof));

//fin de calculos

//alerta de error
}else{
  swal('Faltan Datos!','Faltan datos para agregar este producto','warning');
}
//fin de alerta 

}
function selectedAlumno8() {//boton es lo q atrae del forech

var $moneda = document.getElementById("id_codigo_producto").value;

 $.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",
      url: "<?php echo CController::createUrl('/facturas/factura/detalleMaterial4') ?>",
      //async: false,
      data: { id_cliente: $moneda}//valosr q pasa por post para la consulta
    })
    .done(function( msg ) {//retorno de la funcion detallematerial
      //console.log(msg);
      //material = msg;
      //console.log(event);
      //acionAgregar(msg);
      //alert( "Data Saved: " + msg );
      //
    // console.log(msg);

     var $moneda = document.getElementById("id_moneda").value;
     if($moneda==1){//colones

      $('#precio_aimpu').val(msg.precioc_id);

     }else{
      $('#precio_aimpu').val(msg.precios_id);

     }
           
     
     // $('#dynamicDiv').html(msg);//imprime con html un string directo al iv y brra el anterior

    });
  
}
function selectedAlumno9() {

   var colonesfinal= $('#preciosinin_puesto').val();
  var tablaiv= $('#dynamicDiv table tr').eq(1).find('td').eq(2).text();
 
 if(parseInt(colonesfinal)>0){


if(isNaN(colonesfinal)){

 swal("Atencion!", "Precio a.Impu ₡ no puede ser letra","error");
  $( "#preciosinin_puesto" ).val('');

 }else{
   if(tablaiv!=''){

 var preciofs=((parseInt(colonesfinal)*parseInt(tablaiv))/100);

 var colonesfinal3=parseInt(colonesfinal)+parseInt(preciofs) ; 

$('#precio_finalc_id').val(colonesfinal3);

   }else{

     var colonesfinal3=colonesfinal; 

$('#precio_finalc_id').val(colonesfinal3);
   }
 

 }  
 }else{
swal("Atencion!", "Precio a.Impu ₡ no puede ser menor a 0","error");
  $( "#preciosinin_puesto" ).val('');
  $('#precio_finalc_id').val('');
  
 }


}

function selectedAlumno10() {

   var colonesfinal= $('#preciossinin_puesto').val();
  var tablaiv= $('#dynamicDiv table tr').eq(1).find('td').eq(2).text();
 
if(parseInt(colonesfinal)>0){



   if(isNaN(colonesfinal)){

 swal("Atencion!", "Precio a.Impu $ no puede ser letra","error");
  $( "#preciossinin_puesto" ).val('');

 }else{

 if(tablaiv!=''){

 var preciofs=((parseInt(colonesfinal)*parseInt(tablaiv))/100);

 var colonesfinal3=parseInt(colonesfinal)+parseInt(preciofs) ; 

 $('#precio_finals_id').val(colonesfinal3);

   }else{
      var colonesfinal3=colonesfinal;
      $('#precio_finals_id').val(colonesfinal3);
  
   } 

 }
 }else{
 swal("Atencion!", "Precio a.Impu $ no puede ser menor a 0","error");
  $( "#preciossinin_puesto" ).val('');
  $('#precio_finals_id').val('');

 }
}

function habilitaCompras()
{
    var estadoActual = document.getElementById("dias");
    var vacio = "";

 
    if(estadoActual.readOnly==true)
    {
        estadoActual.readOnly= false;
       
    }
    else
    {
        estadoActual.readOnly= true;
        $('#dias2').val(vacio);
    }
}


$( "#factura-form" ).submit(function( event ) {

//#facturas #productos #clientes
var href=$('.active a').attr('href');
//comienzo factura 

if(href=='#clientes'){
if($('#identificacioncedula').val()=='' || $('#Clientes_id_tipo_identificacioj').val()=='')
   {
      swal('Faltan Datos!!','Los campos con un * al inicio no pueden ir vacíos clientes','warning');
      event.preventDefault(); 
   }
var tipo=$( "#Clientes_id_tipo_identificacioj" ).val();
var cedula=$( "#identificacioncedula" ).val();

if(isNaN(cedula)){
 swal("Atencion!", "Identificación solo puede ser numerico.","error");
 $( "#identificacion" ).val('');
  event.preventDefault();
}

 else if(tipo==1)
{
  if(cedula.length<9 ||cedula.length>9){

  swal("Atencion!", "El número de identificación para Cédula Física debe estar compuesta por 9 digítos numéricos.","error");
  event.preventDefault();
}

}
else if(tipo==2)
{
  if(cedula.length<10 ||cedula.length>10){

  swal("Atencion!", "El número de identificación para Cédula Jurídica debe estar compuesta por 10 digítos numéricos.","error");
  event.preventDefault();
}

}
else if(tipo==3)
{
  if(cedula.length!=11 ||cedula.length!=12){

  swal("Atencion!", "El número de identificación para DIMEX debe estar compuesta por 11 o 12 digítos numéricos.","error");
  event.preventDefault();
}

}
else if(tipo==4)
{
  if(cedula.length!=10 ){

  swal("Atencion!", "El número de identificación para NITE debe estar compuesta por 10 digítos numéricos.","error");
  event.preventDefault();
}

}
else if(cedula!=4)
{
  if(cedula.length!=10 ){

  swal("Atencion!", "El número de identificación para NITE debe estar compuesta por 10 digítos numéricos.","error");
  event.preventDefault();
}

}


}

if(href=='#productos'){
if($('#procodigo').val()=='' || $('#tipo_codigo_id').val()=='' || $('#descripcioncc').val()=='' || $('#descripcionll').val()=='' )
   {
      swal('Faltan Datos!!','Los campos con un * al inicio no pueden ir vacíos producto','warning');
      event.preventDefault(); 

   }

}

if(href=='#facturas'){

  if($('#total2').val()!='')
   {

    var total= $('#productotal table tr');//tamaño de la tabla
    //campos q almacenan string para guardas en la bbdd
    var arrayid="";
    var arraycodigo="";
    var arrayarticulo="";
    var arraycantidad="";
    var arrayprecio="";
    var arraydescuento="";
    var arraytotal="";
    var arrayiv="";
//fin campos q almacenan string para guardas en la bbdd
//campos q almasenaran el string
var arraytipoexoneracion='';
var arraydocumentoexo='';
var arrayinstituexo='';
var arrayfechaexo='';
var arrayimpuestoexo='';
var arrayporcentajeexo='';
//fin campos q almasenaran el string

    var vueltas=total.length;//cantidad de vueltas del for

   for (var i=1; i<vueltas; i++) {
  
//valores tabulares de la tabla
var idarticulo= $('#productotal table tr').eq(''+i+'').find('td').eq(0).text();
var codigo= $('#productotal table tr').eq(''+i+'').find('td').eq(1).text();
var articulo= $('#productotal table tr').eq(''+i+'').find('td').eq(2).text();
var cantidad= $('#productotal table tr').eq(''+i+'').find('td').eq(3).text();
var precio= $('#productotal table tr').eq(''+i+'').find('td').eq(4).text();
var descuento= $('#productotal table tr').eq(''+i+'').find('td').eq(5).text();
var total= $('#productotal table tr').eq(''+i+'').find('td').eq(6).text();
var iv= $('#productotal table tr').eq(''+i+'').find('td').eq(7).text();

if(arrayid==""){

arrayid=idarticulo;

}else if(arrayid!=""){

arrayid=arrayid+","+idarticulo;

}

if(arraycodigo==""){

arraycodigo=codigo;

}else if(arraycodigo!=""){

arraycodigo=arraycodigo+","+codigo;

}

if(arrayarticulo==""){

arrayarticulo=articulo;

}else if(arrayarticulo!=""){

arrayarticulo=arrayarticulo+","+articulo;

}



if(arraycantidad==""){

arraycantidad=cantidad;

}else if(arraycantidad!=""){

arraycantidad=arraycantidad+","+cantidad;

}  


if(arrayprecio==""){

arrayprecio=precio;

}else if(arrayprecio!=""){

arrayprecio=arrayprecio+","+precio;

}


if(arraydescuento==""){

arraydescuento=descuento;

}else if(arraydescuento!=""){

arraydescuento=arraydescuento+","+descuento;

}

if(arraytotal==""){

arraytotal=total;

}else if(arraytotal!=""){

arraytotal=arraytotal+","+total;

}

if(arrayiv==""){

arrayiv=iv;

}else if(arrayiv!=""){

arrayiv=arrayiv+","+iv;

}
// finvalores tabulares de la tabla

//valores tabulares de la modal


//campos de la modales
var tipoexoneracion=$('#exoneracion'+i+' #tipoexoneracion').val();
var documentoexo=$('#exoneracion'+i+' #documentoexo').val();
var instituexo=$('#exoneracion'+i+' #instituexo').val();
var fechaexo=$('#exoneracion'+i+' #fechaexo').val();
var impuestoexo=$('#exoneracion'+i+' #impuestoexo').val();
var porcentajeexo=$('#exoneracion'+i+' #porcentajeexo').val();
//fin campos de la modales

//llena los datos del array
if(arraytipoexoneracion==""){
if(tipoexoneracion==""){
arraytipoexoneracion='0';
}else{
  arraytipoexoneracion=tipoexoneracion;
}

}else if(arraytipoexoneracion!=""){

arraytipoexoneracion=arraytipoexoneracion+","+tipoexoneracion;

}

if(arraydocumentoexo==""){

  if(documentoexo==""){
arraydocumentoexo='0';
}else{
  arraydocumentoexo=documentoexo;
}

}else if(arraydocumentoexo!=""){

arraydocumentoexo=arraydocumentoexo+","+documentoexo;

}

if(arrayinstituexo==""){

  if(instituexo==""){
 arrayinstituexo='0';
}else{
  arrayinstituexo=instituexo;
}

}else if(arrayinstituexo!=""){

arrayinstituexo=arrayinstituexo+","+instituexo;

}

if(arrayfechaexo==""){
if(fechaexo==""){
arrayfechaexo='0';
}else{
  arrayfechaexo=fechaexo;
}

}else if(arrayfechaexo!=""){

arrayfechaexo=arrayfechaexo+","+fechaexo;

}

if(arrayimpuestoexo==""){
  if(impuestoexo==""){
arrayimpuestoexo='0';
}else{
  arrayimpuestoexo=impuestoexo;
}

}else if(arrayimpuestoexo!=""){

arrayimpuestoexo=arrayimpuestoexo+","+impuestoexo;

}

if(arrayporcentajeexo==""){
if(porcentajeexo==""){
arrayporcentajeexo='0';
}else{
  arrayporcentajeexo=porcentajeexo;
}

}else if(arrayporcentajeexo!=""){

arrayporcentajeexo=arrayporcentajeexo+","+porcentajeexo;

}
//fin llena los datos del array

//fin valores tabulares de la modal

}

//asigna array de modular de exoneracion
$('#tipoexoneracionv').val(arraytipoexoneracion);
$('#documentoexov').val(arraydocumentoexo);
$('#instituexov').val(arrayinstituexo);
$('#fechaexov').val(arrayfechaexo);
$('#impuestoexov').val(arrayimpuestoexo);
$('#porcentajeexov').val(arrayporcentajeexo);
//fin

//asigna array de la tabla al campo
$('#id_producto').val(arrayid);
$('#codigo_produc').val(arraycodigo);
$('#nombre_produc').val(arrayarticulo);
$('#totalp').val(arraytotal);
$('#cantidadp').val(arraycantidad);
$('#costo_productop').val(arrayprecio);
$('#descuentop').val(arraydescuento);
$('#ivp').val(arrayiv);
//fin asignacion array de la tabla al campo


   }else{
 
      swal('Atención!','No puede generar un documento con monto total en cero','warning');
      event.preventDefault();     
     
   }
 }
  //factura 
});

function eliminar(id)
{

var codigo= $('#productotal table tr#'+id+'').find('td').eq(1).text();
var articulo= $('#productotal table tr#'+id+'').find('td').eq(2).text();
var cantidad= $('#productotal table tr#'+id+'').find('td').eq(3).text();
var precio= $('#productotal table tr#'+id+'').find('td').eq(4).text();
var descuento= $('#productotal table tr#'+id+'').find('td').eq(5).text();
var total= $('#productotal table tr#'+id+'').find('td').eq(6).text();
var iv=$('#productotal table tr#'+id+'').find('td').eq(7).text();
//tabular
var subtotalt=$('#subtotal').val();
var totalt=$('#total2').val();
var ivat=$('#iv').val();
var descuentot=$('#id_descuentot').val();
//subtotal
var $subtotal=((subtotalt)-((precio)*(cantidad)));
$('#subtotal').val($subtotal);



//total
var $totalconiva= ((parseInt(total)*parseInt(iv))/100);
var $totalconivaf=parseInt($totalconiva)+parseInt(total); 

var $total=(parseInt(totalt)-parseInt($totalconivaf));

$('#total2').val(parseInt($total));

//iv
var $iva=parseInt(ivat)-parseInt($totalconiva); 
$('#iv').val($iva);



//descuento
var $descuento=((parseInt(precio)*parseInt(cantidad))*parseInt(descuento))/100;
var $descuentof=parseInt(descuentot)-$descuento; 

$('#id_descuentot').val($descuentof);
$("#"+id).remove(); 
}

function modificar(id)
{
var codigo= $('#productotal table tr#'+id+'').find('td').eq(1).text();
var articulo= $('#productotal table tr#'+id+'').find('td').eq(2).text();
var cantidad= $('#productotal table tr#'+id+'').find('td').eq(3).text();
var precio= $('#productotal table tr#'+id+'').find('td').eq(4).text();
var descuento= $('#productotal table tr#'+id+'').find('td').eq(5).text();
var total= $('#productotal table tr#'+id+'').find('td').eq(6).text();
var iv=$('#productotal table tr#'+id+'').find('td').eq(7).text();

//tabular
var subtotalt=$('#subtotal').val();
var totalt=$('#total2').val();
var ivat=$('#iv').val();
var descuentot=$('#id_descuentot').val();


$('#id_codigo_producto').val(codigo);
$('#id_articulo').val(articulo);
$('#cantidad').val(cantidad);
$('#precio_aimpu').val(precio);

$('#id_descuento').val(descuento);
$('#total').val(total);
$('#ivt').val(iv);

//subtotal
var $subtotal=(parseInt(subtotalt)-(parseInt(precio)*parseInt(cantidad)));

$('#subtotal').val($subtotal);

//total
var $totalconiva=((parseInt(total)*parseInt(iv))/100);
var $totalconivaf=parseInt($totalconiva)+parseInt(total); 

var $total=(parseInt(totalt)-parseInt($totalconivaf));

$('#total2').val(parseInt($total));

//iv
var $iva=parseInt(ivat)-parseInt($totalconiva); 
$('#iv').val($iva);



//descuento
var $descuento=((parseInt(precio)*parseInt(cantidad))*parseInt(descuento))/100;
var $descuentof=parseInt(descuentot)-$descuento; 

$('#id_descuentot').val($descuentof);


$("#"+id).remove(); 

}

function selectedAlumno11(){

  var cedula=$( "#identificacioncedula" ).val();

$.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/facturas/factura/detalleMaterial5') ?>", 
      //async: false,
      data: { id_cliente: cedula}//valosr q pasa por post para la consulta
    })
    .done(function( msg ) {

if(msg==1){
  swal("Atencion!", "identificacion no se puede repetir","error");
  $( "#identificacioncedula" ).val('');
}
    });


}

function eliminarp(){
var tablaiv= $('#dynamicDiv table tr').eq(1).find('td').eq(2).text();
 var precioc=$('#preciosinin_puesto').val();
 var preciocf=$('#precio_finalc_id').val();

 var precios=$('#preciossinin_puesto').val();
 var preciosf=$('#precio_finals_id').val();




 var impuestoc=(parseInt(precioc)*parseInt(tablaiv))/100;
 var impuestos=(parseInt(precios)*parseInt(tablaiv))/100;

if(precios!=''){
   $('#precio_finals_id').val(parseInt(preciosf)-parseInt(impuestos));
}
if(precioc!=''){
 $('#precio_finalc_id').val(parseInt(preciocf)-parseInt(impuestoc));

}

  $("#1").remove(); 
}

function selectedAlumno20() {

  var codigo= $( "#procodigo" ).val();

 $.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/registro/productos/detalleMaterial2') ?>", 
      //async: false,
      data: { id_cliente: codigo}//valosr q pasa por post para la consulta
    })
    .done(function( msg ) {

if(msg==1){
  swal("Atención!", "Código del Producto ya existe","error");
  $( "#procodigo" ).val('');
}
    });
 

}
function exoneracion(id,iv) {
if(iv!=0){
  $('#exoneracion'+id+'').modal();
  $('#exoneracion'+id+' #idfactura ').val(id);
  $('#exoneracion'+id+' #ivafactura').val(iv);
}else{
    swal('error!','producto no tiene impuesto','warning');
}
}

function desexo(idfac) {

 var idfac=$('#exoneracion'+idfac+' #idfactura').val();//factura q se va a modificar (linea tr)
 var porexo=$('#exoneracion'+idfac+' #porcentajeexo').val();//porcentaje q se le descontara al iv
 var finiva= $('#exoneracion'+idfac+' #impuestoexo').val();//como quedara el iv
 var actualiv=$('#exoneracion'+idfac+' #ivafactura').val();//impuesto actual e la factura

if(porexo>100 || porexo<0){
if(porexo>100){
  swal("Atencion!", "La cantidad ingresada debe ser menor de 100","warning");
}
if(porexo<0){
  swal("Atencion!", "La cantidad ingresada debe ser mayor a 0","warning");
}


}else{
 var nuevoiv=(actualiv*porexo)/100;//porcentaje descontado
var nuevoivfin=actualiv-nuevoiv;//descontado menos actual
$('#exoneracion'+idfac+' #impuestoexo').val(nuevoivfin);
}


}

function modexo(id){

if($('#exoneracion'+id+' #documentoexo').val()=="" || $('#exoneracion'+id+' #instituexo').val()=="" || $('#exoneracion'+id+' #fechaexo').val()=="" || $('#exoneracion'+id+' #impuestoexo').val()=="" || $('#exoneracion'+id+' #porcentajeexo').val()==""){
swal("Atencion!", "Los campos marcados con * son obligatorios","warning");
}else{
var $idpro= $('#productotal table tr#'+id+'').find('td').eq(0).text();
var $codigo= $('#productotal table tr#'+id+'').find('td').eq(1).text();
var $articulo= $('#productotal table tr#'+id+'').find('td').eq(2).text();
var $cantidad= $('#productotal table tr#'+id+'').find('td').eq(3).text();
var $precio= $('#productotal table tr#'+id+'').find('td').eq(4).text();
var $descuento= $('#productotal table tr#'+id+'').find('td').eq(5).text();
var $total= $('#productotal table tr#'+id+'').find('td').eq(6).text();
var $iv=$('#productotal table tr#'+id+'').find('td').eq(7).text();
//nuevo iv
var nuevoiv=$('#exoneracion'+id+' #impuestoexo').val();
$ivn=nuevoiv;
$("#"+id).remove();
$('#productotal table').append('<tr id='+id+'><td>'+$idpro+'</td><td>'+$codigo+'</td><td>'+$articulo+'</td><td>'+$cantidad+'</td><td>'+$precio+'</td><td>'+$descuento+'</td><td>'+$total+'</td><td >'+$ivn+'</td><td><a href=# onclick="eliminar('+id+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Modificar" ></span></a></td><td><a href=# onclick="modificar('+id+')"><span class="glyphicon glyphicon-pencil" ></span></a></td><td><a  class="fa fa-pencil-square-o" data-toggle="modal" href=# onclick="exoneracion('+id+','+parseInt($ivn)+')" ></a> </td></tr>');
//valores finales
var descuento=$("#id_descuento").val();
var subtotal=$("#subtotal").val();
var ivatotal=$("#iv").val();
var totalfinal=$("#total2").val();
var $ivaent = ivatotal;
ivatotal2=(parseInt(nuevoiv)*(parseInt($precio)*parseInt($cantidad)))/100;
var $resta=$iv-$ivn;
var $muere=$ivaent-$resta;
ivatotal=$ivaent-$iv;
$("#iv").val($muere);
totalfinal=parseInt(descuento)+parseInt(ivatotal)+parseInt(subtotal);
$("#total2").val(totalfinal);
$('#exoneracion'+id+'').modal('hide');
}
}

function contin(){
  var n_con=$('#nu_conting').val();
  var f_em=$('#f_emis').val();
  var razon=$('#c_razon').val();
  
 
  $("#n_conting").val(n_con);
  $("#fecha_conti").val(f_em);
  $("#razon_con").val(razon);
  $('#exampleModalC').modal('hide');
  }
  function tipobusqueda(){
    
   var identificacion=$('#identificacion').val();
   if(identificacion!=''){
    if(!isNaN(identificacion)){

    $.ajax({ 
      method: "POST", 
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/facturas/factura/Tipobusqueda') ?>",
      //async: false,

      data: {id_cliente: identificacion}
    })
    .done(function(msg) {
      if(msg==1){
      swal("No encontrado!!", "Este Cliente no se encuentra registrado","warning");
      $('#identificacion').val("");
       }else{
 
      $('#nombre_cliente').val(msg.clientes);
      $('#id_cliente').val(msg.id_cliente);
      $('#identificacion').val(msg.identificacion);
       }
     
});
     }else{
      swal("No encontrado!!", "Este Cliente no se encuentra registrado","warning");
      $('#identificacion').val("");
     }
   }else{
    $('#exampleModal2').modal();
   }
    

  }
function busquedapro(){
   var identificacion=$('#id_codigo_producto').val();
   if(identificacion!=''){

   $.ajax({ 
      method: "POST", 
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/facturas/factura/Busquedapro') ?>",
      //async: false,

      data: {id_cliente: identificacion} 
    })
    .done(function(msg) {
      if(msg==1){
      swal("No encontrado!!", "Este Producto no se encuentra registrado","warning");
      $('#id_codigo_producto').val("");
       }else{
        var $moneda = document.getElementById("id_moneda").value; 

     if($moneda==1){//colones

      $('#precio_aimpu').val(msg.precioc_id);
      $('#total').val(msg.precioc_id);
     }else{
      $('#precio_aimpu').val(msg.precios_id);
      $('#total').val(msg.precios_id);
     }
      $('#id_codigo_producto').val(msg.codigo);//imprime los valores con el json reutiliza los anteriores
      $('#id_articulo').val(msg.descripcionc);
      
      //$('#total').val(msg.preciosinin_puesto);
      

      $('#ivt').val(msg.lista_impuesto_id);
      $('#id_productos').val(msg.id_productos);
       }
     
});

   }else{
    $('#exampleModal3').modal();
   }

}

 </script>

<?php if($_GET['id']){ ?>

 <script > 
$( document ).ready(function() {
var $_GET = <?php echo json_encode($_GET); ?>;
  $.ajax({ 
      method: "POST", 
    
      dataType: "json",
      url: "<?php echo CController::createUrl('/facturas/factura/Updatetabular') ?>",
      //async: false,

      data: {id_cliente: $_GET['id']}
    })
    .done(function(msg) {
      console.log(msg);
    for(var i=0;i<msg.length;i++){
$('#productotal table').append('<tr id='+idtabular+'><td>'+msg[i].id_producto+'</td><td>'+msg[i].codigo_produc+'</td><td>'+msg[i].nombre_produc+'</td><td>'+msg[i].cantidad+'</td><td>'+msg[i].costo_producto+'</td><td>'+msg[i].descuento+'</td><td>'+msg[i].total+'</td><td style="display:none">'+msg[i].iv+'</td><td><a href=# onclick="eliminar('+idtabular+')" ><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Modificar" ></span></a></td><td><a href=# onclick="modificar('+idtabular+')"><span class="glyphicon glyphicon-pencil" ></span></a></td><td><a  class="fa fa-pencil-square-o" data-toggle="modal" href=# onclick="exoneracion('+idtabular+','+msg[i].iv+')" ></a> </td></tr>');

//fin tablña tabular


//tabular modal

var modales=
'<div class="modal fade" id="exoneracion'+idtabular+'" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><h2 class="modal-title" id="exampleModal3Label"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" style="color:red">&times;</span></button><h5 style="color: #22C7FC; " >DATOS DE EXONERACIÓN PRODUCTO #'+idtabular+'</h5></h2></div><div class="modal-body"><div class="table table-responsive table-striped"><div class="col-md-6 col-sm-6 col-xs-12 form-group "><label for="fullname">  Tipo de Documento:  </label><select class="form-control  " id="tipoexoneracion" style="width:100%" name="TablaProducto[tipoexoneracion]"><option value="1" >Compras Autorizadas</option><option value="2" >Venta Exentas a Diplomáticos</option><option value="3">Orden de Compra (instituciones Públicas y Otros organismos)</option><option value="4">Exenciones Dirección General de Hacienda</option><option value="5">Zonas Francas</option><option value="6">Otros</option></select><?php  echo $form->error($tablaProducto,'tipoexoneracion',array('class'=>'btn-xs alert-danger text-center')); ?></div><div class="col-md-6 col-sm-6 col-xs-12 form-group "><label for="fullname">  *Número de Documento </label><input type="text" id="documentoexo" class="form-control" size="100%" value="'+msg[i].documentoexo+'" name="TablaProducto[documentoexo]"></div><div class="col-md-6 col-sm-6 col-xs-12 form-group "><label for="fullname">  *Nombre de la Institución </label><input type="text" id="instituexo" class="form-control" size="100%" name="TablaProducto[instituexo]"  value="'+msg[i].instituexo+'"></div> <div class="col-md-6 col-sm-6 col-xs-12 form-group "><label for="fullname">  *Fecha Emisión </label><input id="fechaexo" class="form-control" placeholder="" name="TablaProducto[fechaexo]" type="date" value="'+msg[i].fechaexo+'"></div><div class="col-md-6 col-sm-6 col-xs-12 form-group "><label for="fullname">  *Monto del Impuesto  </label><input type="text" id="impuestoexo" class="form-control" size="100%" readonly="readonly" name="TablaProducto[impuestoexo]" value="'+msg[i].impuestoexo+'"></div><div class="col-md-6 col-sm-6 col-xs-12 form-group "><label for="fullname">  *Porcentaje de Compra  </label><input type="text" id="porcentajeexo" class="form-control" size="100%" onblur="desexo(\''+idtabular+'\')" name="TablaProducto[porcentajeexo]" value="'+msg[i].porcentajeexo+'"/></div>  <div class="col-md-8 col-sm-6 col-xs-12 form-group "  style="display:none"><label for="fullname">  *oculto factura  </label><?php echo $form->textField($model,'idfactura',array('type'=>'text','id'=>'idfactura','class'=>'form-control','size'=>'100%','disabled'=>true)); ?><?php echo $form->error($model,'idfactura',array('class'=>'btn-xs alert-danger text-center')); ?></div><div class="col-md-8 col-sm-6 col-xs-12 form-group " style="display:none"><label for="fullname">  *oculto iva  </label><?php echo $form->textField($model,'ivafactura',array('type'=>'text','id'=>"ivafactura",'class'=>'form-control','size'=>'100%','disabled'=>true)); ?><?php echo $form->error($model,'ivafactura',array('class'=>'btn-xs alert-danger text-center')); ?></div></div><div class="modal-footer"><a href="#" onclick="modexo('+idtabular+')"><button  class="btn btn-dark btn-lg" style="color:white;background-color:#990033">Guardar</button></a><button type="button" class="btn btn-dark btn-lg" data-dismiss="modal">Cerrar</button></div></div></div></div></div>';
$('#modales').append(modales);
idtabular++;
}
});


 });</script>

<?php } ?>
 
<?php if($_GET['guard']==1){ ?>
  <script>
$( document ).ready(function() {

 swal("Atencion!", "Proforma guardada con exito","success");
});
</script>
  <?php } ?>


<?php if($_GET['update']==1){ ?>
  <script>
$( document ).ready(function() {
 swal("Atencion!", "Proforma actualizada con exito","success");

});
</script>
  <?php } ?>


  