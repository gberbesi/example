<?php
/* @var $this ProformaController */
/* @var $model Proforma */

$this->breadcrumbs=array(
	'Proformas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Proforma', 'url'=>array('index')),
	array('label'=>'Manage Proforma', 'url'=>array('admin')),
);
?> 

 
<?php $this->renderPartial('_form', array('model'=>$model,'modelPro'=>$modelPro,'modelClie'=>$modelClie,'clientes'=>$clientes,'clientes2'=>$clientes2,'clientes3'=>$clientes3,'tablaProducto'=>$tablaProducto,'estadoListData'=>$estadoListData)); ?> 