<?php

/**
 * This is the model class for table "tabla_producto_proforma".
 *
 * The followings are the available columns in table 'tabla_producto_proforma':
 * @property integer $id
 * @property integer $id_factura
 * @property integer $id_cliente
 * @property integer $id_producto
 * @property string $codigo_produc
 * @property string $nombre_produc
 * @property integer $costo_producto
 * @property integer $cantidad
 * @property integer $total
 * @property integer $descuento
 * @property integer $iv
 * @property integer $documentoexo
 * @property string $tipoexoneracion
 * @property string $instituexo
 * @property string $fechaexo
 * @property integer $impuestoexo
 * @property integer $porcentajeexo
 */
class TablaProductoProforma extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tabla_producto_proforma';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_factura, id_cliente, codigo_produc, nombre_produc, costo_producto, cantidad, total, descuento, iv, documentoexo, tipoexoneracion, instituexo, fechaexo, impuestoexo, porcentajeexo', 'required'),
			array('id_factura, id_cliente, id_producto, costo_producto, cantidad, total, descuento, iv, documentoexo, impuestoexo, porcentajeexo', 'numerical', 'integerOnly'=>true),
			array('codigo_produc', 'length', 'max'=>11),
			array('tipoexoneracion, instituexo', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_factura, id_cliente, id_producto, codigo_produc, nombre_produc, costo_producto, cantidad, total, descuento, iv, documentoexo, tipoexoneracion, instituexo, fechaexo, impuestoexo, porcentajeexo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_factura' => 'Id Factura',
			'id_cliente' => 'Id Cliente',
			'id_producto' => 'Id Producto',
			'codigo_produc' => 'Codigo Produc',
			'nombre_produc' => 'Nombre Produc',
			'costo_producto' => 'Costo Producto',
			'cantidad' => 'Cantidad',
			'total' => 'Total',
			'descuento' => 'Descuento',
			'iv' => 'Iv',
			'documentoexo' => 'Documentoexo',
			'tipoexoneracion' => 'Tipoexoneracion',
			'instituexo' => 'Instituexo',
			'fechaexo' => 'Fechaexo',
			'impuestoexo' => 'Impuestoexo',
			'porcentajeexo' => 'Porcentajeexo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_factura',$this->id_factura);
		$criteria->compare('id_cliente',$this->id_cliente);
		$criteria->compare('id_producto',$this->id_producto);
		$criteria->compare('codigo_produc',$this->codigo_produc,true);
		$criteria->compare('nombre_produc',$this->nombre_produc,true);
		$criteria->compare('costo_producto',$this->costo_producto);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('total',$this->total);
		$criteria->compare('descuento',$this->descuento);
		$criteria->compare('iv',$this->iv);
		$criteria->compare('documentoexo',$this->documentoexo);
		$criteria->compare('tipoexoneracion',$this->tipoexoneracion,true);
		$criteria->compare('instituexo',$this->instituexo,true);
		$criteria->compare('fechaexo',$this->fechaexo,true);
		$criteria->compare('impuestoexo',$this->impuestoexo);
		$criteria->compare('porcentajeexo',$this->porcentajeexo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TablaProductoProforma the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
