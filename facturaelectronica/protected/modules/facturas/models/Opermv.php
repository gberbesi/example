<?php

/**
 * This is the model class for table "opermv".
 *
 * The followings are the available columns in table 'opermv':
 * @property integer $id_operti
 * @property integer $id_opermv
 * @property integer $id_articulo
 * @property integer $id_almacen
 * @property integer $id_vendedor
 * @property double $costounit
 * @property double $preciooriginal
 * @property double $dsctounit
 * @property double $preciofin
 * @property double $dsctoprc
 * @property double $dsctomtolinea
 * @property double $cantidad
 * @property double $montoneto
 * @property double $totimpuesto
 * @property string $notas
 * @property string $fechayhora
 * @property double $cant_devuelta
 * @property double $cant_facturada
 * @property integer $es_materia_prima
 * @property integer $id_articulo_padre
 * @property integer $id_unidad
 *
 * The followings are the available model relations:
 * @property Kardex[] $kardexes
 * @property Almacenes $idAlmacen
 * @property Articulo $idArticulo
 * @property Vendedores $idVendedor
 * @property Operti $idOperti
 * @property Articulo $idArticuloPadre
 * @property OpermvImpuestos[] $opermvImpuestoses
 */
class Opermv extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'opermv';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_operti, id_articulo, id_almacen, id_vendedor, notas, fechayhora, id_unidad', 'required'),
            array('id_operti, id_articulo, id_almacen, id_vendedor, es_materia_prima, id_articulo_padre, id_unidad', 'numerical', 'integerOnly'=>true),
            array('costounit, preciooriginal, dsctounit, preciofin, dsctoprc, dsctomtolinea, cantidad, montoneto, totimpuesto, cant_devuelta, cant_facturada', 'numerical'),
            array('notas', 'length', 'max'=>250),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_operti, id_opermv, id_articulo, id_almacen, id_vendedor, costounit, preciooriginal, dsctounit, preciofin, dsctoprc, dsctomtolinea, cantidad, montoneto, totimpuesto, notas, fechayhora, cant_devuelta, cant_facturada, es_materia_prima, id_articulo_padre, id_unidad', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'kardexes' => array(self::HAS_MANY, 'Kardex', 'id_opermv'),
            'idAlmacen' => array(self::BELONGS_TO, 'Almacenes', 'id_almacen'),
            'idArticulo' => array(self::BELONGS_TO, 'Articulo', 'id_articulo'),
            'idVendedor' => array(self::BELONGS_TO, 'Vendedores', 'id_vendedor'),
            'idOperti' => array(self::BELONGS_TO, 'Operti', 'id_operti'),
            'idArticuloPadre' => array(self::BELONGS_TO, 'Articulo', 'id_articulo_padre'),
            'opermvImpuestoses' => array(self::HAS_MANY, 'OpermvImpuestos', 'id_opermv'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_operti' => 'Id',
            'id_opermv' => 'Numero',
            'id_articulo' => 'Nombre del producto',
            'cliente_n'=>'Nombre de Cliente',
            'id_almacen' => 'ID del almacen o deposito',
            'id_vendedor' => 'ID Auto-numérico que representa el código del vendedor',
            'costounit' => 'costo unitario del producto, viene la tabla artículo, por lo general es el costo promedio',
            'preciooriginal' => 'Es el precio bruto que viene de la tabla articulo',
            'dsctounit' => 'es el monto por descuento (unitario) que se resta al precio original',
            'preciofin' => 'Es el precio resultante de restar el precio original menos el descuento unitario',
            'dsctoprc' => 'Porcentaje de descuento aplicado al precio original',
            'dsctomtolinea' => 'monto por descuento aplicado multiplicado por la cantidad',
            'cantidad' => 'Cantidad de unidades por linea',
            'montoneto' => 'resultado de multiplicar el preciofin por la cantidad',
            'totimpuesto' => '(REVISAR) porcentaje de impuesto aplicado ',
            'notas' => 'Notas o comentarios por linea',
            'fechayhora' => 'Fecha y hora en la cual se registró la linea',
            'cant_devuelta' => 'Cant Devuelta',
            'cant_facturada' => 'Cant Facturada',
            'es_materia_prima' => 'Es Materia Prima',
            'id_articulo_padre' => 'Id Articulo Padre',
            'id_unidad' => 'Id Unidad',

        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_operti',$this->id_operti);
        $criteria->compare('id_opermv',$this->id_opermv);
        $criteria->compare('id_articulo',$this->id_articulo);
        $criteria->compare('id_almacen',$this->id_almacen);
        $criteria->compare('id_vendedor',$this->id_vendedor);
        $criteria->compare('costounit',$this->costounit);
        $criteria->compare('preciooriginal',$this->preciooriginal);
        $criteria->compare('dsctounit',$this->dsctounit);
        $criteria->compare('preciofin',$this->preciofin);
        $criteria->compare('dsctoprc',$this->dsctoprc);
        $criteria->compare('dsctomtolinea',$this->dsctomtolinea);
        $criteria->compare('cantidad',$this->cantidad);
        $criteria->compare('montoneto',$this->montoneto);
        $criteria->compare('totimpuesto',$this->totimpuesto);
        $criteria->compare('notas',$this->notas,true);
        $criteria->compare('fechayhora',$this->fechayhora,true);
        $criteria->compare('cant_devuelta',$this->cant_devuelta);
        $criteria->compare('cant_facturada',$this->cant_facturada);
        $criteria->compare('es_materia_prima',$this->es_materia_prima);
        $criteria->compare('id_articulo_padre',$this->id_articulo_padre);
        $criteria->compare('id_unidad',$this->id_unidad);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Opermv the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}