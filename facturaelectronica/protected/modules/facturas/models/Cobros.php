
<?php

/**
 * This is the model class for table "cobros".
 *
 * The followings are the available columns in table 'cobros':
 * @property integer $id_registro
 * @property integer $id_tipo_documento
 * @property integer $id_vendedor
 * @property integer $id_operti
 * @property integer $id_tipo_forma_de_pago
 * @property string $serie
 * @property string $numero_documento
 * @property string $fecha_doc
 * @property double $monto
 * @property string $referencia
 * @property string $confirmacion
 * @property string $concepto
 * @property integer $procede_cre
 * @property integer $uso_interno
 * @property integer $id_tipo_moneda
 * @property double $factor_cambio
 *
 * The followings are the available model relations:
 * @property TipoDocumentos $idTipoDocumento
 * @property Vendedores $idVendedor
 * @property Operti $idOperti
 * @property TipoFormaDePago $idTipoFormaDePago
 * @property TiposMonedas $idTipoMoneda
 */
class Cobros extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'cobros';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('serie, numero_documento, id_tipo_moneda', 'required'),
            array('id_tipo_documento, id_vendedor, id_operti, id_tipo_forma_de_pago, procede_cre, uso_interno, id_tipo_moneda', 'numerical', 'integerOnly'=>true),
            array('monto, factor_cambio', 'numerical'),
            array('serie', 'length', 'max'=>3),
            array('numero_documento', 'length', 'max'=>10),
            array('referencia', 'length', 'max'=>40),
            array('confirmacion', 'length', 'max'=>20),
            array('fecha_doc, concepto', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_registro, id_tipo_documento, id_vendedor, id_operti, id_tipo_forma_de_pago, serie, numero_documento, fecha_doc, monto, referencia, confirmacion, concepto, procede_cre, uso_interno, id_tipo_moneda, factor_cambio', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idTipoDocumento' => array(self::BELONGS_TO, 'TipoDocumentos', 'id_tipo_documento'),
            'idVendedor' => array(self::BELONGS_TO, 'Vendedores', 'id_vendedor'),
            'idOperti' => array(self::BELONGS_TO, 'Operti', 'id_operti'),
            'idTipoFormaDePago' => array(self::BELONGS_TO, 'TipoFormaDePago', 'id_tipo_forma_de_pago'),
            'idTipoMoneda' => array(self::BELONGS_TO, 'TiposMonedas', 'id_tipo_moneda'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_registro' => 'Id Auto-numérico que representa el registro',
            'id_tipo_documento' => 'Id Auto-numérico que representa el tipo de documento a afectar (recibo)',
            'id_vendedor' => 'Id Auto-numérico que representa al vendedor-cobrador',
            'id_operti' => 'Id que representa al documento afectado con el cobro',
            'id_tipo_forma_de_pago' => 'ID que representa la forma de pago (Efectivo, Cheque, etc)',
            'serie' => 'Número de serie del recibo',
            'numero_documento' => 'Numero del recibo',
            'fecha_doc' => 'fecha del recibo',
            'monto' => 'monto cobrado',
            'referencia' => 'Numero de referencia del instrumento de pago usado (ej. nro de la tarjeta de credito)',
            'confirmacion' => 'Número de confirmación de la transacción por parte del banco',
            'concepto' => 'Explicación del concepto del cobro (opcional)',
            'procede_cre' => '0=cobro normal / 1=procede de la aplicación de una nota de crédito del cliente o de terceros',
            'uso_interno' => 'Uso Interno',
            'id_tipo_moneda' => 'Id Tipo Moneda',
            'factor_cambio' => 'Factor Cambio',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_registro',$this->id_registro);
        $criteria->compare('id_tipo_documento',$this->id_tipo_documento);
        $criteria->compare('id_vendedor',$this->id_vendedor);
        $criteria->compare('id_operti',$this->id_operti);
        $criteria->compare('id_tipo_forma_de_pago',$this->id_tipo_forma_de_pago);
        $criteria->compare('serie',$this->serie,true);
        $criteria->compare('numero_documento',$this->numero_documento,true);
        $criteria->compare('fecha_doc',$this->fecha_doc,true);
        $criteria->compare('monto',$this->monto);
        $criteria->compare('referencia',$this->referencia,true);
        $criteria->compare('confirmacion',$this->confirmacion,true);
        $criteria->compare('concepto',$this->concepto,true);
        $criteria->compare('procede_cre',$this->procede_cre);
        $criteria->compare('uso_interno',$this->uso_interno);
        $criteria->compare('id_tipo_moneda',$this->id_tipo_moneda);
        $criteria->compare('factor_cambio',$this->factor_cambio);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Cobros the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}