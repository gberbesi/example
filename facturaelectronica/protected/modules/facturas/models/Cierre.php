<?php

/**
 * This is the model class for table "cierre".
 *
 * The followings are the available columns in table 'cierre':
 * @property string $id_cierre
 * @property string $fecha_cierre
 * @property integer $id_usuario
 * @property integer $institucion_id
 * @property integer $tipo_cierrre
 */
class Cierre extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'cierre';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('fecha_cierre, id_usuario, institucion_id, tipo_cierre, st_cierre', 'required'),
            array('id_usuario, institucion_id, tipo_cierre', 'numerical', 'integerOnly'=>true),
            array('fecha_cierre', 'length', 'max'=>30),
            
            array('id_usuario','safe'),
            array('cierre','safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_cierre, fecha_cierre, id_usuario, institucion_id, tipo_cierre,st_cierre', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_cierre' => 'Id Cierre',
            'fecha_cierre' => 'Fecha Cierre',
            'id_usuario' => 'Id Usuario',
            'institucion_id' => 'Institucion',
            'tipo_cierre' => 'Tipo Cierre',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_cierre',$this->id_cierre,true);
        $criteria->compare('fecha_cierre',$this->fecha_cierre,true);
        $criteria->compare('id_usuario',$this->id_usuario);
        $criteria->compare('institucion_id',$this->institucion_id);
        $criteria->compare('tipo_cierrre',$this->tipo_cierrre);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Cierre the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}