<?php

/**
 * This is the model class for table "factura".
 *
 * The followings are the available columns in table 'factura':
 * @property integer $id_factura 
 * @property string $factura
 * @property integer $id_cliente
 * @property integer $id_moneda
 * @property string $fecha
 * @property string $id_codigo_producto
 * @property integer $dias
 * @property integer $cantidad
 * @property integer $id_descuento
 * @property integer $total
 * @property integer $iv
 * @property integer $otros_impuestos_id
 * @property string $observaciones
 * @property string $nombre_cliente
 * @property integer $id_articulo
 * @property integer $precio_aimpu
 * @property integer $subtotal
 * @property integer $total2
 * @property integer $descuento2
 * @property integer $credito
 * @property integer $efec_monto
 * @property integer $tj_nm
 * @property integer $tj_ref
 * @property integer $tj_monto
 * @property integer $ch_bnc
 * @property integer $ch_ref
 * @property integer $ch_monto
 * @property integer $trs_bnc
 * @property integer $trs_ref
 * @property integer $trs_monto
 * @property integer $ot_ref
 * @property integer $ot_monto
 * @property integer $total_payment
 * @property integer $total_completo
 * @property integer $vuelto
 * @property integer $contingencia 
 */
class Factura extends CActiveRecord
{
    public $id_provincia;
    public $id_canton;
    public $id_distrito;

    public $idfactura;
    public $ivafactura;
    public $ivt;
    public $id_codigo_producto2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'factura';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
           /* array('factura, id_cliente, id_moneda, fecha, id_codigo_producto, dias, cantidad, id_descuento, total, iv, otros_impuestos_id, observaciones, nombre_cliente, id_articulo, precio_aimpu, subtotal, total2, descuento2, credito, efec_monto, tj_nm, tj_ref, tj_monto, ch_bnc, ch_ref, ch_monto, trs_bnc, trs_ref, trs_monto, ot_ref, ot_monto, total_payment, total_completo, vuelto, contingencia', 'required'),*/
           array('nombre_cliente','required'),
           array('id_articulo','safe'),

            array('id_cliente, id_moneda, dias, cantidad, id_descuento, total, iv, otros_impuestos_id, id_articulo, precio_aimpu, subtotal, total2, descuento2, credito, efec_monto, tj_nm, tj_ref, tj_monto, ch_bnc, ch_ref, ch_monto, trs_bnc, trs_ref, trs_monto, ot_ref, ot_monto, total_payment, total_completo, vuelto, contingencia', 'numerical'),

            array('factura, id_codigo_producto', 'length', 'max'=>20),
            array('observaciones', 'length', 'max'=>100),
            array('fecha','safe'),
            array('iv','safe'),
            array('n_conting,fecha_conting,razon_cont','safe'),

            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_factura, factura, id_cliente, id_moneda, fecha, id_codigo_producto, dias, cantidad, id_descuento, total, iv, otros_impuestos_id, observaciones, nombre_cliente, id_articulo, precio_aimpu, subtotal, total2, descuento2, credito, efec_monto, tj_nm, tj_ref, tj_monto, ch_bnc, ch_ref, ch_monto, trs_bnc, trs_ref, trs_monto, ot_ref, ot_monto, total_payment, total_completo, vuelto, contingencia,n_conting,fecha_conting,razon_cont', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
              'idCliente' => array(self::BELONGS_TO, 'Clientes', 'id_cliente'),
            'idMoneda' => array(self::BELONGS_TO, 'Moneda', 'id_moneda'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */

    public function attributeLabels()
    {
        return array(
            'id_factura' => 'Id Factura',
            'factura' => 'Factura',
            'id_cliente' => 'Id Cliente',
            'id_moneda' => 'Id Moneda',
            'fecha' => 'Fecha',
            'id_codigo_producto' => 'Id Codigo Producto',
            'dias' => 'Dias',
            'cantidad' => 'Cantidad',
            'id_descuento' => 'Id Descuento',
            'total' => 'Total',
            'iv' => 'Iv',
            'otros_impuestos_id' => 'Otros Impuestos',
            'observaciones' => 'Observaciones',
            'nombre_cliente' => 'Nombre Cliente',
            'id_articulo' => 'Id Articulo',
            'precio_aimpu' => 'Precio Aimpu',
            'subtotal' => 'Subtotal',
            'total2' => 'Total2',
            'descuento2' => 'Descuento2',
            'credito' => 'Credito',
            'efec_monto' => 'Efec Monto',
            'tj_nm' => 'Tj Nm',
            'tj_ref' => 'Tj Ref',
            'tj_monto' => 'Tj Monto',
            'ch_bnc' => 'Ch Bnc',
            'ch_ref' => 'Ch Ref',
            'ch_monto' => 'Ch Monto',
            'trs_bnc' => 'Trs Bnc',
            'trs_ref' => 'Trs Ref',
            'trs_monto' => 'Trs Monto',
            'ot_ref' => 'Ot Ref',
            'ot_monto' => 'Ot Monto',
            'total_payment' => 'Total Payment',
            'total_completo' => 'Total Completo',
            'vuelto' => 'Vuelto',
            'contingencia' => 'Contingencia',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {   
        // @todo Please modify the following code to remove attributes that should not be searched.
        Yii::import("application.modules.usuario.models.Usuario", true); 
       
        $id=(int)Yii::app()->user->id;
       // var_dump($id);exit;
        $clientes=Usuario::model()->find('usuario_id='.$id.'');
        $inst=$clientes->institucion_id;

    
                $criteria=new CDbCriteria;
        $criteria->join="LEFT JOIN moneda m1 ON t.id_moneda = m1.id_moneda LEFT JOIN clientes c1 ON t.id_cliente = c1.id_cliente";
        
       
   
       // $criteria->addBetweenCondition('fecha',$this->fecha,$this->dias,true);
        $criteria->addSearchCondition('c1.identificacion',$this->factura,true, 'OR');
        $criteria->addSearchCondition('c1.clientes',$this->factura,true, 'OR');
        $criteria->addSearchCondition('id_factura',$this->factura,true, 'OR');
        $criteria->addSearchCondition('m1.aka',$this->factura,true, 'OR');
        $criteria->addSearchCondition('total2',$this->factura,true, 'OR');
        $criteria->addSearchCondition('institucion_id',$inst);



        $sort=new CSort();
             $sort->defaultOrder='id_factura DESC';
              $sort->attributes=array('id_factura'=>array(
                                    'asc'=>'id_factura',
                                    'desc'=>'id_factura DESC',
                                ),
                                'fecha'=>array(
                                    'asc'=>'fecha ',
                                    'desc'=>'fecha DESC'
                                ),
                                'total2'=>array(
                                    'asc'=>'total2',
                                    'desc'=>'total2 DESC'
                                ),
                                'dias'=>array(
                                    'asc'=>'dias',
                                    'desc'=>'dias DESC'
                                ),
            );
             return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort'=>$sort,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Factura the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}