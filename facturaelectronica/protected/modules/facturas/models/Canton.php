<?php

/**
 * This is the model class for table "canton".
 *
 * The followings are the available columns in table 'canton':
 * @property integer $id_canton
 * @property string $canton
 * @property integer $id_distrito
 * @property integer $id_provincia
 *
 * The followings are the available model relations:
 * @property Provincia $idProvincia
 */
class Canton extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'canton';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('canton, id_distrito, id_provincia', 'required'),
            array('id_distrito, id_provincia', 'numerical', 'integerOnly'=>true),
            array('canton', 'length', 'max'=>20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_canton, canton, id_distrito, id_provincia', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idProvincia' => array(self::BELONGS_TO, 'Provincia', 'id_provincia'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_canton' => 'Id Canton',
            'canton' => 'Canton',
            'id_distrito' => 'Id Distrito',
            'id_provincia' => 'Id Provincia',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_canton',$this->id_canton);
        $criteria->compare('canton',$this->canton,true);
        $criteria->compare('id_distrito',$this->id_distrito);
        $criteria->compare('id_provincia',$this->id_provincia);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Canton the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}