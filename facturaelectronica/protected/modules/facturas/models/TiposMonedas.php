
<?php

/**
 * This is the model class for table "tipos_monedas".
 *
 * The followings are the available columns in table 'tipos_monedas':
 * @property integer $id_tipo_moneda
 * @property string $codigo
 * @property string $simbolo
 * @property integer $estatus
 * @property double $factor_cambio
 * @property integer $extranjera
 * @property integer $multiplicar_dividir
 * @property string $plural
 * @property string $singular
 *
 * The followings are the available model relations:
 * @property Cobros[] $cobroses
 * @property Configuracion[] $configuracions
 */
class TiposMonedas extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tipos_monedas';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('codigo, simbolo, factor_cambio, plural, singular', 'required'),
            array('estatus, extranjera, multiplicar_dividir', 'numerical', 'integerOnly'=>true),
            array('factor_cambio', 'numerical'),
            array('codigo', 'length', 'max'=>6),
            array('simbolo', 'length', 'max'=>5),
            array('plural, singular', 'length', 'max'=>60),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_tipo_moneda, codigo, simbolo, estatus, factor_cambio, extranjera, multiplicar_dividir, plural, singular', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cobroses' => array(self::HAS_MANY, 'Cobros', 'id_tipo_moneda'),
            'configuracions' => array(self::HAS_MANY, 'Configuracion', 'id_tipo_moneda'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_tipo_moneda' => 'Id Tipo Moneda',
            'codigo' => 'Codigo',
            'simbolo' => 'Simbolo',
            'estatus' => 'Estatus',
            'factor_cambio' => 'Factor Cambio',
            'extranjera' => 'Extranjera',
            'multiplicar_dividir' => 'Multiplicar Dividir',
            'plural' => 'Plural',
            'singular' => 'Singular',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_tipo_moneda',$this->id_tipo_moneda);
        $criteria->compare('codigo',$this->codigo,true);
        $criteria->compare('simbolo',$this->simbolo,true);
        $criteria->compare('estatus',$this->estatus);
        $criteria->compare('factor_cambio',$this->factor_cambio);
        $criteria->compare('extranjera',$this->extranjera);
        $criteria->compare('multiplicar_dividir',$this->multiplicar_dividir);
        $criteria->compare('plural',$this->plural,true);
        $criteria->compare('singular',$this->singular,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TiposMonedas the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}