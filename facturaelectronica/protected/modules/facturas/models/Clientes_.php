<?php

/**
 * This is the model class for table "clientes".
 *
 * The followings are the available columns in table 'clientes':
 * @property integer $id_cliente
 * @property integer $id_tipo_cliente
 * @property integer $id_tipo_contribuyente
 * @property integer $id_tipo_precios
 * @property integer $id_vendedor
 * @property integer $id_tipo_documento
 * @property string $codigo
 * @property string $nombre
 * @property string $doc_identificacion
 * @property string $fecha_inicio
 * @property string $idfiscal1
 * @property string $idfiscal2
 * @property string $direccion
 * @property string $historia
 * @property string $motivo_suspend
 * @property string $telefonos
 * @property string $telefono_movil
 * @property double $limite_credito
 * @property integer $dias_credito
 * @property integer $tolerancia
 * @property integer $estatus
 * @property string $nota
 * @property string $email
 * @property string $website
 * @property double $descuento_prc
 * @property string $persona_contacto
 * @property integer $id_ciudad
 * @property integer $id_estado
 * @property integer $id_pais
 * @property integer $id_sector
 * @property integer $id_tipo_doc_identificacion
 * @property integer $usa_credito
 *
 * The followings are the available model relations:
 * @property Vendedores $idVendedor
 * @property TipoDocumentos $idTipoDocumento
 * @property Sectores $idSector
 * @property TipoClientes $idTipoCliente
 * @property TipoContribuyente $idTipoContribuyente
 * @property TipoPrecios $idTipoPrecios
 * @property Ciudades $idCiudad
 * @property Estados $idEstado
 * @property Paises $idPais
 * @property TiposDocIdentificacion $idTipoDocIdentificacion
 * @property Hashtag[] $hashtags
 * @property Operti[] $opertis
 * @property Usuarios[] $usuarioses
 */
class Clientes extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'clientes';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_tipo_cliente, id_tipo_contribuyente, id_tipo_precios, fecha_inicio, direccion, historia, motivo_suspend, id_tipo_doc_identificacion', 'required'),
            array('id_tipo_cliente, id_tipo_contribuyente, id_tipo_precios, id_vendedor, id_tipo_documento, dias_credito, tolerancia, estatus, id_ciudad, id_estado, id_pais, id_sector, id_tipo_doc_identificacion, usa_credito', 'numerical', 'integerOnly'=>true),
            array('limite_credito, descuento_prc', 'numerical'),
            array('codigo', 'length', 'max'=>20),
            array('nombre, telefonos, telefono_movil, email, website', 'length', 'max'=>150),
            array('doc_identificacion, idfiscal1, idfiscal2', 'length', 'max'=>25),
            array('direccion', 'length', 'max'=>200),
            array('historia, nota', 'length', 'max'=>250),
            array('motivo_suspend, persona_contacto', 'length', 'max'=>100),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_cliente, id_tipo_cliente, id_tipo_contribuyente, id_tipo_precios, id_vendedor, id_tipo_documento, codigo, nombre, doc_identificacion, fecha_inicio, idfiscal1, idfiscal2, direccion, historia, motivo_suspend, telefonos, telefono_movil, limite_credito, dias_credito, tolerancia, estatus, nota, email, website, descuento_prc, persona_contacto, id_ciudad, id_estado, id_pais, id_sector, id_tipo_doc_identificacion, usa_credito', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idVendedor' => array(self::BELONGS_TO, 'Vendedores', 'id_vendedor'),
            'idTipoDocumento' => array(self::BELONGS_TO, 'TipoDocumentos', 'id_tipo_documento'),
            'idSector' => array(self::BELONGS_TO, 'Sectores', 'id_sector'),
            'idTipoCliente' => array(self::BELONGS_TO, 'TipoClientes', 'id_tipo_cliente'),
            'idTipoContribuyente' => array(self::BELONGS_TO, 'TipoContribuyente', 'id_tipo_contribuyente'),
            'idTipoPrecios' => array(self::BELONGS_TO, 'TipoPrecios', 'id_tipo_precios'),
            'idCiudad' => array(self::BELONGS_TO, 'Ciudades', 'id_ciudad'),
            'idEstado' => array(self::BELONGS_TO, 'Estados', 'id_estado'),
            'idPais' => array(self::BELONGS_TO, 'Paises', 'id_pais'),
            'idTipoDocIdentificacion' => array(self::BELONGS_TO, 'TiposDocIdentificacion', 'id_tipo_doc_identificacion'),
            'hashtags' => array(self::HAS_MANY, 'Hashtag', 'id_cliente'),
            'opertis' => array(self::HAS_MANY, 'Operti', 'id_cliente'),
            'usuarioses' => array(self::HAS_MANY, 'Usuarios', 'id_cliente'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_cliente' => 'Id auto-numerico que identifica al cliente',
            'id_tipo_cliente' => 'Id auto-numerico que representa el tipo de cliente, segun tabla respectiva',
            'id_tipo_contribuyente' => 'ID Auto-numero que representa el tipo de contribuyente',
            'id_tipo_precios' => 'ID Auto-numérico que representa el tipo precio con el cual se manejará el cliente',
            'id_vendedor' => 'Id Vendedor',
            'id_tipo_documento' => 'ID Auto-Numèrico que indica el numero de serie por default que usarà el cliente (opcional)',
            'codigo' => 'Código publico del cliente',
            'nombre' => 'Nombre del cliente',
            'doc_identificacion' => 'numero de cedula del cliente',
            'fecha_inicio' => 'Desde cuando es cliente de la empresa',
            'idfiscal1' => 'Numero id fiscal (rif, ruc, etc)',
            'idfiscal2' => 'Numero fiscal secundario (DV, NIT, Etc)',
            'direccion' => 'Direcciòn del cliente',
            'historia' => 'narrativa de la historia del cliente, el usuario puede hacer anotaciones varias sobre el desempeño del cliente',
            'motivo_suspend' => 'Motivo de suspención, en el caso que status sea 1',
            'telefonos' => 'numeros de teléfonos del cliente',
            'telefono_movil' => 'número de celular(es) del cliente',
            'limite_credito' => 'monto del límite de crédito otorgado al cliente',
            'dias_credito' => 'Días de crédito otorgados al cliente',
            'tolerancia' => 'Son dias extras al límite de credio que se le otorgan al cliente',
            'estatus' => 'Status del cliente (0=activo / 1=suspendido)',
            'nota' => 'notas o comentarios sobre el cliente',
            'email' => 'Direccion(es) de correo del cliente',
            'website' => 'URL de la página del cliente',
            'descuento_prc' => 'Porcentaje de descuento otorgado al cliente por convenio',
            'persona_contacto' => 'nombre de la persona contacto',
            'id_ciudad' => 'Id Ciudad',
            'id_estado' => 'Id Estado',
            'id_pais' => 'Id Pais',
            'id_sector' => 'Id Auto-numerico que indifica a que sector o zona geografica pertene el cliente, segun tabla respectiva',
            'id_tipo_doc_identificacion' => 'Id Tipo Doc Identificacion',
            'usa_credito' => 'Usa Credito',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_cliente',$this->id_cliente);
        $criteria->compare('id_tipo_cliente',$this->id_tipo_cliente);
        $criteria->compare('id_tipo_contribuyente',$this->id_tipo_contribuyente);
        $criteria->compare('id_tipo_precios',$this->id_tipo_precios);
        $criteria->compare('id_vendedor',$this->id_vendedor);
        $criteria->compare('id_tipo_documento',$this->id_tipo_documento);
        $criteria->compare('codigo',$this->codigo,true);
        $criteria->compare('nombre',$this->nombre,true);
        $criteria->compare('doc_identificacion',$this->doc_identificacion,true);
        $criteria->compare('fecha_inicio',$this->fecha_inicio,true);
        $criteria->compare('idfiscal1',$this->idfiscal1,true);
        $criteria->compare('idfiscal2',$this->idfiscal2,true);
        $criteria->compare('direccion',$this->direccion,true);
        $criteria->compare('historia',$this->historia,true);
        $criteria->compare('motivo_suspend',$this->motivo_suspend,true);
        $criteria->compare('telefonos',$this->telefonos,true);
        $criteria->compare('telefono_movil',$this->telefono_movil,true);
        $criteria->compare('limite_credito',$this->limite_credito);
        $criteria->compare('dias_credito',$this->dias_credito);
        $criteria->compare('tolerancia',$this->tolerancia);
        $criteria->compare('estatus',$this->estatus);
        $criteria->compare('nota',$this->nota,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('website',$this->website,true);
        $criteria->compare('descuento_prc',$this->descuento_prc);
        $criteria->compare('persona_contacto',$this->persona_contacto,true);
        $criteria->compare('id_ciudad',$this->id_ciudad);
        $criteria->compare('id_estado',$this->id_estado);
        $criteria->compare('id_pais',$this->id_pais);
        $criteria->compare('id_sector',$this->id_sector);
        $criteria->compare('id_tipo_doc_identificacion',$this->id_tipo_doc_identificacion);
        $criteria->compare('usa_credito',$this->usa_credito);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Clientes the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}