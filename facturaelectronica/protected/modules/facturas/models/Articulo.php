<?php

/**
 * This is the model class for table "articulo".
 *
 * The followings are the available columns in table 'articulo':
 * @property integer $id_articulo
 * @property integer $id_grupo
 * @property integer $id_sub_grupo
 * @property integer $id_unidad
 * @property integer $id_modelo
 * @property integer $id_marca
 * @property string $codigo
 * @property string $nombre
 * @property string $referencia
 * @property integer $usaexist
 * @property double $costo
 * @property double $costo_ant
 * @property double $costo_prom
 * @property double $minimo
 * @property double $maximo
 * @property string $rutafoto
 * @property string $detalles
 * @property integer $usointerno
 * @property string $fechacrea
 * @property integer $estatus
 * @property integer $fraccionable
 * @property integer $integrado
 * @property integer $agrupado
 *
 * The followings are the available model relations:
 * @property Agrupados[] $agrupadoses
 * @property TipoGrupos $idGrupo
 * @property TipoMarcas $idMarca
 * @property TipoModelos $idModelo
 * @property TipoSubGrupos $idSubGrupo
 * @property TipoUnidad $idUnidad
 * @property CargoDescargoMv[] $cargoDescargoMvs
 * @property Compramv[] $compramvs
 * @property Existencias[] $existenciases
 * @property Hashtag[] $hashtags
 * @property Integrados[] $integradoses
 * @property Integrados[] $integradoses1
 * @property Kardex[] $kardexes
 * @property Opermv[] $opermvs
 * @property Opermv[] $opermvs1
 * @property ProductosImpuestos[] $productosImpuestoses
 * @property ProductosPrecios[] $productosPrecioses
 * @property TrasladoInvMv[] $trasladoInvMvs
 */
class Articulo extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'articulo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_grupo, codigo, nombre, fechacrea, fraccionable', 'required'),
            array('id_grupo, id_sub_grupo, id_unidad, id_modelo, id_marca, usaexist, usointerno, estatus, fraccionable, integrado, agrupado', 'numerical', 'integerOnly'=>true),
            array('costo, costo_ant, costo_prom, minimo, maximo', 'numerical'),
            array('codigo', 'length', 'max'=>25),
            array('nombre', 'length', 'max'=>150),
            array('referencia', 'length', 'max'=>20),
            array('rutafoto', 'length', 'max'=>200),
            array('detalles', 'length', 'max'=>250),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_articulo, id_grupo, id_sub_grupo, id_unidad, id_modelo, id_marca, codigo, nombre, referencia, usaexist, costo, costo_ant, costo_prom, minimo, maximo, rutafoto, detalles, usointerno, fechacrea, estatus, fraccionable, integrado, agrupado', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'agrupadoses' => array(self::HAS_MANY, 'Agrupados', 'id_articulo'),
            'idGrupo' => array(self::BELONGS_TO, 'TipoGrupos', 'id_grupo'),
            'idMarca' => array(self::BELONGS_TO, 'TipoMarcas', 'id_marca'),
            'idModelo' => array(self::BELONGS_TO, 'TipoModelos', 'id_modelo'),
            'idSubGrupo' => array(self::BELONGS_TO, 'TipoSubGrupos', 'id_sub_grupo'),
            'idUnidad' => array(self::BELONGS_TO, 'TipoUnidad', 'id_unidad'),
            'cargoDescargoMvs' => array(self::HAS_MANY, 'CargoDescargoMv', 'id_articulo'),
            'compramvs' => array(self::HAS_MANY, 'Compramv', 'id_articulo'),
            'existenciases' => array(self::HAS_MANY, 'Existencias', 'id_articulo'),
            'hashtags' => array(self::HAS_MANY, 'Hashtag', 'id_articulo'),
            'integradoses' => array(self::HAS_MANY, 'Integrados', 'id_articulo_padre'),
            'integradoses1' => array(self::HAS_MANY, 'Integrados', 'id_articulo_hijo'),
            'kardexes' => array(self::HAS_MANY, 'Kardex', 'id_articulo'),
            'opermvs' => array(self::HAS_MANY, 'Opermv', 'id_articulo'),
            'opermvs1' => array(self::HAS_MANY, 'Opermv', 'id_articulo_padre'),
            'productosImpuestoses' => array(self::HAS_MANY, 'ProductosImpuestos', 'id_articulo'),
            'productosPrecioses' => array(self::HAS_MANY, 'ProductosPrecios', 'id_articulo'),
            'trasladoInvMvs' => array(self::HAS_MANY, 'TrasladoInvMv', 'id_articulo'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_articulo' => 'Es el código unico auto-numérico que identifica al artículo en la base de datos',
            'id_grupo' => 'Identificador unico del grupo de inventario ',
            'id_sub_grupo' => 'Id Sub Grupo',
            'id_unidad' => 'Id Unidad',
            'id_modelo' => 'Id Modelo',
            'id_marca' => 'Id Marca',
            'codigo' => 'Codigo del artículo',
            'nombre' => 'Nombre del artículo',
            'referencia' => 'Número de referencia del artículo, se usa por ejemplo para codigo del fabricante',
            'usaexist' => 'indica 0=no usa existencia (servicio) 1=usa existencia (producto)',
            'costo' => 'Costo inicial del articulo',
            'costo_ant' => 'Costo anterior a la ultima compra',
            'costo_prom' => 'Costo promedio del articulo',
            'minimo' => 'Se trata del stock minimo que debe haber del articulo 
',
            'maximo' => 'Se trata del stock maximo que puede haber del articulo',
            'rutafoto' => 'aca se almacena la ruta de la foto del articulo, la idea es crear una tabla de fotos (a futuro)',
            'detalles' => 'Almacena notas generales descriptivas del articulo',
            'usointerno' => 'Identifica si el artículo es 0=para la venta / 1=uso interno (consumo propio)',
            'fechacrea' => 'Fecha en la cual se registro este articulo por primera vez, no se actualiza jamas',
            'estatus' => '0=activo / 1=inactivo',
            'fraccionable' => 'Indica 0=Solo enteros / 1=si permite decimales',
            'integrado' => 'Integrado',
            'agrupado' => 'Agrupado',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_articulo',$this->id_articulo);
        $criteria->compare('id_grupo',$this->id_grupo);
        $criteria->compare('id_sub_grupo',$this->id_sub_grupo);
        $criteria->compare('id_unidad',$this->id_unidad);
        $criteria->compare('id_modelo',$this->id_modelo);
        $criteria->compare('id_marca',$this->id_marca);
        $criteria->compare('codigo',$this->codigo,true);
        $criteria->compare('nombre',$this->nombre,true);
        $criteria->compare('referencia',$this->referencia,true);
        $criteria->compare('usaexist',$this->usaexist);
        $criteria->compare('costo',$this->costo);
        $criteria->compare('costo_ant',$this->costo_ant);
        $criteria->compare('costo_prom',$this->costo_prom);
        $criteria->compare('minimo',$this->minimo);
        $criteria->compare('maximo',$this->maximo);
        $criteria->compare('rutafoto',$this->rutafoto,true);
        $criteria->compare('detalles',$this->detalles,true);
        $criteria->compare('usointerno',$this->usointerno);
        $criteria->compare('fechacrea',$this->fechacrea,true);
        $criteria->compare('estatus',$this->estatus);
        $criteria->compare('fraccionable',$this->fraccionable);
        $criteria->compare('integrado',$this->integrado);
        $criteria->compare('agrupado',$this->agrupado);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Articulo the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}