
<?php

/**
 * This is the model class for table "tipo_forma_de_pago".
 *
 * The followings are the available columns in table 'tipo_forma_de_pago':
 * @property integer $id_tipo_forma_de_pago
 * @property string $codigo
 * @property string $descripcion
 * @property integer $clasificacion
 * @property integer $es_efectivo
 * @property string $rutafoto
 * @property string $dato_adicional1
 * @property string $dato_adicional2
 * @property string $dato_adicional3
 * @property string $dato_adicional4
 *
 * The followings are the available model relations:
 * @property Cobros[] $cobroses
 */
class TipoFormaDePago extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tipo_forma_de_pago';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('codigo, descripcion', 'required'),
            array('clasificacion, es_efectivo', 'numerical', 'integerOnly'=>true),
            array('codigo', 'length', 'max'=>6),
            array('descripcion', 'length', 'max'=>65),
            array('rutafoto', 'length', 'max'=>150),
            array('dato_adicional1, dato_adicional2, dato_adicional3, dato_adicional4', 'length', 'max'=>45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_tipo_forma_de_pago, codigo, descripcion, clasificacion, es_efectivo, rutafoto, dato_adicional1, dato_adicional2, dato_adicional3, dato_adicional4', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cobroses' => array(self::HAS_MANY, 'Cobros', 'id_tipo_forma_de_pago'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_tipo_forma_de_pago' => 'ID Auto-numérico que representa la formas de pago',
            'codigo' => 'Codigo de la forma de pago',
            'descripcion' => 'Nombre de la forma de pago',
            'clasificacion' => 'Clasificación de la forma de pago 1=Efectivo,2=cheque,3=tar.cred,4=tar.deb,5=transferencia,6=nota de credito,0=otros',
            'es_efectivo' => 'Es Efectivo',
            'rutafoto' => 'Rutafoto',
            'dato_adicional1' => 'Dato Adicional1',
            'dato_adicional2' => 'Dato Adicional2',
            'dato_adicional3' => 'Dato Adicional3',
            'dato_adicional4' => 'Dato Adicional4',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_tipo_forma_de_pago',$this->id_tipo_forma_de_pago);
        $criteria->compare('codigo',$this->codigo,true);
        $criteria->compare('descripcion',$this->descripcion,true);
        $criteria->compare('clasificacion',$this->clasificacion);
        $criteria->compare('es_efectivo',$this->es_efectivo);
        $criteria->compare('rutafoto',$this->rutafoto,true);
        $criteria->compare('dato_adicional1',$this->dato_adicional1,true);
        $criteria->compare('dato_adicional2',$this->dato_adicional2,true);
        $criteria->compare('dato_adicional3',$this->dato_adicional3,true);
        $criteria->compare('dato_adicional4',$this->dato_adicional4,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TipoFormaDePago the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}