<?php

/**
 * This is the model class for table "Contribuyente".
 *
 * The followings are the available columns in table 'Contribuyente':
 * @property integer $id_contribuyente
 * @property string $nombre
 * @property string $nombre_abre
 * @property string $tipo_identificacion
 * @property integer $identificacion
 * @property string $contacto
 * @property string $moneda
 * @property string $correo
 * @property string $pagina
 * @property integer $pais
 * @property integer $provincia
 * @property integer $canton
 * @property integer $distrito
 * @property string $telefono
 * @property integer $fax
 * @property string $fecha_ins
 * @property string $direccion
 * @property integer $cuenta_banc
 * @property string $foto
 */
class Contribuyente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Contribuyente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, nombre_abre, tipo_identificacion, identificacion, contacto, moneda, correo, pagina, pais, provincia, canton, distrito, telefono, fax, fecha_ins, direccion, cuenta_banc, foto', 'required'),
			array('identificacion, pais, provincia, canton, distrito, fax, cuenta_banc', 'numerical', 'integerOnly'=>true),
			array('nombre, tipo_identificacion, contacto, correo, pagina, direccion, foto', 'length', 'max'=>100),
			array('nombre_abre, moneda', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_contribuyente, nombre, nombre_abre, tipo_identificacion, identificacion, contacto, moneda, correo, pagina, pais, provincia, canton, distrito, telefono, fax, fecha_ins, direccion, cuenta_banc, foto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_contribuyente' => 'Id Contribuyente',
			'nombre' => 'Nombre',
			'nombre_abre' => 'Nombre Abre',
			'tipo_identificacion' => 'Tipo Identificacion',
			'identificacion' => 'Identificacion',
			'contacto' => 'Contacto',
			'moneda' => 'Moneda',
			'correo' => 'Correo',
			'pagina' => 'Pagina',
			'pais' => 'Pais',
			'provincia' => 'Provincia',
			'canton' => 'Canton',
			'distrito' => 'Distrito',
			'telefono' => 'Telefono',
			'fax' => 'Fax',
			'fecha_ins' => 'Fecha Ins',
			'direccion' => 'Direccion',
			'cuenta_banc' => 'Cuenta Banc',
			'foto' => 'Foto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_contribuyente',$this->id_contribuyente);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('nombre_abre',$this->nombre_abre,true);
		$criteria->compare('tipo_identificacion',$this->tipo_identificacion,true);
		$criteria->compare('identificacion',$this->identificacion);
		$criteria->compare('contacto',$this->contacto,true);
		$criteria->compare('moneda',$this->moneda,true);
		$criteria->compare('correo',$this->correo,true);
		$criteria->compare('pagina',$this->pagina,true);
		$criteria->compare('pais',$this->pais);
		$criteria->compare('provincia',$this->provincia);
		$criteria->compare('canton',$this->canton);
		$criteria->compare('distrito',$this->distrito);
		$criteria->compare('telefono',$this->telefono,true);
		$criteria->compare('fax',$this->fax);
		$criteria->compare('fecha_ins',$this->fecha_ins,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('cuenta_banc',$this->cuenta_banc);
		$criteria->compare('foto',$this->foto,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contribuyente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
