<?php

/**
 * This is the model class for table "operti".
 *
 * The followings are the available columns in table 'operti':
 * @property integer $id_operti
 * @property integer $id_tipo_documento
 * @property integer $id_cliente
 * @property integer $id_almacen
 * @property integer $id_vendedor
 * @property string $serie
 * @property string $documento
 * @property string $orden_compra
 * @property string $emision
 * @property string $recepcion
 * @property string $vence
 * @property double $totcosto
 * @property double $totbruto
 * @property double $totneto
 * @property double $totpagos
 * @property double $totalfinal
 * @property double $totimpuest
 * @property double $totexento
 * @property double $totdescuen
 * @property double $recargos
 * @property string $notas
 * @property integer $estatus
 * @property double $vuelto
 * @property string $fechayhora
 * @property string $serialprintf
 * @property string $documentofiscal
 * @property integer $id_operti_devol
 * @property integer $es_devolucion
 * @property integer $es_temporal
 * @property integer $id_usuario
 * @property integer $id_agencia
 * @property integer $id_operti_inicial
 * @property integer $estatus_fact_elect
 * @property string $fact_elect_response
 *
 * The followings are the available model relations:
 * @property Cobros[] $cobroses
 * @property Opermv[] $opermvs
 * @property Agencias $idAgencia
 * @property Almacenes $idAlmacen
 * @property Clientes $idCliente
 * @property Vendedores $idVendedor
 * @property Operti $idOpertiDevol
 * @property Operti[] $opertis
 * @property Operti $idOpertiInicial
 * @property Operti[] $opertis1
 * @property TipoDocumentos $idTipoDocumento
 * @property Usuarios $idUsuario
 */
class Operti extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'operti';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_tipo_documento, serie, emision, recepcion, vence, id_usuario, id_agencia', 'required'),
            array('id_tipo_documento, id_cliente, id_almacen, id_vendedor, estatus, id_operti_devol, es_devolucion, es_temporal, id_usuario, id_agencia, id_operti_inicial, estatus_fact_elect', 'numerical', 'integerOnly'=>true),
            array('totcosto, totbruto, totneto, totpagos, totalfinal, totimpuest, totexento, totdescuen, recargos, vuelto', 'numerical'),
            array('serie', 'length', 'max'=>6),
            array('documento', 'length', 'max'=>10),
            array('orden_compra', 'length', 'max'=>15),
            array('notas', 'length', 'max'=>250),
            array('serialprintf', 'length', 'max'=>25),
            array('documentofiscal', 'length', 'max'=>12),
            array('fechayhora, fact_elect_response', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_operti, id_tipo_documento, id_cliente, id_almacen, id_vendedor, serie, documento, orden_compra, emision, recepcion, vence, totcosto, totbruto, totneto, totpagos, totalfinal, totimpuest, totexento, totdescuen, recargos, notas, estatus, vuelto, fechayhora, serialprintf, documentofiscal, id_operti_devol, es_devolucion, es_temporal, id_usuario, id_agencia, id_operti_inicial, estatus_fact_elect, fact_elect_response', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cobroses' => array(self::HAS_MANY, 'Cobros', 'id_operti'),
            'opermvs' => array(self::HAS_MANY, 'Opermv', 'id_operti'),
            'idAgencia' => array(self::BELONGS_TO, 'Agencias', 'id_agencia'),
            'idAlmacen' => array(self::BELONGS_TO, 'Almacenes', 'id_almacen'),
            'idCliente' => array(self::BELONGS_TO, 'Clientes', 'id_cliente'),
            'idVendedor' => array(self::BELONGS_TO, 'Vendedores', 'id_vendedor'),
            'idOpertiDevol' => array(self::BELONGS_TO, 'Operti', 'id_operti_devol'),
            'opertis' => array(self::HAS_MANY, 'Operti', 'id_operti_devol'),
            'idOpertiInicial' => array(self::BELONGS_TO, 'Operti', 'id_operti_inicial'),
            'opertis1' => array(self::HAS_MANY, 'Operti', 'id_operti_inicial'),
            'idTipoDocumento' => array(self::BELONGS_TO, 'TipoDocumentos', 'id_tipo_documento'),
            'idUsuario' => array(self::BELONGS_TO, 'Usuarios', 'id_usuario'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_operti' => 'ID Auto-numérico',
            'id_tipo_documento' => 'ID tipo de documento 
',
            'id_cliente' => 'Id Cliente',
            'id_almacen' => 'ID Auto-numérico que representa el código del almacen o deposito principal (cabecera)',
            'id_vendedor' => 'Id Vendedor',
            'serie' => 'Número de serie del documento',
            'documento' => 'Número de documento',
            'orden_compra' => 'Número de orden de compra',
            'emision' => 'Fecha de emisión del documento',
            'recepcion' => 'Fecha de recepción del documento',
            'vence' => 'Fecha de vencimiento del documento',
            'totcosto' => 'Total costo involucrado en el documento',
            'totbruto' => 'Total bruto (suma de precios multiplicado por cantidad sin descuento) 
',
            'totneto' => 'Total neto (precios con descuento multiplicados por cantidad)',
            'totpagos' => 'Total pagado por el cliente',
            'totalfinal' => 'Total final, suma de neto más impuesto + recargo',
            'totimpuest' => 'Total impuestos involucrados en el documento',
            'totexento' => 'Total excento (monto exonerado, sin impuesto) ',
            'totdescuen' => 'Total descuento otorgado',
            'recargos' => 'Total recargos del documento',
            'notas' => 'Notas libres sobre el documento',
            'estatus' => '0=pendiente por cobro / 1=posee abonos parciales / 2=totalmente cobrado / 3=anulado o inactivo',
            'vuelto' => 'Monto por vuelto o cambio',
            'fechayhora' => 'Fecha y hora del documento',
            'serialprintf' => 'Serial del impresor fiscal',
            'documentofiscal' => 'Documento fiscal (viene del impresor fiscal)',
            'id_operti_devol' => 'ID documento a devolver',
            'es_devolucion' => 'Se utiliza para saber si una nota de credito fue generada por una devolucion',
            'es_temporal' => 'Es Temporal',
            'id_usuario' => 'Id Usuario',
            'id_agencia' => 'Id Agencia',
            'id_operti_inicial' => 'Este id sirve para enlazar las notas de credito y debito que se generaron debido a una factura, y asi poder hacer la conversion de moneda afectando todos sus documentos derivados.',
            'estatus_fact_elect' => 'Estatus Fact Elect',
            'fact_elect_response' => 'Fact Elect Response',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_operti',$this->id_operti);
        $criteria->compare('id_tipo_documento',$this->id_tipo_documento);
        $criteria->compare('id_cliente',$this->id_cliente);
        $criteria->compare('id_almacen',$this->id_almacen);
        $criteria->compare('id_vendedor',$this->id_vendedor);
        $criteria->compare('serie',$this->serie,true);
        $criteria->compare('documento',$this->documento,true);
        $criteria->compare('orden_compra',$this->orden_compra,true);
        $criteria->compare('emision',$this->emision,true);
        $criteria->compare('recepcion',$this->recepcion,true);
        $criteria->compare('vence',$this->vence,true);
        $criteria->compare('totcosto',$this->totcosto);
        $criteria->compare('totbruto',$this->totbruto);
        $criteria->compare('totneto',$this->totneto);
        $criteria->compare('totpagos',$this->totpagos);
        $criteria->compare('totalfinal',$this->totalfinal);
        $criteria->compare('totimpuest',$this->totimpuest);
        $criteria->compare('totexento',$this->totexento);
        $criteria->compare('totdescuen',$this->totdescuen);
        $criteria->compare('recargos',$this->recargos);
        $criteria->compare('notas',$this->notas,true);
        $criteria->compare('estatus',$this->estatus);
        $criteria->compare('vuelto',$this->vuelto);
        $criteria->compare('fechayhora',$this->fechayhora,true);
        $criteria->compare('serialprintf',$this->serialprintf,true);
        $criteria->compare('documentofiscal',$this->documentofiscal,true);
        $criteria->compare('id_operti_devol',$this->id_operti_devol);
        $criteria->compare('es_devolucion',$this->es_devolucion);
        $criteria->compare('es_temporal',$this->es_temporal);
        $criteria->compare('id_usuario',$this->id_usuario);
        $criteria->compare('id_agencia',$this->id_agencia);
        $criteria->compare('id_operti_inicial',$this->id_operti_inicial);
        $criteria->compare('estatus_fact_elect',$this->estatus_fact_elect);
        $criteria->compare('fact_elect_response',$this->fact_elect_response,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Operti the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}