<?php
Yii::app()->clientScript->registerScript('searchxx', "

$('.search2-form form').submit(function(){
    $('#usuario2-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>         
                            </br>
                                
     <div class="search2-form">
        <?php $this->renderPartial('application.modules.registro.views.productos._search2',array(
    'model'=>$model,
)); ?>

</div>
                  <div class="table table-striped table-bordered table-condensed table-responsive" cellspacing="0" cellpadding="3" rules="all" style="background-color:White;border-color:#CCCCCC;border-width:1px;border-style:None;border-collapse:collapse;">


                <?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'usuario2-grid',
                        'dataProvider'=>$model->search(),
                       
                        'itemsCssClass'=>'table table-striped table-bordered text-center',
                        'pagerCssClass'=>'pagination pull-right',
                        'afterAjaxUpdate'=>'function(){$.getScript("themes/classic/plugins/archivos_js/campo-numero.js");$.getScript("themes/classic/plugins/archivos_js/campo-letra.js");
                                $.getScript("themes/classic/plugins/archivos_js/script_select2.js");}',
                        'pager'=>array(
                            'htmlOptions'=>array('class'=>'pagination pagination-sm no-margin'),
                            'cssFile'=>'',
                            'hiddenPageCssClass'=>'disabled',
                            'selectedPageCssClass'=>'active',
                            'header'=>''),
                            'ajaxUrl'=>Yii::app()->createUrl("/registro/Productos/admin"),
                                    'columns'=>array(
                                           
                                             
                                         array(
                                                    'header'=>"",
                                                    'htmlOptions'=>array('class'=>'col-md-1 col-lg-1 text-center'),
                                                    'value'=>'"<a data-toggle=\"tooltip\" onclick=\"selectedAlumno3(".$data->id_productos.")\" title=\"Añadir Producto\" href=\"#\"><img src=\"/facturaelectronica/images/agregar.png\" alt=\"Añadir Cliente\"></p>"',
                                                    'type'=>'raw',
                                                ),       




                                            array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Código</p>",
                                                    'name'=>'codigo',
                                                    'htmlOptions'=>array('class'=>'col-md-1 text-center'),
                                                    'value'=>'$data->codigo',
                                                    'filter'=>CHtml::activeTextField($model,'codigo',array('class'=>'form-control campo-letra text-center','placeholder'=>'CÓDIGO','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),

                                            
                                            

                                                 array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Tipo</p>",
                                                    'name'=>'tipo_producto',
                                                    'htmlOptions'=>array('class'=>'col-md-1 text-center'),
                                                
                                                    'value'=>'($data->tipo_producto==1)?"SERVICIO":"PRODUCTO"',


                                                    'filter'=>CHtml::activeTextField($model,'tipo_producto',array('class'=>'form-control campo-letra text-center','placeholder'=>'TIPO','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),

                                                      array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Desc.Corta</p>",
                                                    'name'=>'descripcionc',
                                                    'htmlOptions'=>array('class'=>'col-md-2 text-center'),
                                                    'value'=>'$data->descripcionc',
                                                    'filter'=>CHtml::activeTextField($model,'descripcionc',array('class'=>'form-control campo-letra text-center','placeholder'=>'DESRIPCIÓN CORTA','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),

                                                 
                                                   array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Precio ₡</p>",
                                                    'name'=>'precioc_id',
                                                    'htmlOptions'=>array('class'=>'col-md-1 text-center'),
                                                    'value'=>'$data->precioc_id',
                                                    'filter'=>CHtml::activeTextField($model,'precioc_id',array('class'=>'form-control campo-numero text-center','placeholder'=>'PRECIO ₡','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),

                                                          array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Precio $</p>",
                                                    'name'=>'precios_id',
                                                    'htmlOptions'=>array('class'=>'col-md-1 text-center'),
                                                    'value'=>'$data->precios_id',
                                                    'filter'=>CHtml::activeTextField($model,'precios_id',array('class'=>'form-control campo-numero text-center','placeholder'=>'PRECIO $','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),

                                                           


                                         
                                           
                                              
                                                   
                                           ),)); ?>

                  </div><!-- /.box-body -->
            </div><!-- /.box -->
