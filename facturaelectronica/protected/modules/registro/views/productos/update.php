<?php
/* @var $this ProductosController */
/* @var $model Productos */

$this->breadcrumbs=array(
	'Productoses'=>array('index'),
	$model->id_productos=>array('view','id'=>$model->id_productos),
	'Update',
);

$this->menu=array(
	//array('label'=>'List Productos', 'url'=>array('index')),
	//array('label'=>'Create Productos', 'url'=>array('create')),
	//array('label'=>'View Productos', 'url'=>array('view', 'id'=>$model->id_productos)),
	//array('label'=>'Manage Productos', 'url'=>array('admin')),
);
?>


<?php $this->renderPartial('_form', array('model'=>$model,'clientes'=>$clientes)); ?>