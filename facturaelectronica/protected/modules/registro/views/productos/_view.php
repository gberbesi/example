<?php
/* @var $this ProductosController */
/* @var $data Productos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_productos')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_productos), array('view', 'id'=>$data->id_productos)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo')); ?>:</b>
	<?php echo CHtml::encode($data->codigo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_producto')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_producto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precioc_id')); ?>:</b>
	<?php echo CHtml::encode($data->precioc_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precios_id')); ?>:</b>
	<?php echo CHtml::encode($data->precios_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('unidad_medida_id')); ?>:</b>
	<?php echo CHtml::encode($data->unidad_medida_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo_codigo_id')); ?>:</b>
	<?php echo CHtml::encode($data->tipo_codigo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precio_finalc_id')); ?>:</b>
	<?php echo CHtml::encode($data->precio_finalc_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precio_finals_id')); ?>:</b>
	<?php echo CHtml::encode($data->precio_finals_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('otro_impuesto')); ?>:</b>
	<?php echo CHtml::encode($data->otro_impuesto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lista_impuesto_id')); ?>:</b>
	<?php echo CHtml::encode($data->lista_impuesto_id); ?>
	<br />

	*/ ?>

</div>