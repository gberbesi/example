<?php
/* @var $this ListaImpuestoController */
/* @var $model ListaImpuesto */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_impuesto'); ?>
		<?php echo $form->textField($model,'id_impuesto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'impuesto_nombre'); ?>
		<?php echo $form->textField($model,'impuesto_nombre',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'monto'); ?>
		<?php echo $form->textField($model,'monto'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->