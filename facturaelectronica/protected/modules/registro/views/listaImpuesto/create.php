<?php
/* @var $this ListaImpuestoController */
/* @var $model ListaImpuesto */

$this->breadcrumbs=array(
	'Lista Impuestos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ListaImpuesto', 'url'=>array('index')),
	array('label'=>'Manage ListaImpuesto', 'url'=>array('admin')),
);
?>

<h1>Create ListaImpuesto</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>