<?php
/* @var $this ListaImpuestoController */
/* @var $data ListaImpuesto */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_impuesto')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_impuesto), array('view', 'id'=>$data->id_impuesto)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('impuesto_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->impuesto_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('monto')); ?>:</b>
	<?php echo CHtml::encode($data->monto); ?>
	<br />


</div>