<?php
/* @var $this ListaImpuestoController */
/* @var $model ListaImpuesto */

$this->breadcrumbs=array(
	'Lista Impuestos'=>array('index'),
	$model->id_impuesto=>array('view','id'=>$model->id_impuesto),
	'Update',
);

$this->menu=array(
	array('label'=>'List ListaImpuesto', 'url'=>array('index')),
	array('label'=>'Create ListaImpuesto', 'url'=>array('create')),
	array('label'=>'View ListaImpuesto', 'url'=>array('view', 'id'=>$model->id_impuesto)),
	array('label'=>'Manage ListaImpuesto', 'url'=>array('admin')),
);
?>

<h1>Update ListaImpuesto <?php echo $model->id_impuesto; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>