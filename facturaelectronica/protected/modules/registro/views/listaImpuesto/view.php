<?php
/* @var $this ListaImpuestoController */
/* @var $model ListaImpuesto */

$this->breadcrumbs=array(
	'Lista Impuestos'=>array('index'),
	$model->id_impuesto,
);

$this->menu=array(
	array('label'=>'List ListaImpuesto', 'url'=>array('index')),
	array('label'=>'Create ListaImpuesto', 'url'=>array('create')),
	array('label'=>'Update ListaImpuesto', 'url'=>array('update', 'id'=>$model->id_impuesto)),
	array('label'=>'Delete ListaImpuesto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_impuesto),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ListaImpuesto', 'url'=>array('admin')),
);
?>

<h1>View ListaImpuesto #<?php echo $model->id_impuesto; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_impuesto',
		'impuesto_nombre',
		'porcentaje',
	),
)); ?>
