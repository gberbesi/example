<?php
/* @var $this ListaImpuestoController */
/* @var $model ListaImpuesto */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'lista-impuesto-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_impuesto'); ?>
		<?php echo $form->textField($model,'id_impuesto'); ?>
		<?php echo $form->error($model,'id_impuesto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'impuesto_nombre'); ?>
		<?php echo $form->textField($model,'impuesto_nombre',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'impuesto_nombre'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'porcentaje'); ?>
		<?php echo $form->textField($model,'porcentaje'); ?>
		<?php echo $form->error($model,'porcentaje'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->