<?php
/* @var $this ListaImpuestoController */
/* @var $model ListaImpuesto */

$this->breadcrumbs=array(
	'Lista Impuestos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ListaImpuesto', 'url'=>array('index')),
	array('label'=>'Create ListaImpuesto', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#lista-impuesto-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'lista-impuesto-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_impuesto',
		'impuesto_nombre',
		'monto',
		array(
                                                    'header'=>"Acción",
                                                    'htmlOptions'=>array('class'=>'col-md-2 text-center'),
                                                    'class'=>'CButtonColumn',
                                                    
                                                    'template' => '{update}{delete}{myButton}',
                                                    'buttons'=>array(
                                                              
                                                                    
           
                                                                    'update' => array(
                                                                                    'label'=>'Agregar impuesto',
                                                                                    'options'=>array('data-toggle'=>"tooltip"),
                                                                                    ),

                                                                   
                                                                    'delete' => array(
                                                                                    'options'=>array('data-toggle'=>"tooltip"),
                                                                                   
                                                                                    'label'=>'Eliminar',		),
		'myButton'=>array(
			'label'=>'Agregar Impuesto',
			'imageUrl'=>'images/aceptar.png',
			

			//'url'=>'Yii::app()->createUrl("controller/action", array("id"=>$data->id))',
			'click'=>'',
			'options'=>array('data-toggle'=>"tooltip"),

			),
		),)	,
	),
));



 ?>
 <div id="dynamicDiv">
        
        </div>

        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    
    
  
  
    <div class="container">
      <a class="btn btn-primary" href="javascript:void(0)" id="addInput">
        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        Adicionar Campo
      </a>
      <br/>
     
 

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

    
    </div>


 <script>
      $(function () {
          var scntDiv = $('#dynamicDiv');
          $(document).on('click', '#addInput', function () {
              $('<p>'+
                'hola '+
                '<a class="btn btn-danger" href="javascript:void(0)" id="remInput">'+
              '<span class="glyphicon glyphicon-minus" aria-hidden="true"></span> '+
              'Remover'+
                '</a>'+
          '</p>').appendTo(scntDiv);
              return false;
          });
          $(document).on('click', '#remInput', function () {
                $(this).parents('p').remove();
              return false;
          });
      });
      </script>