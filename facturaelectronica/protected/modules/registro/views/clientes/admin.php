<?php



Yii::app()->clientScript->registerScript('search2', "
$('.searchman-form form').submit(function(){console.log('hola');
    $('#usuario-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?> <?php 
   $tipo=$_GET['d'];
   if($tipo==1){
    $msg="Eliminados";
   }else if(!$tipo){
    $msg=" ";
   }
   ?>
            <div >
                <div class="box-header with-border">
                 <h3 class="box-title"><h1 style="color:#2a3f54;"> &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-user"></span><span style="color:black">  Clientes <?php echo $msg ;?> </span></h1></h3>
                <nav class="breadcrumb" style="background-color:white; padding-left: 10px;margin-left: 30px; font-size: 14px;">
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('usuario/usuario/index');?>" style="color:black"><span class="glyphicon glyphicon-home"></span><span style="color:#22C7FC">&nbsp;&nbsp;Inicio</span> </a><a>/</a>
  <a class="breadcrumb-item active" style="color:black"><span class="glyphicon glyphicon-user"></span><span style="color:#22C7FC">&nbsp;&nbsp;Clientes</span> </a>
   
</nav>                  
                </div>                 
                 <div class="searchman-form">
                <?php $this->renderPartial('_search',array(
                    'model'=>$model,
                )); ?>
</div>
                  <div class="table table-striped  table-condensed table-responsive" cellspacing="0" cellpadding="3" rules="all" style="background-color:transparent;border-color:#CCCCCC;border-width:1px;border-style:none;border-collapse:collapse;  font-size: 14px;">
                <?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'usuario-grid',
                      'dataProvider'=>$model->search(),
                        //'filter'=>$model,
                        'itemsCssClass'=>'table table-striped  text-center',
                        'pagerCssClass'=>'pagination pull-right',
                        'afterAjaxUpdate'=>'function(){$.getScript("themes/classic/plugins/archivos_js/campo-numero.js");$.getScript("themes/classic/plugins/archivos_js/campo-letra.js");
                                $.getScript("themes/classic/plugins/archivos_js/script_select2.js");}',
                        'pager'=>array(
                            'htmlOptions'=>array('class'=>'pagination pagination-sm no-margin'),
                            'cssFile'=>'',
                            'hiddenPageCssClass'=>'disabled',
                            'selectedPageCssClass'=>'active',
                            'header'=>''),
                            'ajaxUrl'=>Yii::app()->createUrl("/registro/clientes/admin"),
                                    'columns'=>array(
                                           array(
                                                    'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>ID</p>",
                                                    'name'=>'id_cliente',
                                                    'htmlOptions'=>array('class'=>'col-md-0 '),
                                                    'value'=>'$data->id_cliente',
                                                    //'filter'=>CHtml::activeTextField($model,'id_productos',array('class'=>'form-control campo-letra ','placeholder'=>'ID','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),
                                            
                                            array(
                                                'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Identificación</p>",
                                                'name'=>'identificacion',
                                                'htmlOptions'=>array('class'=>'col-md-0 text-center'),
                                                'value'=>'$data->identificacion',
                                                'filter'=>CHtml::activeTextField($model,'identificacion',array('class'=>'form-control campo-numero text-center','placeholder'=>'IDENTIFICACIÓN')),
                                                ),

                                                      array(
                                                'header'=>"<p class='text-center'  style='color:#22C7FC;font-weight:bold'>Nombre</p>",
                                                'name'=>'clientes',
                                                'htmlOptions'=>array('class'=>'col-md-9 text-center'),
                                                'value'=>'$data->clientes',
                                                'filter'=>CHtml::activeTextField($model,'clientes',array('class'=>'form-control campo-numero text-center','placeholder'=>'CLIENTES')),
                                                ),
                                       array(
                                          'header'=>"",
                                          'visible'=>''.$tipo.'',
                                                    'htmlOptions'=>array('class'=>'col-md-1 col-lg-1 text-center','style'=>'color:red;'),
                                         'value'=>'"<a data-toggle=\"tooltip\" onclick=\"selectedAlumno3(".$data->id_cliente.")\" title=\"Restaurar\" href=\"#\"><i class=\" glyphicon glyphicon-ok\" style=\"color:#99cc33;font-size:21px; \"></i></a>"',
                                                    'type'=>'raw',
                                         ),
                                         
                                         
                                           
                                              
                                                   
                                            array(
                                                    'header'=>"",
                                                    'htmlOptions'=>array('class'=>'col-md-2 text-center'),
                                                    'class'=>'CButtonColumn',
                                                    'template' => '{update}{borrar}',
                                                    'buttons'=>array(
                                                              
                                                                    

                'update' => array(
                                'label'=>'',
                                
                                'visible'=>''.!$tipo.'',
                                'options'=>array('class'=>'fa fa-pencil','data-toggle'=>"tooltip",'title'=>'Editar','style'=>'color:black;font-size:20px;'),
                                'imageUrl'=>false,
                                ),

               
                'borrar' => array(
                    'label'=>'',
                                
                               'visible'=>''.!$tipo.'',
                              'options'=>array('class'=>'fa fa-times borrado','style'=>'margin-left:10px;color:red;font-size:20px;', 'data-toggle'=>"tooltip",'title'=>'Eliminar',),
                                'imageUrl'=>false,
                                'url'=>'Yii::app()->createUrl("registro/clientes/delete", array("id" => $data->id_cliente))',
                               // 'click'=>'function(){return confirm("¿Desea Eliminar Este Tipo de Producto?");}',
                                ),
)),),)); ?>

</div>
</div>

         <div class="modal fade" id="impFac" name='<?php echo $ruta;?>' role="dialog">
    <div class="modal-dialog" id="forma" name='<?php echo $vista;?>'>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Restaurar Cliente</h4>
        </div>
        <div class="modal-body" id="listo">
         
            
            </div><br><br><br></p>

             <div class="modal-footer">
          <button type="button" class="btn btn-dark" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
       
      </div>
      
    </div>
  </div>

<script src="js/sweetalert.min.js"></script>
<script>

/*<![CDATA[*/
jQuery(function($) {

jQuery(document).on('click','.borrado',function(event) {
    var th = this;
event.preventDefault(); 
    swal({
      title: "¿Eliminar?",
      text: "¿Está seguro que desea borrar este elemento?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {

        jQuery('#usuario-grid').yiiGridView('update', {
            type: 'POST',
            url: jQuery(th).attr('href'),
            success: function(data) {
                if(data!='error') {
                   jQuery('#usuario-grid').yiiGridView('update');

                    swal("Borrado exitoso", {
                      icon: "success",
                    }); 
                } else {
                    swal("Este usuario posee facturas activas", {
                      icon: "error",
                    }); 
                }
                
            },
            error: function(XHR) {

            }
        });


        
      } else {
        //swal("Your imaginary file is safe!");
      }
    });


    
    
});
});
function selectedAlumno3($id){
 $('#impFac').modal(); 
  var html='<a href="index.php?r=registro/clientes/Restaurar&id='+$id+' " target="_blank"><button type="button" class="btn  col-md-12 col-xs-12  " style="background-color:#99cc33;color:white;font-weight:bold;font-size:17px;" onclick="cerrar();"  > Restaurar Cliente Eliminado</button></a>';
 $('#listo').html(html); 


} 
function cerrar(){
  window.top.close();
}
/*]]>*/
</script>
