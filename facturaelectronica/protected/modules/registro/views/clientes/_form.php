 <script src="js/sweetalert.min.js"></script>
 <br>
  
 <h1 style="color:#2a3f54;"> &nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon glyphicon-user"></span><span style="color:black">  Clientes </span></h1>
<nav class="breadcrumb" style="background-color:white; padding-left: 10px;margin-left: 30px; font-size: 14px;">
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('usuario/usuario/index');?>" style="color:black"><span class="glyphicon glyphicon-home"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Inicio </a><a>/</a>
  <a class="breadcrumb-item" href="<?php echo Yii::app()->createUrl('registro/clientes/admin');?>" style="color:black"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Clientes </a><a>/</a>
  <span class="breadcrumb-item active" style="color:black"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<span style="color:#22C7FC">Registro  </span>
</nav>    
 




                <?php $form=$this->beginWidget('CActiveForm', array('id'=>'productos-form',)); ?>
                      <div class="col-md-4 col-sm-6 col-xs-12 form-group ">
                       
                            <label for="fullname">Tipo de Identificación </label>
                       <?php echo $form->dropDownList($model,'id_tipo_identificacioj',CHtml::listData(Identificacion::model()->findAll(),'id_identificacion', 'identificacion'),array('class'=>'form-control select2 ','style'=>'width:100%')) ?>
                        <?php echo $form->error($model,'id_tipo_identificacioj',array('class'=>'btn-xs alert-danger text-center')); ?> 
                      </div>

                      <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
                         <label for="fullname">*Identificación </label>
                            <?php echo $form->textField($model,'identificacion',array('type'=>'text','id'=>'identificacion','class'=>'form-control','placeholder'=>'Identificación','required'=>true,'onblur'=>'alumno()')); ?>
                            <?php echo $form->error($model,'identificacion',array('class'=>'btn-xs alert-danger text-center')); ?>
                        
                      </div>

                  


                        <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
                         <label for="fullname">*Nombre  </label>
                            <?php echo $form->textField($model,'clientes',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Nombre','required'=>true)); ?>
                            <?php echo $form->error($model,'clientes',array('class'=>'btn-xs alert-danger text-center')); ?>
                        
                      </div>


                       <div class="col-md-1 col-sm-6 col-xs-12 form-group ">
                           <label for="fullname">Área  </label>
                            <?php echo $form->textField($model,'area1',array('type'=>'numeric','id'=>'fullname','class'=>'form-control','value'=>'506','style'=>'padding:0')); ?>
                            <?php echo $form->error($model,'area1',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                        <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                       
                            <label for="fullname">Teléfono  </label>
                            <?php echo $form->textField($model,'telefono',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Teléfono','maxlength'=>20,'onkeypress'=>'return soloNumeros(event)')); ?>
                            <?php echo $form->error($model,'telefono',array('class'=>'btn-xs alert-danger text-center')); ?>
                       
                      </div>


                      <div class="col-md-2 col-sm-6 col-xs-12 form-group  " >
                         <label for="fullname">Área </label>
                            <?php echo $form->textField($model,'area2',array('type'=>'text','id'=>'fullname','class'=>'form-control','value'=>'506','style'=>'font-size: 12px;')); ?>
                            <?php echo $form->error($model,'area2',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                       

                      <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
                           <label for="fullname"> Fax  </label>
                            <?php echo $form->textField($model,'fax',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Fax','maxlength'=>20,'onkeypress'=>'return soloNumeros(event)')); ?>
                            <?php echo $form->error($model,'fax',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                        <div class="col-md-2 col-sm-6 col-xs-12 form-group ">
                       
                            <label for="fullname"> Email  </label>
                            <?php echo $form->emailField($model,'correo',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Email','email'=>true)); ?>
                            <?php echo $form->error($model,'correo',array('class'=>'btn-xs alert-danger text-center')); ?>
                       
                      </div>


                      <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                         <label for="fullname">Apartado  </label>
                            <?php echo $form->textField($model,'apartado',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Apartado')); ?>
                            <?php echo $form->error($model,'apartado',array('class'=>'btn-xs alert-danger text-center')); ?>
                        
                      </div>
                      <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                         <label for="fullname"> Web</label>
                            <?php echo $form->urlField($model,'web',array('type'=>'url','id'=>'fullname','class'=>'form-control','placeholder'=>'Web')); ?>
                            <?php echo $form->error($model,'web',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                       <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                           <label for="fullname"> *País</label>
                                 
                        <?php echo $form->dropDownList($model,'pais',CHtml::listData(Pais::model()->findAll(),'id_pais', 'pais'),array('class'=>'form-control select2','style'=>'width:100%','required'=>true)) ?>
                        <?php echo $form->error($model,'pais',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>


                        <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                            <label for="fullname"> *Provincia</label>
                            <?php
                            if ($model->isNewRecord) {
                                $model->id_provincia=1;
                            }
                            
                                    echo $form->dropDownList($model, 'id_provincia',$estadoListData, array(
                                         'class'=>'form-control select2',
                                         'style'=>'width:100%',
                                         'required'=>true,
                                       

                                        'ajax'=>array(
                                            'type'=>'POST',
                                            'url'=>$this->createUrl('SelectMunicipio'),
                                            'update'=>'#' . CHtml::activeId($model, 'id_canton'),
                                            
                                        ),
                                    ));
                                    ?>
                            <?php echo $form->error($model,'id_provincia',array('class'=>'btn-xs alert-danger text-center'));
                             ?>
                          </div>


                         <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                          <label for="fullname"> *Cantón</label>
                           <?php 

                               if(!$model->isNewRecord){
                                   
                                    $lista1=Canton::model()->find('id_canton=:id_canton',array(':id_canton'=>$model->id_canton));
                                   
                                   
                                    $model->id_canton=$lista1->id_canton;


                                  }
                                  elseif($model->isNewRecord){
                                    $list=Canton::model()->find('id_canton=1');
                                    $model->id_canton=$list->id_canton;
                                  }



                            ?>

                           <?php
                                  echo $form->dropDownList($model, 'id_canton',CHtml::listData(Canton::model()->findAll('provincia_id =:provincia_id', array(':provincia_id'=>$model->id_provincia)), 'id_canton', 'canton'), 
                                    array(
                                        'class'=>'form-control select2',
                                         'style'=>'width:100%',
                                         'required'=>true,
                                         

                                        'ajax'=>array(
                                            'type'=>'POST',
                                            'url'=>$this->createUrl('SelectParroquia'),
                                            'update'=>'#' . CHtml::activeId($model, 'id_distrito'),
                                            
                                      ),
                                  ));
                                  
                                  ?>
                          <?php echo $form->error($model,'id_canton',array('class'=>'btn-xs alert-danger text-center')); ?>
                          </div>
                          

                          <div class="col-md-3 col-sm-6 col-xs-12 form-group ">
                          <label for="fullname"> *Distrito</label>

                          <?php

                            if (!$model->isNewRecord ){
                                   
                                    $lista2=Distrito::model()->find('id_distrito=:id_distrito',array(':id_distrito'=>$model->id_distrito));
                                   
                                   
                                    $model->id_distrito=$lista2->id_distrito;
                                  }
                                  else{
                                    $lista2=Distrito::model()->find('id_distrito=1');
                                    $model->id_distrito=$lista2->id_distrito;
                                  }



                           ?>

                           <?php echo $form->dropDownList($model, 'id_distrito',CHtml::listData(Distrito::model()->findAll('canton_id =:canton_id', array(':canton_id'=>$model->id_canton)), 'id_distrito', 'distrito'), array('class'=>'form-control select2','style'=>'width:100%' ,'required'=>true)); ?>

                          <?php echo $form->error($model,'id_distrito',array('class'=>'btn-xs alert-danger text-center')); ?>
                         </div>

                      <div class="col-md-4 col-sm-6 col-xs-12 form-group ">
                           <label for="fullname"> Email Nº2  </label>
                            <?php echo $form->emailField($model,'correo2',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Email Nº2')); ?>
                            <?php echo $form->error($model,'correo2',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>
                        <div class="col-md-8 col-sm-6 col-xs-12 form-group ">
                           <label for="fullname">  *Dirección  </label>
                            <?php echo $form->textArea($model,'direccion',array('type'=>'text','id'=>'fullname','class'=>'form-control','placeholder'=>'Dirección','size'=>'100%','required'=>true )); ?>
                            <?php echo $form->error($model,'direccion',array('class'=>'btn-xs alert-danger text-center')); ?>
                      </div>

                       <div class="col-md-7  col-sm-6 col-xs-12  pull-right">
                        <div class="col-md-4  col-md-offset-5 ">
                          
                            <?php echo CHtml::submitButton('Guardar',array('class'=>"btn btn-dark btn-block  btn-lg",'style'=>'background-color:#ff333a;font-weight:bold;')); ?>
                          </div>
                        <div class="col-md-3">
                           <?php echo CHtml::link('<span class="glyphicon glyphicon-step-backward " aria-hidden="true"></span>  Ir Atrás',array('/registro/clientes/admin'),array('id'=>'bt1','class'=>'btn btn-dark btn-block btn-lg','style'=>'font-weight:bold;')); ?>
                        </div>
                          
                        </div>
                       
<?php $this->endWidget(); ?> 


<script src="themes/gentelella/vendors/jquery/dist/jquery.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
 

$( document ).ready(function() {//poner esto para q funcione bien
  
$( "#productos-form" ).submit(function( event ) {

var tipo=$( "#Clientes_id_tipo_identificacioj" ).val();
var cedula=$( "#identificacion" ).val();

if(isNaN(cedula)){
 swal("Atención!", "Identificación solo puede ser numerico.","error");
 $( "#identificacion" ).val('');
  event.preventDefault();
}

 else if(tipo==1)
{
  if(cedula.length<9 ||cedula.length>9){

  swal("Atención!", "El número de identificación para Cédula Física debe estar compuesta por 9 digítos numéricos.","error");
  event.preventDefault();
}

}
else if(tipo==2)
{
  if(cedula.length<10 ||cedula.length>10){

  swal("Atención!", "El número de identificación para Cédula Jurídica debe estar compuesta por 10 digítos numéricos.","error");
  event.preventDefault();
}

}
else if(tipo==3)
{
  if(cedula.length!=11 ||cedula.length!=12){

  swal("Atención!", "El número de identificación para DIMEX debe estar compuesta por 11 o 12 digítos numéricos.","error");
  event.preventDefault();
}

}
else if(tipo==4)
{
  if(cedula.length!=10 ){

  swal("Atención!", "El número de identificación para NITE debe estar compuesta por 10 digítos numéricos.","error");
  event.preventDefault();
}

}
else if(cedula!=4)
{
  if(cedula.length!=10 ){

  swal("Atención!", "El número de identificación para NITE debe estar compuesta por 10 digítos numéricos.","error");
  event.preventDefault();
}

}


});
});

 function alumno()      
{

  var cedula=$( "#identificacion" ).val();

$.ajax({
      method: "POST",
      //dataType: "json",//trae arry
      dataType: "json",//trae html
      url: "<?php echo CController::createUrl('/registro/clientes/detalleMaterial2') ?>", 
      //async: false,
      data: { id_cliente: cedula}//valosr q pasa por post para la consulta
    })
    .done(function( msg ) {

if(msg==1){
  swal("Atención!", "identificación no se puede repetir","error");
  $( "#identificacion" ).val('');
}
    });

}

function soloNumeros(e){

  var key = window.Event ? e.which : e.keyCode

  return (key >= 48 && key <= 57)

}

</script>

<?php if($_GET['guardado']==1){ ?>

 <script > swal("Guardado!", "Registro guardado exitosamente!", "success");</script>

<?php } ?>


