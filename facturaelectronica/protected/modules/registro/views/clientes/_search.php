<?php
/* @var $this ClientesController */
/* @var $model Clientes */
/* @var $form CActiveForm */

?>

<div class="wide formxx">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	
)); ?>

<?php 
   $tipo=$_GET['d'];
   if($tipo==1){
    $msg="none";
   }else if(!$tipo){
    $msg="block ";
   }
   ?>


<div class="col col-lg-5  col-xs-12" >
		<label  style='color:#22C7FC;font-weight:bold'>Nombre o Identificación :</label>
		<?php echo $form->textField($model,'id_cliente',array('style'=>'width:100%','class'=>'form-control','placeholder'=>'Ingrese el nombre o identificación')); ?>
</div>

<div class="col col-lg-2  col-xs-12 ">
			<br>
			<?php echo CHtml::submitButton('Buscar',array('class'=>'btn btn-dark btn-lg btn-block has-feedback')); ?>
</div>

<div class="col col-lg-2  col-xs-12  "> <br>
		<button onclick="window.location.reload()" class="btn btn-dark btn-block btn-lg pull-left has-feedback">Refrescar</button>
</div>

<div class="col col-lg-3 col-xs-12 "style="display:<?php echo $msg;?>"><br>
            <?php echo CHtml::link('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo ',array('clientes/create'),array('id'=>'bt1','class'=>"btn btn-lg btn-block has-feedback",'style'=>'background-color: #99cc33;color:white'));?>
 </div>

<?php $this->endWidget(); ?>
<br>
</div><!-- search-form -->