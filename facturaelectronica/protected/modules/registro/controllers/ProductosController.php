<?php

class ProductosController extends Controller 
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';   

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','admin2','detalleMaterial2','Restaurar'), 
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','DetalleMaterial'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
			$this->layout ='//layouts/column1';
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{	$clientes=ListaImpuesto::model()->findAll();
			$this->layout ='//layouts/main';
		$model=new Productos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Productos']))
		{

			$model->attributes=$_POST['Productos'];
		
			$valido=$model->validate();

			if($valido){

				$model->save();				
				$this->redirect(array('create&guardado=1'));
			}
				
		}

		$this->render('create',array(
			'model'=>$model,'clientes'=>$clientes
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{	$clientes=ListaImpuesto::model()->findAll();
		$this->layout ='//layouts/main';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Productos']))
		{
			$model->attributes=$_POST['Productos'];
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,'clientes'=>$clientes
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model =$this->loadModel($id);

		if(!(count($model->unidadMedida)>0)) {
			$model->st_producto=false;
			$model->save();
		} else  {
			
			$model->st_producto=false;
			$model->save();
			//throw new CHttpException(500,'Este usuario posee facturas activas');
			//mensaje no se puede borrar
		}
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	public function actionRestaurar($id)
	{
		$model =$this->loadModel($id);
			$model->st_producto=true;
			$model->save();
			
			if(!isset($_GET['ajax'])){
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

		}
	}
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Productos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layout ='//layouts/main';
		$model=new Productos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Productos']))
			$model->attributes=$_GET['Productos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	public function actionAdmin2()
	{
		$this->layout ='//layouts/main';
		$model=new Productos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Productos']))
			$model->attributes=$_GET['Productos'];

		$this->render('admin2',array(
			'model'=>$model,
		));
	}

	


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Productos the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Productos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Productos $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='productos-form') 
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionDetalleMaterial()
	{

		$clientes=ListaImpuesto::model()->findByPk($_POST['id_cliente']);

if($_POST['prueba']=='0')
{
if($clientes->impuesto_nombre=='Impuesto de Ventas'){
		$tabla='
<table class="table table-responsive table-striped" >
 				<tr style="color:#22C7FC;background-color: transparent;font-weight: bold;  ">
                           <td >ID</td>
                            <td colspan="2">Nombre</td>
                            <td>Porcentaje</td>
                            <td></td>
                          </tr>
		    <tr id=1>
		      <td>'.$clientes->id_impuesto.'</td>
		      <td colspan="2">'.$clientes->impuesto_nombre.'</td>
		      <td>'.$clientes->monto.'</td>
		      <td > <a href=# onclick="eliminar('.$value->id_impuesto.')"><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="eliminar" ></span></a></td>
		    </tr>
		
                     </table> 
                       ';	
	}

if($clientes->impuesto_nombre=='Impuesto de Servicio'){

?>
<script>
swal('Atención!','Este impuesto solo puede ser agregado a un servicio','warning'); 
</script>

 <?php

$tabla='  <table class="table table-responsive table-striped"  >
   <tr style="color:#22C7FC;background-color: transparent;font-weight: bold;  ">
 			<td>ID</td>
            <td colspan="2">Nombre</td>
            <td>Porcentaje</td>
   </tr>
                           </table> 
                           <br>';

}	

}
if($_POST['prueba2']=='1')
{
if($clientes->impuesto_nombre=='Impuesto de Servicio'){
		$tabla='
<table class="table table-responsive table-striped" >
 				<tr style="color:#22C7FC;background-color: transparent;font-weight: bold;  ">
                           <td >ID</td>
                            <td colspan="2">Nombre</td>
                            <td>Porcentaje</td>
                            <td></td>
                          </tr>
		    <tr id=1>
		      <td>'.$clientes->id_impuesto.'</td>
		      <td colspan="2">'.$clientes->impuesto_nombre.'</td>
		      <td>'.$clientes->monto.'</td>
		      <td > <a href=# onclick="eliminar('.$value->id_impuesto.')"><span class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="eliminar" ></span></a></td>
		    </tr>
		
                     </table> 
                       ';	
	}

if($clientes->impuesto_nombre=='Impuesto de Ventas'){

?>
<script>
swal('Atención!','Este impuesto solo puede ser agregado a un producto','warning');
</script>

 <?php

$tabla=' <table class="table table-responsive table-striped"  >
   <tr style="color:#22C7FC;background-color: transparent;font-weight: bold;  ">
 			<td>ID</td>
            <td colspan="2">Nombre</td>
            <td>Porcentaje</td>
   </tr>
                           </table> 
                           <br>';

}	

}
                   
echo $tabla;

}
		


	public function actionDetalleMaterial2()
	{

		$clientes=Productos::model()->find('codigo='.$_POST['id_cliente']);
		//echo CJSON::encode($clientes);

		if($clientes){
			$esta=1;
		}else{
			$esta=0;
		}

		echo CJSON::encode($esta);
		
	}	

}
