<?php

/**
 * This is the model class for table "distrito".
 *
 * The followings are the available columns in table 'distrito':
 * @property integer $id_distrito
 * @property string $distrito
 * @property integer $id_canton
 */
class Distrito extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'distrito';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('distrito, id_canton', 'required'),
            array('id_canton', 'numerical', 'integerOnly'=>true),
            array('distrito', 'length', 'max'=>20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_distrito, distrito, id_canton', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_distrito' => 'Id Distrito',
            'distrito' => 'Distrito',
            'id_canton' => 'Id Canton',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_distrito',$this->id_distrito);
        $criteria->compare('distrito',$this->distrito,true);
        $criteria->compare('id_canton',$this->id_canton);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Distrito the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}