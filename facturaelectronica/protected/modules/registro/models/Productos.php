<?php
/**
 * This is the model class for table "productos".
 *
 * The followings are the available columns in table 'productos':
 * @property string $id_productos
 * @property string $nombre
 * @property string $codigo
 * @property integer $tipo_producto
 * @property string $descripcion
 * @property integer $precioc_id
 * @property integer $precios_id
 * @property integer $unidad_medida_id
 * @property integer $tipo_codigo_id
 * @property integer $precio_finalc_id
 * @property integer $precio_finals_id
 * @property integer $otro_impuesto
 * @property integer $lista_impuesto_id
 */
class Productos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'productos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			

			array('preciosinin_puesto,precio_finalc_id,codigo,tipo_producto, precio_finals_id, tipo_codigo_id,descripcionc, descripcion, otro_impuesto, lista_impuesto_id, nombre,precios_id,unidad_medida_id,precioc_id,preciossinin_puesto','safe'),

			//array('tipo_codigo_id,tipo_producto,descripcion,unidad_medida_id,preciossinin_puesto,codigo,precioc_id,preciosinin_puesto,precios_id, precio_finals_id, precio_finalc_id ','required'),

			array('id_productos','safe'),

			
			
			array('codigo', 'length', 'max'=>20),
			array('descripcion', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('descripcionc,preciossinin_puesto,preciosinin_puesto,id_productos, nombre, codigo, tipo_producto, descripcion, precioc_id, precios_id, unidad_medida_id, tipo_codigo_id, precio_finalc_id, precio_finals_id, otro_impuesto, lista_impuesto_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'unidadMedida' => array(self::BELONGS_TO, 'UnidadMedida', 'unidad_medida_id'),
            'tablaProductos' => array(self::HAS_MANY, 'TablaProducto', 'id_producto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_productos' => ' Productos',
			'nombre' => 'Nombre',
			'codigo' => 'Codigo del Producto',
			'tipo_producto' => 'Tipo Producto',
			'descripcion' => 'Descripcion',
			'precioc_id' => 'Precio  ₡',
			'precios_id' => 'Precio $',
			'unidad_medida_id' => 'Unidad Medida',
			'tipo_codigo_id' => 'Tipo Codigo',
			'precio_finalc_id' => 'Precio Final  ₡ ',
			'precio_finals_id' => 'Precio Final $',
			'otro_impuesto' => 'Otro Impuesto',
			'lista_impuesto_id' => 'Lista Impuesto',
			'preciosinin_puesto'=>'Precio antes de Impuesto $ ',
			'preciossinin_puesto'=>'Precio antes de Impuesto $',
			'descripcionc'=>'Descripción Corta'

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{	 
		
		
	
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$tipo=$_GET['d'];
		if($tipo==1){
			$criteria->compare('st_producto',0);
		}else if(!$tipo){
		$criteria->compare('st_producto',1);
		}
				
       $criteria->join="LEFT JOIN unidad_medida um ON t.unidad_medida_id = um.id_um";
        $criteria->addSearchCondition('codigo',$this->id_productos,true, 'OR');
        $criteria->addSearchCondition('descripcionc',$this->id_productos,true, 'OR');
        $criteria->addSearchCondition('um.um',$this->id_productos,true,'OR');
        $criteria->addSearchCondition('descripcion',$this->id_productos,true,'OR');	

		 
			$sort=new CSort();
             $sort->defaultOrder='id_productos DESC';
              $sort->attributes=array('id_productos'=>array(
                                    'asc'=>'id_productos',
                                    'desc'=>'id_productos DESC',
                                ),
                                'codigo'=>array(
                                    'asc'=>'codigo ',
                                    'desc'=>'codigo DESC'
                                ),
                                'tipo_producto'=>array(
                                    'asc'=>'tipo_producto',
                                    'desc'=>'tipo_producto DESC'
                                ),
                                'descc'=>array(
                                    'asc'=>'descripcionc',
                                    'desc'=>'descripcionc DESC'
                                ),
                                 'descr'=>array(
                                    'asc'=>'descripcion',
                                    'desc'=>'descripcion DESC'
                                ),
                                  'precioc'=>array(
                                    'asc'=>'precioc_id',
                                    'desc'=>'precioc_id DESC'
                                ),
                                   'precios'=>array(
                                    'asc'=>'precios_id',
                                    'desc'=>'precios_id DESC'
                                ),
                                  'unidad'=>array(
                                    'asc'=>'um.um',
                                    'desc'=>'um.um DESC'
                                ), 
                                   'inventario'=>array(
                                    'asc'=>'codigo',
                                    'desc'=>'codigo DESC'
                                ), 
            );


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			   'sort'=>$sort
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Productos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
