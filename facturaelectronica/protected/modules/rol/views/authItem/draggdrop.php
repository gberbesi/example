    <link rel="stylesheet" type="text/css" href="themes/classic/plugins/dualListbox-3.2/bootstrap-duallistbox.css">
    <script src="themes/classic/plugins/dualListbox-3.2/jquery.bootstrap-duallistbox.js"></script>

      <div class="nav-tabs-custom">

        <ul class="nav nav-tabs pull-right">
          <li><a href="#tab_3-2" data-toggle="tab">Operac.</a></li>
          <li><a href="#tab_2-2" data-toggle="tab">Tarea</a></li>
          <li class="active"><a href="#tab_1-1" data-toggle="tab">Rol</a></li>                  
        </ul>

        <div class="tab-content text-center">
          <div class="tab-pane active" id="tab_1-1"><!--tab-pane -->
            <select multiple="multiple" size="10" name="roles[]">
                <!-- este toma los no seleccionados -->
              <?php foreach($roles as $rol) { echo "<option class='".'text-center'."' value='".$rol."'>".$rol."</option>"; } ?>
                <!-- este toma los seleccionados -->
              <?php foreach($rolesChild as $rol) { echo "<option class='".'text-center'."' value='".$rol['child']."' selected='".'selected'."'>".$rol['child']."</option>"; } ?>
            </select>
          </div><!-- /.tab-pane -->

          <div class="tab-pane" id="tab_2-2"><!--tab-pane -->
            <select multiple="multiple" size="10" name="tareas[]">
                  <!-- este toma los no seleccionados -->
                <?php foreach($tareas as $tarea) { echo "<option class='".'text-center'."' value='".$tarea."'>".$tarea."</option>"; } ?>
                  <!-- este toma los seleccionados -->
                <?php foreach($tareasChild as $tarea) { echo "<option class='".'text-center'."' value='".$tarea['child']."' selected='".'selected'."'>".$tarea['child']."</option>"; } ?>
            </select>
          </div><!-- /.tab-pane -->

          <div class="tab-pane" id="tab_3-2"><!--tab-pane -->
              <select multiple="multiple" size="10" name="operaciones[]">
                  <!-- este toma los no seleccionados -->
                <?php foreach($operaciones as $operacion) { echo "<option class='".'text-center'."' value='".$operacion."'>".$operacion."</option>"; } ?>
                  <!-- este toma los seleccionados -->                
                <?php foreach($operacionesChild as $operacion) { echo "<option class='".'text-center'."' value='".$operacion['child']."' selected='".'selected'."'>".$operacion['child']."</option>"; } ?>
              </select>
          </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
      </div><!-- nav-tabs-custom -->



  <script>

    var roles = $('select[name="roles[]"]').bootstrapDualListbox();
    $("#demoform").submit(function() {
      alert($('[name="roles[]"]').val());
      return false;
    });

    var tareas = $('select[name="tareas[]"]').bootstrapDualListbox();
    $("#demoform").submit(function() {
      alert($('[name="tareas[]"]').val());
      return false;
    });    

    var operaciones = $('select[name="operaciones[]"]').bootstrapDualListbox();
    $("#demoform").submit(function() {
      alert($('[name="operaciones[]"]').val());
      return false;
    });    

  </script>







