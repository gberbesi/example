
        <div>
          <?php foreach(Yii::app()->user->getFlashes() as $key => $message) {
                  echo '<div class="alert alert-success" style="text-align:center;">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  ' . $message . "</div>\n"; } ?>
        </div>


            <div class="box box-default box-solid">
                <div class="box-header with-border">
                <spam class="glyphicon glyphicon-cog icon-red"></spam>
                  <h3 class="box-title">Gestor de Autorizaci&oacute;n</h3>
                  <div class="box-tools pull-right">
                    <button title="Refrescar" onclick="window.location.reload()" class="btn btn-box-tool" data-toggle="tooltip"><i class="glyphicon glyphicon-repeat"></i></button>                  
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->

                  <div class="box-body table-responsive">

                         <?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'auth-item-grid',
                        'dataProvider'=>$model->search(),
                        'filter'=>$model,
                        'itemsCssClass'=>'table table-striped table-bordered text-center',
                        'pagerCssClass'=>'pagination pull-right',
                        'afterAjaxUpdate'=>'function(){$.getScript("themes/classic/plugins/archivos_js/campo-numero.js");$.getScript("themes/classic/plugins/archivos_js/campo-letra.js");
                                        $.getScript("themes/classic/plugins/archivos_js/script_select2.js");}',
                        'pager'=>array(
                            'htmlOptions'=>array('class'=>'pagination pagination-sm no-margin'),
                            'cssFile'=>'',
                            'hiddenPageCssClass'=>'disabled',
                            'selectedPageCssClass'=>'active',
                            'header'=>''), 
                        'columns'=>array(

                            array(    
                              'name'=>'name',
                              'htmlOptions'=>array('class'=>'col-xs-4 text-center'),
                              'value'=>'$data->name',
                              'filter'=>CHtml::activeTextField($model,'name',array('class'=>'form-control text-center campo-letra','placeholder'=>'NOMBRE','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),  
                            ),                            

                            array(
                              'name'=>'type', 
                              'htmlOptions'=>array('class'=>'col-xs-2 text-center'),
                              'value'=>'$data->tipoItem($data->type)',
                              'filter' =>CHtml::activeDropDownList($model,'type',array('2' => 'ROL', '1' =>'TAREA', '0' => 'OPERACION'),array('class'=>'form-control select2 text-center','empty'=>'SELECCIONE')),
                            ),

                            array(    
                              'name'=>'description',
                              'htmlOptions'=>array('class'=>'col-xs-4 text-center'),
                              'value'=>'$data->description',
                              'filter'=>CHtml::activeTextField($model,'description',array('class'=>'form-control text-center campo-letra','placeholder'=>'DESCRIPCION','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),  
                            ),  

                                  array(
                                    'header'=>"Acción",
                                    'class'=>'CButtonColumn',
                                    'htmlOptions'=>array('class'=>'col-xs-5 text-center'),
                                    'template' => '{view} {update} {delete} ',

                                    'buttons'=>array(
                                      'view' => array(
                                        'label'=>'Ver',
                                        'options'=>array('class'=>'fancybox', 'data-fancybox-type'=>'iframe','data-toggle'=>"tooltip"),
                                           ),

                                      'update' => array(
                                        'label'=>'Actualizar',
                                        'options'=>array('data-toggle'=>"tooltip"),
                                      ), 

                                      'delete' => array(
                                                    'label'=> 'Eliminar',
                                                    'url' =>'Yii::app()->controller->createUrl("authItem/delete",array("id"=>$data->name,))',
                                                    'options'=>array('data-toggle'=>"tooltip"),
                                                    'click'=>"function() {
                                                            if(!confirm('¿Seguro que desea eliminar registro?')) return false;
                                                            $.fn.yiiGridView.update('auth-item-grid', {
                                                                    type:'POST',
                                                                    url:$(this).attr('href'),
                                                                    success:function(texto) {
                                                                        if(texto=='eliminado')alert('Elemento de autorizacion ha sido eliminado exitosamente.');
                                                                        else if(texto=='asignado')alert('Error, elemento de autorizacion está asignado actualmente a uno o mas usuarios.');
                                                                        else if(texto=='relacionado')alert('Error, elemento de autorizacion está relacionado actualmente con uno o mas elementos de autorizacion.');
                                                                        else if(texto=='no existe')alert('Error, elemento de autorizacion no existe.');
                                                                        else alert('Error al eliminar elemento de autorizacion.');
                                                                            $.fn.yiiGridView.update('auth-item-grid');
                                                                    }
                                                            });
                                                            return false;
                                                    }",
                                    ),
                                  ),
                                  ),),)); ?>

                  </div><!-- /.box-body -->
            </div><!-- /.box -->

