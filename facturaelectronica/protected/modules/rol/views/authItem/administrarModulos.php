 

            <!-- general form elements -->
              <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <spam class="glyphicon glyphicon-cog icon-red"></spam>
                    <h3 class="box-title">Modulos del sistema</h3>
                </div><!-- /.box-header -->

                  <?php $form=$this->beginWidget('CActiveForm', array('id'=>'administrar-modulos-form','enableAjaxValidation'=>false,));?>

                  <div class="box-body">

                    <?php Yii::app()->clientScript->registerCoreScript('jquery');?>
                    <?php Yii::app()->clientScript->registerScript('suno','function buscarData(){ var modulo=$("#AuthItem_modulos2 option:selected").text();var modulo_value=$("#AuthItem_modulos2 option:selected").attr("value");
                                                                    if(modulo_value==""){ $("#bVerReglas").css("display","none");
                                                                    $(\'#acciones\').html("");
                                                                    return;}           
                                                                    $("#bVerReglas").css("display","block");
                                                                    c="'.CController::createUrl('/rol/authItem/BuscarAcciones').'"
                                                                        $.ajax({ url: c, cache: false, type: "POST", data: ({modulo : modulo}), beforeSend: function() {$(\'#acciones\').html(\'Cargando...\')}, 
                                                                            success: function(data) { if(data==\'\')data="No se encontraron nuevas acciones del modulo "+modulo; $(\'#acciones\').html(data);}});
                                                                    }',CClientScript::POS_HEAD); ?>
                    <?php echo $form->errorSummary($model,array('class'=>'alert alert-danger text-center')); ?>
                    
                    <div class="form-group table table-striped table-bordered table-responsive">
                      <div class="form-group">
                        <?php echo $form->labelEx($model,'modulos2'); ?>
                          <?php  $mod=Yii::app()->metadata->getModules();
                                 $mod2=array();
                                   foreach ($mod as $i => $value) {
                                      if ($mod[$i]=='.svn'){ unset($mod[$i]); } else { $mod2[$value]=$value; }
                                   }
                              echo $form->dropDownList($model,'modulos2', $mod2, array('class'=>'form-control select2','style'=>'width: 100%;','empty'=>'--SELECCIONE--','onChange'=>'buscarData();')); ?>
                              <?php echo $form->error($model,'modulos2'); ?>
                      </div>

                      <div id="bVerReglas" class="box-footer text-center" style="display: none">
                        <?php echo CHtml::button('Ver Reglas', array('class'=>'btn btn-info','submit' => array('/rol/authItem/VerReglas')));  ?>  
                      </div>
      
                      <div id="acciones" class="form-group text-center">
                                
                      </div>

                    </div>

                <?php $this->endWidget(); ?>

                  </div><!-- /.box-body -->
              </div><!-- /.box -->

      <div class="control-sidebar-bg"></div>
 







