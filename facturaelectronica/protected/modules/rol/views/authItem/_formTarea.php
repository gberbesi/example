
           <!-- general form elements -->
        <div class="box box-default box-solid">
                <div class="box-header with-border">
	                <spam class="glyphicon glyphicon-floppy-save"></spam>
					<h2 class="box-title"><?php echo ($model->isNewRecord)? "Registrar Tarea": "Actualizar Tarea";?></h2>
						<div class="box-tools pull-right">
		                	<?php echo CHtml::link('<span class="glyphicon glyphicon-share-alt"></span>',array('/rol/authItem/admin'),array("class"=>"btn btn-box-tool","data-toggle"=>"tooltip","title"=>"Ir al Administrador"));?>
		                 </div><!-- /.box-tools -->
                </div><!-- /.box-header -->

					<?php $form=$this->beginWidget('CActiveForm', array('id'=>'auth-item-form','enableAjaxValidation'=>true,'htmlOptions' => array('OnSubmit'=>'$("#box2Clear").click();$("#box4Clear").click();$("#box6Clear").click();'),));?>
					<p class="note text-center text-success"> Campos con <span class="required">*</span> son requeridos.</p>

					<?php //echo $form->errorSummary(array($model,$descripcion)); ?>

	                <form role="form">
		                <div class="box-body">

							<div class="form-group has-feedback <?php echo ($model->hasErrors('name'))?'error':(($getPost)?'success':''); ?>">
									<?php echo $form->labelEx($model,'name',array('class'=>'control-label')); ?>
									<?php echo $form->textField($model,'name',array(  'readonly'=>($model->isNewRecord) ? false:true,'type'=>'text', 
																							'class'=>'form-control','placeholder'=>$model->getAttributeLabel('name'),
																							'onkeyup'=>'javascript:this.value=this.value.toUpperCase();','maxlength'=>'64')); ?>
									<?php echo $form->error($model,'name',array('class'=>'alert alert-danger text-center')); ?>
								 <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
							</div>

							<div class="form-group has-feedback <?php echo ($model->hasErrors('description'))?'error':(($getPost)?'success':''); ?>">
									<?php echo $form->labelEx($model,'description',array('class'=>'control-label')); ?>
									<?php echo $form->textArea($model,'description',array( 'type'=>'text', 'class'=>'form-control',
																								 'placeholder'=>$model->getAttributeLabel('description'),
																								 'onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>
									<?php echo $form->error($model,'description',array('class'=>'alert alert-danger text-center')); ?>
						        <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
							</div>					
							
							<div class="form-group">
								<?php echo $this->renderPartial('draggdropTarea', array( 'model'=>$model,'roles'=>$roles,
																						 'tareas'=>$tareas,'operaciones'=>$operaciones,
																						 'rolesChild'=>$rolesChild,'tareasChild'=>$tareasChild,
																						 'operacionesChild'=>$operacionesChild)); ?>
							</div>

		                </div><!-- /.box-body -->

						<div class="box-footer text-center">
							<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Actualizar',array('confirm'=>'¿Está seguro de Guardar los Cambios?','class'=>'btn btn-success')); ?>
						</div>

	                </form>
	                <?php Yii::app()->clientScript->registerScript('search', "");?>
				<?php $this->endWidget(); ?>                
        </div><!-- /.box -->

