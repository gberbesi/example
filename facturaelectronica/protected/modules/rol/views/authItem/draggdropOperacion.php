    <link rel="stylesheet" type="text/css" href="themes/classic/plugins/dualListbox-3.2/bootstrap-duallistbox.css">
    <script src="themes/classic/plugins/dualListbox-3.2/jquery.bootstrap-duallistbox.js"></script>

      <div class="nav-tabs-custom">

        <ul class="nav nav-tabs pull-right">
          <li class="active"><a href="#tab_3-2" data-toggle="tab">Operac.</a></li>
        </ul>

        <div class="tab-content text-center">

          <div class="tab-pane active" id="tab_3-2"><!--tab-pane -->
              <select multiple="multiple" size="10" name="operaciones[]">
                  <!-- este toma los no seleccionados -->
                <?php foreach($operaciones as $operacion) { echo "<option class='".'text-center'."' value='".$operacion."'>".$operacion."</option>"; } ?>
                  <!-- este toma los seleccionados -->                
                <?php foreach($operacionesChild as $operacion) { echo "<option class='".'text-center'."' value='".$operacion['child']."' selected='".'selected'."'>".$operacion['child']."</option>"; } ?>
              </select>
          </div><!-- /.tab-pane -->

        </div><!-- /.tab-content -->
      </div><!-- nav-tabs-custom -->



  <script>

    var operaciones = $('select[name="operaciones[]"]').bootstrapDualListbox();
    $("#demoform").submit(function() {
      alert($('[name="operaciones[]"]').val());
      return false;
    });    

  </script>


