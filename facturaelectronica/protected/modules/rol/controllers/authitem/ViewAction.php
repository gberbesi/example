<?php
class ViewAction extends CAction
{
   public function run($id)
   {
	$this->controller->layout='//layouts/partial';	
	$model=$this->controller->loadModel($id);
	$this->controller->render('view',array('model'=>$model));
	}
}