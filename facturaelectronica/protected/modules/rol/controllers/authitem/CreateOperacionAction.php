<?php
class CreateOperacionAction extends CAction
{
   public function run()
   {
		$model=new AuthItem;
		$getPost=false;

		// Uncomment the following line if AJAX validation is needed
		$this->controller->performAjaxValidation($model);

		if(isset($_POST['AuthItem']))
		{
			$model->attributes=$_POST['AuthItem'];
			$getPost=true;
			//if($model->save())
			if($model->validate()){
				$auth=Yii::app()->authManager;
		    	$bizRule='';
		    	
				try
				{
					$auth->createOperation($model->name, $model->description, $bizRule);
					if(isset($_POST['roles'])){
						$roles=$_POST['roles'];
						for($i=0;$i<count($roles);$i++) {
							$auth->addItemChild($model->name,$roles[$i]);
						}
					}
					
					if(isset($_POST['tareas'])){
						$tareas=$_POST['tareas'];
						for($i=0;$i<count($tareas);$i++) {
							$auth->addItemChild($model->name,$tareas[$i]);
						}
					}					

					if(isset($_POST['operaciones'])){
						$operaciones=$_POST['operaciones'];
						for($i=0;$i<count($operaciones);$i++) {
							$auth->addItemChild($model->name,$operaciones[$i]);
						}
					}	
					Yii::app()->user->setFlash('notice', 'Registro guardado exitósamente. <br/> Para ver el registro haga click '.CHtml::link('Aquí',array('authItem/view','id'=>$model->name),array('target'=>'_blank','class'=>'fancybox')).'<br/> Para modificar el registro haga click '.CHtml::link('Aquí',array('authItem/update','id'=>$model->name)));
					$this->controller->redirect(array('admin'));
				}
				catch(Exception $e) // an exception is raised if a query fails
				{
				    throw new CHttpException('500','No se puede crear el Item.');
				    
					$this->controller->render('create',array(
						'model'=>$model,
					));
				}	    	
		    	
		    	//$auth->createRole($model->name, $model->description, $bizRule);
					//$this->controller->redirect(array('view','id'=>$model->name));
			}
		}

		$roles=array();//CHtml::listData(AuthItem::model()->findAll('type=:tipo',array(':tipo'=>2)),"name","name");
		$tareas=CHtml::listData(AuthItem::model()->findAll('type=:tipo',array(':tipo'=>1)),"name","name");
		$operaciones=CHtml::listData(AuthItem::model()->findAll('type=:tipo',array(':tipo'=>0)),"name","name");
				
		$rolesChild=array();
		$tareasChild=array();
		$operacionesChild=array();
		
		
		/*
		foreach($roles as $i=>$rol) {
					
		print($rol->name);echo "<br/><br/><br/>";
		}
		die();
		*/
		$this->controller->render('createOperacion',array(
			'model'=>$model,'roles'=>$roles,'tareas'=>$tareas,'operaciones'=>$operaciones,
		 	'rolesChild'=>$rolesChild,'tareasChild'=>$tareasChild,'operacionesChild'=>$operacionesChild,
			'getPost'=>$getPost,
		));
	}
} 
