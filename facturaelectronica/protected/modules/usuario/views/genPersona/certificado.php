<?php //Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/jquery.print.js');?>
<style type="text/css">
<!--
table.qr
{
border-collapse: collapse;
border: solid 1px black;
table-layout: fixed;
}

table.qr td
{
width: 5px;
height: 5px;
font-size: 2px;
}

table.qr td.on
{
background: #000000;
}
-->
</style>
<?php
Yii::import('application.extensions.mpdf60.qrcode.*');
require_once('qrcode.class.php');
$url=Yii::app()->getBaseUrl(true).'/index.php?r=usuario/genPersona/validarCertificado&id='.md5($datos->persona_id);
$qrcode = new QRcode(utf8_encode($url),'M');
//$qrcode->disableBorder();



$header='
		<table border="0" width="100%" style="margin:0px;">
			<tr>
			    <td align="left"><img width = "1024" heigth = "100" src="'.Yii::app()->request->baseUrl.'/images/cintillo-zamora.png" />
			      </td>
			   </tr>
		</table>';


$html='<link rel="stylesheet" type="text/css" href="'.Yii::app()->request->baseUrl.'/css/vistadata.css"/>
 				<link rel="stylesheet" type="text/css" href="'.Yii::app()->request->baseUrl.'/css/jquery.css" />
				<br>
				<br>
 <p>El Gobierno Bolivariano de Venezuela a través de la Gran Misión Vivienda Venezuela y su ente ejecutor Inmobiliaria Nacional S.A, certifican que el ciudadano (a):</p>
 <br>
 <br>
 <br>
 <br>
 <h1 align="center">'.$datos->nombre1.' '.$datos->nombre2.' '.$datos->apellido1.' '.$datos->apellido2.'</h1>
 <h2 align="center"> C.I '.number_format($datos->cedula,0,",", ".").'</h2>';


$html.='
	<br>
	<p align="justify" >Ha  sido pre-asignado en una vivienda en el Desarrollo Urbanístico Conjunto Residencial Colinas de Río Tuy, Municipio Urdaneta, Parroquia Cúa del Estado Miranda
	 y que ha realizado exitosamente la carga de documentos al Sistema http://recaudosriotuy.minhvi.gob.ve, por lo tanto está habilitado para la consignación de la documentación requerida, para su respectiva verificación.</p>

<br><br>
';


$footer='
<table border="0" width="100%" style="margin:0px;">
	<tr>
			<td align="left"><img width = "1024" heigth = "100" src="'.Yii::app()->request->baseUrl.'/images/firma.png" />
				</td>
		 </tr>
</table>
<div class="letra" style="text-align:right;font-size:10px;">
	Pág. {PAGENO}/{nbpg}
</div>
<div style="text-align:center;border-top-width:2px;border-top-style:solid;border-top-color:#AD1818;font-size:12px;font-family: arial;">
	Avenida Francisco de Miranda, Torre del Ministerio del Poder Popular para Vivienda y Hábitat,  Municipio Chacao, Edo. Miranda, Venezuela.
	Telfs: 58-212-2668625 / 58-212-2069517 / 58-212-2069519, Fax: 58-212-2654644 <br>
	<span align="center">RIF.: G -200094907 MINVIH - Copyleft 2012.<span>
</div>';
//echo $html;exit();
$pdf = Yii::createComponent('application.extensions.mpdf60.mpdf');
$mpdf=new mPDF('utf-8','LETTER','','',15,15,25,12,7,7);
$mpdf->SetHTMLHeader($header,'O');
$mpdf->SetHTMLHeader($header,'E');
$mpdf->SetHTMLFooter($footer,'O');
$mpdf->SetHTMLFooter($footer,'E');
$mpdf->WriteHTML("$html");
$qrcode->displayFPDF($mpdf,100,180,30);
$mpdf->Output('Certificado-'.$model[0]->familia_id.'.pdf','D');
exit;

?>
