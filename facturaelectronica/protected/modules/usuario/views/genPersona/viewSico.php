  <?php foreach(Yii::app()->user->getFlashes() as $key => $message) { echo '<div class="alert alert-danger" style="text-align:center;"> <button type="button" class="close" data-dismiss="alert">×</button> ' . $message . "</div>\n"; } ?>



<div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-default">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="themes/classic/dist/img/man.png" alt="User profile picture">
                  <h3 class="profile-username text-center"><?php echo $model[0]['nombre_apellido'];?></h3>
                  <p class="text-muted text-center">Cédula / Rif: <?php echo $model[0]['rif']."-".$model[0]['cedula'];?></p>
                  <p class="text-muted text-center"><?php echo $model[0]['tipo'];?></p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Telf.:</b> <a class="pull-center"><?php echo "0".$model[0]['telefono'];?></a>
                    </li>
                    <li class="list-group-item">
                      <b>Email.:</b> <a class="pull-center"><?php echo $model[0]['email'];?></a>
                    </li>
                  </ul>
               </div><!-- /.box-body -->
              </div><!-- /.box -->

            <?php foreach ($model as $key => $value):?>
              <!-- About Me Box -->
              <div class="box box-default">
                <div class="box-header with-border">
                <spam class="glyphicon glyphicon-duplicate"></spam>
                  <h3 class="box-title"><b>OBRA:</b> <?php echo $model[$key]['nombre_obra'];?></h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                  <strong><i class="fa fa-map-marker margin-r-5"></i> Ubicación </strong>
                  <p class="text-muted"><?php echo "<b>Estado</b>: ".$model[$key]['estado'];?></p>
                  <p class="text-muted"><?php echo "<b>Municipio</b>: ".$model[$key]['municipio'];?></p>
                  <p class="text-muted"><?php echo "<b>Parroquia</b>: ".$model[$key]['parroquia'];?></p>
                  <p class="text-muted"><?php echo "<b>Referencia</b>: ".$model[$key]['ubicacion'];?></p>

                  <hr>


                  <strong><i class="fa fa-book margin-r-5"></i> Inspector </strong>
                  <p class="text-muted"><?php echo "<b>Inspector</b>: ".$model[$key]['inspector'];?></p>
                  <p class="text-muted"><?php echo "<b>Cedula</b>: ".number_format($model[$key]['cedula_inspector'],0,",", ".");?></p>
                  <p class="text-muted"><?php echo "<b>Telf</b>: ".$model[$key]['telefono_inspector'];?></p>
                  <p class="text-muted"><?php echo "<b>Email</b>: ".$model[$key]['emal_inspector'];?></p>

                  <hr>

                  <strong><i class="fa fa-file-text-o margin-r-5"></i> Detalles </strong>
                  <p class="text-muted"><?php echo "<b>N° contrato</b>: ".$model[$key]['num_contrato'];?></p>
                  <p class="text-muted"><?php echo "<b>Tipo de contrato</b>: ".$model[$key]['tipo_contrato'];?></p>
                  <p class="text-muted"><?php echo "<b>Plan de la obra</b>: ".$model[$key]['plan_obra'];?></p>
                  <p class="text-muted"><?php echo "<b>Objeto del contrato</b>: ".$model[$key]['objeto_contrato'];?></p>
                  <p class="text-muted"><?php echo "<b>Monto del contrato</b>: ".number_format($model[$key]['monto_contrato'],2,",", ".")." Bs";?></p>
                  <p class="text-muted"><?php echo "<b>Monto de la construcción</b>: ".number_format($model[$key]['monto_construccion'],2,",", ".")." Bs";?></p>
                  <?php $total = ($model[$key]['monto_contrato'])- ($model[$key]['monto_construccion']); ?>
                  <p class="text-muted"><?php echo "<b> HP </b>: ".number_format($total,2,",", ".")." Bs";?></p>
                  <p class="text-muted"><?php echo "<b>Area total</b>: ".$model[$key]['total_area']." Mts<sup>&nbsp;2</sup>";?></p>
                  <p class="text-muted"><?php echo "<b>Cantidad de Viviendas</b>: ".$model[$key]['cant_viviendas'];?></p>

                </div><!-- /.box-body -->
              </div><!-- /.box -->
            <?php endforeach;?>


            </div>