<?php
/* @var $this ColorController */
/* @var $model Color */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cedula-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>




	<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { echo '<div class="alert alert-danger" style="text-align:center;"> <button type="button" class="close" data-dismiss="alert">×</button> ' . $message . "</div>\n"; } ?>




			<div class="box box-default box-solid">
                <div class="box-header with-border">
                <spam class="glyphicon glyphicon-cog"></spam>
                  <h3 class="box-title">SEGUIMIENTO...</h3>
                  <div class="box-tools pull-right">
                	<?php echo CHtml::link('<span class="glyphicon glyphicon-new-window"></span>','#',array('class'=>'search-button','data-toggle'=>"tooltip","title"=>"Busqueda Avanzada"));?>                 
					<button title="Refrescar" onclick="window.location.reload()" class="btn btn-box-tool" data-toggle="tooltip"><i class="glyphicon glyphicon-repeat"></i></button> 
                	<?php //echo CHtml::link('<span class="glyphicon glyphicon-share-alt"></span>',array('/'),array('data-toggle'=>"tooltip","title"=>"Inicio"));?>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->

                <div class="box-body">
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
                </div>



                  <div class="box-body table-responsive">

					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'cedula-grid',
						'dataProvider'=>$model->search(),
						'filter'=>$model,
						'itemsCssClass'=>'table table-striped table-bordered text-center',
				        'pagerCssClass'=>'pagination pull-right',
						'afterAjaxUpdate'=>'function(){$.getScript("themes/classic/plugins/archivos_js/campo-numero.js");$.getScript("themes/classic/plugins/archivos_js/campo-letra.js");
								$.getScript("themes/classic/plugins/archivos_js/script_select2.js");}',				        
				        'pager'=>array(
				            'htmlOptions'=>array('class'=>'pagination dataTables_info'),
				            'cssFile'=>'',
				            'hiddenPageCssClass'=>'disabled',
				            'selectedPageCssClass'=>'active',
				            'header'=>'',
				                ), 						
								'columns'=>array(
										array(
											'name'=>'rif',
											'htmlOptions'=>array('class'=>'col-xs-1 text-center text-default'), 
											'value'=>'$data->rif',
											'filter'=>CHtml::activeTextField($model,'rif',array('class'=>'campo-letra text-center form-control','placeholder'=>'- - - - -','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),	
										),							
										array(
											'name'=>'cedula',
											'htmlOptions'=>array('class'=>'col-xs-1 text-center text-default'), 
											'value'=>'$data->cedula',
											'filter'=>CHtml::activeTextField($model,'cedula',array('class'=>'campo-numero text-center form-control','placeholder'=>'- - - - -','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),	
										),									
										array(
											'name'=>'nombre_apellido',
											'htmlOptions'=>array('class'=>'col-xs-1 text-center text-default'), 
											'value'=>'$data->nombre_apellido',
											'filter'=>CHtml::activeTextField($model,'nombre_apellido',array('class'=>'campo-letra text-center form-control','placeholder'=>'- - - - -','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),	
										),	
										array(
											'name'=>'cedula_inspector',
											'htmlOptions'=>array('class'=>'col-xs-1 text-center text-default'), 
											'value'=>'$data->cedula_inspector',
											'filter'=>CHtml::activeTextField($model,'cedula_inspector',array('class'=>'campo-numero text-center form-control','placeholder'=>'- - - - -','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),	
										),

										array(
											'name'=>'inspector',
											'htmlOptions'=>array('class'=>'col-xs-1 text-center text-default'), 
											'value'=>'$data->inspector',
											'filter'=>CHtml::activeTextField($model,'inspector',array('class'=>'campo-letra text-center form-control','placeholder'=>'- - - - -','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),	
										),

										array(
											'name'=>'tipo', 
											'htmlOptions'=>array('class'=>'col-xs-2 text-center'),
											'value'=>'($data->tipo=="EMPRESA")?"EMPRESA":"GERENTE TECNICO"',
											'filter' =>CHtml::activeDropDownList($model,'tipo',array('EMPRESA' => 'EMPRESA', 'GERENTE TECNICO' =>'GERENTE TECNICO'),array('class'=>'form-control select2 text-center','empty'=>'SELECCIONE')),
										),

										array(
											'name'=>'nombre_obra',
											'htmlOptions'=>array('class'=>'col-xs-1 text-center text-default'), 
											'value'=>'$data->nombre_obra',
											'filter'=>CHtml::activeTextField($model,'nombre_obra',array('class'=>'campo-letra text-center form-control','placeholder'=>'- - - - -','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),	
										),
										array(		
											'name'=>'tipo_contrato',
											'htmlOptions'=>array('class'=>'col-xs-2 text-center'),
											'value'=>'$data->tipo_contrato',
											'filter' =>CHtml::activeDropDownList($model,'tipo_contrato',$tipo_contrato,array('class'=>'form-control select2 text-center','empty'=>'SELECCIONE')),
      									),
      									array(
											'name'=>'num_contrato',
											'htmlOptions'=>array('class'=>'col-xs-1 text-center text-default'), 
											'value'=>'$data->num_contrato',
											'filter'=>CHtml::activeTextField($model,'num_contrato',array('class'=>'text-center form-control','placeholder'=>'- - - - -','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),	
										),
									/*	array(
											'name'=>'objeto_contrato',
											'htmlOptions'=>array('class'=>'col-xs-2 text-center text-default'), 
											'value'=>'$data->objeto_contrato',
											'filter'=>CHtml::activeTextField($model,'objeto_contrato',array('class'=>'text-center form-control','placeholder'=>'- - - - -','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),	
										),*/
										
										array(
											'name'=>'plan_obra', 
											'htmlOptions'=>array('class'=>'col-xs-2 text-center'),
											'value'=>'$data->plan_obra',
											'filter' =>CHtml::activeDropDownList($model,'idplan_obra',$planObras,array('class'=>'form-control select2 text-center','empty'=>'SELECCIONE')),

										),
										
										
								

										array(
										'header'=>"Acciones",
										'htmlOptions'=>array('class'=>'col-md-3 text-center'),								
										'class'=>'CButtonColumn',
												//'deleteConfirmation'=>"Seguro que desea desactivar este usuario?",
												'deleteConfirmation'=>"js:'¿Seguro que desea Eliminar el registro \"'+$(this).parent().parent().children(':first-child').text()+'\"?'",				
												'template' => '{view}',
												'buttons' => array(  
															'view' => array(
																			'options'=>array('class'=>'fancybox', 'data-fancybox-type'=>'iframe','data-toggle'=>"tooltip"),
																			'label'=>'Visualizar',
																			'url'=>'Yii::app()->createUrl("usuario/GenPersona/ViewSico",array("id" => "$data->cedula"))'),
															),
											),
										),)); ?>

                  </div><!-- /.box-body -->
            </div><!-- /.box -->

