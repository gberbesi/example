<?php /* @var $this GenPersonaController *//* @var $model GenPersona *//* @var $form CActiveForm */ ?>


	<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { echo '<div class="alert alert-warning" style="text-align:center;"> <button type="button" class="close" data-dismiss="alert">×</button> ' . $message . "</div>\n"; } ?>

           <!-- general form elements -->
              <div class="box box-default box-solid" style="background-color: #F0F0F0 !important">
                <div class="box-header with-border">
                <spam class="glyphicon glyphicon-list-alt" aria-hidden="true"></spam>
                  <h3 class="box-title">Actualización de Datos</h3>
                </div><!-- /.box-header -->

			<?php $form=$this->beginWidget('CActiveForm', array('id'=>'gen-persona-form','enableAjaxValidation'=>false,'htmlOptions' => array('enctype' => 'multipart/form-data')));?>

				<p class="note text-center text-success"> Campos con <span class="required">*</span> son requeridos.</p>

				<?php //echo $form->errorSummary(array($model,$descripcion)); ?>

                <!-- form start -->
                <form role="form">
                        <div class="box box-default" style="background-color: #F0F0F0 !important">
                            <div class="box-header with-border">
                            <spam class="glyphicon glyphicon-user icon-green"></spam>
                              <h3 class="box-title">Datos Personales</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="form-group">
                                        <?php echo $form->labelEx($model,'nacionalidad'); ?>
                                        <?php echo $form->dropDownList($model,'nacionalidad',array('V' => 'VENEZOLANO', 'E' => 'EXTRAJERO'),array('disabled'=>(($model->nacionalidad=='' || $model->getErrors('nacionalidad'))?false:true),'class'=>'form-control campo-letra','empty'=>'','style'=>'width:100%')); ?>
                                        <?php echo $form->error($model,'nacionalidad',array('class'=>'btn-xs alert-danger text-center')); ?>
                                </div>

                                <div class="form-group">
                                        <?php echo $form->labelEx($model,'pais_id'); ?>
                                        <?php echo $form->dropDownList($model,'pais_id',$pais, array('class'=>'form-control campo-letra','empty'=>'','style'=>'width:100%')); ?>
                                        <?php echo $form->error($model,'pais_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                                </div>

                                <div class="form-group has-feedback">
                                        <?php echo $form->labelEx($model,'cedula'); ?>
                                        <?php echo $form->textField($model,'cedula',array('type'=>'text', 'class'=>'form-control campo-numero', 'placeholder'=>'Cedula','onkeyup'=>'javascript:this.value=this.value.toUpperCase();','readonly'=>(($model->cedula=='' || $model->getErrors('cedula'))?false:true))); ?>
                                        <?php echo $form->error($model,'cedula',array('class'=>'alert alert-danger text-center')); ?>
                                    <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback">
                                        <?php echo $form->labelEx($model,'nombre1'); ?>
                                        <?php echo $form->textField($model,'nombre1',array('type'=>'text', 'class'=>'form-control campo-letra', 'placeholder'=>'Primer Nombre','onkeyup'=>'javascript:this.value=this.value.toUpperCase();','readonly'=>(($model->nombre1=='' || $model->getErrors('nombre1'))?false:true))); ?>
                                        <?php echo $form->error($model,'nombre1',array('class'=>'alert alert-danger text-center')); ?>
                                    <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback">
                                        <?php echo $form->labelEx($model,'nombre2'); ?>
                                        <?php echo $form->textField($model,'nombre2',array('type'=>'text', 'class'=>'form-control campo-letra', 'placeholder'=>'Segundo Nombre','onkeyup'=>'javascript:this.value=this.value.toUpperCase();','readonly'=>(($model->nombre2=='' || $model->getErrors('nombre2'))?false:true))); ?>
                                        <?php echo $form->error($model,'nombre2',array('class'=>'alert alert-danger text-center')); ?>
                                    <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback">
                                        <?php echo $form->labelEx($model,'apellido1'); ?>
                                        <?php echo $form->textField($model,'apellido1',array('type'=>'text', 'class'=>'form-control campo-letra', 'placeholder'=>'Primer Apellido','onkeyup'=>'javascript:this.value=this.value.toUpperCase();','readonly'=>(($model->apellido1=='' || $model->getErrors('apellido1'))?false:true))); ?>
                                        <?php echo $form->error($model,'apellido1',array('class'=>'alert alert-danger text-center')); ?>
                                    <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback">
                                        <?php echo $form->labelEx($model,'apellido2'); ?>
                                        <?php echo $form->textField($model,'apellido2',array('type'=>'text', 'class'=>'form-control campo-letra', 'placeholder'=>'Segundo Apellido','onkeyup'=>'javascript:this.value=this.value.toUpperCase();','readonly'=>(($model->apellido2=='' || $model->getErrors('apellido2'))?false:true))); ?>
                                        <?php echo $form->error($model,'apellido2',array('class'=>'alert alert-danger text-center')); ?>
                                    <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback">
                                        <?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
                                        <?php echo $form->textField($model,'fecha_nacimiento',array('type'=>'text', 'class'=>'form-control', 'placeholder'=>'Día/Mes/Año','onKeyUp'=>'this.value=formateafecha(this.value)')); ?>
                                        <?php echo $form->error($model,'fecha_nacimiento',array('class'=>'alert alert-danger text-center')); ?>
                                    <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                                </div>

                                <div class="form-group">
                                        <?php echo $form->labelEx($model,'sexo'); ?>
                                        <?php echo $form->dropDownList($model,'sexo',array('F' => 'FEMENIMO', 'M' => 'MASCULINO'),array('class'=>'form-control select2 campo-letra','empty'=>'','style'=>'width:100%')); ?>
                                        <?php echo $form->error($model,'sexo',array('class'=>'alert alert-danger text-center')); ?>
                                </div>
                              
                            </div>
                        </div>

			<div class="box box-default" style="background-color: #F0F0F0 !important">
                            <div class="box-header with-border">
                            <spam class="fa fa-signal icon-green"></spam>
                              <h3 class="box-title">Datos Socio-Economicos</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">

                                <div class="form-group">
                                        <?php echo $form->labelEx($model,'estado_civil_id'); ?>
                                        <?php echo $form->dropDownList($model,'estado_civil_id',$estadocivil, array('class'=>'form-control campo-letra','empty'=>'','style'=>'width:100%')); ?>
                                        <?php echo $form->error($model,'estado_civil_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                                </div>

                                <div class="form-group">
                                        <?php echo $form->labelEx($model,'profesion_id'); ?>
                                        <?php echo $form->dropDownList($model,'profesion_id',$profesion, array('class'=>'form-control campo-letra','empty'=>'','style'=>'width:100%')); ?>
                                        <?php echo $form->error($model,'profesion_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                                </div>
                                
                                <div class="form-group has-feedback">
                                        <?php echo $form->labelEx($model,'ocupacion'); ?>
                                        <?php echo $form->textField($model,'ocupacion',array('type'=>'text', 'class'=>'form-control campo-letra', 'placeholder'=>'Ocupación','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>
                                        <?php echo $form->error($model,'ocupacion',array('class'=>'alert alert-danger text-center')); ?>
                                    <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
                                </div>
                                
                                <div class="form-group has-feedback">
                                        <?php echo $form->labelEx($model,'ingreso_mensual'); ?>
                                        <?php echo $form->textField($model,'ingreso_mensual',array('type'=>'number', 'class'=>'form-control campo-cantidad', 'placeholder'=>'Ejemplo: 120.000,00', 'maxlength'=>10, 'onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>
                                        <?php echo $form->error($model,'ingreso_mensual',array('class'=>'btn-xs alert-danger text-center')); ?>
                                <span class="fa fa-credit-card form-control-feedback"></span>
                                </div>                                
                                
                            </div>
			</div>                    
                    
			<div class="box box-default" style="background-color: #F0F0F0 !important">
                            <div class="box-header with-border">
                            <spam class="glyphicon glyphicon-picture icon-green"></spam>
                              <h3 class="box-title"><?php echo "Foto Personal";?></h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="form-group col-md-4">
                                        <?php echo $form->fileField($model,'imagen',array('id'=>'files')); ?>
                                        <?php echo $form->error($model,'imagen',array('class'=>'btn-xs alert-danger text-center')); ?>
                                </div>
                                <div class="form-group col-md-4">
                                        <output class="form-group text-center" id="list"></output>
                                </div>
                                <?php if(!$model->isNewRecord && ($model->imagen!="" || $model->imagen!=null)):?>
                                        <div class="form-group col-md-4">
                                          <img width="100%" src="<?php echo $imagen="http://".$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.Yii::app()->params['RutaUsuarios'].$model->imagen; ?>">
                                        </div>
                                <?php endif?>
                            </div>
			</div>
                    
                        <div class="box box-default" style="background-color: #F0F0F0 !important">
                            <div class="box-header with-border">
                            <spam class="fa fa-street-view icon-green"></spam>
                              <h3 class="box-title">Datos de Contacto</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="form-group has-feedback  <?php // echo ($model->hasErrors('telefono'))?'error':(($getPost)?'success':''); ?>">
                                        <?php echo $form->labelEx($model,'telefono1'); ?>
                                        <?php $this->widget('CMaskedTextField', array('model' => $model,
                                                                          'attribute' => "telefono1",
                                                                          'mask' => '(9999)-999-9999',
                                                                          'htmlOptions' => array('class'=>'form-control','placeholder'=>'Ejemplo: (0412)-999-9999')));?>
                                        <?php echo $form->error($model,'telefono1',array('class'=>'alert alert-danger text-center')); ?>
                                        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback  <?php // echo ($model->hasErrors('telefono'))?'error':(($getPost)?'success':''); ?>">
                                        <?php echo $form->labelEx($model,'telefono2'); ?>
                                        <?php $this->widget('CMaskedTextField', array('model' => $model,
                                                                          'attribute' => "telefono2",
                                                                          'mask' => '(9999)-999-9999',
                                                                          'htmlOptions' => array('class'=>'form-control','placeholder'=>'Ejemplo: (0412)-999-9999')));?>
                                        <?php echo $form->error($model,'telefono2',array('class'=>'alert alert-danger text-center')); ?>
                                        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback  <?php // echo ($model->hasErrors('telefono'))?'error':(($getPost)?'success':''); ?>">
                                        <?php echo $form->labelEx($model,'telefono3'); ?>
                                        <?php $this->widget('CMaskedTextField', array('model' => $model,
                                                                          'attribute' => "telefono3",
                                                                          'mask' => '(9999)-999-9999',
                                                                          'htmlOptions' => array('class'=>'form-control','placeholder'=>'Ejemplo: (0412)-999-9999')));?>
                                        <?php echo $form->error($model,'telefono3',array('class'=>'alert alert-danger text-center')); ?>
                                        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback">
                                        <?php echo $form->labelEx($model,'correo'); ?>
                                        <?php echo $form->textField($model,'correo',array('type'=>'text', 'class'=>'form-control', 'placeholder'=>'Text','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>
                                        <?php echo $form->error($model,'correo',array('class'=>'alert alert-danger text-center')); ?>
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                            </div>
                        </div>


                        <div class="box box-default" style="background-color: #F0F0F0 !important">
                            <div class="box-header with-border">
                            <spam class="glyphicon glyphicon-map-marker icon-green"></spam>
                              <h3 class="box-title">Datos de Ubicación</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                <div class="form-group has-feedback">
                                                <?php echo $form->labelEx($modeldireccion,'piso'); ?>
                                                <?php echo $form->textField($modeldireccion,'piso',array('type'=>'text', 'class'=>'form-control', 'placeholder'=>'Piso / Referencia','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>
                                                <?php echo $form->error($modeldireccion,'piso',array('class'=>'btn-xs alert-danger text-center')); ?>
                                <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback">
                                                <?php echo $form->labelEx($modeldireccion,'numero'); ?>
                                                <?php echo $form->textField($modeldireccion,'numero',array('type'=>'text', 'class'=>'form-control', 'placeholder'=>'Numero','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>
                                                <?php echo $form->error($modeldireccion,'numero',array('class'=>'btn-xs alert-danger text-center')); ?>
                                <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
                                </div>
                                
                                <div class="form-group has-feedback">
                                                <?php echo $form->labelEx($modeldireccion,'sector'); ?>
                                                <?php echo $form->textField($modeldireccion,'sector',array('type'=>'text','class'=>'form-control campo-letra', 'placeholder'=>'Sector','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>
                                                <?php echo $form->error($modeldireccion,'sector',array('class'=>'btn-xs alert-danger text-center')); ?>
                                <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback">
                                                <?php echo $form->labelEx($modeldireccion,'calle_avenida'); ?>
                                                <?php echo $form->textField($modeldireccion,'calle_avenida',array('type'=>'text', 'class'=>'form-control campo-letra', 'placeholder'=>'Calle o avenida','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>
                                                <?php echo $form->error($modeldireccion,'calle_avenida',array('class'=>'btn-xs alert-danger text-center')); ?>
                                <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback">
                                                <?php echo $form->labelEx($modeldireccion,'estado_id'); ?>
                                                <?php echo $form->dropDownList($modeldireccion,'estado_id',$estado,
                                                                                array('ajax'=>array('type'=>'POST',
                                                                                                'url'=>CController::createUrl('/tablas_referencias/Direccion/Municipio'), //url to call.
                                                                                                'update'=>'#Direccion_municipio_id', //selector to update
                                                                                                ),'empty' =>'','class'=>'form-control','style'=>'width:100%'));?>  
                                                <?php echo $form->error($modeldireccion,'estado_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                                <span class="form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback">
                                                <?php echo $form->labelEx($modeldireccion,'municipio_id'); ?>
                                                <?php if($modeldireccion->estado_id=='')$modeldireccion->estado_id=NULL; 
                                                                echo $form->dropDownList($modeldireccion,'municipio_id',
                                                                                            CHtml::listData(Municipio::model()->findAll('estado_id=:estado_id',array(':estado_id'=>$modeldireccion->estado_id)),'municipio_id', 'nombre'),
                                                                                            array('ajax'=>array('type'=>'POST',
                                                                                                            'url'=>CController::createUrl('/tablas_referencias/Direccion/parroquia'), //url to call.
                                                                                                            'update'=>'#Direccion_parroquia_id', //selector to update
                                                                                                            ), 'empty' =>'','class'=>'form-control','style'=>'width:100%'));?>  
                                                <?php echo $form->error($modeldireccion,'municipio_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                                <span class="form-control-feedback"></span>
                                </div>

                                <div class="form-group has-feedback">
                                                <?php echo $form->labelEx($modeldireccion,'parroquia_id'); ?>
                                                <?php if ($modeldireccion->municipio_id=='')$modeldireccion->municipio_id=NULL;
                                                                echo $form->dropDownList($modeldireccion,'parroquia_id',
                                                                                            CHtml::listData(Parroquia::model()->findAll('municipio_id=:municipio_id',array(':municipio_id'=>$modeldireccion->municipio_id)),'parroquia_id', 'nombre'),
                                                                                            array('empty' =>'','class'=>'form-control','style'=>'width:100%'));?>  
                                                <?php echo $form->error($modeldireccion,'parroquia_id',array('class'=>'btn-xs alert-danger text-center')); ?>
                                <span class="form-control-feedback"></span>
                                </div>

                            </div>
			</div>


				<div class="box-footer text-center" style="background-color: #F0F0F0 !important">
					<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>'btn btn-success')); ?>
				</div>

				<?php $this->endWidget(); ?>

                </form>
              </div><!-- /.box -->
              
    <!-- mostrar imagen antes de subir-->
    <script src="themes/classic/plugins/archivos_js/mostar_imagen.js"></script>