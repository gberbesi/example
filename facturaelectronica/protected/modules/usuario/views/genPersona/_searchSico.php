<?php
/* @var $this GenPersonaController */
/* @var $model GenPersona */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'cedula'); ?>
		<?php echo $form->textField($model,'cedula'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('_search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->