<?php
/* @var $this GenPersonaController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Gen Personas',
);

$this->menu=array(
	array('label'=>'Create GenPersona', 'url'=>array('create')),
	array('label'=>'Manage GenPersona', 'url'=>array('admin')),
);
?>

<h1>Gen Personas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
