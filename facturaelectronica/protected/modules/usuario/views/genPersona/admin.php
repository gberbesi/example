
		<?php foreach(Yii::app()->user->getFlashes() as $key => $message) {
		        echo '<div class="alert alert-success" style="text-align:center;">
		        <button type="button" class="close" data-dismiss="alert">×</button>
		        ' . $message . "</div>\n";}
		?>


			<div class="box box-default box-solid">
                <div class="box-header with-border">
                <spam class="glyphicon glyphicon-cog"></spam>
                  <h3 class="box-title">Gestor de Personas</h3>
                  <div class="box-tools pull-right">
                    <?php echo CHtml::link('<span class="glyphicon glyphicon-plus"></span>',array('/usuario/GenPersona/Create'),array('data-toggle'=>"tooltip","title"=>"Registrar persona.",'class'=>'fancybox', 'data-fancybox-type'=>'iframe'));?>
					<button title="Refrescar" onclick="window.location.reload()" class="btn btn-box-tool" data-toggle="tooltip"><i class="glyphicon glyphicon-repeat"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->

                  <div class="box-body table-responsive">

                <?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'usuario-grid',
                        'dataProvider'=>$model->search(),
                        'filter'=>$model,
                        'itemsCssClass'=>'table table-striped table-bordered text-center',
                        'pagerCssClass'=>'pagination pull-right',
                        'afterAjaxUpdate'=>'function(){$.getScript("themes/classic/plugins/archivos_js/campo-numero.js");$.getScript("themes/classic/plugins/archivos_js/campo-letra.js");
                                $.getScript("themes/classic/plugins/archivos_js/script_select2.js");}',
                        'pager'=>array(
                            'htmlOptions'=>array('class'=>'pagination pagination-sm no-margin'),
                            'cssFile'=>'',
                            'hiddenPageCssClass'=>'disabled',
                            'selectedPageCssClass'=>'active',
                            'header'=>''),
                            'ajaxUrl'=>Yii::app()->createUrl("/usuario/GenPersona/admin"),
                                    'columns'=>array(
	                                            array(
	                                                    'header'=>'Cédula',
	                                                    'name'=>'cedula',
														'htmlOptions'=>array('class'=>'col-xs-1 text-center'),
	                                                    'value'=>'$data->cedula',
	                                                    'filter'=>CHtml::activeTextField($model,'cedula',array('class'=>'form-control campo-numero text-center','placeholder'=>'CEDULA')),
	                                                    ), 
	                                            array(
	                                                    'name'=>'apellido1',
	                                                    'htmlOptions'=>array('class'=>'col-md-2 text-center'),
	                                                    'value'=>'$data->apellido1',
	                                                    'filter'=>CHtml::activeTextField($model,'apellido1',array('class'=>'form-control campo-letra text-center','placeholder'=>'APELLIDO','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
	                                                    ),
	                                            array(
	                                                    'name'=>'nombre1',
	                                                    'htmlOptions'=>array('class'=>'col-md-2 text-center'),
	                                                    'value'=>'$data->nombre1',
	                                                    'filter'=>CHtml::activeTextField($model,'nombre1',array('class'=>'form-control campo-letra text-center','placeholder'=>'NOMBRE','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
	                                                    ),
												array(
														'name'=>'carga_finalizada',
														'htmlOptions'=>array('class'=>'col-xs-1 text-center'),
														'value'=>'($data->carga_finalizada==1)?"SI":"NO"',
														'filter' =>CHtml::activeDropDownList($model,'carga_finalizada',array('1' => 'SI', '0' =>'NO'),array('class'=>'form-control select2 text-center','empty'=>'SELECCIONE')),
														),
												array(
														'name'=>'carpeta_fisico',
														'htmlOptions'=>array('class'=>'col-xs-1 text-center'),
														'value'=>'($data->carpeta_fisico==1)?"SI":"NO"',
														'filter' =>CHtml::activeDropDownList($model,'carpeta_fisico',array('1' => 'SI', '0' =>'NO'),array('class'=>'form-control select2 text-center','empty'=>'SELECCIONE')),
														),	
                                                array(      
                                                    'name'=>'institucion_id',
                                                    'htmlOptions'=>array('class'=>'col-xs-1 text-center'),
                                                    'value'=>'$data->insti->nb_institucion',
                                                    'filter' =>CHtml::activeDropDownList($model,'institucion_id',$insti,array('class'=>'form-control select2','empty'=>'SELECCIONE')),
                                                    ),
                                                    array(
                                                            'header'=>"Acción",
                                                            'htmlOptions'=>array('class'=>'col-md-2 text-center'),
                                                            'class'=>'CButtonColumn',
                                                            'template' => '{finalizado}{no_finalizado}{view}{update}',
                                                            'buttons'=>array(
                                                                            'finalizado' => array(
                                                                                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/iconos/icono_47.png',
                                                                                    'options'=>array('data-toggle'=>"tooltip"),
                                                                                    'visible'=>'$data->carpeta_fisico == "true" && Yii::app()->user->checkAccess("usuario/GenPersona/Carpetafisica") && $data->carga_finalizada == "true"',
                                                                                    'label'=>'Consignó carpeta',
                                                                                    'click'=>'function(){return confirm("¿Desea revertir el proceso de consignación de la carpeta?");}',
                                                                                    'url'=>'Yii::app()->createUrl("usuario/GenPersona/carpetafisica",array("id" => "$data->persona_id"))',
                                                                                    ),

                                                                    'no_finalizado' => array(
                                                                                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/iconos/icono_46.png',
                                                                                    'options'=>array('data-toggle'=>"tooltip"),
                                                                                    'visible'=>'$data->carpeta_fisico != "false" && Yii::app()->user->checkAccess("usuario/GenPersona/Carpetafisica") && $data->carga_finalizada == "true"',
                                                                                    'label'=>'Marca como carpeta consignada',
                                                                                    'click'=>'function(){return confirm("¿Desea consignar la carpeta del usuario?");}',
                                                                                    'url'=>'Yii::app()->createUrl("usuario/GenPersona/carpetafisica",array("id" => "$data->persona_id"))',
                                                                                    ),

                                                                    'view' => array(
                                                                                    'label'=>'Ver',
                                                                                    'visible'=>'Yii::app()->user->checkAccess("usuario/User/View")',
                                                                                    'options'=>array('class'=>'fancybox', 'data-fancybox-type'=>'iframe','data-toggle'=>"tooltip"),
                                                                                    ),
           
                                                                    'update' => array(
                                                                                    'label'=>'Modificar Datos para el sistema',
                                                                                    'visible'=>'Yii::app()->user->checkAccess("usuario/User/Update")',
                                                                                    'options'=>array('class'=>'fancybox', 'data-fancybox-type'=>'iframe','data-toggle'=>"tooltip"),
                                                                                    ),

                                              )),),)); ?>

                  </div><!-- /.box-body -->
            </div><!-- /.box -->
