

    <?php foreach(Yii::app()->user->getFlashes() as $key => $message) { echo '<div class="alert alert-success" style="text-align:center;"> <button type="button" class="close" data-dismiss="alert">×</button> ' . $message . "</div>\n"; } ?>

    <div class="content" style="max-width:400px;">

    <?php $form=$this->beginWidget('CActiveForm', array('id'=>'user-usuarios-form','clientOptions'=>array(),)); ?>
      
      <div class="login-logo">
        <a style="color:#000" href="<?php echo Yii::app()->createUrl('/'); ?>" ><b style="color:#000">Venta de Frutas</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body" style="background-color: #f3f4f5 ;background: rgba(0, 0, 0, 0) radial-gradient(circle farthest-corner at 50% 0 , #f3f4f5  0px, #f3f4f5  180%) repeat scroll 0 0;box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);
                                        border: 1px solid rgba(34, 36, 38, 0.15); border-radius: 0.285714rem">
          <p class="login-box-msg" style="color:#6e6d6d">CAMBIAR CLAVE</p>
          <form method="post">

            <div class="form-group has-feedback  <?php echo ($model->hasErrors('clave'))?'error':(($getPost)?'success':''); ?>" style="color:#6e6d6d">
              <?php echo $form->labelEx($model,'clave'); ?>
              <?php echo $form->passwordField($model,'clave',array('type'=>'password', 'class'=>'form-control', 'placeholder'=>'Antigua clave')); ?>
              <?php echo $form->error($model,'clave'); ?>
              <span class="glyphicon glyphicon-edit form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback  <?php echo ($model->hasErrors('nueva'))?'error':(($getPost)?'success':''); ?>" style="color:#6e6d6d">
              <?php echo $form->labelEx($model,'nueva'); ?>
              <?php echo $form->passwordField($model,'nueva',array('type'=>'password', 'class'=>'form-control', 'placeholder'=>'Nueva clave')); ?>
              <?php echo $form->error($model,'nueva'); ?>
              <span class="glyphicon glyphicon-edit form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback  <?php echo ($model->hasErrors('repetirnueva'))?'error':(($getPost)?'success':''); ?>" style="color:#6e6d6d">
              <?php echo $form->labelEx($model,'repetirnueva'); ?>
              <?php echo $form->passwordField($model,'repetirnueva',array('type'=>"password", 'class'=>'form-control', 'placeholder'=>'Repetir clave')); ?>
              <?php echo $form->error($model,'repetirnueva'); ?>
              <span class="glyphicon glyphicon-edit form-control-feedback"></span>
            </div>

            <div class="row">
              <div class="col-xs-12">
                  <?php echo CHtml::submitButton('Guardar' ,array('class'=>'btn btn-default btn-block btn-flat')); ?>
                  <?php echo CHtml::resetButton('Limpiar' ,array('class'=>'btn btn-danger btn-block btn-flat')); ?>
              </div><!-- /.col -->
            </div>
          </form>

        </div><!-- /.login-box-body -->
      <?php $this->endWidget(); ?>
    </div><!-- /.login-box -->







