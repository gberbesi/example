

    <?php foreach(Yii::app()->user->getFlashes() as $key => $message) { echo '<div class="alert alert-success" style="text-align:center;"> <button type="button" class="close" data-dismiss="alert">×</button> ' . $message . "</div>\n"; } ?>

    <div class="content" style="max-width:400px;">

    <?php $form=$this->beginWidget('CActiveForm', array('id'=>'recuperar-form','clientOptions'=>array(),)); ?>
      
      <div class="login-logo">
        <a style="color:#000" href="<?php echo Yii::app()->createUrl('/'); ?>" ><b style="color:#000"> <h2>Sistema de Gestión de Documentos Pre-Asignación Colina de Río Tuy</h2></b></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body" style="background-color: #f3f4f5 ;background: rgba(0, 0, 0, 0) radial-gradient(circle farthest-corner at 50% 0 , #f3f4f5  0px, #f3f4f5  180%) repeat scroll 0 0;box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);
                                        border: 1px solid rgba(34, 36, 38, 0.15); border-radius: 0.285714rem">          <p class="login-box-msg" style="color:#6e6d6d">RECUPERAR CLAVE</p>
          <form method="post">
            <div class="form-group has-feedback" style="color:#6e6d6d">
              <?php echo $form->labelEx($model,'usuario'); ?>
              <?php echo $form->textField($model,'usuario',array('type'=>'number', 'class'=>'form-control', 'placeholder'=>'Usuario')); ?>
              <?php echo $form->error($model,'usuario'); ?>
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <?php if(CCaptcha::checkRequirements()): ?>
            <div class="form-group has-feedback" style="color:#6e6d6d">
              <?php echo $form->labelEx($Captcha,'verifyCode'); ?>
              <?php echo $form->textField($Captcha,'verifyCode',array('type'=>'number', 'autocomplete'=>'off', 'class'=>'form-control', 'placeholder'=>'Código de Verificación')); ?>
              <?php echo $form->error($Captcha,'verifyCode'); ?>
              <span class="glyphicon glyphicon-edit form-control-feedback"></span>
              <div  style="text-align:center">
                <br>
                <?php $this->widget('CCaptcha',array('clickableImage'=>true,'showRefreshButton'=>false)); ?>
              </div>  
            </div>
            <?php endif; ?>

            <div class="row">
              <div class="col-xs-12">
                  <?php echo CHtml::submitButton('Enviar' ,array('class'=>'btn btn-default btn-block btn-flat')); ?>
                  <?php echo CHtml::button('Cancelar' ,array('submit' => array('/usuario/Usuario/login'),'class'=>'btn btn-danger btn-block btn-flat')); ?>
              </div><!-- /.col -->
            </div>
          </form>

        </div><!-- /.login-box-body -->
      <?php $this->endWidget(); ?>
    </div><!-- /.login-box -->






