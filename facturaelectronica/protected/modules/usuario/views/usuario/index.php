<!DOCTYPE html>
<html lang="en">
   <body class="nav-md">
    <div class="container body">
      
         
            
    <ul class="nav nav-tabs col-md-offset-3" >
    <li  class="col col-md-3 col-xs-4 active"><a data-toggle="tab" href="#dia" style="color:#22C7FC; font-size: 17px;" ><strong>Diario</strong></a></li>
    <li class="col col-md-3 col-xs-4"><a data-toggle="tab" href="#semana" style="color: #22C7FC; font-size: 17px;"><strong>Semanal</strong> </a></li>
    <li class="col col-md-3 col-xs-4"><a data-toggle="tab" href="#mes" style="color: #22C7FC; font-size: 17px;"><strong>Mensual</strong></a></li>
    </ul>

              <div class="dashboard_graph">
                 <div class="tab-content">
    <div id="dia" class="tab-pane fade in active ">
          <div class="row tile_count"   >
            <h3 align="center" style="font-weight:bold">Cifras del Día Actual</h3><br>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count"  >
              <span class="count_top" align="center"><i class="fa fa-user"></i> Facturas</span>
              <div class="count" style="font-size: 22px"><?php echo $facturasdeldia;?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Ventas en Colones</span>
              <div class="count" style="font-size: 22px"><?php  {
                # code...
              } echo $ventasdiac."₡";?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Ventas en Dolares</span>
              <div class="count" style="font-size: 22px"><?php echo $ventasdiad."$";?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Ventas a Credito</span>
              <div class="count" style="font-size: 22px"><?php echo $creditodia;?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Ventas a Contado</span>
              <div class="count" style="font-size: 22px"><?php echo $contadodia;?></div>
            </div>
          </div>
        </div>
        
          <div id="semana" class="tab-pane fade  ">
                   <div class="row tile_count">
            <h3 align="center" style="font-weight:bold">Cifras de la Semana Actual</h3><br>
            <div class="col-md-2  col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Facturas </span>
              <div class="count" style="font-size: 22px"><?php echo $facturassemana;?></div>
            </div>

             <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Ventas en Colones</span>
              <div class="count" style="font-size: 22px"><?php echo $ventassemanac ."₡";?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Ventas en Dolares</span>
              <div class="count" style="font-size: 22px"><?php echo $ventassemanad ."$";?></div>
            </div>

             <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Ventas a Credito</span>
               <div class="count" style="font-size: 22px"><?php echo $creditosemana;?></div>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Ventas a Contado</span>
               <div class="count" style="font-size: 22px"><?php echo $contadosemana;?></div>
            </div>
           

          </div>
        </div>
          <div id="mes" class="tab-pane fade  ">

                            <div class="row tile_count">
            <h3 align="center" style="font-weight:bold">Cifras del Mes Actual</h3><br>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Facturas</span>
              <div class="count" style="font-size: 22px"><?php echo $facturasmes;?></div>
            </div>

             <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Ventas en Colones</span>
              <div class="count" style="font-size: 22px"><?php echo $ventasmesc ."₡";?></div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Ventas en Dolares</span>
              <div class="count" style="font-size: 22px"><?php echo $ventasmesd ."$";?></div>
            </div>

             <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Ventas a Credito</span>
               <div class="count" style="font-size: 22px"><?php echo $creditomes;?></div>
            </div>

            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top" align="center"><i class="fa fa-user"></i> Ventas a Contado</span>
               <div class="count" style="font-size: 22px"><?php echo $contadomes;?></div>
            </div>
           
 
          </div>
        </div>
                     </div> 
                      

        
               
                  <div class="col-md-12 col-xs-12" ><br>
                    <h3 align="center" style="font-weight: bold">Estadiscas generales de la semana actual</h3><br></div>
                 <div style="margin-left: -10px;">
                  
                    <div id="containerG1" class="col-md-6 col-xs-12 " style="min-width: 320px"></div>
 

                    <div id="containerG2" class="col-md-6 col-xs-12" style="min-width: 340px"></div>
          </div>
                  <div class="col-md-12 col-xs-12"><br>
                    <h3 align="center" style="font-weight: bold">Estadiscas generales de los meses</h3><br><br></div>
                 

                  <div id="containerG3"  class="col-md-6 col-xs-12" style="min-width: 320px"></div><br>
                    
             
                
                  <div id="containerG4" class="col-md-6 col-xs-12" style="min-width: 320px">
                    <table class="table table-responsive">
<thead>
<tr>
<th> Mes</th>
<th>Pagos<span style="color:white">----</span></th>&nbsp;&nbsp;<th>Pendientes</th>
</tr>
</thead>
<tbody>
<tr>
<th >Enero</th>
<td>5</td>
<td>5</td>
</tr>
<tr>
<th>Febrero</th>
<td>5</td>
<td>5</td>
</tr>
<tr>
<th>Marzo</th>
<td>5</td>
<td>5</td>
</tr>
<tr>
<th>Abril</th>
<td>5</td>
<td>5</td>
</tr>
<tr>
<th>Mayo</th>
<td>5</td>
<td>5</td>
</tr>
<tr>
<th>Junio</th>
<td>5</td>
<td>5</td>
</tr>
<tr>
<th>Julio</th>
<td>5</td>
<td>5</td>
</tr>
<tr>
<th>Agosto</th>
<td>5</td>
<td>5</td>
</tr>
<tr>
<th>Septiembre</th>
<td>5</td>
<td>5</td>
</tr>
<tr>
<th>Octubre</th>
<td>5</td>
<td>5</td>
</tr>
<tr>
<th>Nobiembre</th>
<td>5</td>
<td>5</td>
</tr>
<tr>
<th>Diciembre</th>
<td>5</td>
<td>5</td>
</tr>
</tbody>
</table>
             
                  </div>
                
   
 </div>
                      

             
        

          </div>
     




  
  </body>
</html>

<script src="themes/gentelella/vendors/jquery/dist/jquery.min.js"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/ve/ve-all.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>



<script>
  
var chart = new Highcharts.Chart({
    chart: {
        renderTo: 'containerG1',
        type: 'column',
        options3d: {
            enabled: true,
            alpha: 15,
            beta: 15,
            depth: 50,
            viewDistance: 25
        }
    },
    title: {
        text: 'ventas '
    },
        xAxis: {
        categories: ['Semana1','Semana2','Semana3','Semana4']
    },
    subtitle: {
        text: 'Colones  ₡'
    },
    plotOptions: {
        column: {
            depth: 25
        }
    },
    series: [{
      name: 'semana1',
         data: [
            ['Semana1', 8 ],
            ['Semana2', 3],
            ['Semana3', 1],
            ['Semana4', 6],
          
           
        ]
    }]
});

  Highcharts.chart('containerG2', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45
        }
    },
    title: {
        text: 'Ventas'
    },
    subtitle: {
        text: 'Dólares $'
    },
    plotOptions: {
        pie: {
            innerSize: 100,
            depth: 45
        }
    },
    series: [{
        name: 'Delivered amount',
        data: [
            ['Semana1', 8 ],
            ['Semana2', 3],
            ['Semana3', 1],
            ['Semana4', 6],

        ]
    }]
});


Highcharts.chart('containerG3', {

    title: {
        text: 'Ejemplo de gráfica anual'
    },

    xAxis: {
       categories: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Nobiembre','Diciembre']
    },

    yAxis: {
        type: 'logarithmic',
        minorTickInterval: 0.1
    },

    tooltip: {
        headerFormat: '<b>{series.name}</b><br />',
        pointFormat: 'x = {point.x}, y = {point.y}'
    },

    series: [{
        data: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12],
      
    }]
});


Highcharts.chart('containerG4', {
data: {
table: 'containerG4'
},
chart: {
type: 'column'
},
title: {
text: 'Ejemplo de gráfica anual'
},
yAxis: {
allowDecimals: false,
title: {
text: 'Recibos'
}
},
tooltip: {
formatter: function () {
return '<b>' + this.series.name + '</b><br/>' +
this.point.y + ' ' + this.point.name.toLowerCase();
}
}
});
</script>





