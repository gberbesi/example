<?php
/* @var $this InstitucionController */
/* @var $data Institucion */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_institucion')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_institucion), array('view', 'id'=>$data->id_institucion)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nb_institucion')); ?>:</b>
	<?php echo CHtml::encode($data->nb_institucion); ?>
	<br />


</div>