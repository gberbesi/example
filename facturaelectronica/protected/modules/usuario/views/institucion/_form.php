<?php /* @var $this AutomovilController */ /* @var $model Automovil */ /* @var $form CActiveForm */ ?>


	<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { echo '<div class="alert alert-warning" style="text-align:center;"> <button type="button" class="close" data-dismiss="alert">×</button> ' . $message . "</div>\n"; } ?>

           <!-- general form elements -->
              <div class="box box-primary box-solid">
                <div class="box-header with-border">
                <spam class="glyphicon glyphicon-edit" aria-hidden="true"></spam>
                  <h3 class="box-title">Institución</h3>
                </div><!-- /.box-header -->

				<?php $form=$this->beginWidget('CActiveForm', array('id'=>'automovil-form','enableAjaxValidation'=>false,'htmlOptions' => array('enctype' => 'multipart/form-data')));?>

				<p class="note text-center text-success"> Campos con <span class="required">*</span> son requeridos.</p>

				<?php //echo $form->errorSummary(array($model,$descripcion)); ?>

                <!-- form start -->
                <form role="form">
				
                	<div class="box-body">

						<div class="form-group has-feedback">
								<?php echo $form->labelEx($model,'nb_institucion'); ?>
								<?php echo $form->textField($model,'nb_institucion',array('type'=>'text','class'=>'form-control', 'placeholder'=>'PRUEBA','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')); ?>
								<?php echo $form->error($model,'nb_institucion',array('class'=>'btn-xs alert-danger text-center')); ?>
					        <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
						</div>
					
					</div>


				<div class="box-footer text-center" style="background-color: #F0F0F0 !important">
					<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>'btn btn-success')); ?>
				</div>

				<?php $this->endWidget(); ?>

                </form>
              </div><!-- /.box -->
