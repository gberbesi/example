		<?php foreach(Yii::app()->user->getFlashes() as $key => $message) {
		        echo '<div class="alert alert-success" style="text-align:center;">
		        <button type="button" class="close" data-dismiss="alert">×</button>
		        ' . $message . "</div>\n";}
		?>


			<div >
                <div class="box-header with-border">
                
                  <h3 class="box-title">&nbsp;<i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;Usuarios</a></h3>
                  <div class="box-tools pull-right">
					<button title="Refrescar" onclick="window.location.reload()" class="btn btn-box-tool" data-toggle="tooltip"><i class="glyphicon glyphicon-repeat"></i></button>
                  </div><!-- /.box-tools -->
                  <div class="col-md-6 col-sm-2 col-xs-12 col-md-offset-3">
                                <?php
                               
                                    echo CHtml::link('<span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo Usuario',array('user/create'),array('id'=>'bt1','class'=>"btn btn-primary btn-block"));
                                ?>
                             </div>
                </div><!-- /.box-header -->
							</br>
								

                  <div class="table table-striped table-bordered table-condensed" cellspacing="0" cellpadding="3" rules="all" style="background-color:White;border-color:#CCCCCC;border-width:1px;border-style:None;border-collapse:collapse;">


                <?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'usuario-grid',
                        'dataProvider'=>$model->search(),
                        'filter'=>$model,
                        'itemsCssClass'=>'table table-striped table-bordered text-center',
                        'pagerCssClass'=>'pagination pull-right',
                        'afterAjaxUpdate'=>'function(){$.getScript("themes/classic/plugins/archivos_js/campo-numero.js");$.getScript("themes/classic/plugins/archivos_js/campo-letra.js");
                                $.getScript("themes/classic/plugins/archivos_js/script_select2.js");}',
                        'pager'=>array(
                            'htmlOptions'=>array('class'=>'pagination pagination-sm no-margin'),
                            'cssFile'=>'',
                            'hiddenPageCssClass'=>'disabled',
                            'selectedPageCssClass'=>'active',
                            'header'=>''),
                            'ajaxUrl'=>Yii::app()->createUrl("/usuario/user/admin"),
                                    'columns'=>array(
                                           
                                            array(
                                                'header'=>"<a>USUARIO</a>",
                                                'name'=>'usuario',
                                                'htmlOptions'=>array('class'=>'col-md-2 text-center'),
                                                'value'=>'$data->usuario',
                                                'filter'=>CHtml::activeTextField($model,'usuario',array('class'=>'form-control campo-numero text-center','placeholder'=>'USUARIO')),
                                                ),
                                           
                                            array(
                                                    'header'=>"<a>NOMBRE</a>",
                                                    'name'=>'nombre1',
                                                    'htmlOptions'=>array('class'=>'col-md-2 text-center'),
                                                    'value'=>'$data->persona->nombre1',
                                                    'filter'=>CHtml::activeTextField($model,'persona_id',array('class'=>'form-control campo-letra text-center','placeholder'=>'NOMBRE','onkeyup'=>'javascript:this.value=this.value.toUpperCase();')),
                                                    ),
                                           
                                              array(
                                                    'header'=>"<a>PERFIL</a>",
                                                    'name'=>'usuario_activo_sistema',
                                                    'htmlOptions'=>array('class'=>'col-xs-1 text-center'),
                                                    'value'=>'($data->usuario_activo_sistema==1)?"ACTIVO":"NO ACTIVO"',
                                                    'filter' =>CHtml::activeDropDownList($model,'usuario_activo_sistema',array('1' => 'ACTIVO', '0' =>'NO ACTIVO'),array('class'=>'form-control select2 text-center','empty'=>'SELECCIONE')),
                                                    ),
                                                   
                                            array(
                                                    'header'=>"Acción",
                                                    'htmlOptions'=>array('class'=>'col-md-2 text-center'),
                                                    'class'=>'CButtonColumn',
                                                    'template' => '{update}{delete}',
                                                    'buttons'=>array(
                                                              /*      'finalizado' => array(
                                                                                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/iconos/icono_47.png',
                                                                                    'options'=>array('data-toggle'=>"tooltip"),
                                                                                    'visible'=>'$data->persona->carpeta_fisico == "true" && Yii::app()->user->checkAccess("usuario/User/Carpetafisica")',
                                                                                    'label'=>'Consignó carpeta',
                                                                                    'click'=>'function(){return confirm("¿Desea revertir el proceso de consignación de la carpeta?");}',
                                                                                    'url'=>'Yii::app()->createUrl("usuario/User/carpetafisica",array("id" => "$data->usuario_id"))',
                                                                                    ),

                                                                    'no_finalizado' => array(
                                                                                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/iconos/icono_46.png',
                                                                                    'options'=>array('data-toggle'=>"tooltip"),
                                                                                    'visible'=>'$data->persona->carpeta_fisico != "false" && Yii::app()->user->checkAccess("usuario/User/Carpetafisica")',
                                                                                    'label'=>'Marca como carpeta consignada',
                                                                                    'click'=>'function(){return confirm("¿Desea consignar la carpeta del usuario?");}',
                                                                                    'url'=>'Yii::app()->createUrl("usuario/User/carpetafisica",array("id" => "$data->usuario_id"))',
                                                                                    ),*/

                                                                    'view' => array(
                                                                                    'label'=>'Ver',
                                                                                    'visible'=>'Yii::app()->user->checkAccess("root")',
                                                                                    'options'=>array('class'=>'fancybox', 'data-fancybox-type'=>'iframe','data-toggle'=>"tooltip"),
                                                                                    ),
           
                                                                    'update' => array(
                                                                                    'label'=>'Modificar Datos para el sistema',
                                                                                    'visible'=>'Yii::app()->user->checkAccess("root")',
                                                                                    'options'=>array('data-toggle'=>"tooltip"),
                                                                                    ),

                                                                    'update_data' => array(
                                                                                    'options'=>array('class'=>'fancybox','data-fancybox-type'=>'iframe','data-toggle'=>"tooltip"),
                                                                                    'visible'=>'Yii::app()->user->checkAccess("root")',
                                                                                    'label'=>'Actualizar Ficha del Usuario',
                                                                                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/iconos/icono_10.png',
                                                                                    'url'=>'Yii::app()->createUrl("usuario/GenPersona/update",array("id" => "$data->persona_id"))',
                                                                                    ),
                                                                    'estatus_act' => array(
                                                                                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/iconos/icono_18.png',
                                                                                    'options'=>array('data-toggle'=>"tooltip"),
                                                                                    'visible'=>'Yii::app()->user->checkAccess("root")',
                                                                                    'label'=>'Activar',
                                                                                    'click'=>'function(){return confirm("¿Desea Activar este usuario?");}',
                                                                                    'url'=>'Yii::app()->createUrl("usuario/User/estatus",array("id" => "$data->usuario_id"))',
                                                                                    ),

                                                                    'estatus_desac' => array(
                                                                                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/iconos/icono_26.png',
                                                                                    'options'=>array('data-toggle'=>"tooltip"),
                                                                                    'visible'=>'$data->usuario_activo_sistema == "true" && Yii::app()->user->checkAccess("root")',
                                                                                    'label'=>'Desactivar',
                                                                                    'click'=>'function(){return confirm("¿Desea Desactivar este usuario?");}',
                                                                                    'url'=>'Yii::app()->createUrl("usuario/User/estatus",array("id" => "$data->usuario_id"))',
                                                                                    ),
                                                                    'delete' => array(
                                                                                    'options'=>array('data-toggle'=>"tooltip"),
                                                                                    //'visible'=>'$data->estatus != "true" && Yii::app()->user->checkAccess("factura_mercancia/Factura/delete")',
                                                                                    'label'=>'Eliminar',
                                                                                    //'click'=>'function(){return confirm("¿Desea Eliminar Este Tipo de Producto?");}',
                                                                                    ),
                                              )),),)); ?>

                  </div><!-- /.box-body -->
            </div><!-- /.box -->
