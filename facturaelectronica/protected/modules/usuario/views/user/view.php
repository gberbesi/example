
  <?php foreach(Yii::app()->user->getFlashes() as $key => $message) { echo '<div class="alert alert-success" style="text-align:center;"> <button type="button" class="close" data-dismiss="alert">×</button> ' . $message . "</div>\n"; } ?>


           <!-- general form elements -->
              <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <spam class="glyphicon glyphicon-user"></spam>
                    <h3 class="box-title">Usuario</h3>
                </div><!-- /.box-header -->

                  <div class="box-body">
                    <div class="post">
                        <div class="user-block">
                            <a data-toggle="tooltip" title="<?php echo (($model->persona->imagen)?"Foto de Usuario":"Genero de Usuario");?>" href="<?php echo (($model->persona->imagen)?$imagen="http://".$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.Yii::app()->params['RutaUsuarios'].$model->persona->imagen: (($model->persona->sexo)? ($model->persona->sexo=="M")?"themes/classic/dist/img/man.png":"themes/classic/dist/img/woman.png" :"themes/classic/dist/img/user.png"));?>" class="fancybox">
                                <img alt="ser image" src="<?php echo (($model->persona->imagen)?$imagen="http://".$_SERVER['HTTP_HOST'].Yii::app()->baseUrl.Yii::app()->params['RutaUsuarios'].$model->persona->imagen: (($model->persona->sexo)? ($model->persona->sexo=="M")?"themes/classic/dist/img/man.png":"themes/classic/dist/img/woman.png" :"themes/classic/dist/img/user.png"));?>" class="img-circle img-bordered-sm"></img>
                            </a> 
                                <span class="username">
                                    <a href="#"><?php echo (($model->persona->apellido1 && $model->persona->nombre1)? $model->persona->apellido1." ".$model->persona->nombre1:"No Se Especifica.");?></a>
                                </span>
                                <span class="description"><?php echo "<b>Cedula: </b>".(($model->usuario)? $model->usuario:"No Se Especifica.");?></span>
                        </div><!-- /.user-block -->

                  <strong><i class="fa fa-book margin-r-5"></i><a>Nombres y Apellidos</a></strong>
                      <p class="text-muted">
                         <?php echo ($model->persona->nombre1)? "<b>-Primer Nombre: </b>".$model->persona->nombre1:"<b>-Primer Nombre: </b>No Se Especifica.";?><br>                 
                         <?php echo ($model->persona->nombre2)? "<b>-Segundo Nombre: </b>".$model->persona->nombre2:"<b>-Segundo Nombre: </b>No Se Especifica.";?><br>
                         <?php echo ($model->persona->apellido1)? "<b>-Primer Apellido: </b>".$model->persona->apellido1:"<b>-Primer Apellido: </b>No Se Especifica.";?><br>
                         <?php echo ($model->persona->apellido2)? "<b>-Segundo Apellido: </b>".$model->persona->apellido2:"<b>-Segundo Apellido: </b>No Se Especifica.";?>
                      </p>
                  <hr>

                  <strong><i class="fa fa-map-marker margin-r-5"></i><a>Contacto</a></strong>
                      <p class="text-muted">
                         <?php echo ($model->persona->telefono1)? "<b>-Teléfono: </b>".$model->persona->telefono1:"<b>-Teléfono: </b>No Se Especifica.";?><br>
                         <?php echo ($model->persona->telefono2)? "<b>-Teléfono: </b>".$model->persona->telefono2:"<b>-Teléfono: </b>No Se Especifica.";?><br>                 
                         <?php echo ($model->persona->telefono3)? "<b>-Teléfono: </b>".$model->persona->telefono3:"<b>-Teléfono: </b>No Se Especifica.";?><br>                 
                         <?php echo ($model->persona->correo)? "<b>-Correo: </b>".$model->persona->correo:"<b>-Correo: </b>No Se Especifica.";?><br>
                      </p>
                  <hr>

                 
                      
                 
                      
                    </div>                    
                  </div>
              </div>
