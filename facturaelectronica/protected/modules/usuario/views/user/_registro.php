

    <?php foreach(Yii::app()->user->getFlashes() as $key => $message) { echo '<div class="alert alert-warning" style="text-align:center;"> <button type="button" class="close" data-dismiss="alert">×</button> ' . $message . "</div>\n"; } ?>


    <div class="content" style="max-width:400px;">

	<?php $form=$this->beginWidget('CActiveForm', array('id'=>'usuario-form','enableAjaxValidation'=>false,'htmlOptions' => array('enctype' => 'multipart/form-data'))); ?>
        <?php //echo $form->errorSummary(array($model,$Captcha)); ?>

      <div class="login-logo">
        <a style="color:#000" href="<?php echo Yii::app()->createUrl('/'); ?>" ><b style="color:#000"> <h2>Venta de Frutas</h2></b></a>
      </div><!-- /.login-logo -->

      <div class="login-box-body" style="background-color: #f3f4f5 ;background: rgba(0, 0, 0, 0) radial-gradient(circle farthest-corner at 50% 0 , #f3f4f5  0px, #f3f4f5  180%) repeat scroll 0 0;box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);
                                        border: 1px solid rgba(34, 36, 38, 0.15); border-radius: 0.285714rem">
        <p class="login-box-msg" style="color:#6e6d6d">REGISTRARSE</p>
        <form method="post">

            <div class="form-group <?php echo ($model->hasErrors('nacionalidad'))?'error':(($getPost)?'success':''); ?>" style="color:#6e6d6d">
                <?php echo $form->labelEx($model,'nacionalidad',array('class'=>'control-label')); ?>
                <?php echo $form->dropDownList($model,'nacionalidad',array('V' => 'VENEZOLANO', 'E' => 'EXTRAJERO'),array('readonly'=>($model->isNewRecord)?false:true,'class'=>'form-control select2','empty'=>'','style'=>'width:100%')) ?>
                <?php echo $form->error($model,'nacionalidad',array('class'=>'btn-xs alert-danger text-center')); ?>
            </div>

            <div class="form-group has-feedback <?php echo ($model->hasErrors('cedula'))?'error':(($getPost)?'success':''); ?>" style="color:#6e6d6d">
                    <?php echo $form->labelEx($model,'cedula'); ?>
                    <?php echo $form->textField($model,'cedula',array('type'=>'number', 'class'=>'form-control campo-numero','maxlength'=>8,'placeholder'=>'Digitos','readonly'=>($model->isNewRecord) ? false:true)); ?>
                    <?php echo $form->error($model,'cedula',array('class'=>'btn-xs alert-danger text-center')); ?>
                <span class="glyphicon glyphicon-sort-by-order form-control-feedback"></span>

                 

            </div>	

            <div class="form-group has-feedback">
                    <?php echo $form->labelEx($model,'fecha_nacimiento'); ?>
                    <?php echo $form->textField($model,'fecha_nacimiento',array('type'=>'text', 'class'=>'form-control', 'placeholder'=>'Día/Mes/Año')); ?>
                    <?php echo $form->error($model,'fecha_nacimiento',array('class'=>'btn-xs alert-danger text-center')); ?>
                <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
            </div>					

            <div class="form-group has-feedback <?php echo ($model->hasErrors('nombre1'))?'error':(($getPost)?'success':''); ?>" style="color:#6e6d6d">
                    <?php echo $form->labelEx($model,'nombre1'); ?>
                    <?php echo $form->textField($model,'nombre1',array('type'=>'text', 'class'=>'form-control campo-texto')); ?>
                    <?php echo $form->error($model,'nombre1',array('class'=>'btn-xs alert-danger text-center')); ?>
                <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
            </div>	

            <div class="form-group has-feedback <?php echo ($model->hasErrors('apellido1'))?'error':(($getPost)?'success':''); ?>" style="color:#6e6d6d">
                    <?php echo $form->labelEx($model,'apellido1'); ?>
                    <?php echo $form->textField($model,'apellido1',array('type'=>'text', 'class'=>'form-control campo-texto', 'placeholder'=>'Text')); ?>
                    <?php echo $form->error($model,'apellido1',array('class'=>'btn-xs alert-danger text-center')); ?>
                <span class="glyphicon glyphicon-text-size form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback <?php echo ($model->hasErrors('correo'))?'error':(($getPost)?'success':''); ?>" style="color:#6e6d6d">
                    <?php echo $form->labelEx($model,'correo'); ?>
                    <?php echo $form->textField($model,'correo',array('type'=>'email', 'class'=>'form-control', 'placeholder'=>'Email')); ?>
                    <?php echo $form->error($model,'correo',array('class'=>'btn-xs alert-danger text-center')); ?>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback  <?php echo ($model->hasErrors('telefono'))?'error':(($getPost)?'success':''); ?>" style="color:#6e6d6d">
                    <?php echo $form->labelEx($model,'telefono'); ?>
                    <?php $this->widget('CMaskedTextField', array('model' => $model,
                                                      'attribute' => "telefono",
                                                      'mask' => '(9999)-999-9999',
                                                      'htmlOptions' => array('class'=>'form-control','placeholder'=>'Ejemplo: (0412)-999-9999')));?>
                    <?php echo $form->error($model,'telefono',array('class'=>'btn-xs alert-danger text-center')); ?>
                <span class="glyphicon glyphicon-phone form-control-feedback"></span>
            </div>

            <?php if(CCaptcha::checkRequirements()): ?>
                <div class="form-group has-feedback <?php echo ($Captcha->hasErrors('verifyCode'))?'error':(($getPost)?'success':''); ?>" style="color:#6e6d6d">
                        <?php echo $form->labelEx($Captcha,'verifyCode'); ?>
                        <?php echo $form->textField($Captcha,'verifyCode',array('type'=>'number', 'autocomplete'=>'off','class'=>'form-control', 'placeholder'=>'Código de Verificación')); ?>
                        <?php echo $form->error($Captcha,'verifyCode'); ?>
                    <span class="glyphicon glyphicon-edit form-control-feedback"></span>
                    <div  style="text-align:center">
                        <br>
                        <?php $this->widget('CCaptcha',array('clickableImage'=>true,'showRefreshButton'=>false)); ?>
                    </div>  
                </div>
            <?php endif; ?>
        </form>

        <div class="text-center">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar Usuario' : 'Actualizar',array('class'=>'btn btn-success')); ?>
                <?php //echo CHtml::resetButton('Limpiar', array('class'=>'btn btn-danger')); ?>	
        </div>

      </div><!-- /.login-box-body -->
      <?php $this->endWidget(); ?>
    </div><!-- /.login-box -->
