<?php 

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	private $_id;
	
	public function autenticar() {
		$record=Usuario::model()->find('lower(usuario)=:usuario and estatus=true',array(':usuario'=>strtolower($this->username)));
			if($record===null)
				$this->errorCode=self::ERROR_USERNAME_INVALID;
			else if($record->clave!==md5($this->password))
				$this->errorCode=self::ERROR_PASSWORD_INVALID;
			else if(!$record->usuario_activo_sistema) {
				$this->errorCode=3;
			} else {
				$this->_id=$record->usuario_id;
				//$this->setState('rol', $record->rol->tx_rol);
				Yii::import('application.modules.usuario.models.AuthItem',true);
				$this->setState('rol', $record->authitems[0]->name);
				$this->setState('usucedula', $record->persona->cedula);
				
				$this->errorCode=self::ERROR_NONE;
			}
			return !$this->errorCode;
	}
	
	public function getId() {
        return $this->_id;
    }
    
}