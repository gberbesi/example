<?php
	//Yii::import('application.modules.archivos.models.DataCargada',true);

class UsuarioController extends Controller
{ 

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	
	public $layout='//layouts/column2';

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('login','registro','recuperar','logout','consulta'),
				'users'=>array('*'),
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('nueva','index'),
				'users'=>array('@'),
			),



			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actions()
	{ 
		//return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
		// 	'captcha'=>array(
		// 		'class'=>'CCaptchaAction',
		// 		'backColor'=>0xF5F5F5,
		// 		'testLimit'=>1,
		// 	),
		// );
	}

	public function actionIndex()
	{
		$this->layout ='//layouts/main';
		Yii::import('application.modules.facturas.models.Factura');
	   	$fecha=date('j/n/Y')."%";
	   	
       $facturas=Factura::model()->findAll('fecha LIKE "'.$fecha.'"');

        $facturasdeldia=count($facturas);
        $contadodia=0;
        $creditodia=0;
 		$ventasdiac=0;
 		$ventasdiad=0;


 if(date('j')>=1 && date('j')<=7){
 	$rango1=date('1/n/Y');
 	$rango2=date('2/n/Y');
 	$rango3=date('3/n/Y');
 	$rango4=date('4/n/Y');
 	$rango5=date('5/n/Y');
 	$rango6=date('6/n/Y');
 	$rango7=date('7/n/Y');
 	$rango8=date('7/n/Y');
 	$rango9=date('7/n/Y');
 	$rango10=date('7/n/Y');


 }
 else if(date('j')>=8 && date('j')<=14){
 	$rango1=date('8/n/Y');
 	$rango2=date('9/n/Y');
 	$rango3=date('10/n/Y');
 	$rango4=date('11/n/Y');
 	$rango5=date('12/n/Y');
 	$rango6=date('13/n/Y');
 	$rango7=date('14/n/Y');
 	$rango8=date('14/n/Y');
 	$rango9=date('14/n/Y');
 	$rango10=date('14/n/Y');
 }
 else if(date('j')>=15 && date('j')<=21){
 	$rango1=date('15/n/Y');
 	$rango2=date('16/n/Y');
 	$rango3=date('17/n/Y');
 	$rango4=date('18/n/Y');
 	$rango5=date('19/n/Y');
 	$rango6=date('20/n/Y');
 	$rango7=date('21/n/Y');
 	$rango8=date('21/n/Y');
 	$rango9=date('21/n/Y');
 	$rango10=date('21/n/Y');
 }
 else if(date('j')>=22 && date('j')<=31){
 	$rango1=date('22/n/Y');
 	$rango2=date('23/n/Y');
 	$rango3=date('24/n/Y');
 	$rango4=date('25/n/Y');
 	$rango5=date('26/n/Y');
 	$rango6=date('27/n/Y');
 	$rango7=date('28/n/Y');
 	$rango8=date('29/n/Y');
 	$rango9=date('30/n/Y');
 	$rango10=date('31/n/Y');
 }
 $facturas2=Factura::model()->findAll('fecha like "'.$rango1.'%" or fecha like "'.$rango2.'%" or fecha like "'.$rango3.'%" or fecha like "'.$rango4.'%" or fecha like "'.$rango5.'%" or fecha like "'.$rango6.'%" or fecha like "'.$rango7.'%" or fecha like "'.$rango8.'%" or fecha like "'.$rango9.'%" or fecha like "'.$rango10.'%"');

 $facturassemana=count($facturas2);	
 $contadosemana=0;
 $creditosemana=0;
 $ventassemanac=0;
 $ventassemanad=0;

$fechames="%".date('/n/Y')."%";
 $facturas3=Factura::model()->findAll('fecha LIKE "'.$fechames.'"');

$facturasmes=count($facturas3);	
 $contadomes=0;
 $creditomes=0;
 $ventasmesc=0;
 $ventasmesd=0;


        foreach ($facturas3 as $key => $value) {	
        	if($value->dias==0){
        		$contadomes++;
        	}else{
        		$creditomes++;
        	}
        	if($value->id_moneda==1){
			$ventasmesc+=$value->total2;
			}else{
			$ventasmesd+=$value->total2;
			}
				
        }
        foreach ($facturas as $key => $value) {	
        	if($value->dias==0){
        		$contadodia++;
        	}else{
        		$creditodia++;
        	}
        	if($value->id_moneda==1){
			$ventasdiac+=$value->total2;
			}else{
			$ventasdiad+=$value->total2;
			}
				
        }

       foreach ($facturas2 as $key => $value) {	
        	if($value->dias==0){
        		$contadosemana++;
        	}else{
        		$creditosemana++;
        	}
			
			if($value->id_moneda==1){
				$ventassemanac+=$value->total2;
				}else{
				$ventassemanad+=$value->total2;
				}
        }

     $semana1=Factura::model()->findAll('fecha like "'.date('1/n/Y').'%" or fecha like "'.date('2/n/Y').'%" or fecha like "'.date('3/n/Y').'%" or fecha like "'.date('4/n/Y').'%" or fecha like "'.date('5/n/Y').'%" or fecha like "'.date('6/n/Y').'%" or fecha like "'.date('7/n/Y').'%" or fecha like "'.date('7/n/Y').'%" or fecha like "'.date('7/n/Y').'%" or fecha like "'.date('7/n/Y').'%"');

 $semana1factura=count($semana1);	
 $semana1contado=0;
 $semana1credito=0;
 $semana1ventasc=0;
 $semana1ventasd=0;


        foreach ($semana1 as $key => $value) {	
        	if($value->dias==0){
        		 $semana1contado++;
        	}else{
        		$semana1credito++;
        	}
        	if($value->id_moneda==1){
			$semana1ventasc+=$value->total2;
			}else{
			$semana1ventasd+=$value->total2;
			}
				
        } 

             $semana2=Factura::model()->findAll('fecha like "'.date('8/n/Y').'%" or fecha like "'.date('9/n/Y').'%" or fecha like "'.date('10/n/Y').'%" or fecha like "'.date('11/n/Y').'%" or fecha like "'.date('12/n/Y').'%" or fecha like "'.date('13/n/Y').'%" or fecha like "'.date('14/n/Y').'%" or fecha like "'.date('14/n/Y').'%" or fecha like "'.date('14/n/Y').'%" or fecha like "'.date('14/n/Y').'%"');

 $semana2factura=count($semana2);	
 $semana2contado=0;
 $semana2credito=0;
 $semana2ventasc=0;
 $semana2ventasd=0;


        foreach ($semana2 as $key => $value) {	
        	if($value->dias==0){
        		 $semana2contado++;
        	}else{
        		$semana2credito++;
        	}
        	if($value->id_moneda==1){
			$semana2ventasc+=$value->total2;
			}else{
			$semana2ventasd+=$value->total2;
			}
				
        }

         $semana3=Factura::model()->findAll('fecha like "'.date('15/n/Y').'%" or fecha like "'.date('16/n/Y').'%" or fecha like "'.date('17/n/Y').'%" or fecha like "'.date('18/n/Y').'%" or fecha like "'.date('19/n/Y').'%" or fecha like "'.date('20/n/Y').'%" or fecha like "'.date('21/n/Y').'%" or fecha like "'.date('21/n/Y').'%" or fecha like "'.date('21/n/Y').'%" or fecha like "'.date('21/n/Y').'%"');

 $semana3factura=count($semana3);	
 $semana3contado=0;
 $semana3credito=0;
 $semana3ventasc=0;
 $semana3ventasd=0;


        foreach ($semana3 as $key => $value) {	
        	if($value->dias==0){
        		 $semana3contado++;
        	}else{
        		$semana3credito++;
        	}
        	if($value->id_moneda==1){
			$semana3ventasc+=$value->total2;
			}else{
			$semana3ventasd+=$value->total2;
			}
				
        }

          $semana4=Factura::model()->findAll('fecha like "'.date('22/n/Y').'%" or fecha like "'.date('23/n/Y').'%" or fecha like "'.date('24/n/Y').'%" or fecha like "'.date('25/n/Y').'%" or fecha like "'.date('26/n/Y').'%" or fecha like "'.date('27/n/Y').'%" or fecha like "'.date('28/n/Y').'%" or fecha like "'.date('29/n/Y').'%" or fecha like "'.date('30/n/Y').'%" or fecha like "'.date('31/n/Y').'%"');

 $semana4factura=count($semana4);	
 $semana4contado=0;
 $semana4credito=0;
 $semana4ventasc=0;
 $semana4ventasd=0;


        foreach ($semana4 as $key => $value) {	
        	if($value->dias==0){
        		 $semana4contado++;
        	}else{
        		$semana4credito++;
        	}
        	if($value->id_moneda==1){
			$semana4ventasc+=$value->total2;
			}else{
			$semana4ventasd+=$value->total2;
			}
				
        }

   $totalsemanac= $semana1ventasc+$semana2ventasc+$semana3ventasc+$semana4ventasc;
   if($totalsemanac!=0){
   $porcentajesemana1c=($semana1ventasc*100)/$totalsemanac;
   $porcentajesemana2c=($semana2ventasc*100)/$totalsemanac;
   $porcentajesemana3c=($semana3ventasc*100)/$totalsemanac;
   $porcentajesemana4c=($semana4ventasc*100)/$totalsemanac;
}else{
   $porcentajesemana1c=0;
   $porcentajesemana2c=0;
   $porcentajesemana3c=0;
   $porcentajesemana4c=0;

}

   $totalsemanad= $semana1ventasd+$semana2ventasd+$semana3ventasd+$semana4ventasd;   
if($totalsemanad!=0){
   $porcentajesemana1d=($semana1ventasd*100)/$totalsemanad;
   $porcentajesemana2d=($semana2ventasd*100)/$totalsemanad;
   $porcentajesemana3d=($semana3ventasd*100)/$totalsemanad;
   $porcentajesemana4d=($semana4ventasd*100)/$totalsemanad;
}else{
    $porcentajesemana1d=0;
   $porcentajesemana2d=0;
   $porcentajesemana3d=0;
   $porcentajesemana4d=0;
}

 $enero=Factura::model()->findAll('fecha LIKE "%'.date('/1/Y').'%"');
 $facturasmesenero=count($enero);	
 $contadoenero=0;
 $creditoenero=0;
 $ventaseneroc=0;
 $ventasenerod=0;


        foreach ($enero as $key => $value) {	
        	if($value->dias==0){
        		$contadoenero++;
        	}else{
        		$creditoenero++;
        	}
        	if($value->id_moneda==1){
			$ventaseneroc+=$value->total2;
			}else{
			$ventasenerod+=$value->total2;
			}
				
        }

 $febrero=Factura::model()->findAll('fecha LIKE "%'.date('/2/Y').'%"');
 $facturasmesfebrero=count($febrero);	
 $contadofebrero=0;
 $creditofebrero=0;
 $ventasfebreroc=0;
 $ventasfebrerod=0;


        foreach ($febrero as $key => $value) {	
        	if($value->dias==0){
        		$contadofebrero++;
        	}else{
        		$creditofebrero++;
        	}
        	if($value->id_moneda==1){
			$ventasfebreroc+=$value->total2;
			}else{
			$ventasfebrerod+=$value->total2;
			}
				
        }
 $marzo=Factura::model()->findAll('fecha LIKE "%'.date('/3/Y').'%"');
 $facturasmesmarzo=count($marzo);	
 $contadomarzo=0;
 $creditomarzo=0;
 $ventasmarzoc=0;
 $ventasmarzod=0;


        foreach ($marzo as $key => $value) {	
        	if($value->dias==0){
        		$contadomarzo++;
        	}else{
        		$creditomarzo++;
        	}
        	if($value->id_moneda==1){
			$ventasmarzoc+=$value->total2;
			}else{
			$ventasmarzod+=$value->total2;
			}
				
        }
 $abril=Factura::model()->findAll('fecha LIKE "%'.date('/4/Y').'%"');
 $facturasmesabril=count($abril);	
 $contadoabril=0;
 $creditoabril=0;
 $ventasabrilc=0;
 $ventasabrild=0;


        foreach ($abril as $key => $value) {	
        	if($value->dias==0){
        		$contadoabril++;
        	}else{
        		$creditoabril++;
        	}
        	if($value->id_moneda==1){
			$ventasabrilc+=$value->total2;
			}else{
			$ventasabrild+=$value->total2;
			}
				
        }
 $mayo=Factura::model()->findAll('fecha LIKE "%'.date('/5/Y').'%"');
 $facturasmesmayo=count($mayo);	
 $contadomayo=0;
 $creditomayo=0;
 $ventasmayoc=0;
 $ventasmayod=0;


        foreach ($mayo as $key => $value) {	
        	if($value->dias==0){
        		$contadomayo++;
        	}else{
        		$creditomayo++;
        	}
        	if($value->id_moneda==1){
			$ventasmayoc+=$value->total2;
			}else{
			$ventasmayod+=$value->total2;
			}
				
        }
 $junio=Factura::model()->findAll('fecha LIKE "%'.date('/6/Y').'%"');
 $facturasmesjunio=count($junio);	
 $contadojunio=0;
 $creditojunio=0;
 $ventasjunioc=0;
 $ventasjuniod=0;


        foreach ($junio as $key => $value) {	
        	if($value->dias==0){
        		$contadojunio++;
        	}else{
        		$creditojunio++;
        	}
        	if($value->id_moneda==1){
			$ventasjunioc+=$value->total2;
			}else{
			$ventasjuniod+=$value->total2;
			}
				
        }
 $julio=Factura::model()->findAll('fecha LIKE "%'.date('/7/Y').'%"');
 $facturasmesjulio=count($julio);	
 $contadojulio=0;
 $creditojulio=0;
 $ventasjulioc=0;
 $ventasjuliod=0;


        foreach ($julio as $key => $value) {	
        	if($value->dias==0){
        		$contadojulio++;
        	}else{
        		$creditojulio++;
        	}
        	if($value->id_moneda==1){
			$ventasjulioc+=$value->total2;
			}else{
			$ventasjuliod+=$value->total2;
			}
				
        }
 $agosto=Factura::model()->findAll('fecha LIKE "%'.date('/8/Y').'%"');
 $facturasmesagosto=count($agosto);	
 $contadoagosto=0;
 $creditoagosto=0;
 $ventasagostoc=0;
 $ventasagostod=0;


        foreach ($agosto as $key => $value) {	
        	if($value->dias==0){
        		$contadoagosto++;
        	}else{
        		$creditoagosto++;
        	}
        	if($value->id_moneda==1){
			$ventasagostoc+=$value->total2;
			}else{
			$ventasagostod+=$value->total2;
			}
				
        }
 $septiembre=Factura::model()->findAll('fecha LIKE "%'.date('/9/Y').'%"');
 $facturasmesseptiembre=count($septiembre);	
 $contadoseptiembre=0;
 $creditoseptiembre=0;
 $ventasseptiembrec=0;
 $ventasseptiembred=0;


        foreach ($septiembre as $key => $value) {	
        	if($value->dias==0){
        		$contadoseptiembre++;
        	}else{
        		$creditoseptiembre++;
        	}
        	if($value->id_moneda==1){
			$ventasseptiembrec+=$value->total2;
			}else{
			$ventasseptiembred+=$value->total2;
			}
				
        }
 $octubre=Factura::model()->findAll('fecha LIKE "%'.date('/10/Y').'%"');
 $facturasmesoctubre=count($octubre);	
 $contadooctubre=0;
 $creditooctubre=0;
 $ventasoctubrec=0;
 $ventasoctubred=0;


        foreach ($octubre as $key => $value) {	
        	if($value->dias==0){
        		$contadooctubre++;
        	}else{
        		$creditooctubre++;
        	}
        	if($value->id_moneda==1){
			$ventasoctubrec+=$value->total2;
			}else{
			$ventasoctubred+=$value->total2;
			}
				
        }
 $noviembre=Factura::model()->findAll('fecha LIKE "%'.date('/11/Y').'%"');
 $facturasmesnoviembre=count($noviembre);	
 $contadonoviembre=0;
 $creditonoviembre=0;
 $ventasnoviembrec=0;
 $ventasnoviembred=0;


        foreach ($noviembre as $key => $value) {	
        	if($value->dias==0){
        		$contadonoviembre++;
        	}else{
        		$creditonoviembre++;
        	}
        	if($value->id_moneda==1){
			$ventasnoviembrec+=$value->total2;
			}else{
			$ventasnoviembred+=$value->total2;
			}
				
        }
 $diciembre=Factura::model()->findAll('fecha LIKE "%'.date('/12/Y').'%"');
 $facturasmesdiciembre=count($diciembre);	
 $contadodiciembre=0;
 $creditodiciembre=0;
 $ventasdiciembrec=0;
 $ventasdiciembred=0;


        foreach ($diciembre as $key => $value) {	
        	if($value->dias==0){
        		$contadodiciembre++;
        	}else{
        		$creditodiciembre++;
        	}
        	if($value->id_moneda==1){
			$ventasdiciembrec+=$value->total2;
			}else{
			$ventasdiciembred+=$value->total2;
			}
				
        }

    $totalmesesc= $ventaseneroc+$ventasfebreroc+$ventasmarzoc+$ventasabrilc+$ventasmayoc+$ventasjunioc+$ventasjulioc+$ventasagostoc+$ventasseptiembrec+$ventasoctubrec+$ventasnoviembrec+$ventasdiciembrec;

   $eneromesc=($ventaseneroc*100)/$totalmesesc;
   $febreromesc=($ventasfebreroc*100)/$totalmesesc;
   $marzomesc=($ventasmarzoc*100)/$totalmesesc;
   $abrilmesc=($ventasabrilc*100)/$totalmesesc;
   $mayomesc=($ventasmayoc*100)/$totalmesesc;
   $juniomesc=($ventasjunioc*100)/$totalmesesc;
   $juliomesc=($ventasjulioc*100)/$totalmesesc;
   $agostomesc=($ventasagostoc*100)/$totalmesesc;
   $septiembremesc=($ventasseptiembrec*100)/$totalmesesc;
   $octubremesc=($ventasoctubrec*100)/$totalmesesc;
   $noviembremesc=($ventasnoviembrec*100)/$totalmesesc;
   $diciembremesc=($ventasdiciembrec*100)/$totalmesesc;

   $totalmesesd= $ventasenerod+$ventasfebrerod+$ventasmarzod+$ventasabrild+$ventasmayod+$ventasjuniod+$ventasjuliod+$ventasagostod+$ventasseptiembred+$ventasoctubred+$ventasnoviembred+$ventasdiciembred;   

   $eneromesd=($ventasenerod*100)/$totalmesesc;
   $febreromesd=($ventasfebrerod*100)/$totalmesesc;
   $marzomesd=($ventasmarzod*100)/$totalmesesc;
   $abrilmesd=($ventasabrild*100)/$totalmesesc;
   $mayomesd=($ventasmayod*100)/$totalmesesc;
   $juniomesd=($ventasjuniod*100)/$totalmesesc;
   $juliomesd=($ventasjuliod*100)/$totalmesesc;
   $agostomesd=($ventasagostod*100)/$totalmesesc;
   $septiembremesd=($ventasseptiembred*100)/$totalmesesc;
   $octubremesd=($ventasoctubred*100)/$totalmesesc;
   $noviembremesd=($ventasnoviembred*100)/$totalmesesc;
   $diciembremesd=($ventasdiciembred*100)/$totalmesesc;    


		$this->render('index',array('facturasdeldia'=>$facturasdeldia,
			'ventasdiac'=>$ventasdiac,
			'contadodia'=>$contadodia,
			'creditodia'=>$creditodia,
			'ventasdiad'=>$ventasdiad,
			'facturassemana'=>$facturassemana,
			'contadosemana'=>$contadosemana,
			'creditosemana'=>$creditosemana,
			'ventassemanac'=>$ventassemanac,
			'ventassemanad'=>$ventassemanad,
			'facturasmes'=>$facturasmes,
			'contadomes'=>$contadomes,
			'creditomes'=>$creditomes,
			'ventasmesc'=>$ventasmesc,
			'ventasmesd'=>$ventasmesd,
			'semana1ventasc'=>$semana1ventasc,
			'semana1ventasd'=>$semana1ventasd,
			'semana2ventasc'=>$semana2ventasc,
			'semana2ventasd'=>$semana2ventasd,
			'semana3ventasc'=>$semana3ventasc,
			'semana3ventasd'=>$semana3ventasd,
			'semana4ventasc'=>$semana4ventasc,
			'semana4ventasd'=>$semana4ventasd,
			'porcentajesemana1c'=>$porcentajesemana1c,
			'porcentajesemana2c'=>$porcentajesemana2c,
			'porcentajesemana3c'=>$porcentajesemana3c,
			'porcentajesemana4c'=>$porcentajesemana4c,
			'porcentajesemana1d'=>$porcentajesemana1d,
			'porcentajesemana2d'=>$porcentajesemana2d,
			'porcentajesemana3d'=>$porcentajesemana3d,
			'porcentajesemana4d'=>$porcentajesemana4d,
			'ventaseneroc'=>$ventaseneroc,
			'ventasenerod'=>$ventasenerod,
			'ventasfebreroc'=>$ventasfebreroc,
			'ventasfebrerod'=>$ventasfebrerod,
			'ventasmarzoc'=>$ventasmarzoc,
			'ventasmarzod'=>$ventasmarzod,
			'ventasabrilc'=>$ventasabrilc,
			'ventasabrild'=>$ventasabrild,
			'ventasmayoc'=>$ventasmayoc,
			'ventasmayod'=>$ventasmayod,
			'ventasjunioc'=>$ventasjunioc,
			'ventasjuniod'=>$ventasjuniod,
			'ventasjulioc'=>$ventasjulioc,
			'ventasjuliod'=>$ventasjuliod,
			'ventasagostoc'=>$ventasagostoc,
			'ventasagostod'=>$ventasagostod,
			'ventasseptiembrec'=>$ventasseptiembrec,
			'ventasseptiembred'=>$ventasseptiembred,
			'ventasoctubrec'=>$ventasoctubrec,
			'ventasoctubred'=>$ventasoctubred,
			'ventasnoviembrec'=>$ventasnoviembrec,
			'ventasnoviembred'=>$ventasnoviembred,
			'ventasdiciembrec'=>$ventasdiciembrec,
			'ventasdiciembred'=>$ventasdiciembred,
			'eneromesc'=>$eneromesc,
			'febreromesc'=>$febreromesc,
			'marzomesc'=>$marzomesc,
			'abrilmesc'=>$abrilmesc,
			'mayomesc'=>$mayomesc,
			'juniomesc'=>$juniomesc,
			'juliomesc'=>$juliomesc,
			'agostomesc'=>$agostomesc,
			'septiembremesc'=>$septiembremesc,
			'octubremesc'=>$octubremesc,
			'noviembremesc'=>$noviembremesc,
			'diciembremesc'=>$diciembremesc,

			'eneromesd'=>$eneromesd,
			'febreromesd'=>$febreromesd,
			'marzomesd'=>$marzomesd,
			'abrilmesd'=>$abrilmesd,
			'mayomesd'=>$mayomesd,
			'juniomesd'=>$juniomesd,
			'juliomesd'=>$juliomesd,
			'agostomesd'=>$agostomesd,
			'septiembremesd'=>$septiembremesd,
			'octubremesd'=>$octubremesd,
			'noviembremesd'=>$noviembremesd,
			'diciembremesd'=>$diciembremesd,



			


		));
	}


	public function actionLogin()
	{
$this->layout ='//layouts/mainlog';
		$getPost=false;

		$datospost=false;
		if(!Yii::app()->user->isGuest) {
				$this->redirect(Yii::app()->homeUrl);
		}
		$model=new Usuario('login');
		//$Captcha=new Captcha();
		//$Captcha->validate();
		//$Captcha=new Captcha();
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['Usuario']))
		{
			$getPost=true;
			$datospost=true;
			$model->attributes=$_POST['Usuario'];
			//$Captcha->attributes=$_POST['Captcha'];
			// validate user input and redirect to the previous page if valid
//$Captcha->validate()&&
			if( $model->validate() && $model->login()) {    
				$user=Usuario::model()->findByAttributes(array('usuario_id'=>Yii::app()->user->id));
				$user->acceso = true;
				$user->save(false);
				$this->redirect(Yii::app()->homeUrl);
			}
		}
		// display the login form
		//$Captcha->verifyCode='';
		$this->render('login',array('model'=>$model,'datospost'=>$datospost,'getPost'=>$getPost));
	}

	public function actionNueva()
	{
		$layout='//layouts/column1';
		$getPost=false;
		$model=New Usuario('cambio');
		// Uncomment the following line if AJAX validation is needed
		 //$this->performAjaxValidation($model);

		if(isset($_POST['Usuario']))
		{
			$model->attributes=$_POST['Usuario'];
			if($model->validate()) {
				$transaction = $model->dbConnection->beginTransaction();
				try {
					$user=Usuario::model()->findByAttributes(array('usuario_id'=>Yii::app()->user->id));
					if($user->clave!=md5($model->clave)) {
						$model->addError('clave','Contraseña actual incorrecta.');
					} else {
							$password=$model->nueva;
							$user->clave=md5($model->nueva);
							$user->usuario_id_aud=Yii::app()->user->id;
							if($user->save()){

															// $persona=array('nombre1'=>$user->persona->nombre1,'apellido1'=>$user->persona->apellido1);
			            //             $text='El dia: '.date('d/M/Y').', usted ha cambiado su clave para ingresar al Sistema de Gestión de Documentos Pre-Asignación Colina de Río Tuy, su clave es: '.$password;
			            //             $mensaje= FuncionesGlobales::textcorreo($text,$user->persona);
			            //             $correo=FuncionesGlobales::correo($user->persona->correo,$user->persona->nombre1,$mensaje);
			            //                 if($correo==false){ 
			            //                 	//throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente, nuestros analistas ya trabajan para solventar el problema.',500); 
			            //                 }

									$transaction->commit();
									Yii::app()->user->setFlash('notice', "Cambio de clave realizado con exito.");
									$this->redirect(array('/usuario/usuario/nueva'));
									Yii::app()->user->logout();
									
									Yii::app()->end();
									
							}
						}
				} catch(Exception $e) {
					$transaction->rollBack();
					throw new CHttpException($e->getCode(),$e->getMessage());
				}
			}
		}
		$this->render('nueva',array('model'=>$model,'getPost'=>$getPost));
	}

    public function actionConsulta()
    {
    	$layout='//layouts/column1';
	    //header('Content-type: application/json');
	    if(trim($_POST['cedula'])=="" || trim($_POST['nacionalidad'])=="" || !is_numeric($_POST['cedula']))
	    {
		    $existe='1';
		    echo CJSON::encode($existe);
		    Yii::app()->end();
		}
	    else {

				$data_cargada=DataCargada::model()->find('cedula=:cedula',array(':cedula'=>$_POST['cedula']));

	    		if($data_cargada){

					$usuario=Usuario::model()->find('usuario=:usuario',array(':usuario'=>$_POST['cedula']));

					if(!$usuario){

					  	$datos_onidex=Onidex::model()->find('cedula=:cedula and nac=:nac',array(':cedula'=>$_POST['cedula'],':nac'=>$_POST['nacionalidad']));
						$array_aux=array();
						$array_aux["persona"]=$datos_onidex;

					    echo CJSON::encode($array_aux);
					    Yii::app()->end();
					}
					if($usuario){
						$existe='4';
					    echo CJSON::encode($existe);
					    Yii::app()->end();
					}
				}

				if(!$data_cargada){
					$existe='2';
				    echo CJSON::encode($existe);
				    Yii::app()->end();
				}

	    }

    Yii::app()->end();

    }


	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionRecuperar()
	{
		if(!Yii::app()->user->isGuest){ $this->redirect(Yii::app()->user->returnUrl);	}
		$model=new Usuario('recuperar');
		//$Captcha=new Captcha();

		if(isset($_POST['ajax']) && $_POST['ajax']==='recuperar-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if(isset($_POST['Usuario']))
		{
			$model->attributes=$_POST['Usuario'];
			//$Captcha->attributes=$_POST['Captcha'];
			$valido=true;
			$valido=$model->validate()&&$valido;
			//$valido=$Captcha->validate()&&$valido;
			if($valido) {
				$transaction = $model->dbConnection->beginTransaction();
				try {
					$user = Usuario::model()->findByAttributes(array('usuario'=>$model->usuario,'estatus'=>true));
					if(!$user) {
						$model->addError('usuario','Usuario no esta registrado.');
					} else if(!$user->usuario_activo_sistema) {
								$model->addError('usuario','Usuario bloqueado');
							} else {
											$psswd = substr( md5(microtime()), 1, 8);
											$user->clave=md5($psswd);
											//$user->bo_temporal=1;
											if(!Yii::app()->user->isGuest) {
												$user->usuario_id_aud=Yii::app()->user->id;
											} else {
													$user->usuario_id_aud=$user->usuario_id;
												}
												if($user->save()){
													$persona=array('nombre1'=>$user->persona->nombre1,'apellido1'=>$user->persona->apellido1);
					                                $text='Se le ha restablecido su contraseña: '.$psswd;
					                                $mensaje= FuncionesGlobales::textcorreo($text,$persona);
					                                $correo=FuncionesGlobales::correo($user->persona->correo,$user->persona->nombre1,$mensaje);
					                                    if($correo==false){ throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente, nuestros analistas ya trabajan para solventar el problema.',500); }
						                            $transaction->commit();

						                            Yii::app()->user->setFlash('notice', 'El sistema ha generado exitosamente una clave para usted, tomando en cuenta los datos suministrados por usted en datos de contacto, dicha clave fue enviada al correo: '.$user->persona->correo);
													$this->redirect(array('/usuario/Usuario/login'));
												}
										}

				}
				catch(Exception $e) {
					$transaction->rollBack();
					throw new CHttpException($e->getCode(),$e->getMessage());
				}
			}
		}
		$this->render('recuperar',array('model'=>$model));  //,'Captcha'=>$Captcha
	}

}
