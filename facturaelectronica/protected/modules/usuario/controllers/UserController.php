<?php
	
class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		// return array(
		// 	// captcha action renders the CAPTCHA image displayed on the contact page
		// 	'captcha'=>array(
		// 		'class'=>'CCaptchaAction',
		// 		'backColor'=>0xF5F5F5,
		// 		'testLimit'=>1,
		// 	),
		// );
	}

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
		'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
					);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','Nuevo'),   //'captcha'
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );

	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->layout='//layouts/partial';
		$model=$this->loadModel($id);
		$rol=AuthAssignment::model()->find("userid=:userid",array(":userid"=>(string)$model->usuario_id));
                (($rol)?$rol="<b class='text-success'>".strtoupper($rol->itemname)."</b>":$rol="<b class='text-danger'>SIN ROL ASIGNADO</b>");
		$this->render('view',array('model'=>$model,'rol'=>$rol));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
        {
        $this->layout ='//layouts/main';
        $getPost=false;
        $model=new Usuario('reg');
	    $model_auth=new AuthAssignment;
        

        //inicio validad si puede asignar usuarios root
        $var_aux_root=null;
        $model_auth_root=AuthAssignment::model()->find('userid=:userid',array(':userid'=>Yii::app()->user->id));
        if($model_auth_root && $model_auth_root->itemname!='root'){ $var_aux_root='AND name!=\'root\'';}
        $rol=CHtml::listData(AuthItem::model()->findAll('type=:tipo AND name!=:name1 AND name!=:name2 '.$var_aux_root.' order by "name"',array(':tipo'=>2,':name1'=>'guest',':name2'=>'authenticated')), 'name','name' );
        //fin valida si puede asignar usuarios root
                // Uncomment the following line if AJAX validation is needed
                // $this->performAjaxValidation($model);

                if(isset($_POST['Usuario']) && isset($_POST['AuthAssignment']))
                {
                    $getPost=true;
                    $model->attributes=$_POST['Usuario'];
                    $model_auth->attributes=$_POST['AuthAssignment'];

                        $model->clave=md5($model->clave);
                        $model->acceso=1;
                        $model->usuario_activo_sistema=1;
                        $model->estatus=1;
                       // $model->usuario=strtolower(trim($model->cedula));
                            if($model->validate()){
                                $transaction=Yii::app()->db->beginTransaction();
                                try {
                                    $persona = new GenPersona();
                                    if($aux=GenPersona::model()->findByAttributes(array('cedula'=>$model->cedula))){
                                                    $aux->nacionalidad = $model->nacionalidad;
                                                    $aux->cedula = trim($model->cedula);
                                                    $aux->nombre1 = mb_strtoupper($model->nombre1, "utf-8");
                                                    $aux->apellido1 = mb_strtoupper($model->apellido1, "utf-8");
                                                    $aux->correo = $model->correo;
                                                    $aux->telefono1=$model->telefono;
                                            $persona=$aux;
                                    }else{
                                            $persona->nacionalidad=$model->nacionalidad;
                                            $persona->cedula=$model->cedula;
                                            $persona->nombre1 = mb_strtoupper($model->nombre1, "utf-8");
                                            $persona->apellido1 = mb_strtoupper($model->apellido1, "utf-8");
                                            $persona->correo=$model->correo;
                                            $persona->telefono1=$model->telefono;
                                    }
                                    $persona->usuario_id_aud=Yii::app()->user->id;;
                                    if($persona->save()){
                                            $model->persona_id=$persona->persona_id;
                                            $model->usuario_id_aud=Yii::app()->user->id;;
                                            if($model->save()){

                                                    $model_auth->userid=$model->usuario_id;
                                                    $model_auth->data="N";
                                                    $model_auth->save();
                                                    
                                                   
                                                    $transaction->commit();
                									    Yii::app()->user->setFlash('notice', 'Registro guardado exitosamente.');
                                					    $this->redirect(array('admin'));
                                                
                                            }
                                    }
                                }
                                    catch(Exception $e) {
                                            $transaction->rollBack();
                                            //throw new CHttpException($e->getCode(),$e->getMessage());
                                    }
                            }
                }

        $this->render('create',array('model'=>$model,'getPost'=>$getPost, 'model_auth'=>$model_auth,'rol'=>$rol));

        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */




	public function actionUpdate($id)
        {
        $this->layout ='//layouts/main';
        $model=$this->loadModel($id);
        $getPost=false;
        $model->scenario='reg';
        $model_auth=AuthAssignment::model()->find('userid=:userid',array(':userid'=>"$model->usuario_id"));
        if($model_auth=="" || $model_auth==null){ $model_auth=new AuthAssignment();}

        $password=$model->clave;
        $password_cam=null;

        //inicio validad si puede asignar usuarios root
        $var_aux_root=null;
        $model_auth_root=AuthAssignment::model()->find('userid=:userid',array(':userid'=>Yii::app()->user->id));
        if($model_auth_root && $model_auth_root->itemname!='root'){ $var_aux_root='AND name!=\'root\'';}
        $rol=CHtml::listData(AuthItem::model()->findAll('type=:tipo AND name!=:name1 AND name!=:name2 '.$var_aux_root.' order by "name"',array(':tipo'=>2,':name1'=>'guest',':name2'=>'authenticated')), 'name','name' );
        //fin valida si puede asignar usuarios root

        $persona=$model->persona;
        $model->nacionalidad=$persona->nacionalidad;
        $model->cedula=$id;
        if($model->nacionalidad=="" || $model->nacionalidad==null){ $model->scenario='reg_modi'; }
        $model->nombre1=$persona->nombre1;
        $model->nombre2=$persona->nombre2;
        $model->apellido1=$persona->apellido1;
        $model->apellido2=$persona->apellido2;
        $model->correo=$persona->correo;
        $model->fecha_nacimiento=date("d-m-Y", strtotime($persona->fecha_nacimiento));
        $model->telefono=$persona->telefono1;


                // Uncomment the following line if AJAX validation is needed
                // $this->performAjaxValidation($model);

                if(isset($_POST['Usuario']) && isset($_POST['AuthAssignment']))
                {

                   // $getPost=true;
                    $model->attributes=$_POST['Usuario'];
                    $model_auth->attributes=$_POST['AuthAssignment'];

                    if ($model->estatus==1) {
                        $model->estatus=1;
                        
                    }else{
                        $model->estatus=0;
                    }

                    if ($model->usuario_activo_sistema==1) {
                         $model->usuario_activo_sistema=1;
                    }else{

                        $model->usuario_activo_sistema=0;
                    }

                    $model->acceso=1;

                      if($password!=$model->clave){ $password_cam=$model->clave; }

                        $model->clave=md5($model->clave);
                       // $model->usuario=strtolower(trim($model->cedula));
                            if($model->validate()){
                                $transaction=Yii::app()->db->beginTransaction();
                                try {
                                    $persona = new GenPersona();
                                    if($persona=GenPersona::model()->findByAttributes(array('persona_id'=>$id))){
                                            $persona->nacionalidad = $model->nacionalidad;
                                            $persona->cedula = trim($model->cedula);
                                            $persona->nombre1 = mb_strtoupper($model->nombre1, "utf-8");
                                           // $persona->nombre2 = mb_strtoupper($model->nombre2, "utf-8");
                                            $persona->apellido1 = mb_strtoupper($model->apellido1, "utf-8");
                                          //  $persona->apellido2 = mb_strtoupper($model->apellido2, "utf-8");
                                            //$persona->fecha_nacimiento = $model->fecha_nacimiento;
                                         //   $persona->correo = $model->correo;
                                          //  $persona->telefono1=$model->telefono;

                                    }else{
                                           // $persona->nacionalidad=$model->nacionalidad;
                                          //  $persona->cedula=$model->cedula;
                                            $persona->nombre1 = mb_strtoupper($model->nombre1, "utf-8");
                                          //  $persona->nombre2 = mb_strtoupper($model->nombre2, "utf-8");
                                           // $persona->apellido1 = mb_strtoupper($model->apellido1, "utf-8");
                                            //$persona->apellido2 = mb_strtoupper($model->apellido2, "utf-8");
                                            //$persona->fecha_nacimiento = $model->fecha_nacimiento;
                                            //$persona->correo=$model->correo;
                                            //$persona->telefono1=$model->telefono;
                                    }

                                    $persona->usuario_id_aud=Yii::app()->user->id;;
                                    if($persona->save()){
                                            $model->persona_id=$id;
                                            $model->usuario_id_aud=Yii::app()->user->id;;
                                            if($model->save()){

                                                    $model_auth->userid=$model->usuario_id;
                                                    $model_auth->data="N";

                                                if($model_auth->save()){

                                                    if($password_cam!=null && $password_cam!=""){
                                                        $text='El Usuario Administrador del sistema ha reestablecido su clave, el sistema le ha asignado como clave: '.$password_cam;
                                                       // $mensaje= FuncionesGlobales::textcorreo($text,$persona);
                                                        //$correo=FuncionesGlobales::correo($persona->correo,$persona->apellido1,$mensaje);
                                                            //if($correo==false){ 
                                                                //throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente, nuestros analistas ya trabajan para solventar el problema.',500); 
                                                           // }
                                                    }

                                                    $transaction->commit();

                                                    Yii::app()->user->setFlash('notice', 'Registro guardado exitosamente.');
                                                        $this->redirect(array('admin'));
                                                }
                                            }
                                    }
                                }
                                    catch(Exception $e) {
                                            $transaction->rollBack();
                                            throw new CHttpException($e->getCode(),$e->getMessage());
                                    }
                            }
                }

        $this->render('update',array('model'=>$model,'getPost'=>$getPost, 'model_auth'=>$model_auth,'rol'=>$rol));

        }

    public function actionNuevo()
    {
    $getPost=false;
    $model=new Usuario('reg');
    //$Captcha=new Captcha();

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['Usuario']))    //&& isset($_POST['Captcha'])
            {
                $model->attributes=$_POST['Usuario'];
                //$Captcha->attributes=$_POST['Captcha'];
            //if($Captcha->validate()){
                   // $clavealeatoria=FuncionesGlobales::claveAleatoria();
                    //$model->clave=md5($clavealeatoria);
                    $model->clave=md5($model->clave);
                    $model->usuario=strtolower(trim($model->cedula));

                    $model->usuario_activo_sistema=1;
                    $model->estatus=1;
                    $model->acceso=1;
                    $mail_correcto = false;
                    $Sintaxis='#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';
                    if(preg_match($Sintaxis,$model->correo)){ $mail_correcto = true; }

                    if($mail_correcto == true){

                        if($model->validate()){
                            $transaction=Yii::app()->db->beginTransaction();
                            try {
                                $persona = new GenPersona();
                                if($aux=GenPersona::model()->findByAttributes(array('cedula'=>$model->cedula))){
                                                $aux->nacionalidad = $model->nacionalidad;
                                                $aux->cedula = trim($model->cedula);
                                                $aux->nombre1 = mb_strtoupper($model->nombre1, "utf-8");
                                                $aux->apellido1 = mb_strtoupper($model->apellido1, "utf-8");
                                                $aux->correo = $model->correo;
                                                $aux->telefono1=$model->telefono;
                                        $persona=$aux;
                                }else{
                                        $persona->nacionalidad=$model->nacionalidad;
                                        $persona->cedula=$model->cedula;
                                        $persona->nombre1 = mb_strtoupper($model->nombre1, "utf-8");
                                        $persona->apellido1 = mb_strtoupper($model->apellido1, "utf-8");
                                        $persona->correo=$model->correo;
                                        $persona->telefono1=	$model->telefono;
                                }
                                $persona->usuario_id_aud=0;
                                if($persona->save()){
                                        $model->persona_id=$persona->persona_id;
                                        $model->usuario_id_aud=0;
                                        if($model->save()){
                                            $model_auth=new AuthAssignment;
                                                $model_auth->itemname="AFILIADOS";
                                                $model_auth->userid=$model->usuario_id;
                                                $model_auth->data="N";
                                            if($model_auth->save()){


                                                    $transaction->commit();
                                                    Yii::app()->user->setFlash('notice', 'Registro guardado Exitosamente su Usuario.'.$model->persona->cedula." y clave: ".$clavealeatoria.'');
                                                     $text='Registro guardado exitosamente en el sistema de gestión de documentos. Con el usuario; '.$model->persona->cedula." y clave: ".$clavealeatoria.', usted podr&aacute; ingregar al sistema';
                                                    $this->redirect(array('/usuario/Usuario/login'));

                                                    $text='Registro guardado exitosamente en el sistema de gestión de documentos. Con el usuario; '.$model->persona->cedula." y clave: ".$clavealeatoria.', usted podr&aacute; ingregar al sistema';
                                                    $mensaje= FuncionesGlobales::textcorreo($text,$model->persona);
                                                    $correo=FuncionesGlobales::correo($model->persona->correo,$model->persona->apellido1,$mensaje);
                                                        //if($correo==false){ throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente, nuestros analistas ya trabajan para solventar el problema.',500); }

                                               
                                            }
                                        }
                                }
                            }
                                catch(Exception $e) {
                                        $transaction->rollBack();
                                        throw new CHttpException($e->getCode(),$e->getMessage());
                                }
                        }
                    }
                    if($mail_correcto == false){
                        Yii::app()->user->setFlash('error', "La dirección de correo no es valida");

                    }
            }
           //}

    $this->render('nuevo',array('model'=>$model,'getPost'=>$getPost));   //,'Captcha'=>$Captcha

}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionEstatus($id)
	{
        $this->layout ='//layouts/main';
		// inicio del codigo para activar y desactivar estatus

					$model=$this->loadModel($id);

                $transaction=Yii::app()->db->beginTransaction();
                try {
                            if($model->usuario_activo_sistema==0){
                                $model->usuario_activo_sistema=1;
                                if($model->save()){
                                    //$transaction->commit();
                                    //$text='Su usuario a sido Activado en el sistema de gestión de documentos.';
                                    //$mensaje= FuncionesGlobales::textcorreo($text,$model->persona);
                                    //$correo=FuncionesGlobales::correo($model->persona->correo,$model->persona->apellido1,$mensaje);
                                        
                                        if(!isset($_GET['ajax'])){ 
                                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin')); 
                                        }
                                }
                            } else if ($model->usuario_activo_sistema==1){
        			$model->usuario_activo_sistema=0;
        			if($model->save()){
                                    ////$transaction->commit();
                                    //$text='Su usuario a sido Bloqueado en el sistema de gestión de documentos';
                                    //$mensaje= FuncionesGlobales::textcorreo($text,$model->persona);
                                    //$correo=FuncionesGlobales::correo($model->persona->correo,$model->persona->apellido1,$mensaje);
                                   
                                    if(!isset($_GET['ajax'])){	
                                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin')); 
                                    }
                                }
                            }
                }
                catch(Exception $e) {
                                  $transaction->rollBack();
                                  throw new CHttpException($e->getCode(),$e->getMessage());
                }
		// fin del codigo para activar y desactivar estatus
	}
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
        $this->layout ='//layouts/main';
		$model=new Usuario('search');
		$model->unsetAttributes();  // clear any default values

        $authItem=CHtml::listData(AuthItem::model()->findAll(array('order'=>"name",'condition'=>"type=2 AND name!='guest' and name!='authenticated'")),'name','name');


		if(isset($_GET['Usuario']))
			$model->attributes=$_GET['Usuario'];

		$this->render('admin',array('model'=>$model,'authItem'=>$authItem));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Usuario::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'La página solicitada no existe.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='usuario-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
