<?php
	
class GenPersonaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('validarCertificado'),
				'users'=>array('*'),
			),
			
			array('allow','actions' => array('Update'), 'roles' => array('usuario/GenPersona/Update')),
			array('allow','actions' => array('Carpetafisica'), 'roles' => array('usuario/GenPersona/Carpetafisica')),			
			array('allow','actions' => array('Admin'), 'roles' => array('usuario/GenPersona/Admin')),

			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','certificado','admin'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','municipio','parroquia'/*,'Certificado'*/),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $this->layout='//layouts/partial';
		$model=$this->loadModel($id);
				$usuario=Usuario::model()->find("persona_id=:persona_id",array(":persona_id"=>(string)$model->persona_id));
				if($usuario){
				$rol=AuthAssignment::model()->find("userid=:userid",array(":userid"=>(string)$usuario->usuario_id));
		                (($rol)?$rol="<b class='text-success'>".strtoupper($rol->itemname)."</b>":$rol="<b class='text-danger'>SIN ROL ASIGNADO</b>");
		        }
		$this->render('view',array('model'=>$model,'rol'=>$rol));
	}

    public function actionCertificado()
    {
	    $usuario = Usuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>Yii::app()->user->id));//realidad
	    $datos = GenPersona::model()->find('persona_id=:persona_id',array(':persona_id'=>$usuario->persona_id));//realidad
        $this->render('certificado',array('datos'=>$datos));
    }

		public function actionValidarCertificado($id)
		{
			//$usuario = Usuario::model()->find('usuario_id=:usuario_id',array(':usuario_id'=>Yii::app()->user->id));//realidad
			$datos = GenPersona::model()->find('md5(persona_id::TEXT)=:persona_id',array(':persona_id'=>$id));//realidad
				$this->renderPartial('certificado2',array('datos'=>$datos));
		}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	/*public function actionCreate()
	{
	    $this->layout='//layouts/partial';

		$model=new GenPersona;
        $profesion=CHtml::listData(Profesion::model()->findAll(array('order'=>'nombre ASC'),'estatus != False'), 'profesion_id', 'nombre');
        $estadocivil=CHtml::listData(EstadoCivil::model()->findAll(array('order'=>'nombre ASC'),'estatus != False'), 'estado_civil_id', 'nombre');
        $pais=CHtml::listData(Pais::model()->findAll(array('order'=>'nombre ASC'),'estatus != False'), 'pais_id', 'nombre');
        $estado=CHtml::listData(Estado::model()->findAll(array('order'=>'nombre ASC'),'estatus != false'), 'estado_id', 'nombre');
        $insti=CHtml::listData(Institucion::model()->findAll(), 'id_institucion', 'nb_institucion');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['GenPersona']))
		{
			$model->attributes=$_POST['GenPersona'];

				$transaction=Yii::app()->db->beginTransaction();
				try {
                            $model->ingreso_mensual=str_replace('.', '', $model->ingreso_mensual);
                            $model->ingreso_mensual=str_replace(',', '.', $model->ingreso_mensual);                                                        //IMAGEN DEL USUARIO
                            $uploadedFile=CUploadedFile::getInstance($model,'imagen');
                            $tipo_archivo=substr($uploadedFile, -4);
                            $fileName = $model->cedula.$tipo_archivo;   // random number + file name or puedes usar: $fileName=$uploadedFile->getName();
                            if(!empty($uploadedFile))  // check if uploaded file is set or not
                            {
                            $uploadedFile->saveAs(YiiBase::getPathOfAlias("webroot").Yii::app()->params['RutaUsuarios'].$fileName);  // image will uplode to rootDirectory/banner/
                            $model->imagen= $fileName;
                        	}

							if($model->validate()){
								if($model->save()){
                                        $text='El sistama de gestion, consultor sico a recibido con exito la actualización de sus datos...';
                                        $mensaje= FuncionesGlobales::textcorreo($text,$model);
                                        $correo=FuncionesGlobales::correo($model->correo,$model->apellido1,$mensaje);
                                            if($correo==false){ throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente, nuestros analistas ya trabajan para solventar el problema.',500); }

									$transaction->commit();
									$this->redirect(array('view','id'=>$model->persona_id));
								}
							}
				}
				catch(Exception $e) {
				                  $transaction->rollBack();
				                  throw new CHttpException($e->getCode(),$e->getMessage());
				}
				$model->validate();
		}

		$this->render('create',array('model'=>$model,'pais'=>$pais,'estado'=>$estado,'profesion'=>$profesion,'estadocivil'=>$estadocivil,'insti'=>$insti));
	}*/
	public function actionCreate()
	{
        $this->layout ='//layouts/partial';
		$model=new GenPersona;
        $profesion=CHtml::listData(Profesion::model()->findAll(array('order'=>'nombre ASC'),'estatus != False'), 'profesion_id', 'nombre');
        $estadocivil=CHtml::listData(EstadoCivil::model()->findAll(array('order'=>'nombre ASC'),'estatus != False'), 'estado_civil_id', 'nombre');
        $pais=CHtml::listData(Pais::model()->findAll(array('order'=>'nombre ASC'),'estatus != False'), 'pais_id', 'nombre');
        $insti=CHtml::listData(Institucion::model()->findAll(), 'id_institucion', 'nb_institucion');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GenPersona']))
		{

			$model->attributes=$_POST['GenPersona'];
            $model->ingreso_mensual=0;

			if($model->save())
				$this->redirect(array('view','id'=>$model->persona_id));
		}

		$this->render('registro_persona',array('model'=>$model,'pais'=>$pais,'profesion'=>$profesion,'estadocivil'=>$estadocivil,'insti'=>$insti));

	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
	        $this->layout ='//layouts/partial';
            $model=$this->loadModel($id);
            $model->fecha_nacimiento=date("d-m-Y", strtotime($model->fecha_nacimiento));
            $modeldireccion=$model->direccion;
            if($modeldireccion=="" || $modeldireccion==null){ $modeldireccion=new Direccion; }
            $text_imagen=$model->imagen;
            $model->ingreso_mensual=number_format($model->ingreso_mensual,2,",", ".");
	        //$insti=CHtml::listData(Institucion::model()->findAll(), 'id_institucion', 'nb_institucion');

            $profesion=CHtml::listData(Profesion::model()->findAll(array('order'=>'nombre ASC'),'estatus != False'), 'profesion_id', 'nombre');
            $estadocivil=CHtml::listData(EstadoCivil::model()->findAll(array('order'=>'nombre ASC'),'estatus != False'), 'estado_civil_id', 'nombre');
            $pais=CHtml::listData(Pais::model()->findAll(array('order'=>'nombre ASC'),'estatus != False'), 'pais_id', 'nombre');
            $estado=CHtml::listData(Estado::model()->findAll(array('order'=>'nombre ASC'),'estatus != false'), 'estado_id', 'nombre');
            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

		if(isset($_POST['Direccion']) && isset($_POST['GenPersona']))
		{
			$modeldireccion->attributes=$_POST['Direccion'];
			$model->attributes=$_POST['GenPersona'];

			$modeldireccion->usuario_id_cargo_en_sistema=yii::app()->user->id;
				$transaction=Yii::app()->db->beginTransaction();
				try {
					if($modeldireccion->validate()){
						if($modeldireccion->save()){
                                                        if($model->imagen==null || $model->imagen==""){ $model->imagen=$text_imagen; }
															$model->direccion_id=$modeldireccion->direccion_id;
	                                                        $model->ingreso_mensual=str_replace('.', '', $model->ingreso_mensual);
	                                                        $model->ingreso_mensual=str_replace(',', '.', $model->ingreso_mensual);                                                        //IMAGEN DEL USUARIO
	                                                        $uploadedFile=CUploadedFile::getInstance($model,'imagen');
	                                                        $tipo_archivo=substr($uploadedFile, -4);
	                                                        $fileName = $model->cedula.$tipo_archivo;   // random number + file name or puedes usar: $fileName=$uploadedFile->getName();
	                                                        if(!empty($uploadedFile))  // check if uploaded file is set or not
	                                                        {
                                                            $uploadedFile->saveAs(YiiBase::getPathOfAlias("webroot").Yii::app()->params['RutaUsuarios'].$fileName);  // image will uplode to rootDirectory/banner/
                                                            $model->imagen= $fileName;
                                                        }

							if($model->validate()){
								if($model->save()){

                                        $text='El sistama de gestion, consultor sico a recibido con exito la actualización de sus datos...';
                                        $mensaje= FuncionesGlobales::textcorreo($text,$model);
                                        $correo=FuncionesGlobales::correo($model->correo,$model->apellido1,$mensaje);
                                            if($correo==false){ throw new CHttpException(500,'EL servidor de correo esta inhabilitado temporalmente, nuestros analistas ya trabajan para solventar el problema.',500); }

									$transaction->commit();
									$this->redirect(array('/usuario/User/view','id'=>$model->usuario[0]['usuario_id']));
								}
							}
						}
					}
				}
				catch(Exception $e) {
				                  $transaction->rollBack();
				                  throw new CHttpException($e->getCode(),$e->getMessage());
				}
				$model->validate();
		}
		$this->render('update',array('model'=>$model,'pais'=>$pais,'estado'=>$estado,'modeldireccion'=>$modeldireccion,'profesion'=>$profesion,'estadocivil'=>$estadocivil,'insti'=>$insti));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('GenPersona');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layout ='//layouts/partial';
		$model=new GenPersona('search');
		$model->unsetAttributes();  // clear any default values
        $insti=CHtml::listData(Institucion::model()->findAll(), 'id_institucion', 'nb_institucion');

		if(isset($_GET['GenPersona']))
			$model->attributes=$_GET['GenPersona'];

		$this->render('admin',array('model'=>$model,'insti'=>$insti));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return GenPersona the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=GenPersona::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param GenPersona $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gen-persona-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    public function actionCarpetafisica($id)
    {
        // inicio del codigo para activar y desactivar estatus

                $model=$this->loadModel($id);

                $transaction=Yii::app()->db->beginTransaction();
                try {

                    if($model->carpeta_fisico==false){
                                
                                $var = GenPersona::model()->findByPk($model->persona_id);
                                $var->carpeta_fisico = true;
                                $var->update();
                               
                                $transaction->commit();

                                if(!isset($_GET['ajax'])){ $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin')); }

                    } else if ($model->carpeta_fisico==true){
                                
                                $var = GenPersona::model()->findByPk($model->persona_id);
                                $var->carpeta_fisico = false;
                                $var->update();
                                                               
                                $transaction->commit();
                                if(!isset($_GET['ajax'])){  $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin')); }
                           }
                }
                catch(Exception $e) {
                                  $transaction->rollBack();
                                  throw new CHttpException($e->getCode(),$e->getMessage());
                }
        // fin del codigo para activar y desactivar estatus
    }


}
