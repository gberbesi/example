<?php

/**
 * This is the model class for table "persona_recaudo".
 *
 * The followings are the available columns in table 'persona_recaudo':
 * @property integer $persona_id
 * @property integer $recaudo_id
 * @property string $fe_registro
 */
class PersonaRecaudo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PersonaRecaudo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'persona_recaudo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('persona_id, recaudo_id', 'required'),
			array('persona_id, recaudo_id', 'numerical', 'integerOnly'=>true),
			array('fe_registro', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('persona_id, recaudo_id, fe_registro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'persona_id' => 'Persona',
			'recaudo_id' => 'Recaudo',
			'fe_registro' => 'Fe Registro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('persona_id',$this->persona_id);
		$criteria->compare('recaudo_id',$this->recaudo_id);
		$criteria->compare('fe_registro',$this->fe_registro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}