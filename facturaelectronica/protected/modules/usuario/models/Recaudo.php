<?php

/**
 * This is the model class for table "recaudo".
 *
 * The followings are the available columns in table 'recaudo':
 * @property integer $recaudo_id
 * @property string $nombre
 * @property boolean $obligatorio
 * @property boolean $st_recaudo
 */
class Recaudo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Recaudo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'recaudo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, obligatorio, st_recaudo', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('recaudo_id, nombre, obligatorio, st_recaudo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'recaudo_id' => 'Recaudo',
			'nombre' => 'Nombre',
			'obligatorio' => 'Obligatorio',
			'st_recaudo' => 'St Recaudo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('recaudo_id',$this->recaudo_id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('obligatorio',$this->obligatorio);
		$criteria->compare('st_recaudo',$this->st_recaudo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}