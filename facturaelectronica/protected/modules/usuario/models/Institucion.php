<?php

/**
 * This is the model class for table "archivos_0800.institucion".
 *
 * The followings are the available columns in table 'archivos_0800.institucion':
 * @property integer $id_institucion
 * @property string $nb_institucion
 */
class Institucion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Institucion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'archivos_0800.institucion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nb_institucion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_institucion, nb_institucion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_institucion' => 'Id Institucion',
			'nb_institucion' => 'Institución',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_institucion',$this->id_institucion);
		$criteria->compare('nb_institucion',$this->nb_institucion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}