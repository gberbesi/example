<?php

/**
 * This is the model class for table "onidex".
 *
 * The followings are the available columns in table 'onidex':
 * @property string $nac
 * @property integer $cedula
 * @property string $nacionalidad_acr
 * @property string $nacionalidad
 * @property string $nombre1
 * @property string $nombre2
 * @property string $apellido1
 * @property string $apellido2
 * @property string $fecha_nac
 * @property string $codobjecion
 * @property string $codoficina
 * @property string $edo_civil
 * @property string $naturalizado
 * @property string $sexo
 * @property integer $id
 *
 * The followings are the available model relations:
 * @property EdoCivil $edoCivil
 */
class Onidex extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Onidex the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->onidex;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'onidex';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cedula', 'numerical', 'integerOnly'=>true),
			array('nac, nacionalidad_acr, nacionalidad, nombre1, nombre2, apellido1, apellido2, fecha_nac, codobjecion, codoficina, edo_civil, naturalizado, sexo', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nac, cedula, nacionalidad_acr, nacionalidad, nombre1, nombre2, apellido1, apellido2, fecha_nac, codobjecion, codoficina, edo_civil, naturalizado, sexo, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'edoCivil' => array(self::BELONGS_TO, 'EdoCivil', 'edo_civil'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nac' => 'Nac',
			'cedula' => 'Cedula',
			'nacionalidad_acr' => 'Nacionalidad Acr',
			'nacionalidad' => 'Nacionalidad',
			'nombre1' => 'Nombre1',
			'nombre2' => 'Nombre2',
			'apellido1' => 'Apellido1',
			'apellido2' => 'Apellido2',
			'fecha_nac' => 'Fecha Nac',
			'codobjecion' => 'Codobjecion',
			'codoficina' => 'Codoficina',
			'edo_civil' => 'Edo Civil',
			'naturalizado' => 'Naturalizado',
			'sexo' => 'Sexo',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nac',$this->nac,true);
		$criteria->compare('cedula',$this->cedula);
		$criteria->compare('nacionalidad_acr',$this->nacionalidad_acr,true);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('fecha_nac',$this->fecha_nac,true);
		$criteria->compare('codobjecion',$this->codobjecion,true);
		$criteria->compare('codoficina',$this->codoficina,true);
		$criteria->compare('edo_civil',$this->edo_civil,true);
		$criteria->compare('naturalizado',$this->naturalizado,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}