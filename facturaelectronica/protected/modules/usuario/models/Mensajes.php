<?php

/**
 * This is the model class for table "mensajes".
 *
 * The followings are the available columns in table 'mensajes':
 * @property integer $id_mensajes
 * @property string $fecha
 * @property string $mensaje
 * @property string $sistema
 * @property string $numero
 */
class Mensajes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Mensajes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->sms_db;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mensajes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('sistema', 'length', 'max'=>100),
			array('fecha, mensaje, numero', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_mensajes, fecha, mensaje, sistema, numero', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_mensajes' => 'Id Mensajes',
			'fecha' => 'Fecha',
			'mensaje' => 'Mensaje',
			'sistema' => 'Sistema',
			'numero' => 'Numero',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_mensajes',$this->id_mensajes);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('mensaje',$this->mensaje,true);
		$criteria->compare('sistema',$this->sistema,true);
		$criteria->compare('numero',$this->numero,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}