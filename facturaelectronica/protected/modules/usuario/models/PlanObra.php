<?php

/**
 * This is the model class for table "plan_obra".
 *
 * The followings are the available columns in table 'plan_obra':
 * @property integer $idplan_obra
 * @property string $plan_obra
 * @property integer $operador
 * @property string $fecha
 * @property string $hora
 * @property integer $eliminado
 */
class PlanObra extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PlanObra the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->sico;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'plan_obra';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('operador, eliminado', 'numerical', 'integerOnly'=>true),
			array('plan_obra', 'length', 'max'=>100),
			array('fecha, hora', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idplan_obra, plan_obra, operador, fecha, hora, eliminado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idplan_obra' => 'Idplan Obra',
			'plan_obra' => 'Plan Obra',
			'operador' => 'Operador',
			'fecha' => 'Fecha',
			'hora' => 'Hora',
			'eliminado' => 'Eliminado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idplan_obra',$this->idplan_obra);
		$criteria->compare('plan_obra',$this->plan_obra,true);
		$criteria->compare('operador',$this->operador);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('hora',$this->hora,true);
		$criteria->compare('eliminado',$this->eliminado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}