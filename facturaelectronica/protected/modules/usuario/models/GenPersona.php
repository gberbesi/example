<?php

/**
 * This is the model class for table "gen_persona".
 *
 * The followings are the available columns in table 'gen_persona':
 * @property string $persona_id
 * @property string $nacionalidad
 * @property string $cedula
 * @property string $nombre1
 * @property string $nombre2
 * @property string $apellido1
 * @property string $apellido2
 * @property string $fecha_nacimiento
 * @property string $sexo
 * @property string $telefono1
 * @property string $telefono2
 * @property string $telefono3
 * @property string $correo
 * @property integer $pais_id
 * @property integer $estado_civil_id
 * @property integer $profesion_id
 * @property string $usuario_id_aud
 * @property string $direccion_id
 * @property string $imagen
 * @property string $ocupacion
 * @property string $ingreso_mensual
 * @property boolean $carga_finalizada	
 * @property boolean $carpeta_fisico	
 */
class GenPersona extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GenPersona the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gen_persona';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre1', 'required'),
			array('nacionalidad, sexo', 'length', 'max'=>1),
			array('ingreso_mensual', 'length', 'max'=>10),
			array('nombre2, apellido2, fecha_nacimiento, telefono1, telefono2, telefono3, usuario_id_aud, correo, direccion_id, imagen, ocupacion, carga_finalizada, carpeta_fisico, institucion_id,nacionalidad,apellido1,cedula', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('persona_id, nacionalidad, cedula, nombre1, nombre2, apellido1, apellido2, fecha_nacimiento, sexo, telefono1, telefono2, telefono3, correo, pais_id, estado_civil_id, profesion_id, usuario_id_aud, direccion_id, imagen, ocupacion, ingreso_mensual, institucion_id,carpeta_fisico, carga_finalizada', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pais' => array(self::BELONGS_TO, 'Pais', 'pais_id'),
			'estadocivil' => array(self::BELONGS_TO, 'EstadoCivil', 'estado_civil_id'),                    
			'profesion' => array(self::BELONGS_TO, 'Profesion', 'profesion_id'),
			'direccion' => array(self::BELONGS_TO, 'Direccion', 'direccion_id'),
			'usuario' => array(self::HAS_MANY, 'Usuario', 'persona_id'),
			'insti' => array(self::BELONGS_TO, 'Institucion', 'institucion_id'),

                );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'persona_id' => 'ID',
			'nacionalidad' => 'Nacionalidad',
			'cedula' => 'Cédula',
			'nombre1' => 'Primer nombre',
			'nombre2' => 'Segundo nombre',
			'apellido1' => 'Primer apellido',
			'apellido2' => 'Segundo apellido',
			'fecha_nacimiento' => 'Fecha de nacimiento',
			'sexo' => 'Genero',
			'telefono1' => 'Primer telefono',
			'telefono2' => 'Segundo telefono',
			'telefono3' => 'Tercer telefono',
			'correo' => 'Correo',
			'pais_id' => 'Pais',
			'estado_civil_id' => 'Estado Civil',
			'profesion_id' => 'Profesión',
			'usuario_id_aud' => 'Usuario Id Aud',
			'direccion_id' => 'Dirección',
			'imagen' => 'Imagen',
			'ocupacion' => 'Ocupación',
			'ingreso_mensual' => 'Ingreso Mensual',
			'carpeta_fisico' => '¿Carpeta consignada?',
			'carga_finalizada' => '¿Documentos adjuntados?',
			'institucion_id' => 'Institución',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('persona_id',$this->persona_id,false);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('cedula',$this->cedula,true);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('telefono1',$this->telefono1,true);
		$criteria->compare('telefono2',$this->telefono2,true);
		$criteria->compare('telefono3',$this->telefono3,true);
		$criteria->compare('correo',$this->correo,true);
		$criteria->compare('pais_id',$this->pais_id);
		$criteria->compare('estado_civil_id',$this->estado_civil_id);
		$criteria->compare('profesion_id',$this->profesion_id);
		$criteria->compare('usuario_id_aud',$this->usuario_id_aud,true);
		$criteria->compare('direccion_id',$this->direccion_id,true);
		$criteria->compare('imagen',$this->imagen,true);
		$criteria->compare('ocupacion',$this->ocupacion,true);
		$criteria->compare('ingreso_mensual',$this->ingreso_mensual,true);
		$criteria->compare('carga_finalizada',$this->carga_finalizada,false);
		$criteria->compare('carpeta_fisico',$this->carpeta_fisico,false);
		$criteria->compare('institucion_id',$this->institucion_id,false);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}