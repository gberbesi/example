<?php

/**
 * This is the model class for table "gen_persona".
 *
 * The followings are the available columns in table 'gen_persona':
 * @property string $persona_id
 * @property string $nacionalidad
 * @property string $cedula
 * @property string $nombre1
 * @property string $nombre2
 * @property string $apellido1
 * @property string $apellido2
 * @property string $fecha_nacimiento
 * @property string $sexo
 * @property string $telefono1
 * @property string $telefono2
 * @property string $telefono3
 * @property string $correo
 * @property integer $pais_id
 * @property integer $estado_civil_id
 * @property integer $profesion_id
 * @property string $usuario_id_aud
 * @property string $
 * @property string $imagen
 * @property string $ocupacion
 * @property string $ingreso_mensual
 */
class GenPersona extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GenPersona the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gen_persona';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nacionalidad, cedula, nombre1, apellido1, telefono1, correo', 'required'),
			array('pais_id, estado_civil_id, profesion_id', 'numerical', 'integerOnly'=>true),
			array('nacionalidad, sexo', 'length', 'max'=>1),
			array('ingreso_mensual', 'length', 'max'=>10),
			array('nombre2, apellido2, fecha_nacimiento, telefono2, telefono3, usuario_id_aud, , imagen, ocupacion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('persona_id, nacionalidad, cedula, nombre1, nombre2, apellido1, apellido2, fecha_nacimiento, sexo, telefono1, telefono2, telefono3, correo, pais_id, estado_civil_id, profesion_id, usuario_id_aud, , imagen, ocupacion, ingreso_mensual', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pais' => array(self::BELONGS_TO, 'Pais', 'pais_id'),
			'estadocivil' => array(self::BELONGS_TO, 'EstadoCivil', 'estado_civil_id'),                    
			'profesion' => array(self::BELONGS_TO, 'Profesion', 'profesion_id'),
			'direccion' => array(self::BELONGS_TO, 'Direccion', ''),
			'usuario' => array(self::HAS_MANY, 'Usuario', 'persona_id'),
                );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'persona_id' => 'ID',
			'nacionalidad' => 'Nacionalidad',
			'cedula' => 'Cedula',
			'nombre1' => 'Nombre1',
			'nombre2' => 'Nombre2',
			'apellido1' => 'Apellido1',
			'apellido2' => 'Apellido2',
			'fecha_nacimiento' => 'Fecha Nacimiento',
			'sexo' => 'Sexo',
			'telefono1' => 'Telefono1',
			'telefono2' => 'Telefono2',
			'telefono3' => 'Telefono3',
			'correo' => 'Correo',
			'pais_id' => 'Pais',
			'estado_civil_id' => 'Estado Civil',
			'profesion_id' => 'Profesion',
			'usuario_id_aud' => 'Usuario Id Aud',
			'' => 'Direccion',
			'imagen' => 'Imagen',
			'ocupacion' => 'Ocupacion',
			'ingreso_mensual' => 'Ingreso Mensual',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('persona_id',$this->persona_id);
		$criteria->compare('nacionalidad',$this->nacionalidad);
		$criteria->compare('cedula',$this->cedula,true);
		$criteria->compare('nombre1',$this->nombre1);
		$criteria->compare('nombre2',$this->nombre2);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('telefono1',$this->telefono1,true);
		$criteria->compare('telefono2',$this->telefono2,true);
		$criteria->compare('telefono3',$this->telefono3,true);
		$criteria->compare('correo',$this->correo,true);
		$criteria->compare('pais_id',$this->pais_id);
		$criteria->compare('estado_civil_id',$this->estado_civil_id);
		$criteria->compare('profesion_id',$this->profesion_id);
		$criteria->compare('usuario_id_aud',$this->usuario_id_aud,true);
	
		$criteria->compare('imagen',$this->imagen,true);
		$criteria->compare('ocupacion',$this->ocupacion,true);
		$criteria->compare('ingreso_mensual',$this->ingreso_mensual,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}