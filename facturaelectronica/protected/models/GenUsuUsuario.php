<?php

/**
 * This is the model class for table "usu_usuario".
 *
 * The followings are the available columns in table 'usu_usuario':
 * @property integer $usuario_id
 * @property string $nb_usuario
 * @property string $contrasena
 * @property string $persona_id
 * @property boolean $estatus
 *
 * The followings are the available model relations:
 * @property AudAuditoria[] $audAuditorias
 * @property UsuRol[] $usuRols
 * @property PerPersona $persona
 */
class GenUsuUsuario extends CActiveRecord
{
	public $diaActual;
	/**
	 * Returns the static model of the specified AR class.
	 * @return UsuUsuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

			
			array('usuario, clave, cedula, nombre1, apellido1, ente_id,correo,telefono,nacionalidad', 'required', 'on'=>'reg'),
			array('clave', 'length','min'=>4, 'on'=>'reg'),
			array('nombre1, apellido1,nombre2, apellido2', 'match', 'pattern'=>'/^([a-zA-ZÁÉÍÓÚáéíóúñÑ\s])+$/', 'message' => 'Este campo puede contener solamente letras', 'on'=>'reg'),
			array('usuario', 'usuarioUnicoAdmin', 'on'=>'reg'),
			array('cedula', 'cedulaUnicoAdmin', 'on'=>'reg'),	
			array('correo', 'correoUnicoAdmin', 'on'=>'reg'),			
			array('bo_activo', 'safe', 'on'=>'reg'),
			array('cedula,telefono', 'numerical', 'integerOnly'=>true, 'on'=>'reg'),
			array('cedula', 'numerical', 'integerOnly'=>true, 'min'=>1, 'on'=>'reg'),
			array('telefono', 'length', 'min'=>11, 'on'=>'reg'),
			array('correo', 'email', 'message' => 'No es un correo valido','on'=>'reg'),

			array('usuario, clave', 'required' , 'on'=>'login'),
			array('clave', 'autenticar','on'=>'login'),
			array('recordarme', 'safe','on'=>'login'),

			array('clave', 'required','message' => 'Debe ingresar una contraseña valida','on'=>'cambio'),
			array('nueva', 'required','message' => 'Debe escribir una contraseña valida','on'=>'cambio'),
			array('repetirnueva', 'required','message' => 'Debe escribir una contraseña valida','on'=>'cambio'),
			array('nueva', 'compare', 'compareAttribute'=>'repetirnueva','message' => 'Las contraseñas nuevas no son iguales', 'on'=>'cambio'),
			array('repetirnueva', 'compare', 'compareAttribute'=>'nueva','message' => 'Las contraseñas nuevas no son iguales', 'on'=>'cambio'),
			array('nueva', 'length','min'=>4,'on'=>'cambio'),
			

			
			array('tx_pregunta_secreta, tx_respuesta_pregunta, tx_cod_activacion, rol_id, usuario_activo_sistema, bo_temporal, estatus, usuario_id_aud, persona_id, correo, telefono,nombre2, apellido2', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('usuario_id, usuario, clave, tx_pregunta_secreta, tx_respuesta_pregunta, tx_cod_activacion, rol_id, usuario_activo_sistema, bo_temporal, estatus, usuario_id_aud, persona_id, banco_id,rol', 'safe', 'on'=>'search'),
		);
	}
	
	public function usuarioUnico($attribute,$params)	{
		if(!$this->hasErrors())	{
			
			if($user = Usuario::model()->find('usuario ILIKE :usuario AND estatus=true',array(':usuario'=>$this->usuario))) {
				$this->addError('usuario','Usuario ya esta registrado.');
			}
				
		}
	}
	
	public function usuarioUnicoAdmin($attribute,$params)	{
		if(!$this->hasErrors('usuario'))	{
			if($user = Usuario::model()->find('usuario ILIKE :usuario AND estatus=true',array(':usuario'=>$this->usuario))) {
				if($this->isNewRecord) {
					$this->addError('usuario','Usuario ya esta registrado.');
				} else if($user->usuario_id!=$this->usuario_id){
					$this->addError('usuario','Usuario ya esta registrad.');
				}
			}
				
		}
	}
	
	public function cedulaUnicoAdmin($attribute,$params)	{
		if(!$this->hasErrors('cedula'))	{
			if($user = Usuario::model()->with('persona')->find('nu_cedula =:nu_cedula AND estatus=true and st_persona=true',array(':nu_cedula'=>$this->cedula))) {
				if($this->isNewRecord) {
					$this->addError('cedula','Persona ya posee usuario.');
				} else if($user->usuario_id!=$this->usuario_id){
					$this->addError('cedula','Persona ya posee usuario.');
				}
			}
		}
	}
	
	public function correoUnicoAdmin($attribute,$params)	{
		if(!$this->hasErrors('correo')&&!$this->hasErrors('cedula'))	{
		
			if($user = Usuario::model()->with('persona')->find('tx_correo =:tx_correo AND estatus=true and st_persona=true',array(':tx_correo'=>$this->correo))) {
				if($this->isNewRecord) {
					$this->addError('correo','Correo ya esta en uso por otro usuario.');
				} else if($user->persona->nu_cedula!=$this->cedula){
					$this->addError('correo','Correo ya esta en uso por otro usuario.');
				}
			}
		}
	}	
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'rol' => array(self::BELONGS_TO, 'Rol', 'rol_id'),
			'authitems' => array(self::MANY_MANY, 'Authitem', 'authassignment(userid, itemname)'),
			'persona' => array(self::BELONGS_TO, 'GenGenPersona', 'persona_id'),
			'ente' => array(self::BELONGS_TO, 'Ente', 'ente_id'),
			'usuarioinstitucions' => array(self::HAS_MANY, 'UsuarioInstitucion', 'usuario_id','condition'=>'estatus_institucion=true'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'usuario_id' => 'Id Usuario',
			'usuario' => 'Usuario',
			'clave' => 'Contraseña',
			'tx_pregunta_secreta' => 'Pregunta Secreta',
			'tx_respuesta_pregunta' => 'Respuesta de Pregunta Secreta',
			'tx_cod_activacion' => 'Tx Cod Activacion',
			'rol_id' => 'Rol',
			'usuario_activo_sistema' => 'Activo',
			'bo_temporal' => 'Bo Temporal',
			'estatus' => 'St Usuario',
			'usuario_id_aud' => 'Usuario Id Aud',
			'persona_id' => 'Persona',
			'repetirclave' => 'Repetir Contraseña',
			'nueva' => 'Nueva Contraseña',
			'repetirnueva' => 'Repetir Contraseña Nueva',
			'ente_id' => 'Sede',
			'correo' => 'Correo',
			'telefono' => 'Teléfono',
			'nombre1' => 'Primer Nombre',
			'nombre2' => 'Segundo Nombre',
			'apellido1' => 'Primer Apellido',
			'apellido2' => 'Segundo Apellido',

		);
	}

	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->with=array('authitems');
		$criteria->together=true;

		$criteria->compare('usuario_id',$this->usuario_id,true);
		$criteria->compare('upper(usuario)',strtoupper($this->usuario),true);
		$criteria->compare('clave',$this->clave,true);
		
		$criteria->compare('itemname',$this->rol,false);
		
		$criteria->compare('usuario_activo_sistema',$this->usuario_activo_sistema);

		$criteria->compare('estatus',$this->estatus);
		$criteria->compare('usuario_id_aud',$this->usuario_id_aud,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
 			'sort'=>array(
            	'defaultOrder'=>'usuario_id ASC',
        	),			
		));
	}	
	
	
	public function  comparar($attribute,$params){
	
		if ($this->hasErrors('fe_expiracion')){
			$this->fe_expiracion=NULL;
		}
		if (!$this->hasErrors()) {
			if (strtotime($this->fe_expiracion)<strtotime($this->diaActual)){
				$this->addError('fe_expiracion','"Fecha de expiracion" debe ser mayor a la fecha actual "'.$this->diaActual.'".');
			}
		}
	
	}	
	public function  comparar2($attribute,$params){
	
		if ($this->hasErrors('fech_expiracion')){
			$this->fech_expiracion=NULL;
		}
		if (!$this->hasErrors()) {
			if (strtotime($this->fech_expiracion)<strtotime($this->diaActual)){
				$this->addError('fech_expiracion','"Fecha de expiracion" debe ser mayor a la fecha actual "'.$this->diaActual.'".');
			}
		}
	
	}
	
	
	public function validarUsuarioSesion(){
			Yii::import('application.modules.usuario.models.AuthAssignment',true);
			if (!$this){
				Yii::app()->user->logout();
				return false;
			}
			else if ($this&&($this->estatus==0 || $this->usuario_activo_sistema==0)){
				Yii::app()->user->logout();
				return false;				
			}
			/*else if ($this){
				$uValido=true;
				$uValidoPlan=true;
				if($this->fe_expiracion!=NULL){
					$this->diaActual=date('d-m-Y H:i');
					$this->scenario='confe';
					$this->fe_expiracion=date('d-m-Y H:i',strtotime($this->fe_expiracion));
					$uValido=$this->validate(array('fe_expiracion'));
				}
				if($this->fech_expiracion!=NULL){
					$this->diaActual=date('d-m-Y H:i');
					$this->scenario='confe';
					$this->fech_expiracion=date('d-m-Y H:i',strtotime($this->fech_expiracion));
					$uValidoPlan=$this->validate(array('fech_expiracion'));
				}

				$user3=GenUsuUsuario::model()->findByPk(Yii::app()->user->id);
				if(!$uValido ||!$uValidoPlan ){
					if (!$uValido){
						$user3->estatus=0;
					}
				
					if (!$uValidoPlan){
						$user3->estatus_plan=0;
					}
						
					$user3->usu_aud_id=Yii::app()->user->id;
					$user3->save();
				}

				if(!$uValidoPlan){
					$modelAuthAssignment=AuthAssignment::model()->findAll('itemname ilike \'Consignacion\' and userid=:id',array(':id'=>(string)$this->usuario_id));
					if($modelAuthAssignment){
						foreach ($modelAuthAssignment as $key =>$value){
							$value->delete();
						}
					}
				}
				if(!$uValido){
					$user= true;
					$modelAuthAssignment=AuthAssignment::model()->findAll('itemname not ilike \'Consignacion\' and userid=:id',array(':id'=>(string)$this->usuario_id));
					if($modelAuthAssignment){
						foreach ($modelAuthAssignment as $key =>$value){
							$value->delete();
						}
					}
				}
				if(!$uValido &&!$uValidoPlan){
					Yii::app()->user->logout();
					return false;}
				
			}*/
		return true;
	} 
	
	//VALIDAR SI USUARIO LOGUEADO PUEDE MODIFICAR LA DATA SEGUN INSTITUCION
	public static function  validarUsuarioData($id_institucion){
			if (!in_array($id_institucion, Yii::app()->getSession()->get('usuario_institucion'))) {
					throw new CHttpException(403,'Usted no se encuentra autorizado a realizar esta acción.');
			}
	}
	
}