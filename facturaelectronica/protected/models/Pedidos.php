<?php

/**
 * This is the model class for table "pedidos".
 *
 * The followings are the available columns in table 'pedidos':
 * @property integer $id_pedido
 * @property string $id_medida
 * @property string $id_producto
 * @property string $fecha
 * @property string $id_usuario
 * @property double $cantidad
 *
 * The followings are the available model relations:
 * @property Producto $idProducto
 * @property Medida $idMedida
 * @property GenPersona $idUsuario
 */
class Pedidos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $cedula;

	public $precio;
	public function tableName()
	{
		return 'pedidos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cantidad', 'numerical'),
			array('id_medida, id_producto, fecha, id_usuario,cedula,total,precio', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_pedido, id_medida, id_producto, fecha, id_usuario, cantidad,cedula,total,precio', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idProducto' => array(self::BELONGS_TO, 'Producto', 'id_producto'),
			'idMedida' => array(self::BELONGS_TO, 'Medida', 'id_medida'),
			'idUsuario' => array(self::BELONGS_TO, 'GenPersona', 'id_usuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_pedido' => 'Pedido',
			'id_medida' => 'Unidad de Medida',
			'id_producto' => 'Producto',
			'fecha' => 'Fecha',
			'id_usuario' => 'Nombre Cliente',
			'cantidad' => 'Cantidad',
			'precio'=>'Precio',
			'total'=>'Total',
			'cedula'=>'Cédula',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_pedido',$this->id_pedido);
		$criteria->compare('id_medida',$this->id_medida,true);
		$criteria->compare('id_producto',$this->id_producto,true);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('id_usuario',$this->id_usuario,true);
		$criteria->compare('cantidad',$this->cantidad);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pedidos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
