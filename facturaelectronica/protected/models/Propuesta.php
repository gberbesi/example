<?php

/**
 * This is the model class for table "propuesta".
 *
 * The followings are the available columns in table 'propuesta':
 * @property integer $id
 * @property integer $tipo_proyecto_id
 * @property string $empresa_ejecutora_id
 * @property integer $estatus_ejecucion_id
 * @property integer $estatus_propuesta_id
 * @property string $motivo_paralizado_id
 * @property string $fe_paralizado
 * @property string $fe_inicio
 * @property string $fe_fin
 * @property string $otro_ente_organismo
 * @property integer $linea_accion_id
 * @property integer $estado_id
 * @property integer $municipio_id
 * @property integer $parroquia_id
 * @property string $sector
 * @property string $calle
 * @property double $monto_inversion
 * @property double $monto_materiales
 * @property double $parcela
 * @property integer $viviendas
 * @property integer $fases
 * @property integer $creado_por
 * @property string $latitud
 * @property string $cemento
 * @property string $cabilla
 * @property string $agregado
 * @property string $electricidad
 * @property string $urbanismo
 * @property double $cant_agregado
 * @property string $agua
 * @property double $cant_cemento
 * @property string $longitud
 * @property double $cant_cabilla
 * @property string $agua_servida
 * @property string $fe_registro
 * @property string $fuente_financiamiento
 * @property double $monto_por_ejecutar
 * @property double $monto_faltante_materiales
 * @property double $monto_ejecutado
 * @property double $monto_aprobado
 * @property integer $centro_acopio
 * @property integer $cant_fam_benef
 * @property integer $cant_brigadas
 * @property integer $cant_personas
 * @property integer $cant_brigadistas
 * @property string $rif_empresa_ejecutora
 * @property string $convenio_internacional
 * @property string $suvi
 * @property integer $pais_id
 * @property integer $clasificacion_id
 * @property string $fuente_financiamiento_id
 * @property string $descripcion
 * @property string $nombre
 * @property integer $tipo_zona_id
 * @property double $lon
 * @property double $lat
 * @property string $cod_sap
 * @property integer $programa_id
 * @property integer $titulo_terreno_id
 * @property integer $transf_terreno
 * @property integer $cond_transf_id
 * @property string $lindero_norte
 * @property string $lindero_sur
 * @property string $lindero_este
 * @property string $lindero_oeste
 * @property double $costo_vivienda
 * @property string $organismo_titular_terreno
 * @property double $metraje_vivienda
 * @property double $ha_area_total_terreno
 * @property string $centro_electoral
 * @property integer $estatus_construpatria
 * @property boolean $nuevo_pedido
 * @property boolean $migrado_sap
 * @property boolean $modificado_osv
 * @property integer $estatus_anteproyecto
 * @property integer $estatus_bancoproyecto
 * @property string $nro_id_terreno
 * @property string $ced_catastro
 */
class Propuesta extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'propuesta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipo_proyecto_id, estatus_ejecucion_id, estatus_propuesta_id, linea_accion_id, estado_id, municipio_id, parroquia_id, viviendas, fases, creado_por, centro_acopio, cant_fam_benef, cant_brigadas, cant_personas, cant_brigadistas, pais_id, clasificacion_id, tipo_zona_id, programa_id, titulo_terreno_id, transf_terreno, cond_transf_id, estatus_construpatria, estatus_anteproyecto, estatus_bancoproyecto', 'numerical', 'integerOnly'=>true),
			array('monto_inversion, monto_materiales, parcela, cant_agregado, cant_cemento, cant_cabilla, monto_por_ejecutar, monto_faltante_materiales, monto_ejecutado, monto_aprobado, lon, lat, costo_vivienda, metraje_vivienda, ha_area_total_terreno', 'numerical'),
			array('empresa_ejecutora_id, motivo_paralizado_id, otro_ente_organismo, sector, latitud, cemento, cabilla, agregado, electricidad, urbanismo, agua, longitud, agua_servida, fuente_financiamiento, rif_empresa_ejecutora, fuente_financiamiento_id, nombre, cod_sap, lindero_norte, lindero_sur, lindero_este, lindero_oeste, organismo_titular_terreno', 'length', 'max'=>512),
			array('convenio_internacional, suvi', 'length', 'max'=>1),
			array('descripcion', 'length', 'max'=>5120),
			array('centro_electoral', 'length', 'max'=>10),
			array('fe_paralizado, fe_inicio, fe_fin, calle, fe_registro, nuevo_pedido, migrado_sap, modificado_osv, nro_id_terreno, ced_catastro', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tipo_proyecto_id, empresa_ejecutora_id, estatus_ejecucion_id, estatus_propuesta_id, motivo_paralizado_id, fe_paralizado, fe_inicio, fe_fin, otro_ente_organismo, linea_accion_id, estado_id, municipio_id, parroquia_id, sector, calle, monto_inversion, monto_materiales, parcela, viviendas, fases, creado_por, latitud, cemento, cabilla, agregado, electricidad, urbanismo, cant_agregado, agua, cant_cemento, longitud, cant_cabilla, agua_servida, fe_registro, fuente_financiamiento, monto_por_ejecutar, monto_faltante_materiales, monto_ejecutado, monto_aprobado, centro_acopio, cant_fam_benef, cant_brigadas, cant_personas, cant_brigadistas, rif_empresa_ejecutora, convenio_internacional, suvi, pais_id, clasificacion_id, fuente_financiamiento_id, descripcion, nombre, tipo_zona_id, lon, lat, cod_sap, programa_id, titulo_terreno_id, transf_terreno, cond_transf_id, lindero_norte, lindero_sur, lindero_este, lindero_oeste, costo_vivienda, organismo_titular_terreno, metraje_vivienda, ha_area_total_terreno, centro_electoral, estatus_construpatria, nuevo_pedido, migrado_sap, modificado_osv, estatus_anteproyecto, estatus_bancoproyecto, nro_id_terreno, ced_catastro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tipo_proyecto_id' => 'Tipo Proyecto',
			'empresa_ejecutora_id' => 'Empresa Ejecutora',
			'estatus_ejecucion_id' => 'Estatus Ejecucion',
			'estatus_propuesta_id' => 'Estatus Propuesta',
			'motivo_paralizado_id' => 'Motivo Paralizado',
			'fe_paralizado' => 'Fe Paralizado',
			'fe_inicio' => 'Fe Inicio',
			'fe_fin' => 'Fe Fin',
			'otro_ente_organismo' => 'Otro Ente Organismo',
			'linea_accion_id' => 'Linea Accion',
			'estado_id' => 'Estado',
			'municipio_id' => 'Municipio',
			'parroquia_id' => 'Parroquia',
			'sector' => 'Sector',
			'calle' => 'Calle',
			'monto_inversion' => 'Monto Inversion',
			'monto_materiales' => 'Monto Materiales',
			'parcela' => 'Parcela',
			'viviendas' => 'Viviendas',
			'fases' => 'Fases',
			'creado_por' => 'Creado Por',
			'latitud' => 'Latitud',
			'cemento' => 'Cemento',
			'cabilla' => 'Cabilla',
			'agregado' => 'Agregado',
			'electricidad' => 'Electricidad',
			'urbanismo' => 'Urbanismo',
			'cant_agregado' => 'Cant Agregado',
			'agua' => 'Agua',
			'cant_cemento' => 'Cant Cemento',
			'longitud' => 'Longitud',
			'cant_cabilla' => 'Cant Cabilla',
			'agua_servida' => 'Agua Servida',
			'fe_registro' => 'Fe Registro',
			'fuente_financiamiento' => 'Fuente Financiamiento',
			'monto_por_ejecutar' => 'Monto Por Ejecutar',
			'monto_faltante_materiales' => 'Monto Faltante Materiales',
			'monto_ejecutado' => 'Monto Ejecutado',
			'monto_aprobado' => 'Monto Aprobado',
			'centro_acopio' => 'Centro Acopio',
			'cant_fam_benef' => 'Cant Fam Benef',
			'cant_brigadas' => 'Cant Brigadas',
			'cant_personas' => 'Cant Personas',
			'cant_brigadistas' => 'Cant Brigadistas',
			'rif_empresa_ejecutora' => 'Rif Empresa Ejecutora',
			'convenio_internacional' => 'Convenio Internacional',
			'suvi' => 'Suvi',
			'pais_id' => 'Pais',
			'clasificacion_id' => 'Clasificacion',
			'fuente_financiamiento_id' => 'Fuente Financiamiento',
			'descripcion' => 'Descripcion',
			'nombre' => 'Nombre',
			'tipo_zona_id' => 'Tipo Zona',
			'lon' => 'Lon',
			'lat' => 'Lat',
			'cod_sap' => 'Cod Sap',
			'programa_id' => 'Programa',
			'titulo_terreno_id' => 'Titulo Terreno',
			'transf_terreno' => 'Transf Terreno',
			'cond_transf_id' => 'Cond Transf',
			'lindero_norte' => 'Lindero Norte',
			'lindero_sur' => 'Lindero Sur',
			'lindero_este' => 'Lindero Este',
			'lindero_oeste' => 'Lindero Oeste',
			'costo_vivienda' => 'Costo Vivienda',
			'organismo_titular_terreno' => 'Organismo Titular Terreno',
			'metraje_vivienda' => 'Metraje Vivienda',
			'ha_area_total_terreno' => 'Ha Area Total Terreno',
			'centro_electoral' => 'Centro Electoral',
			'estatus_construpatria' => 'Estatus Construpatria',
			'nuevo_pedido' => 'Nuevo Pedido',
			'migrado_sap' => 'Migrado Sap',
			'modificado_osv' => 'Modificado Osv',
			'estatus_anteproyecto' => 'Estatus Anteproyecto',
			'estatus_bancoproyecto' => 'Estatus Bancoproyecto',
			'nro_id_terreno' => 'Nro Id Terreno',
			'ced_catastro' => 'Ced Catastro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tipo_proyecto_id',$this->tipo_proyecto_id);
		$criteria->compare('empresa_ejecutora_id',$this->empresa_ejecutora_id,true);
		$criteria->compare('estatus_ejecucion_id',$this->estatus_ejecucion_id);
		$criteria->compare('estatus_propuesta_id',$this->estatus_propuesta_id);
		$criteria->compare('motivo_paralizado_id',$this->motivo_paralizado_id,true);
		$criteria->compare('fe_paralizado',$this->fe_paralizado,true);
		$criteria->compare('fe_inicio',$this->fe_inicio,true);
		$criteria->compare('fe_fin',$this->fe_fin,true);
		$criteria->compare('otro_ente_organismo',$this->otro_ente_organismo,true);
		$criteria->compare('linea_accion_id',$this->linea_accion_id);
		$criteria->compare('estado_id',$this->estado_id);
		$criteria->compare('municipio_id',$this->municipio_id);
		$criteria->compare('parroquia_id',$this->parroquia_id);
		$criteria->compare('sector',$this->sector,true);
		$criteria->compare('calle',$this->calle,true);
		$criteria->compare('monto_inversion',$this->monto_inversion);
		$criteria->compare('monto_materiales',$this->monto_materiales);
		$criteria->compare('parcela',$this->parcela);
		$criteria->compare('viviendas',$this->viviendas);
		$criteria->compare('fases',$this->fases);
		$criteria->compare('creado_por',$this->creado_por);
		$criteria->compare('latitud',$this->latitud,true);
		$criteria->compare('cemento',$this->cemento,true);
		$criteria->compare('cabilla',$this->cabilla,true);
		$criteria->compare('agregado',$this->agregado,true);
		$criteria->compare('electricidad',$this->electricidad,true);
		$criteria->compare('urbanismo',$this->urbanismo,true);
		$criteria->compare('cant_agregado',$this->cant_agregado);
		$criteria->compare('agua',$this->agua,true);
		$criteria->compare('cant_cemento',$this->cant_cemento);
		$criteria->compare('longitud',$this->longitud,true);
		$criteria->compare('cant_cabilla',$this->cant_cabilla);
		$criteria->compare('agua_servida',$this->agua_servida,true);
		$criteria->compare('fe_registro',$this->fe_registro,true);
		$criteria->compare('fuente_financiamiento',$this->fuente_financiamiento,true);
		$criteria->compare('monto_por_ejecutar',$this->monto_por_ejecutar);
		$criteria->compare('monto_faltante_materiales',$this->monto_faltante_materiales);
		$criteria->compare('monto_ejecutado',$this->monto_ejecutado);
		$criteria->compare('monto_aprobado',$this->monto_aprobado);
		$criteria->compare('centro_acopio',$this->centro_acopio);
		$criteria->compare('cant_fam_benef',$this->cant_fam_benef);
		$criteria->compare('cant_brigadas',$this->cant_brigadas);
		$criteria->compare('cant_personas',$this->cant_personas);
		$criteria->compare('cant_brigadistas',$this->cant_brigadistas);
		$criteria->compare('rif_empresa_ejecutora',$this->rif_empresa_ejecutora,true);
		$criteria->compare('convenio_internacional',$this->convenio_internacional,true);
		$criteria->compare('suvi',$this->suvi,true);
		$criteria->compare('pais_id',$this->pais_id);
		$criteria->compare('clasificacion_id',$this->clasificacion_id);
		$criteria->compare('fuente_financiamiento_id',$this->fuente_financiamiento_id,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('tipo_zona_id',$this->tipo_zona_id);
		$criteria->compare('lon',$this->lon);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('cod_sap',$this->cod_sap,true);
		$criteria->compare('programa_id',$this->programa_id);
		$criteria->compare('titulo_terreno_id',$this->titulo_terreno_id);
		$criteria->compare('transf_terreno',$this->transf_terreno);
		$criteria->compare('cond_transf_id',$this->cond_transf_id);
		$criteria->compare('lindero_norte',$this->lindero_norte,true);
		$criteria->compare('lindero_sur',$this->lindero_sur,true);
		$criteria->compare('lindero_este',$this->lindero_este,true);
		$criteria->compare('lindero_oeste',$this->lindero_oeste,true);
		$criteria->compare('costo_vivienda',$this->costo_vivienda);
		$criteria->compare('organismo_titular_terreno',$this->organismo_titular_terreno,true);
		$criteria->compare('metraje_vivienda',$this->metraje_vivienda);
		$criteria->compare('ha_area_total_terreno',$this->ha_area_total_terreno);
		$criteria->compare('centro_electoral',$this->centro_electoral,true);
		$criteria->compare('estatus_construpatria',$this->estatus_construpatria);
		$criteria->compare('nuevo_pedido',$this->nuevo_pedido);
		$criteria->compare('migrado_sap',$this->migrado_sap);
		$criteria->compare('modificado_osv',$this->modificado_osv);
		$criteria->compare('estatus_anteproyecto',$this->estatus_anteproyecto);
		$criteria->compare('estatus_bancoproyecto',$this->estatus_bancoproyecto);
		$criteria->compare('nro_id_terreno',$this->nro_id_terreno,true);
		$criteria->compare('ced_catastro',$this->ced_catastro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Propuesta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
