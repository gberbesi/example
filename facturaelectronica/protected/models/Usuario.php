<?php

/**
 * This is the model class for table "usuario".
 *
 * The followings are the available columns in table 'usuario':
 * @property string $usuario_id
 * @property string $usuario
 * @property string $clave
 * @property string $tx_pregunta_secreta
 * @property string $tx_respuesta_pregunta
 * @property string $tx_cod_activacion
 * @property string $rol_id
 * @property boolean $usuario_activo_sistema
 * @property boolean $bo_temporal
 * @property boolean $estatus
 * @property string $usuario_id_aud
 * @property string $persona_id
 *
 * The followings are the available model relations:
 * @property Rol $rol
 * @property Persona $persona
 * @property UsuarioUnidadProduccion[] $usuarioUnidadProduccions
 */
class Usuario extends CActiveRecord
{
	public $recordarme;
	public $repetirclave;
	public $nueva;
	public $repetirnueva;
	public $_identificacion;
	public $cedula;
	public $nacionalidad;
	public $nombre1;
	public $nombre2;
	public $apellido1;	
	public $apellido2;	
	public $correo;
	public $telefono;
	public $telefono2;
	public $fecha_nacimiento;
	public $rol;
	public $instituciones=array();
	public $ente;
	
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'seguridad.usuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		
			array('usuario', 'required' , 'on'=>'recuperar'),

			array('usuario, clave, cedula, nombre1, apellido1, correo, telefono,nacionalidad,fecha_nacimiento', 'required', 'on'=>'reg'),
			array('usuario, clave, cedula, nombre1,fecha_nacimiento', 'required', 'on'=>'reg_modi'),
			array('clave', 'length','min'=>4, 'on'=>'reg,reg_modi'),
			array('nombre1, apellido1,nombre2, apellido2', 'match', 'pattern'=>'/^([a-zA-ZÁÉÍÓÚáéíóúñÑ\s])+$/', 'message' => 'Este campo puede contener solamente letras', 'on'=>'reg,reg_modi'),

			array('correo', 'match', 'pattern'=>'/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/', 'message' => 'Correo no valido. Formato valido: Ejemplo@tudominio.com'),

			array('usuario', 'usuarioUnicoAdmin', 'on'=>'reg'),
			array('cedula', 'cedulaUnicoAdmin', 'on'=>'reg'),	
			array('correo', 'correoUnicoAdmin', 'on'=>'reg'),			
			array('bo_activo', 'safe', 'on'=>'reg'),
			array('cedula', 'numerical', 'integerOnly'=>true, 'on'=>'reg,reg_modi'),
			array('cedula', 'numerical', 'integerOnly'=>true, 'min'=>1, 'on'=>'reg,reg_modi'),
			array('telefono', 'length', 'min'=>11, 'on'=>'reg,reg_modi'),

			array('usuario, clave', 'required' , 'on'=>'login'),
			array('clave', 'autenticar','on'=>'login'),
			array('recordarme', 'safe','on'=>'login'),
			
//			array('usuario_id','required','on'=>'usuario_reporte'),
			array('usuario_id','required','message'=>'Indique {attribute}','on'=>'usuario_reporte'),

			array('clave', 'required','message' => 'Debe ingresar una contraseña valida','on'=>'cambio'),
			array('nueva', 'required','message' => 'Debe escribir una contraseña valida','on'=>'cambio'),
			array('repetirnueva', 'required','message' => 'Debe escribir una contraseña valida','on'=>'cambio'),
			array('nueva', 'compare', 'compareAttribute'=>'repetirnueva','message' => 'Las contraseñas nuevas no son iguales', 'on'=>'cambio'),
			array('repetirnueva', 'compare', 'compareAttribute'=>'nueva','message' => 'Las contraseñas nuevas no son iguales', 'on'=>'cambio'),
			array('nueva', 'length','min'=>4,'on'=>'cambio'),
			

			
			array('fecha_nacimiento, rol_id, usuario_activo_sistema, estatus, usuario_id_aud, persona_id, correo, telefono, nombre2, apellido2, institucion_id, cedula,ente', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('fecha_nacimiento,usuario_id, usuario, clave, rol_id, usuario_activo_sistema, estatus, usuario_id_aud, persona_id,rol, institucion_id, cedula, apellido1, nombre1, nacionalidad,ente', 'safe', 'on'=>'search'),
		);
	}
	
	public function usuarioUnico($attribute,$params)	{
		if(!$this->hasErrors())	{
			
			if($user = Usuario::model()->find('usuario ILIKE :usuario AND estatus=true',array(':usuario'=>$this->usuario))) {
				$this->addError('usuario','Usuario ya esta registrado.');
			}
				
		}
	}
	
	public function usuarioUnicoAdmin($attribute,$params)	{
		if(!$this->hasErrors('usuario'))	{
			if($user = Usuario::model()->find('usuario ILIKE :usuario AND estatus=true',array(':usuario'=>$this->usuario))) {
				if($this->isNewRecord) {
					$this->addError('usuario','Usuario ya esta registrado.');
				} else if($user->usuario_id!=$this->usuario_id){
					$this->addError('usuario','Usuario ya esta registrad.');
				}
			}
				
		}
	}
	
	public function cedulaUnicoAdmin($attribute,$params)	{
		if(!$this->hasErrors('cedula'))	{
			if($user = Usuario::model()->with('persona')->find('cedula =:nu_cedula AND estatus=true',array(':nu_cedula'=>$this->cedula))) {
				if($this->isNewRecord) {
					$this->addError('cedula','Persona ya posee usuario.');
				}
				/* else if($user->usuario_id!=$this->usuario_id){
					$this->addError('cedula','Persona ya posee usuario.');
				}*/
			}
		}
	}
	
	public function correoUnicoAdmin($attribute,$params)	{
		if(!$this->hasErrors('correo')&&!$this->hasErrors('cedula'))	{
		
			if($user = Usuario::model()->with('persona')->find('correo =:tx_correo AND estatus=true ',array(':tx_correo'=>$this->correo))) {
				if($this->isNewRecord) {
					$this->addError('correo','Correo ya esta en uso por otro usuario.');
				} else if($user->persona->cedula!=$this->cedula){
					$this->addError('correo','Correo ya esta en uso por otro usuario.');
				}
			}
		}
	}	
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'rol' => array(self::BELONGS_TO, 'Rol', 'rol_id'),
			'authitems' => array(self::MANY_MANY, 'Authitem', 'seguridad.authassignment(userid, itemname)'),
			'persona' => array(self::BELONGS_TO, 'GenPersona', 'persona_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'usuario_id' => 'Usuario',
			'usuario' => 'Cedula',
			'clave' => 'Contraseña',
			'tx_pregunta_secreta' => 'Pregunta Secreta',
			'tx_respuesta_pregunta' => 'Respuesta de Pregunta Secreta',
			'tx_cod_activacion' => 'Tx Cod Activacion',
			'rol_id' => 'Rol',
			'usuario_activo_sistema' => 'Estatus',
			'bo_temporal' => 'Bo Temporal',
			'estatus' => 'St Usuario',
			'usuario_id_aud' => 'Usuario Id Aud',
			'persona_id' => 'Persona',
			'repetirclave' => 'Repetir Contraseña',
			'nueva' => 'Nueva Contraseña',
			'repetirnueva' => 'Repetir Contraseña Nueva',
			'ente_id' => 'Sede',
			'correo' => 'Correo',
			'telefono' => 'Teléfono principal',
			'telefono2' => 'Teléfono secundario',
			'nombre1' => 'Primer Nombre',
			'nombre2' => 'Segundo Nombre',
			'apellido1' => 'Primer Apellido',
			'apellido2' => 'Segundo Apellido',
			'institucion_id' => 'Institución',
			'instituciones'=>'Acceso a instituciones',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($accion=0)
	{
 
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with=array('authitems','persona');
		$criteria->together=true;
		$criteria->compare('usuario_id',$this->usuario_id,true);
		//$criteria->compare('cedula::text',strtolower($this->cedula),false);

		$criteria->compare('LOWER("persona".apellido1::text)',strtolower(trim($this->apellido1)),true);
		$criteria->compare('LOWER("persona".nombre1::text)',strtolower(trim($this->nombre1)),true);
		//$criteria->compare('"persona".cedula::text',mb_strtoupper(trim($this->cedula)),true);

		$criteria->compare('upper(usuario)',mb_strtoupper($this->usuario,'utf-8'),true);
		$criteria->compare('clave',$this->clave,true);
		$criteria->compare('institucion_id',$this->institucion_id,false);
		$criteria->compare('itemname',$this->rol,false);
		$criteria->compare('usuario_activo_sistema',$this->usuario_activo_sistema,false);
		$criteria->compare('estatus',$this->estatus);
		$criteria->compare('usuario_id_aud',$this->usuario_id_aud,true);



		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
 			'sort'=>array(
            	'defaultOrder'=>'usuario_id ASC',
        	),			
		));
	}
	
	public function autenticar($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identificacion=new UserIdentity($this->usuario,$this->clave);
			if(!$this->_identificacion->autenticar()) {
				if($this->_identificacion->errorCode==UserIdentity::ERROR_USERNAME_INVALID) {
					$this->addError('usuario','Usuario no existe.');
				} else if($this->_identificacion->errorCode==UserIdentity::ERROR_PASSWORD_INVALID) {
					$this->addError('clave','Contraseña invalida.');
				} else if($this->_identificacion->errorCode==3) {
					$this->addError('usuario','Usuario bloqueado.');
				} else if($this->_identificacion->errorCode==4) {
					$this->addError('usuario','El Último dígito de su cédula no corresponde con el día de hoy para realizar operaciones en el sistema');
				}
			}
		}
	}
	
	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identificacion===null)
		{
			$this->_identificacion=new UserIdentity($this->usuario,$this->clave);
			$this->_identificacion->autenticar();
		}
		if($this->_identificacion->errorCode===UserIdentity::ERROR_NONE)
		{
			$duracion=$this->recordarme ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identificacion,$duracion);
	
			return true;
		}
		else
			return false;
	}
	

	
	protected function beforeValidate() {
		foreach ($this->getTableSchema()->columns as $column) {
			if ($column->allowNull == 1 && $this->getAttribute($column->name) == '')
				$this->setAttribute($column->name, null);
		}
	
		return parent::beforeValidate();
	}
	
	public function rol($usuario_id){
		
		$rol_=AuthAssignment::model()->find("userid=:userid",array(":userid"=>(string)$usuario_id));
		if ($rol_){
			echo $rol_->itemname;
		}
		else {
			echo "No asignado";
		}
	}	
       
}