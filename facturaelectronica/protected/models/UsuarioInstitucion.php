<?php

/**
 * This is the model class for table "seguridad.usuario_institucion".
 *
 * The followings are the available columns in table 'seguridad.usuario_institucion':
 * @property integer $usuario_id_institucion
 * @property integer $usuario_id
 * @property integer $institucion_id
 * @property boolean $estatus_institucion
 */
class UsuarioInstitucion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UsuarioInstitucion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'seguridad.usuario_institucion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usuario_id, institucion_id', 'numerical', 'integerOnly'=>true),
			array('estatus_institucion', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('usuario_id_institucion, usuario_id, institucion_id, estatus_institucion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'usuario_id'),
			'institucion' => array(self::BELONGS_TO, 'InsInstitucion', 'institucion_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'usuario_id_institucion' => 'Id Usuario Institucion',
			'usuario_id' => 'Usuario',
			'institucion_id' => 'Institucion',
			'estatus_institucion' => 'St Usuario Institucion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('usuario_id_institucion',$this->usuario_id_institucion);
		$criteria->compare('usuario_id',$this->usuario_id);
		$criteria->compare('institucion_id',$this->institucion_id);
		$criteria->compare('estatus_institucion',$this->estatus_institucion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}