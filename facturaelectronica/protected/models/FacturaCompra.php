<?php

/**
 * This is the model class for table "factura_compra".
 *
 * The followings are the available columns in table 'factura_compra':
 * @property integer $id_factura_compra
 * @property string $factura_compra
 * @property integer $id_provedor
 * @property integer $institucion_id
 * @property integer $id_moneda
 * @property string $fecha
 * @property string $id_codigo_producto
 * @property integer $dias
 * @property integer $cantidad
 * @property integer $id_descuento
 * @property string $total
 * @property integer $iv
 * @property integer $otros_impuestos_id
 * @property string $observaciones
 * @property string $nombre_cliente
 * @property integer $id_articulo
 * @property integer $precio_aimpu
 * @property string $subtotal
 * @property string $total2
 * @property integer $descuento2
 * @property integer $credito
 * @property integer $efec_monto
 * @property integer $tj_nm
 * @property integer $tj_ref
 * @property integer $tj_monto
 * @property integer $ch_bnc
 * @property integer $ch_ref
 * @property integer $ch_monto
 * @property integer $trs_bnc
 * @property integer $trs_ref
 * @property integer $trs_monto
 * @property integer $ot_ref
 * @property integer $ot_monto
 * @property integer $total_payment
 * @property integer $total_completo
 * @property integer $vuelto
 * @property integer $contingencia
 * @property string $tipoexoneracion
 * @property integer $documentoexo
 * @property string $instituexo
 * @property string $fechaexo
 * @property integer $impuestoexo
 * @property integer $porcentajeexo
 * @property integer $n_conting
 * @property string $fecha_conting
 * @property string $razon_cont
 */
class FacturaCompra extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'factura_compra';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('factura_compra, institucion_id, id_moneda, fecha, id_codigo_producto, dias, cantidad, id_descuento, total, iv, otros_impuestos_id, observaciones, nombre_cliente, id_articulo, precio_aimpu, subtotal, total2, descuento2, credito, efec_monto, tj_nm, tj_ref, tj_monto, ch_bnc, ch_ref, ch_monto, trs_bnc, trs_ref, trs_monto, ot_ref, ot_monto, total_payment, total_completo, vuelto, contingencia, tipoexoneracion, documentoexo, instituexo, fechaexo, impuestoexo, porcentajeexo, fecha_conting', 'required'),
			array('id_provedor, institucion_id, id_moneda, dias, cantidad, id_descuento, iv, otros_impuestos_id, id_articulo, precio_aimpu, descuento2, credito, efec_monto, tj_nm, tj_ref, tj_monto, ch_bnc, ch_ref, ch_monto, trs_bnc, trs_ref, trs_monto, ot_ref, ot_monto, total_payment, total_completo, vuelto, contingencia, documentoexo, impuestoexo, porcentajeexo, n_conting', 'numerical', 'integerOnly'=>true),
			array('factura_compra, id_codigo_producto, fechaexo', 'length', 'max'=>20),
			array('fecha', 'length', 'max'=>19),
			array('total, subtotal, total2', 'length', 'max'=>11),
			array('observaciones, tipoexoneracion, instituexo', 'length', 'max'=>100),
			array('fecha_conting', 'length', 'max'=>50),
			array('razon_cont', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_factura_compra, factura_compra, id_provedor, institucion_id, id_moneda, fecha, id_codigo_producto, dias, cantidad, id_descuento, total, iv, otros_impuestos_id, observaciones, nombre_cliente, id_articulo, precio_aimpu, subtotal, total2, descuento2, credito, efec_monto, tj_nm, tj_ref, tj_monto, ch_bnc, ch_ref, ch_monto, trs_bnc, trs_ref, trs_monto, ot_ref, ot_monto, total_payment, total_completo, vuelto, contingencia, tipoexoneracion, documentoexo, instituexo, fechaexo, impuestoexo, porcentajeexo, n_conting, fecha_conting, razon_cont', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_factura_compra' => 'Id Factura Compra',
			'factura_compra' => 'Factura Compra',
			'id_provedor' => 'Id Provedor',
			'institucion_id' => 'Institucion',
			'id_moneda' => 'Id Moneda',
			'fecha' => 'Fecha',
			'id_codigo_producto' => 'Id Codigo Producto',
			'dias' => 'Dias',
			'cantidad' => 'Cantidad',
			'id_descuento' => 'Id Descuento',
			'total' => 'Total',
			'iv' => 'Iv',
			'otros_impuestos_id' => 'Otros Impuestos',
			'observaciones' => 'Observaciones',
			'nombre_cliente' => 'Nombre Cliente',
			'id_articulo' => 'Id Articulo',
			'precio_aimpu' => 'Precio Aimpu',
			'subtotal' => 'Subtotal',
			'total2' => 'Total2',
			'descuento2' => 'Descuento2',
			'credito' => 'Credito',
			'efec_monto' => 'Efec Monto',
			'tj_nm' => 'Tj Nm',
			'tj_ref' => 'Tj Ref',
			'tj_monto' => 'Tj Monto',
			'ch_bnc' => 'Ch Bnc',
			'ch_ref' => 'Ch Ref',
			'ch_monto' => 'Ch Monto',
			'trs_bnc' => 'Trs Bnc',
			'trs_ref' => 'Trs Ref',
			'trs_monto' => 'Trs Monto',
			'ot_ref' => 'Ot Ref',
			'ot_monto' => 'Ot Monto',
			'total_payment' => 'Total Payment',
			'total_completo' => 'Total Completo',
			'vuelto' => 'Vuelto',
			'contingencia' => 'Contingencia',
			'tipoexoneracion' => 'Tipoexoneracion',
			'documentoexo' => 'Documentoexo',
			'instituexo' => 'Instituexo',
			'fechaexo' => 'Fechaexo',
			'impuestoexo' => 'Impuestoexo',
			'porcentajeexo' => 'Porcentajeexo',
			'n_conting' => 'N Conting',
			'fecha_conting' => 'Fecha Conting',
			'razon_cont' => 'Razon Cont',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_factura_compra',$this->id_factura_compra);
		$criteria->compare('factura_compra',$this->factura_compra,true);
		$criteria->compare('id_provedor',$this->id_provedor);
		$criteria->compare('institucion_id',$this->institucion_id);
		$criteria->compare('id_moneda',$this->id_moneda);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('id_codigo_producto',$this->id_codigo_producto,true);
		$criteria->compare('dias',$this->dias);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('id_descuento',$this->id_descuento);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('iv',$this->iv);
		$criteria->compare('otros_impuestos_id',$this->otros_impuestos_id);
		$criteria->compare('observaciones',$this->observaciones,true);
		$criteria->compare('nombre_cliente',$this->nombre_cliente,true);
		$criteria->compare('id_articulo',$this->id_articulo);
		$criteria->compare('precio_aimpu',$this->precio_aimpu);
		$criteria->compare('subtotal',$this->subtotal,true);
		$criteria->compare('total2',$this->total2,true);
		$criteria->compare('descuento2',$this->descuento2);
		$criteria->compare('credito',$this->credito);
		$criteria->compare('efec_monto',$this->efec_monto);
		$criteria->compare('tj_nm',$this->tj_nm);
		$criteria->compare('tj_ref',$this->tj_ref);
		$criteria->compare('tj_monto',$this->tj_monto);
		$criteria->compare('ch_bnc',$this->ch_bnc);
		$criteria->compare('ch_ref',$this->ch_ref);
		$criteria->compare('ch_monto',$this->ch_monto);
		$criteria->compare('trs_bnc',$this->trs_bnc);
		$criteria->compare('trs_ref',$this->trs_ref);
		$criteria->compare('trs_monto',$this->trs_monto);
		$criteria->compare('ot_ref',$this->ot_ref);
		$criteria->compare('ot_monto',$this->ot_monto);
		$criteria->compare('total_payment',$this->total_payment);
		$criteria->compare('total_completo',$this->total_completo);
		$criteria->compare('vuelto',$this->vuelto);
		$criteria->compare('contingencia',$this->contingencia);
		$criteria->compare('tipoexoneracion',$this->tipoexoneracion,true);
		$criteria->compare('documentoexo',$this->documentoexo);
		$criteria->compare('instituexo',$this->instituexo,true);
		$criteria->compare('fechaexo',$this->fechaexo,true);
		$criteria->compare('impuestoexo',$this->impuestoexo);
		$criteria->compare('porcentajeexo',$this->porcentajeexo);
		$criteria->compare('n_conting',$this->n_conting);
		$criteria->compare('fecha_conting',$this->fecha_conting,true);
		$criteria->compare('razon_cont',$this->razon_cont,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FacturaCompra the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
