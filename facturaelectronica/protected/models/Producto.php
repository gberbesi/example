<?php

/**
 * This is the model class for table "producto".
 *
 * The followings are the available columns in table 'producto':
 * @property integer $id_producto
 * @property string $cod_producto
 * @property string $descripcion
 * @property string $calidad
 * @property double $precio
 * @property boolean $disponible
 *
 * The followings are the available model relations:
 * @property Facrura[] $facruras
 */
public $id_productos;
class Producto extends CActiveRecord
{
	public $id_productos;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Producto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'producto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
			array('cant,id_medida', 'required','on'=>'cant'),
		     array('cant', 'length', 'max'=>5,'on'=>'cant'),
		    array('precio', 'length', 'max'=>10),
			array('precio', 'required'),
			array('cod_producto', 'required'),
			//array('cod_producto', 'codUnico','on'=>'reg'),
			//array('disponible', 'safe','on'=>'cant'),
			array('codigo','numerical')
			array('cod_fac,descripcion, calidad, disponible,comprar,total,fecha,cliente,lista_impuesto_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('cod_fac,cliente,fecha,id_medida,id_producto, cod_producto, descripcion, calidad, precio, disponible,comprar,cant,total,lista_impuesto_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'facruras' => array(self::HAS_MANY, 'Facrura', 'id_producro'),
			'Medida' => array(self::BELONGS_TO, 'Medida', 'id_medida'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_producto' => 'Id Producto',
			'cod_producto' => 'Cod Producto',
			'descripcion' => 'Descripción',
			'calidad' => 'Calidad',
			'precio' => 'Precio',
			'disponible' => 'Disponible',
			'total'=>'Total',
			'cant'=>'Cantidad',
			'id_medida'=>'Unidad de Medida'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_producto',$this->id_producto);
		$criteria->compare('cod_producto',$this->cod_producto,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('calidad',$this->calidad,true);
		$criteria->compare('precio',$this->precio);
		$criteria->compare('disponible',$this->disponible);
		$criteria->compare('total',$this->total);
		$criteria->compare('id_medida',$this->id_medida);
		$criteria ->order = 'id_producto desc';
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave() {  //PARA CAMBIAR AL FORMATO QUE ACEPTA POSTGRES ANTES DE GUARDAR
	
		//$this->cant=str_replace('.', '', $this->cant);
		//$this->cant=str_replace(',', '.', $this->cant);
		if($this->cant =="")
			$this->cant = NULL;
		return parent::beforeSave();
	}
	
}