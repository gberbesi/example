<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Factura E </title>

    <!-- Bootstrap -->
    <link href="../facturaelectronica/themes/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../facturaelectronica/themes/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../facturaelectronica/themes/gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../facturaelectronica/themes/gentelella/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center" style="color:white">
              <h1 class="error-number"> Algo ha salido mal!</h1>
              <h2>Lo sentimos estamos trabajando para resolver este inconveniente. </h2>
              <p>El equipo de Sistemas ya ha sido notificado de este Problema.
              	<br><br>
              	<b>Para Volver al nuestro sistema </b><br><br>
              	<div><a href="<?php echo Yii::app()->createUrl('/'); ?>"><button  class="btn  col-lg-4 col-xs-12 col-lg-offset-4" style="color:black"   ><strong> Presione  Aquí</strong></button></a></div>
              </p><br><br><br><br>
              <br>
              <p>Código de Operación :Exception500</p>
              
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    </div>

    <!-- jQuery -->
   <script src="themes/gentelella/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../facturaelectronica/themes/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../facturaelectronica/themes/gentelella/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="../facturaelectronica/themes/gentelella/vendors/nprogress/nprogress.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../facturaelectronica/themes/gentelella/build/js/custom.min.js"></script>
  </body>
</html>
