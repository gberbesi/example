<?php

/**
 * This is the model class for table "clientes".
 *
 * The followings are the available columns in table 'clientes':
 * @property integer $id_cliente
 * @property string $clientes
 * @property string $telefono
 * @property string $fax
 * @property string $correo
 * @property string $apartado
 * @property string $web
 * @property string $correo2
 * @property string $pais
 * @property integer $id_provincia
 * @property integer $id_canton
 * @property integer $id_distrito
 * @property string $direccion
 * @property string $identificacion
 * @property integer $id_tipo_identificacioj
 * @property integer $area1
 * @property integer $area2
 * @property integer $canton
 */
class Clientes extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'clientes';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id_cliente, clientes, telefono, fax, correo, apartado, web, correo2, pais, id_provincia, id_canton, id_distrito, identificacion, id_tipo_identificacioj, area1, area2, canton', 'required'),
            array(' id_provincia, id_canton, id_distrito, id_tipo_identificacioj, area1, area2, canton', 'numerical', 'integerOnly'=>true),
            array('clientes, telefono, fax, apartado, web, identificacion', 'length', 'max'=>20),
            array('correo, correo2, direccion', 'length', 'max'=>100),
            array('pais', 'length', 'max'=>50),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id_cliente, clientes, telefono, fax, correo, apartado, web, correo2, pais, id_provincia, id_canton, id_distrito, direccion, identificacion, id_tipo_identificacioj, area1, area2, canton', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id_cliente' => 'Id Cliente',
            'clientes' => 'Clientes',
            'telefono' => 'Telefono',
            'fax' => 'Fax',
            'correo' => 'Correo',
            'apartado' => 'Apartado',
            'web' => 'Web',
            'correo2' => 'Correo2',
            'pais' => 'Pais',
            'id_provincia' => 'Id Provincia',
            'id_canton' => 'Id Canton',
            'id_distrito' => 'Id Distrito',
            'direccion' => 'Direccion',
            'identificacion' => 'Identificacion',
            'id_tipo_identificacioj' => 'Id Tipo Identificacioj',
            'area1' => 'Area1',
            'area2' => 'Area2',
            'canton' => 'Canton',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id_cliente',$this->id_cliente);
        $criteria->compare('clientes',$this->clientes,true);
        $criteria->compare('telefono',$this->telefono,true);
        $criteria->compare('fax',$this->fax,true);
        $criteria->compare('correo',$this->correo,true);
        $criteria->compare('apartado',$this->apartado,true);
        $criteria->compare('web',$this->web,true);
        $criteria->compare('correo2',$this->correo2,true);
        $criteria->compare('pais',$this->pais,true);
        $criteria->compare('id_provincia',$this->id_provincia);
        $criteria->compare('id_canton',$this->id_canton);
        $criteria->compare('id_distrito',$this->id_distrito);
        $criteria->compare('direccion',$this->direccion,true);
        $criteria->compare('identificacion',$this->identificacion,true);
        $criteria->compare('id_tipo_identificacioj',$this->id_tipo_identificacioj);
        $criteria->compare('area1',$this->area1);
        $criteria->compare('area2',$this->area2);
        $criteria->compare('canton',$this->canton);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Clientes the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}   