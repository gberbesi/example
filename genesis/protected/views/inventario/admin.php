<?php
/* @var $this InventarioController */
/* @var $model Inventario */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('inventario-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'inventario-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'table table-striped table-responsive  text-center',
                        'pagerCssClass'=>'pagination pull-right',
                  
	'filter'=>$model,
	'columns'=>array(

		 array(
      			'header'=>'<p style="color:#22C7FC;font-weight:bold;font-size: 130%;" align="center">Id</p>',
 				'name'=>'id_producto',
 
 				'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  				'value'=>'$data->id_producto',
    ),
		 array(
      			'header'=>'<p style="color:#22C7FC;font-weight:bold;font-size: 130%;" align="center">Codigo</p>',
 				'name'=>'codigo',
 
 				'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  				'value'=>'$data->codigo',
    ),
    		 array(
      			'header'=>'<p style="color:#22C7FC;font-weight:bold;font-size: 130%;" align="center">Nombre</p>',
 				'name'=>'nombre',
 
 				'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  				'value'=>'$data->nombre',
    ),
    		 		 array(
      			'header'=>'<p style="color:#22C7FC;font-weight:bold;font-size: 130%;" align="center">Cantidad</p>',
 				'name'=>'cantidad',
 
 				'htmlOptions'=>array('class'=>'col-md-0 text-center'), 
  				'value'=>'$data->cantidad',
    ),

		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
