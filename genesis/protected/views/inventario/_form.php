<?php
/* @var $this InventarioController */
/* @var $model Inventario */
/* @var $form CActiveForm */
?>
<h1 align="center" style="color:#2a3f54;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">Agregar Productos</h1>
 <div class="col-md-12 col-sm-12 col-xs-12 form-group "> <br><br><br>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'inventario-form',
	'enableAjaxValidation'=>false,
)); ?>




     <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group ">
     <label for="fullname">Codigo del producto</label>
      <?php echo $form->textField($model,'codigo',array('type'=>'text','id'=>'codigo','class'=>'form-control','onChange'=>'consecutiva()')); ?>
       <?php echo $form->error($model,'codigo',array('class'=>'btn-xs alert-danger text-center')); ?>
       
    </div>

         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group ">
     <label for="fullname">Nombre del Producto</label>
      <?php echo $form->textField($model,'nombre',array('type'=>'text','id'=>'nombre','class'=>'form-control','onChange'=>'consecutiva()')); ?>
       <?php echo $form->error($model,'nombre',array('class'=>'btn-xs alert-danger text-center')); ?>
       
    </div>

         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 form-group ">
     <label for="fullname">Cantidad de producto existente</label>
      <?php echo $form->textField($model,'cantidad',array('type'=>'text','id'=>'cantidad','class'=>'form-control','onChange'=>'consecutiva()')); ?>
       <?php echo $form->error($model,'cantidad',array('class'=>'btn-xs alert-danger text-center')); ?>
       
    </div>


	              <div>
           
                      <?php echo CHtml::submitButton('Agregar Producto' ,array('class'=>'btn btn-success col-lg-3 col-lg-offset-5 col-md-3 col-md-offset-5 col-sm-12 col-xs-12' ,'style'=>'border-radius:30px;')); ?>
                
              </div><br>
<br>


<?php $this->endWidget(); ?>

</div><!-- form -->