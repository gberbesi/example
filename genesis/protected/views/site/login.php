
<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12" style="margin-bottom:35% ">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

            <form >
            <br><br><br><br>
              <h3 align="center">Iniciar Sesión</h3><br>
                
              <div class="col col-xs-12" >
            
                <?php echo $form->textField($model,'username',array('type'=>'number', 'class'=>'form-control form-control-lg', 'placeholder'=>'Usuario')); ?><br>
              </div>
              <div class="col col-xs-12" >
               <?php echo $form->passwordField($model,'password',array('type'=>'password', 'class'=>'form-control form-control-lg', 'placeholder'=>'Contraseña')); ?><br><br>
              </div>
              <div>
           
                      <?php echo CHtml::submitButton('Entrar' ,array('class'=>'btn btn-success col-xs-8 col-xs-offset-2' ,'style'=>'border-radius:30px;')); ?>
                
              </div><br>
<br>
              <div class="clearfix"></div> <div class="clearfix"></div>
<br>
              <div class="separator">
               <br />

              </div>
            </form>
 

<?php $this->endWidget(); ?>
</div><!-- form -->

