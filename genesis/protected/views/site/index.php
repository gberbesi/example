<!--========================================================
                        SLIDER
=========================================================-->
<div id="myCarousel" class="carousel slide" data-ride="carousel" >
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner ">
    <div class="item active ">
      <img style="display: block; margin-bottom: 17px; height: 500px;" src="img/slide-1.jpg" alt="Los Angeles">
          <div class="carousel-caption txtalingcar">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="box box-1">
									<div class="row">
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
											<div class="icon"><i class="fa fa-laptop"></i>
											</div>
										</div>
										<div class="col-lg-8  col-md-8  col-sm-12 col-xs-12">
											<h2>Servicio Tecnico <br><span>Reparacion de computadoras</span></h2>
											<!--<h6>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt <br>ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</h6>-->

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
     
        <div>
          <!-- <p class="parrafocarr">Referido a las prácticas, comportamiento y actitudes que aporten <br> al crecimiento del grupo de trabajo y al logro de los objetivos planteados.</p>-->
        </div>        

    </div>

    </div>

    <div class="item">
      <img style="display: block; margin-bottom: 17px; height: 500px;" src="img/slide-2.jpg" alt="Chicago">
                <div class="carousel-caption txtalingcar">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="box box-1">
									<div class="row">
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
											<div class="icon"><i class="fa fa-tablet"></i>
											</div>
										</div>
										<div class="col-lg-8  col-md-8  col-sm-12 col-xs-12">
											<h2>Servicio Tecnico <br><span>Reparacion de tablets</span></h2>
											<h4>Cambio de conectores de carga.

Cambio de Mica Táctil.

Actualización <br>de Software.

Transferencia de Datos & BackUp.

.</h4>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
     
        <div>
          <!-- <p class="parrafocarr">Referido a las prácticas, comportamiento y actitudes que aporten <br> al crecimiento del grupo de trabajo y al logro de los objetivos planteados.</p>-->
        </div>        

    </div>
    </div>

        <div class="item">
      <img style="display: block; margin-bottom: 17px; height: 500px;" src="img/slide-2.jpg" alt="Chicago">
                <div class="carousel-caption txtalingcar">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="box box-1">
									<div class="row">
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
											<div class="icon"><i class="fa fa-mobile-phone"></i>
											</div>
										</div>
										<div class="col-lg-8  col-md-8  col-sm-12 col-xs-12">
											<h2>Servicio Tecnico <br><span>Reparacion de telefono</span></h2>
											<h4>Cambio de conectores de carga.

Cambio de Mica Táctil.

Actualización <br>de Software.

Transferencia de Datos & BackUp.

.</h4>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
     
        <div>
          <!-- <p class="parrafocarr">Referido a las prácticas, comportamiento y actitudes que aporten <br> al crecimiento del grupo de trabajo y al logro de los objetivos planteados.</p>-->
        </div>        

    </div>
    </div>

    <div class="item">
      <img style="display: block; margin-bottom: 17px; height: 500px;" src="img/slide-3.jpg" alt="New York">
                <div class="carousel-caption txtalingcar">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="box box-1">
									<div class="row">
										<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
											<div class="icon"><i class="fa fa-thumbs-o-up"></i>
											</div>
										</div>
										<div class="col-lg-8  col-md-8  col-sm-12 col-xs-12">
											<h2>Servicio Tecnico <br><span>Solicite cotización, Tenemos los <br>mejores Precios</span></h2>
											<h4> <br>La mayor rapidez en la reparación de sus equipos,<br> porque valoramos su tiempo.

Actualizaciones y liberaciones <br>en tiempo récord.

Software de Stock oficiales, con o sin logos <br>de operadoras..</h4>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
     
        <div>
          <!-- <p class="parrafocarr">Referido a las prácticas, comportamiento y actitudes que aporten <br> al crecimiento del grupo de trabajo y al logro de los objetivos planteados.</p>-->
        </div>        

    </div>
    </div>
  </div>

</div>
	<!--========================================================
                        	CONTENT
	=========================================================-->
	<div id="content">
		<div class="block-2 bg-1">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<h3 class="title title-1">Nuestros <span> servicios</span></h3>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
						<a class="box box-2" href="#" style="height: auto;">
							<div class="icon">
								<i class="fa fa-laptop"></i>
							</div>
							<div class="wrapper">
								<h6>Computadoras de escritorio y laptos </h6>
								<p>Reparacion, Actualizacion, Formateo. </p>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
						<a class="box box-2" href="#" style="height: auto;">
							<div class="icon">
								<i class="fa fa-tablet"></i>
							</div>
							<div class="wrapper">
								<h6>Tablets</h6>
								<p>Reparacion, Actualizacion de software, rooting, Cambio de componenetes. </p>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
						<a class="box box-2" href="#" style="height: auto;">
							<div class="icon">
								<i class="fa fa-history"></i>
							</div>
							<div class="wrapper">
								<h6>Servicio tecnico a distancia</h6>
								<p>Liberacion de bandas. </p>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
						<a class="box box-2" href="#" style="height: auto;">
							<div class="icon">
								<i class="fa  fa-mobile"></i>
							</div>
							<div class="wrapper">
								<h6>Telefonos Moviles</h6>
								<p>Reparacion, Actualizacion de software, rooting, Cambio de componenetes, Apertura de bandas. </p>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
						<a class="box box-2" href="#" style="height: auto;">
							<div class="icon">
								<i class="fa fa-cogs"></i>
							</div>
							<div class="wrapper">
								<h6>En Proceso</h6>
								<p>. </p>
							</div>
						</a>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
						<a class="box box-2" href="#" style="height: auto;">
							<div class="icon">
								<i class="fa fa-shield"></i>
							</div>
							<div class="wrapper">
								<h6>En Proceso</h6>
								<p>. </p>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="block-3 parallax parallax-bg-2">
			<div class="box box-3">
				<div class="container">
					<div class="row">
						<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
							<div class="icon" style=" background:white;">
								<img style=" height: 120px;" src="img/154508.png" >
							</div>
							<div class="wrapper">
								<h2>Diagnosticamos <br><span>Y Reparamos su problema</span></h2>
							</div>

						</div>
						<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
							<h6 style="font-size: 15px;">Somos un servicio técnico en electrónica, especializado en la reparación actualización y liberación de teléfonos celulares.</h6>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--
		<div class="block-4">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<h3 class="title title-1"><span>Computer</span> Repair</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
						<div class="box box-4" style="height: auto;">
							<h6>Lorem ipsum dolor sit amet cons</h6>
							<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident. Lorem ipsum dolor sit amet conse ctetur adipisicin.</p>
							<a href="https://livedemo00.template-help.com/wt_53695_new/#" class="btn-default btn btn-3">Learn more</a>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
						<div class="box box-4" style="height: auto;">
							<h6>Lorem ipsum dolor sit amet cons</h6>
							<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident. Lorem ipsum dolor sit amet conse ctetur adipisicin.</p>
							<a href="https://livedemo00.template-help.com/wt_53695_new/#" class="btn-default btn btn-3">Learn more</a>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
						<div class="box box-4" style="height: auto;">
							<h6>Lorem ipsum dolor sit amet cons</h6>
							<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident. Lorem ipsum dolor sit amet conse ctetur adipisicin.</p>
							<a href="https://livedemo00.template-help.com/wt_53695_new/#" class="btn-default btn btn-3">Learn more</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="block-5 img-bg-1">
			<div class="container">
				<div class="row">
					<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
						<div class="box box-5">
							<h2>Questions? <span>Live chat with us!</span></h2>
							<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
							<a href="https://livedemo00.template-help.com/wt_53695_new/#" class="btn-default btn btn-2">Live Chat</a>
						</div>
					-->
					</div>
				</div>
			</div>
		</div>
	</div>
